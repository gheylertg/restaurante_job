<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/


$route['default_controller'] = 'Login';
$route['default_controller'] = 'Login';
$route['dashboard'] = 'Acceso/dashboard';



$route['login'] = 'Login';

$route['admin']   = 'Login/admin';
$route['empresa']   = 'Login/empresa';
$route['selEmpresa']   = 'Acceso/selEmpresa';
$route['selSucursal']   = 'Acceso/selSucursal';

$route['generaMenuEmpresa'] = 'Acceso/generarMenu_adminEmpresa';

//$route['empresa']   = 'Login/empresa';




$route['generarMenuAdmin'] = 'Acceso/generarMenu_admin';
$route['generarMenuAdminEmpresa'] = 'Acceso/generarMenu_adminEmpresa';
$route['generarMenu_adminSucursal'] = 'Acceso/generarMenu_adminSucursal';

$route['empresa'] = 'Login/empresa';


$route['acceso'] = 'Acceso';
$route['accesoAdmin'] = 'Acceso/accesoAdmin';
$route['valAccesoAdmin'] = 'Acceso/valAccesoAdmin';


//Modulos del sistema
$route['tablaGenerica'] = 'TablaGenerica/index';
$route['modulo'] = 'Sg_modulo/index';
$route['subModulo'] = 'Es_subModulo/index';

//seguridad
$route['rolesUsuario'] = 'Sg_rolesUsuario/index';
$route['usuario'] = 'Sg_usuario/index';
$route['rolModulo'] = 'Sg_rolModulo/index';
//$route['rolModuloActualizar/(:any)'] = 'Sg_rolModulo/actualizarModulo/$1';
$route['rolModuloActualizar'] = 'Sg_rolModulo/actualizarModulo';
$route['rolModuloProceso'] = 'Sg_rolModulo/actualizarProceso';


$route['administradorGeneral'] = 'Sg_administradorGeneral/index';
$route['administradorEmpresa'] = 'Sg_administradorEmpresa/index';
$route['administradorSucursal'] = 'Sg_administradorSucursal/index';
$route['personalSucursal'] = 'Sg_personalSucursal/index';               

//usuario - empresa -rol
$route['usuarioEmpresa'] = 'Sg_usuarioEmpresa/index';
$route['usuarioEmpresaActualizar'] = 'Sg_usuarioEmpresa/actualizarEmpresa';
$route['usuarioEmpresa_AM'] = 'Sg_usuarioEmpresa/actualizarEmpresaModulo';
$route['usuarioModuloProceso'] = 'Sg_usuarioEmpresa/actualizarProceso';


//mantenimiento
$route['ingrediente'] = 'Mt_ingrediente/index';
$route['categoriaProducto'] = 'Mt_categoriaProducto/index';
$route['unidadMedida'] = 'Mt_unidadMedida/index';
$route['producto'] = 'Mt_producto/index';
$route['proveedor'] = 'Mt_proveedor/index';
$route['tipoProducto'] = 'Mt_tipoProducto/index';
$route['condicionPago'] = 'Mt_condicionPago/index';

$route['tipoDinero'] = 'Mt_tipoDinero/index';
$route['tipoMovimientoCaja'] = 'Mt_tipoMovimientoCaja/index';




//Configuracion Sistema.
$route['actEmpresa'] = 'Cf_empresa/index';
$route['sucursal'] = 'Cf_sucursal/index';
$route['region'] = 'Cf_region/index';
$route['dolarImpuesto'] = 'Ad_dolarImpuesto/index';



//Inventario
$route['conceptoInventario'] = 'In_conceptoInventario/index';
$route['inventario'] = 'In_inventario/index';
$route['entradaInventario'] = 'In_entrada/index';
$route['salidaInventario'] = 'In_salida/index';


//$route['incEntrada'] = 'In_entrada/incEntrada';



//*****************************Compra
//orden compra
$route['actCompra'] = 'Cp_orden/index';
$route['incOrden'] = 'Cp_orden/incOrden';
$route['verOrden/(:any)'] = 'Cp_orden/verOrden/$1';
$route['act_mp/(:any)'] = 'Cp_orden/act_modificar_producto/$1';

//registro factura
$route['actFactura'] = 'Cp_factura/index';
$route['incFactura'] = 'Cp_factura/incFactura';
$route['verFactura/(:any)'] = 'Cp_factura/verFactura/$1';
$route['act_mpf/(:any)'] = 'Cp_factura/act_modificar_producto/$1';


//*************************Administracion
$route['sala'] = 'Ad_sala/index';
$route['categoriaADM'] = 'Ad_categoria/index';
$route['medidaADM'] = 'Ad_medida/index';
$route['mesaADM'] = 'Ad_mesa/index';
$route['mesonero'] = 'Ad_mesonero/index';




//**************** Contendor **************
$route['contenedor'] = 'Co_contenedor/index';



//************* Despacho *******************
$route['despacho'] = 'Dp_despacho/index';
$route['despachoActivo'] = 'Dp_despacho/despachoActivo';


$route['despachoCliente'] = 'Dp_despacho/despachoCliente';

$route['despachoRetiro'] = 'Dp_despacho/cargaRetiro';
$route['despachoDelivery'] = 'Dp_despacho/cargaDelivery';

$route['preCuenta/(:any)'] = 'Dp_despacho/imprimir_preCuenta/$1';





//***********************caja
$route['aperturaCaja'] = 'Cj_aperturaCaja/index';
$route['movimientoCaja'] = 'Cj_movimientoCaja/index';
$route['cobroCaja'] = 'Cj_cobroCaja/index';
$route['cierreCaja'] = 'Cj_cierreCaja/index';
$route['detalleCaja'] = 'Cj_cierreCaja/resumenCajaUsuario';


//----------------------- administrador Menu
$route['categoriaMenu'] = 'Mn_categoriaMenu/index';
$route['ingrediente'] = 'Mn_ingrediente/index';
$route['alimento'] = 'Mn_alimento/index';
$route['incAlimento'] = 'Mn_alimento/incAlimento';
$route['act_mIngrediente/(:any)'] = 'mn_alimento/act_modificar_ingrediente/$1';
$route['act_mPresentacion/(:any)'] = 'mn_alimento/act_presentacion/$1';
$route['incPresentacion'] = 'Mn_alimento/incPresentacion';
$route['act_pIngrediente/(:any)'] = 'mn_presentacionIngrediente/act_presentacion_ingrediente/$1';
  

$route['envoltura'] = 'Ad_envoltura/index';
$route['ingredienteAdicional'] = 'Mn_ingredienteAdicional/index';





$route['verAlimento/(:any)'] = 'Mn_alimento/verAlimento/$1';
//$route['actSabor'] = 'Cp_factura/index';



//facturacion
$route['sucursalFacturacion'] = 'Fa_empresa/index';
$route['totalesFacturacion'] = 'Fa_empresa/totalesFacturacion';
//facturacion // sucursal
$route['consultaFactura_S'] = 'Fa_sucursal/index';
$route['totalesFacturacion_S'] = 'Fa_sucursal/totalesFacturacion';



//reporte pdf

//reporte sucursalFactura
$route['pdf_sucursalFactura'] = 'Fa_empresa/imprimir_sucursalFactura';
$route['pdf_totalesFactura'] = 'Fa_empresa/imprimir_totalesFactura';


$route['pdf_facturaSucursal'] = 'Fa_sucursal/imprimir_sucursalFactura';
$route['pdf_individualFactura/(:any)'] = 'Fa_sucursal/imprimir_individualFactura/$1';
$route['pdf_totalesFactura_S'] = 'Fa_sucursal/imprimir_totalesFactura';


$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;






