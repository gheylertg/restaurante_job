<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Acceso extends CI_Controller {
	


	public function __construct() {
         parent::__construct();
         $this->load->model(array('Sg_usuarioEmpresaModulo_mdl', 'Cj_aperturaCaja_mdl', 'Sg_usuarioEmpresa_mdl','Sg_usuario_mdl',
         'Sg_administradorGeneral_mdl','Sg_administradorEmpresa_mdl','Sg_administradorSucursal_mdl'));
         $this->load->library(array('Clssession')); 

    }


    public function accesoAdmin(){
    	$run = $this->input->post('usuario');
    	//$clave = $this->input->post('pass');
    	$password = $this->input->post('pass');
        $password = md5($password);

    	$valUsuario = $this->Sg_administradorGeneral_mdl->valLoginAdm($run, $password);

        $idUsuario = 0; 
        $validar=0;
        if($valUsuario==false){
            $valUsuario = 0;
        }else{
            $validar = 1;
            $idUsuario = $valUsuario->id;
        }    



        if($validar==1){
        	$rowUsuario = $this->Sg_administradorGeneral_mdl->obtInfUsuario($idUsuario);
            $nombreApellido = trim($rowUsuario->nombre) . ", " . trim($rowUsuario->apellido);
            $idUsuario = $rowUsuario->id;
            $administrador = 1;
            $idEmpresa = 1;
            $idSucursal = 1;
			$row = $this->Sg_usuarioEmpresa_mdl->obtDatosUsuarioEmpresa($idUsuario, $idEmpresa, $idSucursal);
			$empresa = $row->empresa;
			$sucursal = $row->sucursal;

			$sucursal_rut = $row->sucursal_rut;
			$sucursal_direccion = $row->sucursal_direccion;
			$sucursal_correo = $row->sucursal_correo;
			$sucursal_telefono = $row->sucursal_telefono;
					
			$usuario = $row->usuario . ", " . $row->apellido;
			$datosUES = trim($usuario) . " / " . trim($empresa) . " / " . trim($sucursal); 

			$datoSucursal = trim($empresa) . " / " . trim($sucursal); 

            $data = array(
            	'idUsuario'=>$rowUsuario->id,
                'run'=>$rowUsuario->run,
            	'administrador'=>1,
            	'idEmpresa'=>1,
            	'idSucursal'=>1,
            	'empresa'=>$row->empresa,
            	'sucursal'=>$row->sucursal,
				'sucursal_rut'=>$sucursal_rut,
				'sucursal_direccion'=>$sucursal_direccion,
				'sucursal_correo'=>$sucursal_correo,
				'sucursal_telefono'=>$sucursal_telefono,
            	'nombre'=>$rowUsuario->nombre,
            	'nombreUsuario'=>$nombreApellido,
            	'apellido'=>$rowUsuario->apellido,
            	'usuario'=>$usuario,
            	'idUsuarioEmpresa'=>$row->id,
            	'nombreRol'=>$row->rol,
            	'idCaja'=>0,
            	'logoEmpresa'=>"",
            	'imagenEmpresa'=>"",
            	'datosUES'=>$datosUES,
            	'datoSucursal'=>$datoSucursal,
            	'email'=>$rowUsuario->email,
            	'foto'=>$rowUsuario->foto,
            	'sexo'=>$rowUsuario->sexo,
            	'idRol'=>1,
            ); 

            $this->session->set_userdata($data);
        	$data = array(
               'status'=>0
        	);

            echo json_encode($data);
        }else{
        	$data = array(
               'status'=>1
        	);
            echo json_encode($data);
        }
    }


/*
public function accesoAdmin(){
        $usuario = $this->input->post('usuario');
        //$clave = $this->input->post('pass');
        $password = $this->input->post('pass');
        $password = md5($password);

        $valUsuario = $this->Sg_administradorGeneral_mdl->valLoginAdm($usuario, $password);
        if($valUsuario==1){
            $rowUsuario = $this->Sg_administradorGeneral_mdl->obtInfUsuario($usuario, $password);
            $nombreApellido = trim($rowUsuario->nombre) . ", " . trim($rowUsuario->apellido);
            $idUsuario = $rowUsuario->id;
            $administrador = 1;
            $idEmpresa = 1;
            $idSucursal = 1;
            $row = $this->Sg_usuarioEmpresa_mdl->obtDatosUsuarioEmpresa($idUsuario, $idEmpresa, $idSucursal);
            $empresa = $row->empresa;
            $sucursal = $row->sucursal;

            $sucursal_rut = $row->sucursal_rut;
            $sucursal_direccion = $row->sucursal_direccion;
            $sucursal_correo = $row->sucursal_correo;
            $sucursal_telefono = $row->sucursal_telefono;
                    
            $usuario = $row->usuario . ", " . $row->apellido;
            $datosUES = trim($usuario) . " / " . trim($empresa) . " / " . trim($sucursal); 

            $datoSucursal = trim($empresa) . " / " . trim($sucursal); 


            $data = array(
                'idUsuario'=>$rowUsuario->id,
                'login'=>$usuario,
                'administrador'=>1,
                'idEmpresa'=>1,
                'idSucursal'=>1,
                'empresa'=>$row->empresa,
                'sucursal'=>$row->sucursal,
                'sucursal_rut'=>$sucursal_rut,
                'sucursal_direccion'=>$sucursal_direccion,
                'sucursal_correo'=>$sucursal_correo,
                'sucursal_telefono'=>$sucursal_telefono,
                'nombre'=>$rowUsuario->nombre,
                'nombreUsuario'=>$nombreApellido,
                'apellido'=>$rowUsuario->apellido,
                'usuario'=>$usuario,
                'idUsuarioEmpresa'=>$row->id,
                'nombreRol'=>$row->rol,
                'idCaja'=>0,
                'logoEmpresa'=>"",
                'imagenEmpresa'=>"",
                'datosUES'=>$datosUES,
                'datoSucursal'=>$datoSucursal,
                'email'=>$rowUsuario->email,
                'foto'=>$rowUsuario->foto,
                'sexo'=>$rowUsuario->sexo,
                'idRol'=>1,
            ); 

            $this->session->set_userdata($data);
            $data = array(
               'status'=>0
            );

            echo json_encode($data);
        }else{
            $data = array(
               'status'=>1
            );
            echo json_encode($data);
        }
    }
*/


    public function generarMenu_admin(){
    	$idUsuario = $this->session->userdata('idUsuario');
    	$idEmpresa = $this->session->userdata('idEmpresa');
    	$idSucursal = $this->session->userdata('idSucursal');
        $idUsuarioEmpresa = $this->session->userdata('idUsuarioEmpresa');
        $administrador = $this->session->userdata('administrador');

		$rowPermiso=$this->Sg_usuarioEmpresaModulo_mdl->obtPermiso($idUsuarioEmpresa);
 	    
 	    /*Permiso sub Modulo*/
		$rowPermisoSM = $this->Sg_usuarioEmpresaModulo_mdl->obtPermisoSM($idUsuarioEmpresa);
		//print_r($rowPermisoSM);


		/* construir arreglo de permiso de los modulos usuario */
		if($rowPermiso){
			  $i = 0;
			  $ind=0;
		      foreach($rowPermiso as $key){
				  //$arregloPermiso[$i][0] = $key->id_modulo;
				  $arregloPermiso[$i][0] = $key->id_modulo;
				  $arregloPermiso[$i][1] = $key->nombre;
				  $arregloPermiso[$i][2] = $key->icono_fas_fa;
				  $identificador = $key->identificador;
				  
				  
                  //obtener los permisos del modulo.
                  $rowAccionesModulo = $this->Sg_usuarioEmpresaModulo_mdl->obtAcciones($key->id, $key->id_modulo);

                  
                  foreach ($rowAccionesModulo as $keyAccion) {
                      //$arregloAccion[$ind][0] = $keyAccion->id_modulo;
                      $arregloAccion[$ind][0] = $identificador;
                      
                      $arregloAccion[$ind][1] = $keyAccion->accion;                   	  
                  	  $ind+=1; 
                  }
	 			  $i+=1;			  
			  }	  
			  $i=$i-1;
			  $ind = $ind - 1;

			  
			  
			  /* construir arreglo de permiso de los sub modulos usuario */			  
			  $j = 0;

		      foreach($rowPermisoSM as $key){
				  $arregloPermisoSM[$j][0] = $key->id_sub_modulo;
				  $arregloPermisoSM[$j][1] = $key->sub_modulo;
				  $arregloPermisoSM[$j][2] = $key->url_sub_modulo;
				  $arregloPermisoSM[$j][3] = $key->id_modulo;
				  $j+=1;			  
			  }	  
			  $j=$j-1;
			  
			  
		  
			  
			  $dataPermiso = array(
				  'arregloPermisoModulo'=>$arregloPermiso,
				  'nroFilaModulo'=>$i,
				  'arregloPermisoSM'=>$arregloPermisoSM,
				  'nroFilaSM'=>$j,
				  'arregloAccion'=>$arregloAccion,
				  'nroFilaAccion'=>$ind,
			  );
			  $this->session->set_userdata($dataPermiso);		  		  
			  
			  
			  $data = array(
				  'accion'=>0,
  				  'logoEmpresa'=>"",
  				  'imagenEmpresa'=>"",
			  );
			  
			  $this->load->view('header');
			  $this->load->view('menu',$data);
			  $this->load->view('plantillaSushi');
			  $this->load->view('footer/footer',$data);
		  }else{
			 /*Remitirlo a una pantalla de que no tiene permiso*/ 
		    $mensaje = "El USuario no posee permiso para el Rol";
			$mensaje.= "<br><br>Comuniquese con el Administrador del Sistema.";
			$data = array('mensaje'=>$mensaje);
			$this->load->view('header');
			$this->load->view('login/permisoError',$data);
			$this->load->view('footer/footer');		  
		  }   
	}



    // usuario admin empresa
    public function accesoEmpresa(){
		$idEmpresa = $this->input->post('id_cbo_empresaAdministrador');
    	$run = $this->input->post('usuario');
    	$password = $this->input->post('pass');
    	$password = md5($password);
    	$valUsuario = $this->Sg_administradorEmpresa_mdl->valLoginEmp($run, $password);

        if($valUsuario > 0){
            if($valUsuario == 1){ 
                $rowinformacionUnicaEmp = $this->Sg_administradorEmpresa_mdl->informacionUnicaEmp($run,$password);
                $idUsuario = $rowinformacionUnicaEmp->id;
                $data = $this->infUsuarioEmpresa($idUsuario,0);
                echo json_encode($data);
            }else{
                $rowUsuarioEmpresa = $this->Sg_administradorEmpresa_mdl->infUsuarioEmpresas($run, $password);

                $dataSession = array(
                    'rowEmpresaUsuario'=>$rowUsuarioEmpresa,
                    //'administrador'=>2
                );

                $this->session->set_userdata($dataSession);
                  
                $data = array(
                   'status'=>2
                );
                echo json_encode($data);

            }  
        }else{
            $data = array(
               'status'=>1
            );
            echo json_encode($data);
        } 
    }

    
    public function selEmpresa(){

        $data = array(
            'empresa'=>$this->session->userdata('rowEmpresaUsuario')
        );  

        $this->load->view('login/header');
        $this->load->view('login/login_selEmpresa',$data);
        $this->load->view('login/footer',$data);
        $this->load->view('footer/footer');                
    }


    public function empresaSeleccionada($idEmpresa, $idUsuario){
        $row = $this->infUsuarioEmpresa($idUsuario, $idEmpresa);
        $data = array(
           'administrador'=>10
        );
        $this->load->view('login/header');
        $this->load->view('login/footer',$data);
        $this->load->view('footer/footer');                
    }


    public function infUsuarioEmpresa($idUsuario, $idEmpresa){
            if($idEmpresa==0){
                $rowUsuario = $this->Sg_administradorEmpresa_mdl->obtInfUsuarioId($idUsuario);
            }else{
                $rowUsuario = $this->Sg_administradorEmpresa_mdl->obtInfUsuarioEmpresa($idUsuario, $idEmpresa); 
            }    


            $nombreApellido = trim($rowUsuario->nombre) . ", " . trim($rowUsuario->apellido);
            $idUsuario = $rowUsuario->id;
            $administrador = 2;
            $idEmpresa = $rowUsuario->id_empresa;
            $idSucursal = $rowUsuario->id_sucursal;
            $row = $this->Sg_usuarioEmpresa_mdl->obtDatosUsuarioEmpresa($idUsuario, $idEmpresa, $idSucursal);


            $nombreRol = $row->rol;             
            $empresa = $row->empresa;
            $sucursal = $row->sucursal;
            $usuario = $row->usuario . ", " . $row->apellido;
            $datosUES = trim($usuario) . " / " . trim($empresa) . " / " . trim($sucursal); 
            $datoSucursal = trim($empresa) . " / " . trim($sucursal); 

            $data = array(
                'idUsuario'=>$rowUsuario->id,
                'login'=>$usuario,
                'administrador'=>$administrador,
                'idEmpresa'=>$rowUsuario->id_empresa,
                'idSucursal'=>$rowUsuario->id_sucursal,
                'empresa'=>$row->empresa,
                'logoEmpresa'=>$row->logo,
                'imagenEmpresa'=>$row->imagen,
                'rutEmpresa'=>$row->rut,
                'direccionEmpresa'=>$row->direccion,
                'sucursal'=>$row->sucursal,
                'sucursal_rut'=>$row->sucursal_rut,
                'sucursal_direccion'=>$row->sucursal_direccion,
                'sucursal_correo'=>$row->sucursal_correo,
                'sucursal_telefono'=>$row->sucursal_telefono,
                
                'nombre'=>$rowUsuario->nombre,
                'apellido'=>$rowUsuario->apellido,
                'nombreUsuario'=>$nombreApellido,
                'usuario'=>$usuario,
                'nombreRol'=>$nombreRol,
                'idUsuarioEmpresa'=>$row->id,
                'idCaja'=>0,
                'datosUES'=>$datosUES,
                'datoSucursal'=>$datoSucursal,
                'email'=>$rowUsuario->email,
                'foto'=>$rowUsuario->foto,
                'sexo'=>$rowUsuario->sexo,
                'idRol'=>2,
            ); 
            
            
            $this->session->set_userdata($data);
            
            $data = array(
               'status'=>0,
               'logoEmpresa'=>$this->session->userdata('logoEmpresa'),
               'imagenEmpresa'=>$this->session->userdata('imagenEmpresa'),
            );
            return $data;
    }


    
    public function generarMenu_adminEmpresa(){

    	$idUsuario = $this->session->userdata('idUsuario');
    	$idEmpresa = $this->session->userdata('idEmpresa');
    	$idSucursal = $this->session->userdata('idSucursal');
        $idUsuarioEmpresa = $this->session->userdata('idUsuarioEmpresa');
        $administrador = $this->session->userdata('administrador');

		$rowPermiso=$this->Sg_usuarioEmpresaModulo_mdl->obtPermiso($idUsuarioEmpresa);
		/* construir arreglo de permiso de los modulos usuario */

 	    /*Permiso sub Modulo*/
		$rowPermisoSM = $this->Sg_usuarioEmpresaModulo_mdl->obtPermisoSM($idUsuarioEmpresa);
		//print_r($rowPermisoSM);		
		
		if($rowPermiso){
			  $i = 0;
			  $ind=0;
		      foreach($rowPermiso as $key){
				  //$arregloPermiso[$i][0] = $key->id_modulo;
				  $arregloPermiso[$i][0] = $key->id_modulo;
				  $arregloPermiso[$i][1] = $key->nombre;
				  $arregloPermiso[$i][2] = $key->icono_fas_fa;
				  $identificador = $key->identificador;
				  
				  
                  //obtener los permisos del modulo.
                  $rowAccionesModulo = $this->Sg_usuarioEmpresaModulo_mdl->obtAcciones($key->id, $key->id_modulo);

                  
                  foreach ($rowAccionesModulo as $keyAccion) {
                      //$arregloAccion[$ind][0] = $keyAccion->id_modulo;
                      $arregloAccion[$ind][0] = $identificador;
                      
                      $arregloAccion[$ind][1] = $keyAccion->accion;                   	  
                  	  $ind+=1; 
                  }
	 			  $i+=1;			  
			  }	  
			  $i=$i-1;
			  $ind = $ind - 1;
			  
			  /* construir arreglo de permiso de los sub modulos usuario */			  
			  $j = 0;

		      foreach($rowPermisoSM as $key){
				  $arregloPermisoSM[$j][0] = $key->id_sub_modulo;
				  $arregloPermisoSM[$j][1] = $key->sub_modulo;
				  $arregloPermisoSM[$j][2] = $key->url_sub_modulo;
				  $arregloPermisoSM[$j][3] = $key->id_modulo;
				  $j+=1;			  
			  }	  
			  $j=$j-1;
			  
			  $dataPermiso = array(
				  'arregloPermisoModulo'=>$arregloPermiso,
				  'nroFilaModulo'=>$i,
				  'arregloPermisoSM'=>$arregloPermisoSM,
				  'nroFilaSM'=>$j,
				  'arregloAccion'=>$arregloAccion,
				  'nroFilaAccion'=>$ind,
			  );
			  $this->session->set_userdata($dataPermiso);		  		  
			  
			  
			  $data = array(
				  'accion'=>0,
				  'imagenEmpresa'=>$this->session->userdata('imagenEmpresa'),
				  'logoEmpresa'=>$this->session->userdata('logoEmpresa'),
			  );
			  $this->load->view('header');
			  $this->load->view('menu',$data);
			  $this->load->view('plantillaSushi');
			  $this->load->view('footer/footer',$data);
			
		}else{
			 /*Remitirlo a una pantalla de que no tiene permiso*/ 
		    $mensaje = "El USuario no posee permiso para el Rol";
			$mensaje.= "<br><br>Comuniquese con el Administrador del Sistema.";
			$data = array('mensaje'=>$mensaje);
			$this->load->view('header');
			$this->load->view('login/permisoError',$data);
			$this->load->view('footer/footer');			
		}	
	}


	public function dashboard(){
    $valSession = $this->clssession->valSession();
    if($valSession==true){
    	$data = array(
		    'accion'=>0,
            'logoEmpresa'=>$this->session->userdata('logoEmpresa'),
            'imagenEmpresa'=>$this->session->userdata('imagenEmpresa'),
		);
  	    $this->load->view('header');
	    $this->load->view('menu',$data);
		$this->load->view('plantillaSushi');
		$this->load->view('footer/footer',$data);

    } // fin val session		
	}
    



    // usuario admin empresa
    public function accesoSucursal(){
    	$run = $this->input->post('usuario');
    	$password = $this->input->post('pass');
    	$password = md5($password);
    	$valUsuario = $this->Sg_administradorSucursal_mdl->valLoginSuc($run, $password);

        if($valUsuario > 0){
            if($valUsuario == 1){ 
                $rowinformacionUnicaSuc = $this->Sg_administradorSucursal_mdl->informacionUnicaSuc($run,$password);
                $idUsuario = $rowinformacionUnicaSuc->id;
                $data = $this->infUsuarioSucursal($idUsuario,0);
                echo json_encode($data);
            }else{
                $rowUsuarioSucursal = $this->Sg_administradorSucursal_mdl->infUsuarioSucursal($run, $password);

                $dataSession = array(
                    'rowSucursalUsuario'=>$rowUsuarioSucursal,
                    //'administrador'=>2
                );

                $this->session->set_userdata($dataSession);
                  
                $data = array(
                   'status'=>2
                );
                echo json_encode($data);

            }  
        }else{
            $data = array(
               'status'=>1
            );
            echo json_encode($data);
        } 
    }


    public function selSucursal(){

        $data = array(
            'sucursal'=>$this->session->userdata('rowSucursalUsuario')
        );  

        $this->load->view('login/header');
        $this->load->view('login/login_selSucursal',$data);
        $this->load->view('login/footer',$data);
        $this->load->view('footer/footer');                
    }    


    public function infUsuarioSucursal($idUsuario, $idSucursal){
            if($idSucursal==0){
                $rowUsuario = $this->Sg_administradorSucursal_mdl->obtInfUsuarioId($idUsuario);
            }else{
                $rowUsuario = $this->Sg_administradorSucursal_mdl->obtInfUsuarioSucursal($idUsuario, $idSucursal); 
            }   


            $nombreApellido = trim($rowUsuario->nombre) . ", " . trim($rowUsuario->apellido);
            $idUsuario = $rowUsuario->id;
            $administrador = 3;
            $idEmpresa = $rowUsuario->id_empresa;
            $idSucursal = $rowUsuario->id_sucursal;
            $row = $this->Sg_usuarioEmpresa_mdl->obtDatosUsuarioEmpresa($idUsuario, $idEmpresa, $idSucursal);

                        
            if($row->id_rol==3){
                $administrador = 3; 
            }else{
                $administrador = 4; 
            }

            $nombreRol = $row->rol;             
            $empresa = $row->empresa;
            $sucursal = $row->sucursal;
            $usuario = $row->usuario . ", " . $row->apellido;
            $datosUES = trim($usuario) . " / " . trim($empresa) . " / " . trim($sucursal); 
            $datoSucursal = trim($empresa) . " / " . trim($sucursal); 

            $data = array(
                'idUsuario'=>$rowUsuario->id,
                'login'=>$usuario,
                'administrador'=>$administrador,
                'idEmpresa'=>$rowUsuario->id_empresa,
                'idSucursal'=>$rowUsuario->id_sucursal,
                'empresa'=>$row->empresa,
                'logoEmpresa'=>$row->logo,
                'imagenEmpresa'=>$row->imagen,
                'rutEmpresa'=>$row->rut,
                'direccionEmpresa'=>$row->direccion,
                'sucursal'=>$row->sucursal,
                'sucursal_rut'=>$row->sucursal_rut,
                'sucursal_direccion'=>$row->sucursal_direccion,
                'sucursal_correo'=>$row->sucursal_correo,
                'sucursal_telefono'=>$row->sucursal_telefono,
                
                'nombre'=>$rowUsuario->nombre,
                'apellido'=>$rowUsuario->apellido,
                'nombreUsuario'=>$nombreApellido,
                'usuario'=>$usuario,
                'nombreRol'=>$nombreRol,
                'idUsuarioEmpresa'=>$row->id,
                'idCaja'=>0,
                'datosUES'=>$datosUES,
                'datoSucursal'=>$datoSucursal,
                'email'=>$rowUsuario->email,
                'foto'=>$rowUsuario->foto,
                'sexo'=>$rowUsuario->sexo,
                'idRol'=>$row->id_rol,
            ); 
            
            
            $this->session->set_userdata($data);
            
            $data = array(
               'status'=>0,
               'logoEmpresa'=>$this->session->userdata('logoEmpresa'),
               'imagenEmpresa'=>$this->session->userdata('imagenEmpresa'),
            );
            return $data;
    }


    public function sucursalSeleccionada($idSucursal, $idUsuario){
        $row = $this->infUsuarioSucursal($idUsuario, $idSucursal);
        $data = array(
           'administrador'=>11
        );
        $this->load->view('login/header');
        $this->load->view('login/footer',$data);
        $this->load->view('footer/footer');                
    }





    public function generarMenu_adminSucursal(){

    	$idUsuario = $this->session->userdata('idUsuario');
    	$idEmpresa = $this->session->userdata('idEmpresa');
    	$idSucursal = $this->session->userdata('idSucursal');
        $idUsuarioEmpresa = $this->session->userdata('idUsuarioEmpresa');
        $administrador = $this->session->userdata('administrador');

    	$rowPermiso=$this->Sg_usuarioEmpresaModulo_mdl->obtPermiso($idUsuarioEmpresa);
		/* construir arreglo de permiso de los modulos usuario */


 	    /*Permiso sub Modulo*/
		$rowPermisoSM = $this->Sg_usuarioEmpresaModulo_mdl->obtPermisoSM($idUsuarioEmpresa);
		//print_r($rowPermisoSM);		
		
		if($rowPermiso){
			  $i = 0;
			  $ind=0;
		      foreach($rowPermiso as $key){
				  //$arregloPermiso[$i][0] = $key->id_modulo;
				  $arregloPermiso[$i][0] = $key->id_modulo;
				  $arregloPermiso[$i][1] = $key->nombre;
				  $arregloPermiso[$i][2] = $key->icono_fas_fa;
				  $identificador = $key->identificador;
				  
				  
                  //obtener los permisos del modulo.
                  $rowAccionesModulo = $this->Sg_usuarioEmpresaModulo_mdl->obtAcciones($key->id, $key->id_modulo);

                  
                  foreach ($rowAccionesModulo as $keyAccion) {
                      //$arregloAccion[$ind][0] = $keyAccion->id_modulo;
                      $arregloAccion[$ind][0] = $identificador;
                      
                      $arregloAccion[$ind][1] = $keyAccion->accion;                   	  
                  	  $ind+=1; 
                  }
	 			  $i+=1;			  
			  }	  
			  $i=$i-1;
			  $ind = $ind - 1;
			  
			  /* construir arreglo de permiso de los sub modulos usuario */			  
			  $j = 0;

		      foreach($rowPermisoSM as $key){
				  $arregloPermisoSM[$j][0] = $key->id_sub_modulo;
				  $arregloPermisoSM[$j][1] = $key->sub_modulo;
				  $arregloPermisoSM[$j][2] = $key->url_sub_modulo;
				  $arregloPermisoSM[$j][3] = $key->id_modulo;
				  $j+=1;			  
			  }	  
			  $j=$j-1;
			  
			  $dataPermiso = array(
				  'arregloPermisoModulo'=>$arregloPermiso,
				  'nroFilaModulo'=>$i,
				  'arregloPermisoSM'=>$arregloPermisoSM,
				  'nroFilaSM'=>$j,
				  'arregloAccion'=>$arregloAccion,
				  'nroFilaAccion'=>$ind,
			  );
			  $this->session->set_userdata($dataPermiso);		  		  
			  
			  
			  $data = array(
				  'accion'=>0,
	              'logoEmpresa'=>$this->session->userdata('logoEmpresa'),
	              'imagenEmpresa'=>$this->session->userdata('imagenEmpresa'),				  
			  );
			  
			  $this->load->view('header');
			  $this->load->view('menu',$data);
			  $this->load->view('plantillaSushi');
			  $this->load->view('footer/footer',$data);
			
		}else{
			 /*Remitirlo a una pantalla de que no tiene permiso*/ 
		    $mensaje = "El USuario no posee permiso para el Rol";
			$mensaje.= "<br><br>Comuniquese con el Administrador del Sistema.";
			$data = array('mensaje'=>$mensaje);
			$this->load->view('header');
			$this->load->view('login/permisoError',$data);
			$this->load->view('footer/footer');
		}	
	}



	
	

	
}
