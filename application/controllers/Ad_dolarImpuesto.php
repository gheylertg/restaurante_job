<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ad_dolarImpuesto extends CI_Controller {

	
	public function __construct() {
         parent::__construct();
         $this->load->model(array('Ad_dolarImpuesto_mdl'));
         $this->load->library(array('Clssession')); 
    }




	public function index()	{
	//Validar session usuario
	$valSession = $this->clssession->valSession();
	if($valSession==true){

		
        //accion 0 vista sin datatable, accion 1  activar datatable 
		$data=array(
			'accion'=>1
		);
		
	    $this->load->view('header');
		$this->load->view('menu');
		$this->load->view('administracion/dolarImpuesto/act_dolarImpuesto',$data);
		$this->load->view('footer/footer', $data);
		$this->load->view('administracion/dolarImpuesto/footer_dolarImpuesto', $data);
		//$this->load->view('footer/lib_numerica');
    }//fin val session		
	}
	
	function ajax_datatable($id){
	//Validar session usuario
	$valSession = $this->clssession->valSession();
	if($valSession==true){
		$row = $this->Ad_dolarImpuesto_mdl->obt_dataTable();
		$html= $this->generarDatatable($row);
		$data = array(
		    "registro"=>$html);
        echo json_encode($data);	
    }//fin val session
	}	
	


function ajax_modificarDolar($id){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){
    $row = $this->Ad_dolarImpuesto_mdl->obtModificar($id);
    
    
    $dolar = str_replace(".", ",", $row->dolar);
    $data = array(
    	'id'=>$row->id,
    	'dolar'=>$dolar,
    );
    echo json_encode($data);
}//fin val session
}	

function ajax_guardar_dolar(){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){
    $status = 0;
	$dolar = $this->input->post('m_dolar');
    $dolar = str_replace(".", "", $dolar);
    $dolar = str_replace(",", ".", $dolar);  	
	$id = $this->input->post('id_mod');

	$data = array(
		'dolar'=>$dolar,
		'id_update_dolar'=>$this->session->userdata('idUsuario'),
		'date_dolar'=>date('Y-m-d H:i:s')        
	);
	$respuesta = $this->Ad_dolarImpuesto_mdl->guardar_mod($id,$data);
    $row = $this->Ad_dolarImpuesto_mdl->obt_dataTable();
	$html= $this->generarDatatable($row);
	$data = array(
	    "registro"=>$html,
	    "status"=>$status);
    echo json_encode($data);
}//fin val session	
}


function ajax_modificarImpuesto($id){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){
    $row = $this->Ad_dolarImpuesto_mdl->obtModificar($id);
    $impuesto = str_replace(".", ",", $row->impuesto);
    $data = array(
    	'id'=>$row->id,
    	'impuesto'=>$impuesto,
    );
    echo json_encode($data);
}//fin val session
}	


	
function ajax_guardar_impuesto(){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){
    $status = 0;
	$impuesto = $this->input->post('m_impuesto');
    $impuesto = str_replace(".", "", $impuesto);
    $impuesto = str_replace(",", ".", $impuesto);  	
	$id = $this->input->post('id_modImp');

	$data = array(
		'impuesto'=>$impuesto,
		'id_update_impuesto'=>$this->session->userdata('idUsuario'),
		'date_impuesto'=>date('Y-m-d H:i:s')        
	);
	$respuesta = $this->Ad_dolarImpuesto_mdl->guardar_mod($id,$data);
    $row = $this->Ad_dolarImpuesto_mdl->obt_dataTable();
	$html= $this->generarDatatable($row);
	$data = array(
	    "registro"=>$html,
	    "status"=>$status);
    echo json_encode($data);
}//fin val session	
}

	function generarDatatable($row){
		//obtener la información de la tabla seleccionada
		$imgDolar = base_url() . "assets/images/dolar.jpeg";
		$imgImpuesto = base_url() . "assets/images/impuesto.png";
		
        $html = '<table id="basic-datatables" class="display table table-striped table-hover">';
        $html.= '    <thead>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="45%">Dolar</th>';
		$html.= '           <th width="45%">Impuesto</th>';        
        $html.= '           <th width="10%">Acción</th>';
        $html.= '        </tr>';
        $html.= '    </thead>';
        $html.= '    <tfoot>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="45%">Dolar</th>';
		$html.= '           <th width="45%">Impuesto</th>';        
        $html.= '           <th width="10%">Acción</th>';
        $html.= '        </tr>';
        $html.= '    </tfoot>';
        $html.= '    <tbody>';
        if($row==false){
			$html.= '      <tr>';
			$html.= '          <td></td><td></td><td></td>';
			$html.= '      </tr>';
		}else{
		    foreach($row as $key){ 
				$html.= '  <tr>';
				
				$dolar = str_replace(".", ",", $key->dolar);
				$html.= '      <td>' . $dolar.'</td>';
				
				$impuesto = str_replace(".", ",", $key->impuesto);
				$html.= '      <td>' . $impuesto .'</td>';
				
				$html.= '      <td>';				
				if($this->clssession->accion(5,3)==1){ 
					$html.= '<a href="javascript:void(0)" onclick="javascript:modificar_dolar('.$key->id.')">';
					$html.= '<img src="'.$imgDolar.'" style="width:30px; height:30px" alt="Modificar Dolar" title="Modificar Dolar"></a>';
				}	
				if($this->clssession->accion(5,3)==1){ 
					$html.= '&nbsp;<a href="javascript:void(0)" onclick="javascript:modificar_impuesto('.$key->id.')">';
					$html.= '<img src="'.$imgImpuesto.'" style="width:30px; height:30px" alt="Modificar Impuesto" title="Modificar Impuesto"></a>';
				}	
				$html.= '       </td>';				
				$html.= '</tr>';
			}
		}	
		$html.= '    </tbody>';
		$html.= '</table>';
		return $html;    				
	}

}
