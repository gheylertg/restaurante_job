<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ad_envoltura extends CI_Controller {

	

	public function __construct() {
         parent::__construct();
         $this->load->model(array('Ad_envoltura_mdl', 'Mn_ingrediente_mdl'));
         $this->load->library(array('Clssession')); 
    }



	public function index(){
	//Validar session usuario
	$valSession = $this->clssession->valSession();
	if($valSession==true){


        $data = array(
        	'accion'=>1
        );

    	$this->load->view('header');
		$this->load->view('menu');
		$this->load->view('administracion/envoltura/act_envoltura',$data);
		$this->load->view('footer/footer', $data);
		$this->load->view('administracion/envoltura/footer_envoltura', $data);
	}//fin val session	
	}


	function ajax_datatable($id){
	//Validar session usuario
	$valSession = $this->clssession->valSession();
	if($valSession==true){

		$row = $this->Ad_envoltura_mdl->obt_dataTable();
		$html= $this->generarDatatable($row);
		$data = array(
		    "registro"=>$html,
		);
        echo json_encode($data);	
	}//fin val session	
    }



function ajax_modificar($id){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){

	//$this->load->model('sg_rolesUsuario_mdl');
    $row = $this->Ad_envoltura_mdl->obtModificar($id);
    $costo = str_replace('.',',',$row->costo);
    $cantidad = str_replace('.',',',$row->cantidad_ingrediente);

    $data = array(
    	'id'=>$row->id,
    	'nombre'=>$row->nombre,
    	'id_ingrediente'=>$row->id_ingrediente,
    	'cantidad'=>$cantidad,
    	//'unidadMedida'=>$sigla,
    	'costo'=>$costo,
    );
    echo json_encode($data);
}//fin val session	
}


function ajax_guardar_add(){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){

    $status = 0;
	$nombre = $this->input->post('m_nombre');
    $status = $this->ajax_validar_duplicidad($nombre);
    if($status==0){
    	//$this->load->model('Sg_rolesUsuario_mdl');
		$idEmpresa = 0;
		$idSucursal = 0;

		if($this->session->userdata('administrador')==2){
			$idEmpresa = $this->session->userdata('idEmpresa');
		}

		if($this->session->userdata('administrador')>2){
			$idEmpresa = $this->session->userdata('idEmpresa');
			$idSucursal = $this->session->userdata('idSucursal');
		}

        if($this->input->post('m_cantidad')==''){
        	$cantidad = 0.00;
        }else{
			$cantidad = str_replace('.','',$this->input->post('m_cantidad'));
			$cantidad = str_replace(',','.',$cantidad);        
        }	
       
        if($this->input->post('m_costo')==''){
        	$costo = 0.00;
        }else{
			$costo = str_replace('.','',$this->input->post('m_costo'));
			$costo = str_replace(',','.',$costo);        
        }	

        //obtener unidad medida ingrediente

        $rowIngrediente = $this->Mn_ingrediente_mdl->obtModificar($this->input->post('id_cbo_ingrediente'));
        
        
        $idUnidadMedida = $rowIngrediente->id_unidad_medida;  



		$data = array(
	        'nombre'=>$nombre,
	        'id_ingrediente'=>$this->input->post('id_cbo_ingrediente'),	        
	        'costo'=>$costo,
	        'cantidad_ingrediente'=>$cantidad,
	        'id_unidad_medida'=>$idUnidadMedida,
			'administrador'=>$this->session->userdata('administrador'),
			'id_empresa'=>$idEmpresa,
			'id_sucursal'=>$idSucursal,	        
	  	    'id_create'=>$this->session->userdata('idUsuario'),
		);
		$this->Ad_envoltura_mdl->guardar_add($data);

	    $row = $this->Ad_envoltura_mdl->obt_dataTable();
		$html= $this->generarDatatable($row);
		$data = array(
		    "registro"=>$html,
		    "status"=>$status);
	}else{
		$data = array(
		    "status"=>1);
	}	

    echo json_encode($data);
}//fin val session	
}




function ajax_guardar_mod(){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){

    $status = 0;
	$nombre = $this->input->post('m_nombre');
	$nombre_orig = $this->input->post('nombre_orig');

    if(strtoupper($nombre) != strtoupper($nombre_orig)){
	    $status = $this->ajax_validar_duplicidad($nombre);
	}



    if($status==0){

		$id = $this->input->post('id_mod');
        if($this->input->post('m_cantidad')==''){
        	$cantidad = 0.00;
        }else{
			$cantidad = str_replace('.','',$this->input->post('m_cantidad'));
			$cantidad = str_replace(',','.',$cantidad);        
        }	
       
        if($this->input->post('m_costo')==''){
        	$costo = 0.00;
        }else{
			$costo = str_replace('.','',$this->input->post('m_costo'));
			$costo = str_replace(',','.',$costo);        
        }	

        $rowIngrediente = $this->Mn_ingrediente_mdl->obtModificar($this->input->post('id_cbo_ingrediente'));
        $idUnidadMedida = $rowIngrediente->id_unidad_medida;  



		$data = array(
	        'nombre'=>$nombre,
	        'id_ingrediente'=>$this->input->post('id_cbo_ingrediente'),
	        'costo'=>$costo,
	        'cantidad_ingrediente'=>$cantidad,
	        'id_unidad_medida'=>$idUnidadMedida,
	  	    'id_update'=>$this->session->userdata('idUsuario'),
	  	    'date_update'=>date('Y-m-d H:i:s')        
		);
		$respuesta = $this->Ad_envoltura_mdl->guardar_mod($id,$data);

	    $row = $this->Ad_envoltura_mdl->obt_dataTable();
		$html= $this->generarDatatable($row);
		$data = array(
		    "registro"=>$html,
		    "status"=>$status);
	}else{
		$data = array(
		    "status"=>1);
	}	

    echo json_encode($data);
}//fin val session
}	
	

function ajax_desactivar($id){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){

	//$this->load->model('Es_subModulo_mdl');
	$data = array(
  	    'activo'=>0,
  	    'id_update'=>$this->session->userdata('idUsuario'),
  	    'date_update'=>date('Y-m-d H:i:s')
    );


	$respuesta = $this->Ad_envoltura_mdl->desactivar($id,$data);
	$row = $this->Ad_envoltura_mdl->obt_dataTable();
	$html= $this->generarDatatable($row);
	$data = array(
	    "registro"=>$html);
    echo json_encode($data);
}//fin val session    
}

function ajax_reactivar($id){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){

	//$this->load->model('Sg_modulo_mdl');
	$data = array(
  	    'activo'=>1,
  	    'id_update'=>$this->session->userdata('idUsuario'),
  	    'date_update'=>date('Y-m-d H:i:s')
    );


	$respuesta = $this->Ad_envoltura_mdl->desactivar($id,$data);
	
	$row = $this->Ad_envoltura_mdl->obt_dataTable();
	$html= $this->generarDatatable($row);
	$data = array(
	    "registro"=>$html);
    echo json_encode($data);

}//fin val session
}

	function generarDatatable($row){
		//obtener la información de la tabla seleccionada
		$modificar = base_url() . "assets/images/modificar02.jpeg";
		$eliminar = base_url() . "assets/images/eliminar.jpeg";
		$reactivar = base_url() . "assets/images/reactivar.jpeg";
		$lectura = base_url() . "assets/images/ver.png";
		
        $html = '<table id="basic-datatables" class="display table table-striped table-hover">';
        $html.= '    <thead>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="35%">Nombre</th>';
        $html.= '           <th width="15%">Ingrediente</th>';
        $html.= '           <th width="10%">Cantidad</th>';
        $html.= '           <th width="15%">Unidad Medida</th>';
        $html.= '           <th width="10%">Costo</th>';
        $html.= '           <th width="15%">Acción</th>';
        $html.= '        </tr>';
        $html.= '    </thead>';
        $html.= '    <tfoot>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="35%">Nombre</th>';
        $html.= '           <th width="15%">Ingrediente</th>';
        $html.= '           <th width="10%">Cantidad</th>';
        $html.= '           <th width="15%">Unidad Medida</th>';
        $html.= '           <th width="10%">Costo</th>';
        $html.= '           <th width="15%">Acción</th>';
        $html.= '        </tr>';
        $html.= '    </tfoot>';
        $html.= '    <tbody>';
        if($row==false){
			$html.= '      <tr>';
			$html.= '          <td></td><td></td><td></td><td></td><td></td><td></td>';
			$html.= '      </tr>';
		}else{
		    foreach($row as $key){ 
				$html.= '  <tr>';
				$html.= '      <td>' . $key->nombre.'</td>';
				$html.= '      <td>' . $key->ingrediente.'</td>';
                $cantidad = number_format ($key->cantidad_ingrediente, 2,',','.');   


				$html.= '      <td style="text-align:right">' . $cantidad.'</td>';
				$costo = number_format ($key->costo, 2,',','.');
				$html.= '      <td>' . $key->unidad_medida.'</td>';
				$html.= '      <td style="text-align:right">' . $costo.'</td>';
				$html.= '      <td>';				

				if($key->activo==1){
                    if($this->clssession->accion(11,3)==1){ 
						$html.= '<a href="javascript:void(0)" onclick="javascript:modificar_form('.$key->id.')">';
						$html.= '<img src="'.$modificar.'" style="width:30px; height:30px" alt="Modificar" title=""Modificar Envoltura></a>';
					}	
					if($this->clssession->accion(11,4)==1){ 
						$html.= '&nbsp;<a href="javascript:void(0)" onclick="javascript:eliminar_form('.$key->id.')">';
						$html.= '<img src="'.$eliminar.'" style="width:30px; height:30px" alt="Desactivar" title="Desactivar Envoltura"></a>';
					}	
					
				}else{
					if($this->clssession->accion(11,5)==1){ 
						$html.= '<a href="javascript:void(0)" onclick="javascript:reactivar_form('.$key->id.')">';
						$html.= '<img src="'.$reactivar.'" style="width:30px; height:30px" alt="Activar" title="Reactivar Envoltura"></a>';
					}	
				}	

				$html.= '      </td>';				
				$html.= '</tr>';
			}
		}	
		$html.= '    </tbody>';
		$html.= '</table>';
		return $html;    				
	}





	function ajax_validar_duplicidad($nombre){
		$nro = $this->Ad_envoltura_mdl->valNombre($nombre);	
		$status = 0;
		if($nro>0){
		    $status = 1;	
		}

		return $status;
	}



	

}
