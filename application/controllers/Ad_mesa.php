<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ad_mesa extends CI_Controller {

	
	public function __construct() {
         parent::__construct();
         $this->load->model(array('Ad_mesa_mdl'));
         $this->load->library(array('Clssession')); 
    }


	public function index()	{
	//Validar session usuario
	$valSession = $this->clssession->valSession();
	if($valSession==true){

		
        //accion 0 vista sin datatable, accion 1  activar datatable 
		$data=array(
			'accion'=>1
		);
		
	    $this->load->view('header');
		$this->load->view('menu');
		$this->load->view('administracion/mesa/actMesa',$data);
		$this->load->view('footer/footer', $data);
		$this->load->view('administracion/mesa/footer_mesa', $data);
		$this->load->view('footer/lib_numerica');
    }//fin val session		
	}
	
	function ajax_datatable($id){
	//Validar session usuario
	$valSession = $this->clssession->valSession();
	if($valSession==true){

		$row = $this->Ad_mesa_mdl->obt_dataTable();
		$html= $this->generarDatatable($row);
		$data = array(
		    "registro"=>$html);
        echo json_encode($data);	

	}//fin val session	
    }
	


function ajax_modificar($id){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){

    $row = $this->Ad_mesa_mdl->obtModificar($id);

    $data = array(
    	'id'=>$row->id,
    	'nombre'=>$row->nombre,
        'nroSilla'=>$row->nro_silla, 
    	'id_sala'=>$row->id_sala,
    );	
    echo json_encode($data);
}//fin val session
}	



function ajax_guardar_add(){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){

    $status = 0;
	$nombre = $this->input->post('m_nombre');
	$idSala = $this->input->post('id_cbo_sala');
    $status = $this->ajax_validar_duplicidad($nombre, $idSala);

    if($status==0){
		$idSala = $this->input->post('id_cbo_sala');
        $nroSilla=$this->input->post('m_nroSilla');
        $idSala = $this->input->post('id_cbo_sala');
        
		$idEmpresa = 0;
		$idSucursal = 0;

		if($this->session->userdata('administrador')==2){
			$idEmpresa = $this->session->userdata('idEmpresa');
		}

		if($this->session->userdata('administrador')>2){
			$idEmpresa = $this->session->userdata('idEmpresa');
			$idSucursal = $this->session->userdata('idSucursal');
		}
        
		$data = array(
	        'nombre'=>$nombre,
	        'id_sala'=>$idSala,
	        'nro_silla'=>$nroSilla,
			'administrador'=>$this->session->userdata('administrador'),
			'id_empresa'=>$idEmpresa,
			'id_sucursal'=>$idSucursal,	        
	  	    'id_create'=>$this->session->userdata('idUsuario'),
		);
		$this->Ad_mesa_mdl->guardar_add_id($data);

	    $row = $this->Ad_mesa_mdl->obt_dataTable();
		$html= $this->generarDatatable($row);
		$data = array(
		    "registro"=>$html,
		    "status"=>$status);
	}else{
		$data = array(
		    "status"=>1);
	}	

    echo json_encode($data);
}//fin val session	
}


function ajax_guardar_mod(){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){

    $status = 0;
	$nombre = $this->input->post('m_nombre');
	$nombre_orig = $this->input->post('nombre_orig');
	$idSala = $this->input->post('id_cbo_sala');

    if(strtoupper($nombre) != strtoupper($nombre_orig)){
	    $status = $this->ajax_validar_duplicidad($nombre,$idSala);
	}

    if($status==0){
		$id = $this->input->post('id_mod');
		$idSala = $this->input->post('id_cbo_sala');
        $nroSilla=$this->input->post('m_nroSilla');

		$data = array(
	        'nombre'=>$nombre,
	        'id_sala'=>$idSala,
	        'nro_silla'=>$nroSilla,
	  	    'id_update'=>$this->session->userdata('idUsuario'),
            'date_update'=>date('Y-m-d H:i:s')          
        );    

		$respuesta = $this->Ad_mesa_mdl->guardar_mod($id,$data);

	    $row = $this->Ad_mesa_mdl->obt_dataTable();
		$html= $this->generarDatatable($row);
		$data = array(
		    "registro"=>$html,
		    "status"=>$status);
	}else{
		$data = array(
		    "status"=>1);
	}	

    echo json_encode($data);
}//fin val session
}	
	

function ajax_desactivar($id){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){

	$data = array(
  	    'activo'=>0,
  	    'id_update'=>$this->session->userdata('idUsuario'),
  	    'date_update'=>date('Y-m-d H:i:s')
    );


	$respuesta = $this->Ad_mesa_mdl->desactivar($id,$data);
	$row = $this->Ad_mesa_mdl->obt_dataTable();
	$html= $this->generarDatatable($row);
	$data = array(
	    "registro"=>$html);
    echo json_encode($data);
}//fin val session    
}


function ajax_reactivar($id){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){

	$data = array(
  	    'activo'=>1,
  	    'id_update'=>$this->session->userdata('idUsuario'),
  	    'date_update'=>date('Y-m-d H:i:s')
    );


	$respuesta = $this->Ad_mesa_mdl->desactivar($id,$data);
	
	$row = $this->Ad_mesa_mdl->obt_dataTable();
	$html= $this->generarDatatable($row);
	$data = array(
	    "registro"=>$html);
    echo json_encode($data);

}//fin val session
}

	
	
	
	
	function generarDatatable($row){
		//obtener la información de la tabla seleccionada
		$modificar = base_url() . "assets/images/modificar02.jpeg";
		$eliminar = base_url() . "assets/images/eliminar.jpeg";
		$reactivar = base_url() . "assets/images/reactivar.jpeg";
		$lectura = base_url() . "assets/images/ver.png";
		
        $html = '<table id="basic-datatables" class="display table table-striped table-hover">';
        $html.= '    <thead>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="35%">Mesa</th>';
        $html.= '           <th width="15%">Nro silla</th>';
		$html.= '           <th width="35%">Sala ubicación</th>';        
        $html.= '           <th width="15%">Acción</th>';
        $html.= '        </tr>';
        $html.= '    </thead>';
        $html.= '    <tfoot>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="35%">Mesa</th>';
        $html.= '           <th width="15%">Nro silla</th>';
		$html.= '           <th width="35%">Sala ubicación</th>';        
        $html.= '           <th width="15%">Acción</th>';
        $html.= '        </tr>';
        $html.= '    </tfoot>';
        $html.= '    <tbody>';
        if($row==false){
			$html.= '      <tr>';
			$html.= '          <td></td><td></td><td></td><td></td>';
			$html.= '      </tr>';
		}else{
		    foreach($row as $key){ 
				$html.= '  <tr>';
				$html.= '      <td>' . $key->nombre.'</td>';
				$html.= '      <td>' . $key->nro_silla.'</td>';
				$html.= '      <td>' . $key->sala.'</td>';				
				$html.= '      <td>';

				if($key->activo==1){

                    if($this->clssession->accion(9,3)==1){ 
						$html.= '<a href="javascript:void(0)" onclick="javascript:modificar_form('.$key->id.')">';
						$html.= '<img src="'.$modificar.'" style="width:30px; height:30px" 
							alt="Modificar" title="Modificar Mesa"></a>';
					}
						
                    if($this->clssession->accion(9,4)==1){ 					
						$html.= '&nbsp;<a href="javascript:void(0)" onclick="javascript:eliminar_form('.$key->id.')">';
						$html.= '<img src="'.$eliminar.'" style="width:30px; height:30px" alt="Desactivar" title="Desactivar Mesa"></a>';
                    }

				}else{
					if($this->clssession->accion(9,5)==1){ 					
						$html.= '<a href="javascript:void(0)" onclick="javascript:reactivar_form('.$key->id.')">';
						$html.= '<img src="'.$reactivar.'" style="width:30px; height:30px" alt="Activar" title="Reactivar Mesa"></a>';
					}	
				}	
				$html.= '       </td>';				
				$html.= '</tr>';
			}
		}	
		$html.= '    </tbody>';
		$html.= '</table>';
		return $html;    				
	}


	function ajax_validar_duplicidad($nombre, $idSala){
		$nro = $this->Ad_mesa_mdl->valNombre($nombre, $idSala);
		$status = 0;
		if($nro>0){
		    $status = 1;	
		}

		return $status;
	}
	

}
