<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ad_sabor extends CI_Controller {


    public function __construct() {
         parent::__construct();
         $this->load->model(array('Ad_sabor_mdl','Ad_ingrediente_mdl'));
         $this->load->library(array('Clssession')); 
    }


    public function index() {
    //Validar session usuario
    $valSession = $this->clssession->valSession();
    if($valSession==true){
        //accion 0 vista sin datatable, accion 1  activar datatable 
        $data=array(
            'accion'=>1,
            'modulo'=>1,
        );

        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('administracion/sabor/act_sabor',$data);
        $this->load->view('footer/footer', $data);
        $this->load->view('administracion/sabor/footer_sabor', $data);
    }//fin val session
    }
    

    function ajax_datatable($id){
    //Validar session usuario
    $valSession = $this->clssession->valSession();
    if($valSession==true){

        $row = $this->Ad_sabor_mdl->obt_dataTable();
        $html= $this->generarDatatable($row);
        $data = array(
            "registro"=>$html);
        echo json_encode($data);    
    }//fin val session
    }   
    
    
    function generarDatatable($row){

        //obtener la información de la tabla seleccionada
        $modificarSabor = base_url() . "assets/images/sabor.jpeg";
        $modificarIngrediente = base_url() . "assets/images/ingrediente.png";
        $eliminar = base_url() . "assets/images/eliminar.jpeg";
        $reactivar = base_url() . "assets/images/reactivar.jpeg";
        $lectura = base_url() . "assets/images/ver.png";
        
        $html = '<table id="basic-datatables" class="display table table-striped table-hover">';
        $html.= '    <thead>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="30%">Sabor</th>';
        $html.= '           <th width="40%">Descripción</th>';   
        $html.= '           <th width="10%">Estado Actual</th>';   
        $html.= '           <th width="20%">Acción</th>';
        $html.= '        </tr>';
        $html.= '    </thead>';
        $html.= '    <tfoot>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="30%">Sabor</th>';
        $html.= '           <th width="40%">Descripción</th>';
        $html.= '           <th width="10%">Estado Actual</th>';
        $html.= '           <th width="20%">Acción</th>';
        $html.= '        </tr>';
        $html.= '    </tfoot>';
        $html.= '    <tbody>';
        if($row==false){
            $html.= '      <tr>';
            $html.= '          <td></td><td></td><td></td><td></td>';    
            $html.= '      </tr>';
        }else{
            $nro=0;
            foreach($row as $key){ 
                $html.= '  <tr>';
                $html.= '      <td>' . $key->nombre.'</td>';
                $html.= '      <td>' . $key->descripcion.'</td>';

                if($key->activo==1){
                   $estado = "ACTIVO"; 
                }else{
                   $estado = "INACTIVO";  
                }   
                $html.= '      <td>' . $estado.'</td>';                

                    $html.= '      <td>';               


                if($this->clssession->accion(11,1)==1){ 

                    $html.='<a href="javascript:void(0)" onclick="javascript:ver_form('.$key->id.')">';
                    $html.= '<img src="'.$lectura.'" style="width:25px; height:25px" alt="Ver" ';
                    $html.= 'title="Ver Sabor"></a>';
                }    

                if($this->clssession->accion(11,3)==1){ 
                    $html.= '&nbsp;<a href="javascript:void(0)" onclick="javascript:modificar_sabor('.$key->id.')">';    
                    
                    $html.= '<img src="'.$modificarSabor.'" style="width:30px; height:30px" alt="Modificar" ';
                    $html.= 'title="Actualizar Sabor"></a>';
                }    

                if($this->clssession->accion(11,3)==1){ 
                    $html.= '&nbsp;<a href="javascript:void(0)" onclick="javascript:mod_ingrediente('.$key->id.')">';
                    $html.= '<img src="'.$modificarIngrediente.'" style="width:30px; height:30px" alt="Modificar" ';
                    $html.= 'title="Actualizar Ingrediente"></a>';
                }    

                $html.= '       </td>';             
                $html.= '</tr>';
            }
        }  

        $html.= '    </tbody>';
        $html.= '</table>';
        return $html;                   
    }


    public function incSabor(){
    //Validar session usuario
    $valSession = $this->clssession->valSession();
    if($valSession==true){


         $array_ingre = array('nroIngrediente','arregloIngrediente');
         $this->session->unset_userdata($array_ingre);  



            $arregloIngrediente[0][0]="";
            $arregloIngrediente[0][1]="";
            $arregloIngrediente[0][2]="";
            $arregloIngrediente[0][3]="";
            $arregloIngrediente[0][4]="";
            $arregloIngrediente[0][5]="";


           
            //arreglo para el detalle entrada
            $dataArreglo = array(
                'nroIngrediente' => 0,
                'arregloIngrediente' => $arregloIngrediente,
                'status'=>0
            );    
            $this->session->set_userdata($dataArreglo);           
            $data=array(
                'accion'=>1,
                'modulo'=>2,
            );
            $this->load->view('header');
            $this->load->view('menu');
            $this->load->view('administracion/sabor/incSabor',$data);
            $this->load->view('footer/footer', $data);
            $this->load->view('administracion/sabor/footer_sabor', $data);
    }//fin val session        
    } 






     public function ajax_datatable_sabor(){

        $nro = $this->session->userdata("nroIngrediente");
        $arreglo = $this->session->userdata("arregloIngrediente");
        $eliminar = base_url() . "assets/images/eliminar.jpeg";
        $modificar = base_url() . "assets/images/modificar.jpeg";

        $html = '<table class="table table-bordered table-head-bg-info ';
        $html.= 'table-bordered-bd-info mt-4">';
        $html.= '<thead>';
        $html.= '<tr>';
        $html.= '    <th scope="col" width="5%">#</th>';
        $html.= '    <th scope="col" width="45%" style="text-align:center">Ingrediente</th>';
        $html.= '    <th scope="col" width="15%">cantidad</th>';
        $html.= '    <th scope="col" width="25%">Unidad Medida</th>';
        $html.= '    <th scope="col" width="10%" style="text-align:center">Acción</th>';
        $html.= '</tr>';
        $html.= '</thead>';

        $html.= '<tbody>';        

        if($nro == 0){
            $html.= '<tr>';
            $html.= '    <td></td>';
            $html.= '    <td></td>';
            $html.= '    <td></td>';
            $html.= '    <td></td>';
            $html.= '    <td></td>';
            $html.= '</tr>';
        } 
        else{
            $i = 1;
            while($i <= $nro){
                if(0==0){
                    $html.= '<tr>';
                    $html.= '    <td>' .$i . '</td>';
                    $html.= '    <td>' .$arreglo[$i][1] . '</td>';
                    $html.= '    <td style="text-align:center">' .$arreglo[$i][2] . '</td>';
                    $html.= '    <td>' .$arreglo[$i][3] . '</td>';

                    $html.= '<td><a href="javascript:void(0)" onclick="javascript:modificar_ingrediente('.$i.')">';
                    $html.= '<img src="'.$modificar.'" style="width:25px; height:25px" alt="Modificar "></a></td';


                    $html.= '<td><a href="javascript:void(0)" onclick="javascript:eliminar_ingrediente('.$i.')">';
                    $html.= '<img src="'.$eliminar.'" style="width:25px; height:25px" alt="Eliminar"></a></td';
                    $html.= '</tr>';
                }    
                $i+=1;
            }   
        }    
        $html.='</tbody>';
        $html.='</table>';
         
        $data = array(
            "registro"=>$html, 
             "status"=>0,
            "nroIngrediente"=>$nro,
        );
        echo json_encode($data);

    }


public function ajax_modificar_ingrediente($indice){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){

    $arreglo = $this->session->userdata("arregloIngrediente");
    $cantidad = $arreglo[$indice][2];
    $data = array(
        'id'=>$indice,
        'cantidad'=>$cantidad,
    );
    echo json_encode($data);
}//fin val session
}



public function ajax_modificar_ingredienteSabor($id){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){

    $arregloE = $this->session->userdata("arregloIngrediente");
    $nroE = $this->session->userdata("nroIngrediente");
    $row = $this->Ad_sabor_mdl->obtModificarIngredienteSabor($id);
    $cantidad = str_replace(".",",",$row->cantidad);
    $data = array(
        'id'=>$id,
        'cantidad'=>$cantidad,
    );
    echo json_encode($data);
}//fin val session
}


public function ajax_guardar_ingredienteSB(){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){

    $idIngrediente = $this->input->post("id_cbo_ingrediente");
    $cantidad = $this->input->post("cantidad");

    $arreglo = $this->session->userdata("arregloIngrediente");
    $nro = $this->session->userdata("nroIngrediente");

    $validar = 0;
    if($nro>0){
        $i=1;
        $swParar = 0;
        while($i <= $nro && $swParar==0){
            if($arreglo[$i][0]==$idIngrediente){
                $swParar=1;
            }
            $i+=1;
        }
        if($swParar==1){
            $validar = 1; 
        }
    }   

    if($validar==0){
        $rowIngrediente = $this->Ad_ingrediente_mdl->obtIngredienteSB($idIngrediente);
        $nro+=1;
        $arreglo[$nro][0] = $idIngrediente;
        $arreglo[$nro][1] = $rowIngrediente->nombre; 
        $arreglo[$nro][2] = $cantidad;        
        $arreglo[$nro][3] = $rowIngrediente->unidad_medida;
        $arreglo[$nro][4] = $rowIngrediente->id_unidad_medida;
        $arreglo[$nro][5] = 0;

       //arreglo para el detalle entrada
        //borrar variables de session
        $dataArreglo = array(
           'nroIngrediente' => $nro,
           'arregloIngrediente' => $arreglo,
           'status' => 0,
        );


        $this->session->set_userdata($dataArreglo);  
        $this->ajax_datatable_sabor();
    }else{
        $data = array(
            "status"=>1
        );
        echo json_encode($data);
    } 
}//fin val session   
}

public function ajax_guardar_ingrediente_mod(){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){

    $cantidad = $this->input->post("x_cantidad");
    $indice = $this->input->post("id_mod_x");

    $arreglo = $this->session->userdata("arregloIngrediente");
    $nro = $this->session->userdata("nroIngrediente");

    $arreglo[$indice][2] = $cantidad;
        //arreglo para el detalle entrada
        $dataArreglo = array(
           'nroIngrediente' => $nro,
           'arregloIngrediente' => $arreglo,
           'status' => 0,
        );    
        $this->session->set_userdata($dataArreglo);  
        $this->ajax_datatable_sabor();
}//fin val session
}

public function ajax_eliminar_ingrediente($id){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){

  
    $arregloE = $this->session->userdata("arregloIngrediente");
    $nroE = $this->session->userdata("nroIngrediente");

    $arregloIngredientes[0][0]="";  // id_ingrediente
    $arregloIngredientes[0][1]="";  // ingrediente
    $arregloIngredientes[0][2]="";  // cantidad
    $arregloIngredientes[0][3]="";  // unidad medida
    $arregloIngredientes[0][4]="";  // id_unidad medida
    $arregloIngredientes[0][5] = 0;

    if($nroE==0 || $nroE==1){
       $nroE=0;
    }

    if($nroE>1){
        $i=1;  $indice=1;

        while($i <= $nroE){
            if($i != $id){
                $arregloIngredientes[$indice][0]=$arregloE[$i][0];
                $arregloIngredientes[$indice][1]=$arregloE[$i][1];
                $arregloIngredientes[$indice][2]=$arregloE[$i][2];
                $arregloIngredientes[$indice][3]=$arregloE[$i][3];
                $arregloIngredientes[$indice][4]=$arregloE[$i][4];
                $arregloIngredientes[$indice][5]=0;
                $indice+=1;
            }
            $i+=1;
        }  
        $nroE = $nroE - 1;
    }    

    $dataArreglo = array(
        'nroIngrediente' => $nroE,
        'arregloIngrediente' => $arregloIngredientes
    );  
    $this->session->set_userdata($dataArreglo);     
    $this->ajax_datatable_sabor();   

}//fin val session      
}


public function ajax_guardar_ingredienteSabor_mod($idSabor){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){

    $cantidad = $this->input->post("m_cantidad");
    $id = $this->input->post("id_mod_mo");

    $cantidad = str_replace('.','',$cantidad);
    $cantidad = str_replace(',','.',$cantidad);

    $data=array(
        'cantidad'=>$cantidad,
        'id_update'=>$this->session->userdata("idUsuario"),
        'date_update'=>date('Y-m-d H:i:s')  
    );

    $this->Ad_sabor_mdl->guardar_mod_ingredienteSabor($id, $data);

    $rowTabla = $this->Ad_sabor_mdl->obt_dataTableIngrediente($idSabor);
    $html= $this->generarDatatableIngrediente($rowTabla);
    $data = array(
        "registro"=>$html,
        "status"=>0
    );    
    echo json_encode($data); 

}//fin val session
}





public function ajax_guardar_saborSB(){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){
    $data=array(
        'nombre'=>$this->input->post("m_nombre"),
        'descripcion'=>$this->input->post("m_descripcion"), 
        'id_empresa'=>$this->session->userdata("idEmpresa"),
        'id_sucursal'=>$this->session->userdata("idSucursal"),
        'id_create'=>$this->session->userdata("idUsuario")
    );

    $idSabor = $this->Ad_sabor_mdl->guardar_saborSB($data);
    
    
    //Guardar Ingredientes

    $arreglo = $this->session->userdata('arregloIngrediente');
    $nro = $this->session->userdata('nroIngrediente');    

    $idEmpresa = $this->session->userdata("idEmpresa");
    $idSucursal = $this->session->userdata("idSucursal");

    $i=1;
    while($i <= $nro){
        $idIngrediente = $arreglo[$i][0];
        $cantidad = $arreglo[$i][2];
        $cantidad = str_replace('.','',$cantidad);
        $cantidad = str_replace(',','.',$cantidad);

        $data = array(
            'id_sabor'=> $idSabor,
            'id_ingrediente'=>$idIngrediente,
            'cantidad'=>$cantidad,
            'id_unidad_medida'=>$arreglo[$i][4],
            'id_create'=>$this->session->userdata("idUsuario"),
        );
        $this->Ad_sabor_mdl->guardar_detalleIngrediente($data);


        $i+=1;  //incrementar indice del arreglo
    }
    $data = array(
        'status'=>0
    );
    echo json_encode($data);
}//fin val session      
}

function ajax_guardar_mod_sabor (){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){
    $status = 0;
    $id_sabor =  $this->input->post('id_mod_mo');
    $nombre = $this->input->post("mo_nombre");
    $descripcion = $this->input->post("mo_descripcion");
    $nombreOrig = $this->input->post("nombreOrig");

    $status = 0;
    if($nombre != $nombreOrig){
        $status = $this->ajax_validar_duplicidad($nombre);
    }

    if($status==0){
        $data=array(
            'nombre'=>$nombre,
            'descripcion'=>$descripcion,
            'id_update'=>$this->session->userdata("idUsuario"),
            'date_update'=>date('Y-m-d H:i:s')  
       );

        $this->Ad_sabor_mdl->guardar_mod_sabor($id_sabor, $data);

        $row = $this->Ad_sabor_mdl->obt_dataTable();
        $html= $this->generarDatatable($row);
        $data = array(
            "registro"=>$html,
            "status"=>0
        );        
        echo json_encode($data);    
     }else{
        $data = array(
            "status"=>1
        );        
        echo json_encode($data);    
     }   

}//fin val session   
}



    function ajax_validar_duplicidad($nombre){
        $nro = $this->Ad_sabor_mdl->valNombre($nombre);
        $status = 0;
        if($nro>0){
            $status = 1;    
        }

        return $status;
    }



    public function verSabor($id){
        
        $rowSabor = $this->Ad_sabor_mdl->verSabor($id);
        
        $arregloSabor[0] = $rowSabor->nombre;
        $arregloSabor[1] = $rowSabor->descripcion;

        $rowSaborIngrediente = $this->Ad_ingrediente_mdl->obtSaborIngrediente($rowSabor->id);



        $i = 0;
        foreach($rowSaborIngrediente as $key){
            $arregloIngrediente[$i][0] = $key->id;
            $arregloIngrediente[$i][1] = $key->ingrediente; 
            $arregloIngrediente[$i][2] = $key->unidad_medida;
            $arregloIngrediente[$i][3] = $key->cantidad;       
            $i+=1;
        }   

       
        //arreglo para el detalle entrada
        $dataArreglo = array(
            'arregloSabor'=>$arregloSabor,
            'nroIngrediente' => $i,
            'arregloIngrediente' => $arregloIngrediente,
            'status'=>0
        );    
        $this->session->set_userdata($dataArreglo);           
        $data=array(
            'accion'=>1,
            'modulo'=>4,
            'idSabor'=>$id
        );
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('administracion/sabor/verSabor',$data);
        $this->load->view('footer/footer', $data);
        $this->load->view('administracion/sabor/footer_sabor', $data);
    }  


    public function ajax_datatable_verSabor(){
        $nro = $this->session->userdata("nroIngrediente");
        $arreglo = $this->session->userdata("arregloIngrediente");

        $arregloSabor = $this->session->userdata("arregloSabor");
        
        $eliminar = base_url() . "assets/images/eliminar.jpeg";

        $html = '<table class="table table-bordered table-head-bg-info ';
        $html.= 'table-bordered-bd-info mt-4">';
        $html.= '<thead>';
        $html.= '<tr>';
        $html.= '    <th scope="col" width="5%">#</th>';
        $html.= '    <th scope="col" width="45%">Ingrediente</th>';
        $html.= '    <th scope="col" width="10%" style="text-align:center">Cantidad</th>';        
        $html.= '    <th scope="col" width="20%">Unidad Medida</th>';
        $html.= '</tr>';
        $html.= '</thead>';

        $html.= '<tbody>';        

            $i = 0;
            while($i < $nro){
                $html.= '<tr>';
                $num = $i + 1;
                $html.= '    <td>' .$num. '</td>';
                $html.= '    <td style="text-align:left">' .$arreglo[$i][1] . '</td>';
                $html.= '    <td style="text-align:right">' .$arreglo[$i][3] . '</td>';
                $html.= '    <td>' .$arreglo[$i][2] . '</td>';
                $html.= '</tr>';
                $i+=1;
            }   

        $html.='</tbody>';
        $html.='</table>';
        
        $nombre = $arregloSabor[0];
        $descripcion = $arregloSabor[1];

        $data = array(
            "registro"=>$html,
            "status"=>0,
            "nroIngrediente"=>$nro,
            "nombre"=>$nombre,
            "descripcion"=>$descripcion,
        );
         echo json_encode($data);        
    }








    
    
        function generarDatatableIngrediente($row){
        //La variable rol trae los diferentes roles sin estar asociados a los modulos. 
        //obtener la información de la tabla seleccionada
        $modificar = base_url() . "assets/images/modificar02.jpeg";
        $eliminar = base_url() . "assets/images/eliminar.jpeg";
        $reactivar = base_url() . "assets/images/reactivar.jpeg";
        $lectura = base_url() . "assets/images/ver.png";

        $html = '<table id="basic-datatables" class="display table table-striped table-hover">';
        $html.= '    <thead>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="45%">Nombre Sabor</th>';
        $html.= '           <th width="15%">Cantidad</th>';        
        $html.= '           <th width="25%">Unidad Medida</th>';        
        $html.= '           <th width="15%">Acción</th>';
        $html.= '        </tr>';
        $html.= '    </thead>';
        $html.= '    <tfoot>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="45%">Nombre Sabor</th>';
        $html.= '           <th width="15%">Cantidad</th>';        
        $html.= '           <th width="25%">Unidad Medida</th>';        
        $html.= '           <th width="15%">Acción</th>';
        $html.= '        </tr>';
        $html.= '    </tfoot>';
        $html.= '    <tbody>';
        if($row==false){
            $html.= '      <tr>';
            $html.= '         <td></td>
            <td></td>
            <td></td>
            <td></td>

           ';
								
            $html.= '      </tr>';
        }else{
            foreach($row as $key){ 
                $html.= '  <tr>';
                $html.= '      <td>' . $key->ingrediente.'</td>';
                $cantidad = number_format ($key->cantidad, 2,',','.'); 

                $html.= '      <td style="text-align:right">' . $cantidad.'</td>';                
                $html.= '      <td>' . $key->unidad_medida.'</td>';                
                $html.= '      <td>';               
                //$html.= '<a href="javascript:void(0)" onclick="javascript:modificar_form('.$key->id.')">';


                if($this->clssession->accion(11,3)==1){ 
                $html.='&nbsp;<a href="javascript:void(0)" onclick="javascript:mod_ingredienteSabor('.$key->id.')">';$html.= '<img src="'.$modificar.'" style="width:30px; height:30px" alt="Modificar" ';
                    $html.= 'title="Modificar Ingrediente"></a>';
                }    

                if($this->clssession->accion(11,4)==1){ 
                  $html.='&nbsp;<a href="javascript:void(0)" onclick="javascript:elim_ingrediente('.$key->id.')">';
                  $html.= '<img src="'.$eliminar.'" style="width:25px; height:25px" alt="Eliminar" ';
                  $html.= 'title="Eliminar Ingrediente"></a>';'></a>';
                }    

                $html.= '       </td>';             
                $html.= '</tr>';
            }
        }  

        $html.= '    </tbody>';
        $html.= '</table>';
        return $html;                   
    }





 





public function ajax_act_modificar_sabor($id){
    $row = $this->Ad_sabor_mdl->obtModificarSabor($id);

    $data = array(
        'id'=>$row->id,
        'nombre'=>$row->nombre,
        'descripcion'=>$row->descripcion,
    );
   
    echo json_encode($data);    
}





public function ajax_guardar_add_ingredienteSabor($idSabor){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){

    $idIngrediente = $this->input->post("id_cbo_ingrediente");
    $cantidad = $this->input->post("cantidad");
    $status = 0;
    $status = $this->Ad_sabor_mdl->valIngrediente($idSabor, $idIngrediente);


    if($status==0){
		$cantidad = str_replace('.','',$cantidad);
		$cantidad = str_replace(',','.',$cantidad);    

        $rowIngrediente = $this->Ad_ingrediente_mdl->obtIngredienteSB($idIngrediente);

    
		$data = array(
			  'id_sabor'=>$idSabor, 
			  'id_ingrediente'=>$idIngrediente,
			  'cantidad'=>$cantidad, 
              'id_unidad_medida'=>$rowIngrediente->id_unidad_medida,
			  'id_create'=>$this->session->userdata('idUsuario'),
		);

		$resultado = $this->Ad_sabor_mdl->guardar_ingrediente_add($data);

        $rowTabla = $this->Ad_sabor_mdl->obt_dataTableIngrediente($idSabor);
        $html= $this->generarDatatableIngrediente($rowTabla);
        $data = array(
            "registro"=>$html,
            "status"=>0
        );    
        echo json_encode($data);         

	}else{
		$data = array(
			"status"=>$status
		);
        echo json_encode($data);
	}	


}//fin val session    
}



public function act_modificar_ingrediente($id){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){

   //accion 0 vista sin datatable, accion 1  activar datatable 
    $data=array(
        'accion'=>1,
        'modulo'=>3,
        'idSabor'=>$id,
    );
    
    $this->load->view('header');
    $this->load->view('menu');
    $this->load->view('administracion/sabor/act_ingredienteSabor',$data);
    $this->load->view('footer/footer', $data);
    $this->load->view('administracion/sabor/footer_sabor', $data);
}//fin val session    
}

    

    function ajax_datatableSabor($id){
    //Validar session usuario
    $valSession = $this->clssession->valSession();
    if($valSession==true){

        //$this->load->model('sg_rolesUsuario_mdl');
        $row = $this->Ad_sabor_mdl->obt_dataTableIngrediente($id);
        $html= $this->generarDatatableIngrediente($row);
        $data = array(
            "registro"=>$html);
        echo json_encode($data);    
    }//fin val session    

    }   








public function ajax_borrar_ingrediente(){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){

	$idDetalle = $_GET['id'];
	$idSabor = $_GET['idSabor'];
    $status=0;
    
  
		$idEmpresa = $this->session->userdata('idEmpresa');
		$idSucursal = $this->session->userdata('idSucursal');

		$resultado = $this->Ad_sabor_mdl->eliminarIngrediente($idDetalle);		 
		$rowTabla = $this->Ad_sabor_mdl->obt_dataTableIngrediente($idSabor);
		$html= $this->generarDatatableIngrediente($rowTabla);
		$data = array(
		    "registro"=>$html,
			"status"=>0
		);    
		echo json_encode($data); 
}//fin val session
}


}  //fin de la clase
