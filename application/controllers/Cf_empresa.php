<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cf_empresa extends CI_Controller {

	
	public function __construct() {
         parent::__construct();
         $this->load->model(array('Cf_empresa_mdl', 'Cf_sucursal_mdl'));
         $this->load->library(array('Clssession')); 
    }



	public function index($opcion=0){
	//Validar session usuario
	$valSession = $this->clssession->valSession();
	if($valSession==true){
        //accion 0 vista sin datatable, accion 1  activar datatable 
		$data=array(
			'accion'=>1,
			'opcion'=>$opcion
		);

		    $this->load->view('header');
			$this->load->view('menu');
			$this->load->view('configuracion/empresa/act_empresa',$data);
			$this->load->view('footer/footer', $data);
			$this->load->view('configuracion/empresa/footer_empresa', $data);

		//$this->load->view('footer/lib_numerica');
    }//fin val session		
	}
	
	function ajax_datatable(){
	//Validar session usuario
	$valSession = $this->clssession->valSession();
	if($valSession==true){

		//$this->load->model('sg_rolesUsuario_mdl');
		$row = $this->Cf_empresa_mdl->obt_dataTable();
		$html= $this->generarDatatable($row);
		$data = array(
		    "registro"=>$html);
        echo json_encode($data);	
	}//fin val session	
	}
	


function ajax_modificar($id){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){
	//$this->load->model('sg_rolesUsuario_mdl');
    $row = $this->Cf_empresa_mdl->obtModificar($id);

    $data = array(
    	'id'=>$row->id,
    	'nombre'=>$row->nombre,
    	'rut'=>$row->rut,
    	'correo'=>$row->correo,
    	'contacto'=>$row->contacto,
    	'direccion'=>$row->direccion,
    	'telefonoLocal'=>$row->telefono_local,
    	'telefonoCelular'=>$row->telefono_celular,
    );
    echo json_encode($data);
}//fin val session	
}


function ajax_ver($id){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){
    $row = $this->Cf_empresa_mdl->obtModificar($id);

    if($row->imagen==null){
  		$imagen = "assets/images/sinImages.jpeg";
    }else{
        $imagen = $row->imagen;
        $imagen = base_url()."assets/images/img_empresa/".$imagen;
    }
    $html = '<img src="'.$imagen.'" style="width:230px; height:230x" alt="Imagen">';


    if($row->logo==null){
  		$logo = "assets/images/sinImages.jpeg";
    }else{
        $logo = $row->logo;
        $logo = base_url()."assets/images/img_empresa/".$logo;
    }
    $htmlLogo = '<img src="'.$logo.'" style="width:150px; height:150x" alt="Logo">';




    $data = array(
    	'id'=>$row->id,
    	'nombre'=>$row->nombre,
    	'rut'=>$row->rut,
    	'correo'=>$row->correo,
    	'contacto'=>$row->contacto,
    	'direccion'=>$row->direccion,
    	'telefonoLocal'=>$row->telefono_local,
    	'telefonoCelular'=>$row->telefono_celular,
    	'imagen'=>$html,
    	'logo'=>$htmlLogo,
    );
    echo json_encode($data);
}//fin val session	
}



function ajax_guardar_add(){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){
    $status = 0;
    $imagen = $_FILES['m_imagen']['name'];
    //$logo = $_FILES['m_logo']['name'];


    $nombreImagen = "NO";
    $nombreLogo = "NO";

    $opcion=0;

/*    if($opcion==0){
	    if(!empty($logo)){
		    $nombreLogo = $this->session->userdata('idEmpresa') ."-".date('Y-m-d-s')."-logo-" .$logo;
		    $configL['upload_path']          = './assets/images/img_empresa/'; // Carpeta a subr
		    $configL['allowed_types']        = 'gif|jpg|png|jpeg'; // Tipos permitidos
		    $configL['max_size']             = 2000; // Max 2Mb
		    $configL['max_width']            = 0; // Sin límite de width
		    $configL['max_height']           = 0; // Sin límite de height
		    $configL['file_name'] = $nombreLogo; // Nombre de la imagen
		    $this->load->library('upload', $configL);
		    if(!$this->upload->do_upload('m_logo')) { 
		   		$opcion=1;
				$data=array(
					'accion'=>$opcion,
					'opcion'=>$opcion
				);
			    $this->load->view('header');
				$this->load->view('footer/footer', $data);			
			    $this->load->view('configuracion/empresa/footer_empresa', $data);				
		    }
		}
	}	

*/
    if(!empty($imagen)){
	    $nombreImagen = $this->session->userdata('idEmpresa') . "-" . date('Y-m-d-s')."-imagen-" .$imagen;
	    $config['upload_path']          = './assets/images/img_empresa/'; // Carpeta a subr
	    $config['allowed_types']        = 'gif|jpg|png|jpeg'; // Tipos permitidos
	    $config['max_size']             = 2000; // Max 2Mb
	    $config['max_width']            = 0; // Sin límite de width
	    $config['max_height']           = 0; // Sin límite de height
	    $config['file_name'] = $nombreImagen; // Nombre de la imagen
	    $this->load->library('upload', $config);
	    if(!$this->upload->do_upload('m_imagen')) { 
	   		$opcion=1;
			$data=array(
				'accion'=>$opcion,
				'opcion'=>$opcion
			);
		    $this->load->view('header');
			$this->load->view('footer/footer', $data);			
		    $this->load->view('configuracion/empresa/footer_empresa', $data);				
	    }
	}


	if($opcion==0){
		$data = array(
		    'nombre'=>$this->input->post('m_nombre'),
		    'rut'=>$this->input->post('m_rut'),
		    'correo'=>$this->input->post('m_correo'),
		    'direccion'=>$this->input->post('m_direccion'),
		    'telefono_local'=>$this->input->post('m_telefono_local'),
		    'telefono_celular'=>$this->input->post('m_telefono_celular'),
		    'contacto'=>$this->input->post('m_contacto'),
		    'id_create'=>$this->session->userdata('idUsuario'),
            'administrador'=>$this->session->userdata("administrador"),
            'id_empresa'=>$this->session->userdata("idEmpresa"),
            'id_sucursal'=>$this->session->userdata("idSucursal"),
		    'imagen'=>$nombreImagen,
		    //'logo'=>$nombreLogo,	

		);

		$idEmpresa = $this->Cf_empresa_mdl->guardar_add($data);
        //crear sucursal principal
        $nombreSucursal = "SUCURSAL PRINCIPAL " . $this->input->post('m_nombre'); 
        $dataSuc = array(
        	'nombre'=>$nombreSucursal,
        	'rut'=>0,
        	'id_region'=>1,
        	'id_empresa'=>$idEmpresa,
        	'administrador'=>1,
        	'no_modificar'=>1,
        	'id_sucursal'=>0,
        	'sw_sucursal'=>0,
        	'id_create'=>$this->session->userdata('idUsuario'),
        );
        $resultado = $this->Cf_sucursal_mdl->guardar_add($dataSuc);
		$opcion=3;
			$data=array(
				'accion'=>$opcion,
				'opcion'=>$opcion
			);

		    $this->load->view('header');
			$this->load->view('footer/footer', $data);			
		    $this->load->view('configuracion/empresa/footer_empresa', $data);	
	}	
}//fin val session	
}

function ajax_guardar_mod(){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){

    $status = 0;
	$nombre = $this->input->post('m_nombre');
	$nombre_orig = $this->input->post('nombre_orig');

    if(strtoupper($nombre) != strtoupper($nombre_orig)){
	    $status = $this->ajax_validar_duplicidad_modificar($nombre);
	}

    if($status==0){
    	//$this->load->model('Es_subModulo_mdl');
		$id = $this->input->post('id_mod');

		$data = array(
	        'nombre'=>$nombre,
		    'rut'=>$this->input->post('m_rut'),
		    'correo'=>$this->input->post('m_correo'),
		    'direccion'=>$this->input->post('m_direccion'),
		    'telefono_local'=>$this->input->post('m_telefono_local'),
		    'telefono_celular'=>$this->input->post('m_telefono_celular'),
		    'contacto'=>$this->input->post('m_contacto'),
	  	    'id_update'=>$this->session->userdata('idUsuario'),
	  	    'date_update'=>date('Y-m-d H:i:s')        
		);
		$respuesta = $this->Cf_empresa_mdl->guardar_mod($id,$data);

	    $row = $this->Cf_empresa_mdl->obt_dataTable();
		$html= $this->generarDatatable($row);
		$data = array(
		    "registro"=>$html,
		    "status"=>$status);
	}else{
		$data = array(
		    "status"=>1);
	}	

    echo json_encode($data);
}//fin val session	
}
	
	
function ajax_guardar_modImagen(){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){	
    $status = 0;
    $imagen = $_FILES['mod_imagen']['name'];
    $id = $this->input->post('id_mod_imagen');
    $opcion=0;
  
	$nombreImagen = $this->session->userdata('idEmpresa') . "-mod-imagen-" .$imagen;    
    $config['upload_path']          = './assets/images/img_empresa/'; // Carpeta a subr
    $config['allowed_types']        = 'gif|jpg|png|jpeg'; // Tipos permitidos
    $config['max_size']             = 5000; // Max 2Mb
    $config['max_width']            = 0; // Sin límite de width
    $config['max_height']           = 0; // Sin límite de height
    $config['file_name'] = $nombreImagen; // Nombre de la imagen
    $this->load->library('upload', $config);
    if(!$this->upload->do_upload('mod_imagen')) {    
   		$opcion=2;
		$this->index($opcion);
    }
	$data = array(
		'imagen'=>$nombreImagen,
  	    'id_update'=>$this->session->userdata('idUsuario'),
  	    'date_update'=>date('Y-m-d H:i:s')        
	);

	$respuesta = $this->Cf_empresa_mdl->guardar_mod($id,$data);

	$opcion=3;
		$data=array(
			'accion'=>$opcion,
			'opcion'=>$opcion
		);

	    $this->load->view('header');
		$this->load->view('footer/footer', $data);			
	    $this->load->view('configuracion/empresa/footer_empresa', $data);	
		//$this->index($opcion);

}//fin val session	
}	
	

function ajax_guardar_modLogo(){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){	
    $status = 0;
    $logo = $_FILES['mod_logo']['name'];
    $id = $this->input->post('id_mod_logo');
    $opcion=0;
  
	$nombreLogo = $this->session->userdata('idEmpresa') . "-mod-logo-" .$logo;    
    $config['upload_path']          = './assets/images/img_empresa/'; // Carpeta a subr
    $config['allowed_types']        = 'gif|jpg|png|jpeg'; // Tipos permitidos
    $config['max_size']             = 5000; // Max 2Mb
    $config['max_width']            = 0; // Sin límite de width
    $config['max_height']           = 0; // Sin límite de height
    $config['file_name'] = $nombreLogo; // Nombre de la imagen
    $this->load->library('upload', $config);
    if(!$this->upload->do_upload('mod_logo')) {    
   		$opcion=2;
		$this->index($opcion);
    }
	$data = array(
		'logo'=>$nombreLogo,
  	    'id_update'=>$this->session->userdata('idUsuario'),
  	    'date_update'=>date('Y-m-d H:i:s')        
	);

	$respuesta = $this->Cf_empresa_mdl->guardar_mod($id,$data);
	$opcion=3;
		$data=array(
			'accion'=>$opcion,
			'opcion'=>$opcion
		);

	    $this->load->view('header');
		$this->load->view('footer/footer', $data);			
	    $this->load->view('configuracion/empresa/footer_empresa', $data);	

}//fin val session	
}	


	

function ajax_desactivar($id){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){
	//$this->load->model('Es_subModulo_mdl');
	$data = array(
  	    'activo'=>0,
  	    'id_update'=>$this->session->userdata('idUsuario'),
  	    'date_update'=>date('Y-m-d H:i:s')
    );


	$respuesta = $this->Cf_empresa_mdl->desactivar($id,$data);
	$row = $this->Cf_empresa_mdl->obt_dataTable();
	$html= $this->generarDatatable($row);
	$data = array(
	    "registro"=>$html);
    echo json_encode($data);
}//fin val session    
}


function ajax_reactivar($id){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){
	$data = array(
  	    'activo'=>1,
  	    'id_update'=>$this->session->userdata('idUsuario'),
  	    'date_update'=>date('Y-m-d H:i:s')
    );


	$respuesta = $this->Cf_empresa_mdl->desactivar($id,$data);
	
	$row = $this->Cf_empresa_mdl->obt_dataTable();
	$html= $this->generarDatatable($row);
	$data = array(
	    "registro"=>$html);
    echo json_encode($data);

}//fin val session
}

	
	
	
	
	function generarDatatable($row){
		//obtener la información de la tabla seleccionada
		$modificar = base_url() . "assets/images/modificar02.jpeg";
		$eliminar = base_url() . "assets/images/eliminar.jpeg";
		$reactivar = base_url() . "assets/images/reactivar.jpeg";
		$lectura = base_url() . "assets/images/ver.png";
		$modificarImagen = base_url() . "assets/images/imagen.png";
		$modificarLogo = base_url() . "assets/images/logoL.jpeg";

		

		
        $html = '<table id="basic-datatables" class="display table table-striped table-hover">';
        $html.= '    <thead>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="31%">Nombre</th>';
        $html.= '           <th width="10%">RUT</th>';
        $html.= '           <th width="17%">Contacto</th>';
        $html.= '           <th width="12%">Teléfono Local</th>';
        $html.= '           <th width="8%">Logo</th>';
        $html.= '           <th width="8%">Imagen</th>';
        $html.= '           <th width="14%">Acción</th>';
        $html.= '        </tr>';
        $html.= '    </thead>';
        $html.= '    <tfoot>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="31%">Nombre</th>';
        $html.= '           <th width="10%">RUT</th>';
        $html.= '           <th width="17%">Contacto</th>';
        $html.= '           <th width="12%">Teléfono Local</th>';
        $html.= '           <th width="8%">Logo</th>';
        $html.= '           <th width="8%">Imagen</th>';
        $html.= '           <th width="14%">Acción</th>';
        $html.= '        </tr>';
        $html.= '    </tfoot>';
        $html.= '    <tbody>';
        if($row==false){
			$html.= '      <tr>';
			$html.= '          <td></td><td></td><td></td><td></td><td></td><td></td><td></td>';
			$html.= '      </tr>';
		}else{
		    foreach($row as $key){ 
				$html.= '  <tr>';
				$html.= '      <td>' . $key->nombre.'</td>';
				$html.= '      <td>' . $key->rut.'</td>';
				$html.= '      <td>' . $key->contacto.'</td>';
				$html.= '      <td>' . $key->telefono_local.'</td>';



                //mostrar logo.



                if($key->logo==null || empty($key->logo)){
                	$logo = base_url() . "assets/images/sinImages.jpeg";
                }else{
                    $logo = base_url() . "assets/images/img_empresa/" . $key->logo;

                }


				
	
				$html.= '<td style="text-align="center">';
				$html.= '<a href="javascript:void(0)" onclick="javascript:modificarLogo_form('.$key->id.')">';
				$html.= '<img src="'.$logo.'" style="width:40px; height:40px" alt="Logo" title="Modificar Logo"></a>';
				$html.= '</td>';


                //mostrar fotos.
                if($key->imagen==null || empty($key->imagen)){
                	$imagen = base_url() . "assets/images/sinImages.jpeg";
                }else{
                    $imagen = base_url() . "assets/images/img_empresa/" . $key->imagen;

                }
				
	
				$html.= '<td style="text-align="center">';
				$html.= '<a href="javascript:void(0)" onclick="javascript:modificarImagen_form('.$key->id.')">';
				$html.= '<img src="'.$imagen.'" style="width:40px; height:40px" alt="Imagen" title="Modificar Imagen"></a>';
				$html.= '</td>';

				$html.= '      <td>';				
				if($key->activo==1){
                    if($this->clssession->accion(1,1)==1){ 
						$html.= '<a href="javascript:void(0)" onclick="javascript:ver_form('.$key->id.')">';
						$html.= '<img src="'.$lectura.'" style="width:30px; height:30px" alt="ver" title="Ver Empresa"></a>';
					}	

                    if($this->clssession->accion(1,3)==1){ 
						$html.= '<a href="javascript:void(0)" onclick="javascript:modificar_form('.$key->id.')">';
						$html.= '<img src="'.$modificar.'" style="width:30px; height:30px" alt="Modificar" title="Modificar Empresa"></a>';
					}	
					
					if($this->clssession->accion(1,4)==1){ 
						$html.= '&nbsp;<a href="javascript:void(0)" onclick="javascript:eliminar_form('.$key->id.')">';
						$html.= '<img src="'.$eliminar.'" style="width:30px; height:30px" alt="Desactivar" title="Desactivar Empresa"></a>';
					}	
					
				}else{
					if($this->clssession->accion(1,5)==1){ 
						$html.= '<a href="javascript:void(0)" onclick="javascript:reactivar_form('.$key->id.')">';
						$html.= '<img src="'.$reactivar.'" style="width:30px; height:30px" alt="Activar" Reactivar Empresa></a>';
					}	
				}	
				$html.= '       </td>';				
				$html.= '</tr>';
			}
		}	
		$html.= '    </tbody>';
		$html.= '</table>';
		return $html;    				
	}


	function ajax_validar_duplicidad(){
        
        $nombre = $_GET['nombre']; 
		$nro = $this->Cf_empresa_mdl->valNombre($nombre);
		$status = 0;


		if($nro>0){
		    $status = 1;	
		}

		$data = array(
		    "status"=> $status
		);
	    echo json_encode($data);
	}    		


	function ajax_validar_duplicidad_modificar($nombre){
        
		$nro = $this->Cf_empresa_mdl->valNombre($nombre);
		$status = 0;


		if($nro>0){
		    $status = 1;	
		}
		return $status;

	}    		

	

}
