<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cf_sucursal extends CI_Controller {


	
	public function __construct() {
         parent::__construct();
         $this->load->model(array('Cf_sucursal_mdl','Cf_configuracion_mdl'));
         $this->load->library(array('Clssession')); 
    }


	public function index()	{
	//Validar session usuario
	$valSession = $this->clssession->valSession();
	if($valSession==true){
		
        //accion 0 vista sin datatable, accion 1  activar datatable 
		$data=array(
			'accion'=>1
		);
		
	    $this->load->view('header');
		$this->load->view('menu');
		$this->load->view('configuracion/sucursal/actSucursal',$data);
		$this->load->view('footer/footer', $data);
		$this->load->view('configuracion/sucursal/footer_sucursal', $data);
		//$this->load->view('footer/lib_numerica');
    }//fin val session		
	}
	
	function ajax_datatable($id){
	//Validar session usuario
	$valSession = $this->clssession->valSession();
	if($valSession==true){

		$row = $this->Cf_sucursal_mdl->obt_dataTable();
		$html= $this->generarDatatable($row);
		$data = array(
		    "registro"=>$html);
        echo json_encode($data);	
    }//fin val session 
	}	
	


function ajax_modificar($id){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){

    $row = $this->Cf_sucursal_mdl->obtModificar($id);

    $data = array(
    	'id'=>$row->id,
    	'nombre'=>$row->nombre,
    	'id_region'=>$row->id_region,
    	'id_empresa'=>$row->id_empresa,
        'rut'=>$row->rut,
        'contacto'=>$row->contacto,
        'direccion'=>$row->direccion,
        'correo'=>$row->correo,
        'telefono_local'=>$row->telefono_local,
        'telefono_celular'=>$row->telefono_celular,
    );

    echo json_encode($data);
}//fin val session	
}


function ajax_ver($id){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){
    $row = $this->Cf_sucursal_mdl->obtVer($id);
    $data = array(
    	'id'=>$row->id,
    	'nombre'=>$row->nombre,
    	'id_region'=>$row->id_region,
    	'id_empresa'=>$row->id_empresa,
        'rut'=>$row->rut,
        'contacto'=>$row->contacto,
        'direccion'=>$row->direccion,
        'correo'=>$row->correo,
        'telefono_local'=>$row->telefono_local,
        'telefono_celular'=>$row->telefono_celular,
        'empresa'=>$row->empresa,
        'region'=>$row->region,

    );

    echo json_encode($data);
}//fin val session	
}



function ajax_guardar_add(){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){
    $status = 0;
	$nombre = $this->input->post('m_nombre');
	//$idEmpresa = $this->input->post('id_cbo_empresa');


    $status = $this->ajax_validar_duplicidad($nombre, $this->session->userdata('idEmpresa'));
    if($status==0){
		$idRegion = $this->input->post('id_cbo_region');
		$data = array(
	        'nombre'=>$nombre,
	        'rut'=>$this->input->post('m_rut'),
	        'correo'=>$this->input->post('m_correo'),
	        'contacto'=>$this->input->post('m_contacto'),
            'direccion'=>$this->input->post('m_direccion'),
            'telefono_local'=>$this->input->post('m_telefono_local'),
            'telefono_celular'=>$this->input->post('m_telefono_celular'),
	        'id_region'=>$idRegion,
	        'id_empresa'=>$this->session->userdata('idEmpresa'),
	        //'id_empresa'=>$idEmpresa,
	        'administrador'=>$this->session->userdata('administrador'),
	        'id_sucursal'=>$this->session->userdata('idSucursal'),
	        'sw_sucursal'=>1,
	  	    'id_create'=>$this->session->userdata('idUsuario'),
		);
		$idSucursal = $this->Cf_sucursal_mdl->guardar_add($data);

		//Generar dolarImpuesto.
	    $data=array(
	        'administrador'=>$this->session->userdata('administrador'),
	        'id_empresa'=>$this->session->userdata('idEmpresa'),
	        'id_sucursal'=>$idSucursal,
	    );
	    $this->Cf_configuracion_mdl->guardar_add($data);
	    
	    //creaer datatable
	    $row = $this->Cf_sucursal_mdl->obt_dataTable();
		$html= $this->generarDatatable($row);
		$data = array(
		    "registro"=>$html,
		    "status"=>$status);
	}else{
		$data = array(
		    "status"=>1);
	}	

    echo json_encode($data);
}//fin val session	
}




function ajax_guardar_mod(){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){

    $status = 0;
	$nombre = $this->input->post('m_nombre');
	$nombre_orig = $this->input->post('nombre_orig');
	//$idEmpresa = $this->input->post('id_cbo_empresa');
    $idEmpresa_orig = $this->session->userdata('idEmpresa');

    if(strtoupper($nombre) != strtoupper($nombre_orig)){
	    $status = $this->ajax_validar_duplicidad($nombre, $idEmpresa_orig);
	}



    if($status==0){
		$id = $this->input->post('id_mod');
		$idRegion = $this->input->post('id_cbo_region');		

		$data = array(
	        'nombre'=>$nombre,
	        'id_region'=>$idRegion,	    
	        'rut'=>$this->input->post('m_rut'),
	        'correo'=>$this->input->post('m_correo'),
	        'contacto'=>$this->input->post('m_contacto'),
            'direccion'=>$this->input->post('m_direccion'),
            'telefono_local'=>$this->input->post('m_telefono_local'),
            'telefono_celular'=>$this->input->post('m_telefono_celular'),
	  	    'id_update'=>$this->session->userdata('idUsuario'),
	  	    'date_update'=>date('Y-m-d H:i:s')        
		);
		$respuesta = $this->Cf_sucursal_mdl->guardar_mod($id,$data);

	    $row = $this->Cf_sucursal_mdl->obt_dataTable();
		$html= $this->generarDatatable($row);
		$data = array(
		    "registro"=>$html,
		    "status"=>$status);
	}else{
		$data = array(
		    "status"=>1);
	}	

    echo json_encode($data);
}//fin val session	
}
	

function ajax_desactivar($id){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){

	$data = array(
  	    'activo'=>0,
  	    'id_update'=>$this->session->userdata('idUsuario'),
  	    'date_update'=>date('Y-m-d H:i:s')
    );


	$respuesta = $this->Cf_sucursal_mdl->desactivar($id,$data);
	$row = $this->Cf_sucursal_mdl->obt_dataTable();
	$html= $this->generarDatatable($row);
	$data = array(
	    "registro"=>$html);
    echo json_encode($data);
}//fin val session    
}


function ajax_reactivar($id){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){

	$data = array(
  	    'activo'=>1,
  	    'id_update'=>$this->session->userdata('idUsuario'),
  	    'date_update'=>date('Y-m-d H:i:s')
    );


	$respuesta = $this->Cf_sucursal_mdl->desactivar($id,$data);
	
	$row = $this->Cf_sucursal_mdl->obt_dataTable();
	$html= $this->generarDatatable($row);
	$data = array(
	    "registro"=>$html);
    echo json_encode($data);

}//fin val session
}


	
	
	
	
	function generarDatatable($row){
		//obtener la información de la tabla seleccionada
		$modificar = base_url() . "assets/images/modificar02.jpeg";
		$eliminar = base_url() . "assets/images/eliminar.jpeg";
		$reactivar = base_url() . "assets/images/reactivar.jpeg";
		$lectura = base_url() . "assets/images/ver.png";
		
        $html = '<table id="basic-datatables" class="display table table-striped table-hover">';
        $html.= '    <thead>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="30%">Nombre</th>';
		$html.= '           <th width="10%">Rut</th>';        
        $html.= '           <th width="25%">Empresa Asociada</th>';
		$html.= '           <th width="20%">Región</th>';        
        $html.= '           <th width="15%">Acción</th>';
        $html.= '        </tr>';
        $html.= '    </thead>';
        $html.= '    <tfoot>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="30%">Nombre</th>';
		$html.= '           <th width="10%">Rut</th>';        
        $html.= '           <th width="25%">Empresa Asociada</th>';
		$html.= '           <th width="20%">Región</th>';        
        $html.= '           <th width="15%">Acción</th>';
        $html.= '        </tr>';
        $html.= '    </tfoot>';
        $html.= '    <tbody>';
        if($row==false){
			$html.= '      <tr>';
			$html.= '          <td></td><td></td><td></td><td></td><td></td>';
			$html.= '      </tr>';
		}else{
		    foreach($row as $key){ 
				$html.= '  <tr>';
				$html.= '      <td>' . $key->nombre.'</td>';
				$html.= '      <td>' . $key->rut.'</td>';
				$html.= '      <td>' . $key->empresa.'</td>';
				$html.= '      <td>' . $key->region.'</td>';				

				$html.= '      <td>';				
				if($key->activo==1){

					if($this->clssession->accion(1,1)==1){ 
						$html.= '<a href="javascript:void(0)" onclick="javascript:ver_form('.$key->id.')">';
						$html.= '<img src="'.$lectura.'" style="width:30px; height:30px" alt="Ver" title="Ver datos sucursal"></a>';
					}

                    if($this->clssession->accion(1,3)==1){ 
						$html.= '<a href="javascript:void(0)" onclick="javascript:modificar_form('.$key->id.')">';
						$html.= '<img src="'.$modificar.'" style="width:30px; height:30px" 
							alt="Modificar" title="Modificar Sucursal"></a>';
					}
						
                    if($this->clssession->accion(1,4)==1){ 					
						$html.= '&nbsp;<a href="javascript:void(0)" onclick="javascript:eliminar_form('.$key->id.')">';
						$html.= '<img src="'.$eliminar.'" style="width:30px; height:30px" alt="Desactivar" title="Desactivar Sucursal"></a>';
                    }

				}else{
					if($this->clssession->accion(1,5)==1){ 					
						$html.= '<a href="javascript:void(0)" onclick="javascript:reactivar_form('.$key->id.')">';
						$html.= '<img src="'.$reactivar.'" style="width:30px; height:30px" alt="Activar" title="Activar Sucursal"></a>';
					}	
				}	
				$html.= '       </td>';				
				$html.= '</tr>';
			}
		}	
		$html.= '    </tbody>';
		$html.= '</table>';
		return $html;    				
	}


	function ajax_validar_duplicidad($nombre, $idEmpresa){
		$nro = $this->Cf_sucursal_mdl->valNombre($nombre, $idEmpresa);
		$status = 0;
		if($nro>0){
		    $status = 1;	
		}

		return $status;
	}
	

}
