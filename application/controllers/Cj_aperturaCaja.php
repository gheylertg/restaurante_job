<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cj_aperturaCaja extends CI_Controller {

	
	public function __construct() {
        parent::__construct();
        $this->load->model(array('Cj_aperturaCaja_mdl','Sg_usuario_mdl','Cj_movimientoCaja_mdl'));
        $this->load->library(array('Clssession')); 



    }


	public function index()	{
    //Validar session usuario
    $valSession = $this->clssession->valSession();
    if($valSession==true){  
        $idUsuario = $this->session->userdata('idUsuario');
        $idEmpresa = $this->session->userdata('idEmpresa');
        $idSucursal = $this->session->userdata('idSucursal');        
		$status = $this->Cj_aperturaCaja_mdl->val_apertura($idUsuario, $idEmpresa, $idSucursal);
        if($status==0){       
            $dataSession = array(
                'idCaja'=>0
            );
            $this->session->set_userdata($dataSession);
            $data=array(
                'apertura'=>0,
                'accion'=>0
            );
		    $this->load->view('header');
			$this->load->view('menu');
			$this->load->view('caja/aperturaCaja/act_aperturaCaja',$data);
			$this->load->view('footer/footer', $data);
			$this->load->view('caja/aperturaCaja/footer_aperturaCaja', $data);
			$this->load->view('footer/lib_numerica');
        }else{
            $data=array(
                'apertura'=>1,
                'accion'=>0
            );
		    $this->load->view('header');
			$this->load->view('menu');
			$this->load->view('caja/aperturaCaja/act_aperturaCaja',$data);			
			$this->load->view('footer/footer', $data);
			$this->load->view('caja/aperturaCaja/footer_aperturaCaja', $data);
        }
	} //fin val session
    }


	function ajax_obtDatosUsuario(){
    //Validar session usuario
    $valSession = $this->clssession->valSession();
    if($valSession==true){          
        $rowUsuario = $this->Sg_usuario_mdl->obtModificar($this->session->userdata('idUsuario'));
            //mostrar fotos.
        if($rowUsuario->foto==null || empty($rowUsuario->foto)){
        	if($rowUsuario->sexo=="F"){
                $foto = base_url() . "assets/images/mujer_sinFoto.jpeg";
        	}else{
        		$foto = base_url() . "assets/images/hombre_sinFoto.jpeg";
        	}
        }else{
             $foto = base_url() . "assets/foto/" . $rowUsuario->foto;

        } 

        $html = '<img src="'.$foto.'" style="width:150px; height:150px" alt="Foto">';           

        $data=array(
        	'nombreUsuario'=>$rowUsuario->nombre,
        	'apellidoUsuario'=>$rowUsuario->apellido,
        	'fotoUsuario'=>$html,
        );
        echo json_encode($data);
	} //fin val session
    }


	function ajax_guardar_aperturaCaja(){
    //Validar session usuario
    $valSession = $this->clssession->valSession();
    if($valSession==true){          
        $monto = $this->input->post('m_monto');
        if(trim($monto)==""){
        	$monto = "0,00";
        }
		$monto = str_replace('.','',$monto);
		$monto = str_replace(',','.',$monto);        
        $observacion = $this->input->post('m_observacion');
		$data = array(
	        'id_usuario_empresa'=>$this->session->userdata('idUsuarioEmpresa'),
	        'id_usuario'=>$this->session->userdata('idUsuario'),
	        'id_empresa'=>$this->session->userdata('idEmpresa'),
	        'id_sucursal'=>$this->session->userdata('idSucursal'),
	        'estado'=>1,
	        'observacion'=>$observacion,
	  	    'id_create'=>$this->session->userdata('idUsuario'),
		);

		$idCaja = $this->Cj_aperturaCaja_mdl->aperturarCaja($data);

        //guardar variable de session

        $dataSession = array(
           'idCaja'=>$idCaja 
        ); 
        $this->session->set_userdata($dataSession);
        


		//registrar movimiento caja apertura

		$data = array(
            'id_caja'=>$idCaja,
            'id_tipo_operacion'=>1,
            'id_tipo_movimiento'=>1,
            'monto'=>$monto,
        );
        
        $this->Cj_movimientoCaja_mdl->guardarMovimiento($data);
		$data = array(
		    "status"=>0
		);
		echo json_encode($data);

    } //fin val session
	}


    function ajax_errorApertura(){
    //Validar session usuario
    $valSession = $this->clssession->valSession();
    if($valSession==true){  		
        $rowUsuario = $this->Sg_usuario_mdl->obtModificar($this->session->userdata('idUsuario'));
        $rowDatoApertura = $this->Cj_aperturaCaja_mdl->obt_datosApertura($this->session->userdata('idUsuario'),$this->session->userdata('idEmpresa'), $this->session->userdata('idSucursal'));

            //mostrar fotos.
        if($rowUsuario->foto==null || empty($rowUsuario->foto)){
        	if($rowUsuario->sexo=="F"){
                $foto = base_url() . "assets/images/mujer_sinFoto.jpeg";
        	}else{
        		$foto = base_url() . "assets/images/hombre_sinFoto.jpeg";
        	}
        }else{
             $foto = base_url() . "assets/foto/" . $rowUsuario->foto;

        } 



        $html = '<img src="'.$foto.'" style="width:150px; height:150px" alt="Foto">';           

        //formatear fecha
        $fecha = substr($rowDatoApertura->fecha,0,19);
        $mensajeError = "Esta caja fué aperturada en esta fecha / hora <br><br>";
        $mensajeError.= "<b>" . $fecha . ".</b> <br><br>";
        $mensajeError.= "Y no ha sido cerrada.";




        $data=array(
        	'nombreUsuario'=>$rowUsuario->nombre,
        	'apellidoUsuario'=>$rowUsuario->apellido,
        	'fecha'=>$fecha,
           	'mensajeError'=>$mensajeError,
        	'fotoUsuario'=>$html,
        );
        echo json_encode($data);

    }  //fin val session
    }
}
