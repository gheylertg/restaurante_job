<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cj_cierreCaja extends CI_Controller {

	
	public function __construct() {
         parent::__construct();
         $this->load->model(array('Cj_movimientoCaja_mdl', 'Cj_cierreCaja_mdl','Cj_cobroCaja_mdl',
                                  'Cj_aperturaCaja_mdl'));
         $this->load->library(array('Clssession')); 
    }

	public function index(){
    //Validar session usuario
    $valSession = $this->clssession->valSession();
    if($valSession==true){		
       //Validar apertura de caja
	      $resultado = $this->Cj_cierreCaja_mdl->obt_cajaCierreGlobal();
	      if($resultado==0){	
  			    $data=array(
  			  	   'accion'=>2
  			    );
  	        $this->load->view('header');
  			    $this->load->view('menu');
  			    $this->load->view('footer/footer', $data);
  			    $this->load->view('caja/cierreCaja/footer_cierreCaja', $data);
        }else{  
	          //accion 0 vista sin datatable, accion 1  activar datatable 
			      $data=array(
				       'accion'=>1
			      );
	    	    $this->load->view('header');
			      $this->load->view('menu');
			      $this->load->view('caja/cierreCaja/act_cierreCaja',$data);
			      $this->load->view('caja/cierreCaja/act_resumenCaja',$data);
			      $this->load->view('footer/footer', $data);
			      $this->load->view('caja/cierreCaja/footer_cierreCaja', $data);
			      //$this->load->view('footer/lib_numerica');
		    }	
	  }//Fin val session	
	}



	function ajax_datatable($id){
		$row = $this->Cj_cierreCaja_mdl->obt_dataTable();
		$html= $this->generarDatatable($row);
		$nombreEmpresa = $this->session->userdata('datosUES');
		$data = array(
		    "registro"=>$html,
		    "nombreEmpresa"=>$nombreEmpresa
		);
        echo json_encode($data);	

	}	


	function generarDatatable($row){
		//obtener la información de la tabla seleccionada
		$modificar = base_url() . "assets/images/cerrarCaja.jpeg";
		$eliminar = base_url() . "assets/images/eliminar.jpeg";
		$reactivar = base_url() . "assets/images/reactivar.jpeg";
		$lectura = base_url() . "assets/images/ver.png";
		
        $html = '<table id="basic-datatables" class="display table table-striped table-hover">';
        $html.= '    <thead>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="11%">Fecha/Hora Apertura</th>';
        $html.= '           <th width="22%">Apertura por</th>';
        $html.= '           <th width="12%">Ingresos</th>';
        $html.= '           <th width="12%">Egresos</th>';
        $html.= '           <th width="12%">Total En Caja</th>';
        $html.= '           <th width="19%">Observación Apertura</th>';
        $html.= '           <th width="10%">Acción</th>';
        $html.= '        </tr>';
        $html.= '    </thead>';
        $html.= '    <tfoot>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="11%">Fecha/Hora Apertura</th>';
        $html.= '           <th width="22%">Apertura por</th>';
        $html.= '           <th width="12%">Ingresos</th>';
        $html.= '           <th width="12%">Egresos</th>';
        $html.= '           <th width="12%">Total En Caja</th>';
        $html.= '           <th width="19%">Observación Apertura</th>';
        $html.= '           <th width="10%">Acción</th>';
        $html.= '        </tr>';
        $html.= '    </tfoot>';
        $html.= '    <tbody>';
        if($row==false){
			$html.= '      <tr>';
			$html.= '          <td></td><td></td><td></td><td></td>
							   <td></td><td></td><td></td>';
			$html.= '      </tr>';
		}else{
		    foreach($row as $key){ 
				$html.= '  <tr>';
				$html.= '      <td>' . $key->fecha .'</td>';
                $nombre = trim($key->nombre) .", ". trim($key->apellido);
				$html.= '      <td>' . $nombre .'</td>';
				//$ingreso = str_replace(".",",",$key->ingreso);
				$ingreso = number_format ($key->ingreso, 2,',','.'); 
				$html.= '      <td style="text-align:right">' . $ingreso .'</td>';
                if($key->egreso==null){
                    $egreso= "0,00"; 
                }else{  
					//$egreso = str_replace(".",",",$key->egreso);
					$egreso = number_format ($key->egreso, 2,',','.'); 
				}	
				$html.= '      <td style="text-align:right">' . $egreso .'</td>';
				$total = $key->ingreso - $key->egreso;
                $monto = number_format ($total, 2,',','.'); 
				//$monto = str_replace(".",",",$total);


				$html.= '      <td style="text-align:right">' . $monto .'</td>';
				$observa = substr($key->observacion,0,100);
				$html.= '      <td>' . $observa .'</td>';

				$html.= '      <td>';				


                $html.= '<a href="javascript:void(0)" onclick="javascript:detalleCaja_form('.$key->id.')">';
				$html.= '<img src="'.$lectura.'" style="width:25px; height:25px" 
						alt="Resumen Caja" title="Resumen Caja"></a>';
				
				$html.= '&nbsp;<a href="javascript:void(0)" onclick="javascript:cerrarCaja_form('.$key->id.')">';
				$html.= '<img src="'.$modificar.'" style="width:25px; height:25px" 
						alt="Cerrar Caja" title="Cerrar Caja"></a>';
					
				$html.= '       </td>';				
				$html.= '</tr>';
			}
		}	
		$html.= '    </tbody>';
		$html.= '</table>';
		return $html;    				
	}


    function resumenCajaUsuario(){
    //Validar session usuario
    $valSession = $this->clssession->valSession();
    if($valSession==true){		
        $idUsuario = $this->session->userdata('idUsuario');
	      $resultado = $this->Cj_cierreCaja_mdl->obt_cajaCierre($idUsuario);
	      if($resultado==0){	
            $dataSession = array(
                'idCaja'=>0
            );
            $this->session->set_userdata($dataSession);
      			$data=array(
  		    		'accion'=>4
  			    );
  	    	  $this->load->view('header');
  			    $this->load->view('menu');
  			    $this->load->view('footer/footer', $data);
  			    $this->load->view('caja/cierreCaja/footer_cierreCaja');		
		    }else{
      			$data=array(
      				'accion'=>3,
      			);
      			$this->load->view('header');
			      $this->load->view('menu');
			      $this->load->view('caja/cierreCaja/act_resumenCaja',$data);			
			      $this->load->view('footer/footer', $data);
			      $this->load->view('caja/cierreCaja/footer_cierreCaja', $data);
		    }
	  } //fin val session
    }


function ajax_cerrarCaja($idCaja){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){		
    		  
    $rowCaja = $this->Cj_aperturaCaja_mdl->obt_datosCaja($idCaja);
    $rowCobro = $this->Cj_cobroCaja_mdl->obtCobro($idCaja);
    $rowMovimiento = $this->Cj_movimientoCaja_mdl->obt_totalesMovimiento($idCaja);

    $totalCobrar = number_format ($rowCobro->total_cobrar, 2,',','.');
    $totalApertura = number_format ($rowMovimiento->apertura, 2,',','.');
    $totalIngreso = number_format ($rowMovimiento->ingreso, 2,',','.');
    $totalEgreso = number_format ($rowMovimiento->egreso, 2,',','.');

    $total_caja = ($rowMovimiento->apertura + $rowCobro->total_cobrar + $rowMovimiento->ingreso) - $rowMovimiento->egreso;
    $totalcaja = number_format ($total_caja, 2,',','.');    


    $data = array(
    	'idCaja'=>$idCaja,
        'fecha'=>$rowCaja->fecha,
        'observacion'=>$rowCaja->observacion,
        'cobro'=>$totalCobrar,
        'apertura'=>$totalApertura,
        'ingreso'=>$totalIngreso,
        'egreso'=>$totalEgreso,
        'totalCaja'=>$totalcaja,
        'monto_totalCaja'=>$total_caja,

    );
    echo json_encode($data);	

} //fin val session
}	


function ajax_guardarCierre(){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){		
  	$id = $this->input->post('id_mod');
    $monto = $this->input->post('monto_totalCaja');
  	$monto = str_replace('.','',$monto);
  	$monto = str_replace(',','.',$monto); 
  	$status=0;
  	$data = array(
        'fecha_cierre'=>date('Y-m-d H:i:s'),
        'monto_cierre'=>$monto,
        'estado'=>2,
        'observacion_cierre'=>$this->input->post('observacionCierre'),
        'id_encargado_cierre'=>$this->session->userdata('idUsuario'),
  	    'id_update'=>$this->session->userdata('idUsuario'),
  	    'date_update'=>date('Y-m-d H:i:s')        
  	);
  	$respuesta = $this->Cj_cierreCaja_mdl->guardarCierre($id,$data);
    $arrayCaja = array(
        'idCaja'=>0
    );
    $this->session->set_userdata($arrayCaja); 


    //$this->session->unset_userdata($arrayCaja);
  	$row = $this->Cj_cierreCaja_mdl->obt_dataTable();
  	$html= $this->generarDatatable($row);
  	$nombreEmpresa = $this->session->userdata('datosUES');
  	$data = array(
  		"status"=>0,
  	    "registro"=>$html,
  	    "nombreEmpresa"=>$nombreEmpresa
  	);
    echo json_encode($data);	
}//fin val session	
}


public function ajax_detalleCaja($idCaja){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){		

	if($idCaja==0){
		$idCaja = $this->session->userdata('idCaja');
	}

	// obtener datos del usuario caja
   $this->load->model('Sg_usuario_mdl');   
   $rowUsuario = $this->Sg_usuario_mdl->obtModificar($this->session->userdata('idUsuario'));
   $nombreUsuario = trim($rowUsuario->nombre) . ", " . trim($rowUsuario->apellido);
    $rowIngreso = $this->Cj_movimientoCaja_mdl->obt_ingreso($idCaja,1);

    $htmlIng= '<div class="container" style="height: 150px;overflow-y: auto;width:100%; border:1px solid black">';
   	$htmlIng.= '<table>';
    $htmlIng.= '	<tbody style="font-size:12px">';
    $totalIngreso=0.00;
    if($rowIngreso==false){
			$htmlIng.= '		<tr>';
			$htmlIng.= '		    <td></td>';
			$htmlIng.= '		    <td></td>';
			$htmlIng.= '		</tr>';
    }else{
	    foreach ($rowIngreso as $key) {
	    	$totalIngreso = $totalIngreso + $key->monto;
			$htmlIng.= '		<tr>';
			$htmlIng.= '		    <td width="500px">' .$key->movimiento. '</td>';
			$total = number_format ($key->monto, 2,',','.'); 
			$htmlIng.= '		    <td style="width:100px; text-align:right">' .$total.'</td>';
			$htmlIng.= '		</tr>';
		}
	}		
	$htmlIng.= '	</tbody>';
	$htmlIng.= '</table>';
	$htmlIng.= '</div>';

    $totalIng = number_format ($totalIngreso, 2,',','.'); 

    ///Egresos
    $rowEgreso = $this->Cj_movimientoCaja_mdl->obt_ingreso($idCaja,2);

    $htmlEgr = '<div class="container" style="height: 150px;overflow-y: auto;width:100%; border:1px solid black">';
   	$htmlEgr.= '<table>';
    $htmlEgr.= '	<tbody style="font-size:12px">';
    $totalEgreso=0.00;
    if($rowEgreso==false){
			$htmlEgr.= '		<tr>';
			$htmlEgr.= '		    <td></td>';
			$htmlEgr.= '		    <td></td>';
			$htmlEgr.= '		</tr>';
    }else{
	    foreach ($rowEgreso as $key) {
	    	$totalEgreso = $totalEgreso + $key->monto;
			$htmlEgr.= '		<tr>';
			$htmlEgr.= '		    <td width="500px">' .$key->movimiento. '</td>';
			$total = number_format ($key->monto, 2,',','.'); 
			$htmlEgr.= '		    <td style="width:100px; text-align:right">' .$total.'</td>';
			$htmlEgr.= '		</tr>';
		}
	}		
	$htmlEgr.= '	</tbody>';
	$htmlEgr.= '</table>';
	$htmlEgr.= '</div>';
    $totalEgr = number_format ($totalEgreso, 2,',','.'); 


    //Ventas
    $rowVenta = $this->Cj_cobroCaja_mdl->obtCobrosDetallados($idCaja);





    $htmlVenta = '<div class="container" style="height: 200px;overflow-y: auto;width:100%; border:1px solid black">';
   	$htmlVenta.= '<table>';
    $htmlVenta.= '	<tbody style="font-size:11px">';
	$htmlVenta.= '		<thead>';
	$htmlVenta.= '		    <th width="130px">Fecha</th>';
	$htmlVenta.= '		    <th style="width:130px; text-align:center">Documento</th>';
	$htmlVenta.= '		    <th style="width:80px;">Tipo Despacho</th>';
	$htmlVenta.= '		    <th style="width:100px;">Monto a cobrar </th>';
	$htmlVenta.= '		    <th style="width:100px; text-align:right">Descuento </th>';
	$htmlVenta.= '		    <th style="width:100px;text-align:right">Total Venta</th>';
	$htmlVenta.= '		</thead>';

    $totalCobro=0.00;
    $totDescuento=0.00;
    if($rowVenta==false){
			$htmlVenta.= '		<tr>';
			$htmlVenta.= '		    <td></td>';
			$htmlVenta.= '		    <td></td>';
			$htmlVenta.= '		    <td></td>';
			$htmlVenta.= '		    <td></td>';
			$htmlVenta.= '		    <td></td>';
			$htmlVenta.= '		</tr>';
    }else{

	    foreach ($rowVenta as $key) {
	    	$totalCobro = $totalCobro + $key->total_cobrar;
	    	$totDescuento = $totDescuento + $key->descuento;




			$htmlVenta.= '		<tr style="font-size:11px">';
			$htmlVenta.= '		    <td width="130px">' .$key->fecha. '</td>';
         	//$htmlVenta.= '		    <td style="width:130px; text-align:center">' .$key->documento. '</td>';

         	$htmlVenta.= '		    <td style="width:130px; text-align:center">00001</td>';

			$htmlVenta.= '		    <td width="180px">' .$key->despacho. '</td>';

			$mtoCobrar = number_format ($key->monto_cobrar, 2,',','.'); 
			$htmlVenta.= '		    <td style="width:100px; text-align:right">' .$mtoCobrar.'</td>';

			$descuento = number_format ($key->descuento, 2,',','.'); 
			$htmlVenta.= '		    <td style="width:100px; text-align:right">' .$descuento.'</td>';

			$totVenta = number_format ($key->total_cobrar, 2,',','.'); 
			$htmlVenta.= '		    <td style="width:100px; text-align:right">' .$totVenta.'</td>';



			$htmlVenta.= '		</tr>';
		}
	}		
	$htmlVenta.= '	</tbody>';
	$htmlVenta.= '</table>';
	$htmlVenta.= '</div>';


	$totDescuento = number_format ($totDescuento, 2,',','.'); 
	$totCobro = number_format ($totalCobro, 2,',','.'); 

    $totVenta = '<span style="color: #00f;">DESCUENTO:&nbsp;&nbsp; </span>' . $totDescuento;

    $totVenta.= ' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="color: #00f;">TOTAL VENTA:&nbsp;&nbsp;  </span>'. $totCobro;

    $totalCaja = ($totalCobro + $totalIngreso) - $totalEgreso;
    $totCaja = number_format ($totalCaja, 2,',','.');     
    


	$data = array(
		"status"=>0,
		"tablaIngreso"=>$htmlIng,
		"totalIngreso"=>$totalIng,
		"tablaEgreso"=>$htmlEgr,
		"totalEgreso"=>$totalEgr,
        "tablaVenta"=>$htmlVenta,
        "totalVenta"=>$totVenta,
        "tVenta"=>$totCobro,
        "tCaja"=>$totCaja,
        "idCaja"=>$idCaja,
        "nombreUsuario"=>$nombreUsuario
	);
    echo json_encode($data);	
}//fin val session
}



public function ajax_arqueoCaja($idCaja){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){		


   $peso=0.00;
   $pesoDel=0.00; 
   $pesoVuelto=0.00;
   $totPeso=0.00;

   $dolar=0.00;
   $dolarDel=0.00; 
   $dolarVuelto=0.00;
   $totDolar = 0.00;

   $debito=0.00;
   $debitoDel=0.00; 
   $totDebito = 0.00;

   $credito=0.00;
   $creditoDel=0.00; 
   $totCredito = 0.00;

   $transferencia=0.00;
   $transferenciaDel=0.00; 
   $totTransferencia = 0.00;

   //operaciones de movimeinto 
   $rowMovimiento = $this->Cj_cierreCaja_mdl->arqueoMovimiento($idCaja);

   foreach ($rowMovimiento as $key) {

   		if($key->id_tipo_operacion==1){
            $peso = $peso + $key->monto;
   		}else{
            $peso = $peso - $key->monto;    
   		}
   }



   //operaciones de movimeinto 
   $rowCobrar = $this->Cj_cobroCaja_mdl->obtArqueoCobro($idCaja);
   foreach ($rowCobrar as $key) {
       switch($key->id_tipo_dinero){
       case 1:
           if($key->id_tipo_despacho==3){
                $pesoDel=$pesoDel+$key->monto_peso;
           }else{
               $peso=$peso+$key->monto_peso;
           } 
           break;

       case 2:
           if($key->id_tipo_despacho==3){
               $dolarDel=$dolarDel+$key->monto_dolar;
           }else{
               $dolar=$dolar+$key->monto_dolar;
           } 
           break;

       case 3:
           if($key->id_tipo_despacho==3){
               $debitoDel=$debitoDel+$key->monto_peso;
           }else{
               $debito=$debito+$key->monto_peso;
           } 
           break;

       case 4:
           if($key->id_tipo_despacho==3){
               $creditoDel=$creditoDel+$key->monto_peso;
           }else{
               $credito=$credito+$key->monto_peso;
           } 
           break;
       case 5:
           $dolarVuelto = $dolarVuelto + $key->monto_dolar;
           $pesoVuelto = $pesoVuelto + $key->monto_peso;
           break;


       case 6:
           if($key->id_tipo_despacho==3){
                $transferenciaDel= $transferenciaDel+$key->monto_peso;
           }else{
                $transferencia= $transferencia+$key->monto_peso;
           } 
           break;
        }  
   }

    
   $totPeso = ($peso + $pesoDel) - $pesoVuelto;
   $totDolar = ($dolar + $dolarDel) - $dolarVuelto;
   $totDebito = $debito + $debitoDel;
   $totCredito = $credito + $creditoDel;
   $totTransferencia = $transferencia + $transferenciaDel;

   $peso=number_format ($peso, 2,',','.');
   $pesoDel=number_format ($pesoDel, 2,',','.');
   $pesoVuelto=number_format ($pesoVuelto, 2,',','.');
   $totPeso = number_format ($totPeso, 2,',','.');

   $dolar=number_format ($dolar, 2,',','.');
   $dolarDel=number_format ($dolarDel, 2,',','.');
   $dolarVuelto=number_format ($dolarVuelto, 2,',','.');
   $totDolar = number_format ($totDolar, 2,',','.');

   $debito=number_format ($debito, 2,',','.');
   $debitoDel=number_format ($debitoDel, 2,',','.');
   $totDebito = number_format ($totDebito, 2,',','.');

   $credito=number_format ($credito, 2,',','.');
   $creditoDel=number_format ($creditoDel, 2,',','.');
   $totCredito = number_format ($totCredito, 2,',','.');

   $transferencia=number_format ($transferencia, 2,',','.');
   $transferenciaDel=number_format ($transferenciaDel, 2,',','.');
   $totTransferencia = number_format ($totTransferencia, 2,',','.');

// obtener datos del usuario caja
   $this->load->model('Sg_usuario_mdl');   
   $rowUsuario = $this->Sg_usuario_mdl->obtModificar($this->session->userdata('idUsuario'));
   $nombreApellido = trim($rowUsuario->nombre) . ", " . trim($rowUsuario->apellido);





	$data = array(
		"status"=>0,
		"peso"=>$peso,
		"pesoDel"=>$pesoDel,
		"pesoVuelto"=>$pesoVuelto,
		"totPeso"=>$totPeso,

		"dolar"=>$dolar,
		"dolarDel"=>$dolarDel,
		"dolarVuelto"=>$dolarVuelto,
		"totDolar"=>$totDolar,

		"debito"=>$debito,
		"debitoDel"=>$debitoDel,
		"totDebito"=>$totDebito,

		"credito"=>$credito,
		"creditoDel"=>$creditoDel,
		"totCredito"=>$totCredito,


		"transferencia"=>$transferencia,
		"transferenciaDel"=>$transferenciaDel,
		"totTransferencia"=>$totTransferencia,

		"nombreApellido"=>$nombreApellido

	);
    echo json_encode($data);	
} //fin val session
}

///////////////////////////////////////////////////////////////////////////////



	

function ajax_modificar($id){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){		

    $row = $this->Cj_movimientoCaja_mdl->obtModificar($id);

    $monto = $row->monto;
    $monto = str_replace(".",",",$monto);

    $data = array(
    	'id'=>$row->id,
    	'id_tipo_operacion'=>$row->id_tipo_operacion,
    	'id_tipo_movimiento'=>$row->id_tipo_movimiento,
    	'id_tipo_dinero'=>$row->id_tipo_dinero,
    	'monto'=>$monto,
    	'observacion'=>$row->observacion,
    );

    echo json_encode($data);
}//fin val session
}



function ajax_ver($id){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){		

    $row = $this->Cj_movimientoCaja_mdl->obt_Ver($id);

    $monto = $row->monto;
    $monto = str_replace(".",",",$monto);

    $operacion = "CRÉDITO";
    if($row->id_tipo_operacion==2){
       $operacion = "DÉBITO";
    } 


    $data = array(
    	'id'=>$row->id,
    	'tipo_operacion'=>$operacion,
    	'tipo_movimiento'=>$row->movimiento,
    	'tipo_dinero'=>$row->dinero,
    	'monto'=>$monto,
    	'reverso'=>$row->reverso,
    	'motivo_reverso'=>$row->motivo_reverso,
    	'fecha_reverso'=>$row->fecha_reverso,


    	'observacion'=>$row->observacion,
    );
    echo json_encode($data);
} //fin val session
}


function ajax_guardar_mod(){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){		

	$id = $this->input->post('id_mod');
    $monto = $this->input->post('m_monto');
	$monto = str_replace('.','',$monto);
	$monto = str_replace(',','.',$monto); 

	$status=0;
    
	$data = array(
        'id_tipo_operacion'=>$this->input->post('id_cbo_tipoOperacion'),
        'id_tipo_movimiento'=>$this->input->post('id_cbo_tipoMovimiento'),
        'id_tipo_dinero'=>$this->input->post('id_cbo_tipoDinero'),
        'monto'=>$monto,
        'observacion'=>$this->input->post('m_observacion'),
  	    'id_update'=>$this->session->userdata('idUsuario'),
  	    'date_update'=>date('Y-m-d H:i:s')        
	);
	$respuesta = $this->Cj_movimientoCaja_mdl->guardar_mod($id,$data);

	$row = $this->Cj_movimientoCaja_mdl->obt_dataTable();
	$html= $this->generarDatatable($row);
	$data = array(
	    "registro"=>$html,
	    "status"=>$status
	);
    echo json_encode($data);
} //fin val session
}	


function ajax_guardarApertura_mod(){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){		

    $status = 0;

	$id = $this->input->post('id_mod_apertura');
    $monto = $this->input->post('aper_monto');
	$monto = str_replace('.','',$monto);
	$monto = str_replace(',','.',$monto); 


	$data = array(
        'monto'=>$monto,
        'observacion'=>$this->input->post('aper_observacion'),
  	    'id_update'=>$this->session->userdata('idUsuario'),
  	    'date_update'=>date('Y-m-d H:i:s')        
	);
	$respuesta = $this->Cj_movimientoCaja_mdl->guardar_mod($id,$data);

	$row = $this->Cj_movimientoCaja_mdl->obt_dataTable();
	$html= $this->generarDatatable($row);
	$data = array(
	    "registro"=>$html,
	    "status"=>$status
	);
    echo json_encode($data);
} //Fin val session	
}


function ajax_guardar_reverso(){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){		

    $status = 0;

	$id = $this->input->post('id_mod_reverso');

	$data = array(
        'reverso'=>1,
        'motivo_reverso'=>$this->input->post('r_motivo'),
        'fecha_reverso'=>date('Y-m-d H:i:s'),        
        'activo'=>0,
  	    'id_update'=>$this->session->userdata('idUsuario'),
  	    'date_update'=>date('Y-m-d H:i:s')        
	);

	$respuesta = $this->Cj_movimientoCaja_mdl->guardar_mod($id,$data);

	$row = $this->Cj_movimientoCaja_mdl->obt_dataTable();
	$html= $this->generarDatatable($row);
	$data = array(
	    "registro"=>$html,
	    "status"=>$status
	);
    echo json_encode($data);
} //fin val session
}




/* **************************************************************** */


	
/*
function ajax_desactivar($id){
	$data = array(
  	    'activo'=>0,
  	    'id_update'=>$this->session->userdata('idUsuario'),
  	    'date_update'=>date('Y-m-d H:i:s')
    );


	$respuesta = $this->Cj_movimientoCaja_mdl->desactivar($id,$data);
	$row = $this->Cj_movimientoCaja_mdl->obt_dataTable();
	$html= $this->generarDatatable($row);
	$data = array(
	    "registro"=>$html);
    echo json_encode($data);
}    


function ajax_reactivar($id){
	$data = array(
  	    'activo'=>1,
  	    'id_update'=>$this->session->userdata('idUsuario'),
  	    'date_update'=>date('Y-m-d H:i:s')
    );


	$respuesta = $this->Cj_movimientoCaja_mdl->desactivar($id,$data);
	
	$row = $this->Cj_movimientoCaja_mdl->obt_dataTable();
	$html= $this->generarDatatable($row);
	$data = array(
	    "registro"=>$html);
    echo json_encode($data);

}




	function ajax_validar_duplicidad($nombre){
		$nro = $this->Cj_movimientoCaja->valNombre($nombre);
		$status = 0;
		if($nro>0){
		    $status = 1;	
		}

		return $status;
	}
*/	



} //fin del controlador
