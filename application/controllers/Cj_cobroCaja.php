<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cj_cobroCaja extends CI_Controller {

	
	public function __construct() {
         parent::__construct();
         $this->load->model(array('Dp_despacho_mdl', 'Cf_configuracion_mdl','Mt_tipoDinero_mdl','Cj_cobroCaja_mdl','Cf_correlativo_mdl'));
         $this->load->library(array('Clssession')); 
    }



	public function index(){
	   //Validar apertura de caja
    //Validar session usuario
    $valSession = $this->clssession->valSession();
    if($valSession==true){
        $this->load->model('Cj_aperturaCaja_mdl');
        $idUsuario = $this->session->userdata('idUsuario');
        $idEmpresa = $this->session->userdata('idEmpresa');
        $idSucursal = $this->session->userdata('idSucursal');        
        $status = $this->Cj_aperturaCaja_mdl->val_apertura($idUsuario, $idEmpresa, $idSucursal);
        if($status==0){       
            $dataSession = array(
                'idCaja'=>0
            );
            $this->session->set_userdata($dataSession);

			$data=array(
				'accion'=>2
			);
	    	$this->load->view('header');
			$this->load->view('menu');
			$this->load->view('footer/footer', $data);
			$this->load->view('caja/cobroCaja/footer_cobroCaja', $data);
	    }else{	
	        //accion 0 vista sin datatable, accion 1  activar datatable 
			$data=array(
				'accion'=>1
			);
	    	$this->load->view('header');
			$this->load->view('menu');
			$this->load->view('caja/cobroCaja/act_cobroCaja',$data);
			$this->load->view('footer/footer', $data);
			$this->load->view('caja/cobroCaja/footer_cobroCaja', $data);
			//$this->load->view('footer/lib_numerica');
		}
	}//fin val session	
    }

	function ajax_datatable($id){
    //Validar session usuario
    $valSession = $this->clssession->valSession();
    if($valSession==true){
		$row = $this->Dp_despacho_mdl->obt_despachoCobro();
		$html= $this->generarDatatable($row);
		$nombreEmpresa = $this->session->userdata('datosUES');
		$data = array(
		    "registro"=>$html,
		    "nombreEmpresa"=>$nombreEmpresa
		);
        echo json_encode($data);	
	}//fin val session	
    }


    function ajax_datatable_new(){
    //Validar session usuario
    $valSession = $this->clssession->valSession();
    if($valSession==true){
       $validar = $this->Dp_despacho_mdl->obt_despachoCobro_new();
       if($validar==1){   
            $this->Dp_despacho_mdl->act_cobro_new();



            $row = $this->Dp_despacho_mdl->obt_despachoCobro();
            $html= $this->generarDatatable($row);
            $data = array(
                "registro"=>$html,
                "status"=>1
            );
            echo json_encode($data);    
        }else{
            $data = array(
                "status"=>0
            );
            echo json_encode($data);    
        }    
    }//fin val session  
    }








	function generarDatatable($row){

		//obtener la información de la tabla seleccionada
		$cobrar = base_url() . "assets/images/cobrar01.jpeg";
		//$eliminar = base_url() . "assets/images/eliminar.jpeg";
		//$reactivar = base_url() . "assets/images/reactivar.jpeg";
		$lectura = base_url() . "assets/images/ver.png";
		
        $html = '<table id="basic-datatables" class="display table table-striped table-hover">';
        $html.= '    <thead>';

        $html.= '        <tr class="tr-datatable">';

        $html.= '           <th width="10%">Código Venta</th>';
        $html.= '           <th width="20%">Tipo Despacho</th>';
        $html.= '           <th width="30%">Información Cliente</th>';
        $html.= '           <th width="10%">monto Pedido</th>';
        $html.= '           <th width="10%">Impuesto</th>';
        $html.= '           <th width="10%">Monto total</th>';
        $html.= '           <th width="10%">Acción</th>';
        $html.= '        </tr>';
        $html.= '    </thead>';
        $html.= '    <tfoot>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="10%">Código Venta</th>';
        $html.= '           <th width="20%">Tipo Despacho</th>';
        $html.= '           <th width="30%">Información Cliente</th>';
        $html.= '           <th width="10%">monto Pedido</th>';
        $html.= '           <th width="10%">Impuesto</th>';
        $html.= '           <th width="10%">Monto total</th>';
        $html.= '           <th width="10%">Acción</th>';
        $html.= '        </tr>';
        $html.= '    </tfoot>';
        $html.= '    <tbody>';
        if($row==false){
			$html.= '      <tr>';
			$html.= '          <td></td><td></td><td></td><td></td><td></td><td></td><td></td>';
			$html.= '      </tr>';
		}else{
		    foreach($row as $key){ 
				$html.= '  <tr>';
                $html.= '      <td>' . $key->codigo_venta.'</td>';                
                $html.= '      <td>' . $key->tipo_despacho.'</td>';
                if($key->id_tipo_despacho==1){  
                    $información = trim($key->sala) . " / " . trim($key->mesa);
                }else{
                    $información = trim($key->nombre_cliente);
                }    
                $html.= '      <td>' . $información.'</td>';  
				$monto = number_format ($key->monto, 2,',','.');
				$html.= '      <td style="text-align:right">' . $monto.'</td>';


                $impuesto = $key->monto * ($key->impuesto / 100);
                $impuestoF = number_format ($impuesto, 2,',','.');
                $html.= '      <td style="text-align:right">' . $impuestoF.'</td>';


                $total = $key->monto + $impuesto;
                $total = number_format ($total, 2,',','.');

                $html.= '      <td style="text-align:right">' . $total.'</td>';



				$html.= '      <td>';		

                    /*		
                    if($this->clssession->accion(10,1)==1){ 
						$html.= '<a href="javascript:void(0)" onclick="javascript:ver_form('.$key->id.')">';
						$html.= '<img src="'.$lectura.'" style="width:25px; height:25px" title="Detalle Mesa" alt="Detalle Mesa"></a>';
					}
                    */

                    if($this->clssession->accion(10,3)==1){ 
						$html.= '&nbsp;&nbsp<a href="javascript:void(0)" onclick="javascript:cobrar_form('.$key->id.')">';
						$html.= '<img src="'.$cobrar.'" style="width:25px; height:25px" title="Cobrar Despacho" 
							alt="Cobrar"></a>';
					}


			}
					
				$html.= '       </td>';				
				$html.= '</tr>';
		}
		$html.= '    </tbody>';
		$html.= '</table>';
		return $html;    				
	}







function ajax_cobrar($id){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){


    //obtener datos configracion para obtener precio del dolar
    $rowConfiguracion = $this->Cf_configuracion_mdl->obtDatosSucursal();    
    $tasaDolar = $rowConfiguracion->dolar;
    $tasaDolarF = number_format ($rowConfiguracion->dolar, 2,',','.');

    //$impuesto = $rowConfiguracion->impuesto;
    //$impuestoF = number_format ($rowConfiguracion->impuesto, 2,',','.');


    //Obtener datos del despacho
    $row = $this->Dp_despacho_mdl->infDespacho($id);
    //formatear monto a pagar

    $monto  = $row->monto;
    $montoF  = number_format ($row->monto, 2,',','.');

    $impuesto  = $row->impuesto;
    $impuestoF  = number_format ($row->impuesto, 2,',','.');

    $mtoImpuesto  = $monto * ($impuesto / 100);
    $mtoImpuestoF  = number_format ($mtoImpuesto, 2,',','.');

    $total = $monto + $mtoImpuesto;
    $totalF  = number_format ($total, 2,',','.');

    $montoDolar = $total / $tasaDolar;       
    $montoDolarF = number_format ($montoDolar, 2,',','.');



    //configurar cliente
    if($row->id_tipo_despacho==1){
        $htmlC ='<label style="padding-top: 0px; margin-top: 0px;margin-left:0px;">';
        $htmlC.='<span style="color:#0000ff; font-size:14px; font-weight:600">SALA:&nbsp;&nbsp; </span>';
        $htmlC.='</label>';
        $htmlC.='<span style="padding-top: 0px; margin-top: 5px;margin-left:0px; font-size:14px; font-weight:600">'.$row->sala.'</span><br>';
        $htmlC.='<label style="padding-top: 0px; margin-top: 0px;margin-left:0px;">';
        $htmlC.='<span style="color:#0000ff; font-size:14px; font-weight:600">MESA:&nbsp;&nbsp; </span>';
        $htmlC.='</label>';
        $htmlC.='<span style="padding-top: 0px; margin-top: 5px;margin-left:0px; font-size:14px; font-weight:600">'.$row->mesa.'</span><br>';
    }else{
        $htmlC ='<label style="padding-top: 0px; margin-top: 0px;margin-left:0px;">';
        $htmlC.='<span style="color:#0000ff; font-size:14px; font-weight:600">CLIENTE:&nbsp;&nbsp; </span>';
        $htmlC.='</label>';
        $htmlC.='<span style="padding-top: 0px; margin-top: 5px;margin-left:0px; font-size:14px; font-weight:600">'.$row->nombre_cliente.'</span><br>';
    }

    $data = array(
    	'cliente'=>$htmlC,
    	//'mesonero'=>$mesonero,
        'idDespacho'=>$id,
        'idTipoDespacho'=>$row->id_tipo_despacho,
    	'id'=>$row->id,
    	'montoF'=>$montoF,
    	'monto'=>$row->monto,

        'impuestoF'=>$impuestoF,
        'impuesto'=>$impuesto,

        'mtoImpuestoF'=>$mtoImpuestoF,
        'mtoImpuesto'=>$mtoImpuesto,

        'totalF'=>$totalF,
        'total'=>$total,

    	'tasaDolar'=>$tasaDolar,
        'tasaDolarF'=>$tasaDolar,
        'montoDolar'=> $montoDolar,         
        'montoDolarF'=> $montoDolarF,         
    );
    echo json_encode($data);
}//fin val session
}

function ajax_guardar_cobro(){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){



//moneda 
$moneda = str_replace('.','',$this->input->post('m_moneda'));
$moneda = str_replace(',','.',$moneda); 
//dolar
$dolar = str_replace('.','',$this->input->post('m_dolar'));
$dolar = str_replace(',','.',$dolar); 

//debito
$debito = str_replace('.','',$this->input->post('m_debito'));
$debito = str_replace(',','.',$debito); 

//debito
$credito = str_replace('.','',$this->input->post('m_credito'));
$credito = str_replace(',','.',$credito); 

//transferencia
$transferencia = str_replace('.','',$this->input->post('m_transferencia'));
$transferencia = str_replace(',','.',$transferencia); 




$mtoCobrar = $this->input->post('monto');

//si hay vuelto debirtar caja
$tipoVuelto = $this->input->post('tipoVuelto');
$vueltoMoneda=0;
$vueltoDolar=0;

if($tipoVuelto > 0){
    if($tipoVuelto == 1){
        $vueltoMoneda=$this->input->post('totalVuelto');
    }else{
        $vueltoDolar=$this->input->post('totalVueltoDolar');
    }
}

$idVenta= 1;
//guardar movimiento de caja

$mtoTotal = $mtoCobrar;
$mtoDescuento = 0;

//descuento
$porcDescuento = str_replace('.','',$this->input->post('m_descuento'));
$porcDescuento = str_replace(',','.',$porcDescuento); 

if($porcDescuento>0.00){
    $mtoDescuento = ($mtoCobrar * $porcDescuento) / 100; 
    $mtoTotal = $mtoCobrar - $mtoDescuento;
}

$totalCobro = $mtoTotal;
$mtoImpuesto = 0.00;

$porcImpuesto = $this->input->post('tasaImpuesto');
if($porcImpuesto > 0.00){
    $mtoImpuesto = $mtoTotal * ($porcImpuesto / 100);
    $TotalCobro=$totalCobro + $mtoImpuesto;

}    

$idTipoDespacho = $this->input->post('idTipoDespacho');


$data = array(
	'id_venta'=>$idVenta,
	'id_caja'=>$this->session->userdata('idCaja'),
    'monto_pedido'=>$mtoCobrar,
	'monto_cobrar'=>$mtoTotal,
    'id_tipo_despacho'=>$idTipoDespacho,
    'porc_descuento'=>$porcDescuento,
	'descuento'=>$mtoDescuento,
    'total_cobrar'=>$TotalCobro,
    'porc_impuesto'=>$porcImpuesto,
    'impuesto'=>$mtoImpuesto,


    'id_empresa'=>$this->session->userdata('idEmpresa'),
    'id_sucursal'=>$this->session->userdata('idSucursal'),
	'id_create'=>$this->session->userdata('idUsuario'),
);

$idCobro = $this->Cj_cobroCaja_mdl->guardarCobro($data);

//guardar pagos
if($moneda > 0.00){
    $data = array(
    	'id_cobrar'=>$idCobro,
        'id_caja'=>$this->session->userdata('idCaja'),
    	'id_tipo_dinero'=>1,
    	'monto_peso'=>$moneda,
    	'id_create'=>$this->session->userdata('idUsuario'),
    	'id_tipo_despacho'=>$idTipoDespacho,
	  );    
	$this->Cj_cobroCaja_mdl->guardarCobroDinero($data);
}

//guardar pagos
if($dolar > 0.00){
    $data = array(
    	'id_cobrar'=>$idCobro,
        'id_caja'=>$this->session->userdata('idCaja'),
       	'id_tipo_dinero'=>2,
    	'monto_dolar'=>$dolar,
    	'id_tipo_despacho'=>$idTipoDespacho,
    	'id_create'=>$this->session->userdata('idUsuario'),
	  );    
	$this->Cj_cobroCaja_mdl->guardarCobroDinero($data);
}

//guardar pagos
if($debito > 0.00){
    $data = array(
        'id_cobrar'=>$idCobro,
        'id_caja'=>$this->session->userdata('idCaja'),
        'id_tipo_dinero'=>3,
        'id_tipo_despacho'=>$idTipoDespacho,
        'monto_peso'=>$debito,
        'id_create'=>$this->session->userdata('idUsuario'),
      );    
    $this->Cj_cobroCaja_mdl->guardarCobroDinero($data);
}

//guardar pagos
if($credito > 0.00){
    $data = array(
        'id_cobrar'=>$idCobro,
        'id_caja'=>$this->session->userdata('idCaja'),
        'id_tipo_dinero'=>4,
        'id_tipo_despacho'=>$idTipoDespacho,
        'monto_peso'=>$credito,
        'id_create'=>$this->session->userdata('idUsuario'),
      );    
    $this->Cj_cobroCaja_mdl->guardarCobroDinero($data);
}


//transferencia
if($transferencia > 0.00){
    $data = array(
        'id_cobrar'=>$idCobro,
        'id_caja'=>$this->session->userdata('idCaja'),
        'id_tipo_dinero'=>6,
        'id_tipo_despacho'=>$idTipoDespacho,
        'monto_peso'=>$transferencia,
        'id_create'=>$this->session->userdata('idUsuario'),
      );    
    $this->Cj_cobroCaja_mdl->guardarCobroDinero($data);
}

//guardar pagos vuelto

if($tipoVuelto > 0){
    $data = array(
        'id_cobrar'=>$idCobro,
        'id_caja'=>$this->session->userdata('idCaja'),
        'id_tipo_dinero'=>5,
        'id_tipo_despacho'=>$idTipoDespacho,
        'monto_peso'=>$vueltoMoneda,
        'monto_dolar'=>$vueltoDolar,
        'id_create'=>$this->session->userdata('idUsuario'),
      );    
    $this->Cj_cobroCaja_mdl->guardarCobroDinero($data);
}

/// Guardo Factura


//modifica estado del despacho a a tendido
$idDespacho = $this->input->post('idDespacho');
$cobrado=1;
$estadoDespacho=5;
$despachado=1;

if($idTipoDespacho!=1){
    $estadoDespacho=2;
    $despachado=0;
}

$estadoDespacho=5;
$despachado=1;

if($idTipoDespacho==3){
    $estadoDespacho=2;
    $despachado=0;
}

$data = array(
    'cobrado'=>1,
    'id_estado_despacho'=>$estadoDespacho,
    'despachado'=>$despachado,
    'id_update'=>$this->session->userdata('idUsuario'),
    'date_update'=>date('Y-m-d H:i:s'),
);    
$resultado = $this->Dp_despacho_mdl->modificarEstadoDespacho($idDespacho, $data);

//Generar venta
$resultdo = $this->generarVenta($idCobro, $idDespacho);


//generar datatable
$row = $this->Dp_despacho_mdl->obt_despachoCobro();
$html= $this->generarDatatable($row);

$data=array(
    "registro"=>$html,
    "status"=>0);
echo json_encode($data);

}//fin val session
}



function generarVenta($idCobro, $idDespacho){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){



$rowCobro = $this->Cj_cobroCaja_mdl->obtCobroInd($idCobro);

//generar_nroFactura
$rowCorrelativo = $this->Cf_correlativo_mdl->obtCorrelativo();
$nroDocumento = $rowCorrelativo->nro_factura + 1;

//actualizar el correlativo venta
$data=array(
    'nro_factura' => $nroDocumento
);
$this->Cf_correlativo_mdl->actCorrelativo($data);


$data = array(
    'id_cobro'=>$idCobro,
    //'documento'=>str_pad($nroDocumento, 6, "0", STR_PAD_LEFT),
    'documento'=>$nroDocumento,
    'monto_cobrar'=>$rowCobro->monto_pedido,
    'descuento'=>$rowCobro->descuento,
    'monto_venta'=>$rowCobro->monto_cobrar,
    'porcentaje_impuesto'=>$rowCobro->porc_impuesto,
    'impuesto'=>$rowCobro->impuesto,
    'total_venta'=>$rowCobro->total_cobrar,
    'id_empresa'=>$this->session->userdata('idEmpresa'),
    'id_sucursal'=>$this->session->userdata('idSucursal'),
    'id_create'=>$this->session->userdata('idUsuario')
);

$this->load->model('Vt_venta_mdl');
$idVenta = $this->Vt_venta_mdl->guardar_add($data);

//generar detalle factura

$rowDD_acum = $this->Dp_despacho_mdl->DetalleDespacho_acum($idDespacho);

foreach ($rowDD_acum as $key) {
    $rowDetalle = $this->Dp_despacho_mdl->DetalleDespacho_ind($key->id_despacho, $key->id_alimento, $key->id_presentacion, $key->id_envoltura);
    
    $envoltura="";

    $descripcion = trim($rowDetalle->alimento);
    if($rowDetalle->id_envoltura > 0){
        $descripcion." / Envoltura:" . trim($rowDetalle->envoltura); 
    }
    if($rowDetalle->id_categoria_menu==1){   
		$descripcion.= " / " . $rowDetalle->nro_pieza . " " . trim($rowDetalle->unidad_medida);
	}	

    $data = array(
        'id_venta'=>$idVenta,
        'descripcion'=>$descripcion,
        'precio_unitario'=>$rowDetalle->precio_unitario,
        'cantidad'=>$key->cantidad,
        'precio'=>$key->precio,
        'id_alimento'=>$key->id_alimento,
        'id_presentacion'=>$key->id_presentacion,
        'id_envoltura'=>$key->id_envoltura,
        'id_create'=>$this->session->userdata('idUsuario')
    );
    $resultado = $this->Vt_venta_mdl->guardarDetalle($data);
}
return true;
}//fin val session
}



// ************************************************************************

}
