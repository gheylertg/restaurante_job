<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cj_movimientoCaja extends CI_Controller {

	
	public function __construct() {
         parent::__construct();
         $this->load->model(array('Cj_movimientoCaja_mdl'));
         $this->load->library(array('Clssession')); 
    }



	public function index()	{
    //Validar session usuario
    $valSession = $this->clssession->valSession();
    if($valSession==true){
	   //Validar apertura de caja
        $accion=1;
        $this->load->model('Cj_aperturaCaja_mdl');
        $idUsuario = $this->session->userdata('idUsuario');
        $idEmpresa = $this->session->userdata('idEmpresa');
        $idSucursal = $this->session->userdata('idSucursal');        
		$status = $this->Cj_aperturaCaja_mdl->val_apertura($idUsuario, $idEmpresa, $idSucursal);
        if($status==0){       
            $dataSession = array(
                'idCaja'=>0
            );
            $this->session->set_userdata($dataSession);
			$data=array(
				'accion'=>2
			);
	    	$this->load->view('header');
			$this->load->view('menu');
			$this->load->view('footer/footer', $data);
			$this->load->view('caja/movimientoCaja/footer_movimientoCaja', $data);
        }else{  
	        //accion 0 vista sin datatable, accion 1  activar datatable 
			$data=array(
				'accion'=>1
			);
	    	$this->load->view('header');
			$this->load->view('menu');
			$this->load->view('caja/movimientoCaja/act_movimientoCaja',$data);
			$this->load->view('footer/footer', $data);
			$this->load->view('caja/movimientoCaja/footer_movimientoCaja', $data);
			//$this->load->view('footer/lib_numerica');
		}	
	}//fin val session	
	}


	function ajax_datatable($id){
    //Validar session usuario
    $valSession = $this->clssession->valSession();
    if($valSession==true){
		$row = $this->Cj_movimientoCaja_mdl->obt_dataTable();
		$html= $this->generarDatatable($row);
		$nombreEmpresa = $this->session->userdata('datosUES');
		$data = array(
		    "registro"=>$html,
		    "nombreEmpresa"=>$nombreEmpresa
		);
        echo json_encode($data);	
    }//fin val session
	}	


	function generarDatatable($row){
		//obtener la información de la tabla seleccionada
		$modificar = base_url() . "assets/images/modificar02.jpeg";
		$eliminar = base_url() . "assets/images/eliminar.jpeg";
		$reactivar = base_url() . "assets/images/reactivar.jpeg";
		$lectura = base_url() . "assets/images/ver.png";
		
        $html = '<table id="basic-datatables" class="display table table-striped table-hover">';
        $html.= '    <thead>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="15%">Fecha/Hora</th>';
        $html.= '           <th width="12%">Tipo Operación</th>';
        $html.= '           <th width="20%">Tipo Movimiento</th>';
        $html.= '           <th width="10%">Monto</th>';
        $html.= '           <th width="23%">Observación / Nota</th>';
        $html.= '           <th width="10%">Estado</th>';
        $html.= '           <th width="10%">Acción</th>';
        $html.= '        </tr>';
        $html.= '    </thead>';
        $html.= '    <tfoot>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="15%">Fecha/Hora</th>';
        $html.= '           <th width="12%">Tipo Operación</th>';
        $html.= '           <th width="20%">Tipo Movimiento</th>';
        $html.= '           <th width="10%">Monto</th>';
        $html.= '           <th width="23%">Observación / Nota</th>';
        $html.= '           <th width="10%">Estado</th>';
        $html.= '           <th width="10%">Acción</th>';
        $html.= '        </tr>';
        $html.= '    </tfoot>';
        $html.= '    <tbody>';
        if($row==false){
			$html.= '      <tr>';
			$html.= '          <td></td><td></td><td></td>
							   <td></td><td></td><td></td>';
			$html.= '      </tr>';
		}else{
		    foreach($row as $key){ 
				$html.= '  <tr>';
				$html.= '      <td>' . $key->fecha.'</td>';
				$tipoOperacion="CRÉDITO";
                if($key->id_tipo_operacion==2){
                    $tipoOperacion="DÉBITO"; 
                } 
				$html.= '      <td>' . $tipoOperacion .'</td>';
				$html.= '      <td>' . $key->movimiento.'</td>';
				//$html.= '      <td>' . $key->dinero.'</td>';
				$monto = str_replace(".",",",$key->monto );
				$html.= '      <td style="text-align:right">' . $monto.'</td>';
                $html.= '      <td>' . $key->observacion.'</td>'; 

                if($key->activo==1){
                   $estado = "Activo";
                }else{
                   $estado = "Reversado";	
                } 
                $html.= '      <td>' . $estado.'</td>';

				$html.= '      <td>';				
				if($key->activo==1){



                    if($this->clssession->accion(10,3)==1){ 
                        if($key->id_tipo_movimiento != 1){
							$html.= '<a href="javascript:void(0)" onclick="javascript:modificar_form('.$key->id.')">';
							$html.= '<img src="'.$modificar.'" style="width:25px; height:25px" 
								alt="Modificar" title="Modificar Movimiento"></a>';
						}else{
							$html.= '<a href="javascript:void(0)" onclick="javascript:modApertura_form('.$key->id.')">';
							$html.= '<img src="'.$modificar.'" style="width:25px; height:25px" 
								alt="Modificar" title="Modificar Apertura"></a>';
						}		
					}

					if($key->id_tipo_movimiento != 1){

							
	                    if($this->clssession->accion(10,4)==1){ 					
							$html.= '&nbsp;<a href="javascript:void(0)" onclick="javascript:reverso_form('.$key->id.')">';
							$html.= '<img src="'.$reactivar.'" style="width:25px; height:25px" alt="Reverso" title="Reversar Movimiento"></a>';
	                    }
	                }    
				}
					
				$html.= '       </td>';				
				$html.= '</tr>';
			}
		}	
		$html.= '    </tbody>';
		$html.= '</table>';
		return $html;    				
	}



function ajax_guardar_movimientoCaja(){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){

    $status = 0;

    $monto = $this->input->post('m_monto');
	$monto = str_replace('.','',$monto);
	$monto = str_replace(',','.',$monto);  

	$data = array(
        'id_caja'=>$this->session->userdata('idCaja'),
        'id_tipo_operacion'=>$this->input->post('id_cbo_tipoOperacion'),
        'id_tipo_movimiento'=>$this->input->post('id_cbo_tipoMovimiento'),
        //'id_tipo_dinero'=>$this->input->post('id_cbo_tipoDinero'),
        'observacion'=>$this->input->post('m_observacion'),
        'monto'=>$monto,
  	    'id_create'=>$this->session->userdata('idUsuario'),
	);
	$respuesta = $this->Cj_movimientoCaja_mdl->guardar_add($data);


    $row = $this->Cj_movimientoCaja_mdl->obt_dataTable();
	$html= $this->generarDatatable($row);
	$data = array(
	    "registro"=>$html,
	    "status"=>$status);
    echo json_encode($data);
}//fin val session	
}
	

function ajax_modificar($id){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){

    $row = $this->Cj_movimientoCaja_mdl->obtModificar($id);

    $monto = $row->monto;
    $monto = str_replace(".",",",$monto);

    $data = array(
    	'id'=>$row->id,
    	'id_tipo_operacion'=>$row->id_tipo_operacion,
    	'id_tipo_movimiento'=>$row->id_tipo_movimiento,
    	//'id_tipo_dinero'=>$row->id_tipo_dinero,
    	'monto'=>$monto,
    	'observacion'=>$row->observacion,
    );

    echo json_encode($data);
}//fin val session
}


function ajax_ver($id){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){

    $row = $this->Cj_movimientoCaja_mdl->obt_Ver($id);

    $monto = $row->monto;
    $monto = str_replace(".",",",$monto);

    $operacion = "CRÉDITO";
    if($row->id_tipo_operacion==2){
       $operacion = "DÉBITO";
    } 


    $data = array(
    	'id'=>$row->id,
    	'tipo_operacion'=>$operacion,
    	'tipo_movimiento'=>$row->movimiento,
    	//'tipo_dinero'=>$row->dinero,
    	'monto'=>$monto,
    	'reverso'=>$row->reverso,
    	'motivo_reverso'=>$row->motivo_reverso,
    	'fecha_reverso'=>$row->fecha_reverso,


    	'observacion'=>$row->observacion,
    );
    echo json_encode($data);
}//fin val session
}



function ajax_guardar_mod(){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){


	$id = $this->input->post('id_mod');
    $monto = $this->input->post('m_monto');
	$monto = str_replace('.','',$monto);
	$monto = str_replace(',','.',$monto); 

	$status=0;
    
	$data = array(
        'id_tipo_operacion'=>$this->input->post('id_cbo_tipoOperacion'),
        'id_tipo_movimiento'=>$this->input->post('id_cbo_tipoMovimiento'),
        //'id_tipo_dinero'=>$this->input->post('id_cbo_tipoDinero'),
        'monto'=>$monto,
        'observacion'=>$this->input->post('m_observacion'),
  	    'id_update'=>$this->session->userdata('idUsuario'),
  	    'date_update'=>date('Y-m-d H:i:s')        
	);
	$respuesta = $this->Cj_movimientoCaja_mdl->guardar_mod($id,$data);

	$row = $this->Cj_movimientoCaja_mdl->obt_dataTable();
	$html= $this->generarDatatable($row);
	$data = array(
	    "registro"=>$html,
	    "status"=>$status
	);
    echo json_encode($data);
}//fin val session	
}


function ajax_guardarApertura_mod(){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){
    $status = 0;

	$id = $this->input->post('id_mod_apertura');
    $monto = $this->input->post('aper_monto');
	$monto = str_replace('.','',$monto);
	$monto = str_replace(',','.',$monto); 


	$data = array(
        'monto'=>$monto,
        'observacion'=>$this->input->post('aper_observacion'),
  	    'id_update'=>$this->session->userdata('idUsuario'),
  	    'date_update'=>date('Y-m-d H:i:s')        
	);
	$respuesta = $this->Cj_movimientoCaja_mdl->guardar_mod($id,$data);

	$row = $this->Cj_movimientoCaja_mdl->obt_dataTable();
	$html= $this->generarDatatable($row);
	$data = array(
	    "registro"=>$html,
	    "status"=>$status
	);
    echo json_encode($data);
}//fin val session
}	

function ajax_guardar_reverso(){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){
    $status = 0;
    $id = $this->input->post('id_mod_reverso');
	$data = array(
        'reverso'=>1,
        'motivo_reverso'=>$this->input->post('r_motivo'),
        'fecha_reverso'=>date('Y-m-d H:i:s'),        
        'activo'=>0,
  	    'id_update'=>$this->session->userdata('idUsuario'),
  	    'date_update'=>date('Y-m-d H:i:s')        
	);

	$respuesta = $this->Cj_movimientoCaja_mdl->guardar_mod($id,$data);

	$row = $this->Cj_movimientoCaja_mdl->obt_dataTable();
	$html= $this->generarDatatable($row);
	$data = array(
	    "registro"=>$html,
	    "status"=>$status
	);
    echo json_encode($data);
}//fin val session	
}




/* **************************************************************** */


	

function ajax_desactivar($id){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){
	$data = array(
  	    'activo'=>0,
  	    'id_update'=>$this->session->userdata('idUsuario'),
  	    'date_update'=>date('Y-m-d H:i:s')
    );


	$respuesta = $this->Cj_movimientoCaja_mdl->desactivar($id,$data);
	$row = $this->Cj_movimientoCaja_mdl->obt_dataTable();
	$html= $this->generarDatatable($row);
	$data = array(
	    "registro"=>$html);
    echo json_encode($data);
}//fin val session    
}


function ajax_reactivar($id){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){

	$data = array(
  	    'activo'=>1,
  	    'id_update'=>$this->session->userdata('idUsuario'),
  	    'date_update'=>date('Y-m-d H:i:s')
    );


	$respuesta = $this->Cj_movimientoCaja_mdl->desactivar($id,$data);
	
	$row = $this->Cj_movimientoCaja_mdl->obt_dataTable();
	$html= $this->generarDatatable($row);
	$data = array(
	    "registro"=>$html);
    echo json_encode($data);

}//fin val session
}




	function ajax_validar_duplicidad($nombre){
		$nro = $this->Cj_movimientoCaja->valNombre($nombre);
		$status = 0;
		if($nro>0){
		    $status = 1;	
		}

		return $status;
	}
	

}
