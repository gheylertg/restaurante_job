<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Co_contenedor extends CI_Controller {


	
	public function __construct() {
         parent::__construct();
         $this->load->model(array('Co_contenedor_mdl'));
         $this->load->library(array('Clssession')); 
    }


	public function index()
	{
    //Validar session usuario
    $valSession = $this->clssession->valSession();
    if($valSession==true){		
		
		
        //accion 0 vista sin datatable, accion 1  activar datatable 
		$data=array(
			'accion'=>1
		);
	    $this->load->view('header');
		$this->load->view('menu');
		$this->load->view('contenedor/contenedor/actContenedor',$data);
		$this->load->view('footer/footer', $data);
		$this->load->view('contenedor/contenedor/footer_contenedor', $data);
		
	}//fin de session	
	}
	
	function ajax_datatable($id){
		$row = $this->Co_contenedor_mdl->obt_dataTable();
		$html= $this->generarDatatable($row);
		$data = array(
		    "registro"=>$html);
        echo json_encode($data);	

	}	

	function generarDatatable($row){
		//obtener la información de la tabla seleccionada
		$modificar = base_url() . "assets/images/modificar02.jpeg";
		$eliminar = base_url() . "assets/images/eliminar.jpeg";
		$reactivar = base_url() . "assets/images/reactivar.jpeg";
		$lectura = base_url() . "assets/images/ver.png";
		
        $html = '<table id="basic-datatables" class="display table table-striped table-hover">';
        $html.= '    <thead>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="70%">Sabor Sushi</th>';
        $html.= '           <th width="15%">Cantidad Disponible</th>';
        $html.= '           <th width="15%">Acción</th>';
        $html.= '        </tr>';
        $html.= '    </thead>';
        $html.= '    <tfoot>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="65%">Sabor Sushi</th>';
        $html.= '           <th width="15%">Cantidad Disponible</th>';
        $html.= '           <th width="20%">Acción</th>';
        $html.= '        </tr>';
        $html.= '    </tfoot>';
        $html.= '    <tbody>';
        if($row==false){
			$html.= '      <tr>';
			$html.= '          <td></td><td></td><td></td>';
			$html.= '      </tr>';
		}else{
		    foreach($row as $key){ 
				$html.= '  <tr>';
				$html.= '      <td>' . $key->sabor.'</td>';	
				if($key->cantidad > 0){
					$html.= '      <td>' . $key->cantidad.'</td>';
				}else{
					$html.= '      <td style="background-color:red; color:white">' . $key->cantidad.'</td>';
				}	
				
				
				$html.= '      <td>';
				$html.= ' <a href="javascript:void(0)" class="btn btn-primary btn-sm item_edit2" onclick="javascript:actContenedor('.$key->id.')" ';
				$html.= ' style="border-radius: 15px; font-size:16px; font-weight:600; height:40px;">&nbsp;&nbsp;&nbsp;Actualizar Cantidad&nbsp;&nbsp;&nbsp;</a>';
				$html.= '      </td>';
				$html.= '</tr>';
			}
		}	
		$html.= '    </tbody>';
		$html.= '</table>';
		return $html;    				
	}


function ajax_resetear_contenedores(){
    //Validar session usuario
    $valSession = $this->clssession->valSession();
    if($valSession==true){		
	
	
		$rowContenedor = $this->Co_contenedor_mdl->obt_dataTable();
		foreach($rowContenedor as $key){
			$id = $key->id;
			$data = array(
				'cantidad' => 0,
				'id_update'=>$this->session->userdata('idUsuario'),
				'date_update'=>date('Y-m-d H:i:s')          
			);
			$this->Co_contenedor_mdl->resetearContenedor($id, $data);
		}	
		$rowContenedor = $this->Co_contenedor_mdl->obt_dataTable();
		$html= $this->generarDatatable($rowContenedor);
		$data = array(
			"registro"=>$html
		);
		echo json_encode($data);	
	} //fin val session	
}


function ajax_modificar($id){
    //Validar session usuario
    $valSession = $this->clssession->valSession();
    if($valSession==true){		
	
		$row = $this->Co_contenedor_mdl->obtModificar($id);
		$data = array(
			'id'=>$row->id,
			'cantidad'=>$row->cantidad,
		);	
		echo json_encode($data);
	} //fin val session	
}


function ajax_guardar_mod(){
	    //Validar session usuario
    $valSession = $this->clssession->valSession();
    if($valSession==true){		

		$id = $this->input->post('idContenedor');
		$data = array(
			'cantidad'=>$this->input->post('m_cantidad'),
			'id_update'=>$this->session->userdata('idUsuario'),
			'date_update'=>date('Y-m-d H:i:s')          
		);    
		$this->Co_contenedor_mdl->guardar_mod($id,$data);

		$rowContenedor = $this->Co_contenedor_mdl->obt_dataTable();
		$html= $this->generarDatatable($rowContenedor);
		$data = array(
			"registro"=>$html,
			'status'=>0
		);
		echo json_encode($data);	
	} //fin val session	

}




	

}  //fin del controller
