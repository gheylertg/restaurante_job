<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Combo_ctrl extends CI_Controller {
	
	

	
	public function __construct() {
         parent::__construct();
         //$this->load->model(array('configmodel', 'usuariomodel', 'menumodel'));
    }


	public function index()
	{


	}	
	

	public function ajax_combo_modulo($id){


        $this->load->model('Combo_mdl');


 	    $recordset = $this->Combo_mdl->combo_modulo();


        $html = '<Select name="id_cbo_modulo" id="id_cbo_modulo" class="form-control" >';	
        if($id==0){

	        $html.= '<option value = "0" selected>--Seleccione Modulo--</option>';
	    }else{
		    $html.= '<option value = "0">--Seleccione Modulo--</option>';
	    }	
      
        foreach ($recordset as $registro){
		    if($registro->id == $id){
			   $html.= '<option selected value="' . $registro->id.'">'.$registro->nombre.'</option>';
		    }else{
			   $html.= '<option value="' . $registro->id.'">'. $registro->nombre.'</option>';			  
		    }	
	    }	  	 			 
        $html.= '</select>';	

        //die($html);


        $data = array(
           'combo'=>$html
        );
        echo json_encode($data);	
	}
    //------- Fin de combo_modelo()



	public function ajax_combo_rol($id){


        $this->load->model('Combo_mdl');


 	    $recordset = $this->Combo_mdl->combo_rol();


        $html = '<Select name="id_cbo_rol" id="id_cbo_rol" class="form-control" >';	
        if($id==0){

	        $html.= '<option value = "0" selected>--Seleccione Rol del Usuario--</option>';
	    }else{
		    $html.= '<option value = "0">--Seleccione Rol del USuario--</option>';
	    }	
      
        foreach ($recordset as $registro){
		    if($registro->id == $id){
			   $html.= '<option selected value="' . $registro->id.'">'.$registro->nombre.'</option>';
		    }else{
			   $html.= '<option value="' . $registro->id.'">'. $registro->nombre.'</option>';			  
		    }	
	    }	  	 			 
        $html.= '</select>';	

        $data = array(
           'combo'=>$html
        );
        echo json_encode($data);	
	}
    //------- Fin de combo_modelo()





	public function ajax_combo_rolModulo($id){
        $this->load->model('Combo_mdl');
 	    $recordset = $this->Combo_mdl->combo_rolModulo($id);

        $html = '<Select name="id_cbo_rolModulo" id="id_cbo_rolModulo" class="form-control" >';	
        $html.= '<option value = "0" selected>--Seleccione Modulo--</option>';
        foreach ($recordset as $registro){
		   $html.= '<option value="' . $registro->id.'">'. $registro->nombre.'</option>';			  
	    }	  	 			 
        $html.= '</select>';	

        $data = array(
           'combo'=>$html
        );
        echo json_encode($data);	
	}
    //------- Fin de combo_modelo()



	public function ajax_combo_empresaUsuario($id){
		 
        $this->load->model('Combo_mdl');
 	    $recordset = $this->Combo_mdl->combo_empresaUsuario($id);
	    

        $html = '<Select name="id_cbo_empresas" id="id_cbo_empresas" class="form-control" onChange="cbo_sel_sucursal()">';	
	        $html.= '<option value = "0" selected>--Seleccione Empresa--</option>';
      
        foreach ($recordset as $registro){
			   $html.= '<option value="' . $registro->id.'">'. $registro->nombre.'</option>';			  

	    }	  	 			 
        $html.= '</select>';	

        $data = array(
           'combo'=>$html
        );
        echo json_encode($data);	
	}
    //------- Fin de combo_modelo()



	public function ajax_combo_sexo($id){

        //$this->load->model('Combo_mdl');
 	    //$recordset = $this->Combo_mdl->combo_modelo();

        $html = '<Select name="id_cbo_sexo" id="id_cbo_sexo" class="form-control" >';	
        if($id=="0"){
	        $html.= '<option value = "0" selected>--Seleccione Sexo--</option>';
	    }else{
		    $html.= '<option value = "0">--Seleccione Sexo--</option>';
	    }	
	    
	    if($id=="F"){
		   $html.= '<option selected value="F">Femenino</option>';
	    }else{
		   $html.= '<option value="F">Femenino</option>';
		}
		
	    if($id=="M"){
		   $html.= '<option selected value="M">Masculino</option>';
	    }else{
		   $html.= '<option value="M">Masculino</option>';
		}
        $html.= '</select>';	

        $data = array(
           'combo'=>$html
        );
        echo json_encode($data);	
	}
    //------- Fin de combo_modelo()



	public function ajax_combo_debitoCredito($id){

        //$this->load->model('Combo_mdl');
 	    //$recordset = $this->Combo_mdl->combo_modelo();

        $html = '<Select name="id_cbo_debitoCredito" id="id_cbo_debitoCredito" class="form-control" >';	
        if($id=="0"){
	        $html.= '<option value = "0" selected>--Seleccione Tipo Operción--</option>';
	    }else{
		    $html.= '<option value = "0">--Seleccione Tipo Operción--</option>';
	    }	
	    
	    if($id=="1"){
		   $html.= '<option selected value="1">CRÉDITO</option>';
	    }else{
		   $html.= '<option value="1">CRÉDITO</option>';
		}


	    if($id=="2"){
		   $html.= '<option selected value="2">DÉBITO</option>';
	    }else{
		   $html.= '<option value="2">DÉBITO</option>';
		}
		
        $html.= '</select>';	

        $data = array(
           'combo'=>$html
        );
        echo json_encode($data);	
	}
    //------- Fin de combo_modelo()



	public function ajax_combo_usuarioEmpresa($id){
        $this->load->model('Combo_mdl');
 	    $recordset = $this->Combo_mdl->combo_moduloUsuario($id);
        $html = '<Select name="id_cbo_moduloUsuario" id="id_cbo_moduloUsuario" class="form-control" >';	


        $html.= '<option value = "0" selected>--Seleccione Modulo--</option>';

      
        foreach ($recordset as $registro){
			   $html.= '<option value="' . $registro->id.'">'. $registro->nombre.'</option>';			  

	    }	  	 			 
        $html.= '</select>';	

        $data = array(
           'combo'=>$html
        );
        echo json_encode($data);	
	}
    //------- Fin de combo_modelo()


   // categoria de producto   
	public function ajax_combo_categoriaProducto($id){
        $this->load->model('Combo_mdl');
 	    $recordset = $this->Combo_mdl->combo_categoriaProducto();

        $html = '<Select name="id_cbo_categoriaProducto" id="id_cbo_categoriaProducto" class="form-control" >';	
        if($id==0){

	        $html.= '<option value = "0" selected>--Seleccione Categoria--</option>';
	    }else{
		    $html.= '<option value = "0">--Seleccione Categoria--</option>';
	    }	
      
        foreach ($recordset as $registro){
		    if($registro->id == $id){
			   $html.= '<option selected value="' . $registro->id.'">'.$registro->nombre.'</option>';
		    }else{
			   $html.= '<option value="' . $registro->id.'">'. $registro->nombre.'</option>';			  
		    }	
	    }	  	 			 
        $html.= '</select>';	
        $data = array(
           'combo'=>$html
        );
        echo json_encode($data);	
	}
    //------- Fin de combo_categoria producto()


   // unidad medida   
	public function ajax_combo_unidadMedida($id){
        $this->load->model('Combo_mdl');
 	    $recordset = $this->Combo_mdl->combo_unidadMedida();

        $html = '<Select name="id_cbo_unidadMedida" id="id_cbo_unidadMedida" class="form-control" >';	
        if($id==0){

	        $html.= '<option value = "0" selected>--Seleccione Unidad Medida--</option>';
	    }else{
		    $html.= '<option value = "0">--Seleccione Unidad Medida--</option>';
	    }	
      
        foreach ($recordset as $registro){
		    if($registro->id == $id){
			   $html.= '<option selected value="' . $registro->id.'">'.$registro->nombre.'</option>';
		    }else{
			   $html.= '<option value="' . $registro->id.'">'. $registro->nombre.'</option>';			  
		    }	
	    }	  	 			 
        $html.= '</select>';	
        $data = array(
           'combo'=>$html
        );
        echo json_encode($data);	
	}
    //------- Fin de combo_unidad medida()


   // unidad medida para presentacion alimento  
	public function ajax_combo_unidadMedida_presentacion($id){
        $this->load->model('Combo_mdl');
 	    $recordset = $this->Combo_mdl->combo_unidadMedida_presentacion();

        $html = '<Select name="id_cbo_unidadMedida" id="id_cbo_unidadMedida" class="form-control" >';	
        if($id==0){

	        $html.= '<option value = "0" selected>--Seleccione Unidad Medida--</option>';
	    }else{
		    $html.= '<option value = "0">--Seleccione Unidad Medida--</option>';
	    }	
      
        foreach ($recordset as $registro){
		    if($registro->id == $id){
			   $html.= '<option selected value="' . $registro->id.'">'.$registro->nombre.'</option>';
		    }else{
			   $html.= '<option value="' . $registro->id.'">'. $registro->nombre.'</option>';			  
		    }	
	    }	  	 			 
        $html.= '</select>';	
        $data = array(
           'combo'=>$html
        );
        echo json_encode($data);	
	}
    //------- Fin de combo_modelo()


   // empresa   
	public function ajax_combo_empresa($id){
        $this->load->model('Combo_mdl');
 	    $recordset = $this->Combo_mdl->combo_empresa();

        $html = '<Select name="id_cbo_empresa" id="id_cbo_empresa" class="form-control" >';	
        if($id==0){

	        $html.= '<option value = "0" selected>--Seleccione Empresa--</option>';
	    }else{
		    $html.= '<option value = "0">--Seleccione Empresa--</option>';
	    }	
      
        foreach ($recordset as $registro){
		    if($registro->id == $id){
			   $html.= '<option selected value="' . $registro->id.'">'.$registro->nombre.'</option>';
		    }else{
			   $html.= '<option value="' . $registro->id.'">'. $registro->nombre.'</option>';			  
		    }	
	    }	  	 			 
        $html.= '</select>';	
        $data = array(
           'combo'=>$html
        );
        echo json_encode($data);	
	}
    //------- Fin de combo_categoria producto()




   // REgion ubicacion   
	public function ajax_combo_region($id){
        $this->load->model('Combo_mdl');
 	    $recordset = $this->Combo_mdl->combo_region();

        $html = '<Select name="id_cbo_region" id="id_cbo_region" class="form-control" >';	
        if($id==0){

	        $html.= '<option value = "0" selected>--Seleccione Región--</option>';
	    }else{
		    $html.= '<option value = "0">--Seleccione Región--</option>';
	    }	
      
        foreach ($recordset as $registro){
		    if($registro->id == $id){
			   $html.= '<option selected value="' . $registro->id.'">'.$registro->nombre.'</option>';
		    }else{
			   $html.= '<option value="' . $registro->id.'">'. $registro->nombre.'</option>';			  
		    }	
	    }	  	 			 
        $html.= '</select>';	
        $data = array(
           'combo'=>$html
        );
        echo json_encode($data);	
	}
    //------- Fin de combo_categoria producto()

    //Combo sucursal
	public function ajax_combo_sucursalUsuario($id){
	  $arreglo = explode("aa", $id);	
	  $id = $arreglo[0];
	  $idEmpresa = $arreglo[1];
	  $idUsuario = $arreglo[2];

	  $this->load->model('Combo_mdl');	
	  $recordset = $this->Combo_mdl->combo_sucursalUsuario($idEmpresa, $idUsuario);
 
	  
	  $html = "";
      $html = '<Select name="id_cbo_sucursal" id="id_cbo_sucursal" class="form-control" >';	
      if($id==0){
	  	  $html.= '<option value = "0" selected>--Seleccione Sucursal--</option>';
	  }else{
		  $html.= '<option value = "0">--Seleccione Sucursal--</option>';
	  }	
	  
	  foreach($recordset as $registro){
				if($registro->id == $id){
					$html.= '<option selected value="' . $registro->id.'">'.$registro->nombre.'</option>';
				}else{
					$html.= '<option value="' . $registro->id.'">'. $registro->nombre.'</option>';			  
				}	
			}	  	 	

				 
        $html.= '</select>';	
        $data = array(
           'combo'=>$html
        );
        
        echo json_encode($data);	           	
	}
    //------- Fin de combo_municipio()    



    //------- combo_tipo operacion inventario()    
	public function ajax_combo_tipoOperacion($id){
        $html = '<Select name="id_cbo_tipoOperacion" id="id_cbo_tipoOperacion" class="form-control" >';	
        if($id=="0"){
	        $html.= '<option value = "0" selected>--Seleccione Tipo Operación--</option>';
	    }else{
		    $html.= '<option value = "0">--Seleccione Tipo Operación--</option>';
	    }	
	    
	    if($id=="1"){
		   $html.= '<option selected value="1">Entrada</option>';
	    }else{
		   $html.= '<option value="1">Entrada</option>';
		}
		
	    if($id=="2"){
		   $html.= '<option selected value="2">Salida</option>';
	    }else{
		   $html.= '<option value="2">Salida</option>';
		}
        $html.= '</select>';	

        $data = array(
           'combo'=>$html
        );
        echo json_encode($data);	
	}
    //------- Fin de combo_modelo()



// Provvedor Inventario
	public function ajax_combo_proveedorIn($id){
        $this->load->model('Combo_mdl');
 	    $recordset = $this->Combo_mdl->combo_proveedorIn();

        $html = '<Select name="id_cbo_proveedorIn" id="id_cbo_proveedorIn" class="form-control" >';	
        if($id==0){

	        $html.= '<option value = "0" selected>--SIN PROVEEDOR--</option>';
	    }else{
		    $html.= '<option value = "0">--SIN PROVEEDOR--</option>';
	    }	
      
        foreach ($recordset as $registro){
		    if($registro->id == $id){
			   $html.= '<option selected value="' . $registro->id.'">'.$registro->nombre.'</option>';
		    }else{
			   $html.= '<option value="' . $registro->id.'">'. $registro->nombre.'</option>';			  
		    }	
	    }	  	 			 
        $html.= '</select>';	
        $data = array(
           'combo'=>$html
        );
        echo json_encode($data);	
	}
    //------- Fin de combo_proveedor inventario()


// Concepto operacion Inventario
	public function ajax_combo_conceptoIn(){
		
		$id = $_GET['id'];
		$tipo = $_GET['tipo']; 
		
        $this->load->model('Combo_mdl');
 	    $recordset = $this->Combo_mdl->combo_conceptoIn($tipo);

        $html = '<Select name="id_cbo_conceptoIn" id="id_cbo_conceptoIn" class="form-control" >';	
        if($id==0){

	        $html.= '<option value = "0" selected>--SELECCIONE CONCEPTO--</option>';
	    }else{
		    $html.= '<option value = "0">--SELECCIONE CONCEPTO--</option>';
	    }	
      
        foreach ($recordset as $registro){
		    if($registro->id == $id){
			   $html.= '<option selected value="' . $registro->id.'">'.$registro->nombre.'</option>';
		    }else{
			   $html.= '<option value="' . $registro->id.'">'. $registro->nombre.'</option>';			  
		    }	
	    }	  	 			 
        $html.= '</select>';	
        $data = array(
           'combo'=>$html
        );
        echo json_encode($data);	
	}
    //------- Fin Concepto operacion Inventario



// Concepto operacion Inventario
	public function ajax_combo_productoIn($id){
        $this->load->model('Combo_mdl');
 	    $recordset = $this->Combo_mdl->combo_productoIn();

        $html = '<Select name="id_cbo_productoIn" id="id_cbo_productoIn" class="form-control" >';	
        if($id==0){

	        $html.= '<option value = "0" selected>--SELECCIONE EL PRODUCTO--</option>';
	    }else{
		    $html.= '<option value = "0">--SELECCIONE EL PRODUCTO--</option>';
	    }	
      
        foreach ($recordset as $registro){
		    if($registro->id == $id){
			   $html.= '<option selected value="' . $registro->id.'">'.$registro->nombre.'</option>';
		    }else{
			   $html.= '<option value="' . $registro->id.'">'. $registro->nombre.'</option>';			  
		    }	
	    }	  	 			 
        $html.= '</select>';	
        $data = array(
           'combo'=>$html
        );
        echo json_encode($data);	
	}
    //------- Fin Concepto operacion Inventario


// Concepto Tipo producto

	public function ajax_combo_tipoProducto($id){
        $this->load->model('Combo_mdl');
 	    $recordset = $this->Combo_mdl->combo_tipoProducto();
        $html = '<Select name="id_cbo_tipoProducto" id="id_cbo_tipoProducto" class="form-control" >';	
        if($id==0){

	        $html.= '<option value = "0" selected>--SELECCIONE TIPO PRODUCTO--</option>';
	    }else{
		    $html.= '<option value = "0">--SELECCIONE TIPO PRODUCTO--</option>';
	    }	
      
        foreach ($recordset as $registro){
		    if($registro->id == $id){
			   $html.= '<option selected value="' . $registro->id.'">'.$registro->nombre.'</option>';
		    }else{
			   $html.= '<option value="' . $registro->id.'">'. $registro->nombre.'</option>';			  
		    }	
	    }	  	 			 
        $html.= '</select>';	
        $data = array(
           'combo'=>$html
        );
        echo json_encode($data);	
	}
    //------- Fin Tipo Producto


//*************************** compra
// Proveedor Compra
	public function ajax_combo_proveedorCp($id){
		
        $this->load->model('Combo_mdl');
 	    $recordset = $this->Combo_mdl->combo_proveedorCp();

        $html = '<Select name="id_cbo_proveedorCp" id="id_cbo_proveedorCp" class="form-control" >';	
        if($id==0){
	        $html.= '<option value = "0" selected>--SELECCIONAR PROVEEDOR--</option>';
	    }else{
		    $html.= '<option value = "0">--SELECCIONAR PROVEEDOR--</option>';
	    }	
      
        foreach ($recordset as $registro){
		    if($registro->id == $id){
			   $html.= '<option selected value="' . $registro->id.'">'.$registro->nombre.'</option>';
		    }else{
			   $html.= '<option value="' . $registro->id.'">'. $registro->nombre.'</option>';			  
		    }	
	    }	  	 			 
        $html.= '</select>';	
        $data = array(
           'combo'=>$html
        );
        echo json_encode($data);	
	}
    //------- Fin de combo_proveedor compra().



// Concepto operacion detalle orden Compra
	public function ajax_combo_productoCp($id){
        $this->load->model('Combo_mdl');
 	    //$recordset = $this->Combo_mdl->combo_productoCp();
 	    $recordset = $this->Combo_mdl->combo_productoInEn();
 	    

        $html = '<Select name="id_cbo_productoCp" id="id_cbo_productoCp" class="form-control" >';	
        if($id==0){

	        $html.= '<option value = "0" selected>--SELECCIONE EL PRODUCTO--</option>';
	    }else{
		    $html.= '<option value = "0">--SELECCIONE EL PRODUCTO--</option>';
	    }	
      
        foreach ($recordset as $registro){
		    if($registro->id == $id){
			   $html.= '<option selected value="' . $registro->id.'">'.$registro->nombre.'</option>';
		    }else{
			   $html.= '<option value="' . $registro->id.'">'. $registro->nombre.'</option>';			  
		    }	
	    }	  	 			 
        $html.= '</select>';	
        $data = array(
           'combo'=>$html
        );
        echo json_encode($data);	
	}
    //------- Fin Concepto operacion Inventario
    
    
    
// administracion sala
	public function ajax_combo_sala($id){
        $this->load->model('Combo_mdl');
 	    $recordset = $this->Combo_mdl->combo_sala();

        $html = '<Select name="id_cbo_sala" id="id_cbo_sala" class="form-control" >';	
        if($id==0){

	        $html.= '<option value = "0" selected>--SELECCIONE LA SALA--</option>';
	    }else{
		    $html.= '<option value = "0">--SELECCIONE LA SALA--</option>';
	    }	
      
        foreach ($recordset as $registro){
		    if($registro->id == $id){
			   $html.= '<option selected value="' . $registro->id.'">'.$registro->nombre.'</option>';
		    }else{
			   $html.= '<option value="' . $registro->id.'">'. $registro->nombre.'</option>';			  
		    }	
	    }	  	 			 
        $html.= '</select>';	
        $data = array(
           'combo'=>$html
        );
        echo json_encode($data);	
	}
    //------- Fin Concepto operacion Inventario    


// administracion sala Anidado
	public function ajax_combo_sala_anid($id){
        $this->load->model('Combo_mdl');
 	    $recordset = $this->Combo_mdl->combo_sala();

        $html = '<Select name="id_cbo_sala" id="id_cbo_sala" class="form-control" onChange="cbo_sel_mesa()">';
        $html.= '<option value = "0" selected>--SELECCIONE LA SALA--</option>';
      
        foreach ($recordset as $registro){
		   $html.= '<option value="' . $registro->id.'">'. $registro->nombre.'</option>';			  
	    }	  	 			 
        $html.= '</select>';	
        $data = array(
           'combo'=>$html
        );
        echo json_encode($data);	
	}
    //------- Fin Concepto operacion Inventario    

    //Combo sucursal
                    
	public function ajax_anid_mesa($id){
	  $this->load->model('Combo_mdl');	
	  $recordset = $this->Combo_mdl->combo_anid_mesa($id);
  
	  $html = "";
      $html = '<Select name="id_cbo_mesa" id="id_cbo_mesa" class="form-control" >';	
  	  $html.= '<option value = "0" selected>--SELECCIONE MESA--</option>';
  
	  foreach($recordset as $registro){
		$html.= '<option value="' . $registro->id.'">'. $registro->nombre.'</option>';			  
 	  }	  	 	
        $html.= '</select>';	
        $data = array(
           'combo'=>$html
        );
        
        echo json_encode($data);	           	
	}
    //------- Fin de combo_municipio()    


	public function ajax_combo_mesonero($id){
        $this->load->model('Combo_mdl');
 	    $recordset = $this->Combo_mdl->combo_mesonero();

        $html = '<Select name="id_cbo_mesonero" id="id_cbo_mesonero" class="form-control" >';	
        if($id==0){

	        $html.= '<option value = "0" selected>--SELECCIONE MESONERO--</option>';
	    }else{
		    $html.= '<option value = "0">--SELECCIONE MESONERO--</option>';
	    }	
      
        foreach ($recordset as $registro){
			$nombre = trim($registro->nombre) . ", " . $registro->apellido;
		    if($registro->id == $id){
			   $html.= '<option selected value="' . $registro->id.'">'.$nombre.'</option>';
		    }else{
			   $html.= '<option value="' . $registro->id.'">'. $nombre.'</option>';			  
		    }	
	    }	  	 			 
        $html.= '</select>';	
        $data = array(
           'combo'=>$html
        );
        echo json_encode($data);	
	}
    //------- Fin Concepto operacion Inventario    





    //------- combo_tipo operacion caja()    
	public function ajax_anid_tipoOperacion($id){
        $html = '<Select name="id_cbo_tipoOperacion" id="id_cbo_tipoOperacion" class="form-control" onChange="cbo_sel_tipoMovimiento()">';	
        if($id==0){
	        $html.= '<option value = "0" selected>--Seleccione Tipo Operación--</option>';
	    }else{
		    $html.= '<option value = "0">--Seleccione Tipo Operación--</option>';
	    }	
	    
	    if($id==1){
		   $html.= '<option selected value="1">CRÉDITO</option>';
	    }else{
		   $html.= '<option value="1">CRÉDITO</option>';
		}
		
	    if($id==2){
		   $html.= '<option selected value="2">DÉBITO</option>';
	    }else{
		   $html.= '<option value="2">DÉBITO</option>';
		}
        $html.= '</select>';	

        $data = array(
           'combo'=>$html
        );
        echo json_encode($data);	
	}
    //------- Fin de combo_modelo()



    //Combo sucursal
                    
	public function ajax_anid_tipoMovimiento($id){
	  $arreglo = explode("aa", $id);	
	  $id = $arreglo[0];
	  $idTipoOperacion = $arreglo[1];
	 

	  $this->load->model('Combo_mdl');	
	  $recordset = $this->Combo_mdl->combo_anid_tipoMovimiento($idTipoOperacion);
  
	  $html = "";
      $html = '<Select name="id_cbo_tipoMovimiento" id="id_cbo_tipoMovimiento" class="form-control" >';	
      if($id==0){
	  	  $html.= '<option value = "0" selected>--Seleccione Tipo Movimiento--</option>';
	  }else{
		  $html.= '<option value = "0">--Seleccione Tipo Movimiento--</option>';
	  }	
	  
	  foreach($recordset as $registro){
				if($registro->id == $id){
					$html.= '<option selected value="' . $registro->id.'">'.$registro->nombre.'</option>';
				}else{
					$html.= '<option value="' . $registro->id.'">'. $registro->nombre.'</option>';			  
				}	
			}	  	 	

				 
        $html.= '</select>';	
        $data = array(
           'combo'=>$html
        );
        
        echo json_encode($data);	           	
	}
    //------- Fin de combo_municipio()    

// mantenimiento tipo moneda
	public function ajax_combo_tipoDinero($id){
        $this->load->model('Combo_mdl');
 	    $recordset = $this->Combo_mdl->combo_tipoDinero();

        $html = '<Select name="id_cbo_tipoDinero" id="id_cbo_tipoDinero" class="form-control" >';	
        if($id==0){

	        $html.= '<option value = "0" selected>--Seleccione Tipo Dinero--</option>';
	    }else{
		    $html.= '<option value = "0">--Seleccione Tipo Dinero--</option>';
	    }	
      
        foreach ($recordset as $registro){
		    if($registro->id == $id){
			   $html.= '<option selected value="' . $registro->id.'">'.$registro->nombre.'</option>';
		    }else{
			   $html.= '<option value="' . $registro->id.'">'. $registro->nombre.'</option>';			  
		    }	
	    }	  	 			 
        $html.= '</select>';	
        $data = array(
           'combo'=>$html
        );
        echo json_encode($data);	
	}
    //------- Fin Concepto operacion Inventario    



// Producto Inventario
	public function ajax_combo_productoInventario($id){
        $this->load->model('Combo_mdl');
 	    $recordset = $this->Combo_mdl->combo_productoInventario();

        $html = '<Select name="id_cbo_productoIn" id="id_cbo_productoIn" class="form-control" >';	
        if($id==0){

	        $html.= '<option value = "0" selected>--SELECCIONE EL PRODUCTO--</option>';
	    }else{
		    $html.= '<option value = "0">--SELECCIONE EL PRODUCTO--</option>';
	    }	
      
        foreach ($recordset as $registro){
		    if($registro->id == $id){
			   $html.= '<option selected value="' . $registro->id.'">'.$registro->nombre.'</option>';
		    }else{
			   $html.= '<option value="' . $registro->id.'">'. $registro->nombre.'</option>';			  
		    }	
	    }	  	 			 
        $html.= '</select>';	
        $data = array(
           'combo'=>$html
        );
        echo json_encode($data);	
	}
    //------- Fin Concepto operacion Inventario


// Producto Inventario
	public function ajax_combo_productoInEn($id){
        $this->load->model('Combo_mdl');
 	    $recordset = $this->Combo_mdl->combo_productoInEn();

        $html = '<Select name="id_cbo_productoIn" id="id_cbo_productoIn" class="form-control" onChange="disponibilidadProducto()">';	
        if($id==0){

	        $html.= '<option value = "0" selected>--SELECCIONE EL PRODUCTO--</option>';
	    }else{
		    $html.= '<option value = "0">--SELECCIONE EL PRODUCTO--</option>';
	    }	
      
        foreach ($recordset as $registro){
		    if($registro->id == $id){
			   $html.= '<option selected value="' . $registro->id.'">'.$registro->nombre.'</option>';
		    }else{
			   $html.= '<option value="' . $registro->id.'">'. $registro->nombre.'</option>';			  
		    }	
	    }	  	 			 
        $html.= '</select>';	
        $data = array(
           'combo'=>$html
        );
        echo json_encode($data);	
	}
    //------- Fin Concepto operacion Inventario


// Concepto operacion Inventario entrada
	public function ajax_combo_conceptoInEn($id){
	
        $this->load->model('Combo_mdl');
 	    $recordset = $this->Combo_mdl->combo_conceptoInEn();


        $html = '<Select name="id_cbo_conceptoIn" id="id_cbo_conceptoIn" class="form-control" >';	
        if($id==0){

	        $html.= '<option value = "0" selected>--SELECCIONE CONCEPTO--</option>';
	    }else{
		    $html.= '<option value = "0">--SELECCIONE CONCEPTO--</option>';
	    }	
      
        foreach ($recordset as $registro){
		    if($registro->id == $id){
			   $html.= '<option selected value="' . $registro->id.'">'.$registro->nombre.'</option>';
		    }else{
			   $html.= '<option value="' . $registro->id.'">'. $registro->nombre.'</option>';			  
		    }	
	    }	  	 			 
        $html.= '</select>';	
        $data = array(
           'combo'=>$html
        );
        echo json_encode($data);	
	}
    //------- Fin Concepto operacion Inventario


// Concepto operacion Inventario entrada
	public function ajax_combo_conceptoInSa($id){
	
        $this->load->model('Combo_mdl');
 	    $recordset = $this->Combo_mdl->combo_conceptoInSa();


        $html = '<Select name="id_cbo_conceptoIn" id="id_cbo_conceptoIn" class="form-control" >';	
        if($id==0){

	        $html.= '<option value = "0" selected>--SELECCIONE CONCEPTO--</option>';
	    }else{
		    $html.= '<option value = "0">--SELECCIONE CONCEPTO--</option>';
	    }	
      
        foreach ($recordset as $registro){
		    if($registro->id == $id){
			   $html.= '<option selected value="' . $registro->id.'">'.$registro->nombre.'</option>';
		    }else{
			   $html.= '<option value="' . $registro->id.'">'. $registro->nombre.'</option>';			  
		    }	
	    }	  	 			 
        $html.= '</select>';	
        $data = array(
           'combo'=>$html
        );
        echo json_encode($data);	
	}
    //------- Fin Concepto operacion Inventario



   // Ingredientes
	public function ajax_combo_ingrediente($id){
        $this->load->model('Combo_mdl');
 	    $recordset = $this->Combo_mdl->combo_ingrediente();

        $html = '<Select name="id_cbo_ingrediente" id="id_cbo_ingrediente" class="form-control" >';	
        if($id==0){

	        $html.= '<option value = "0" selected>--Seleccione Ingrediente--</option>';
	    }else{
		    $html.= '<option value = "0">--Seleccione Ingrediente--</option>';
	    }	
      
        foreach ($recordset as $registro){
		    if($registro->id == $id){
			   $html.= '<option selected value="' . $registro->id.'">'.$registro->nombre.'</option>';
		    }else{
			   $html.= '<option value="' . $registro->id.'">'. $registro->nombre.'</option>';			  
		    }	
	    }	  	 			 
        $html.= '</select>';	
        $data = array(
           'combo'=>$html
        );
        echo json_encode($data);	
	}
    //------- Fin de combo_Ingrediente()

	public function ajax_combo_ingredienteAlim($id){
        $this->load->model('Combo_mdl');
 	    $recordset = $this->Combo_mdl->combo_ingredienteAlim();

        $html = '<Select name="id_cbo_ingrediente" id="id_cbo_ingrediente" class="form-control" >';	
        if($id==0){

	        $html.= '<option value = "0" selected>--Seleccione Ingrediente--</option>';
	    }else{
		    $html.= '<option value = "0">--Seleccione Ingrediente--</option>';
	    }	
      
        foreach ($recordset as $registro){
		    if($registro->id == $id){
			   $html.= '<option selected value="' . $registro->id.'">'.$registro->nombre.'</option>';
		    }else{
			   $html.= '<option value="' . $registro->id.'">'. $registro->nombre.'</option>';			  
		    }	
	    }	  	 			 
        $html.= '</select>';	
        $data = array(
           'combo'=>$html
        );
        echo json_encode($data);	
	}
    //------- Fin de combo_Ingrediente()



// Concepto operacion detalle orden Compra
	public function ajax_combo_ingredienteSabor($id){
        $this->load->model('Combo_mdl');
 	    //$recordset = $this->Combo_mdl->combo_productoCp();
 	    $recordset = $this->Combo_mdl->combo_ingredienteSabor($id);
 	    

        $html = '<Select name="id_cbo_ingrediente" id="id_cbo_ingrediente" class="form-control" >';	
        if($id==0){

	        $html.= '<option value = "0" selected>--SELECCIONE INGREDIENTE--</option>';
	    }else{
		    $html.= '<option value = "0">--SELECCIONE INGREDIENTE--</option>';
	    }	
      
        foreach ($recordset as $registro){
		    if($registro->id == $id){
			   $html.= '<option selected value="' . $registro->id.'">'.$registro->nombre.'</option>';
		    }else{
			   $html.= '<option value="' . $registro->id.'">'. $registro->nombre.'</option>';			  
		    }	
	    }	  	 			 
        $html.= '</select>';	
        $data = array(
           'combo'=>$html
        );
        echo json_encode($data);	
	}
    //------- Fin Concepto operacion Inventario


// Concepto operacion detalle orden Compra
	public function ajax_combo_ingredienteAdicional($id){
        $this->load->model('Combo_mdl');
 	    //$recordset = $this->Combo_mdl->combo_productoCp();
 	    $recordset = $this->Combo_mdl->combo_ingredienteAdicional($id);
 	    

        $html = '<Select name="id_cbo_ingrediente" id="id_cbo_ingrediente" class="form-control" onChange="obt_unidad()">';	
        if($id==0){

	        $html.= '<option value = "0" selected>--SELECCIONE INGREDIENTE--</option>';
	    }else{
		    $html.= '<option value = "0">--SELECCIONE INGREDIENTE--</option>';
	    }	
      
        foreach ($recordset as $registro){
		    if($registro->id == $id){
			   $html.= '<option selected value="' . $registro->id.'">'.$registro->nombre.'</option>';
		    }else{
			   $html.= '<option value="' . $registro->id.'">'. $registro->nombre.'</option>';			  
		    }	
	    }	  	 			 
        $html.= '</select>';	
        $data = array(
           'combo'=>$html
        );
        echo json_encode($data);	
	}
    //------- Fin Concepto operacion Inventario


// Categoria de menu
	public function ajax_combo_categoriaMenu($id){
        $this->load->model('Combo_mdl');

 	    $recordset = $this->Combo_mdl->combo_categoriaMenu($id);
        $html = '<Select name="id_cbo_categoriaMenu" id="id_cbo_categoriaMenu" class="form-control" >';	
        if($id==0){

	        $html.= '<option value = "0" selected>--SELECCIONE CATEGORIA--</option>';
	    }else{
		    $html.= '<option value = "0">--SELECCIONE CATEGORIA--</option>';
	    }	
      
        foreach ($recordset as $registro){
		    if($registro->id == $id){
			   $html.= '<option selected value="' . $registro->id.'">'.$registro->nombre.'</option>';
		    }else{
			   $html.= '<option value="' . $registro->id.'">'. $registro->nombre.'</option>';			  
		    }	
	    }	  	 			 
        $html.= '</select>';	
        $data = array(
           'combo'=>$html
        );
        echo json_encode($data);	
	}
    //------- Fin Concepto operacion Inventario
    
    
// Tipo Presentacion
	public function ajax_combo_tipoPresentacion($id){
        $this->load->model('Combo_mdl');

 	    $recordset = $this->Combo_mdl->combo_tipoPresentacion($id);
        $html = '<Select name="id_cbo_tipoPresentacion" id="id_cbo_tipoPresentacion" class="form-control" >';	
        if($id==0){

	        $html.= '<option value = "0" selected>--SELECCIONE TIPO PRESENTACIÓN--</option>';
	    }else{
		    $html.= '<option value = "0">--SELECCIONE TIPO PRESENTACIÓN--</option>';
	    }	
      
        foreach ($recordset as $registro){
		    if($registro->id == $id){
			   $html.= '<option selected value="' . $registro->id.'">'.$registro->nombre.'</option>';
		    }else{
			   $html.= '<option value="' . $registro->id.'">'. $registro->nombre.'</option>';			  
		    }	
	    }	  	 			 
        $html.= '</select>';	
        $data = array(
           'combo'=>$html
        );
        echo json_encode($data);	
	}
    //------- Fin Concepto operacion Inventario    


    public function ajax_combo_administradorUsuario($id){
        $this->load->model('Combo_mdl');

 	    $recordset = $this->Combo_mdl->combo_administradorUsuario();
        $html = '<Select name="id_cbo_administradorUsuario" id="id_cbo_administradorUsuario" class="form-control" >';	
        $html.= '<option value = "0" selected>--SELECCIONE USUARIO--</option>';
      
        foreach ($recordset as $registro){
		   $nombre = trim($registro->nombre) . ", " . trim($registro->apellido); 	
		   $html.= '<option value="' . $registro->id.'">'. $nombre.'</option>';			  
	    }	  	 			 
        $html.= '</select>';	
        $data = array(
           'combo'=>$html
        );
        echo json_encode($data);	
	}


    public function ajax_combo_administradorEmpresa($id){
        $this->load->model('Combo_mdl');

 	    $recordset = $this->Combo_mdl->combo_administradorEmpresa($id);
        $html = '<Select name="id_cbo_administradorEmpresa" id="id_cbo_administradorEmpresa" class="form-control" >';	
        $html.= '<option value = "0" selected>--Seleccione Usuario--</option>';
      
        foreach ($recordset as $registro){
		   $nombre = trim($registro->nombre) . ", " . trim($registro->apellido); 	
		   $html.= '<option value="' . $registro->id.'">'. $nombre.'</option>';			  
	    }	  	 			 
        $html.= '</select>';	
        $data = array(
           'combo'=>$html
        );
        echo json_encode($data);	
	}
	
	
    public function ajax_combo_administradorSucursal($id){
        $this->load->model('Combo_mdl');

 	    $recordset = $this->Combo_mdl->combo_administradorSucursal($id);
        $html = '<Select name="id_cbo_administradorSucursal" id="id_cbo_administradorSucursal" class="form-control" >';	
        $html.= '<option value = "0" selected>--Seleccione Usuario--</option>';
      
        foreach ($recordset as $registro){
		   $nombre = trim($registro->nombre) . ", " . trim($registro->apellido) . " (RUN: " . trim($registro->run) . ")"; 	
		   $html.= '<option value="' . $registro->id.'">'. $nombre.'</option>';			  
	    }	  	 			 
        $html.= '</select>';	
        $data = array(
           'combo'=>$html
        );
        echo json_encode($data);	
	}	
	


    public function ajax_combo_empresaAdministrador($id){
        $this->load->model('Combo_mdl');

 	    $recordset = $this->Combo_mdl->combo_EmpresaAdministrador_login();
        $html = '<Select name="id_cbo_empresaAdministrador" id="id_cbo_empresaAdministrador" class="form-control" onChange="selUsuarioAdm()">';	
        $html.= '<option value = "0" selected>--Seleccione Empresa--</option>';
      
        foreach ($recordset as $registro){
		   $html.= '<option value="' . $registro->id.'">'. $registro->nombre.'</option>';			  
	    }	  	 			 
        $html.= '</select>';	
        $data = array(
           'combo'=>$html
        );
        echo json_encode($data);	
	}
	
    public function ajax_combo_sucursalAdm($id){
        $this->load->model('Combo_mdl');
 	    $recordset = $this->Combo_mdl->combo_sucursalAdm();
        $html = '<Select name="id_cbo_sucursalAdm" id="id_cbo_sucursalAdm" class="form-control" onChange="usuarioSucursal()">';	
        $html.= '<option value = "0" selected>--SELECCIONE SUCURSAL--</option>';
      
        foreach ($recordset as $registro){
		   $html.= '<option value="' . $registro->id.'">'. $registro->nombre.'</option>';			  
	    }	  	 			 
        $html.= '</select>';	
        $data = array(
           'combo'=>$html
        );
        echo json_encode($data);	
	}	
	
	
    public function ajax_combo_empresaLoginAdministrador($id){
        $this->load->model('Combo_mdl');

 	    $recordset = $this->Combo_mdl->combo_EmpresaAdministrador_login();
 	    
        $html = '<Select name="id_cbo_empresaLoginAdministrador" id="id_cbo_empresaLoginAdministrador" ';
        $html.= ' class="form-control" onChange="cbo_sel_sucursal()">';	
        $html.= '<option value = "0" selected>--SELECCIONE EMPRESA--</option>';
      
        foreach ($recordset as $registro){
		   $html.= '<option value="' . $registro->id.'">'. $registro->nombre.'</option>';			  
	    }	  	 			 
        $html.= '</select>';	
        $data = array(
           'combo'=>$html
        );
        echo json_encode($data);	
	}
	

	public function ajax_combo_SucursalLoginAdministrador($id){
	  $this->load->model('Combo_mdl');	
	  $recordset = $this->Combo_mdl->combo_sucursalLoginAdm($id);
  
	  $html = "";
      $html = '<Select name="id_cbo_sucursalLoginAdministrador" id="id_cbo_sucursalLoginAdministrador" class="form-control" >';	
  	  $html.= '<option value = "0" selected>--SELECCIONE SUCURSAL--</option>';
  
	  foreach($recordset as $registro){
		$html.= '<option value="' . $registro->id.'">'. $registro->nombre.'</option>';			  
 	  }	  	 	
        $html.= '</select>';	
        $data = array(
           'comboSuc'=>$html
        );
        
        echo json_encode($data);	           	
	}
	
	public function ajax_combo_SucursalFactura($id){
	  $id = $this->session->userdata('idEmpresa');	
	  $this->load->model('Combo_mdl');	
	  $recordset = $this->Combo_mdl->combo_sucursalLoginAdm($id);
	  $html = "";
      $html = '<Select name="id_cbo_sucursalFactura" id="id_cbo_sucursalFactura" class="form-control" >';	
  	  $html.= '<option value = "0" selected>--SELECCIONE SUCURSAL--</option>';
  	  foreach($recordset as $registro){
		$html.= '<option value="' . $registro->id.'">'. $registro->nombre.'</option>';			  
 	  }	  	 	
        $html.= '</select>';	
        $data = array(
           'combo'=>$html
        );
        
        echo json_encode($data);	           	
	}




    /////////////////////////   personal de sucursal //////////////////////////////////
	public function ajax_combo_personalSucursal($id){
	  $this->load->model('Combo_mdl');	
	  $recordset = $this->Combo_mdl->combo_personalSucursal($id);
  
	  $html = "";
      $html = '<Select name="id_cbo_personalSucursal" id="id_cbo_personalSucursal" class="form-control" >';	
  	  $html.= '<option value = "0" selected>--Seleccione Personal Sucursal--</option>';
  
	  foreach($recordset as $registro){
		$nombre = trim($registro->nombre) .", ". trim($registro->apellido) . " (RUN: " . trim($registro->run).")";   
		  
		$html.= '<option value="' . $registro->id.'">'. $nombre.'</option>';			  
 	  }	  	 	
        $html.= '</select>';	
        $data = array(
           'combo'=>$html
        );
        
        echo json_encode($data);	           	
	}

    //actualizar personal sucursal -- rol    
	public function ajax_combo_rolSucursal($id){
		
        $this->load->model('Combo_mdl');
 	    $recordset = $this->Combo_mdl->combo_rolSucursal();
        $html = '<Select name="id_cbo_rolSucursal" id="id_cbo_rolSucursal" class="form-control" >';	
        if($id==0){
	        $html.= '<option value = "0" selected>--Seleccione Rol del Usuario--</option>';
	    }else{
		    $html.= '<option value = "0">--Seleccione Rol del USuario--</option>';
	    }	
        foreach ($recordset as $registro){
		    if($registro->id == $id){
			   $html.= '<option selected value="' . $registro->id.'">'.$registro->nombre.'</option>';
		    }else{
			   $html.= '<option value="' . $registro->id.'">'. $registro->nombre.'</option>';			  
		    }	
	    }	  	 			 
        $html.= '</select>';	

        $data = array(
           'combo'=>$html
        );
        echo json_encode($data);	
	}
    //------- Fin de rol sucursal()
    
    ////////////////// fin personal sucursal //////////////////////





	
	
}//fin de controlador
