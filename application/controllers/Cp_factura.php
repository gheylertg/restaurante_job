<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cp_factura extends CI_Controller {


    public function __construct() {
         parent::__construct();
         $this->load->model(array('Cp_factura_mdl','Mt_producto_mdl','In_inventario_mdl'));
         $this->load->library(array('Clssession')); 
    }


    public function index(){
    //Validar session usuario
    $valSession = $this->clssession->valSession();
    if($valSession==true){

        
        //accion 0 vista sin datatable, accion 1  activar datatable 
        $data=array(
            'accion'=>1,
            'modulo'=>1,
        );

        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('compra/factura/act_factura',$data);
        $this->load->view('footer/footer', $data);
        $this->load->view('compra/factura/footer_factura', $data);
    }//fin val session
    }
    
    
    
    
        function generarDatatableProducto($row){
        //La variable rol trae los diferentes roles sin estar asociados a los modulos. 
        //obtener la información de la tabla seleccionada
        $modificar = base_url() . "assets/images/modificar02.jpeg";
        $eliminar = base_url() . "assets/images/eliminar.jpeg";
        $reactivar = base_url() . "assets/images/reactivar.jpeg";
        $lectura = base_url() . "assets/images/ver.png";

        $html = '<table id="basic-datatables" class="display table table-striped table-hover">';
        $html.= '    <thead>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="10%">cod. Producto</th>';
        $html.= '           <th width="40%">Producto</th>';        
        $html.= '           <th width="10%">Cantidad</th>';                
        $html.= '           <th width="10%">Costo Unitario</th>';                
        $html.= '           <th width="15%">Acción</th>';
        $html.= '        </tr>';
        $html.= '    </thead>';
        $html.= '    <tfoot>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="10%">cod. Producto</th>';
        $html.= '           <th width="40%">Producto</th>';        
        $html.= '           <th width="10%">Cantidad</th>';                
        $html.= '           <th width="10%">Costo Unitario</th>';                
        $html.= '           <th width="15%">Acción</th>';
        $html.= '        </tr>';
        $html.= '    </tfoot>';
        $html.= '    <tbody>';
        if($row==false){
            $html.= '      <tr>';
            $html.= '         <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
           ';
								
            $html.= '      </tr>';
        }else{
            foreach($row as $key){ 
				$cantidad = str_replace(".",",", $key->cantidad );
                $monto = str_replace(".",",", $key->monto );
				
                $html.= '  <tr>';
                $html.= '      <td>' . $key->cod_producto.'</td>';
                $html.= '      <td>' . $key->producto.'</td>';                
                $html.= '      <td>' . $cantidad.'</td>';                                
                $html.= '      <td>' . $monto.'</td>';                                                
                $html.= '      <td>';               
                //$html.= '<a href="javascript:void(0)" onclick="javascript:modificar_form('.$key->id.')">';


                if($this->clssession->accion(7,3)==1){ 
                $html.='&nbsp;<a href="javascript:void(0)" onclick="javascript:mod_productoFactura('.$key->id.')">';$html.= '<img src="'.$modificar.'" style="width:30px; height:30px" alt="Modificar" ';
                    $html.= 'title="Modificar Producto"></a>';
                }    

                if($this->clssession->accion(7,4)==1){ 
                  $html.='&nbsp;<a href="javascript:void(0)" onclick="javascript:elim_producto('.$key->id.')">';
                  $html.= '<img src="'.$eliminar.'" style="width:25px; height:25px" alt="Eliminar" ';
                  $html.= 'title="Eliminar Producto"></a>';'></a>';
                }    

                $html.= '       </td>';             
                $html.= '</tr>';
            }
        }  

        $html.= '    </tbody>';
        $html.= '</table>';
        return $html;                   
    }




    function ajax_datatable($id){
    //Validar session usuario
    $valSession = $this->clssession->valSession();
    if($valSession==true){

        $row = $this->Cp_factura_mdl->obt_dataTable();
        $html= $this->generarDatatable($row);
        $data = array(
            "registro"=>$html);
        echo json_encode($data);    
    }//fin val session
    }   
    
    
    function generarDatatable($row){

        //obtener la información de la tabla seleccionada
        $modificarFactura = base_url() . "assets/images/factura.png";
        $modificarProducto = base_url() . "assets/images/producto02.jpeg";
        $eliminar = base_url() . "assets/images/eliminar.jpeg";
        $reactivar = base_url() . "assets/images/reactivar.jpeg";
        $lectura = base_url() . "assets/images/ver.png";
        
        $html = '<table id="basic-datatables" class="display table table-striped table-hover">';
        $html.= '    <thead>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="5%">#</th>';
        $html.= '           <th width="10%">Fecha</th>';
        $html.= '           <th width="20%">Nro Factura</th>';        
        $html.= '           <th width="40%">Proveedor</th>';
        $html.= '           <th width="10%">Monto</th>';
        $html.= '           <th width="15%">Acción</th>';
        $html.= '        </tr>';
        $html.= '    </thead>';
        $html.= '    <tfoot>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="5%">#</th>';
        $html.= '           <th width="10%">Fecha</th>';
        $html.= '           <th width="20%">Nro Factura</th>';        
        $html.= '           <th width="40%">Proveedor</th>';
        $html.= '           <th width="10%">Monto</th>';
        $html.= '           <th width="15%">Acción</th>';
        $html.= '        </tr>';
        $html.= '    </tfoot>';
        $html.= '    <tbody>';
        if($row==false){
            $html.= '      <tr>';
            $html.= '          <td></td><td></td><td></td><td></td><td></td><td></td>';    
            $html.= '      </tr>';
        }else{
			$nro=0;
            foreach($row as $key){ 
				$nro+=1;
                $fecha = substr($key->fecha,0,10);
                $html.= '  <tr>';
                $html.= '      <td>' . $nro.'</td>';
                $html.= '      <td>' . $fecha.'</td>';
                $html.= '      <td>' . $key->factura.'</td>';
                $html.= '      <td>' . $key->proveedor.'</td>';
                $html.= '      <td>' . $key->monto.'</td>';
                $html.= '      <td>';               

                if($this->clssession->accion(8,1)==1){ 

                    $html.='<a href="javascript:void(0)" onclick="javascript:ver_form('.$key->id.')">';
                    $html.= '<img src="'.$lectura.'" style="width:25px; height:25px" alt="Ver" ';
                    $html.= 'title="Ver Factura"></a>';
                }    

                if($this->clssession->accion(8,3)==1){ 
                    $html.= '&nbsp;<a href="javascript:void(0)" onclick="javascript:modificar_factura('.$key->id.')">';    
                    
                    $html.= '<img src="'.$modificarFactura.'" style="width:30px; height:30px" alt="Modificar" ';
                    $html.= 'title="Actualizar Factura"></a>';
                }    

                if($this->clssession->accion(8,3)==1){ 
                    $html.= '&nbsp;<a href="javascript:void(0)" onclick="javascript:mod_producto('.$key->id.')">';
                    $html.= '<img src="'.$modificarProducto.'" style="width:30px; height:30px" alt="Modificar" ';
                    $html.= 'title="Actualizar Producto"></a>';
                }    

                $html.= '       </td>';             
                $html.= '</tr>';
            }
        }  

        $html.= '    </tbody>';
        $html.= '</table>';
        return $html;                   
    }


    public function incFactura(){
    //Validar session usuario
    $valSession = $this->clssession->valSession();
    if($valSession==true){

        $arregloProducto[0][0]="";
        $arregloProducto[0][1]="";
        $arregloProducto[0][2]="";
        $arregloProducto[0][3]="";
        $arregloProducto[0][4]="";
        $arregloProducto[0][5]="";

       
        //arreglo para el detalle entrada
        $dataArreglo = array(
            'nroProducto' => 0,
            'arregloProducto' => $arregloProducto,
            'status'=>0
        );    
        $this->session->set_userdata($dataArreglo);           
        $data=array(
            'accion'=>1,
            'modulo'=>2,
        );
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('compra/factura/incFactura',$data);
        $this->load->view('footer/footer', $data);
        $this->load->view('compra/factura/footer_factura', $data);
    }//fin val session  
    }


    public function verFactura($id){
    //Validar session usuario
    $valSession = $this->clssession->valSession();
    if($valSession==true){
		$rowFactura = $this->Cp_factura_mdl->verFactura($id);
		
		$arregloFactura[0] = $rowFactura->fecha;
		$arregloFactura[1] = $rowFactura->proveedor;
        $arregloFactura[2] = $rowFactura->factura;
        $arregloFactura[3] = $rowFactura->monto;



		$rowDetalle = $this->Cp_factura_mdl->obt_dataTableProducto($rowFactura->id);
		$i = 0;
		foreach($rowDetalle as $key){
			$arregloProducto[$i][0] = $key->id_producto;
			$arregloProducto[$i][1] = $key->cod_producto; 
			$arregloProducto[$i][2] = $key->producto;
			$arregloProducto[$i][3] = $key->unidad_medida;
			$arregloProducto[$i][4] = $key->cantidad;		
            $arregloProducto[$i][5] = $key->monto;       

			$i+=1;
		}	

       
        //arreglo para el detalle entrada
        $dataArreglo = array(
            'arregloFactura'=>$arregloFactura,
            'nroProducto' => $i,
            'arregloProducto' => $arregloProducto,
            'status'=>0
        );    
        $this->session->set_userdata($dataArreglo);           
        $data=array(
            'accion'=>1,
            'modulo'=>4,
            'idFactura'=>$id
        );
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('compra/factura/verFactura',$data);
        $this->load->view('footer/footer', $data);
        $this->load->view('compra/factura/footer_factura', $data);
    }//fin val session  
    }


    //generar matriz detalle producto entrada
    public function ajax_datatable_factura(){
       $nro = $this->session->userdata("nroProducto");
        $arreglo = $this->session->userdata("arregloProducto");
        $eliminar = base_url() . "assets/images/eliminar.jpeg";

        $html = '<table class="table table-bordered table-head-bg-info ';
        $html.= 'table-bordered-bd-info mt-4">';
        $html.= '<thead>';
        $html.= '<tr>';
        $html.= '    <th scope="col" width="5%">#</th>';
        $html.= '    <th scope="col" width="10%" style="text-align:center">Cod.</th>';
        $html.= '    <th scope="col" width="40%">Producto</th>';
        $html.= '    <th scope="col" width="20%">Unidad Medida</th>';
        $html.= '    <th scope="col" width="10%" style="text-align:center">Cantidad</th>';
        $html.= '    <th scope="col" width="10%" style="text-align:center">Monto</th>';        
        $html.= '    <th scope="col" width="5%" style="text-align:center">Acción</th>';
        $html.= '</tr>';
        $html.= '</thead>';

        $html.= '<tbody>';        

        if($nro == 0){
            $html.= '<tr>';
            $html.= '    <td></td>';
            $html.= '    <td></td>';
            $html.= '    <td></td>';
            $html.= '    <td></td>';
            $html.= '    <td></td>';
            $html.= '    <td></td>';
            $html.= '    <td></td>';
            $html.= '</tr>';
        } 
        else{
            $i = 1;
            while($i <= $nro){
                $html.= '<tr>';
                $html.= '    <td>' .$arreglo[$i][0] . '</td>';
                $html.= '    <td style="text-align:center">' .$arreglo[$i][1] . '</td>';
                $html.= '    <td>' .$arreglo[$i][2] . '</td>';
                $html.= '    <td>' .$arreglo[$i][3] . '</td>';
                $html.= '    <td style="text-align:right">' .$arreglo[$i][4] . '</td>';
                $html.= '    <td style="text-align:right">' .$arreglo[$i][5] . '</td>';                


                $html.= '<td><a href="javascript:void(0)" onclick="javascript:eliminar_producto('.$i.')">';
                $html.= '<img src="'.$eliminar.'" style="width:25px; height:25px" alt="Eliminar"></a></td';
                $html.= '</tr>';
                $i+=1;
            }   
        }    
        $html.='</tbody>';
        $html.='</table>';
        $data = array(
            "registro"=>$html,
            "status"=>0,
            "nroProducto"=>$nro,
        );
         echo json_encode($data);        
    }

    //generar matriz detalle producto entrada
    public function ajax_datatable_verFactura(){
        $nro = $this->session->userdata("nroProducto");
        $arreglo = $this->session->userdata("arregloProducto");
        $arregloFactura = $this->session->userdata("arregloFactura");
        
        $eliminar = base_url() . "assets/images/eliminar.jpeg";

        $html = '<table class="table table-bordered table-head-bg-info ';
        $html.= 'table-bordered-bd-info mt-4">';
        $html.= '<thead>';
        $html.= '<tr>';
        $html.= '    <th scope="col" width="5%">#</th>';
        $html.= '    <th scope="col" width="10%" style="text-align:center">Cod.</th>';
        $html.= '    <th scope="col" width="45%">Producto</th>';
        $html.= '    <th scope="col" width="20%">Unidad Medida</th>';
        $html.= '    <th scope="col" width="10%" style="text-align:center">Cantidad</th>';
        $html.= '    <th scope="col" width="10%" style="text-align:center">Costo Unitario</th>';        

        $html.= '</tr>';
        $html.= '</thead>';

        $html.= '<tbody>';        

            $i = 0;
            while($i < $nro){
                $html.= '<tr>';
                $html.= '    <td>' .$i . '</td>';
                $html.= '    <td style="text-align:center">' .$arreglo[$i][1] . '</td>';
                $html.= '    <td>' .$arreglo[$i][2] . '</td>';
                $html.= '    <td>' .$arreglo[$i][3] . '</td>';
                $html.= '    <td style="text-align:right">' .$arreglo[$i][4] . '</td>';
                $html.= '    <td style="text-align:right">' .$arreglo[$i][5] . '</td>';
                $html.= '</tr>';
                $i+=1;
            }   

        $html.='</tbody>';
        $html.='</table>';
        
        $proveedor = $arregloFactura[1];
        $fecha = $arregloFactura[0];
        $factura = $arregloFactura[2];
        $monto = $arregloFactura[3];



        $data = array(
            "registro"=>$html,
            "status"=>0,
            "nroProducto"=>$nro,
            "proveedor"=>$proveedor,
            "fecha"=>$fecha,
            "factura"=>$factura,
            "monto"=>$monto            

        );
         echo json_encode($data);        
    }





public function ajax_guardar_productoCp(){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){

    $idProducto = $this->input->post("id_cbo_productoCp");
    $cantidad = $this->input->post("cantidad");
    $monto = $this->input->post("montoF");


    //validar si existe el producto
    $arreglo = $this->session->userdata("arregloProducto");
    $nro = $this->session->userdata("nroProducto");

    $validar = 0;
    if($nro>0){
        $i=1;
        $swParar = 0;
        while($i <= $nro && $swParar==0){
            if($arreglo[$i][0]==$idProducto){
                $swParar=1;
            }
            $i+=1;
        }
        if($swParar==1){
            $validar = 1; 
        }
    }    

    if($validar==0){
        $rowProducto = $this->Mt_producto_mdl->obtProductoCp($idProducto);

        $nro+=1;

        $arreglo[$nro][0] = $idProducto;
        $arreglo[$nro][1] = $rowProducto->cod_producto; 
        $arreglo[$nro][2] = $rowProducto->nombre;
        $arreglo[$nro][3] = $rowProducto->unidad_medida;
        $arreglo[$nro][4] = $cantidad;
        $arreglo[$nro][5] = $monto;



        //arreglo para el detalle entrada
        $dataArreglo = array(
           'nroProducto' => $nro,
           'arregloProducto' => $arreglo,
           'status' => 0,
        );    
      
        
        $this->session->set_userdata($dataArreglo);  
        $this->ajax_datatable_factura();
    }else{
        $data = array(
            "status"=>1
        );
        echo json_encode($data);
    } 
}//fin val session    
}

public function ajax_eliminar_producto($id){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){

    //validar si existe el producto
    $arreglo = $this->session->userdata("arregloProducto");
    $nro = $this->session->userdata("nroProducto");

    if($nro==1){
        $arregloProducto[0][0]="";
        $arregloProducto[0][1]="";
        $arregloProducto[0][2]="";
        $arregloProducto[0][3]="";
        $arregloProducto[0][4]="";
        $arregloProducto[0][5]="";

 
 
        $nro=0;
    }else{
        $i=1;
        $indice = 1;
        while($i <= $nro){
            if($i != $id){
                $arregloProducto[$indice][0]=$arreglo[$i][0];
                $arregloProducto[$indice][1]=$arreglo[$i][1];
                $arregloProducto[$indice][2]=$arreglo[$i][2];
                $arregloProducto[$indice][3]=$arreglo[$i][3];
                $arregloProducto[$indice][4]=$arreglo[$i][4];
                $arregloProducto[$indice][5]=$arreglo[$i][5];
                $indice = $indice + 1; 
            }    
            $i+=1;
        }  
        $nro = $nro - 1;
    }    

    //arreglo para el detalle entrada
    $dataArreglo = array(
        'nroProducto' => $nro,
        'arregloProducto' => $arregloProducto,
        'status'=>0
    );    
    $this->session->set_userdata($dataArreglo);  
    $this->ajax_datatable_factura();

}//fin val session
}

////////////////////////////////////////////////////////////////

public function ajax_guardar_facturaCp(){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){

    $fecha = $this->input->post("m_fecha");
    $idProveedor = $this->input->post("id_cbo_proveedorCp");
    $factura = $this->input->post("m_nroFactura");

    $monto = $this->input->post("m_monto");
    $monto = str_replace('.','',$monto);
    $monto = str_replace(',','.',$monto);


    $data=array(
        'fecha'=>$fecha,
        'factura'=>$factura, 
        'monto'=>$monto, 
        'administrador'=>$this->session->userdata("administrador"),
        'id_empresa'=>$this->session->userdata("idEmpresa"),
        'id_sucursal'=>$this->session->userdata("idSucursal"),
        'id_proveedor'=>$idProveedor,
        'id_create'=>$this->session->userdata("idUsuario")
    );

    $idFactura = $this->Cp_factura_mdl->guardar_facturaCp($data);
    
    
    //Guardar detalle

    $arreglo = $this->session->userdata('arregloProducto');
    $nro = $this->session->userdata('nroProducto');    

    $idEmpresa = $this->session->userdata("idEmpresa");
    $idSucursal = $this->session->userdata("idSucursal");

    $i=1;
    while($i <= $nro){
        $idProd = $arreglo[$i][0];
        $cantidad = $arreglo[$i][4];
        $cantidad = str_replace('.','',$cantidad);
        $cantidad = str_replace(',','.',$cantidad);

        $montoF = $arreglo[$i][5];
        $montoF = str_replace('.','',$montoF);
        $montoF = str_replace(',','.',$montoF);

        $data = array(
            'id_factura'=> $idFactura,
            'id_producto'=>$idProd,
            'cantidad'=>$cantidad,
            'monto'=>$montoF,
            'id_create'=>$this->session->userdata("idUsuario"),
        );
        $this->Cp_factura_mdl->guardar_detalleProducto($data);
        
        //afectar el inventario.
        
        $row = $this->In_inventario_mdl->obtInventarioProducto($idEmpresa, $idSucursal, $idProd);
		if($row){
		   $total = $row->entrada + $cantidad;
		   $data = array(
			   'entrada'=>$total 
		   );
		   $this->In_inventario_mdl->in_actTotProducto($idEmpresa, $idSucursal, $idProd, $data);
		}   
        
        
        $i+=1;
    }
    $data = array(
        'status'=>0
    );
    echo json_encode($data);
}//fin val session      
}


public function ajax_eliminar_factura($id){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){
    // eliminar detalle entrada
    $this->Cp_factura_mdl->eliminarDetalle($id);

    // eliminar Entrada inventario
    $this->Cp_factura_mdl->eliminarFactura($id);

    $row = $this->Cp_factura_mdl->obt_dataTable();
    $html = $this->generarDatatable($row);

    $data = array(
        "registro"=>$html);
    echo json_encode($data);
}//fin val session
}  



public function ajax_act_modificar_factura($id){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){

    $row = $this->Cp_factura_mdl->obtModificarFactura($id);
    
    $fecha = $row->fecha;
    $fecha = substr($fecha,0,10);
      
    $monto = $row->monto;
    $monto = str_replace(".",",", $monto );
    $data = array(
        'id'=>$row->id,
        'fecha'=>$fecha,
        'fecha_amd'=>$row->fecha,
        'id_proveedor'=>$row->id_proveedor,
        'factura'=>$row->factura,
        'monto'=>$monto,

    );
   
    echo json_encode($data);    
}//fin val session
}



public function ajax_act_modificar_producto($id){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){
	
    $row = $this->Cp_factura_mdl->obtModificarProducto($id);
    $cantidad = $row->cantidad;
    $cantidad = str_replace(".",",", $cantidad );
    $monto = $row->monto;
    $monto = str_replace(".",",", $monto);
    
    
     ////////disponibilidad
     $idEmpresa = $this->session->userdata('idEmpresa');
     $idSucursal = $this->session->userdata('idSucursal');
     
    $rowDisponible = $this->In_inventario_mdl->obtInventarioProducto($idEmpresa, $idSucursal, $row->id_producto);
    $total = $rowDisponible->entrada - $rowDisponible->salida;
    
    $data = array(
        'id_mod'=>$row->id,
        'idProducto'=>$row->id_producto,
        'cantidad'=>$cantidad,
        'montoF'=>$monto,
        'producto'=>$row->producto,
        'disponible'=>$total,
        'cantidad_original'=>$row->cantidad
    );
    echo json_encode($data);    
}//fin val session
}

function ajax_guardar_mod_factura (){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){

    $status = 0;
    $id_factura =  $this->input->post('id_mod');
    $fecha = $this->input->post("m_fecha");
    $idProveedor = $this->input->post("id_cbo_proveedorCp");
    $factura = $this->input->post("m_nroFactura");
    $monto = $this->input->post("m_monto");
    $monto = str_replace('.','',$monto);
    $monto = str_replace(',','.',$monto);    



    $data=array(
        'fecha'=>$fecha,
        'id_proveedor'=>$idProveedor,
        'factura'=>$factura,
        'monto'=>$monto,
        'id_update'=>$this->session->userdata("idUsuario"),
        'date_update'=>date('Y-m-d H:i:s')  
   );

    $this->Cp_factura_mdl->guardar_mod_factura($id_factura, $data);

    $row = $this->Cp_factura_mdl->obt_dataTable();
    $html= $this->generarDatatable($row);
    $data = array(
          "registro"=>$html,
          "status"=>0
    );      
    echo json_encode($data);    

}//fin val session   
}

public function ajax_guardar_add_productoCp($idFactura){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){

    $idProducto = $this->input->post("id_cbo_productoCp");
    $cantidad = $this->input->post("cantidad");
    $monto = $this->input->post("montoF");
    $status = 0;
    $status = $this->Cp_factura_mdl->valProducto($idFactura, $idProducto);
    if($status==0){
		$cantidad = str_replace('.','',$cantidad);
		$cantidad = str_replace(',','.',$cantidad);    
    
        $monto = str_replace('.','',$monto);
        $monto = str_replace(',','.',$monto);    

		$data = array(
			  'id_factura'=>$idFactura, 
			  'id_producto'=>$idProducto,
			  'cantidad'=>$cantidad, 
              'monto'=>$monto, 
			  'id_create'=>$this->session->userdata('idUsuario'),
		);
		$this->Cp_factura_mdl->guardar_producto_add($data);
		
		//aplicar
		
        //afectar el inventario.
        $idEmpresa = $this->session->userdata('idEmpresa');
        $idSucursal = $this->session->userdata('idSucursal');
        
        
        $row = $this->In_inventario_mdl->obtInventarioProducto($idEmpresa, $idSucursal, $idProducto);
		if($row){
		   $total = $row->entrada + $cantidad;
		   $data = array(
			   'entrada'=>$total 
		   );
		   $this->In_inventario_mdl->in_actTotProducto($idEmpresa, $idSucursal, $idProducto, $data);
		}   
		$row = $this->Cp_factura_mdl->obt_dataTableProducto($idFactura);
		$html= $this->generarDatatableProducto($row);
		$data = array(
			"registro"=>$html,
			"status"=>$status
		);
	}else{
		$data = array(
			"status"=>$status
		);
	}	
    echo json_encode($data);
}//fin val session    
}



public function act_modificar_producto($id){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){

   //accion 0 vista sin datatable, accion 1  activar datatable 
    $data=array(
        'accion'=>1,
        'modulo'=>3,
        'idFactura'=>$id,
    );
    
    $this->load->view('header');
    $this->load->view('menu');
    $this->load->view('compra/factura/act_productoFac',$data);
    $this->load->view('footer/footer', $data);
    $this->load->view('compra/factura/footer_factura', $data);
}//fin val session    
}


    

    function ajax_datatableFactura($id){
        //$this->load->model('sg_rolesUsuario_mdl');
        $row = $this->Cp_factura_mdl->obt_dataTableProducto($id);
        

        
        $html= $this->generarDatatableProducto($row);
        $data = array(
            "registro"=>$html);
        echo json_encode($data);    

    }   



public function disponibilidadReal($idProducto, $cantidad){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){

    $idEmpresa = $this->session->userdata('idEmpresa');
    $idSucursal = $this->session->userdata('idSucursal');
    $row = $this->In_inventario_mdl->obtInventarioProducto($idEmpresa, $idSucursal, $idProducto);
    $total = $row->entrada - $row->salida;
    if($total > $cantidad){
        return 0;
    }else{
        return 2;
    }
}//fin val session
}



public function guardar_mod_productoFactura($idFactura){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){

    //$idProducto = $this->input->post('id_cbo_productoCp');
    $idProductoOrig = $this->input->post('idProductoOrig');
    $idProducto=$idProductoOrig;
    
    
    $cantidad = $this->input->post("cantidad");
	$cantidad = str_replace(".", "", $cantidad);
	$cantidad = str_replace(",", ".", $cantidad);    

    
    $status=0;
    //if($idProducto != $idProductoOrig){
		
	//	$status = $this->Cp_factura_mdl->valProducto($idFactura, $idProducto);		
	//}
	
	if($status==0){
		$cantidad_original = $this->input->post('cantidad_original');
		$disponible=0.00;
		$operacion=0;
		if($cantidad_original<=$cantidad){
		   $disponible = $cantidad - $cantidad_original;	
		   $operacion = 1;
		}else{
			$disponible = $cantidad_original - $cantidad;
			$status = $this->disponibilidadReal($idProducto, $disponible);
			$operacion = 2;
		}
	}
    
    if($status==0){    
        $idDetalle = $this->input->post('id_mod');
		
        $monto = $this->input->post("montoF");


	//	$cantidad = str_replace(".", "", $cantidad);
	//	$cantidad = str_replace(",", ".", $cantidad);

        $monto = str_replace(".", "", $monto);
        $monto = str_replace(",", ".", $monto);


		$data = array(
			'cantidad'=>$cantidad,
            'monto'=>$monto,
			'id_producto'=>$idProducto,
			'id_update'=>$this->session->userdata('idUsuario'),
			'date_update'=>date('Y-m-d H:i:s') 
		);
		
		$this->Cp_factura_mdl->guardar_mod_producto($idDetalle,$data);
		
		//aplicar en inventario
        $idEmpresa = $this->session->userdata('idEmpresa');
        $idSucursal = $this->session->userdata('idSucursal');
        $row = $this->In_inventario_mdl->obtInventarioProducto($idEmpresa, $idSucursal, $idProducto);
		if($row){
			if($operacion==1){
				$total = $row->entrada + $disponible;
				$data = array(
					'entrada'=>$total 
				);
			}else{
				$total = $row->salida + $disponible;
				$data = array(
					'salida'=>$total 
				);
			}	
		    $this->In_inventario_mdl->in_actTotProducto($idEmpresa, $idSucursal, $idProducto, $data);
		}  		
		
		
		$row = $this->Cp_factura_mdl->obt_dataTableProducto($idFactura);
		$html= $this->generarDatatableProducto($row);
		$data = array(
          "registro"=>$html,
          "status"=>0
		);      
		echo json_encode($data);	
	}else{
		
		
		$data = array(
			"status"=>$status
		);
		echo json_encode($data);	
	}
	


}//fin val session
} 


public function ajax_borrar_producto(){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){

	$idDetalle = $_GET['id'];
	$idFactura = $_GET['idFactura'];
	
	$rowDetalle = $this->Cp_factura_mdl->obtModificar($idDetalle);
		$cantidad = $rowDetalle->cantidad;
		$idProducto = $rowDetalle->id_producto;
    $status=0;
    
    $status = $this->disponibilidadReal($idProducto, $cantidad);
    
    if($status==0){
		//aplicar en inventario
		$idEmpresa = $this->session->userdata('idEmpresa');
		$idSucursal = $this->session->userdata('idSucursal');
		$row = $this->In_inventario_mdl->obtInventarioProducto($idEmpresa, $idSucursal, $idProducto);
		if($row){
		
			$total = $row->salida + $cantidad;
			$data = array(
				'salida'=>$total 
			);
			$this->In_inventario_mdl->in_actTotProducto($idEmpresa, $idSucursal, $idProducto, $data);
		} 
		//Borrar detalle factura		 
		$resultado = $this->Cp_factura_mdl->eliminarProducto($idDetalle);		 
		$rowTabla = $this->Cp_factura_mdl->obt_dataTableProducto($idFactura);
		$html= $this->generarDatatableProducto($rowTabla);
		$data = array(
		    "registro"=>$html,
			"status"=>0
		);    
		echo json_encode($data); 
	}else{	
		$data = array(
			"status"=>1
		);    
		echo json_encode($data); 
	}	
}//fin val session
}

}  //fin de la clase
