<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cp_orden extends CI_Controller {


    public function __construct() {
         parent::__construct();
         $this->load->model(array('Cp_orden_mdl','Mt_producto_mdl', 'Cf_correlativo_mdl'));
         $this->load->library(array('Clssession')); 
    }


    public function index()
    {
        
        //accion 0 vista sin datatable, accion 1  activar datatable 
        $data=array(
            'accion'=>1,
            'modulo'=>1,
        );

        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('compra/ordenCompra/act_compra',$data);
        $this->load->view('footer/footer', $data);
        $this->load->view('compra/ordenCompra/footer_compra', $data);
    }


    function ajax_datatable($id){
        $row = $this->Cp_orden_mdl->obt_dataTable();
        $html= $this->generarDatatable($row);
        $data = array(
            "registro"=>$html);
        echo json_encode($data);    

    }   
    
    
    function generarDatatable($row){

        //obtener la información de la tabla seleccionada
        $modificarOrden = base_url() . "assets/images/factura.png";
        $modificarProducto = base_url() . "assets/images/producto02.jpeg";
        $eliminar = base_url() . "assets/images/eliminar.jpeg";
        $reactivar = base_url() . "assets/images/reactivar.jpeg";
        $lectura = base_url() . "assets/images/ver.png";
        
        $html = '<table id="basic-datatables" class="display table table-striped table-hover">';
        $html.= '    <thead>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="5%">#</th>';
        $html.= '           <th width="10%">Fecha</th>';
        $html.= '           <th width="20%">Nro orden</th>';        
        $html.= '           <th width="50%">Proveedor</th>';        
        $html.= '           <th width="15%">Acción</th>';
        $html.= '        </tr>';
        $html.= '    </thead>';
        $html.= '    <tfoot>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="5%">#</th>';        
        $html.= '           <th width="10%">Fecha</th>';
        $html.= '           <th width="20%">Nro orden</th>';        
        $html.= '           <th width="50%">Proveedor</th>';        
        $html.= '           <th width="15%">Acción</th>';
        $html.= '        </tr>';
        $html.= '    </tfoot>';
        $html.= '    <tbody>';
        if($row==false){
            $html.= '      <tr>';
            $html.= '          <td colspan="6">La Tabla no posee registros asociados</td>';
            $html.= '      </tr>';
        }else{
			$nro=0;
            foreach($row as $key){ 
				$nro+=1;
                $fecha = substr($key->fecha,0,10);
                $html.= '  <tr>';
                $html.= '      <td>' . $nro.'</td>';
                $html.= '      <td>' . $fecha.'</td>';
                $html.= '      <td>' . $key->nro_orden.'</td>';
                $html.= '      <td>' . $key->proveedor.'</td>';
                $html.= '      <td>';               

                if($this->clssession->accion(8,1)==1){ 

                    $html.='<a href="javascript:void(0)" onclick="javascript:ver_form('.$key->id.')">';
                    $html.= '<img src="'.$lectura.'" style="width:25px; height:25px" alt="Ver" ';
                    $html.= 'title="Ver Registro"></a>';
                }    

                if($this->clssession->accion(8,3)==1){ 
                $html.= '&nbsp;<a href="javascript:void(0)" onclick="javascript:modificar_orden('.$key->id.')">';    
                    
                    $html.= '<img src="'.$modificarOrden.'" style="width:30px; height:30px" alt="Modificar" ';
                    $html.= 'title="Actualizar Orden"></a>';
                }    

                if($this->clssession->accion(8,3)==1){ 
                    $html.= '&nbsp;<a href="javascript:void(0)" onclick="javascript:mod_producto('.$key->id.')">';
                    $html.= '<img src="'.$modificarProducto.'" style="width:30px; height:30px" alt="Modificar" ';
                    $html.= 'title="Actualizar Producto"></a>';
                }    


                if($this->clssession->accion(8,4)==1){ 

                  $html.='&nbsp;<a href="javascript:void(0)" onclick="javascript:eliminar_orden('.$key->id.')">';
                  $html.='&nbsp;<a href="javascript:void(0)" onclick="javascript:eliminar_orden('.$key->id.')">';
                  $html.= '<img src="'.$eliminar.'" style="width:25px; height:25px" alt="Ver" ';
                  $html.= 'title="Eliminar Orden"></a>';'></a>';
                }    

                $html.= '       </td>';             
                $html.= '</tr>';
            }
        }  

        $html.= '    </tbody>';
        $html.= '</table>';
        return $html;                   
    }


    public function incOrden(){
        $arregloProducto[0][0]="";
        $arregloProducto[0][1]="";
        $arregloProducto[0][2]="";
        $arregloProducto[0][3]="";
        $arregloProducto[0][4]="";

       
        //arreglo para el detalle entrada
        $dataArreglo = array(
            'nroProducto' => 0,
            'arregloProducto' => $arregloProducto,
            'status'=>0
        );    
        $this->session->set_userdata($dataArreglo);           
        $data=array(
            'accion'=>1,
            'modulo'=>2,
        );
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('compra/ordenCompra/incCompra',$data);
        $this->load->view('footer/footer', $data);
        $this->load->view('compra/ordenCompra/footer_compra', $data);
    }  


    public function verOrden($id){
		
		$rowOrden = $this->Cp_orden_mdl->verOrden($id);
		
		$arregloOrden[0] = $rowOrden->fecha;
		$arregloOrden[1] = $rowOrden->proveedor;
		$rowDetalle = $this->Cp_orden_mdl->obt_dataTableProducto($rowOrden->id);
		$i = 0;
		foreach($rowDetalle as $key){
			$arregloProducto[$i][0] = $key->id_producto;
			$arregloProducto[$i][1] = $key->cod_producto; 
			$arregloProducto[$i][2] = $key->producto;
			$arregloProducto[$i][3] = $key->unidad_medida;
			$arregloProducto[$i][4] = $key->cantidad;		
			$i+=1;
		}	
       
        //arreglo para el detalle entrada
        $dataArreglo = array(
            'arregloOrden'=>$arregloOrden,
            'nroProducto' => $i,
            'arregloProducto' => $arregloProducto,
            'status'=>0
        );    
        $this->session->set_userdata($dataArreglo);           
        $data=array(
            'accion'=>1,
            'modulo'=>4,
            'idOrden'=>$id
        );
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('compra/ordenCompra/verCompra',$data);
        $this->load->view('footer/footer', $data);
        $this->load->view('compra/ordenCompra/footer_compra', $data);
    }  








    //generar matriz detalle producto entrada
    public function ajax_datatable_orden(){
       $nro = $this->session->userdata("nroProducto");
        $arreglo = $this->session->userdata("arregloProducto");
        $eliminar = base_url() . "assets/images/eliminar.jpeg";

        $html = '<table class="table table-bordered table-head-bg-info ';
        $html.= 'table-bordered-bd-info mt-4">';
        $html.= '<thead>';
        $html.= '<tr>';
        $html.= '    <th scope="col" width="5%">#</th>';
        $html.= '    <th scope="col" width="10%" style="text-align:center">Cod.</th>';
        $html.= '    <th scope="col" width="45%">Producto</th>';
        $html.= '    <th scope="col" width="25%">Unidad Medida</th>';
        $html.= '    <th scope="col" width="10%" style="text-align:center">Cantidad</th>';
        $html.= '    <th scope="col" width="5%" style="text-align:center">Acción</th>';
        $html.= '</tr>';
        $html.= '</thead>';

        $html.= '<tbody>';        

        if($nro == 0){
            $html.= '</tr>';
            $html.= '    <td></td>';
            $html.= '    <td></td>';
            $html.= '    <td></td>';
            $html.= '    <td></td>';
            $html.= '    <td></td>';
            $html.= '    <td></td>';
            $html.= '</tr>';
        } 
        else{
            $i = 1;
            while($i <= $nro){
                $html.= '</tr>';
                $html.= '    <td>' .$arreglo[$i][0] . '</td>';
                $html.= '    <td style="text-align:center">' .$arreglo[$i][1] . '</td>';
                $html.= '    <td>' .$arreglo[$i][2] . '</td>';
                $html.= '    <td>' .$arreglo[$i][3] . '</td>';
                $html.= '    <td style="text-align:right">' .$arreglo[$i][4] . '</td>';

                $html.= '<td><a href="javascript:void(0)" onclick="javascript:eliminar_producto('.$i.')">';
                $html.= '<img src="'.$eliminar.'" style="width:25px; height:25px" alt="Eliminar"></a></td';
                $html.= '</tr>';
                $i+=1;
            }   
        }    
        $html.='</tbody>';
        $html.='</table>';
        $data = array(
            "registro"=>$html,
            "status"=>0,
            "nroProducto"=>$nro,
        );
         echo json_encode($data);        
    }

    //generar matriz detalle producto entrada
    public function ajax_datatable_verOrden(){
        $nro = $this->session->userdata("nroProducto");
        $arreglo = $this->session->userdata("arregloProducto");
        $arregloOrden = $this->session->userdata("arregloOrden");
        
        $eliminar = base_url() . "assets/images/eliminar.jpeg";

        $html = '<table class="table table-bordered table-head-bg-info ';
        $html.= 'table-bordered-bd-info mt-4">';
        $html.= '<thead>';
        $html.= '<tr>';
        $html.= '    <th scope="col" width="5%">#</th>';
        $html.= '    <th scope="col" width="10%" style="text-align:center">Cod.</th>';
        $html.= '    <th scope="col" width="50%">Producto</th>';
        $html.= '    <th scope="col" width="25%">Unidad Medida</th>';
        $html.= '    <th scope="col" width="10%" style="text-align:center">Cantidad</th>';
        $html.= '</tr>';
        $html.= '</thead>';

        $html.= '<tbody>';        

            $i = 0;
            while($i < $nro){
                $html.= '<tr>';
                $html.= '    <td>' .$i . '</td>';
                $html.= '    <td style="text-align:center">' .$arreglo[$i][1] . '</td>';
                $html.= '    <td>' .$arreglo[$i][2] . '</td>';
                $html.= '    <td>' .$arreglo[$i][3] . '</td>';
                $html.= '    <td style="text-align:right">' .$arreglo[$i][4] . '</td>';
                $html.= '</tr>';
                $i+=1;
            }   

        $html.='</tbody>';
        $html.='</table>';
        
        $proveedor = $arregloOrden[1];
        $fecha = $arregloOrden[0];
        $data = array(
            "registro"=>$html,
            "status"=>0,
            "nroProducto"=>$nro,
            "proveedor"=>$proveedor,
            "fecha"=>$fecha
        );
         echo json_encode($data);        
    }





public function ajax_guardar_productoCp(){
    $idProducto = $this->input->post("id_cbo_productoCp");
    $cantidad = $this->input->post("cantidad");

    //validar si existe el producto
    $arreglo = $this->session->userdata("arregloProducto");
    $nro = $this->session->userdata("nroProducto");

    $validar = 0;
    if($nro>0){
        $i=1;
        $swParar = 0;
        while($i <= $nro && $swParar==0){
            if($arreglo[$i][0]==$idProducto){
                $swParar=1;
            }
            $i+=1;
        }
        if($swParar==1){
            $validar = 1; 
        }
    }    

    if($validar==0){
        $rowProducto = $this->Mt_producto_mdl->obtProductoCp($idProducto);

        $nro+=1;

        $arreglo[$nro][0] = $idProducto;
        $arreglo[$nro][1] = $rowProducto->cod_producto; 
        $arreglo[$nro][2] = $rowProducto->nombre;
        $arreglo[$nro][3] = $rowProducto->unidad_medida;
        $arreglo[$nro][4] = $cantidad;


        //arreglo para el detalle entrada
        $dataArreglo = array(
           'nroProducto' => $nro,
           'arregloProducto' => $arreglo,
           'status' => 0,
        );    
      
        
        $this->session->set_userdata($dataArreglo);  
        $this->ajax_datatable_orden();
    }else{
        $data = array(
            "status"=>1
        );
        echo json_encode($data);
    } 
}    

public function ajax_eliminar_producto($id){
    //validar si existe el producto
    $arreglo = $this->session->userdata("arregloProducto");
    $nro = $this->session->userdata("nroProducto");

    if($nro==1){
        $arregloProducto[0][0]="";
        $arregloProducto[0][1]="";
        $arregloProducto[0][2]="";
        $arregloProducto[0][3]="";
        $arregloProducto[0][4]="";
 
 
        $nro=0;
    }else{
        $i=1;
        while($i <= $nro){
            if($i != $id){
                $arregloProducto[$i][0]=$arreglo[$i][0];
                $arregloProducto[$i][1]=$arreglo[$i][1];
                $arregloProducto[$i][2]=$arreglo[$i][2];
                $arregloProducto[$i][3]=$arreglo[$i][3];
                $arregloProducto[$i][4]=$arreglo[$i][4];
            }    
            $i+=1;
        }  
        $nro = $nro - 1;
    }    


    //arreglo para el detalle entrada
    $dataArreglo = array(
        'nroProducto' => $nro,
        'arregloProducto' => $arregloProducto,
        'status'=>0
    );    
    $this->session->set_userdata($dataArreglo);  
    $this->ajax_datatable_orden();

}

////////////////////////////////////////////////////////////////





public function ajax_guardar_ordenCp(){
    $fecha = $this->input->post("m_fecha");
    $idProveedor = $this->input->post("id_cbo_proveedorCp");
    
    //obtener correlativo de la orden por sucursal.
    $rowCorrelativo = $this->Cf_correlativo_mdl->obtCorrelativo();
    $idCorrelativo = $rowCorrelativo->orden_compra + 1;
    $data=array(
        'orden_compra' => $idCorrelativo
    );
    $this->Cf_correlativo_mdl->actCorrelativo($data);
    $nroOrden = "ORDEN-" . str_pad($idCorrelativo, 6, "0", STR_PAD_LEFT);
    $data=array(
        'fecha'=>$fecha,
        'nro_orden'=>$nroOrden, 
        'administrador'=>$this->session->userdata("administrador"),
        'id_empresa'=>$this->session->userdata("idEmpresa"),
        'id_sucursal'=>$this->session->userdata("idSucursal"),
        'id_responsable'=>$this->session->userdata("idUsuario"),
        'id_proveedor'=>$idProveedor,
        'id_create'=>$this->session->userdata("idUsuario")
    );

    $idOrden = $this->Cp_orden_mdl->guardar_ordenCp($data);
    
    
    //Guardar detalle

    $arreglo = $this->session->userdata('arregloProducto');
    $nro = $this->session->userdata('nroProducto');    


    $idEmpresa = $this->session->userdata("idEmpresa");
    $idSucursal = $this->session->userdata("idSucursal");


    $i=1;
    while($i <= $nro){
        $idProd = $arreglo[$i][0];
        $cantidad = $arreglo[$i][4];
        $cantidad = str_replace('.','',$cantidad);
        $cantidad = str_replace(',','.',$cantidad);
        $data = array(
            'id_orden'=> $idOrden,
            'id_producto'=>$idProd,
            'cantidad'=>$cantidad,
            'id_create'=>$this->session->userdata("idUsuario"),
        );
        $this->Cp_orden_mdl->guardar_detalleProducto($data);
        $i+=1;
    }
    $data = array(
        'status'=>0
    );
    echo json_encode($data);
}      


public function ajax_eliminar_orden($id){

    // eliminar detalle entrada
    $this->Cp_orden_mdl->eliminarDetalle($id);

    // eliminar Entrada inventario
    $this->Cp_orden_mdl->eliminarOrden($id);

    $row = $this->Cp_orden_mdl->obt_dataTable();
    $html = $this->generarDatatable($row);

    $data = array(
        "registro"=>$html);
    echo json_encode($data);
}  



public function ajax_act_modificar_orden($id){
    $row = $this->Cp_orden_mdl->obtModificarOrden($id);
    
    $fecha = $row->fecha;
    $fecha = substr($fecha,0,10);

    $data = array(
        'id'=>$row->id,
        'fecha'=>$fecha,
        'fecha_amd'=>$row->fecha,
        'id_proveedor'=>$row->id_proveedor,
        'orden'=>$row->nro_orden,
    );
   
    echo json_encode($data);    
}



public function ajax_act_modificar_producto($id){
    $row = $this->Cp_orden_mdl->obtModificarProducto($id);
    $cantidad = $row->cantidad;
    $cantidad = str_replace(".",",", $cantidad );
    $data = array(
        'id_mod'=>$row->id,
        'idProducto'=>$row->id_producto,
        'cantidad'=>$cantidad,
    );
    echo json_encode($data);    
}

function ajax_guardar_mod_orden (){
    $status = 0;
    $id_orden =  $this->input->post('id_mod');
    $fecha = $this->input->post("m_fecha");
    $idProveedor = $this->input->post("id_cbo_proveedorCp");

    $data=array(
        'fecha'=>$fecha,
        'id_proveedor'=>$idProveedor,
        'id_update'=>$this->session->userdata("idUsuario"),
        'date_update'=>date('Y-m-d H:i:s')  
   );

    $this->Cp_orden_mdl->guardar_mod_orden($id_orden, $data);

    $row = $this->Cp_orden_mdl->obt_dataTable();
    $html= $this->generarDatatable($row);
    $data = array(
          "registro"=>$html,
          "status"=>0
    );      
    echo json_encode($data);    

}   

public function ajax_guardar_add_productoCp($idOrden){
    $idProducto = $this->input->post("id_cbo_productoCp");
    $cantidad = $this->input->post("cantidad");
  
    $status = 0;
    
    $status = $this->Cp_orden_mdl->valProducto($idOrden, $idProducto);
    
    if($status==0){
		$cantidad = str_replace('.','',$cantidad);
		$cantidad = str_replace(',','.',$cantidad);    
    

		$data = array(
			  'id_orden'=>$idOrden, 
			  'id_producto'=>$idProducto,
			  'cantidad'=>$cantidad, 
			  'id_create'=>$this->session->userdata('idUsuario'),
		);
		$this->Cp_orden_mdl->guardar_producto_add($data);
		$row = $this->Cp_orden_mdl->obt_dataTableProducto($idOrden);
		$html= $this->generarDatatableProducto($row);
		$data = array(
			"registro"=>$html,
			"status"=>$status
		);
	}else{
		$data = array(
			"status"=>$status
		);
	}	
    echo json_encode($data);
}    




public function act_modificar_producto($id){
   //accion 0 vista sin datatable, accion 1  activar datatable 
    $data=array(
        'accion'=>1,
        'modulo'=>3,
        'idOrden'=>$id,
    );
    $this->load->view('header');
    $this->load->view('menu');
    $this->load->view('compra/ordenCompra/act_productoCp',$data);
    $this->load->view('footer/footer', $data);
    $this->load->view('compra/ordenCompra/footer_compra', $data);
}    


    

    function ajax_datatableOrden($id){
        //$this->load->model('sg_rolesUsuario_mdl');
        $row = $this->Cp_orden_mdl->obt_dataTableProducto($id);
        $html= $this->generarDatatableProducto($row);
        $data = array(
            "registro"=>$html);
        echo json_encode($data);    

    }   

    function generarDatatableProducto($row){

        //La variable rol trae los diferentes roles sin estar asociados a los modulos. 

        //obtener la información de la tabla seleccionada
        $modificar = base_url() . "assets/images/modificar02.jpeg";

        $eliminar = base_url() . "assets/images/eliminar.jpeg";
        $reactivar = base_url() . "assets/images/reactivar.jpeg";
        $lectura = base_url() . "assets/images/ver.png";



        $html = '<table id="basic-datatables" class="display table table-striped table-hover">';
        $html.= '    <thead>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="10%">cod. Producto</th>';
        $html.= '           <th width="50%">Producto</th>';        
        $html.= '           <th width="10%">Cantidad</th>';                
        $html.= '           <th width="15%">Acción</th>';
        $html.= '        </tr>';
        $html.= '    </thead>';
        $html.= '    <tfoot>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="10%">cod. Producto</th>';
        $html.= '           <th width="50%">Producto</th>';        
        $html.= '           <th width="10%">Cantidad</th>';                
        $html.= '           <th width="15%">Acción</th>';
        $html.= '        </tr>';
        $html.= '    </tfoot>';
        $html.= '    <tbody>';
        if($row==false){
            $html.= '      <tr>';
            $html.= '          <td colspan="5">La Tabla no posee registros asociados</td>';
            $html.= '      </tr>';
        }else{
            foreach($row as $key){ 
				$cantidad = str_replace(".",",", $key->cantidad );
				
                $html.= '  <tr>';
                $html.= '      <td>' . $key->cod_producto.'</td>';
                $html.= '      <td>' . $key->producto.'</td>';                
                $html.= '      <td>' . $cantidad.'</td>';                                

                $html.= '      <td>';               
                //$html.= '<a href="javascript:void(0)" onclick="javascript:modificar_form('.$key->id.')">';


                if($this->clssession->accion(7,3)==1){ 
                $html.='&nbsp;<a href="javascript:void(0)" onclick="javascript:mod_productoOrden('.$key->id.')">';$html.= '<img src="'.$modificar.'" style="width:30px; height:30px" alt="Modificar" ';
                    $html.= 'title="Modificar Producto"></a>';
                }    

                if($this->clssession->accion(7,4)==1){ 
                  $html.='&nbsp;<a href="javascript:void(0)" onclick="javascript:elim_producto('.$key->id.')">';
                  $html.= '<img src="'.$eliminar.'" style="width:25px; height:25px" alt="Eliminar" ';
                  $html.= 'title="Eliminar Producto"></a>';'></a>';
                }    

                $html.= '       </td>';             
                $html.= '</tr>';
            }
        }  

        $html.= '    </tbody>';
        $html.= '</table>';
        return $html;                   
    }



public function ajax_guardar_productoOrden($idOrden){

    $idProducto = $this->input->post("id_cbo_productoCp");
    $validar = $this->Cp_orden_mdl->valProductoOrden($idOrden, $idProducto);

    if($validar==0){

        $rowProducto = $this->Mt_producto_mdl->obtProductoIn($idProducto);
        $inventariar = $rowProducto->inventariar;  

        $cantidad = $this->input->post("cantidad");
        $cantidad = str_replace(".", "", $cantidad);
        $cantidad = str_replace(",", ".", $cantidad);

        $precio = $this->input->post("precio");
        if($precio==""){
           $precio="0.00";
        }else{
            $precio = str_replace(".", "", $precio);
            $precio = str_replace(",", ".", $precio);
        }    


        $data = array(
            'id_inventario'=>$idInventario,
            'id_producto'=>$idProducto,
            'id_tipo_operacion'=>1,
            'cantidad'=>$cantidad,
            'inventariar'=>$inventariar,
            'precio'=>$precio,
            'id_create'=>$this->session->userdata('idUsuario'),
        );

        $rowProducto = $this->In_entrada_mdl->guardar_detalleProducto($data);

        $dataArreglo = array(
           'status' => 0,
        );    
        $row = $this->In_entrada_mdl->obt_dataTableProducto($idInventario);
        $html= $this->generarDatatableProducto($row);
        $data = array(
            "registro"=>$html,
            "status"=>0
        );    
        echo json_encode($data); 
    }else{
        $data = array(
            "status"=>1
        );
        echo json_encode($data);
    } 
}    


public function ajax_borrar_producto($id){
	
	$arreglo = explode("aa", $id);
	$idProducto = $arreglo[0];
	$idDetalle = $arreglo[1];
	
	
	
	
    $this->Cp_orden_mdl->eliminarProducto($idProducto);
    
    
    $row = $this->Cp_orden_mdl->obt_dataTableProducto($idDetalle);
    $html= $this->generarDatatableProducto($row);
    $data = array(
        "registro"=>$html,
        "status"=>0
    );    
    echo json_encode($data); 
}


function ajax_guardar_mod_producto($idInventario){
    $status = 0;
    $id =  $this->input->post('id_mod');

    $cantidad = $this->input->post("cantidadMod");
    $cantidad = str_replace(".", "", $cantidad);
    $cantidad = str_replace(",", ".", $cantidad);

    $precio = $this->input->post("precioMod");
    if($precio==""){
       $precio="0.00";
    }else{
        $precio = str_replace(".", "", $precio);
        $precio = str_replace(",", ".", $precio);
    }

    $data=array(
        'cantidad'=>$cantidad,
        'precio'=>$precio,
        'id_update'=>$this->session->userdata("idUsuario"),
        'date_update'=>date('Y-m-d H:i:s')  
    );

    $this->In_entrada_mdl->guardar_mod_producto($id, $data);

    $idInventario = $this->input->post('id_inventarioMod');

    $row = $this->In_entrada_mdl->obt_dataTableProducto($idInventario);
    $html= $this->generarDatatableProducto($row);
    $data = array(
          "registro"=>$html,
          "status"=>0
    );      
    echo json_encode($data);    

}  


public function guardar_mod_productoOrden($idOrden){
    $idProducto = $this->input->post('id_cbo_productoCp');
    $idProductoOrig = $this->input->post('idProductoOrig');

    
    $status=0;
    if($idProducto != $idProductoOrig){
		$status = $this->Cp_orden_mdl->valProducto($idOrden, $idProducto);		
	}
    
    if($status==0){    
        $idDetalle = $this->input->post('id_mod');
		$cantidad = $this->input->post("cantidad");
		$cantidad = str_replace(".", "", $cantidad);
		$cantidad = str_replace(",", ".", $cantidad);

		$data = array(
			'cantidad'=>$cantidad,
			'id_producto'=>$idProducto,
			'id_update'=>$this->session->userdata('idUsuario'),
			'date_update'=>date('Y-m-d H:i:s') 
		);
		
		$this->Cp_orden_mdl->guardar_mod_producto($idDetalle,$data);
		$row = $this->Cp_orden_mdl->obt_dataTableProducto($idOrden);
		$html= $this->generarDatatableProducto($row);
		$data = array(
          "registro"=>$html,
          "status"=>0
		);      
		echo json_encode($data);	
	}else{
		$data = array(
			"status"=>$status
		);
		echo json_encode($data);	
	}
	



} 








}  //fin de la clase
