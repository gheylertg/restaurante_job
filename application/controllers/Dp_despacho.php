<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once __DIR__ . '/../../dompdf/autoload.inc.php';
use Dompdf\Dompdf;

class Dp_despacho extends CI_Controller {

	
	public function __construct() {
         parent::__construct();
         $this->load->model(array('Dp_despacho_mdl', 'Cf_configuracion_mdl','Cf_correlativo_mdl','Mn_alimento_mdl','Dp_despachoDetalle_mdl','Mn_presentacionIngrediente_mdl','Mn_ingredienteAdicional_mdl','Mn_ingrediente_mdl'));
         $this->load->library(array('Clssession')); 
    }




	public function index(){
    //Validar session usuario
    $valSession = $this->clssession->valSession();
    if($valSession==true){
        //Validar apertura de caja
        $this->load->model('Cj_aperturaCaja_mdl');
        $idUsuario = $this->session->userdata('idUsuario');
        $idEmpresa = $this->session->userdata('idEmpresa');
        $idSucursal = $this->session->userdata('idSucursal');        
        $status = $this->Cj_aperturaCaja_mdl->val_apertura($idUsuario, $idEmpresa, $idSucursal);
        if($status==0){       
            $dataSession = array(
                'idCaja'=>0
            );
            $this->session->set_userdata($dataSession);
			$data=array(
				'accion'=>2
			);
	    	$this->load->view('header');
			$this->load->view('menu');
			$this->load->view('footer/footer', $data);
			$this->load->view('despacho/despacho/footer_despacho', $data);
            $this->load->view('footer/lib_numerica');


	    }else{	
	        //accion 0 vista sin datatable, accion 1  activar datatable 
            $dataSession = array(
                'despacho_tipo'=>1 
            );
            $this->session->set_userdata($dataSession);

 			$data=array(
				'accion'=>1
			);

	    	$this->load->view('header');
			$this->load->view('menu');
			$this->load->view('despacho/despacho/act_despacho',$data);
			$this->load->view('footer/footer', $data);
			$this->load->view('despacho/despacho/footer_despacho', $data);
			$this->load->view('footer/lib_numerica');
		}

    }//fin validar session
	}//fin function	


    function ajax_datatable($id){
    $valSession = $this->clssession->valSession();
    if($valSession==true){
        $tipoDespacho = $this->session->userdata("despacho_tipo");

        //obtener nro de pedido activos
        $tipo_1 = 0;
        $tipo_2 = 0;  
        $tipo_3 = 0;

        $rowTipoDespacho = $this->Dp_despacho_mdl->obt_nroDespacho();
        foreach($rowTipoDespacho as $key){
            switch($key->id_tipo_despacho){
            case 1:
               $tipo_1 = $key->cantidad; 
               break;
            case 2:
               $tipo_2 = $key->cantidad; 
               break;
            case 3:
               $tipo_3 = $key->cantidad; 
               break;
            }
        } 

        $row = $this->Dp_despacho_mdl->obt_dataTable($tipoDespacho);
        $html= $this->generarDatatable($row);
        $data = array(
            "registro"=>$html,
            "tipo_1"=>$tipo_1,
            "tipo_2"=>$tipo_2,
            "tipo_3"=>$tipo_3,
            "tipoDespacho"=>$tipoDespacho,
            "nombreEmpresa"=>$this->session->userdata('datoSucursal')
        );
        echo json_encode($data);    
    } //fin validar session 
    }



    function imprimir_preCuenta($idDespacho){
        $dompdf = new DOMPDF(array('enable_remote' => true));

        if($idDespacho==0){
            $idDespacho = $this->session->userdata('idDespacho');
        }     

        $rowDespacho = $this->Dp_despacho_mdl->infDespacho($idDespacho); 
        $rowFactura = $this->Dp_despacho_mdl->obt_detalleFactura($idDespacho);
        $data = array(
           'rowDespacho'=>$rowDespacho,
           'rowFactura' => $rowFactura,
           'empresa'=>$this->session->userdata('empresa')
        );

        $codigoHTML = $this->load->view('pdf/pdf_despacho/pdf_preCuenta', $data  , TRUE );
        $dompdf->loadHtml($codigoHTML);
        //$dompdf->setPaper('A4', 'landscape');
        $dompdf->set_paper("letter", "portrait");
        // Render the HTML as PDF
        $dompdf->render();
        $nombreArchivo = "preCuenta". date("Y-m-d-H-i-s").".pdf";  


        $dompdf->stream($nombreArchivo,array('Attachment'=>1));






    }


    function generarDatatable($row){
        //obtener la información de la tabla seleccionada
        $despachoCliente = base_url() . "assets/images/despacho/despacho02.jpeg";
        $cerrarCuenta = base_url() . "assets/images/despacho/cerrarCuenta.jpeg";
        $reverso = base_url() . "assets/images/eliminar.jpeg";
        $factura = base_url() . "assets/images/factura.png";

        $tipoDespacho = $this->session->userdata('despacho_tipo');   


        $html = '<table id="basic-datatables" class="display table table-striped table-hover">';
        $html.= '    <thead>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="9%">Código Venta</th>';
        $html.= '           <th width="12%">Tipo Despacho</th>';
        
        if($tipoDespacho==1){
            $html.= '           <th width="26%">Sala / Mesa / Mesonero</th>';
        }else{
            $html.= '           <th width="26%">Cliente / Teléfono</th>';
        }    

        $html.= '           <th width="8%">monto</th>';
        $html.= '           <th width="8%">Impuesto</th>';
        $html.= '           <th width="8%">Total</th>';
        $html.= '           <th width="13%">Estado Despacho</th>';
        $html.= '           <th width="16%">Acción</th>';
        $html.= '        </tr>';
        $html.= '    </thead>';


        $html.= '    <tfoot>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="9%">Código Venta</th>';
        $html.= '           <th width="12%">Tipo Despacho</th>';
        if($tipoDespacho==1){
            $html.= '           <th width="26%">Sala / Mesa / Mesonero</th>';
        }else{
            $html.= '           <th width="26%">Cliente / Teléfono</th>';
        }    
        $html.= '           <th width="8%">monto</th>';
        $html.= '           <th width="8%">Impuesto</th>';
        $html.= '           <th width="8%">Total</th>';
        $html.= '           <th width="13%">Estado Despacho</th>';
        $html.= '           <th width="16%">Acción</th>';
        $html.= '        </tr>';
        $html.= '    </tfoot>';
        $html.= '    <tbody>';
        if($row==false){
            $html.= '      <tr>';
            $html.= '          <td></td><td></td><td></td><td></td>';
            $html.= '          <td></td><td></td><td></td><td></td>';
            $html.= '      </tr>';
        }else{
            //$rowConfiguracion = $this->Cf_configuracion_mdl->obtDatosSucursal();
            //$impuesto = $rowConfiguracion->impuesto;
            foreach($row as $key){ 
                $html.= '  <tr>';
                $html.= '      <td>' . $key->codigo_venta.'</td>';                
                $html.= '      <td>' . $key->tipo_despacho.'</td>';
                
                $salaMesa = "";
                if($key->id_tipo_despacho==1){
                    $mesonero = trim($key->nob_mesonero). ", " . trim($key->ape_mesonero);
                    $salaMesa = trim($key->sala) . " / " . trim($key->mesa) . " / " . trim($mesonero);
                }else{
                    $salaMesa = trim($key->nombre_cliente) . "  / Telef.: " .trim($key->telefono_cliente);
                }   
                $html.= '      <td>' . $salaMesa. '</td>';
                $monto = number_format ($key->monto, 2,',','.');
                $html.= '      <td style="text-align:right">' . $monto.'</td>';
                $totalImpuesto =  $key->monto * ($key->impuesto/100);
                $impuestoF = number_format ($totalImpuesto, 2,',','.');
                $html.= '      <td style="text-align:right">' . $impuestoF .'</td>';
                $total = $key->monto + $totalImpuesto;
                $totalF = number_format ($total, 2,',','.');
                $html.= '      <td style="text-align:right">' . $totalF .'</td>';
                $html.= '      <td>' . $key->estado_despacho. '</td>';
                $html.= '      <td>';       

                if($key->id_estado_despacho==1){
                    $html.= '<a href="javascript:void(0)" onclick="javascript:despachoCliente('.$key->id.')">';
                    $html.= '<img src="'.$despachoCliente.'" style="width:35px; height:35px" title="Despacho Cliente" alt="Despacho Cliente"></a>';

                    $html.= '&nbsp;<a href="javascript:void(0)" onclick="javascript:reversarDespacho('.$key->id.')">';
                    $html.= '<img src="'.$reverso.'" style="width:35px; height:35px" title="Reversar Despacho" alt="Reversar Despacho"></a>';

                    $html.= '&nbsp;<a href="javascript:void(0)" onclick="javascript:precuentaDespacho('.$key->id.')">';
                    $html.= '<img src="'.$factura.'" style="width:35px; height:35px" title="Pre Cuenta" alt="Pre Cuenta"></a>';



                    $html.= '&nbsp;<a href="javascript:void(0)" onclick="javascript:cerrarDespacho('.$key->id.')">';
                    $html.= '<img src="'.$cerrarCuenta.'" style="width:35px; height:35px" title="Cerrar Cuenta" alt="Cerrar Cuenta"></a>';
                }
            }
                    
                $html.= '       </td>';             
                $html.= '</tr>';
        }
        $html.= '    </tbody>';
        $html.= '</table>';
        return $html;                   
    }





//Cargar deespacho actual al pulsar boton de tipo despacho
public function ajax_despachoActual($tipoDespacho){    
$valSession = $this->clssession->valSession();

//mantener variable de session del tipo despacho actual
if($valSession==true){
    $dataSession = array(
        'despacho_tipo'=>$tipoDespacho
    );
    $this->session->set_userdata($dataSession);    

    //obtener nro de pedido activos
    $tipo_1 = 0;
    $tipo_2 = 0;  
    $tipo_3 = 0;

    $rowTipoDespacho = $this->Dp_despacho_mdl->obt_nroDespacho();
    foreach($rowTipoDespacho as $key){
        switch($key->id_tipo_despacho){
        case 1:
           $tipo_1 = $key->cantidad; 
           break;
        case 2:
           $tipo_2 = $key->cantidad; 
           break;
        case 3:
           $tipo_3 = $key->cantidad; 
           break;
        }
    } 

    $row = $this->Dp_despacho_mdl->obt_dataTable($tipoDespacho);
    $html= $this->generarDatatable($row);
    $data = array(
        "registro"=>$html,
        "tipo_1"=>$tipo_1,
        "tipo_2"=>$tipo_2,
        "tipo_3"=>$tipo_3,
        "tipoDespacho"=>$tipoDespacho,
    );
    echo json_encode($data);    
} //fin validar session 
}



/////////////// despacho en mesa /////////////////////



////////////////  fin despacho en mesa ///////////////



////////////////// retiro en tienda ////////////////////


//////////////////  retiro en tienda ///////////////////



////////////// delivery //////////////////////////
public function ajax_inicializarDelivery(){
    $dataSession = array(
        'despacho_tipo'=>3 
    );
    $this->session->set_userdata($dataSession);

    $data = array(
        'status'=>true
    );
    echo json_encode($data); 
}

////////// fin delivery ///////////////////////




/////// generar pedido /////////////////
    public function despachoActivo(){
    //Validar session usuario
    $valSession = $this->clssession->valSession();
    if($valSession==true){
        //Validar apertura de caja



        /*if($this->session->userdata('idCaja')==0){
            $data=array(
                'accion'=>2
            );
            $this->load->view('header');
            $this->load->view('menu');
            $this->load->view('footer/footer', $data);
            $this->load->view('despacho/despacho/footer_despacho', $data);
        }else{  
        */    
            //accion 0 vista sin datatable, accion 1  activar datatable 
            $data=array(
                'accion'=>1
            );

            $this->load->view('header');
            $this->load->view('menu');
            $this->load->view('despacho/despacho/act_despacho',$data);
            $this->load->view('footer/footer', $data);
            $this->load->view('despacho/despacho/footer_despacho', $data);
            //$this->load->view('footer/lib_numerica');
        //}

    }//fin validar session
    }//fin function 



/////// fin generar pedido /////////////////



    public function cargaRetiro(){
    //Validar session usuario
    $valSession = $this->clssession->valSession();
    if($valSession==true){
        //Validar apertura de caja
        /*if($this->session->userdata('idCaja')==0){
            $data=array(
                'accion'=>2
            );
            $this->load->view('header');
            $this->load->view('menu');
            $this->load->view('footer/footer', $data);
            $this->load->view('despacho/despacho/footer_despacho', $data);
        }else{  
        */    
            //accion 0 vista sin datatable, accion 1  activar datatable 
            $dataSession = array(
               'despacho_tipo'=> 2
            );
            $this->session->set_userdata($dataSession);
 
            $data=array(
                'accion'=>1
            );

            $this->load->view('header');
            $this->load->view('menu');
            $this->load->view('despacho/despacho/act_despacho',$data);
            $this->load->view('footer/footer', $data);
            $this->load->view('despacho/despacho/footer_despacho', $data);
            //$this->load->view('footer/lib_numerica');
        //}

    }//fin validar session
    }//fin function 

    public function cargaDelivery(){
    //Validar session usuario
    $valSession = $this->clssession->valSession();
    if($valSession==true){
        //Validar apertura de caja
        /*if($this->session->userdata('idCaja')==0){
            $data=array(
                'accion'=>2
            );
            $this->load->view('header');
            $this->load->view('menu');
            $this->load->view('footer/footer', $data);
            $this->load->view('despacho/despacho/footer_despacho', $data);
        }else{  
        */    
            //accion 0 vista sin datatable, accion 1  activar datatable 
            $dataSession = array(
               'despacho_tipo'=> 3
            );
            $this->session->set_userdata($dataSession);
 
            $data=array(
                'accion'=>1
            );

            $this->load->view('header');
            $this->load->view('menu');
            $this->load->view('despacho/despacho/act_despacho',$data);
            $this->load->view('footer/footer', $data);
            $this->load->view('despacho/despacho/footer_despacho', $data);
            //$this->load->view('footer/lib_numerica');
        //}

    }//fin validar session
    }//fin function 



    function nroDelivery(){
        $tipo = 0;

        $rowTipoDespacho = $this->Dp_despacho_mdl->obt_nroDespacho();
        foreach($rowTipoDespacho as $key){
            switch($key->id_tipo_despacho){
            case 3:
               $tipo = $key->cantidad; 
               break;
            }
        } 

        $data = array(
            "tipo"=>$tipo,
        );
        echo json_encode($data);    
    }






function ajax_generarDespachoDM(){
//validar variables de session.    
$valSession = $this->clssession->valSession();
if($valSession==true){
    //obtener codigo de venta
    $dataSession = array(
        'despacho_tipo'=> 1
    );
    $this->session->set_userdata($dataSession);

    $rowCodigoVenta = $this->Cf_correlativo_mdl->obtCorrelativo();
    $codigoVenta = $rowCodigoVenta->codigo_venta + 1;

    $rowConfiguracion = $this->Cf_configuracion_mdl->obtDatosSucursal();       
    $impuesto = $rowConfiguracion->impuesto;
    //Generar despacho
    $data = array(
        'codigo_venta'=>str_pad($codigoVenta, 7, "0", STR_PAD_LEFT),
        'id_sala'=>$this->input->post('id_cbo_sala'),
        'id_mesa'=>$this->input->post('id_cbo_mesa'),
        'id_mesonero'=>$this->input->post('id_cbo_mesonero'),
        'id_tipo_despacho'=>1,
        'impuesto'=>$impuesto,
        'id_empresa'=>$this->session->userdata('idEmpresa'),
        'id_sucursal'=>$this->session->userdata('idSucursal'),
        'id_estado_despacho'=>1,
        'id_create'=>$this->session->userdata('idUsuario'),
    );

    $idDespacho = $this->Dp_despacho_mdl->generarDespacho($data);

    //actualizar el correlativo venta
    $data=array(
        'codigo_venta' => $codigoVenta
    );
    $this->Cf_correlativo_mdl->actCorrelativo($data);


    //crear idDespacho como variable de session
    $dataSession = array(
        'idDespacho'=>$idDespacho
    );
    $this->session->set_userdata($dataSession);

    // obtener informacion de la sala, mesa y mesonero
    $rowSalaMesa = $this->Dp_despacho_mdl->infDespacho($idDespacho);
    $data = array(
        'codigoVenta'=>$rowSalaMesa->codigo_venta,
        'sala'=>$rowSalaMesa->sala,
        'mesa'=>$rowSalaMesa->mesa,
        'mesonero'=>trim($rowSalaMesa->nob_mesonero) . ", " . trim($rowSalaMesa->ape_mesonero)
    );   

    echo json_encode($data); 

} // fin validar session
}


public function despachoCliente(){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){
    $data=array(
        'accion'=>3
    );

    $this->load->view('header');
    $this->load->view('menu');
    $this->load->view('despacho/despacho/despachoCliente',$data);
    $this->load->view('footer/footer', $data);
    $this->load->view('despacho/despacho/footer_despacho', $data);
    $this->load->view('footer/lib_numerica');


} // fin val session    
}


public function ajax_inicializarVariableDespacho($idDespacho){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){
    //crear idDespacho como variable de session
    $dataSession = array(
        'idDespacho'=>$idDespacho
    );
    $this->session->set_userdata($dataSession);
    $data = array(
        'status'=>1,
    );   
    echo json_encode($data); 
}  //fin validar session
}

public function ajax_despachoCliente(){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){

///Datos del despacho
   $rowDespacho = $this->Dp_despacho_mdl->infDespacho($this->session->userdata('idDespacho'));
   $titulo = $rowDespacho->tipo_despacho;

   if($rowDespacho->id_tipo_despacho==1){
       $mesonero = trim($rowDespacho->nob_mesonero) . ", " . trim($rowDespacho->ape_mesonero);
       $datoDespacho = '<span style="color:blue">SALA: </span>&nbsp;' . trim($rowDespacho->sala);
        $datoDespacho.= '<span style="color:blue">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MESA: &nbsp; </span>' . trim($rowDespacho->mesa);
        $datoDespacho.= '<span style="color:blue">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MESONERO: &nbsp; </span>' . trim($mesonero);
   }else{
        $datoDespacho='<span style="color:blue">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLIENTE: &nbsp; </span>' . $rowDespacho->nombre_cliente;
   }     

//// Construir categoria de menu
    $this->load->model('Mn_categoriaMenu_mdl');
    $rowCategoriaMenu = $this->Mn_categoriaMenu_mdl->obt_categoriaOrden();
    $nroCategoria = 0;
    foreach ($rowCategoriaMenu as $key) {
    $nroCategoria+=1;

    }    

    if($nroCategoria < 6){
    $height = "160px";
    }elseif($nroCategoria < 11){
    $height = "200px";
    }else{
    $height = "250px";
    }    

    $html = '<div class="card card-stats card-round">';
    $html.= '<div class="card-body" style="margin-bottom:-30px; height:'.$height.'">';
    $html.= '   <h2><span style="color:blue">Categoría Menú </span></h2>';
    $html.= '       <div class="row">';
    $html.= '          <div class="col-md-12">';

    $html.= '<table class="width:100%">';
    $i=0;
    $columna=0;

    foreach ($rowCategoriaMenu as $key) {
    $i+=1;
    $columna+=1;
    if($columna==1){
        $html.= '<tr>';
    }    
    $html.= '<td style="width:20%">';
    $html.= '    <button class="btn btn-primary btn-round" style="width:100%; height:50px; ';
    $html.= '        align-text:center; padding-left: 10px; padding-right: 10px;" id="boton-"' . $i .'  onclick="categoriaMenu_seleccion(' .$key->id. ', '.$i.')">';
    $html.= $key->nombre_menu; 
    $html.= '</button>';
    $html.= '</td>';

    if($columna==5){
        $html.= '</tr>';
        $columna=0;
    }     
    }

    if($columna==1){
    $html.= '<td style="width:20%"></td><td style="width:20%"></td><td style="width:20%"></td><td style="width:20%"></td>';
    $html.= '</tr>';
    }

    if($columna==2){
    $html.= '<td style="width:20%"></td><td style="width:20%"></td><td style="width:20%"></td>';
    $html.= '</tr>';
    }

    if($columna==3){
    $html.= '<td style="width:20%"></td><td style="width:20%"></td>';
    $html.= '</tr>';
    }

    if($columna==4){
    $html.= '<td style="width:20%"></td>';
    $html.= '</tr>';
    }

    $html.= '</table>';
    $html.= '</div>';
    $html.= '</div>';
    $html.= '</div>';
    $html.= '</div>';
    $html.= '<input type="hidden" id="nroBotonCategoria" value="'.$nroCategoria.'">';


    $htmlA = '<div class="card card-stats card-round">';
    $htmlA.= '    <div class="card-body" style="margin-bottom:-30px; height:160px">';
    $htmlA.= '      <h2><span style="color:blue">Productos </span></h2>';
    $htmlA.= '          <div class="row">';
    $htmlA.= '              <div class="col-md-12">';
    $htmlA.= '              </div>';
    $htmlA.= '          </div>';
    $htmlA.= '   </div>';
    $htmlA.= '</div>';

    //Generar Factura.
    $arreglo = $this->generarFactura($this->session->userdata('idDespacho'));
    $htmlF = $arreglo[0];
    $precio = $arreglo[1];
    $rowConfiguracion = $this->Cf_configuracion_mdl->obtDatosSucursal();
    $impuesto = $rowConfiguracion->impuesto;
    $totalImpuesto = $precio * ($impuesto/100);
    $total = $precio + $totalImpuesto;    
  
    $impuesto = number_format ($impuesto, 2,',','.');
    $precio = number_format ($precio, 2,',','.');
    $totalImpuesto = number_format ($totalImpuesto, 2,',','.');
    $total = number_format ($total, 2,',','.');


    //validar nro de pedido para activar botones
    $nroBoton = $this->Dp_despacho_mdl->val_nroDetalle($this->session->userdata('idDespacho'));


    $data = array(
    'boton'=>$nroBoton,
    'titulo'=>$titulo,  
    'datoDespacho'=>$datoDespacho,  
    'precio'=>$precio,
    'impuesto'=>$impuesto,
    'totalImpuesto'=>$totalImpuesto,
    'total'=>$total,
    'factura'=>$htmlF,
    'categoria'=>$html,
    'alimento'=>$htmlA,
    );

    echo json_encode($data);

} //fin val session    
}


public function ajax_cerrarDespacho($idDespacho){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){
    $data = array(
       'despachado'=>1,
       'id_estado_despacho'=>4,
       'cobrar_nuevo'=>1
    );
  
    if($idDespacho==0){
        $this->Dp_despacho_mdl->cerrarDespacho($this->session->userdata('idDespacho'),$data);
    }else{
        $this->Dp_despacho_mdl->cerrarDespacho($idDespacho,$data);
    }    
    $data = array(
        'status'=>0
    );
    echo json_encode($data);

} //Fin val session
}


function ajax_mostrarAlimento($idCategoria){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){

    //obtener variable de session de la categoria del menu 
    $dataSession = array(
        'idCategoriaMenu'=>$idCategoria,
    );
    $this->session->set_userdata($dataSession);


    $rowAlimento = $this->Mn_alimento_mdl->obt_alimentoCategoria($idCategoria);
    
    
   
    
    
    $nroAlimento = 0;
    foreach ($rowAlimento as $key) {
        if($key->presentacion>0){
            $nroAlimento+=1;
        }    
    }    

    if($nroAlimento==0){
        $htmlA = "";
        $data = array(
            'alimento'=>$htmlA,
            'status'=>1
        );

        echo json_encode($data);
    }else{
        if($nroAlimento < 6){
            $height = "160px";
        }elseif($nroAlimento < 11){
            $height = "200px";
        }elseif($nroAlimento < 16){
            $height = "250px";
        }else{
            $height = "300px";
        }



        $htmlA = '<div class="card card-stats card-round">';
        $htmlA.= '    <div class="card-body" style="margin-bottom:-30px; height:'.$height.'">';
        $htmlA.= '      <h2><span style="color:blue">Productos </span></h2>';
        $htmlA.= '          <div class="row">';
        $htmlA.= '              <div class="col-md-12">';



        $htmlA.= '<table class="width:100%">';
        $i=0;
        $columna=0;

        foreach ($rowAlimento as $key) {
        if($key->presentacion>0){    
            $i+=1;
            $columna+=1;
            if($columna==1){
                $htmlA.= '<tr>';
            }    
            $htmlA.= '<td style="width:20%">';
            $htmlA.= '    <button class="btn btn-warning btn-round" style="width:100%; height:50px; ';
            $htmlA.= '        align-text:center; padding-left: 10px; padding-right: 10px;" id="botonA-"' . $i .'  onclick="cargarAlimento(' .$key->id. ','.$idCategoria.' )">';
            $htmlA.= $key->nombre_menu; 
            $htmlA.= '</button>';
            $htmlA.= '</td>';

            if($columna==5){
                $htmlA.= '</tr>';
                $columna=0;
            }     
        }    
        }

        if($columna==1){
            $htmlA.= '<td style="width:20%"></td><td style="width:20%"></td><td style="width:20%"></td><td style="width:20%"></td>';
            $htmlA.= '</tr>';
        }

        if($columna==2){
            $htmlA.= '<td style="width:20%"></td><td style="width:20%"></td><td style="width:20%"></td>';
            $htmlA.= '</tr>';
        }

        if($columna==3){
            $htmlA.= '<td style="width:20%"></td><td style="width:20%"></td>';
            $htmlA.= '</tr>';
        }

        if($columna==4){
            $htmlA.= '<td style="width:20%"></td>';
            $htmlA.= '</tr>';
        }


        $htmlA.= '</table>';
        $htmlA.= '              </div>';
        $htmlA.= '          </div>';
        $htmlA.= '   </div>';
        $htmlA.= '</div>';

        $data = array(
            'alimento'=>$htmlA,
            'status'=>0
        );

        echo json_encode($data);
    } //fin del if nro alimentos asociados    

} //fin val session
}



function ajax_prepara_sushi($idAlimento){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){



    $dataSession = array(
        'idAlimento'=>$idAlimento
    );
    $this->session->set_userdata($dataSession);

    $swValidar = 0;
    $nroElemento = 0;

    //validar cantidad real del contenedor
    $this->load->model('Co_contenedor_mdl');
    $rowContenedor = $this->Co_contenedor_mdl->obt_alimentoContenedor($idAlimento);

    $cantidad = $rowContenedor->cantidad;
    if($cantidad < 6){
        $swValidar = 1;
    }else{
        $this->load->model('Mn_presentacionIngrediente_mdl');
        $rowPresentacion = $this->Mn_presentacionIngrediente_mdl->mostrarPresentacion($idAlimento);
        $pieza = 0;
        foreach ($rowPresentacion as $key){
            if($pieza == 0){
                $pieza = $key->nro_pieza;
            }else{
                if($pieza > $key->nro_pieza){
                     $pieza = $key->nro_pieza;
                }
            }    
        }
        if($cantidad < $pieza){
                $swValidar = 1;
        }
    }    

    //if($swValidar==0){

        ///presentacion

        $htmlP = $this->presentacion($idAlimento);

        //// Envoltura
        $htmlE = $this->envoltura();

        //// Ingrediente

        $htmlI = $this->ingrediente($idAlimento);

        $htmlADIC = $this->ingrediente_adic();        

        $data = array(
            'status'=>0,
            'validar'=>$swValidar,
            'presentacion'=>$htmlP,
            'envoltura'=>$htmlE,
            'ingrediente'=>$htmlI,
            'ingredienteAdic'=>$htmlADIC,

        );

        echo json_encode($data);
    //}else{
    //    $data = array(
    //        'status'=>1,
    //    );

    //    echo json_encode($data);
    //}

}//fin de val session
}

function presentacion($idAlimento){
$this->load->model('Mn_presentacionIngrediente_mdl');
$rowPresentacion = $this->Mn_presentacionIngrediente_mdl->mostrarPresentacion($idAlimento);

$nroElemento = 0;
foreach ($rowPresentacion as $key){
    $nroElemento = $nroElemento + 1;    
}

$filas = $nroElemento / 4;
if($nroElemento % 4!=0){
    $filas+=1;
}    

$htmlP = '<table style="width:100%;">';

$swChecked = 0;
$columna=0;
//$checked="checked";
$checked="";

if($nroElemento==1){
    $checked="checked";
}else{
    $checked="";
}    



foreach ($rowPresentacion as $key) {
    //if($swChecked==0){
    //    $swChecked=1;
    //}else{
    //    $checked="";
    //}

    if($columna==0){
        $htmlP.= '<tr>';            
    }
    $columna+=1;
    $htmlP.= '<td style="width:15%;">';
        $htmlP.= '<div style="padding-left:40px"><input class="form-check-input" type="radio" ';
        $htmlP.= '    id="presentacion" name="presentacion"' . $checked . ' value="' . $key->id . '"  onclick="valExistencia('.$key->id.')">&nbsp;&nbsp;<span style="font-size:14px;font-weight:700">'.$key->tipo_presentacion.'&nbsp;('.$key->nro_pieza.'&nbsp'.$key->unidad_medida.')</span></div>';
        $htmlP.= '<span style="padding-left:50px">'.$key->precio.'$</span>'; 

        $htmlP.= '</td>';
        if($nroElemento==1){
            $htmlP.= '<input type="hidden" id="valPresentacion" name="valPresentacion" value="' . $key->id . '">'; 
            $htmlP.= '</td>';
        }else{
            $htmlP.= '<input type="hidden" id="valPresentacion" name="valPresentacion" value="0">'; 
            $htmlP.= '</td>';
        }
    $htmlP.= '<td style="width:10%;">';
    $htmlP.= '</td>';

    if($columna==4){
        $columna=0;
        $htmlP.= '</tr>';          
    }
}

if($columna != 4){
   $nroColumna = 4 - $columna;
   while($nroColumna<=4){
       $nroColumna+=1;
       $htmlP.= '<td style="width:15%;">';
       $htmlP.= '</td>';
       $htmlP.= '<td style="width:10%;">';
       $htmlP.= '</td>';
   }
   $htmlP.= '</tr>';          
}

$htmlP.= '</table>';
return $htmlP;

}


function envoltura(){


$this->load->model('Ad_envoltura_mdl');
$rowEnvoltura = $this->Ad_envoltura_mdl->obt_envoltorio();
$nroElemento = 0;
foreach ($rowEnvoltura as $key){
    $nroElemento = $nroElemento + 1;    
}

$filas = $nroElemento / 3;
if($nroElemento % 3!=0){
    $filas+=1;
}    

$html = '<table style="width:100%;">';
$swChecked = 0;
$columna=0;
if($nroElemento==1){
    $checked="checked";
}else{
    $checked="";
}  


foreach ($rowEnvoltura as $key) {

    if($columna==0){
        $html.= '<tr>';            
    }
    $columna+=1;
    $html.= '<td style="width:33%;">';
        $html.= '<div style="padding-left:40px"><input class="form-check-input" type="radio" ';
        $html.= '    id="envoltura" name="envoltura"' . $checked . ' value="' . $key->id . '" >&nbsp;&nbsp;<span style="font-size:14px;font-weight:700">'.$key->nombre.'</span></div>';
        $html.= '<span style="padding-left:50px">'.number_format ($key->costo, 2,",",".").'$</span>';

        $html.= '</td>';

        if($nroElemento==1){
            $html.= '<input type="hidden" id="valEnvoltura" name="valEnvoltura" value="' . $key->id . '">'; 
            $html.= '</td>';
        }else{
            $html.= '<input type="hidden" id="valEnvoltura" name="valEnvoltura" value="0">'; 
            $html.= '</td>';
        }

    if($columna==3){
        $columna=0;
        $html.= '</tr>';          
    }
}

if($columna != 3){
   $nroColumna = 3 - $columna;
   while($nroColumna<=3){
       $nroColumna+=1;
       $html.= '<td style="width:33%;">';
       $html.= '</td>';
   }
   $html.= '</tr>';          
}

$html.= '</table>';
return $html;

}




function Ingrediente($idAlimento){
$this->load->model('Mn_alimento_mdl');

$rowIngrediente = $this->Mn_alimento_mdl->obtMIngredienteAlimento($idAlimento);
$arregloIngrediente[0]=0;
$i=0;
foreach ($rowIngrediente as $key) {
    $i++;
    $arregloIngrediente[$i] = $key->id_ingrediente;
}
$nroElemento = $i;
$dataSession = array(
    'arregloIngrediente'=>$arregloIngrediente,
    'nroIngrediente'=>$nroElemento
);
$this->session->set_userdata($dataSession);

$filas = $nroElemento / 5;
if($nroElemento % 5 != 0){
    $filas+=1;
}  

$html = '<table style="width:100%;">';
$columna=0;
$i=0;
foreach ($rowIngrediente as $key) {

   if($columna==0){
        $html.= '<tr>';            
    }
    $columna+=1;
    $i+=1;

   $html.= '<td style="width:20%;">';
        $html.='<div class="custom-control custom-checkbox">';          
        $html.='<input type="checkbox" class="custom-control-input" id="ingrediente-'. $i .'"';
        $html.='name="ingrediente-' . $i . '" readonly="false" checked>';
        $html.='<label class="custom-control-label" for="ingrediente-'.$i.'" style="font-weight:700">'.$key->ingrediente.'</label>';
        $html.='</div>';
    $html.= '</td>';

    if($columna==5){
        $columna=0;
        $html.= '</tr>';          
    }
}
if($columna != 5){
   $nroColumna = 5 - $columna;
   while($nroColumna<=5){
       $nroColumna+=1;
       $html.= '<td style="width:20%;">';
       $html.= '</td>';
   }
   $html.= '</tr>';          
}
$html.= '</table>';
return $html;
}


function Ingrediente_adic(){
$this->load->model('Mn_alimento_mdl');
$rowIngrediente = $this->Mn_ingredienteAdicional_mdl->obtener_ingrediente();

$arregloIngredienteAdic[0]=0;
$arregloAdicPrecio[0][0]=0.00; //cantidad
$arregloAdicPrecio[0][1]=0.00; //precio
$arregloAdicPrecio[0][2]=0;

$i=0;
foreach ($rowIngrediente as $key) {
    $i++;
    $arregloIngredienteAdic[$i] = $key->id_ingrediente;
    $arregloAdicPrecio[$i][0] = $key->cantidad_ingrediente;
    $arregloAdicPrecio[$i][1] = $key->precio;
    $arregloAdicPrecio[$i][2] = $key->id_unidad_medida;
}
$nroElemento = $i;

$dataSession = array(
    'arregloIngredienteAdic'=>$arregloIngredienteAdic,
    'arregloAdicPrecio'=>$arregloAdicPrecio,
    'nroIngredienteAdic'=>$nroElemento
);
$this->session->set_userdata($dataSession);

$html ="";
if($nroElemento>0){

$html = '<table style="width:100%;">';

$i=0;
$columna=0;
foreach ($rowIngrediente as $key) {
   if($columna==0){
        $html.= '<tr>';            
    }
    $columna+=1;
    $i+=1;

    $html.= '<td style="width:25%; padding-left:5px">';        
        $html.='<div class="custom-control custom-checkbox">';          
        $html.='<input type="checkbox" class="custom-control-input" id="ingredienteAdic-'. $i .'"';
        $html.='name="ingredienteAdic-' . $i . '" readonly="false">';
        $html.='<label class="custom-control-label" for="ingredienteAdic-'.$i.'" style="font-weight:700">'.$key->ingrediente.'</label>';
        $html.='</div><br>';
        $html.= '<span style="padding-left:30px;font-weight:500">'.number_format ($key->precio, 2,",",".").'$</span>';
    $html.= '</td>';

    if($columna==4){
        $columna=0;
        $html.= '</tr>';          
    }
}       


$html.= '</table>';
} //fin val nro elemento

if($columna != 4){
   $nroColumna = 4 - $columna;
   while($nroColumna<=4){
       $nroColumna+=1;
       $html.= '<td style="width:25%;">';
       $html.= '</td>';
   }
   $html.= '</tr>';          
}


return $html;  

}







function presentacion_otro($idAlimento){
$this->load->model('Mn_presentacionIngrediente_mdl');
$rowPresentacion = $this->Mn_presentacionIngrediente_mdl->mostrarPresentacion($idAlimento);

$nroElemento = 0;
foreach ($rowPresentacion as $key){
    $nroElemento = $nroElemento + 1;    
}

$filas = $nroElemento / 3;
if($nroElemento % 3!=0){
    $filas+=1;
}    


$htmlP = '<table style="width:100%;">';
$swChecked = 0;
$columna=0;
$checked="";
if($nroElemento==1){
    $checked="checked";
}else{
    $checked="";
}    

foreach ($rowPresentacion as $key) {
    if($columna==0){
        $htmlP.= '<tr>';            
    }
    $columna+=1;

    $htmlP.= '<td style="width:33%;">';
        $htmlP.= '<div style="padding-left:40px"><input class="form-check-input" type="radio" ';
        $htmlP.= '    id="presentacion_otro" name="presentacion_otro"' . $checked . ' value="' . $key->id . '"  >&nbsp;&nbsp;<span style="font-size:14px;font-weight:700">'.$key->tipo_presentacion.'&nbsp;('.$key->nro_pieza.'&nbsp'.$key->unidad_medida.')</span></div>';
        $htmlP.= '<span style="padding-left:50px">'.$key->precio.'$</span>'; 

        $htmlP.= '</td>';
        if($nroElemento==1){
            $htmlP.= '<input type="hidden" id="valPresentacion_otro" name="valPresentacion_otro" value="' . $key->id . '">'; 
            $htmlP.= '</td>';
        }else{
            $htmlP.= '<input type="hidden" id="valPresentacion_otro" name="valPresentacion_otro" value="0">'; 
            $htmlP.= '</td>';
        }

    if($columna==3){
        $columna=3;
        $htmlP.= '</tr>';          
    }
}

if($columna != 3){
   $nroColumna = 3 - $columna;
   while($nroColumna<=3){
       $nroColumna+=1;
       $htmlP.= '<td style="width:33%;">';
       $htmlP.= '</td>';
   }
   $htmlP.= '</tr>';          
}
$htmlP.= '</table>';
return $htmlP;

}




function ajax_cargarAlimento($idAlimento){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){
    $dataSession = array(
        'idAlimento'=>$idAlimento
    );
    $this->session->set_userdata($dataSession);

    //$swValidar = 0;
    //$nroElemento = 0;

    //$this->load->model('Mn_presentacionIngrediente_mdl');
    //$rowPresentacion = $this->Mn_presentacionIngrediente_mdl->mostrarPresentacion($idAlimento);

    ///presentacion
    $htmlP = $this->presentacion_otro($idAlimento);




    $data = array(
        'status'=>0,
        'presentacion'=>$htmlP,
    );
    echo json_encode($data);
}//fin de val session



}


function ajax_guardarDetalle(){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){
    //presentacion

    $valPresentacion = $this->input->post('valPresentacion');        
    if($valPresentacion==0){
        $idPresentacion = $this->input->post('presentacion');
    }else{
        $idPresentacion = $this->input->post('valPresentacion');
    }



    $valEnvoltura = $this->input->post('valEnvoltura');        
    if($valEnvoltura==0){
        $idEnvoltura = $this->input->post('envoltura');
    }else{
        $idEnvoltura = $this->input->post('valEnvoltura');
    }

    $cantidadSushi = $this->input->post('s_cantidad');


    //validar existencia de la presentacion
    $idAlimento = $this->session->userdata('idAlimento');
    /*$this->load->model('Co_contenedor_mdl');
    $rowContenedor = $this->Co_contenedor_mdl->obt_alimentoContenedor($idAlimento);
    $cantidad = $rowContenedor->cantidad;

    $rowPresentacion = $this->Mn_presentacionIngrediente_mdl->obtPresentacion($idPresentacion);   
    $pieza = $rowPresentacion->nro_pieza * $cantidadSushi; 

    $swValidar = 0;
    if($cantidad < $pieza){
        $swValidar = 1;
    }*/
    $swValidar = 0;

    if($swValidar==0){
        $idCategoria = 1;    
        if($idCategoria==1){
            //almacenar ingrediente adicionales

            $arregloIngredienteAdic = $this->session->userdata('arregloIngredienteAdic');
            $nroElementoAdic = $this->session->userdata('nroIngredienteAdic');
            $arregloAdicPrecio = $this->session->userdata('arregloAdicPrecio');

            $i = 0;
            $precioAdicional = 0.00;
            $descripcionAdic="";
            while($i < $nroElementoAdic){
                $i+=1;
                $check  = "ingredienteAdic-" . $i;
                if($this->input->post("$check")){
                    $rowIngrediente = $this->Mn_ingrediente_mdl->obtModificar($arregloIngredienteAdic[$i]); 
                    $precio = $arregloAdicPrecio[$i][1]; 
                    if($descripcionAdic==""){
                        $descripcionAdic = trim($rowIngrediente->nombre) . "(".$precio.")";
                    }else{
                        $descripcionAdic = $descripcionAdic . ", " . trim($rowIngrediente->nombre). "(".$precio.")";

                    }
                    $precioAdicional = $precioAdicional + $precio;
                }
            }

            //procesar envoltura
            //$idEnvoltura = $this->input->post('id_cbo_envoltorio');    
            $this->load->model('Ad_envoltura_mdl');
            $rowEnvoltura = $this->Ad_envoltura_mdl->obtModificar($idEnvoltura);
            $precioEnvoltura = $rowEnvoltura->costo;
            $cantidad_ingredienteEnv = $rowEnvoltura->cantidad_ingrediente;
            $cantidadEnvoltura = $cantidadSushi;
            $idIngredienteEnv = $rowEnvoltura->id_ingrediente; 


        }else{
            $idEnvoltura = 0;
            $precioEnvoltura=0.00;
            $precioAdicional=0.00;
            $cantidadEnvoltura = 0;
            $descripcionAdic="";
        } 


        //$idAlimento = $this->session->userdata('idAlimento');
        $idAlimento = $this->session->userdata('idAlimento');
        //


        //obtener datos de presentacion
        $rowPresentacion = $this->Mn_alimento_mdl->obtModificarPresentacion($idPresentacion);

        $precioUnico =  $rowPresentacion->precio + $precioEnvoltura + $precioAdicional;  
        $precioTotal = ($rowPresentacion->precio + $precioEnvoltura + $precioAdicional) * $cantidadSushi;

        $data = array(
            'id_despacho'=>$this->session->userdata('idDespacho'),
            'id_alimento'=>$idAlimento,
            'id_categoria_menu'=>$idCategoria,
            'id_presentacion'=>$idPresentacion,
            'id_envoltura'=>$idEnvoltura,
            'precio_sushi'=>$rowPresentacion->precio,
            'precio_unitario'=>$precioUnico,
            'nro_pieza'=>$rowPresentacion->nro_pieza,
            'cantidad'=>$cantidadSushi,
            'cantidad_envoltura'=>$cantidadEnvoltura,
            'precio_envoltura'=>$precioEnvoltura,
            'descripcion_adicional'=>$descripcionAdic,
            'precio_adicional'=>$precioAdicional,            
            'precio'=>$precioTotal,
            'id_unidad_medida'=>$rowPresentacion->id_unidad_medida,
            'id_create'=>$this->session->userdata('idUsuario'),
        );

        $idDetalle = $this->Dp_despachoDetalle_mdl->incluir_despachoDetalle($data);

        //actualizar monto total del despacho
        $this->Dp_despachoDetalle_mdl->totalDespacho($this->session->userdata('idDespacho'));

        //Guardar ingredientes.
        $idCategoria = 1;    
        if($idCategoria==1){
            //verificar ingrediente
            //$idEnvoltura = $this->input->post('envoltura');    
            $arregloIngrediente = $this->session->userdata('arregloIngrediente');
            $nroElemento = $this->session->userdata('nroIngrediente');
            $i = 0;
            ////////////////////////////////////////////////////////////////////////////    
            $sinIngrediente = "";
            while($i < $nroElemento){
                $i+=1;
                $check  = "ingrediente-" . $i;
                if($this->input->post("$check")){
                    $idIngrediente = $arregloIngrediente[$i];
                    $rowingrediente = $this->Mn_presentacionIngrediente_mdl->obt_cantidadIngrediente($this->input->post('presentacion'), $idIngrediente);

                    $cantidad = $rowingrediente->cantidad;
                    $nroAlimento = $cantidadSushi;
                    $cantidadTotal = $cantidad * $cantidadSushi;

                    $data=array(
                        'id_despacho_detalle'=>$idDetalle,
                        'id_ingrediente'=>$idIngrediente,
                        'id_presentacion'=>$this->input->post('presentacion'),
                        'cantidad_individual'=>$cantidad,
                        'nro_alimento'=>$cantidadSushi,
                        'cantidad_total'=>$cantidadTotal,
                        'id_create'=>$this->session->userdata('idUsuario')
                    );

                    $this->Dp_despachoDetalle_mdl->incluir_despachoDetalle_ingrediente($data);
                }else{
                    $idIngrediente = $arregloIngrediente[$i];
                    $rowIngre = $this->Mn_ingrediente_mdl->obtModificar($idIngrediente);

                    if($sinIngrediente==""){
                        $sinIngrediente = trim($rowIngre->nombre); 
                    }else{
                        $sinIngrediente = trim($sinIngrediente) .", " . trim($rowIngre->nombre); 
                    }
                }
            }

            //modificar detalle agregar sin ingrediente
            if($sinIngrediente!=""){
                $data = array(
                    'sin_ingrediente'=>$sinIngrediente
                );
                $this->Dp_despachoDetalle_mdl->guardar_mod_detallePedido($idDetalle,$data);
            }


            ///Guardar ingrediente de la envoltura
                    $cantidad_ingredienteEnvTot = $cantidad_ingredienteEnv * $cantidadSushi;
                    $data=array(
                        'id_despacho_detalle'=>$idDetalle,
                        'id_ingrediente'=>$idIngredienteEnv,
                        'id_presentacion'=>$this->input->post('presentacion'),
                        'cantidad_individual'=>$cantidad_ingredienteEnv,
                        'nro_alimento'=>$cantidadSushi,
                        'cantidad_total'=>$cantidad_ingredienteEnvTot,
                        'id_create'=>$this->session->userdata('idUsuario')
                    );

                    $this->Dp_despachoDetalle_mdl->incluir_despachoDetalle_ingrediente($data);





            //////////////////////////////////////////////// 
            //-------------ingrediente adicionales /////////
            ////////////////////////////////////////////////

            $arregloIngredienteAdic = $this->session->userdata('arregloIngredienteAdic');
            $nroElementoAdic = $this->session->userdata('nroIngredienteAdic');
            $arregloAdicPrecio = $this->session->userdata('arregloAdicPrecio');

            $i = 0;
            while($i < $nroElementoAdic){
                $i+=1;
                $check  = "ingredienteAdic-" . $i;
                if($this->input->post("$check")){
                    $idIngrediente = $arregloIngredienteAdic[$i];
                    $cantidad = $arregloAdicPrecio[$i][0];
                    $cantidadIngTot = $arregloAdicPrecio[$i][0] * $cantidadSushi;
                    $precio = $arregloAdicPrecio[$i][1];
                    $precioTotal = $precio * $cantidadSushi;
                    $idUnidadMedida = $arregloAdicPrecio[$i][2];

                    $data=array(
                        'id_despacho_detalle'=>$idDetalle,
                        'id_ingrediente'=>$idIngrediente,
                        'cantidad'=>$cantidad,
                        'nro_vendido'=>$cantidadSushi,
                        'cantidad_total'=>$cantidadIngTot,
                        'precio'=>$precio,
                        'precio_total'=>$precioTotal,
                        'id_create'=>$this->session->userdata('idUsuario')
                    );
                    $this->Mn_ingredienteAdicional_mdl->incluir_ingredienteAdicional($data);

                   
                    //Agregar en tabla despacho_detalle_ingrediente  
                    $cantidad_ingredienteEnvTot = $cantidad_ingredienteEnv * $cantidadSushi;
                    $data=array(
                        'id_despacho_detalle'=>$idDetalle,
                        'id_ingrediente'=>$idIngrediente,
                        'id_presentacion'=>$this->input->post('presentacion'),
                        'cantidad_individual'=>$arregloAdicPrecio[$i][0],
                        'nro_alimento'=>$cantidadSushi,
                        'cantidad_total'=>$cantidadIngTot,
                        'id_create'=>$this->session->userdata('idUsuario')
                    );

                    $this->Dp_despachoDetalle_mdl->incluir_despachoDetalle_ingrediente($data);





                }
            }
            ///////////fin ingrediente adicionales ////////////

            //descontar del contenedor
            //obtener disponibilidad del contenedor
            $idAlimento = $this->session->userdata('idAlimento');
            $this->load->model('Co_contenedor_mdl');
            $rowContenedor = $this->Co_contenedor_mdl->obt_alimentoContenedor($idAlimento);
            $cantidad = $rowContenedor->cantidad;

            //obtener cantidad pieza del producto

            $rowPresentacion = $this->Mn_presentacionIngrediente_mdl->obtPresentacion($idPresentacion);   
            $pieza = $rowPresentacion->nro_pieza * $cantidadSushi; 

            //calcular existencia y disminuir del contenedor.
            $resultado=$cantidad - $pieza;
            //if($resultado<0){
            //     $resultado =0;  
            //}
            $data=array(
                'cantidad'=>$resultado
            );
            $this->Co_contenedor_mdl->act_contenedor($this->session->userdata('idAlimento'),$data);
        }


        //LLenar factura.

        $arreglo = $this->generarFactura($this->session->userdata('idDespacho')); 

        $htmlF = $arreglo[0];
        $precio = $arreglo[1];

        $rowConfiguracion = $this->Cf_configuracion_mdl->obtDatosSucursal();
        $impuesto = $rowConfiguracion->impuesto;
        $totalImpuesto = $precio * ($impuesto/100);
        $total = $precio + $totalImpuesto;    
      
        $impuesto = number_format ($impuesto, 2,',','.');
        $precio = number_format ($precio, 2,',','.');
        $totalImpuesto = number_format ($totalImpuesto, 2,',','.');
        $total = number_format ($total, 2,',','.');

        $nroBoton = $this->Dp_despacho_mdl->val_nroDetalle($this->session->userdata('idDespacho'));

        $data = array(
            'boton'=>$nroBoton,          
            'factura'=>$htmlF,
            'precio'=>$precio,
            'impuesto'=>$impuesto,
            'totalImpuesto'=>$totalImpuesto,
            'total'=>$total,

            'status'=>0
        );
        echo json_encode($data);        








    }else{
        $data = array(
            'status'=>1
        );
        echo json_encode($data);
    }    









}//fin val session
} //fin ajax_guardarDetalle





//Generar detalle de otro alimento diferente a sushi
//////////////////////////
function ajax_guardarDetalle_otro(){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){
    //presentacion

    $valPresentacion = $this->input->post('valPresentacion_otro');        
    if($valPresentacion==0){
        $idPresentacion = $this->input->post('presentacion_otro');
    }else{
        $idPresentacion = $this->input->post('valPresentacion_otro');
    }

    $swValidar=0; 
    if($swValidar==0){

        $cantidadProd = $this->input->post('m_cantidad');    

        //$idAlimento = $this->session->userdata('idAlimento');
        $idAlimento = $this->session->userdata('idAlimento');
        //


        //obtener datos de presentacion
        $rowPresentacion = $this->Mn_alimento_mdl->obtModificarPresentacion($idPresentacion);

        $precioTotal = $rowPresentacion->precio * $cantidadProd;

        $data = array(
            'id_despacho'=>$this->session->userdata('idDespacho'),
            'id_alimento'=>$idAlimento,
            'id_categoria_menu'=>$this->session->userdata('idCategoriaMenu'),
            'id_presentacion'=>$idPresentacion,
            'precio_unitario'=>$rowPresentacion->precio,
            'nro_pieza'=>$rowPresentacion->nro_pieza,
            'cantidad'=>$cantidadProd,
            'precio'=>$precioTotal,
            'id_unidad_medida'=>$rowPresentacion->id_unidad_medida,
            'id_create'=>$this->session->userdata('idUsuario'),
        );

        $idDetalle = $this->Dp_despachoDetalle_mdl->incluir_despachoDetalle($data);

        //actualizar monto en despacho
        $this->Dp_despachoDetalle_mdl->totalDespacho($this->session->userdata('idDespacho'));

        //Guardar detalle de ingrediente.
        $rowIngrediente = $this->Mn_presentacionIngrediente_mdl->obt_presentacionIngrediente($idPresentacion);

        foreach ($rowIngrediente as $key) {
            $cantidad = $key->cantidad;
            $cantidadTotal = $cantidad * $cantidadProd;
            $data=array(
               'id_despacho_detalle'=>$idDetalle,
               'id_ingrediente'=>$key->id_ingrediente,
               'id_presentacion'=>$idPresentacion,
               'cantidad_individual'=>$cantidad,
               'nro_alimento'=>$cantidadProd,
               'cantidad_total'=>$cantidadTotal,
               'id_create'=>$this->session->userdata('idUsuario')
            );

            $this->Dp_despachoDetalle_mdl->incluir_despachoDetalle_ingrediente($data);
        }
    
        //LLenar factura.

        $arreglo = $this->generarFactura($this->session->userdata('idDespacho')); 

        $htmlF = $arreglo[0];
        $precio = $arreglo[1];

        $rowConfiguracion = $this->Cf_configuracion_mdl->obtDatosSucursal();
        $impuesto = $rowConfiguracion->impuesto;
        $totalImpuesto = $precio * ($impuesto/100);
        $total = $precio + $totalImpuesto;    
      
        $impuesto = number_format ($impuesto, 2,',','.');
        $precio = number_format ($precio, 2,',','.');
        $totalImpuesto = number_format ($totalImpuesto, 2,',','.');
        $total = number_format ($total, 2,',','.');


        $nroBoton = $this->Dp_despacho_mdl->val_nroDetalle($this->session->userdata('idDespacho'));    


        $data = array(
            'boton'=>$nroBoton,
            'factura'=>$htmlF,
            'precio'=>$precio,
            'impuesto'=>$impuesto,
            'totalImpuesto'=>$totalImpuesto,
            'total'=>$total,

            'status'=>0
        );
        echo json_encode($data);

    }else{
        $data = array(
            'status'=>1
        );
        echo json_encode($data);
    }


}//fin val session
} //fin ajax_guardarDetalle




//////// fin detalle alimento diferente a sushi




function generarFactura($idDespacho){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){

    $eliminar = base_url() . "assets/images/eliminar02.jpeg";
    $modificar = base_url() . "assets/images/modificar01.jpeg";
    $mas = base_url() . "assets/images/signo_matematica/mas_mod.png";
    $menos = base_url() . "assets/images/signo_matematica/menos_mod.png";

    $rowFactura = $this->Dp_despacho_mdl->obt_detalleFactura($idDespacho);
    $swColumna = 0;

    $htmlF = '<table style="width:100%; font-size:12px">';
    $htmlF.= '<tr>';
    $htmlF.= '<th style="width:45%">DESCRIPCIÓN</th>';
    $htmlF.= '<th style="width:10%; text-align:center">CANT.</th>';
    $htmlF.= '<th style="width:10%;text-align:center">P/U.</th>';
    $htmlF.= '<th style="width:15%;text-align:right">IMPORTE.</th>';
    $htmlF.= '<th style="width:20%;text-align:center">ACCIÓN</th>';
    $htmlF.= '</tr>';

    $precioT = 0.00;
    foreach ($rowFactura as $key) {
        $precioT=$precioT + $key->precio;


        $precioUnt = number_format ($key->precio_unitario, 2,',','.');
        $precio = number_format ($key->precio, 2,',','.');

        $nombre = trim($key->alimento) . " / " . trim($key->presentacion);
        if($key->id_categoria_menu==1){
            $nombre = trim($key->alimento)." / " . $key->nro_pieza . " PIEZAS ( PRECIO: ".$key->precio_sushi." )";
            $nombre.= " / ENVOLTURA: " . trim($key->envoltura);
            $nombre.= " ( PRECIO: ".$key->precio_envoltura." )";
            if($key->precio_adicional > 0.00){
                $nombre.= " / INGR. ADICIONAL: " . trim($key->descripcion_adicional);
            }
            if($key->sin_ingrediente != ""){
                $nombre.= " / SIN INGR. : " . trim($key->sin_ingrediente);
            }



        }
        $htmlF.= '<tr>';
        $htmlF.= '<td>'.$nombre.'</td>';
        $htmlF.= '<td style="text-align:center">'.$key->cantidad.'</td>';
        $htmlF.= '<td style="text-align:right">'.$precioUnt.'</td>';
        $htmlF.= '<td style="text-align:right">'.$precio.'</td>';

        
        $htmlF.= '<td style="text-align:center">';
         
        //if($key->id_categoria_menu>2){ 

            $htmlF.= '<a href="javascript:void(0)" onclick="javascript:mas_itemFactura('.$key->id.', ' .$key->id_categoria_menu .' )">';
            $htmlF.= '<img src="'.$mas.'" style="width:20px; height:20px" title="Incrementar Item" alt="Incrementar Item"></a>';
            $htmlF.='<a href="javascript:void(0)" onclick="javascript:menos_itemFactura('.$key->id.', '.$key->id_categoria_menu.')">';
            $htmlF.= '<img src="'.$menos.'" style="width:20px; height:20px" title="Disminuir Item" alt="Disminuir Item"></a>';
        //}    

        $htmlF.= '<a href="javascript:void(0)" onclick="javascript:eliminar_itemFactura('.$key->id.')">';
        $htmlF.= '<img src="'.$eliminar.'" style="width:20px; height:20px" title="Eliminar Item" alt="eliminar Item"></a>';
        $htmlF.='</td>';
        $htmlF.= '</tr>';
        $swColumna = 1;
    }

    if($swColumna==0){
        $htmlF.= '<tr>';
        $htmlF.= '<td></td>';
        $htmlF.= '<td></td>';
        $htmlF.= '<td></td>';
        $htmlF.= '<td></td>';
        $htmlF.= '<td></td>';
        $htmlF.= '</tr>';
    }
    $htmlF.= '</table>';

    $arreglo[0]=$htmlF;
    $arreglo[1]=$precioT;
    return $arreglo; 
} //fin val session
}


public function ajax_eliminar_itemFactura($id){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){

    //borrar dp_despacho_detalle_ingrediente
    $this->Dp_despachoDetalle_mdl->eliminar_detalleIngrediente($id);

    //borrar dp_despacho_detalle_ingrediente_adicional
    $this->Mn_ingredienteAdicional_mdl->eliminar_IngredienteAdicional($id);

    //actualizar agregar contenedor
    $rowDetalle = $this->Dp_despachoDetalle_mdl->obtDespachoDetalle($id);
    $idCategoria = $rowDetalle->id_categoria_menu; 


    
    if($idCategoria == 1){
        $nro_pieza = $rowDetalle->nro_pieza;
        $this->load->model('Co_contenedor_mdl');
        $rowContenedor = $this->Co_contenedor_mdl->obt_alimento_contenedor_reversar($rowDetalle->id_alimento);
        $cantidad = $rowContenedor->cantidad + $nro_pieza;
        $data = array(
            'cantidad'=>$cantidad
        );
        $this->Co_contenedor_mdl->guardar_mod($rowContenedor->id, $data);
    }

    //borrar detalle pedido
    $this->Dp_despachoDetalle_mdl->eliminar_detallePedido($id);

    //actualizar monto en despacho
    $this->Dp_despachoDetalle_mdl->totalDespacho($this->session->userdata('idDespacho'));

    //Generar Factura.
    $arreglo = $this->generarFactura($this->session->userdata('idDespacho'));
    $htmlF = $arreglo[0];
    $precio = $arreglo[1];

    $rowConfiguracion = $this->Cf_configuracion_mdl->obtDatosSucursal();
    $impuesto = $rowConfiguracion->impuesto;
    $totalImpuesto = $precio * ($impuesto/100);
    $total = $precio + $totalImpuesto;    
  
    $impuesto = number_format ($impuesto, 2,',','.');
    $precio = number_format ($precio, 2,',','.');
    $totalImpuesto = number_format ($totalImpuesto, 2,',','.');
    $total = number_format ($total, 2,',','.');



    $nroBoton = $this->Dp_despacho_mdl->val_nroDetalle($this->session->userdata('idDespacho'));




    $data = array(
    'boton'=>$nroBoton,
    'precio'=>$precio,
    'impuesto'=>$impuesto,
    'totalImpuesto'=>$totalImpuesto,
    'total'=>$total,

    'factura'=>$htmlF,
    );

    echo json_encode($data);

} //fin val session    
}


public function ajax_mas_itemProducto(){

//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){

    $id = $_GET['id'];
    $idCategoria_menu = $_GET['idCategoria'];

    $row = $this->Dp_despachoDetalle_mdl->obtDespachoDetalle($id);
    $cantidad = $row->cantidad;
    $cantidad = $cantidad + 1;
    $precio_unitario = $row->precio_unitario;
    $precio = $precio_unitario * $cantidad;

    $data = array(
        'cantidad'=>$cantidad,
        'precio'=>$precio,
        'cantidad_envoltura'=>$cantidad
    );
    $this->Dp_despachoDetalle_mdl->guardar_mod_detallePedido($id,$data);

    //actualizar monto en despacho
    $this->Dp_despachoDetalle_mdl->totalDespacho($this->session->userdata('idDespacho'));


    //modificar cantidad de ingrediente
    $rowIngrediente = $this->Dp_despachoDetalle_mdl->obtDespachoDetalleIngrediente($id);
    foreach ($rowIngrediente as $key) {
        $cantidad_individual = $key->cantidad_individual;
        $cantidadTotal = $cantidad_individual * $cantidad;
        $data = array(
            'nro_alimento'=>$cantidad, 
            'cantidad_total'=>$cantidadTotal,
        );    
        $this->Dp_despachoDetalle_mdl->mod_DespachoDetalleIngrediente($key->id,$data);
    }

    if($idCategoria_menu==1){
        //actualizar ingrediente adicionales
        $rowIngredienteAdic = $this->Dp_despachoDetalle_mdl->obtDespachoDetalleIngredienteAdicional($id);
        foreach ($rowIngredienteAdic as $key) {
            $cantidad_individual = $key->cantidad;
            $cantidadTotal = $cantidad_individual * $cantidad;
            $precio = $key->precio;
            $precioTotal = $precio * $cantidad;
            $data = array(
                'nro_vendido'=>$cantidad,
                'cantidad_total'=>$cantidadTotal,
                'precio_total'=>$precioTotal,
            );    
            $this->Dp_despachoDetalle_mdl->mod_DespachoDetalleIngredienteAdicional($key->id,$data);
        }


 



        //actualizar contenedor
        $nro_pieza = $row->nro_pieza;
        $this->load->model('Co_contenedor_mdl');
        $rowContenedor = $this->Co_contenedor_mdl->obt_alimento_contenedor_reversar($row->id_alimento);
        $cantidad = $rowContenedor->cantidad - $nro_pieza;
        $data = array(
            'cantidad'=>$cantidad
        );
        $this->Co_contenedor_mdl->guardar_mod($rowContenedor->id, $data);
    }



    //Generar Factura.
    $arreglo = $this->generarFactura($this->session->userdata('idDespacho'));
    $htmlF = $arreglo[0];
    $precio = $arreglo[1];

    $rowConfiguracion = $this->Cf_configuracion_mdl->obtDatosSucursal();
    $impuesto = $rowConfiguracion->impuesto;
    $totalImpuesto = $precio * ($impuesto/100);
    $total = $precio + $totalImpuesto;    
  
    $impuesto = number_format ($impuesto, 2,',','.');
    $precio = number_format ($precio, 2,',','.');
    $totalImpuesto = number_format ($totalImpuesto, 2,',','.');
    $total = number_format ($total, 2,',','.');

    $nroBoton = $this->Dp_despacho_mdl->val_nroDetalle($this->session->userdata('idDespacho'));

    $data = array(
    'boton'=>$nroBoton,    
    'precio'=>$precio,
    'impuesto'=>$impuesto,
    'totalImpuesto'=>$totalImpuesto,
    'total'=>$total,

    'factura'=>$htmlF,
    );

    echo json_encode($data);
}//fin val session

}


public function ajax_menos_itemProducto(){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){
    $id = $_GET['id'];
    $idCategoria_menu = $_GET['idCategoria'];

    $row = $this->Dp_despachoDetalle_mdl->obtDespachoDetalle($id);
    $cantidad = $row->cantidad;

    if($cantidad > 1){
        $cantidad = $cantidad - 1;
        $precio_unitario = $row->precio_unitario;
        $precio = $precio_unitario * $cantidad;

        $data = array(
            'cantidad'=>$cantidad,
            'precio'=>$precio,
            'cantidad_envoltura'=>$cantidad
        );
        $this->Dp_despachoDetalle_mdl->guardar_mod_detallePedido($id,$data);

        //actualizar monto en despacho
        $this->Dp_despachoDetalle_mdl->totalDespacho($this->session->userdata('idDespacho'));
        

        //modificar cantidad de ingrediente
        $rowIngrediente = $this->Dp_despachoDetalle_mdl->obtDespachoDetalleIngrediente($id);
        foreach ($rowIngrediente as $key) {
            $cantidad_individual = $key->cantidad_individual;
            $cantidadTotal = $cantidad_individual * $cantidad;
            $data = array(
                'nro_alimento'=>$cantidad, 
                'cantidad_total'=>$cantidadTotal,
            );    
            $this->Dp_despachoDetalle_mdl->mod_DespachoDetalleIngrediente($key->id,$data);
        }

        if($idCategoria_menu==1){
            //actualizar ingrediente adicionales
            $rowIngredienteAdic = $this->Dp_despachoDetalle_mdl->obtDespachoDetalleIngredienteAdicional($id);
            foreach ($rowIngredienteAdic as $key) {
                $cantidad_individual = $key->cantidad;
                $cantidadTotal = $cantidad_individual * $cantidad;
                $precio = $key->precio;
                $precioTotal = $precio * $cantidad;
                $data = array(
                    'nro_vendido'=>$cantidad,
                    'cantidad_total'=>$cantidadTotal,
                    'precio_total'=>$precioTotal,
                );    
                $this->Dp_despachoDetalle_mdl->mod_DespachoDetalleIngredienteAdicional($key->id,$data);
            }

            //actualizar contenedor          
            $nro_pieza = $row->nro_pieza;
            $this->load->model('Co_contenedor_mdl');
            $rowContenedor = $this->Co_contenedor_mdl->obt_alimento_contenedor_reversar($row->id_alimento);
            $cantidad = $rowContenedor->cantidad + $nro_pieza;
            $data = array(
                'cantidad'=>$cantidad
            );
            $this->Co_contenedor_mdl->guardar_mod($rowContenedor->id, $data);
        }



        //Generar Factura.
        $arreglo = $this->generarFactura($this->session->userdata('idDespacho'));
        $htmlF = $arreglo[0];
        $precio = $arreglo[1];

        $rowConfiguracion = $this->Cf_configuracion_mdl->obtDatosSucursal();
        $impuesto = $rowConfiguracion->impuesto;
        $totalImpuesto = $precio * ($impuesto/100);
        $total = $precio + $totalImpuesto;    
      
        $impuesto = number_format ($impuesto, 2,',','.');
        $precio = number_format ($precio, 2,',','.');
        $totalImpuesto = number_format ($totalImpuesto, 2,',','.');
        $total = number_format ($total, 2,',','.');

        $nroBoton = $this->Dp_despacho_mdl->val_nroDetalle($this->session->userdata('idDespacho')); 
        $data = array(
            'boton'=>$nroBoton,
            'status'=>0,   
            'precio'=>$precio,
            'impuesto'=>$impuesto,
            'totalImpuesto'=>$totalImpuesto,
            'total'=>$total,

            'factura'=>$htmlF,
        );
        echo json_encode($data);
    }else{
        $data = array(
            'status'=>1,   
        );
        echo json_encode($data);
    }

}//fin val session

}




function ajax_disponibilidad_sushi($idPresentacion){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){
    $idAlimento = $this->session->userdata('idAlimento');
    $this->load->model('Co_contenedor_mdl');
    $rowContenedor = $this->Co_contenedor_mdl->obt_alimentoContenedor($idAlimento);



    
    $cantidad = $rowContenedor->cantidad;

    $rowPresentacion = $this->Mn_presentacionIngrediente_mdl->obtPresentacion($idPresentacion);   
    $pieza = $rowPresentacion->nro_pieza;  

    $swValidar = 0;
    if($cantidad < $pieza){
        $swValidar = 1;
    }

    $data = array(
        'status'=>$swValidar
    );
    echo json_encode($data);
} //Fin de validar session
}









// DEspacho RT**************************************************************
function ajax_generarDespachoRT(){
//validar variables de session.    
$valSession = $this->clssession->valSession();
if($valSession==true){
    //obtener codigo de venta

    $dataSession = array(
        'despacho_tipo'=> 2
    );
    $this->session->set_userdata($dataSession);



    $rowCodigoVenta = $this->Cf_correlativo_mdl->obtCorrelativo();
    $codigoVenta = $rowCodigoVenta->codigo_venta + 1;

    $rowConfiguracion = $this->Cf_configuracion_mdl->obtDatosSucursal();       
    $impuesto = $rowConfiguracion->impuesto;



    //Generar despacho
    $data = array(
        'codigo_venta'=>str_pad($codigoVenta, 7, "0", STR_PAD_LEFT),
        'id_sala'=>0,
        'id_mesa'=>0,
        'id_mesonero'=>0,
        'id_tipo_despacho'=>2,
        'impuesto'=>$impuesto,
        'id_empresa'=>$this->session->userdata('idEmpresa'),
        'id_sucursal'=>$this->session->userdata('idSucursal'),
        'id_estado_despacho'=>1,
        'nombre_cliente'=>$this->input->post('m_nombre'),
        'telefono_cliente'=>$this->input->post('m_telefono'),
        'direccion_cliente'=>$this->input->post('m_direccion'),
        'descripcion_cliente'=>$this->input->post('m_descripcion'),
        'id_create'=>$this->session->userdata('idUsuario'),
    );

    $idDespacho = $this->Dp_despacho_mdl->generarDespacho($data);

    //actualizar el correlativo venta
    $data=array(
        'codigo_venta' => $codigoVenta
    );
    $this->Cf_correlativo_mdl->actCorrelativo($data);


    //crear idDespacho como variable de session
    $dataSession = array(
        'idDespacho'=>$idDespacho
    );
    $this->session->set_userdata($dataSession);

    // obtener informacion de la sala, mesa y mesonero
    $rowInformacion = $this->Dp_despacho_mdl->infDespacho($idDespacho);
    $data = array(
        'codigoVenta'=>$rowInformacion->codigo_venta,
        'nombreCliente'=>$this->input->post('m_nombre'),
    );   

    echo json_encode($data); 

} // fin validar session
}




public function ajax_inicializarRT(){
    $dataSession = array(
        'despacho_tipo'=>2 
    );
    $this->session->set_userdata($dataSession);

    $data = array(
        'status'=>true
    );
    echo json_encode($data); 
}


/*public function ajax_inicializarDM(){
    $dataSession = array(
        'despacho_tipo'=>1 
    );
    $this->session->set_userdata($dataSession);

    $data = array(
        'status'=>true
    );
    echo json_encode($data); 
}*/


/*public function ajax_inicializarDelivery(){
    $dataSession = array(
        'despacho_tipo'=>3 
    );
    $this->session->set_userdata($dataSession);

    $data = array(
        'status'=>true
    );
    echo json_encode($data); 
}*/


// DEspacho deliveryRT**************************************************************
function ajax_generarDespachoDL(){
//validar variables de session.    
$valSession = $this->clssession->valSession();
if($valSession==true){
    //obtener codigo de venta

    $dataSession = array(
        'despacho_tipo'=> 3
    );
    $this->session->set_userdata($dataSession);



    $rowCodigoVenta = $this->Cf_correlativo_mdl->obtCorrelativo();
    $codigoVenta = $rowCodigoVenta->codigo_venta + 1;

    $rowConfiguracion = $this->Cf_configuracion_mdl->obtDatosSucursal();       
    $impuesto = $rowConfiguracion->impuesto;


    //Generar despacho
    $data = array(
        'codigo_venta'=>str_pad($codigoVenta, 7, "0", STR_PAD_LEFT),
        'id_sala'=>0,
        'id_mesa'=>0,
        'id_mesonero'=>0,
        'id_tipo_despacho'=>3,
        'impuesto'=>$impuesto,
        'id_empresa'=>$this->session->userdata('idEmpresa'),
        'id_sucursal'=>$this->session->userdata('idSucursal'),
        'id_estado_despacho'=>1,
        'nombre_cliente'=>$this->input->post('d_nombre'),
        'telefono_cliente'=>$this->input->post('d_telefono'),
        'direccion_cliente'=>$this->input->post('d_direccion'),
        'descripcion_cliente'=>$this->input->post('d_descripcion'),
        'id_create'=>$this->session->userdata('idUsuario'),
    );

    $idDespacho = $this->Dp_despacho_mdl->generarDespacho($data);

    //actualizar el correlativo venta
    $data=array(
        'codigo_venta' => $codigoVenta
    );
    $this->Cf_correlativo_mdl->actCorrelativo($data);



    //crear idDespacho como variable de session
    $dataSession = array(
        'idDespacho'=>$idDespacho
    );
    $this->session->set_userdata($dataSession);

    // obtener informacion de la sala, mesa y mesonero
    $rowInformacion = $this->Dp_despacho_mdl->infDespacho($idDespacho);
    $data = array(
        'codigoVenta'=>$rowInformacion->codigo_venta,
        'nombreCliente'=>$this->input->post('d_nombre'),
    );   

    echo json_encode($data); 

} // fin validar session
}



function ajax_reversoDespacho($idDespacho){
//validar variables de session.    
$valSession = $this->clssession->valSession();
if($valSession==true){
    $rowInformacion = $this->Dp_despacho_mdl->infDespacho($idDespacho);
    $data = array(
        'codigoVenta'=>$rowInformacion->codigo_venta,
        'tipo_despacho'=>$rowInformacion->tipo_despacho
    );
    echo json_encode($data); 
}    
}



function ajax_guardar_reversoDespacho(){
//validar variables de session.    
$valSession = $this->clssession->valSession();
if($valSession==true){
    $idDespacho = $this->input->post('id_reverso');
    $motivo = $this->input->post('r_motivo');

    $data = array(
        'reverso'=>1,
        'motivo_reverso'=>$motivo,
        'id_responsable_reverso'=>$this->session->userdata('idUsuario'),
        'activo'=>0,
        'id_estado_despacho'=>6,

        'id_update'=>$this->session->userdata('idUsuario'),
        'date_update'=>date('Y-m-d H:i:s')        
    );

    $this->Dp_despacho_mdl->reversarDespacho($idDespacho,$data);



    //Devolver piezas de sushi al contenedor almento al contenedor
    $rowDespachoDetalle = $this->Dp_despacho_mdl->obt_despachoDetalle($idDespacho);
    $this->load->model('Co_contenedor_mdl');

    foreach ($rowDespachoDetalle as $key) {
        $cantidad=0;
        if($key->id_categoria_menu==1){
            $nroPieza = $key->nro_pieza;
            $rowContenedor = $this->Co_contenedor_mdl->obt_alimento_contenedor_reversar($key->id_alimento);
            $cantidad = $rowContenedor->cantidad + $nroPieza;
            $data = array(
                'cantidad'=>$cantidad
            );
            $this->Co_contenedor_mdl->guardar_mod($rowContenedor->id, $data);
        } 
    }




        $tipoDespacho = $this->session->userdata("despacho_tipo");

        //obtener nro de pedido activos
        $tipo_1 = 0;
        $tipo_2 = 0;  
        $tipo_3 = 0;

        $rowTipoDespacho = $this->Dp_despacho_mdl->obt_nroDespacho();
        foreach($rowTipoDespacho as $key){
            switch($key->id_tipo_despacho){
            case 1:
               $tipo_1 = $key->cantidad; 
               break;
            case 2:
               $tipo_2 = $key->cantidad; 
               break;
            case 3:
               $tipo_3 = $key->cantidad; 
               break;
            }
        } 

        $row = $this->Dp_despacho_mdl->obt_dataTable($tipoDespacho);
        $html= $this->generarDatatable($row);
        $data = array(
            "registro"=>$html,
            "tipo_1"=>$tipo_1,
            "tipo_2"=>$tipo_2,
            "tipo_3"=>$tipo_3,
            "tipoDespacho"=>$tipoDespacho,
             "nombreEmpresa"=>$this->session->userdata('datoSucursal'),
            'status'=>0, 
        );
    echo json_encode($data); 
}    
}


//****************************************************************************

}
