<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Es_subModulo extends CI_Controller {

	
	public function __construct() {
         parent::__construct();
         $this->load->model(array('Es_subModulo_mdl'));
         $this->load->library(array('Clssession')); 
    }


	public function index(){
    //validar variables de session.
    $valSession = $this->clssession->valSession();
    if($valSession==true){
		$data=array(
			'accion'=>1
		);
		
	    $this->load->view('header');
		$this->load->view('menu');
		$this->load->view('estructura_sistema/subModulo/act_subModulo',$data);
		$this->load->view('footer/footer', $data);
		$this->load->view('estructura_sistema/subModulo/footer_subModulo', $data);
		$this->load->view('footer/lib_numerica');
    }//fin val session		
	}
	
	function ajax_datatable($id){
    //validar variables de session.
    $valSession = $this->clssession->valSession();
    if($valSession==true){
		$row = $this->Es_subModulo_mdl->obt_dataTable();
		$html= $this->generarDatatable($row);
		$data = array(
		    "registro"=>$html);
        echo json_encode($data);	
    }//fin val session
	}	
	


function ajax_modificar($id){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){
    $row = $this->Es_subModulo_mdl->obtModificar($id);
    $data = array(
    	'id'=>$row->id,
    	'nombre'=>$row->nombre,
    	'url'=>$row->url_sub_modulo,
    	'orden'=>$row->orden,
    	'id_modulo'=>$row->id_modulo
    );


    echo json_encode($data);
}//fin val session	
}


function ajax_guardar_add(){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){
    $status = 0;
	$nombre = $this->input->post('m_nombre');
    $status = $this->ajax_validar_duplicidad($nombre);
    if($status==0){
    	$orden = $this->input->post('m_orden');
		$url = $this->input->post('m_url');
		$idModelo = $this->input->post('id_cbo_modulo');


		$data = array(
	        'nombre'=>$nombre,
	        'url_sub_modulo'=>$url,
	        'orden'=>$orden,
	        'id_modulo'=>$idModelo,
	  	    'id_create'=>$this->session->userdata('idUsuario'),
		);
		$respuesta = $this->Es_subModulo_mdl->guardar_add($data);


	    $row = $this->Es_subModulo_mdl->obt_dataTable();
		$html= $this->generarDatatable($row);
		$data = array(
		    "registro"=>$html,
		    "status"=>$status);
	}else{
		$data = array(
		    "status"=>1);
	}	

    echo json_encode($data);
}//fin val session	
}




function ajax_guardar_mod(){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){
    $status = 0;
	$nombre = $this->input->post('m_nombre');
	$nombre_orig = $this->input->post('nombre_orig');

    if(strtoupper($nombre) != strtoupper($nombre_orig)){
	    $status = $this->ajax_validar_duplicidad($nombre);
	}



    if($status==0){
    	$orden = $this->input->post('m_orden');
		$idModulo = $this->input->post('id_cbo_modulo');
		$url = $this->input->post('m_url');
		$id = $this->input->post('id_mod');

		$data = array(
	        'nombre'=>$nombre,
	        'orden'=>$orden,
	        'id_modulo'=>$idModulo,
	        'url_sub_modulo'=>$url,
	  	    'id_update'=>$this->session->userdata('idUsuario'),
	  	    'date_update'=>date('Y-m-d H:i:s')        
		);
		$respuesta = $this->Es_subModulo_mdl->guardar_mod($id,$data);

	    $row = $this->Es_subModulo_mdl->obt_dataTable();
		$html= $this->generarDatatable($row);
		$data = array(
		    "registro"=>$html,
		    "status"=>$status);
	}else{
		$data = array(
		    "status"=>1);
	}	

    echo json_encode($data);
}//fin val session	
}
	

function ajax_desactivar($id){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){
	$data = array(
  	    'activo'=>0,
  	    'id_update'=>$this->session->userdata('idUsuario'),
  	    'date_update'=>date('Y-m-d H:i:s')
    );


	$respuesta = $this->Es_subModulo_mdl->desactivar($id,$data);
	$row = $this->Es_subModulo_mdl->obt_dataTable();
	$html= $this->generarDatatable($row);
	$data = array(
	    "registro"=>$html);
    echo json_encode($data);
}//fin val session    
}

function ajax_reactivar($id){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){

	$data = array(
  	    'activo'=>1,
  	    'id_update'=>$this->session->userdata('idUsuario'),
  	    'date_update'=>date('Y-m-d H:i:s')
    );
	$respuesta = $this->Es_subModulo_mdl->desactivar($id,$data);
	
	$row = $this->Es_subModulo_mdl->obt_dataTable();
	$html= $this->generarDatatable($row);
	$data = array(
	    "registro"=>$html);
    echo json_encode($data);
}//fin val session
}


	
	
	
	
	function generarDatatable($row){
		//obtener la información de la tabla seleccionada
		$modificar = base_url() . "assets/images/modificar02.jpeg";
		$eliminar = base_url() . "assets/images/eliminar.jpeg";
		$reactivar = base_url() . "assets/images/reactivar.jpeg";
		$lectura = base_url() . "assets/images/ver.png";
		
        $html = '<table id="basic-datatables" class="display table table-striped table-hover">';
        $html.= '    <thead>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="30%">Nombre</th>';
        $html.= '           <th width="20%">Url Sub Modelo</th>';
        $html.= '           <th width="30%">Modelo</th>';
		$html.= '           <th width="5%">Orden</th>';	        
        $html.= '           <th width="15%">Acción</th>';
        $html.= '        </tr>';
        $html.= '    </thead>';
        $html.= '    <tfoot>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="30%">Nombre</th>';
        $html.= '           <th width="20%">Url Sub Modelo</th>';
        $html.= '           <th width="30%">Modelo</th>';
		$html.= '           <th width="5%">Orden</th>';	        
        $html.= '           <th width="15%">Acción</th>';
        $html.= '        </tr>';
        $html.= '    </tfoot>';
        $html.= '    <tbody>';
        if($row==false){
			$html.= '      <tr>';
			$html.= '          <td></td><td></td><td></td><td></td><td></td>';
			$html.= '      </tr>';
		}else{
		    foreach($row as $key){ 
				$html.= '  <tr>';
				$html.= '      <td>' . $key->nombre.'</td>';
				$html.= '      <td>' . $key->url_sub_modulo.'</td>';
				$html.= '      <td>' . $key->modulo.'</td>';
				$html.= '      <td style="text-align:center">' . $key->orden.'</td>';
				$html.= '      <td>';				
				if($key->activo==1){
                    if($this->clssession->accion(4,3)==1){ 
						$html.= '<a href="javascript:void(0)" onclick="javascript:modificar_form('.$key->id.')">';
						$html.= '<img src="'.$modificar.'" style="width:30px; height:30px" 
							alt="Modificar" title="Modificar Sub Módulo"></a>';
					}
						
                    if($this->clssession->accion(4,4)==1){ 					
						$html.= '&nbsp;<a href="javascript:void(0)" onclick="javascript:eliminar_form('.$key->id.')">';
						$html.= '<img src="'.$eliminar.'" style="width:30px; height:30px" alt="Desactivar" title="Desactivar Sub Módulo"></a>';
                    }

				}else{
					if($this->clssession->accion(4,5)==1){ 					
						$html.= '<a href="javascript:void(0)" onclick="javascript:reactivar_form('.$key->id.')">';
						$html.= '<img src="'.$reactivar.'" style="width:30px; height:30px" alt="Activar" title="Reactivar Sub Módulo"></a>';
					}	
				}	
				$html.= '       </td>';				
				$html.= '</tr>';
			}
		}	
		$html.= '    </tbody>';
		$html.= '</table>';
		return $html;    				
	}


	function ajax_validar_duplicidad($nombre){
		$nro = $this->Es_subModulo_mdl->valNombre($nombre);
		$status = 0;
		if($nro>0){
		    $status = 1;	
		}

		return $status;
	}
	

}
