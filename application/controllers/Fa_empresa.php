<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once __DIR__ . '/../../dompdf/autoload.inc.php';

use Dompdf\Dompdf;


class Fa_empresa extends CI_Controller {

	
	public function __construct() {
         parent::__construct();
         $this->load->model(array('Fa_factura_mdl','Cf_sucursal_mdl'));
         $this->load->library(array('Clssession')); 
    }




	public function index(){
    //Validar session usuario
    $valSession = $this->clssession->valSession();
    if($valSession==true){
		$data=array(
			'accion'=>1
		);
       	$this->load->view('header');
		$this->load->view('menu');
		$this->load->view('factura/empresa/cons_facturaEmpresa',$data);
		$this->load->view('footer/footer', $data);
		$this->load->view('factura/empresa/footer_facturaEmpresa', $data);
    }//fin validar session
	}//fin function	

    public function ajax_generarFactura(){
        $idSucursal = $this->input->post('id_cbo_sucursalFactura');
        $swFecha = 0;
        $fecInicial ="";
        $fecFinal ="";

        if($this->input->post("sw_fecha1")==1){
            $swFecha = 1;
            $fecInicial = $this->input->post('m_fechaI'); 
             $fecFinal = ""; 
            if($this->input->post('sw_fecha2')==1){
                $fecFinal = $this->input->post('m_fechaF'); 
                $swFecha = 2;
            }    
        }
        $rowConsulta = $this->Fa_factura_mdl->consultaSucursal($idSucursal,$swFecha, $fecInicial, $fecFinal);
        $dataSession = array(
            "rowConsulta"=>$rowConsulta,
            "idSucursalRep"=>$idSucursal,
            "swFecha"=>$swFecha,
            "fecInicial"=>$fecInicial,
            "fecFinal"=>$fecFinal,
        );
        $this->session->set_userdata($dataSession);
        $html="";
        $status=0;
        if($rowConsulta){
            $html= $this->generarDatatable_consultaFactura($rowConsulta);
        }else{
            $status=1;
        }


        $data = array(
            "registro"=>$html,
            "status"=>$status,
        );
        echo json_encode($data);    
    }


    function generarDatatable_consultaFactura($row){
        //obtener la información de la tabla seleccionada
        $reporte = base_url() . "assets/images/imprimir.jpeg"; 
        $ruta =  base_url('pdf_sucursalFactura');
        $html ='<a href="' . $ruta . '" id="generar01">';
        $html.='<img src="'.$reporte.'" style="width:30px; height:30px" alt="Reporte" title="Reporte">';
        $html.='&nbsp;&nbsp;<span style="font-size:18px; font-weight:500">Generar Reporte</span></a><br> <br>';  
        $html.= '<table id="basic-datatables" class="display table table-striped table-hover">';
        $html.= '    <thead>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="10%">Nro. Factura</th>';
        $html.= '           <th width="10%">fecha</th>';
        $html.= '           <th width="20%">Tipo Despacho</th>';
        $html.= '           <th width="20%">Cliente</th>';
        $html.= '           <th width="10%">Estatus</th>';
        $html.= '           <th width="10%">Monto a Cobrar</th>';
        $html.= '           <th width="10%">Descuento</th>';
        $html.= '           <th width="10%">Total Factura</th>';
        $html.= '        </tr>';
        $html.= '    </thead>';
        $html.= '    <tfoot>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="10%">Nro. Factura</th>';
        $html.= '           <th width="10%">fecha</th>';
        $html.= '           <th width="20%">Tipo Despacho</th>';
        $html.= '           <th width="20%">Cliente</th>';
        $html.= '           <th width="10%">Estatus</th>';
        $html.= '           <th width="10%">Monto a Cobrar</th>';
        $html.= '           <th width="10%">Descuento</th>';
        $html.= '           <th width="10%">Total Factura</th>';
        $html.= '        </tr>';
        $html.= '    </tfoot>';
        $html.= '    <tbody>';
        if($row==false){
            $html.= '      <tr>';
            $html.= '          <td></td><td></td><td></td><td></td><td></td><td></td>';
            $html.= '          <td></td><td></td>';
            $html.= '      </tr>';
        }else{
            foreach($row as $key){ 
                $html.= '  <tr>';
                $html.= '      <td>' . str_pad($key->documento, 6, "0", STR_PAD_LEFT).'</td>';                
                $fecha = substr($key->fecha, 0,10);
                $fecha = substr($key->fecha, 8,2) ."-".substr($key->fecha, 5,2)."-".substr($key->fecha, 0,4);                
                $html.= '      <td>' . $fecha.'</td>';
                $html.= '      <td>' . $key->tipo_despacho.'</td>';

                if($key->nombre_cliente==null || $key->nombre_cliente==""){
                    $nombreCliente = "Sin Información";
                }else{
                    $nombreCliente = $key->nombre_cliente;
                }
                $html.= '      <td>' . $nombreCliente.'</td>';

                $estado = "Valida";
                if($key->reverso==1){
                    $estado = "Cancelada"; 
                }                
                $html.= '      <td>' . $estado.'</td>';

                $montoCobrar = number_format ($key->monto_cobrar, 2,',','.');
                $html.= '      <td style="text-align:right">' . $montoCobrar.'</td>';
                $descuento = number_format ($key->descuento, 2,',','.');
                $html.= '      <td style="text-align:right">' . $descuento.'</td>';
                $montoFactura = number_format ($key->monto_venta, 2,',','.');
                $html.= '      <td style="text-align:right">' . $montoFactura.'</td>';
            }
                    
                $html.= '       </td>';             
                $html.= '</tr>';
        }
        $html.= '    </tbody>';
        $html.= '</table>';
        return $html;                   
    }


    public function imprimir_sucursalFactura(){
        $dompdf = new DOMPDF(array('enable_remote' => true));


        //obtener nombre de la sucursal
        $rowSucursal = $this->Cf_sucursal_mdl->obtVer($this->session->userdata('idSucursalRep')); 
        $nombreSucursal=$rowSucursal->nombre;
        $data = array(
            'empresa'=>$this->session->userdata('empresa'),
            'logoEmpresa'=>$this->session->userdata('logoEmpresa'),
            'rutEmpresa'=>$this->session->userdata('rutEmpresa'),
            'direccionEmpresa'=>$this->session->userdata('direccionEmpresa'),
            'nombreSucursal'=>$nombreSucursal,
            'rowFactura'=>$this->session->userdata('rowConsulta'),
            'swFecha'=>$this->session->userdata('swFecha'),
            'fecInicial'=>$this->session->userdata('fecInicial'),
            'fecFinal'=>$this->session->userdata('fecFinal'),
        );

        $codigoHTML = $this->load->view('pdf/pdf_sucursalFactura', $data  , TRUE );
        $dompdf->loadHtml($codigoHTML);
        //$dompdf->setPaper('A4', 'landscape');
        $dompdf->set_paper("letter", "portrait");
        // Render the HTML as PDF
        $dompdf->render();
        $nombreArchivo = "facturaSucursal". date("Y-m-d-H-i-s").".pdf";  


        $dompdf->stream($nombreArchivo,array('Attachment'=>1));
        
        $data=array(
           'status'=>0
        );
        echo json_encode($data);
    }



///// totales facturacion

public function totalesFacturacion(){
    //Validar session usuario
    $valSession = $this->clssession->valSession();
    if($valSession==true){
        $data=array(
            'accion'=>1
        );
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('factura/empresa/cons_totalesFactura',$data);
        $this->load->view('footer/footer', $data);
        $this->load->view('factura/empresa/footer_facturaEmpresa', $data);
    }//fin validar session
}  


    public function ajax_generarTotalesFactura(){
        $swFecha = 0;
        $fecInicial ="";
        $fecFinal ="";
        if($this->input->post("sw_fecha1")==1){
            $swFecha = 1;
            $fecInicial = $this->input->post('m_fechaI'); 
             $fecFinal = ""; 
            if($this->input->post('sw_fecha2')==1){
                $fecFinal = $this->input->post('m_fechaF'); 
                $swFecha = 2;
            }    
        }
        $rowConsulta = $this->Fa_factura_mdl->consultaTotalesFactura($swFecha, $fecInicial, $fecFinal);
        $dataSession = array(
            "rowConsulta"=>$rowConsulta,
            "swFecha"=>$swFecha,
            "fecInicial"=>$fecInicial,
            "fecFinal"=>$fecFinal,
        );
        $this->session->set_userdata($dataSession);
        $html="";
        $status=0;
        if($rowConsulta){
            $html= $this->generarDatatable_totalesFactura($rowConsulta);
        }else{
            $status=1;
        }


        $data = array(
            "registro"=>$html,
            "status"=>$status,
        );
        echo json_encode($data);    
    }


    function generarDatatable_totalesFactura($row){
        //obtener la información de la tabla seleccionada
        $reporte = base_url() . "assets/images/imprimir.jpeg"; 
        $ruta =  base_url('pdf_totalesFactura');
        $html ='<a href="' . $ruta . '" id="generar01">';
        $html.='<img src="'.$reporte.'" style="width:30px; height:30px" alt="Reporte" title="Reporte">';
        $html.='&nbsp;&nbsp;<span style="font-size:18px; font-weight:500">Generar Reporte</span></a><br> <br>';  
        $html.= '<table id="basic-datatables" class="display table table-striped table-hover">';
        $html.= '    <thead>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="60%">Sucursal</th>';
        $html.= '           <th width="10%">Cantidad factura</th>';
        $html.= '           <th width="10%">Total Despachado</th>';
        $html.= '           <th width="10%">Total Descuento</th>';        
        $html.= '           <th width="10%">Total Factura</th>';
        $html.= '        </tr>';
        $html.= '    </thead>';
        $html.= '    <tfoot>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="60%">Sucursal</th>';
        $html.= '           <th width="10%">Cantidad factura</th>';
        $html.= '           <th width="10%">Total Despachado</th>';
        $html.= '           <th width="10%">Total Descuento</th>';        
        $html.= '           <th width="10%">Total Factura</th>';
        $html.= '        </tr>';
        $html.= '    </tfoot>';
        $html.= '    <tbody>';
        if($row==false){
            $html.= '      <tr>';
            $html.= '          <td></td><td></td><td></td><td></td><td></td>';
            $html.= '      </tr>';
        }else{
            foreach($row as $key){ 
                $html.= '  <tr>';
                $html.= '      <td>' . $key->sucursal.'</td>';                
                $html.= '      <td>' . $key->cantidad_factura.'</td>';      
                $totalDespachado = number_format ($key->total_despachado, 2,',','.');
                $html.= '      <td style="text-align:right">' . $totalDespachado.'</td>';

                $totalDescuento = number_format ($key->total_descuento, 2,',','.');
                $html.= '      <td style="text-align:right">' . $totalDescuento.'</td>';

                $totalFactura = number_format ($key->total_factura, 2,',','.');
                $html.= '      <td style="text-align:right">' . $totalFactura.'</td>';
            }
                    
                $html.= '       </td>';             
                $html.= '</tr>';
        }
        $html.= '    </tbody>';
        $html.= '</table>';
        return $html;                   
    }


    public function imprimir_totalesFactura(){
        $dompdf = new DOMPDF(array('enable_remote' => true));

        //obtener nombre de la sucursal
        $data = array(
            'empresa'=>$this->session->userdata('empresa'),
            'logoEmpresa'=>$this->session->userdata('logoEmpresa'),
            'rutEmpresa'=>$this->session->userdata('rutEmpresa'),
            'direccionEmpresa'=>$this->session->userdata('direccionEmpresa'),
            'rowFactura'=>$this->session->userdata('rowConsulta'),
            'swFecha'=>$this->session->userdata('swFecha'),
            'fecInicial'=>$this->session->userdata('fecInicial'),
            'fecFinal'=>$this->session->userdata('fecFinal'),
        );

        $codigoHTML = $this->load->view('pdf/pdf_totalesFactura', $data  , TRUE );
        $dompdf->loadHtml($codigoHTML);
        //$dompdf->setPaper('A4', 'landscape');
        $dompdf->set_paper("letter", "portrait");
        // Render the HTML as PDF
        $dompdf->render();

        $nombreArchivo = "totalesFactura". date("Y-m-d-H-i-s").".pdf";  
        $dompdf->stream($nombreArchivo,array('Attachment'=>1));        
        
        $data=array(
           'status'=>0
        );
        echo json_encode($data);
    }




} //fin controlador
