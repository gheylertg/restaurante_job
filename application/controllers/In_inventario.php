<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class In_inventario extends CI_Controller {


	
	public function __construct() {
         parent::__construct();
         $this->load->model(array('In_inventario_mdl','In_transaccion_mdl'));
         $this->load->library(array('Clssession')); 
    }


	public function index()	{
	//validar variables de session.
	$valSession = $this->clssession->valSession();
	if($valSession==true){
        //accion 0 vista sin datatable, accion 1  activar datatable 
		$data=array(
			'accion'=>1
		);
	    $this->load->view('header');
		$this->load->view('menu');
		$this->load->view('inventario/inventario/actInventario',$data);
		$this->load->view('footer/footer', $data);
		$this->load->view('inventario/inventario/footer_inventario', $data);
	}//fin val session
    }
	
	function ajax_datatable($id){
	//validar variables de session.
	$valSession = $this->clssession->valSession();
	if($valSession==true){

		$row = $this->In_inventario_mdl->obt_dataTable();
		$html= $this->generarDatatable($row);
		$data = array(
		    "registro"=>$html);
        echo json_encode($data);	

	}//fin val session	
	}


		function generarDatatable($row){
		//obtener la información de la tabla seleccionada
		$modificar = base_url() . "assets/images/modificar02.jpeg";
		$eliminar = base_url() . "assets/images/eliminar.jpeg";
		$reactivar = base_url() . "assets/images/reactivar.jpeg";
		$lectura = base_url() . "assets/images/ver.png";
		
        $html = '<table id="basic-datatables" class="display table table-striped table-hover">';
        $html.= '    <thead>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="10%">Codigo</th>';
        $html.= '           <th width="38%">Nombre</th>';
        $html.= '           <th width="10%">Entradas</th>';
        $html.= '           <th width="10%">Salidas</th>';
        $html.= '           <th width="10%">Stock Mínimo</th>';
        $html.= '           <th width="10%">Disponible</th>';
        $html.= '           <th width="12%">Acción</th>';
        $html.= '        </tr>';
        $html.= '    </thead>';
        $html.= '    <tfoot>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="10%">Codigo</th>';
        $html.= '           <th width="38%">Nombre</th>';
        $html.= '           <th width="10%">Entradas</th>';
        $html.= '           <th width="10%">Salidas</th>';
        $html.= '           <th width="10%">Stock Mínimo</th>';
        $html.= '           <th width="10%">Disponible</th>';
        $html.= '           <th width="12%">Acción</th>';
        $html.= '        </tr>';
        $html.= '    </tfoot>';
        $html.= '    <tbody>';
        if($row==false){
			$html.= '      <tr>';
			$html.= '          <td></td><td></td><td></td><td></td><td></td><td></td><td></td>';
			$html.= '      </tr>';
		}else{
		    foreach($row as $key){ 
				$html.= '  <tr>';
				$html.= '      <td>' . $key->cod_producto.'</td>';				
				$html.= '      <td>' . $key->nombre.'</td>';

                $entrada = number_format ($key->entrada, 2,'.',',');
				$html.= '      <td>' . $entrada.'</td>';

                $salida = number_format ($key->salida, 2,'.',',');

				$html.= '      <td>' . $key->salida.'</td>';

				$html.= '      <td>' . $key->stock.'</td>';
				$disponible = $key->entrada - $key->salida;
				$disponible = number_format ($disponible, 2,'.',',');

				$html.= '      <td>' . $disponible.'</td>';
				$html.= '      <td>';
				$html.= ' <a href="javascript:void(0)" class="btn btn-primary btn-sm item_edit2" onclick="javascript:stockMinimo('.$key->id.')" style="border-radius: 15px; font-size:16px; font-weight:600; height:30px;">Stock Mínimo</a>';
				$html.= '      </td>';
				$html.= '</tr>';
			}
		}	
		$html.= '    </tbody>';
		$html.= '</table>';
		return $html;    				
	}



function ajax_guardar_add(){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){
    $status = 0;
	$idProducto = $this->input->post('id_cbo_productoIn');
    $cantidad = $this->input->post("cantidad");
    $cantidad = str_replace('.','',$cantidad);
    $cantidad = str_replace(',','.',$cantidad);

    $stock = $this->input->post("stock");
    $stock = str_replace('.','',$stock);
    $stock = str_replace(',','.',$stock);

	$idEmpresa = $this->session->userdata('idEmpresa');

	$idSucursal = $this->session->userdata('idSucursal');

	$data = array(
        'id_producto'=>$idProducto,
        'entrada'=>$cantidad,
        'stock'=>$stock,
        'id_usuario_empresa'=>$this->session->userdata('idUsuario'),
		'id_empresa'=>$idEmpresa,
		'id_sucursal'=>$idSucursal,	        
  	    'id_create'=>$this->session->userdata('idUsuario'),
	);

	$this->In_inventario_mdl->guardar_add($data);


    //Guardar entrada inventario.
    $dataTransaccion = array(
      'id_tipo_operacion'=>1,
      'id_producto'=>$idProducto,
      'id_concepto'=>1,
      'cantidad'=>$cantidad,
  	  'id_empresa'=>$idEmpresa,
      'id_sucursal'=>$idSucursal,	        
  	  'id_create'=>$this->session->userdata('idUsuario'),
    );


	$this->In_transaccion_mdl->add_transaccion($dataTransaccion);

	$row = $this->In_inventario_mdl->obt_dataTable();
	$html= $this->generarDatatable($row);
	$data = array(
		"status"=>0,
	    "registro"=>$html
	);
    echo json_encode($data);	
}//fin val session	
}


function ajax_modificar($id){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){

    $row = $this->In_inventario_mdl->obtModificar($id);
    $stock = str_replace('.',',', $row->stock);
    $data = array(
    	'id'=>$row->id,
    	'stock'=>$stock,
    );	
    echo json_encode($data);
}//fin val session
}	


function ajax_guardar_stock_mod(){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){
    $status = 0;

	$id = $this->input->post('idInventario');
    
    $stock = $this->input->post("stockM");
    $stock = str_replace('.','',$stock);
    $stock = str_replace(',','.',$stock);

	$data = array(
		'stock'=>$stock,
  	    'id_update'=>$this->session->userdata('idUsuario'),
        'date_update'=>date('Y-m-d H:i:s')          
    );    

	$this->In_inventario_mdl->guardar_mod_stock($id,$data);

	$row = $this->In_inventario_mdl->obt_dataTable();
	$html= $this->generarDatatable($row);
	$data = array(
		"status"=>0,
	    "registro"=>$html
	);
    echo json_encode($data);	
}//fin val session
}

	

}  //fin del controller
