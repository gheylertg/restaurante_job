<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class In_salida extends CI_Controller {

    public function __construct() {
         parent::__construct();
         $this->load->model(array('In_transaccion_mdl','Mt_producto_mdl','In_inventario_mdl'));
         $this->load->library(array('Clssession','Clsfecha.php')); 
    }


    public function index(){
    //validar variables de session.
    $valSession = $this->clssession->valSession();
    if($valSession==true){

        

        $data=array(
            'accion'=>1,
            'modulo'=>1,
        );

        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('inventario/salida/act_salidaInventario',$data);
        $this->load->view('footer/footer', $data);
        $this->load->view('inventario/salida/footer_salidaInventario', $data);
    }//fin val session
    }


    function ajax_datatable($id){
    //validar variables de session.
    $valSession = $this->clssession->valSession();
    if($valSession==true){

        //$this->load->model('sg_rolesUsuario_mdl');
        $row = $this->In_transaccion_mdl->obt_dataTableSalida();

        $html= $this->generarDatatable($row);
        $data = array(
            "registro"=>$html);
        echo json_encode($data);    
    }//fin val session 
    }   


    function generarDatatable($row){

        //La variable rol trae los diferentes roles sin estar asociados a los modulos. 

        //obtener la información de la tabla seleccionada
        $modificar = base_url() . "assets/images/modificar.jpeg";

        $eliminar = base_url() . "assets/images/eliminar.jpeg";
        $reactivar = base_url() . "assets/images/reactivar.jpeg";
        $lectura = base_url() . "assets/images/ver.png";

        $html = '<table id="basic-datatables" class="display table table-striped table-hover">';
        $html.= '    <thead>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="10%">Fecha</th>';
        $html.= '           <th width="40%">Producto</th>';                
        $html.= '           <th width="20%">Concepto</th>';                
        $html.= '           <th width="10%">Cantidad</th>';
        $html.= '           <th width="20%">Observación / Nota</th>';
        $html.= '        </tr>';
        $html.= '    </thead>';
        $html.= '    <tfoot>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="10%">Fecha</th>';
        $html.= '           <th width="40%">Producto</th>';                
        $html.= '           <th width="20%">Concepto</th>';                
        $html.= '           <th width="10%">Cantidad</th>';
        $html.= '           <th width="20%">Observación / Nota</th>';
        $html.= '        </tr>';
        $html.= '    </tfoot>';
        $html.= '    <tbody>';
        if($row==false){
            $html.= '      <tr>';
            $html.= '          <td></td><td></td><td></td><td></td><td></td>';
            $html.= '      </tr>';
        }else{
            foreach($row as $key){ 
                $fecha = substr($key->fecha,0,19);
                $html.= '  <tr>';
                $html.= '      <td>' . $fecha.'</td>';
                $html.= '      <td>' . $key->producto.'</td>';
                $html.= '      <td>' . $key->concepto.'</td>';
                $cantidad = number_format ($key->cantidad, 2,'.',',');
                
                $html.= '      <td style="text-align:right">' . $cantidad.'</td>';


                $html.= '      <td>' . $key->observacion.'</td>';                $html.= '</tr>';
            }
        }  

        $html.= '    </tbody>';
        $html.= '</table>';
        return $html;                   
    }



public function ajax_guardar_salida(){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){

    $idProducto = $this->input->post("id_cbo_productoIn");

    $cantidad = $this->input->post("cantidad");
    $cantidad = str_replace(".", "", $cantidad);
    $cantidad = str_replace(",", ".", $cantidad);

    $status = 0;

    $status = $this->disponibilidadReal($idProducto, $cantidad);    

    if($status==0){

        $idEmpresa = $this->session->userdata('idEmpresa');
        $idSucursal = $this->session->userdata('idSucursal');

        $data = array(
            'id_producto'=>$idProducto,
            'id_tipo_operacion'=>2,
            'id_concepto'=>$this->input->post("id_cbo_conceptoIn"),
            'cantidad'=>$cantidad,
            'observacion'=>$this->input->post('observacion'),
            'id_empresa'=>$idEmpresa,
            'id_sucursal'=>$idSucursal, 
            'id_create'=>$this->session->userdata('idUsuario'),
        );

        $this->In_transaccion_mdl->guardar_salida($data);

        //obtener cantidad actual del producto
        $status=0;
        $row = $this->In_inventario_mdl->obtInventarioProducto($idEmpresa, $idSucursal, $idProducto);
        if($row){
           $total = $row->salida + $cantidad;
           $data = array(
               'salida'=>$total 
           );
           $this->In_inventario_mdl->in_actTotProducto($idEmpresa, $idSucursal, $idProducto, $data);
        }else{
           $status=1;

        }   
        $row = $this->In_transaccion_mdl->obt_dataTableSalida();
        $html= $this->generarDatatable($row);
        $data = array(
            "registro"=>$html,
            "status"=>0 
        );    
        echo json_encode($data);    
    }else{
        $data = array(
            "status"=>1 
        );    
        echo json_encode($data);    
    }    
}//fin val session    
}



public function disponibilidadReal($idProducto, $cantidad){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){
    $idEmpresa = $this->session->userdata('idEmpresa');
    $idSucursal = $this->session->userdata('idSucursal');
    $row = $this->In_inventario_mdl->obtInventarioProducto($idEmpresa, $idSucursal, $idProducto);
    $total = $row->entrada - $row->salida;
    if($total > $cantidad){
        return 0;
    }else{
        return 1;
    }
}//fin val session
}




public function ajax_disponibilidad($idProducto){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){

    $idEmpresa = $this->session->userdata('idEmpresa');
    $idSucursal = $this->session->userdata('idSucursal');
    $row = $this->In_inventario_mdl->obtInventarioProducto($idEmpresa, $idSucursal, $idProducto);
    $total = $row->entrada - $row->salida;
    $data = array(
        "disponible"=>$total,
    );    
    echo json_encode($data);        
}
}//fin val session


public function ajax_disponibilidadReal(){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){

   $idProducto = $_GET['idProducto'];
   $cantidad = $_GET['cantidad'];

   $cantidad = str_replace(".", "", $cantidad);
   $cantidad = str_replace(",", ".", $cantidad);

   $idEmpresa = $this->session->userdata('idEmpresa');
   $idSucursal = $this->session->userdata('idSucursal');
   $row = $this->In_inventario_mdl->obtInventarioProducto($idEmpresa, $idSucursal, $idProducto);
   $total = $row->entrada - $row->salida;

   if($total >= $cantidad){
       $data = array(
           'status'=>0,
       );
   }else{
       $data = array(
          'status'=>1,
          'disponible' => $total
       );
   }    
   echo json_encode($data);        
    
}//fin val session
}



}  //fin de la clase
