<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mn_alimento extends CI_Controller {



    public function __construct() {
         parent::__construct();
         $this->load->model(array('Mn_alimento_mdl','Mn_ingrediente_mdl','Co_contenedor_mdl'));
         $this->load->library(array('Clssession')); 
    }



    public function index(){
    //validar variables de session.
    $valSession = $this->clssession->valSession();
    if($valSession==true){
        //accion 0 vista sin datatable, accion 1  activar datatable 
        $data=array(
            'accion'=>1,
            'modulo'=>1,
        );

        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('menu/alimento/act_alimento',$data);
        $this->load->view('footer/footer', $data);
        $this->load->view('menu/alimento/footer_alimento', $data);
        		$this->load->view('footer/lib_numerica');
    }//fin val session
    }
    

    function ajax_datatable($id){
    //validar variables de session.
    $valSession = $this->clssession->valSession();
    if($valSession==true){

        $row = $this->Mn_alimento_mdl->obt_dataTable();
        $html= $this->generarDatatable($row);
        $data = array(
            "registro"=>$html);
        echo json_encode($data);    
    } //fin val session
    }   
    
    
    function generarDatatable($row){
        //obtener la información de la tabla seleccionada
        $modificarAlimento = base_url() . "assets/images/modificar02.jpeg";
        $modificarIngrediente = base_url() . "assets/images/ingrediente.png";
        $eliminar = base_url() . "assets/images/eliminar.jpeg";
        $reactivar = base_url() . "assets/images/reactivar.jpeg";
        $lectura = base_url() . "assets/images/ver.png";
        //$opciones = base_url() . "assets/images/presentacionSushi.jpeg";
        $opciones = base_url() . "assets/images/presentacion01.png";
        $imagenes = base_url() . "assets/images/imagen.png";

        
        $html = '<table id="basic-datatables" class="display table table-striped table-hover">';
        $html.= '    <thead>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="20%">Producto</th>';
        $html.= '           <th width="18%">Descripción</th>';   
        $html.= '           <th width="12%">Categoria Menú</th>'; 
        $html.= '           <th width="12%">Imagen</th>';   
        $html.= '           <th width="9%">Estado Actual</th>';   
        $html.= '           <th width="9%">Orden Visualización</th>';           
        $html.= '           <th width="20%">Acción</th>';
        $html.= '        </tr>';
        $html.= '    </thead>';
        $html.= '    <tfoot>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="20%">Producto</th>';
        $html.= '           <th width="18%">Descripción</th>';   
        $html.= '           <th width="12%">Categoria Menú</th>'; 
        $html.= '           <th width="12%">Imagen</th>';   
        $html.= '           <th width="9%">Estado Actual</th>';   
        $html.= '           <th width="9%">Orden Visualización</th>';           
        $html.= '           <th width="20%">Acción</th>';
        $html.= '        </tr>';
        $html.= '    </tfoot>';
        $html.= '    <tbody>';
        if($row==false){
            $html.= '      <tr>';
            $html.= '          <td></td><td></td><td></td><td></td><td></td><td></td><td></td>';    
            $html.= '      </tr>';
        }else{
            $nro=0;
            foreach($row as $key){ 
                $html.= '  <tr>';
                $html.= '      <td>' . $key->nombre.'</td>';
                $html.= '      <td>' . $key->descripcion.'</td>';
                $html.= '      <td>' . $key->categoria_menu.'</td>';

                if($key->imagen==null || empty($key->imagen)){
                    $imagen = base_url() . "assets/images/sinImages.jpeg";
                }else{
                    $imagen = base_url() . "assets/images/producto/" . $key->imagen;
                }
                $html.= '<td style="text-align:center">';
                $html.= '<a href="javascript:void(0)" onclick="javascript:modificarImagen_form('.$key->id.')">';
                $html.= '<img src="'.$imagen.'" style="width:60px; height:40px" alt="Imagen" title="Modificar Imagen"></a>';
                $html.= '</td>';



                if($key->activo==1){
                   $estado = "ACTIVO"; 
                }else{
                   $estado = "INACTIVO";  
                }   
                $html.= '      <td>' . $estado.'</td>';                
                $html.= '      <td style="text-align:center">' . $key->orden.'</td>';                


                    $html.= '      <td>';               

                if($this->clssession->accion(11,1)==1){ 

                    $html.='<a href="javascript:void(0)" onclick="javascript:ver_form('.$key->id.')">';
                    $html.= '<img src="'.$lectura.'" style="width:25px; height:25px" alt="Ver" ';
                    $html.= 'title="Ver Producto"></a>';
                }    

                if($key->activo==1){                

                    if($this->clssession->accion(11,3)==1){ 
                        $html.= '&nbsp;<a href="javascript:void(0)" onclick="javascript:modificar_alimento('.$key->id.')">';    
                        
                        $html.= '<img src="'.$modificarAlimento.'" style="width:30px; height:30px" alt="Modificar" ';
                        $html.= 'title="Actualizar Producto"></a>';
                    }   

                    if($this->clssession->accion(11,3)==1){ 
                        $html.= '&nbsp;<a href="javascript:void(0)" onclick="javascript:modificarImagen_form('.$key->id.')">';    
                        $html.= '<img src="'.$imagenes.'" style="width:30px; height:30px" alt="Modificar Imagen" ';
                        $html.= 'title="Actualizar Imagen del Producto"></a>';
                    } 

                    if($this->clssession->accion(11,4)==1){ 
                        $html.= '&nbsp;<a href="javascript:void(0)" onclick="javascript:desactivarAlimento_form('.$key->id.')">';
                        $html.= '<img src="'.$eliminar.'" style="width:30px; height:30px" alt="Desactivar Producto" title="Desactivar Producto"></a>';
                    }   

                    if($this->clssession->accion(11,3)==1){ 
                        $html.= '&nbsp;<a href="javascript:void(0)" onclick="javascript:mod_ingrediente('.$key->id.')">';
                        $html.= '<img src="'.$modificarIngrediente.'" style="width:30px; height:30px" alt="Modificar" ';
                        $html.= 'title="Actualizar Ingrediente"></a>';
                    }    

                    if($this->clssession->accion(11,3)==1){ 
                        $html.= '&nbsp;<a href="javascript:void(0)" onclick="javascript:act_presentacion('.$key->id.')">';
                        $html.= '<img src="'.$opciones.'" style="width:30px; height:30px" alt="Modificar" ';
                        $html.= 'title="Presentación del Producto"></a>';
                    }    
                }else{
                    if($this->clssession->accion(11,4)==1){ 
                        $html.= '&nbsp;<a href="javascript:void(0)" onclick="javascript:reactivarAlimento_form('.$key->id.')">';
                        $html.= '<img src="'.$reactivar.'" style="width:30px; height:30px" alt="Reactivar Producto" title="Reactivar Producto"></a>';
                    }   

                }    

                $html.= '       </td>';             
                $html.= '</tr>';
            }
        }  

        $html.= '    </tbody>';
        $html.= '</table>';
        return $html;                   
    }


    public function incAlimento(){
    //validar variables de session.
    $valSession = $this->clssession->valSession();
    if($valSession==true){
         $array_ingre = array('nroIngrediente','arregloIngrediente');
         $this->session->unset_userdata($array_ingre);  
            $arregloIngrediente[0][0]="";
            $arregloIngrediente[0][1]="";
            $arregloIngrediente[0][2]="";
            $arregloIngrediente[0][3]="";
            //$arregloIngrediente[0][4]="";
            //$arregloIngrediente[0][5]="";

            //arreglo para el detalle entrada
            $dataArreglo = array(
                'nroIngrediente' => 0,
                'arregloIngrediente' => $arregloIngrediente,
                'status'=>0
            );    
            $this->session->set_userdata($dataArreglo);           
            $data=array(
                'accion'=>1,
                'modulo'=>2,
            );
            $this->load->view('header');
            $this->load->view('menu');
            $this->load->view('menu/alimento/incAlimento',$data);
            $this->load->view('footer/footer', $data);
            $this->load->view('menu/alimento/footer_alimento', $data);
    } //fin val session       
    } 


function ajax_desactivarAlimento($id){
    //validar variables de session.
    $valSession = $this->clssession->valSession();
    if($valSession==true){

        $data = array(
            'activo'=>0,
            'id_update'=>$this->session->userdata('idUsuario'),
            'date_update'=>date('Y-m-d H:i:s')
        );
        $respuesta = $this->Mn_alimento_mdl->desactivar_alimento($id,$data);

        $row = $this->Mn_alimento_mdl->obt_dataTable();
        $html= $this->generarDatatable($row);
        $data = array(
            "registro"=>$html,
            "status"=>0
        );        
        echo json_encode($data);    
    }//fin val session    
}
  
function ajax_reactivarAlimento($id){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){
    //$this->load->model('Es_subModulo_mdl');
    $data = array(
        'activo'=>1,
        'id_update'=>$this->session->userdata('idUsuario'),
        'date_update'=>date('Y-m-d H:i:s')
    );
    $respuesta = $this->Mn_alimento_mdl->desactivar_alimento($id,$data);

    $row = $this->Mn_alimento_mdl->obt_dataTable();
    $html= $this->generarDatatable($row);
    $data = array(
        "registro"=>$html,
        "status"=>0
    );        
    echo json_encode($data);    
}//fin val session    
}


public function ajax_datatable_alimento(){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){
    $nro = $this->session->userdata("nroIngrediente");
    $arreglo = $this->session->userdata("arregloIngrediente");
    $eliminar = base_url() . "assets/images/eliminar.jpeg";
    $modificar = base_url() . "assets/images/modificar.jpeg";

    $html = '<table class="table table-bordered table-head-bg-info ';
    $html.= 'table-bordered-bd-info mt-4">';
    $html.= '<thead>';
    $html.= '<tr>';
    $html.= '    <th scope="col" width="5%">#</th>';
    $html.= '    <th scope="col" width="55%" style="text-align:center">Ingrediente</th>';
    //$html.= '    <th scope="col" width="15%">cantidad</th>';
    $html.= '    <th scope="col" width="30%">Unidad Medida</th>';
    $html.= '    <th scope="col" width="10%" style="text-align:center">Acción</th>';
    $html.= '</tr>';
    $html.= '</thead>';

    $html.= '<tbody>';        

    if($nro == 0){
        $html.= '<tr>';
        $html.= '    <td></td>';
        $html.= '    <td></td>';
        $html.= '    <td></td>';
        //$html.= '    <td></td>';
        $html.= '    <td></td>';
        $html.= '</tr>';
    } 
    else{
        $i = 1;
        while($i <= $nro){
            if(0==0){
                $html.= '<tr>';
                $html.= '    <td>' .$i . '</td>';
                $html.= '    <td>' .$arreglo[$i][1] . '</td>';
                //$html.= '    <td style="text-align:center">' .$arreglo[$i][2] . '</td>';
                $html.= '    <td>' .$arreglo[$i][2] . '</td>';

                //$html.= '<td><a href="javascript:void(0)" onclick="javascript:modificar_ingrediente('.$i.')">';
                //$html.= '<img src="'.$modificar.'" style="width:25px; height:25px" alt="Modificar "></a></td';


                $html.= '<td><a href="javascript:void(0)" onclick="javascript:eliminar_ingrediente('.$i.')">';
                $html.= '<img src="'.$eliminar.'" style="width:25px; height:25px" alt="Eliminar"></a></td';
                $html.= '</tr>';
            }    
            $i+=1;
        }   
    }    
    $html.='</tbody>';
    $html.='</table>';
     
    $data = array(
        "registro"=>$html, 
         "status"=>0,
        "nroIngrediente"=>$nro,
    );
    echo json_encode($data);

}//fin val session
}

public function ajax_modificar_ingredienteAlimento($id){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){
    $arregloE = $this->session->userdata("arregloIngrediente");
    $nroE = $this->session->userdata("nroIngrediente");
    $row = $this->Mn_alimento_mdl->obtModificarIngredienteAlimento($id);
    //$cantidad = str_replace(".",",",$row->cantidad);
    $data = array(
        'id'=>$id,
    //    'cantidad'=>$cantidad,
    );
    echo json_encode($data);
}//fin val session
}


function ajax_guardar_modImagen(){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){  
    $status = 0;
    $imagen = $_FILES['mod_imagen']['name'];
    $id = $this->input->post('id_mod_imagen');
    $opcion=0;
  
    $nombreImagen = $id ."-". date("Y-m-d-H-i-s") . "-mod-imagen-" .$imagen;    
    $config['upload_path']          = './assets/images/producto/'; // Carpeta a subr
    $config['allowed_types']        = 'jpg|png|jpeg'; // Tipos permitidos
    $config['max_size']             = 5000; // Max 2Mb
    $config['max_width']            = 0; // Sin límite de width
    $config['max_height']           = 0; // Sin límite de height
    $config['file_name'] = $nombreImagen; // Nombre de la imagen
    $this->load->library('upload', $config);
    if(!$this->upload->do_upload('mod_imagen')) {    
        $opcion=2;
        $this->index($opcion);
    }
    $data = array(
        'imagen'=>$nombreImagen,
        'id_update'=>$this->session->userdata('idUsuario'),
        'date_update'=>date('Y-m-d H:i:s')        
    );

    $respuesta = $this->Mn_alimento_mdl->guardar_mod_alimento($id,$data);
    $opcion=3;
        $data=array(
            'modulo'=>10,
            'opcion'=>$opcion
        );
        $this->load->view('header');
        $this->load->view('footer/footer', $data);          
        $this->load->view('menu/alimento/footer_alimento', $data);   

}//fin val session  
}   




public function ajax_guardar_ingredienteSB(){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){
    $idIngrediente = $this->input->post("id_cbo_ingrediente");
    //$cantidad = $this->input->post("cantidad");

    $arreglo = $this->session->userdata("arregloIngrediente");
    $nro = $this->session->userdata("nroIngrediente");

    $validar = 0;
    if($nro>0){
        $i=1;
        $swParar = 0;
        while($i <= $nro && $swParar==0){
            if($arreglo[$i][0]==$idIngrediente){
                $swParar=1;
            }
            $i+=1;
        }
        if($swParar==1){
            $validar = 1; 
        }
    }   

    if($validar==0){
        $rowIngrediente = $this->Mn_ingrediente_mdl->obtIngredienteSB($idIngrediente);
        $nro+=1;
        $arreglo[$nro][0] = $idIngrediente;
        $arreglo[$nro][1] = $rowIngrediente->nombre; 
        //$arreglo[$nro][2] = $cantidad;        
        $arreglo[$nro][2] = $rowIngrediente->unidad_medida;
        $arreglo[$nro][3] = $rowIngrediente->id_unidad_medida;


       //arreglo para el detalle entrada
        //borrar variables de session
        $dataArreglo = array(
           'nroIngrediente' => $nro,
           'arregloIngrediente' => $arreglo,
           'status' => 0,
        );


        $this->session->set_userdata($dataArreglo);  
        $this->ajax_datatable_alimento();
    }else{
        $data = array(
            "status"=>1
        );
        echo json_encode($data);
    } 
}//fin val session   
}

public function ajax_guardar_ingrediente_mod(){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){
    $cantidad = $this->input->post("x_cantidad");
    $indice = $this->input->post("id_mod_x");

    $arreglo = $this->session->userdata("arregloIngrediente");
    $nro = $this->session->userdata("nroIngrediente");

    $arreglo[$indice][2] = $cantidad;
        //arreglo para el detalle entrada
        $dataArreglo = array(
           'nroIngrediente' => $nro,
           'arregloIngrediente' => $arreglo,
           'status' => 0,
        );    
        $this->session->set_userdata($dataArreglo);  
        $this->ajax_datatable_alimento();
}//fin val session
}

public function ajax_eliminar_ingrediente($id){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){  
    $arregloE = $this->session->userdata("arregloIngrediente");
    $nroE = $this->session->userdata("nroIngrediente");

    $arregloIngredientes[0][0]="";  // id_ingrediente
    $arregloIngredientes[0][1]="";  // ingrediente
    $arregloIngredientes[0][2]="";  // unidad medida
    $arregloIngredientes[0][3]="";  // id_unidad medida
    //$arregloIngredientes[0][4]="";  // 
    //$arregloIngredientes[0][5] = 0;

    if($nroE==0 || $nroE==1){
       $nroE=0;
    }

    if($nroE>1){
        $i=1;  $indice=1;

        while($i <= $nroE){
            if($i != $id){
                $arregloIngredientes[$indice][0]=$arregloE[$i][0];
                $arregloIngredientes[$indice][1]=$arregloE[$i][1];
                $arregloIngredientes[$indice][2]=$arregloE[$i][2];
                $arregloIngredientes[$indice][3]=$arregloE[$i][3];
                //$arregloIngredientes[$indice][4]=$arregloE[$i][4];
                //$arregloIngredientes[$indice][5]=0;
                $indice+=1;
            }
            $i+=1;
        }  
        $nroE = $nroE - 1;
    }    

    $dataArreglo = array(
        'nroIngrediente' => $nroE,
        'arregloIngrediente' => $arregloIngredientes
    );  
    $this->session->set_userdata($dataArreglo);     
    $this->ajax_datatable_alimento();   

} //fin de val session     
}


public function ajax_guardar_ingredienteAlimento_mod($idAlimento){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){    
    //$cantidad = $this->input->post("m_cantidad");
    $id = $this->input->post("id_mod_mo");

    //$cantidad = str_replace('.','',$cantidad);
    //$cantidad = str_replace(',','.',$cantidad);

    $data=array(
        'cantidad'=>$cantidad,
        'id_update'=>$this->session->userdata("idUsuario"),
        'date_update'=>date('Y-m-d H:i:s')  
    );

    $this->Mn_alimento_mdl->guardar_mod_ingredienteAlimento($id, $data);

    $rowTabla = $this->Mn_alimento_mdl->obt_dataTableIngrediente($idAlimento);
    $html= $this->generarDatatableIngrediente($rowTabla);
    $data = array(
        "registro"=>$html,
        "status"=>0
    );    
    echo json_encode($data); 
}//fin val sesion
}

public function ajax_guardar_alimentoSB(){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){ 
    $status = 0;
    $imagen = $_FILES['m_imagen']['name'];
    if(empty($imagen)){
        $nombreImagen = null;
    }else{
        $nombreImagen = $imagen;
    }    
    $opcion=0;

    if(!empty($imagen)){
        $nombreImagen = $this->session->userdata('idSucursal') . "-" . date('Y-m-d-s')."-producto-" .$imagen;
        $config['upload_path']          = './assets/images/producto/'; // Carpeta a subr
        $config['allowed_types']        = 'jpg|png|jpeg'; // Tipos permitidos
        $config['max_size']             = 5000; // Max 2Mb
        $config['max_width']            = 0; // Sin límite de width
        $config['max_height']           = 0; // Sin límite de height
        $config['file_name'] = $nombreImagen; // Nombre de la imagen
        $this->load->library('upload', $config);
        if(!$this->upload->do_upload('m_imagen')) { 
            $opcion=1;
            //$data=array(
            //    'accion'=>$opcion,
            //    'opcion'=>$opcion
            //);
            //$this->load->view('header');
            //$this->load->view('footer/footer', $data);          
            //$this->load->view('configuracion/empresa/footer_empresa', $data);               
        }
    }

    if($opcion==1){
        $nombreFoto = null;
    }

    $data=array(
        'nombre'=>$this->input->post("m_nombre"),
        'nombre_menu'=>$this->input->post("m_etiqueta"),
        'orden'=>$this->input->post("m_orden"), 
        'descripcion'=>$this->input->post("m_descripcion"), 
        'imagen'=>$nombreImagen,
        'id_categoria_menu'=>$this->input->post("id_cbo_categoriaMenu"), 
        'id_empresa'=>$this->session->userdata("idEmpresa"),
        'id_sucursal'=>$this->session->userdata("idSucursal"),
        'id_create'=>$this->session->userdata("idUsuario")
    );

    $idAlimento = $this->Mn_alimento_mdl->guardar_alimentoSB($data);

    //Guardar contenedor del sabor si la clasificacion del menu = 1 1
    if($this->input->post("id_cbo_categoriaMenu")==1){
        $data = array(
            'id_alimento'=>$idAlimento,
            'cantidad'=>0,
            'id_empresa'=>$this->session->userdata("idEmpresa"),
            'id_sucursal'=>$this->session->userdata("idSucursal"),
            'id_create'=>$this->session->userdata("idUsuario")
        );

        
        $this->Co_contenedor_mdl->guardarContenedor($data);

    }

    
    //Guardar Ingredientes
    $arreglo = $this->session->userdata('arregloIngrediente');
    $nro = $this->session->userdata('nroIngrediente');    

    $idEmpresa = $this->session->userdata("idEmpresa");
    $idSucursal = $this->session->userdata("idSucursal");

    $i=1;
    while($i <= $nro){
        $idIngrediente = $arreglo[$i][0];
        $cantidad = $arreglo[$i][2];
        $cantidad = str_replace('.','',$cantidad);
        $cantidad = str_replace(',','.',$cantidad);

        $data = array(
            'id_alimento'=> $idAlimento,
            'id_ingrediente'=>$idIngrediente,
            //'cantidad'=>$cantidad,
            'id_unidad_medida'=>$arreglo[$i][3],
            'id_create'=>$this->session->userdata("idUsuario"),
        );
        $this->Mn_alimento_mdl->guardar_detalleIngrediente($data);


        $i+=1;  //incrementar indice del arreglo
    }


    $data = array(
       'modulo'=>10
    );

    $this->load->view('header');
    $this->load->view('footer/footer', $data);
    $this->load->view('menu/alimento/footer_alimento', $data);
}//fin val session      
}


function ajax_guardar_mod_alimento (){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){    
    $status = 0;
    $id_alimento =  $this->input->post('id_mod_mo');
    $nombreOrig = $this->input->post("nombreOrig");
    $nombre = $this->input->post("mo_nombre");

    $status = 0;
    if($nombre != $nombreOrig){
        $status = $this->ajax_validar_duplicidad($nombre);
    }

    if($status==0){
        $data=array(
            'nombre'=>$this->input->post("mo_nombre"),
            'nombre_menu'=>$this->input->post("mo_etiqueta"),
            'orden'=>$this->input->post("mo_orden"),
            'descripcion'=>$this->input->post("mo_descripcion"),
            'id_categoria_menu'=>$this->input->post("id_cbo_categoriaMenu"),
            'id_update'=>$this->session->userdata("idUsuario"),
            'date_update'=>date('Y-m-d H:i:s')  
       );

        $this->Mn_alimento_mdl->guardar_mod_alimento($id_alimento, $data);

        //verificar si modificaron el tipo de menu y si posee contenedor, de no ser asi crearlo
        $resultado = $this->Co_contenedor_mdl->val_existenciaContenedor($id_alimento);
        if($this->input->post("id_cbo_categoriaMenu")==1){
             if($resultado==0){
                    $data = array(
                        'id_alimento'=>$id_alimento,
                        'cantidad'=>0,
                        'id_empresa'=>$this->session->userdata("idEmpresa"),
                        'id_sucursal'=>$this->session->userdata("idSucursal"),
                        'id_create'=>$this->session->userdata("idUsuario")
                    );
                    $this->Co_contenedor_mdl->guardarContenedor($data);
             }

        }else{
            if($resultado==1){
               $this->Co_contenedor_mdl->borrarContenedor($id_alimento);
            }
        }
        $row = $this->Mn_alimento_mdl->obt_dataTable();
        $html= $this->generarDatatable($row);
        $data = array(
            "registro"=>$html,
            "status"=>0
        );        
        echo json_encode($data);    
     }else{
        $data = array(
            "status"=>1
        );        
        echo json_encode($data);    
     }   

}//fin val session
}   



    function ajax_validar_duplicidad(){
        $nombre = $_GET['nombre'];  
        $nro = $this->Mn_alimento_mdl->valNombre($nombre);
        $status = 0;
        if($nro>0){
            $status = 1;    
        }
        $data = array(
            "status"=>$status
        );        
        echo json_encode($data);    
    }



    public function verAlimento($id){
    //validar variables de session.
    $valSession = $this->clssession->valSession();
    if($valSession==true){      
        $rowAlimento = $this->Mn_alimento_mdl->verAlimento($id);
        $arregloAlimento[0] = $rowAlimento->nombre;
        $arregloAlimento[1] = $rowAlimento->descripcion;
        $rowAlimentoIngrediente = $this->Mn_alimento_mdl->obt_dataTableIngrediente($rowAlimento->id);

        $i = 0;
        if($rowAlimentoIngrediente==false){
            $arregloIngrediente[0][0] = "";
            $arregloIngrediente[0][1] = ""; 
            $arregloIngrediente[0][2] = "";
        }else{
            foreach($rowAlimentoIngrediente as $key){
                $arregloIngrediente[$i][0] = $key->id;
                $arregloIngrediente[$i][1] = $key->ingrediente; 
                $arregloIngrediente[$i][2] = $key->unidad_medida;
                $i+=1;
            }   
        }    
       
        //arreglo para el detalle entrada
        $dataArreglo = array(
            'arregloAlimento'=>$arregloAlimento,
            'nroIngrediente' => $i,
            'arregloIngrediente' => $arregloIngrediente,
            'status'=>0
        );    
        $this->session->set_userdata($dataArreglo);           
        $data=array(
            'accion'=>1,
            'modulo'=>4,
            'idAlimento'=>$id
        );
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('menu/alimento/verAlimento',$data);
        $this->load->view('footer/footer', $data);
        $this->load->view('menu/alimento/footer_alimento', $data);
    }//fin val session  
    }

    public function ajax_datatable_verAlimento($idAlimento){
    //validar variables de session.
    $valSession = $this->clssession->valSession();
    if($valSession==true){          
        $nro = $this->session->userdata("nroIngrediente");
        $arreglo = $this->session->userdata("arregloIngrediente");
        $arregloAlimento = $this->session->userdata("arregloAlimento");
        $eliminar = base_url() . "assets/images/eliminar.jpeg";
        $html = '<table class="table table-bordered table-head-bg-info ';
        $html.= 'table-bordered-bd-info mt-4">';
        $html.= '<thead>';
        $html.= '<tr>';
        $html.= '    <th scope="col" width="5%">#</th>';
        $html.= '    <th scope="col" width="70%">Ingrediente</th>';
        $html.= '    <th scope="col" width="25%">Unidad Medida</th>';
        $html.= '</tr>';
        $html.= '</thead>';

        $html.= '<tbody>';        

            $i = 0;
            while($i < $nro){
                $html.= '<tr>';
                $num = $i + 1;
                $html.= '    <td>' .$num. '</td>';
                $html.= '    <td style="text-align:left">' .$arreglo[$i][1] . '</td>';
                $html.= '    <td style="text-align:left">' .$arreglo[$i][2] . '</td>';
                $html.= '</tr>';
                $i+=1;
            }   

        $html.='</tbody>';
        $html.='</table>';
        
        $nombre = $arregloAlimento[0];
        $descripcion = $arregloAlimento[1];
        
        //Construir presentacion
        $htmlP = $this->ver_construirPresentacion($idAlimento);
        
        

        $data = array(
            "registro"=>$html,
            "registroP"=>$htmlP,
            "status"=>0,
            "nroIngrediente"=>$nro,
            "nombre"=>$nombre,
            "descripcion"=>$descripcion,
        );
         echo json_encode($data);        
    }//fin val session
    }
    
    function ver_construirPresentacion($idAlimento){
		$rowP = $this->Mn_alimento_mdl->obt_dataTablePresentacion($idAlimento);

        $htmlP = '<table class="table table-bordered table-head-bg-info ';
        $htmlP.= 'table-bordered-bd-info mt-4">';
        $htmlP.= '<thead>';
        $htmlP.= '<tr>';
        $htmlP.= '    <th scope="col" width="5%">#</th>';
        $htmlP.= '    <th scope="col" width="65%">Presentación</th>';
        $htmlP.= '    <th scope="col" width="15%">Nro Pieza / Porción</th>';
        $htmlP.= '    <th scope="col" width="15%">Precio Venta</th>';
        $htmlP.= '</tr>';
        $htmlP.= '</thead>';
        $htmlP.= '<tbody>';        
        $swP = 0;
        $i = 0;
        foreach($rowP as $key){ 
			$swP = 1;
            $htmlP.= '<tr>';
            $i = $i + 1;
			$htmlP.= '    <td>' .$i. '</td>';
			$htmlP.= '    <td style="text-align:left">' .$key->tipo_presentacion. '</td>';
			$htmlP.= '    <td style="text-align:center">' .$key->nro_pieza. '</td>';
			$htmlP.= '    <td style="text-align:right">' .$key->precio. '</td>';
			$htmlP.= '</tr>';
		}
		
		if($swP == 0){
            $htmlP.= '<tr>';
			$htmlP.= '    <td></td>';
			$htmlP.= '    <td></td>';
			$htmlP.= '    <td></td>';
			$htmlP.= '    <td></td>';
			$htmlP.= '</tr>';
		}
			
        $htmlP.='</tbody>';
        $htmlP.='</table>';
        return $htmlP;
	}
    
    


    
    function generarDatatableIngrediente($row){
        //La variable rol trae los diferentes roles sin estar asociados a los modulos. 
        //obtener la información de la tabla seleccionada
        $modificar = base_url() . "assets/images/modificar02.jpeg";
        $eliminar = base_url() . "assets/images/eliminar.jpeg";
        $reactivar = base_url() . "assets/images/reactivar.jpeg";
        $lectura = base_url() . "assets/images/ver.png";

        $html = '<table id="basic-datatables" class="display table table-striped table-hover">';
        $html.= '    <thead>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="55%">Nombre Producto</th>';
        $html.= '           <th width="30%">Unidad Medida</th>';        
        $html.= '           <th width="15%">Acción</th>';
        $html.= '        </tr>';
        $html.= '    </thead>';
        $html.= '    <tfoot>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="55%">Nombre Producto</th>';
        $html.= '           <th width="30%">Unidad Medida</th>';        
        $html.= '           <th width="15%">Acción</th>';
        $html.= '        </tr>';
        $html.= '    </tfoot>';
        $html.= '    <tbody>';
        if($row==false){
            $html.= '      <tr>';
            $html.= '         <td></td><td></td><td></td>';
            $html.= '      </tr>';
        }else{
            foreach($row as $key){ 
                $html.= '  <tr>';
                $html.= '      <td>' . $key->ingrediente.'</td>';
                $html.= '      <td>' . $key->unidad_medida.'</td>';                
                $html.= '      <td>';               
                //$html.= '<a href="javascript:void(0)" onclick="javascript:modificar_form('.$key->id.')">';

                if($this->clssession->accion(11,4)==1){ 
                  $html.='&nbsp;<a href="javascript:void(0)" onclick="javascript:elim_ingrediente('.$key->id.')">';
                  $html.= '<img src="'.$eliminar.'" style="width:25px; height:25px" alt="Eliminar" ';
                  $html.= 'title="Eliminar Ingrediente"></a>';'></a>';
                }    

                $html.= '       </td>';             
                $html.= '</tr>';
            }
        }  

        $html.= '    </tbody>';
        $html.= '</table>';
        return $html;                   
    }





 





public function ajax_act_modificar_alimento($id){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){          
    $row = $this->Mn_alimento_mdl->obtModificarAlimento($id);
    $data = array(
        'id'=>$row->id,
        'id_categoriaMenu'=>$row->id_categoria_menu,
        'nombre'=>$row->nombre,
        'orden'=>$row->orden,
        'etiqueta'=>$row->nombre_menu,
        'descripcion'=>$row->descripcion,
    );
   
    echo json_encode($data);    
}//fin val session
}


public function ajax_modificarImagen($id){
$valSession = $this->clssession->valSession();
if($valSession==true){          
    $row = $this->Mn_alimento_mdl->obtModificarAlimento($id);
    if($row->imagen==null || $row->imagen==""){
        $imagen = base_url() . "assets/images/sinImages.jpeg";
    }else{
        $imagen = base_url() . "assets/images/producto/" . $row->imagen;    
    }    

    $html = '<img src="'.$imagen.'" style="width:350px; height:350px" title="Imagen Actual">';   
    $data = array(
        'imagen'=>$html,
    );
   
    echo json_encode($data);    
}
}



////////////////////////////////////////////////////////////////
public function ajax_guardar_add_ingredienteAlimento(){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){          
    $idAlimento = $this->session->userdata("idAlimento");
    $idIngrediente = $this->input->post("id_cbo_ingrediente");
    $cantidad = $this->input->post("cantidad");
    $status = 0;
   
    $status = $this->Mn_alimento_mdl->valIngrediente($idAlimento, $idIngrediente);
    if($status==0){
		
        $rowIngrediente = $this->Mn_ingrediente_mdl->obtIngredienteSB($idIngrediente);
        
		$data = array(
			  'id_alimento'=>$idAlimento, 
			  'id_ingrediente'=>$idIngrediente,
              'id_unidad_medida'=>$rowIngrediente->id_unidad_medida,
			  'id_create'=>$this->session->userdata('idUsuario'),
		);
		$idAlimentoIngrediente = $this->Mn_alimento_mdl->guardar_ingrediente_add($data);
        //actualizar ingredientes por presentacion
        $rowPresentacion = $this->Mn_alimento_mdl->obt_presentacionIngrediente($idAlimento);
        
        foreach ($rowPresentacion as $key) {
            $data = array(
                'id_alimento_presentacion'=>$key->id,
                'id_ingrediente'=>$idIngrediente,
                'id_create'=>$this->session->userdata('idUsuario')
            );
            $this->Mn_alimento_mdl->guardarIngredientePresentacion($data);
        }
        
        $rowTabla = $this->Mn_alimento_mdl->obt_dataTableIngrediente($idAlimento);
        $html= $this->generarDatatableIngrediente($rowTabla);
        $data = array(
            "registro"=>$html,
            "status"=>0
        );    
        echo json_encode($data);         

	}else{
		$data = array(
			"status"=>$status
		);
        echo json_encode($data);
	}	

}//fin val session
}    



public function act_modificar_ingrediente($id){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){          
   //accion 0 vista sin datatable, accion 1  activar datatable 
    $data=array(
        'accion'=>1,
        'modulo'=>3,
        'idAlimento'=>$id,
    );
    
    $this->load->view('header');
    $this->load->view('menu');
    $this->load->view('menu/alimento/act_ingredienteAlimento',$data);
    $this->load->view('footer/footer', $data);
    $this->load->view('menu/alimento/footer_alimento', $data);
}    
}//Fin val session


    

function ajax_datatableAlimento($id){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){          
	
	$dataSession = array(
	  'idAlimento'=>$id
	);
	
	$this->session->set_userdata($dataSession);
    $rowAlimento = $this->Mn_alimento_mdl->obtModificarAlimento($id);
    
    
    $dataSession = array(
        'idAlimento'=>$id,
        'nobAlimento'=>$rowAlimento->nombre
    );
    
    $row = $this->Mn_alimento_mdl->obt_dataTableIngrediente($id);
    $html= $this->generarDatatableIngrediente($row);
    $data = array(
        "nombre"=>$rowAlimento->nombre,
        "registro"=>$html);
    echo json_encode($data);    

}//Fin val session   
}


public function ajax_borrar_ingrediente(){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){
	$idDetalle = $_GET['id'];
	$idAlimento = $this->session->userdata('idAlimento');
    $status=0;
		$idEmpresa = $this->session->userdata('idEmpresa');
		$idSucursal = $this->session->userdata('idSucursal');
		$resultado = $this->Mn_alimento_mdl->eliminarIngrediente($idDetalle);		 
		$rowTabla = $this->Mn_alimento_mdl->obt_dataTableIngrediente($idAlimento);
		$html= $this->generarDatatableIngrediente($rowTabla);
		$data = array(
		    "registro"=>$html,
			"status"=>0
		);    
		echo json_encode($data); 
}//fin val session
}





////////////////////// opciones o presentacion del alimento
public function act_presentacion($id=0){
//validar variables de session.

$valSession = $this->clssession->valSession();
if($valSession==true){
   //accion 0 vista sin datatable, accion 1  activar datatable 

    if($id==0){
       $id = $this->session->userdata('idAlimento');
    }else{
        $rowAlimento = $this->Mn_alimento_mdl->obtModificarAlimento($id);
        $dataSession = array(
            'idAlimento'=>$id,
            'nobAlimento'=>$rowAlimento->nombre
        );
        $this->session->set_userdata($dataSession);
    }  


    $data=array(
        'accion'=>1,
        'modulo'=>5,
        'idAlimento'=>$id,
    );
    
    $this->load->view('header');
    $this->load->view('menu');
    $this->load->view('menu/alimento/act_presentacion',$data);
    $this->load->view('footer/footer', $data);
    $this->load->view('menu/alimento/footer_alimento', $data);
}//fin val session    
}


function ajax_datatablePresentacion($id){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){

    //$this->load->model('sg_rolesUsuario_mdl');
    $row = $this->Mn_alimento_mdl->obt_dataTablePresentacion($id);
    $html= $this->generarDatatablePresentacion($row);
    $data = array(
        "registro"=>$html,
        "nobAlimento"=>$this->session->userdata("nobAlimento")
    );
    echo json_encode($data);    
}//fin val session
}   


function generarDatatablePresentacion($row){
        //La variable rol trae los diferentes roles sin estar asociados a los modulos. 
        //obtener la información de la tabla seleccionada
        $modificar = base_url() . "assets/images/modificar02.jpeg";
        $eliminar = base_url() . "assets/images/eliminar.jpeg";
        $reactivar = base_url() . "assets/images/reactivar.jpeg";
        $lectura = base_url() . "assets/images/ver.png";
        $ingrediente = base_url() . "assets/images/ingrediente.jpeg";


        $html = '<table id="basic-datatables" class="display table table-striped table-hover">';
        $html.= '    <thead>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="45%">Presentacion</th>';
        $html.= '           <th width="15%">Nro Piezas / porción</th>';        
        $html.= '           <th width="15%">Precio de Venta</th>';        
        $html.= '           <th width="10%">Orden Visualización</th>';        
        $html.= '           <th width="15%">Acción</th>';
        $html.= '        </tr>';
        $html.= '    </thead>';
        $html.= '    <tfoot>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="45%">Presentacion</th>';
        $html.= '           <th width="15%">Nro Piezas / porción</th>';        
        $html.= '           <th width="15%">Precio de Venta</th>';        
        $html.= '           <th width="10%">Orden Visualización</th>';                
        $html.= '           <th width="15%">Acción</th>';
        $html.= '        </tr>';
        $html.= '    </tfoot>';
        $html.= '    <tbody>';
        if($row==false){
            $html.= '      <tr>';
            $html.= '         <td></td><td></td>
            <td></td>
            <td></td>
            <td></td>
           ';
                                
            $html.= '      </tr>';
        }else{
            foreach($row as $key){ 
                $html.= '  <tr>';
                $html.= '      <td>' . $key->tipo_presentacion.'</td>';
                $html.= '      <td style="text-align:center">' . $key->nro_pieza.'</td>'; 
                
                $precio = number_format ($key->precio, 2,'.',',');  

                $html.= '      <td>' . $precio.'</td>'; 
                $html.= '      <td style="text-align:center">' . $key->orden.'</td>'; 



                $html.= '      <td>';               
                //$html.= '<a href="javascript:void(0)" onclick="javascript:modificar_form('.$key->id.')">';

                if($key->activo==1){ 
                    if($this->clssession->accion(11,3)==1){ 
                      $html.='&nbsp;<a href="javascript:void(0)" onclick="javascript:mod_presentacion('.$key->id.')">';
                      $html.= '<img src="'.$modificar.'" style="width:25px; height:25px" alt="Modificar Presentación" ';
                      $html.= 'title="Modificar Presentación"></a>';'></a>';
                    } 

                    if($this->clssession->accion(11,4)==1){ 
                      $html.='&nbsp;<a href="javascript:void(0)" onclick="javascript:desactivar_presentacion('.$key->id.')">';
                      $html.= '<img src="'.$eliminar.'" style="width:25px; height:25px" alt="Desactivar" ';
                      $html.= 'title="Desactivar Presentación"></a>';'></a>';
                    }    

                    $html.='&nbsp;<a href="javascript:void(0)" onclick="javascript:act_alimento_ingrediente('.$key->id.')">';
                    $html.= '<img src="'.$ingrediente.'" style="width:25px; height:25px" alt="Actualizar" ';
                    $html.= 'title="Cantidad ingrediente utilizado "></a>';'></a>';
                }else{
                    if($this->clssession->accion(11,4)==1){ 
                      $html.='&nbsp;<a href="javascript:void(0)" onclick="javascript:reactivar_presentacion('.$key->id.')">';
                      $html.= '<img src="'.$reactivar.'" style="width:25px; height:25px" alt="Reactivar" ';
                      $html.= 'title="Reactivar Presentación"></a>';'></a>';
                    }    
                }    
                $html.= '       </td>';             
                $html.= '</tr>';
            }
        }  

        $html.= '    </tbody>';
        $html.= '</table>';
        return $html;                   
}




public function incPresentacion(){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){
    $data=array(
        'accion'=>1,
        'modulo'=>6,
    );
    $this->load->view('header');
    $this->load->view('menu');
    $this->load->view('menu/alimento/incPresentacion',$data);
    $this->load->view('footer/footer', $data);
    $this->load->view('menu/alimento/footer_alimento', $data);
}//fin val session 
}


public function ajax_guardar_presentacion(){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){
    $idAlimento = $this->session->userdata("idAlimento");
    $id_tipoPresentacion = $this->input->post('id_cbo_tipoPresentacion');

    $validar = 0;

    //Validar nombre versus alimento
    $validar = $this->Mn_alimento_mdl->valTipoPresentacion($idAlimento, $id_tipoPresentacion);
    if($validar==0){
        $pieza = $this->input->post("m_pieza");

        // Precio.     
        $precio = $this->input->post("m_precio");
        $precio = str_replace(".", "", $precio);
        $precio = str_replace(",", ".", $precio);    


        $precio = str_replace(",", ".", $precio);    

        $data = array(
            'id_alimento'=>$idAlimento,
            'id_tipo_presentacion'=>$id_tipoPresentacion,
            'nro_pieza'=>$this->input->post("m_pieza"),
            'id_unidad_medida'=>$this->input->post("id_cbo_unidadMedida"),
            'precio'=>$precio,
            'orden'=>$this->input->post("m_orden"),
            'id_create'=>$this->session->userdata('idUsuario'),
        );
       
        //Guardar Presentación aliemneto. 
        $idPresentacion = $this->Mn_alimento_mdl->guardar_add_presentacion($data); 

        //Crear los ingredientes automaticamente en 
        $rowIngrediente = $this->Mn_alimento_mdl->obtIngredienteAlimento($idAlimento);

        if($rowIngrediente){
            foreach ($rowIngrediente as $key) {
                $data = array(
                    'id_alimento_presentacion'=>$idPresentacion,
                    'id_ingrediente'=>$key->id_ingrediente,
                    'id_create'=>$this->session->userdata('idUsuario'),
                );

                $this->Mn_alimento_mdl->guardarIngredientePresentacion($data);


            }
        }



        $row = $this->Mn_alimento_mdl->obt_dataTablePresentacion($idAlimento);
        $html= $this->generarDatatablePresentacion($row);
        $data = array(
            "registro"=>$html,
            "status"=>0,
            "idAlimento"=>$idAlimento
        );

        echo json_encode($data);           
    }else{
        $data = array(
            "status"=>1
        );
        echo json_encode($data);
    }    

}//fin val session   
}


public function ajax_modificar_presentacion($id){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){
    $row = $this->Mn_alimento_mdl->obtModificarPresentacion($id);

    $precio = str_replace(".",",",$row->precio);
    $data = array(
        'id'=>$row->id,
        'nombre'=>$row->tipo_presentacion,
        'pieza'=>$row->nro_pieza,
        'unidadMedida'=>$row->id_unidad_medida,
        'orden'=>$row->orden,
        'precio'=>$precio,
    );
   
    echo json_encode($data);    
}//fin val session
}

public function ajax_guardar_mod_presentacion(){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){

    $idAlimento = $this->session->userdata('idAlimento');
    $id_presentacion = $this->input->post('id_mod');
    $validar = 0;
    $pieza = $this->input->post("m_pieza");

    // Precio.     
    $precio = $this->input->post("m_precio");
    $precio = str_replace(".", "", $precio);
    $precio = str_replace(",", ".", $precio);    


    $data = array(
        'nro_pieza'=>$this->input->post("m_pieza"),
        'precio'=>$precio,
        'orden'=>$this->input->post("m_orden"),
        'id_update'=>$this->session->userdata("idUsuario"),
        'date_update'=>date('Y-m-d H:i:s') 
    );
   
    //Guardar Presentación aliemneto. 
    $this->Mn_alimento_mdl->guardar_mod_presentacion($id_presentacion, $data); 

    $row = $this->Mn_alimento_mdl->obt_dataTablePresentacion($idAlimento);
    $html= $this->generarDatatablePresentacion($row);
    $data = array(
        "registro"=>$html,
        "status"=>0,
        "idAlimento"=>$idAlimento
    );

    echo json_encode($data);           

}//fin val session   
}

function ajax_desactivar_presentacion($id){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){    
    //$this->load->model('Es_subModulo_mdl');
    $idAlimento = $this->session->userdata('idAlimento');
    $data = array(
        'activo'=>0,
        'id_update'=>$this->session->userdata('idUsuario'),
        'date_update'=>date('Y-m-d H:i:s')
    );

    $respuesta = $this->Mn_alimento_mdl->guardar_mod_presentacion($id,$data);
    
    $row = $this->Mn_alimento_mdl->obt_dataTablePresentacion($idAlimento);
    $html= $this->generarDatatablePresentacion($row);

    $data = array(
        "registro"=>$html);
    echo json_encode($data);
}//fin val session    
}

function ajax_reactivar_presentacion($id){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){    
    //$this->load->model('Es_subModulo_mdl');
    $idAlimento = $this->session->userdata('idAlimento');
    $data = array(
        'activo'=>1,
        'id_update'=>$this->session->userdata('idUsuario'),
        'date_update'=>date('Y-m-d H:i:s')
    );

    $respuesta = $this->Mn_alimento_mdl->guardar_mod_presentacion($id,$data);
    $row = $this->Mn_alimento_mdl->obt_dataTablePresentacion($idAlimento);
    $html= $this->generarDatatablePresentacion($row);

    $data = array(
        "registro"=>$html);
    echo json_encode($data);

}//fin val session
}






//////////////////////////////////////////////////////////////////////

public function act_presentacion_ingrediente($idPresentacionIngrediente){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){    
    $dataSesion = array(
       'idPresentacionIngrediente'=>$idPresentacionIngrediente
    );

    $this->session->set_userdata($dataSesion);

    $data=array(
        'accion'=>1,
        'modulo'=>9,
        'idPresentacionIngrediente'=>$idPresentacionIngrediente,
    );
    
    $this->load->view('header');
    $this->load->view('menu');
    $this->load->view('menu/alimento/act_presentacionIngrediente',$data);
    $this->load->view('footer/footer', $data);
    $this->load->view('menu/alimento/footer_alimento', $data);
}//fin val session
}


//////////////////////  Presentacion de ingrediente

function ajax_datatablePresentacionIngrediente($id){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){    

    $row = $this->Mn_alimento_mdl->obt_dataTablePresentacionIngrediente($id);
    $html= $this->generarDatatablePresentacionIngrediente($row);
    $data = array(
        "registro"=>$html);
    echo json_encode($data);    

}//fin val session   
}


function generarDatatablePresentacionIngrediente($row){
    //La variable rol trae los diferentes roles sin estar asociados a los modulos. 
    //obtener la información de la tabla seleccionada
    $modificar = base_url() . "assets/images/modificar02.jpeg";
    $eliminar = base_url() . "assets/images/eliminar.jpeg";
    $reactivar = base_url() . "assets/images/reactivar.jpeg";
    $lectura = base_url() . "assets/images/ver.png";
    $ingrediente = base_url() . "assets/images/ingrediente.jpeg";


    $html = '<table id="basic-datatables" class="display table table-striped table-hover">';
    $html.= '    <thead>';
    $html.= '        <tr class="tr-datatable">';
    $html.= '           <th width="50%">Ingrediente</th>';
    $html.= '           <th width="15%">Cantidad</th>';
    $html.= '           <th width="20%">unidad Medida</th>';
    $html.= '           <th width="15%">Acción</th>';
    $html.= '        </tr>';
    $html.= '    </thead>';
    $html.= '    <tfoot>';
    $html.= '        <tr class="tr-datatable">';
    $html.= '           <th width="50%">Ingrediente</th>';
    $html.= '           <th width="15%">Cantidad</th>';
    $html.= '           <th width="20%">unidad Medida</th>';
    $html.= '           <th width="15%">Acción</th>';
    $html.= '        </tr>';
    $html.= '    </tfoot>';
    $html.= '    <tbody>';
    if($row==false){
        $html.= '      <tr>';
        $html.= '         <td></td>
        <td></td>
        <td></td>
        <td></td>

       ';
                            
        $html.= '      </tr>';
    }else{
        foreach($row as $key){ 
            $html.= '  <tr>';
            $html.= '      <td>' . $key->ingrediente.'</td>';
            $cantidad = number_format ($key->cantidad, 2,'.',',');  

            $html.= '      <td>' . $cantidad.'</td>'; 
            $html.= '      <td>' . $key->unidad_medida.'</td>';                 

            $html.= '      <td>';               
            //$html.= '<a href="javascript:void(0)" onclick="javascript:modificar_form('.$key->id.')">';

            $html.='&nbsp;<a href="javascript:void(0)" onclick="javascript:act_alimento_ingrediente('.$key->id.')">';
            $html.= '<img src="'.$modificar.'" style="width:25px; height:25px" alt="Eliminar" ';
            $html.= 'title="Cantidad ingrediente utilizado "></a>';'></a>';



            $html.= '       </td>';             
            $html.= '</tr>';
        }
    }  

    $html.= '    </tbody>';
    $html.= '</table>';
    return $html;                   
}



}  //fin de la clase
