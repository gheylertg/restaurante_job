<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mn_ingredienteAdicional extends CI_Controller {

	
	public function __construct() {
         parent::__construct();
         $this->load->model(array('Mn_alimento_mdl','Mn_ingrediente_mdl','Mn_ingredienteAdicional_mdl'));

         $this->load->library(array('Clssession')); 
    }





public function index()	{
    //validar variables de session.
    $valSession = $this->clssession->valSession();
    if($valSession==true){
        //accion 0 vista sin datatable, accion 1  activar datatable 
        $data=array(
            'accion'=>1,
            'modulo'=>1,
        );

        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('menu/ingredienteAdicional/act_ingredienteAdicional',$data);
        $this->load->view('footer/footer', $data);
        $this->load->view('menu/ingredienteAdicional/footer_ingredienteAdicional', $data);
    }//fin val session

}

    function ajax_datatable(){
    //validar variables de session.
    $valSession = $this->clssession->valSession();
    if($valSession==true){

        $row = $this->Mn_ingredienteAdicional_mdl->obt_dataTable();
        $html= $this->generarDatatable($row);
        $data = array(
            "registro"=>$html);
        echo json_encode($data);    
    } //fin val session
    }   
    
    
    function generarDatatable($row){
        //obtener la información de la tabla seleccionada
        $modificar = base_url() . "assets/images/modificar02.jpeg";
        $eliminar = base_url() . "assets/images/eliminar.jpeg";
        $reactivar = base_url() . "assets/images/reactivar.jpeg";
        $lectura = base_url() . "assets/images/ver.png";
                
        $html = '<table id="basic-datatables" class="display table table-striped table-hover">';
        $html.= '    <thead>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="25%">Ingrediente</th>';
        $html.= '           <th width="15%">Etiqueta Menú</th>';
        $html.= '           <th width="10%">Cantidad Ingrediente</th>';   
        $html.= '           <th width="15%">Unidad Medida</th>';
        $html.= '           <th width="10%">Precio</th>';
        $html.= '           <th width="10%">Estado</th>';   
        $html.= '           <th width="15%">Acción</th>';
        $html.= '        </tr>';
        $html.= '    </thead>';
        $html.= '    <tfoot>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="25%">Ingrediente</th>';
        $html.= '           <th width="15%">Etiqueta Menú</th>';
        $html.= '           <th width="10%">Cantidad Ingrediente</th>';   
        $html.= '           <th width="15%">Unidad Medida</th>';
        $html.= '           <th width="10%">Precio</th>';
        $html.= '           <th width="10%">Estado</th>';   
        $html.= '           <th width="15%">Acción</th>';
        $html.= '        </tr>';
        $html.= '    </tfoot>';
        $html.= '    <tbody>';

        if($row==false){
            $html.= '      <tr>';
            $html.= '          <td></td><td></td><td></td><td></td><td></td><td></td><td></td>';    
            $html.= '      </tr>';
        }else{
            $nro=0;
            foreach($row as $key){ 
                $html.= '  <tr>';
                $html.= '      <td>' . $key->nombre.'</td>';
                $html.= '      <td>' . $key->nombre_menu.'</td>';
                $html.= '      <td>' . $key->cantidad_ingrediente .'</td>';
                $html.= '      <td>' . $key->unidad_medida .'</td>';
                $html.= '      <td>' . $key->precio .'</td>';
                if($key->activo==1){
                   $estado = "ACTIVO"; 
                }else{
                   $estado = "INACTIVO";  
                }   
                $html.= '      <td>' . $estado.'</td>';                

                    $html.= '      <td>';               
  

                if($key->activo==1){                

                    if($this->clssession->accion(11,3)==1){ 
                        $html.= '&nbsp;<a href="javascript:void(0)" onclick="javascript:mod_ingrediente('.$key->id.')">';    
                        
                        $html.= '<img src="'.$modificar.'" style="width:30px; height:30px" alt="Modificar" ';
                        $html.= 'title="Modificar Ingrediente Adicional"></a>';
                    }   

                    if($this->clssession->accion(11,4)==1){ 
                        $html.= '&nbsp;<a href="javascript:void(0)" onclick="javascript:desactivarIngrediente_form('.$key->id.')">';
                        $html.= '<img src="'.$eliminar.'" style="width:30px; height:30px" alt="Desactivar Ingrediente" title="Desactivar Ingrediente"></a>';
                    }   

                }else{
                    if($this->clssession->accion(11,4)==1){ 
                        $html.= '&nbsp;<a href="javascript:void(0)" onclick="javascript:reactivarIngrediente_form('.$key->id.')">';
                        $html.= '<img src="'.$reactivar.'" style="width:30px; height:30px" alt="Reactivar Ingrediente" title="Reactivar Ingrediente"></a>';
                    }   

                }    

                $html.= '       </td>';             
                $html.= '</tr>';
            }
        }  

        $html.= '    </tbody>';
        $html.= '</table>';
        return $html;                   
    }


	

public function ajax_guardar_ingrediente(){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){
	$idEmpresa = 0;
	$idSucursal = 0;

	if($this->session->userdata('administrador')==2){
		$idEmpresa = $this->session->userdata('idEmpresa');
	}

	if($this->session->userdata('administrador')>2){
		$idEmpresa = $this->session->userdata('idEmpresa');
		$idSucursal = $this->session->userdata('idSucursal');
	}

    //obtener nombre el alimento
    $idIngrediente = $this->input->post('id_cbo_ingrediente');
    $rowIngrediente = $this->Mn_ingrediente_mdl->obtIngredienteSB($idIngrediente); 
    $nombre = $rowIngrediente->nombre;
    $idUnidadMedida = $rowIngrediente->id_unidad_medida;

    //guardar alimentos.
    $data = array(
        'id_categoria_menu'=>2,
        'nombre'=>$nombre,
        'nombre_menu'=>$nombre,
        'descripcion'=>$nombre,
        'id_ingrediente'=>$idIngrediente,
        'id_empresa'=>$idEmpresa,
        'id_sucursal'=>$idSucursal,
        'id_create'=>$this->session->userdata('idUsuario'),
    );

    $idAlimento = $this->Mn_alimento_mdl->guardar_alimentoSB($data);


    //Guardar ingrediente del alimento
    $data = array(
       'id_alimento'=>$idAlimento,
       'id_ingrediente'=>$idIngrediente,
       'id_unidad_medida'=>$idUnidadMedida,
       'id_create'=>$this->session->userdata('idUsuario'),
    );
    $this->Mn_alimento_mdl->guardar_detalleIngrediente($data);

    //Guardar presentacion
    $precio = $this->input->post("m_precio");
    $precio = str_replace(".", "", $precio);
    $precio = str_replace(",", ".", $precio);     
 

    $data = array(
       'id_alimento'=>$idAlimento,
       'id_tipo_presentacion'=>1,
       'precio'=>$precio,
       'nro_pieza'=>1,
       'id_unidad_medida'=>1,
       'orden'=>1,
       'id_create'=>$this->session->userdata('idUsuario'),
    );
	$idPresentacion = $this->Mn_alimento_mdl->guardar_add_presentacion($data);

	//Guardar ingrediente por presentacion 

    $cantidad = $this->input->post("m_cantidad");
    $cantidad = str_replace(".", "", $cantidad);
    $cantidad = str_replace(",", ".", $cantidad);     

    $data = array(
    	'id_alimento_presentacion'=>$idPresentacion,
    	'id_ingrediente'=>$idIngrediente,
    	'cantidad'=>$cantidad,
    	'id_create'=>$this->session->userdata('idUsuario'),
    );

    $this->Mn_alimento_mdl->guardarIngredientePresentacion($data);

	    $row = $this->Mn_ingredienteAdicional_mdl->obt_dataTable();
		$html= $this->generarDatatable($row);
		$data = array(
		    "registro"=>$html,
		    "status"=>0);

    echo json_encode($data);
}//fin val session	
}

public function ajax_obt_medida($idIngrediente){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){
   $rowIngrediente = $this->Mn_ingrediente_mdl->obtIngredienteSB($idIngrediente); 
   $data = array(
       'medida'=>$rowIngrediente->sigla
   );
   echo json_encode($data);

}//fin val session
}
	


function ajax_modificar($idAlimento){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){
    $row = $this->Mn_alimento_mdl->verAlimento($idAlimento);
    $nombre = '<input type="text" id="nombre" readonly="true" value="'.$row->nombre.'">'; 

    //obtener informacion presentacion
     $rowPresentacion = $this->Mn_alimento_mdl->obtPresentacion_x_alimento($idAlimento);

     $precio = $rowPresentacion->precio;
     $precio = str_replace(".", ",", $precio);

    $idPresentacion = $rowPresentacion->id;
    $rowInformacionIngrediente = $this->Mn_ingrediente_mdl->obtIngredienteSB($row->id_ingrediente);
    $rowIngrediente = $this->Mn_alimento_mdl->obtIngrediente_x_presentacion($idPresentacion);

    $cantidad = $rowIngrediente->cantidad;
    $cantidad = str_replace(".", ",", $cantidad);

    $data = array(
    	'id'=>$row->id,
    	'nombre'=>$nombre,
    	'id_ingrediente'=>$row->id_ingrediente,
    	'medida'=>$rowInformacionIngrediente->sigla,
        'precio'=>$precio,
        'cantidad'=>$cantidad,

    );


    echo json_encode($data);
}//fin val session
}



function ajax_guardar_mod(){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){
    $idAlimento = $this->input->post('id_mod');

    //Guardar presentacion
    $precio = $this->input->post("m_precio");
    $precio = str_replace(".", "", $precio);
    $precio = str_replace(",", ".", $precio);     

    $data = array(
       'precio'=>$precio,
    );

    $this->Mn_alimento_mdl->guardar_mod_presentacion_adicional($idAlimento, $data);

    $rowDatoIngrediente = $this->Mn_alimento_mdl->obtPresentacion_x_alimento($idAlimento);
    $idPresentacion = $rowDatoIngrediente->id;

    $cantidad = $this->input->post("m_cantidad");
    $cantidad = str_replace(".", "", $cantidad);
    $cantidad = str_replace(",", ".", $cantidad);     
    $data = array(
       'cantidad'=>$cantidad,
    );

    $this->Mn_alimento_mdl->guardar_mod_presentacion_ingrediente($idPresentacion, $data);

    $row = $this->Mn_ingredienteAdicional_mdl->obt_dataTable();
	$html= $this->generarDatatable($row);
	$data = array(
	    "registro"=>$html,
	    "status"=>0);

    echo json_encode($data);

}//fin val session   
}


function ajax_desactivar($idAlimento){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){
	$data = array(
  	    'activo'=>0,
  	    'id_update'=>$this->session->userdata('idUsuario'),
  	    'date_update'=>date('Y-m-d H:i:s')
    );

	$respuesta = $this->Mn_ingredienteAdicional_mdl->desactivar($idAlimento,$data);
    $row = $this->Mn_ingredienteAdicional_mdl->obt_dataTable();
	$html= $this->generarDatatable($row);
	$data = array(
	    "registro"=>$html,
	    "status"=>0);

    echo json_encode($data);
}//fin val session
}

function ajax_reactivar($idAlimento){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){

	$data = array(
  	    'activo'=>1,
  	    'id_update'=>$this->session->userdata('idUsuario'),
  	    'date_update'=>date('Y-m-d H:i:s')
    );

	$respuesta = $this->Mn_ingredienteAdicional_mdl->reactivar($idAlimento,$data);
    $row = $this->Mn_ingredienteAdicional_mdl->obt_dataTable();
	$html= $this->generarDatatable($row);
	$data = array(
	    "registro"=>$html,
	    "status"=>0);

    echo json_encode($data);
}//fin val session
}


} //fin del controlador
