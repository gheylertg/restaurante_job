<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mn_presentacionIngrediente extends CI_Controller {


    public function __construct() {
         parent::__construct();
         $this->load->model(array('Mn_alimento_mdl','Mn_ingrediente_mdl','Mn_presentacionIngrediente_mdl'));
         $this->load->library(array('Clssession')); 
    }



public function act_presentacion_ingrediente($idPresentacion){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){      

    $rowPresentacion = $this->Mn_alimento_mdl->obtModificarPresentacion($idPresentacion);
    $dataSesion = array(
       'idPresentacion'=>$idPresentacion,
       'nob_tipoPresentacion'=>$rowPresentacion->tipo_presentacion
    );
    $this->session->set_userdata($dataSesion);

    $data=array(
        'accion'=>1,
        'modulo'=>1,
        'idPresentacion'=>$idPresentacion,
    );

    $this->load->view('header');
    $this->load->view('menu');
    $this->load->view('menu/presentacionIngrediente/act_presentacionIngrediente',$data);
    $this->load->view('footer/footer', $data);
    $this->load->view('menu/presentacionIngrediente/footer_presentacionIngrediente', $data);
}//fin val session
}


    function ajax_datatablePresentacionIngrediente(){
    //validar variables de session.
    $valSession = $this->clssession->valSession();
    if($valSession==true){      

        $id = $this->session->userdata('idPresentacion');

        $row = $this->Mn_presentacionIngrediente_mdl->obt_dataTablePresentacionIngrediente($id);

        $html= $this->generarDatatablePresentacionIngrediente($row);

        $nobPresentacion = $this->session->userdata('nobAlimento') . " / " . $this->session->userdata('nob_tipoPresentacion');

        $data = array(
            "nobPresentacion"=>$nobPresentacion,
            "registro"=>$html,
        );

        echo json_encode($data);    

    }//fin val session   
    }


function generarDatatablePresentacionIngrediente($row){
    //La variable rol trae los diferentes roles sin estar asociados a los modulos. 
    //obtener la información de la tabla seleccionada
    $modificar = base_url() . "assets/images/modificar02.jpeg";
    $eliminar = base_url() . "assets/images/eliminar.jpeg";
    $reactivar = base_url() . "assets/images/reactivar.jpeg";
    $lectura = base_url() . "assets/images/ver.png";
    $ingrediente = base_url() . "assets/images/ingrediente.jpeg";


    $html = '<table id="basic-datatables" class="display table table-striped table-hover">';
    $html.= '    <thead>';
    $html.= '        <tr class="tr-datatable">';
    $html.= '           <th width="55%">Ingrediente</th>';
    $html.= '           <th width="15%">Cantidad</th>';
    $html.= '           <th width="20%">unidad Medida</th>';
    $html.= '           <th width="10%">Acción</th>';
    $html.= '        </tr>';
    $html.= '    </thead>';
    $html.= '    <tfoot>';
    $html.= '        <tr class="tr-datatable">';
    $html.= '           <th width="55%">Ingrediente</th>';
    $html.= '           <th width="15%">Cantidad</th>';
    $html.= '           <th width="20%">unidad Medida</th>';
    $html.= '           <th width="10%">Acción</th>';
    $html.= '        </tr>';
    $html.= '    </tfoot>';
    $html.= '    <tbody>';
    if($row==false){
        $html.= '      <tr>';
        $html.= '         <td></td>
        <td></td>
        <td></td>
        <td></td>

       ';
                            
        $html.= '      </tr>';
    }else{
        foreach($row as $key){ 
            $html.= '  <tr>';
            $html.= '      <td>' . $key->ingrediente.'</td>';
            $cantidad = number_format ($key->cantidad, 2,'.',',');  
            $html.= '      <td style="text-align:right">' . $cantidad.'</td>'; 
            $html.= '      <td>' . $key->unidad_medida.'</td>';                 

            $html.= '      <td>';               
            //$html.= '<a href="javascript:void(0)" onclick="javascript:modificar_form('.$key->id.')">';


            if($this->clssession->accion(11,4)==1){ 
                $html.='&nbsp;<a href="javascript:void(0)" onclick="javascript:mod_ingrediente('.$key->id.')">';
                $html.= '<img src="'.$modificar.'" style="width:25px; height:25px" alt="Eliminar" ';
                $html.= 'title="Cantidad ingrediente utilizado "></a>';'></a>';
            }    

            $html.= '       </td>';             
            $html.= '</tr>';
        }
    }  

    $html.= '    </tbody>';
    $html.= '</table>';
    return $html;                   
}


public function ajax_modificar_ingrediente($id){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){      
    $row = $this->Mn_presentacionIngrediente_mdl->obtModificarPresentacionIngrediente($id);
    $cantidad = str_replace(".",",",$row->cantidad);
    $data = array(
        'id'=>$row->id,
        'cantidad'=>$cantidad,
    );
    echo json_encode($data);    
}//fin val session
}


function ajax_guardar_mod(){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){      

    $status = 0;
    $id =  $this->input->post('id_mod');
    $cantidad = $this->input->post("m_cantidad");
    $cantidad = str_replace(".", "", $cantidad);
    $cantidad = str_replace(",", ".", $cantidad);  
    $data=array(
        'cantidad'=>$cantidad,
        'id_update'=>$this->session->userdata("idUsuario"),
        'date_update'=>date('Y-m-d H:i:s')  
    );

    $this->Mn_presentacionIngrediente_mdl->guardar_mod($id, $data);


    $id = $this->session->userdata('idPresentacion');

    $row = $this->Mn_presentacionIngrediente_mdl->obt_dataTablePresentacionIngrediente($id);
    $html= $this->generarDatatablePresentacionIngrediente($row);

    $data = array(
        "registro"=>$html,
    );
    echo json_encode($data);    
}//fin val session
}    
   
}  //fin de la clase
