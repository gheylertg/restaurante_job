<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mt_condicionPago extends CI_Controller {

	
	public function __construct() {
         parent::__construct();
         $this->load->model(array('Mt_condicionPago_mdl'));
         $this->load->library(array('Clssession')); 
    }




	public function index()	{
	//validar variables de session.
	$valSession = $this->clssession->valSession();
	if($valSession==true){		

		
        //accion 0 vista sin datatable, accion 1  activar datatable 
		$data=array(
			'accion'=>1
		);
		
	    $this->load->view('header');
		$this->load->view('menu');
		$this->load->view('mantenimiento/condicionPago/act_condicionPago',$data);
		$this->load->view('footer/footer', $data);
		$this->load->view('mantenimiento/condicionPago/footer_condicionPago', $data);
		//$this->load->view('footer/lib_numerica');
	}//fin val session
	}
	
	function ajax_datatable($id){
	//validar variables de session.
	$valSession = $this->clssession->valSession();
	if($valSession==true){		

		//$this->load->model('sg_rolesUsuario_mdl');
		$row = $this->Mt_condicionPago_mdl->obt_dataTable();
		$html= $this->generarDatatable($row);
		$data = array(
		    "registro"=>$html);
        echo json_encode($data);	

	}//fin val session	
	}
	


function ajax_modificar($id){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){		

	//$this->load->model('sg_rolesUsuario_mdl');
    $row = $this->Mt_condicionPago_mdl->obtModificar($id);

    $data = array(
    	'id'=>$row->id,
    	'nombre'=>$row->nombre,
    );
    echo json_encode($data);
}//fin val session
}	


function ajax_guardar_add(){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){		
    $status = 0;
	$nombre = $this->input->post('m_nombre');
    $status = $this->ajax_validar_duplicidad($nombre);
    if($status==0){
    	//$this->load->model('Sg_rolesUsuario_mdl');

		$data = array(
	        'nombre'=>$nombre,
	  	    'id_create'=>$this->session->userdata('idUsuario'),
		);
		$this->Mt_condicionPago_mdl->guardar_add($data);

	    $row = $this->Mt_condicionPago_mdl->obt_dataTable();
		$html= $this->generarDatatable($row);
		$data = array(
		    "registro"=>$html,
		    "status"=>$status);
	}else{
		$data = array(
		    "status"=>1);
	}	

    echo json_encode($data);
}//fin val session
}	





function ajax_guardar_mod(){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){		
    $status = 0;
	$nombre = $this->input->post('m_nombre');
	$nombre_orig = $this->input->post('nombre_orig');

    if(strtoupper($nombre) != strtoupper($nombre_orig)){
	    $status = $this->ajax_validar_duplicidad($nombre);
	}



    if($status==0){
    	//$this->load->model('Es_subModulo_mdl');
		$id = $this->input->post('id_mod');

		$data = array(
	        'nombre'=>$nombre,
	  	    'id_update'=>$this->session->userdata('idUsuario'),
	  	    'date_update'=>date('Y-m-d H:i:s')        
		);
		$respuesta = $this->Mt_condicionPago_mdl->guardar_mod($id,$data);

	    $row = $this->Mt_condicionPago_mdl->obt_dataTable();
		$html= $this->generarDatatable($row);
		$data = array(
		    "registro"=>$html,
		    "status"=>$status);
	}else{
		$data = array(
		    "status"=>1);
	}	

    echo json_encode($data);
}//fin val session	
}
	

function ajax_desactivar($id){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){		
	$data = array(
  	    'activo'=>0,
  	    'id_update'=>$this->session->userdata('idUsuario'),
  	    'date_update'=>date('Y-m-d H:i:s')
    );


	$respuesta = $this->Mt_condicionPago_mdl->desactivar($id,$data);
	$row = $this->Mt_condicionPago_mdl->obt_dataTable();
	$html= $this->generarDatatable($row);
	$data = array(
	    "registro"=>$html);
    echo json_encode($data);
}    
}//fin val session

function ajax_reactivar($id){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){		
	$data = array(
  	    'activo'=>1,
  	    'id_update'=>$this->session->userdata('idUsuario'),
  	    'date_update'=>date('Y-m-d H:i:s')
    );


	$respuesta = $this->Mt_condicionPago_mdl->desactivar($id,$data);
	
	$row = $this->Mt_condicionPago_mdl->obt_dataTable();
	$html= $this->generarDatatable($row);
	$data = array(
	    "registro"=>$html);
    echo json_encode($data);

}//fin val session
}


	
	
	
	
	function generarDatatable($row){
		//obtener la información de la tabla seleccionada
		$modificar = base_url() . "assets/images/modificar02.jpeg";
		$eliminar = base_url() . "assets/images/eliminar.jpeg";
		$reactivar = base_url() . "assets/images/reactivar.jpeg";
		$lectura = base_url() . "assets/images/ver.png";

		
        $html = '<table id="basic-datatables" class="display table table-striped table-hover">';
        $html.= '    <thead>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="90%">Nombre</th>';
        $html.= '           <th width="10%">Acción</th>';
        $html.= '        </tr>';
        $html.= '    </thead>';
        $html.= '    <tfoot>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="90%">Nombre</th>';
        $html.= '           <th width="10%">Acción</th>';
        $html.= '        </tr>';
        $html.= '    </tfoot>';
        $html.= '    <tbody>';
        if($row==false){
			$html.= '      <tr>';
			$html.= '          <td></td><td></td>';
			$html.= '      </tr>';
		}else{
		    foreach($row as $key){ 
				$html.= '  <tr>';
				$html.= '      <td>' . $key->nombre.'</td>';
				$html.= '      <td>';				
				if($key->activo==1){
                    if($this->clssession->accion(5,3)==1){ 
						$html.= '<a href="javascript:void(0)" onclick="javascript:modificar_form('.$key->id.')">';
						$html.= '<img src="'.$modificar.'" style="width:30px; height:30px" alt="Modificar" title="Modificar Condición Pago"></a>';
					}	
					if($this->clssession->accion(5,4)==1){ 
						$html.= '&nbsp;<a href="javascript:void(0)" onclick="javascript:eliminar_form('.$key->id.')">';
						$html.= '<img src="'.$eliminar.'" style="width:30px; height:30px" alt="Desactivar" title="Desactivar Condición Pago"></a>';
					}	
					
				}else{
					if($this->clssession->accion(5,5)==1){ 
						$html.= '<a href="javascript:void(0)" onclick="javascript:reactivar_form('.$key->id.')">';
						$html.= '<img src="'.$reactivar.'" style="width:30px; height:30px" alt="Activar" title="Desactivar Condición Pago"></a>';
					}	
				}	
				$html.= '       </td>';				
				$html.= '</tr>';
			}
		}	
		$html.= '    </tbody>';
		$html.= '</table>';
		return $html;    				
	}


	function ajax_validar_duplicidad($nombre){
		$nro = $this->Mt_condicionPago_mdl->valNombre($nombre);
		$status = 0;
		if($nro>0){
		    $status = 1;	
		}

		return $status;
	}
	

}
