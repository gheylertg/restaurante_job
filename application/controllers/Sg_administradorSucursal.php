<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sg_administradorSucursal extends CI_Controller {

	
	public function __construct() {
         parent::__construct();
         $this->load->model(array('Sg_administradorSucursal_mdl', 'Sg_usuarioEmpresa_mdl', 'Sg_rolModulo_mdl','Cf_sucursal_mdl'));
         $this->load->library(array('Clssession')); 
    }


	public function index(){
    //validar variables de session.
    $valSession = $this->clssession->valSession();
    if($valSession==true){
		$data=array(
			'accion'=>1
		);

	    $this->load->view('header');
		$this->load->view('menu');
		$this->load->view('seguridad/administradorSucursal/act_administradorSucursal',$data);
		$this->load->view('comunes/gifEspera');
		$this->load->view('footer/footer', $data);
		$this->load->view('seguridad/administradorSucursal/footer_administradorSucursal', $data);
		//$this->load->view('footer/lib_numerica');
    }//fin val session		
	}
	
	function ajax_datatable($id){
    //validar variables de session.
    $valSession = $this->clssession->valSession();
    if($valSession==true){

    	$idEmpresa = $this->session->userdata('idEmpresa');
		$row = $this->Sg_administradorSucursal_mdl->obt_dataTable();
		$html= $this->generarDatatable($row);
		$data = array(
			"idEmpresa"=>$idEmpresa,
		    "registro"=>$html);
        echo json_encode($data);	
    }//fin val session
	}	
	
	
	function generarDatatable($row){
		//obtener la información de la tabla seleccionada
		$modificar = base_url() . "assets/images/modificar02.jpeg";
		$eliminar = base_url() . "assets/images/eliminar.jpeg";
		$reactivar = base_url() . "assets/images/reactivar.jpeg";
		$lectura = base_url() . "assets/images/ver.png";
		
        $html = '<table id="basic-datatables" class="display table table-striped table-hover">';
        $html.= '    <thead>';
        $html.= '        <tr class="tr-datatable">';

        $html.= '           <th width="10%">RUN</th>';
        $html.= '           <th width="35%">Nombre, Apellido</th>';
		$html.= '           <th width="35%">Sucursal</th>';

		$html.= '           <th width="20%">Estado</th>';	        
        $html.= '           <th width="10%">Acción</th>';
        $html.= '        </tr>';
        $html.= '    </thead>';
        $html.= '    <tfoot>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="10%">RUN</th>';
        $html.= '           <th width="35%">Nombre, Apellido</th>';
		$html.= '           <th width="35%">Sucursal</th>';

		$html.= '           <th width="20%">Estado</th>';	        
        $html.= '           <th width="10%">Acción</th>';
        $html.= '        </tr>';
        $html.= '    </tfoot>';
        $html.= '    <tbody>';
        if($row==false){
			$html.= '      <tr>';
			$html.= '          <td></td><td></td><td></td><td></td><td></td>';
			$html.= '      </tr>';
		}else{
		    foreach($row as $key){ 
				$html.= '  <tr>';

				$html.= '      <td>' . $key->run.'</td>';
                $nombre = trim($key->nombre) . ", " . trim($key->apellido);
				$html.= '      <td>' . $nombre.'</td>';
				$html.= '      <td>' . $key->sucursal.'</td>';
				$estado = "INACTIVO";
                if($key->activo==1){
                    $estado = "ACTIVO"; 
                }
				$html.= '      <td>' . $estado.'</td>';
				$html.= '      <td>';				
				if($key->activo==1){
                    if($this->clssession->accion(3,4)==1){ 					
						$html.= '&nbsp;<a href="javascript:void(0)" onclick="javascript:eliminar_form('.$key->id.')">';
						$html.= '<img src="'.$eliminar.'" style="width:30px; height:30px" alt="Desactivar" title="Desactivar Administrador General"></a>';
                    }
				}else{
					if($this->clssession->accion(3,5)==1){ 					
						$html.= '<a href="javascript:void(0)" onclick="javascript:reactivar_form('.$key->id.')">';
						$html.= '<img src="'.$reactivar.'" style="width:30px; height:30px" alt="Activar" title="Reactivar Administrador General"></a>';
					}	
				}	
				$html.= '       </td>';				
				$html.= '</tr>';
			}
		}	
		$html.= '    </tbody>';
		$html.= '</table>';
		return $html;    				
	}



function ajax_guardar_add(){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){
    $status = 0;
	$idUsuario = $this->input->post('id_cbo_administradorSucursal');
	$idSucursal = $this->input->post('id_cbo_sucursalAdm');

	$data = array(
	    'id_usuario'=>$idUsuario,
	    'id_rol'=>3,
	    'id_empresa'=>$this->session->userdata('idEmpresa'),
	    'id_sucursal'=>$idSucursal,
	    'id_empresa_create'=>$this->session->userdata('idEmpresa'),
	    'id_sucursal_create'=>$this->session->userdata('idSucursal'),
	    'administrador'=>3,
  	    'id_create'=>$this->session->userdata('idUsuario'),
	);
	$idUsuarioAdministrador = $this->Sg_administradorSucursal_mdl->guardar_add($data);

    //guardar usuario empresa
    $data = array(
       'id_rol'=>3,
       'id_usuario'=>$idUsuario,
       'id_empresa'=>$this->session->userdata('idEmpresa'),
       'id_sucursal'=>$idSucursal,
       'id_empresa_create'=>$this->session->userdata('idEmpresa'),
       'id_sucursal_create'=>$this->session->userdata('idSucursal'),
       'id_create'=>$this->session->userdata('idUsuario')
    );


    $idUsuarioEmpresa = $this->Sg_usuarioEmpresa_mdl->guardar_add_usuarioEmpresa($data);


    //Guardar los modulos del usuario por el rol y la empresa
    //obtener los modulos asociados al rol
    $rowModulo = $this->Sg_rolModulo_mdl->obt_modulo_x_rol(3);
    foreach ($rowModulo as $key) {
        $data = array(
        	'id_usuario_empresa'=>$idUsuarioEmpresa,
        	'id_modulo'=>$key->id_modulo,
        	'id_create'=>$this->session->userdata('idUsuario')
        );

        $id_UEM = $this->Sg_usuarioEmpresa_mdl->guardar_add_usuarioEmpresaModulo($data);             

    	//guardo el modulo y recorro los permisos
    	$rowPermiso = $this->Sg_rolModulo_mdl->obt_permiso_x_modulo($key->id); 

        foreach ($rowPermiso as $key2) {
        	/// guardar los permisos asociados a usuario - empresa - modulo
        	$data = array(
                'id_uem'=>$id_UEM,
                'id_modulo'=>$key2->id_modulo,
                'id_permiso'=>$key2->id_permiso,
                'id_create'=>$this->session->userdata('idUsuario')   
        	); 
            $this->Sg_usuarioEmpresa_mdl->guardar_add_usuarioEmpresaModuloPermiso($data);             
        }
        
        $rowProceso = $this->Sg_rolModulo_mdl->obt_proceso_x_modulo($key->id); 
        foreach($rowProceso as $key3){
			$data = array(
			    'id_uem'=>$id_UEM,
			    'id_modulo'=>$key3->id_modulo,
			    'id_sub_modulo'=>$key3->id_sub_modulo,
			    'id_create'=>$this->session->userdata('idUsuario')   
			);

			$this->Sg_usuarioEmpresa_mdl->guardar_add_usuarioEmpresaModuloProceso($data);             
		}
    }
	$row = $this->Sg_administradorSucursal_mdl->obt_dataTable();
	$html= $this->generarDatatable($row);
	$data = array(
	    "registro"=>$html,
	    "status"=>0
	);
    echo json_encode($data);
}//fin val session	
}



function ajax_desactivar($id){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){
	$data = array(
  	    'activo'=>0,
  	    'id_update'=>$this->session->userdata('idUsuario'),
  	    'date_update'=>date('Y-m-d H:i:s')
    );

	$respuesta = $this->Sg_administradorSucursal_mdl->desactivar($id,$data);
	$row = $this->Sg_administradorSucursal_mdl->obt_dataTable();
	$html= $this->generarDatatable($row);
	$data = array(
	    "registro"=>$html,
	    "status"=>0
	);
    echo json_encode($data);
}//fin val session    
}




function ajax_reactivar($id){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){

	$data = array(
  	    'activo'=>1,
  	    'id_update'=>$this->session->userdata('idUsuario'),
  	    'date_update'=>date('Y-m-d H:i:s')
    );
	$respuesta = $this->Sg_administradorSucursal_mdl->desactivar($id,$data);
	
	$row = $this->Sg_administradorSucursal_mdl->obt_dataTable();
	$html= $this->generarDatatable($row);
	$data = array(
	    "registro"=>$html,
	    "status"=>0
	);
    echo json_encode($data);
}//fin val session
}


	
	
	
	
}
