<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sg_modulo extends CI_Controller {

	
	public function __construct() {
         parent::__construct();
         $this->load->model(array('Sg_modulo_mdl'));
         $this->load->library(array('Clssession')); 
    }


	public function index(){
	//validar variables de session.
    $valSession = $this->clssession->valSession();
    if($valSession==true){	
		$data=array(
			'accion'=>1
		);
  


	    $this->load->view('header');
		$this->load->view('menu');
		$this->load->view('seguridad/modulo/actModulo',$data);
		$this->load->view('footer/footer', $data);
		$this->load->view('seguridad/modulo/footer_modulo', $data);
		$this->load->view('footer/lib_numerica');
    }//Fin val session		
	}
	
	function ajax_datatable($id){
	//validar variables de session.
    $valSession = $this->clssession->valSession();
    if($valSession==true){	
		$row = $this->Sg_modulo_mdl->obtModulo();
		$html= $this->generarDatatable($row);
		$data = array(
		    "registro"=>$html);
        echo json_encode($data);	
    }// fin val session    

	}	
	


function ajax_incluir(){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){	

	$nombre = $this->input->post('nombre');
	$data = array(
	    "nombre"=>$nombre);
    echo json_encode($data);
}//Fin val session


}


function ajax_modificar($id){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){	
    $row = $this->Sg_modulo_mdl->obtModificar($id);

    $data = array(
    	'id'=>$row->id,
    	'nombre'=>$row->nombre,
    	'identificador'=>$row->identificador,

    	'logo'=>$row->icono_fas_fa,
    	'orden'=>$row->orden
    );


    echo json_encode($data);
}//fin val session	
}


function ajax_guardar_add(){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){	

    $status = 0;
	$nombre = $this->input->post('m_nombre');
    $status = $this->ajax_validar_duplicidad($nombre);
    if($status==0){
    	$orden = $this->input->post('m_orden');
		$logo = $this->input->post('m_logo');
		$url = $this->input->post('m_url');
	    $identificador = $this->input->post('m_identificador');
		$data = array(
	        'nombre'=>$nombre,
            'identificador'=>$identificador,
	        'url_modulo'=>$url,
	        'orden'=>$orden,
	        'icono_fas_fa'=>$logo,
	  	    'id_create'=>$this->session->userdata('idUsuario'),
		);
		$respuesta = $this->Sg_modulo_mdl->guardar_add($data);


	    $row = $this->Sg_modulo_mdl->obtModulo();
		$html= $this->generarDatatable($row);
		$data = array(
		    "registro"=>$html,
		    "status"=>$status);
	}else{
		$data = array(
		    "status"=>1);
	}	

    echo json_encode($data);
}//fin val session	
}





function ajax_guardar_mod(){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){	
    $status = 0;
	$nombre = $this->input->post('m_nombre');
	$nombre_orig = $this->input->post('nombre_orig');

    if(strtoupper($nombre) != strtoupper($nombre_orig)){
	    $status = $this->ajax_validar_duplicidad($nombre);
	}



    if($status==0){
    	$identificador = $this->input->post('m_identificador');
    	$orden = $this->input->post('m_orden');
		$logo = $this->input->post('m_logo');
		$id = $this->input->post('id_mod');

		$data = array(
	        'nombre'=>$nombre,
	        'identificador'=>$identificador,
	        'orden'=>$orden,
	        'icono_fas_fa'=>$logo,
	  	    'id_update'=>$this->session->userdata('idUsuario'),
	  	    'date_update'=>date('Y-m-d H:i:s')        
		);
		$respuesta = $this->Sg_modulo_mdl->guardar_mod($id,$data);


	    $row = $this->Sg_modulo_mdl->obtModulo();
		$html= $this->generarDatatable($row);
		$data = array(
		    "registro"=>$html,
		    "status"=>$status);
	}else{
		$data = array(
		    "status"=>1);
	}	

    echo json_encode($data);
}//fin val session	
}
	

function ajax_desactivar($id){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){	
	$data = array(
  	    'activo'=>0,
  	    'id_update'=>$this->session->userdata('idUsuario'),
  	    'date_update'=>date('Y-m-d H:i:s')
    );


	$respuesta = $this->Sg_modulo_mdl->desactivar($id,$data);
    $row = $this->Sg_modulo_mdl->obtModulo();
	$html= $this->generarDatatable($row);
	$data = array(
	    "registro"=>$html);
    echo json_encode($data);
}//fin val session    
}


function ajax_reactivar($id){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){	
	$data = array(
  	    'activo'=>1,
  	    'id_update'=>$this->session->userdata('idUsuario'),
  	    'date_update'=>date('Y-m-d H:i:s')
    );


	$respuesta = $this->Sg_modulo_mdl->desactivar($id,$data);
    $row = $this->Sg_modulo_mdl->obtModulo();
	$html= $this->generarDatatable($row);
	$data = array(
	    "registro"=>$html);
    echo json_encode($data);

}//Fin val session
}

	
	
	
	
	function generarDatatable($row){
		//obtener la información de la tabla seleccionada
		$modificar = base_url() . "assets/images/modificar02.jpeg";
		$eliminar = base_url() . "assets/images/eliminar.jpeg";
		$reactivar = base_url() . "assets/images/reactivar.jpeg";
		$lectura = base_url() . "assets/images/ver.png";
		
        $html = '<table id="basic-datatables" class="display table table-striped table-hover">';
        $html.= '    <thead>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="65%">Nombre</th>';
        $html.= '           <th width="10%">Identificador</th>';
		$html.= '           <th width="10%">Orden</th>';	        
        $html.= '           <th width="15%">Acción</th>';
        $html.= '        </tr>';
        $html.= '    </thead>';
        $html.= '    <tfoot>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="65%">Nombre</th>';
        $html.= '           <th width="10%">Identificador</th>';
		$html.= '           <th width="10%">Orden</th>';	        
        $html.= '           <th width="15%">Acción</th>';
        $html.= '        </tr>';
        $html.= '    </tfoot>';
        $html.= '    <tbody>';
        if($row==false){
			$html.= '      <tr>';
			$html.= '          <td></td><td></td><td></td><td></td>';
			$html.= '      </tr>';
		}else{
		    foreach($row as $key){ 
				$html.= '  <tr>';
				$html.= '      <td>' . $key->nombre.'</td>';
				$html.= '      <td>' . $key->identificador.'</td>';				
				$html.= '      <td style="text-align:center">' . $key->orden.'</td>';
				$html.= '      <td>';				
				if($key->activo==1){



                    if($this->clssession->accion(4,3)==1){ 
					    $html.= '<a href="javascript:void(0)" onclick="javascript:modificar_form('.$key->id.')">';
						$html.= '<img src="'.$modificar.'" style="width:30px; height:30px" alt="Modificar" title="Modificar Módulo"></a>';
					}	

                    if($this->clssession->accion(4,4)==1){ 
						$html.= '&nbsp;<a href="javascript:void(0)" onclick="javascript:eliminar_form('.$key->id.')">';
						$html.= '<img src="'.$eliminar.'" style="width:30px; height:30px" alt="Desactivar" title="Desactivar Módulo"></a>';
					}	


				}else{
					if($this->clssession->accion(4,5)==1){ 
						$html.= '<a href="javascript:void(0)" onclick="javascript:reactivar_form('.$key->id.')">';
						$html.= '<img src="'.$reactivar.'" style="width:30px; height:30px" alt="Activar" title="Reactivar Módulo"></a>';
					}	
				}	
				$html.= '       </td>';				
				$html.= '</tr>';
			}
		}	
		$html.= '    </tbody>';
		$html.= '</table>';
		return $html;    				
	}



	function ajax_validar_duplicidad($nombre){

		$nro = $this->Sg_modulo_mdl->valNombre($nombre);
		$status = 0;
		if($nro>0){
		    $status = 1;	
		}

		return $status;
	}
	

}
