<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sg_rolModulo extends CI_Controller {

	

	public function __construct() {
         parent::__construct();
         $this->load->model(array('Sg_rolesUsuario_mdl', 'Sg_rolModulo_mdl', 'Sg_permiso_mdl','Es_subModulo_mdl', 'Sg_modulo_mdl'));
         $this->load->library(array('Clssession')); 
    }


	public function index(){
    //validar variables de session.
    $valSession = $this->clssession->valSession();
    if($valSession==true){  
		
        //accion 0 vista sin datatable, accion 1  activar datatable 
		$data=array(
			'accion'=>1,
			'modulo'=>1,
		);

	    $this->load->view('header');
		$this->load->view('menu');
		$this->load->view('seguridad/rolModulo/act_rolModulo',$data);
		$this->load->view('footer/footer', $data);
		$this->load->view('seguridad/rolModulo/footer_rolModulo', $data);
	}//Fin val session
    }


	function ajax_datatable($id){
    //validar variables de session.
    $valSession = $this->clssession->valSession();
    if($valSession==true){  

		//$this->load->model('sg_rolesUsuario_mdl');
		$row = $this->Sg_rolesUsuario_mdl->obt_dataTable();
		$html= $this->generarDatatable($row);
		$data = array(
		    "registro"=>$html);
        echo json_encode($data);	

	}//Fin val session
    }	

	function generarDatatable($row){

        //La variable rol trae los diferentes roles sin estar asociados a los modulos. 

		//obtener la información de la tabla seleccionada
		$modificar = base_url() . "assets/images/modificar02.jpeg";
		$eliminar = base_url() . "assets/images/eliminar.jpeg";
		$reactivar = base_url() . "assets/images/reactivar.jpeg";
        $lectura = base_url() . "assets/images/ver.png";



        $html = '<table id="basic-datatables" class="display table table-striped table-hover">';
        $html.= '    <thead>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="30%">Rol</th>';
		$html.= '           <th width="60%">Modulo(s)</th>';        

        $html.= '           <th width="10%">Acción</th>';
        $html.= '        </tr>';
        $html.= '    </thead>';
        $html.= '    <tfoot>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="30%">Rol</th>';
		$html.= '           <th width="60%">Modulo(s)</th>';                
        $html.= '           <th width="10%">Acción</th>';
        $html.= '        </tr>';
        $html.= '    </tfoot>';
        $html.= '    <tbody>';
        if($row==false){
			$html.= '      <tr>';
			$html.= '          <td colspan="3">La Tabla no posee registros asociados</td>';
			$html.= '      </tr>';
		}else{
		    foreach($row as $key){ 
				$html.= '  <tr>';
				$html.= '      <td>' . $key->nombre.'</td>';

                //Configurar los modulos por roles
				$rowModulo = $this->Sg_rolModulo_mdl->obt_modulo_x_rol($key->id);
				$modulo = "";
				$swModulo=0;

                

                foreach($rowModulo as $key2){

                    if($modulo == ""){
                        $modulo = trim($key2->modulo); 
                        $swModulo=1;
                    }else{
                        $modulo = $modulo . " -- " . trim($key2->modulo); 
                    }	
                }  
                if($swModulo==0){
                	$modulo = "SIN MODULO ASOCIADO";
                }


                $html.= '      <td>' . $modulo.'</td>';
           		$html.= '      <td>';				
				//$html.= '<a href="javascript:void(0)" onclick="javascript:modificar_form('.$key->id.')">';

                if($this->clssession->accion(3,3)==1){ 
                    $html.= '<a href="javascript:void(0)" onclick="javascript:rol_moduloAct('.$key->id.')">';
                    $html.= '<img src="'.$modificar.'" style="width:30px; height:30px" alt="Actualizar" title="Actualizar el Rol"></a>';
                }    

                /*if($this->clssession->accion(3,3)==1){ 
				    $html.= '<a href="' . base_url('rolModuloActualizar').'/'.$key->id. '">';
				    $html.= '<img src="'.$modificar.'" style="width:30px; height:30px" alt="Actualizar"></a>';
                }
                */    

				$html.= '       </td>';				
				$html.= '</tr>';
			}
		}	
		$html.= '    </tbody>';
		$html.= '</table>';
		return $html;    				
	}



//***************************acatualizar modulos del rol

public function ajax_actualizarModulo($idRol){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){  
    $dataSession=array(
        'idRol'=>$idRol
    );
    $this->session->set_userdata($dataSession);
    $data = array(
        'status'=>0
    );
    echo json_encode($data);
}
}    



public function actualizarModulo(){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){  

    //accion 0 vista sin datatable, accion 1  activar datatable 
   $idRol = $this->session->userdata('idRol'); 
   $rowRol = $this->Sg_rolesUsuario_mdl->obtModificar($idRol);

   $sw_permisoRol=0;
   if($rowRol->id_empresa==$this->session->userdata('idEmpresa') && 
       $rowRol->id_sucursal==$this->session->userdata('idSucursal')){
       $sw_permisoRol=1;
   }

   $dataSession = array(
       'nobRol'=>$rowRol->nombre,
       'sw_permisoRol'=>$sw_permisoRol
   );

   $this->session->set_userdata($dataSession);
    $data=array(
        'accion'=>1,
        'modulo'=>2,
        'idRol'=>$idRol,
        'sw_permisoRol'=>$sw_permisoRol

    );
    $this->load->view('header');
    $this->load->view('menu');
    $this->load->view('seguridad/rolModulo/act_moduloRol',$data);
    $this->load->view('footer/footer', $data);
    $this->load->view('seguridad/rolModulo/footer_rolModulo', $data);
}//Fin val session
}






/*public function actualizarModulo($idRol){
//validar variables de session.
$valSession = $this->clssession->valSession();


if($valSession==true){  
    $dataSession=array(
        'idRol'=>$idRol
    );

    //accion 0 vista sin datatable, accion 1  activar datatable 

   $rowRol = $this->Sg_rolesUsuario_mdl->obtModificar($idRol);
   $dataSession = array(
       'idRol'=>$idRol,
       'nobRol'=>$rowRol->nombre
   );


   $this->session->set_userdata($dataSession);
	$data=array(
		'accion'=>1,
		'modulo'=>2,
		'idRol'=>$idRol,
	);
    $this->load->view('header');
	$this->load->view('menu');
	$this->load->view('seguridad/rolModulo/act_moduloRol',$data);
	$this->load->view('footer/footer', $data);
	$this->load->view('seguridad/rolModulo/footer_rolModulo', $data);
}//Fin val session
}
*/

public function ajax_datatable_rolModulo($idRol){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){  

	//$this->load->model('sg_rolesUsuario_mdl');

	$row = $this->Sg_rolModulo_mdl->obt_modulo_x_rol($idRol);



	//$rowRol = $this->Sg_rolesUsuario_mdl->obtModificar($idRol);
	$nobRol = $this->session->userdata('nobRol');
		$html= $this->generarDatatable_rolModulo($row, $idRol);
		$data = array(
			"idRol"=>$idRol,
			"nobRol"=>$nobRol,
		    "registro"=>$html,
        );
            
        echo json_encode($data);	
}//Fin val session	
}


    function generarDatatable_rolModulo($row, $idRol){

        //La variable rol trae los diferentes roles sin estar asociados a los modulos. 

        //obtener la información de la tabla seleccionada
        $modificar = base_url() . "assets/images/modificar02.jpeg";

        $eliminar = base_url() . "assets/images/eliminar.jpeg";
        $reactivar = base_url() . "assets/images/reactivar.jpeg";
        $lectura = base_url() . "assets/images/ver.png";
        $proceso = base_url() . "assets/images/proceso.jpeg";

        $html = '<table id="basic-datatables" class="display table table-striped table-hover">';
        $html.= '    <thead>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="40%">Modulo</th>';
        $html.= '           <th width="45%">Permiso(s)</th>';        
        $html.= '           <th width="15%">Acción</th>';
        $html.= '        </tr>';
        $html.= '    </thead>';
        $html.= '    <tfoot>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="40%">Modulo</th>';
        $html.= '           <th width="45%">Permiso(s)</th>';        
        $html.= '           <th width="15%">Acción</th>';
        $html.= '        </tr>';
        $html.= '    </tfoot>';
        $html.= '    <tbody>';
        if($row==false){
            $html.= '      <tr>';
            $html.= '          <td></td><td></td><td></td>';
            $html.= '      </tr>';
        }else{

            foreach($row as $key){ 
                $html.= '  <tr>';
                $html.= '      <td>' . $key->modulo.'</td>';

                //Configurar los modulos por roles
               
                $rowPermiso = $this->Sg_rolModulo_mdl->obt_permiso_x_modulo($key->id);
                
                $permiso = "";
                $swPermiso=0;

                foreach($rowPermiso as $key2){

                    if($permiso == ""){
                        $permiso = trim($key2->permiso); 
                        $swPermiso=1;
                    }else{
                        $permiso = $permiso . " -- " . trim($key2->permiso); 
                    }   
                }  
                if($swPermiso==0){
                    $permiso = "SIN PERMISO ASOCIADO";
                }


                $html.= '      <td>' . $permiso.'</td>';

                $html.= '      <td>';               

                
                if($this->session->userdata('sw_permisoRol')==1){

                    if($this->clssession->accion(3,3)==1){  
                        $html.= '<a href="javascript:void(0)" onclick="javascript:modificar_form('.$key->id.')">';
                        $html.= '<img src="'.$modificar.'" style="width:30px; height:30px" alt="Modificar" title="Modificar Modulo del Rol"></a>';
                    }    

                    if($this->clssession->accion(3,4)==1){                  
                        $html.= '&nbsp;<a href="javascript:void(0)" onclick="javascript:eliminar_form('.$key->id.')">';
                        $html.= '<img src="'.$eliminar.'" style="width:30px; height:30px" alt="Desactivar" title="Eliminar Modulo del Rol"></a>';
                    }    

                }

                if($this->clssession->accion(3,3)==1){  
                    $html.= '&nbsp;<a href="javascript:void(0)" onclick="javascript:moduloProceso_form('.$key->id.')">';
                    $html.= '<img src="'.$proceso.'" style="width:30px; height:30px" alt="Sub Modelo Asociado" title="Asociar Submódulo"></a>';
                }     
                $html.= '       </td>';     
                $html.= '</tr>';
            }
        }   
        $html.= '    </tbody>';
        $html.= '</table>';
        return $html;                   
    }










	public function ajax_permiso_add(){
    //validar variables de session.
    $valSession = $this->clssession->valSession();
    if($valSession==true){  

        $row = $this->Sg_permiso_mdl->obt_permiso();
        $html = "";
        $i=0;
        $cadenaArreglo="";
        $columna = 0;
        foreach ($row as $key) {
        	$i+=1;
        	$columna+=1 ;

        	$permiso_new = "permiso" .$i;

        	if($cadenaArreglo==""){
        		$cadenaArreglo=$cadenaArreglo . $key->id;
        	}else{
        		$cadenaArreglo= $cadenaArreglo . "**" . $key->id;
        	}	
            if($columna==1){
                $html.='<div class="row">';                
            } 
                $html.='<div class="col-md-3">';                

			$html.='<div class="custom-control custom-checkbox">';        	
        	$html.='<input type="checkbox" class="custom-control-input" id="'.$permiso_new.'" name="permiso'.$i.'" ';
        	$html.='readonly="false" enabled="false" checked>';
	    	$html.='<label class="custom-control-label" for="'.$permiso_new.'">'.$key->nombre.'</label>';
	    	$html.= "</div>";
	    	$html.= "</div>"; //fin del col

	    	if($columna==4){
               $html.= "</div>"; //fin del row
               $columna=0;

	    	}
        }
		$data = array(
		    "registro"=>$html,
		    "nroPermiso"=>$i,
		    'arregloPermiso'=>$cadenaArreglo
		);

        echo json_encode($data);	
    }//Fin val session
    }





function ajax_modificarPermiso($id_rolModulo){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){  

	//$this->load->model('sg_rolesUsuario_mdl');
    $row = $this->Sg_rolModulo_mdl->obtModificar_rolModulo($id_rolModulo);
    $nombreModulo = $row->nombre;

    $dataSession = array(
       'idRolModulo'=>$id_rolModulo,
       'nobModulo'=>$nombreModulo,
    );

    $this->session->set_userdata($dataSession);


    $rowPermiso = $this->Sg_permiso_mdl->obt_permiso();
    $rowPermisoModulo = $this->Sg_rolModulo_mdl->obt_permiso_modulo($id_rolModulo);


    $html = "";
    $i=0;
    $cadenaArreglo="";
    $columna = 0;
        foreach ($rowPermiso as $key) {
        	$permiso = 0;
        	foreach ($rowPermisoModulo as $key2) {
                if($key->id==$key2->id_permiso){
	                $permiso = 1;
	                break; 
				}
        	}	

 
        	$i+=1;
        	$columna+=1 ;
        	$permiso_new = "permiso" .$i;

        	$check = "";
        	if($permiso==1){
                $check = "checked"; 
        	}

        	if($cadenaArreglo==""){
        		$cadenaArreglo=$cadenaArreglo . $key->id;
        	}else{
        		$cadenaArreglo= $cadenaArreglo . "**" . $key->id;
        	}	
            if($columna==1){
                $html.='<div class="row">';                
            } 
                $html.='<div class="col-md-3">';                

			$html.='<div class="custom-control custom-checkbox">';        	
        	$html.='<input type="checkbox" class="custom-control-input" id="'.$permiso_new.'" name="permiso'.$i.'" readonly="false" enabled="false" ' .$check. '>';
	    	$html.='<label class="custom-control-label" for="'.$permiso_new.'">'.$key->nombre.'</label>';
	    	$html.= "</div>";
	    	$html.= "</div>"; //fin del col

	    	if($columna==4){
               $html.= "</div>"; //fin del row
               $columna=0;

	    	}
        }

		$data = array(
		    "registro"=>$html,
		    "nroPermiso"=>$i,
		    "arregloPermiso"=>$cadenaArreglo,
		    "nombreModulo"=>$nombreModulo,
		    "id_rolModulo"=>$id_rolModulo,
		);

        echo json_encode($data);	
}//fin val session	
}


function ajax_guardar_add(){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){  


    $status = 0;
    
    $arregloPermiso = $this->input->post('arregloPermiso');
    $arreglo = explode( '**', $arregloPermiso);
    $nroPermiso     = $this->input->post('nroPermiso');

    $idRol   = $this->input->post('idRol');
    $idModulo   = $this->input->post('id_cbo_rolModulo');

    //guardar registro 
    $data1 = array(
       'id_rol'=>$idRol,
       'id_modulo'=>$idModulo,
       'id_create'=>$this->session->userdata('idUsuario')
    );

    $idRolModulo = $this->Sg_rolModulo_mdl->guardar_add($data1);


     ///guardar submodulo
     //obtener submodulo asociado al modulo.
    $rowSubModulo = $this->Es_subModulo_mdl->obtSubModulo($idModulo);

    foreach ($rowSubModulo as $key) {
        $dataSub= array(
            'id_rol_modulo'=>$idRolModulo,
            'id_modulo'=>$idModulo,
            'id_sub_modulo'=>$key->id,
            'id_create'=>$this->session->userdata('idUsuario'),
        );

        $this->Sg_rolModulo_mdl->add_subModulo($dataSub);   
    }

    //Guardar permiso del modulo
    $i = 0;
     
    while($i < $nroPermiso){
    	$i+=1;
    	$check  = "permiso" . $i;
    	$j=0;

    	if($this->input->post("$check")){
    		 $j=$i - 1;
    		 $idPermiso = $arreglo[$j];

    		 $data = array(
                'id_rol_modulo'=>$idRolModulo,
                'id_permiso'=>$idPermiso,
                'id_modulo'=>$idModulo,
                'id_create' =>$this->session->userdata('idUsuario') 
    		 );
             $this->Sg_rolModulo_mdl->guardar_add_permiso($data);
    	}
    }


    $row = $this->Sg_rolModulo_mdl->obt_modulo_x_rol($idRol);
	$html= $this->generarDatatable_rolModulo($row, $idRol);


	$data = array(
	   "registro"=>$html,
	   "status"=>$status

    );
    echo json_encode($data);
}//Fin val session	
}



	

function ajax_guardar_mod(){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){  

    $status = 0;
    $idRol = $this->input->post('idRol');
    $id_rolModulo =  $this->input->post('id_mod');
    
    
    //Obtener modulo
    $rowModulo = $this->Sg_rolModulo_mdl->obtModificar($id_rolModulo);
    $idModulo = $rowModulo->id_modulo;



    $arregloPermiso = $this->input->post('arregloPermiso');
    $arreglo = explode( '**', $arregloPermiso);

    $nroPermiso     = $this->input->post('nroPermiso');
    //Guardar permiso del modulo
    $i = 0;

    $rowPermiso = $this->Sg_rolModulo_mdl->obt_permiso_modulo($id_rolModulo);

    while($i < $nroPermiso){
    	$i+=1;
    	$check  = "permiso" . $i;
        $j = $i-1; 
        $idPermiso = $arreglo[$j]; 
        $swActualizar = 0;  
       	if($this->input->post("$check")){
            foreach ($rowPermiso as $key) {
            	if($key->id_permiso==$idPermiso){
                    $swActualizar = 1;     
                    //$idModulo=$key->id_modulo;
                    break;
            	}
            }

            if($swActualizar == 0){
	    		$data = array(
	                'id_rol_modulo'=>$id_rolModulo,
	                'id_permiso'=>$idPermiso,
	                'id_create' =>$this->session->userdata('idUsuario'),
                    'id_modulo' =>$idModulo,
	    		);
                $this->Sg_rolModulo_mdl->guardar_add_permiso($data);            
            }
        }else{
            foreach ($rowPermiso as $key) {
            	if($key->id_permiso==$idPermiso){
                    $swActualizar = 1;     
                    break;
            	}
            }


            if($swActualizar == 1){
                $this->Sg_rolModulo_mdl->eliminar_permiso($idPermiso, $id_rolModulo);            
            }
        }    
    } //find del while  


    $row = $this->Sg_rolModulo_mdl->obt_modulo_x_rol($idRol);
    $html= $this->generarDatatable_rolModulo($row, $idRol);


	$data = array(
	    "registro"=>$html,
	    "status"=>$status
    );
    echo json_encode($data);
}//Fin val session	
}
	


function ajax_desactivar($id_rolModulo){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){  
	$this->Sg_rolModulo_mdl->eliminar($id_rolModulo);
    $row = $this->Sg_rolModulo_mdl->obt_modulo_x_rol($this->session->userdata('idRol'));
    $html= $this->generarDatatable_rolModulo($row, $this->session->userdata('idRol'));

	//$row = $this->Sg_rolesUsuario_mdl->obt_dataTable();
	//$html= $this->generarDatatable($row);
	$data = array(
	    "registro"=>$html);
    echo json_encode($data);
}//Fin val session    
}





///////////////////////////////////////////////////////////////////
// //////////////// proceso del modulo rol ///////////////////////
//////////////////////////////////////////////////////////////////

public function ajax_actualizarProceso($idRolModulo){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){  
    $dataSession=array(
        'idRolModulo'=>$idRolModulo
    );
    $this->session->set_userdata($dataSession);
    $data = array(
        'status'=>0
    );
    echo json_encode($data);
}
} 

function actualizarProceso(){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){  
    
    $idModuloRol = $this->session->userdata('idRolModulo');
    //Obtener el nombre del modulo
    $rowModulo = $this->Sg_rolModulo_mdl->obtModificar_rolModulo($idModuloRol);

    $dataSession =array(
        'idModuloRol'=>$idModuloRol,
        'nobModulo'=>$rowModulo->nombre,
        'idModulo'=>$rowModulo->id,
    );
    $this->session->set_userdata($dataSession);

    $data=array(
        'accion'=>1,
        'modulo'=>3,
        'idModuloRol'=>$idModuloRol,
    );

    $this->load->view('header');
    $this->load->view('menu');
    $this->load->view('seguridad/rolModulo/act_ProcesoModuloRol',$data);
    
    //$this->load->view('seguridad/usuarioEmpresa/footer', $data);
    $this->load->view('footer/footer', $data);
    $this->load->view('seguridad/rolModulo/footer_rolModulo', $data);
}//Fin val session
}



function ajax_datatable_proceso($idRolModulo){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){  
    $row = $this->Sg_rolModulo_mdl->obt_subModulo_x_modulo($idRolModulo);
    $html= $this->generarDatatable_rolProceso($row, $idRolModulo);
    $data = array(
        "idRolModulo"=>$idRolModulo,
        "nobModulo"=>$this->session->userdata('nobModulo'),
        "idRol"=>$this->session->userdata('idRol'),
        "registro"=>$html);
    echo json_encode($data);    
}//Fin val session
}


function generarDatatable_rolProceso($row, $idRolModulo){
        //La variable rol trae los diferentes roles sin estar asociados a los modulos. 
        ///obtenet los submodulo completos asociado al modelo
        $rowProceso = $this->Es_subModulo_mdl->obtSubModulo($this->session->userdata('idModulo'));
        //obtener la información de la tabla seleccionada
        $modificar = base_url() . "assets/images/modificar02.jpeg";
        $incluir = base_url() . "assets/images/nuevo.png";
        $eliminar = base_url() . "assets/images/eliminar.jpeg";
        $reactivar = base_url() . "assets/images/reactivar.jpeg";
        $lectura = base_url() . "assets/images/ver.png";

        $html = '<table id="basic-datatables" class="display table table-striped table-hover">';
        $html.= '    <thead>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="70%">Sub Modulo</th>';
        $html.= '           <th width="15%">Asociado</th>';
        $html.= '           <th width="15%">Acción</th>';
        $html.= '        </tr>';
        $html.= '    </thead>';
        $html.= '    <tfoot>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="70%">Sub Modulo</th>';
        $html.= '           <th width="15%">Asociado</th>';
        $html.= '           <th width="15%">Acción</th>';
        $html.= '        </tr>';
        $html.= '    </tfoot>';
        $html.= '    <tbody>';
        if($rowProceso==false){
            $html.= '      <tr>';
            $html.= '          <td></td><td></td><td></td>';
            $html.= '      </tr>';
        }else{
            foreach($rowProceso as $key){ 
                $html.= '  <tr>';
                $html.= '      <td>' . $key->nombre.'</td>';
                $sw = 0;
                $asoaciado = "NO ASOCIADO";
                $idModuloProceso = 0;
                foreach($row as $key2){
                    if($key->id == $key2->id){
                         $asoaciado = "ASOCIADO";
                         $idModuloProceso = $key2->id_modulo_proceso;
                         $sw = 1;
                         break;
                    }
                } 

                if ($sw == 1){
                    $html.= '      <td style="background-color:#0000ff; color:white">';               
                }else{    
                    $html.= '      <td>';
                }    
                $html.= $asoaciado;

                $html.= '       </td>';         

                $html.= '      <td>';               

                if($this->session->userdata('sw_permisoRol')==1){

                    if($sw==0){
                        if($this->clssession->accion(3,3)==1){                  
                            $html.= '&nbsp;<a href="javascript:void(0)" onclick="javascript:asociar_form('.$key->id.')">';
                            $html.= '<img src="'.$incluir.'" style="width:30px; height:30px" alt="Asociar"></a>';
                        }    
                    }


                    if($sw==1){  
                        if($this->clssession->accion(3,4)==1){                  
                            $html.= '&nbsp;<a href="javascript:void(0)" onclick="javascript:eliminarProceso_form('.$idModuloProceso.')">';
                            $html.= '<img src="'.$eliminar.'" style="width:30px; height:30px" alt="Eliminar"></a>';
                        }
                    }    
                 }   

                $html.= '       </td>';     




                $html.= '</tr>';
            }
        }   
        $html.= '    </tbody>';
        $html.= '</table>';
        return $html;                   
    }


function ajax_desactivarProceso($id){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){  

   $this->Sg_rolModulo_mdl->eliminarProceso($id);

   $idRolModulo = $this->session->userdata('idModuloRol');   
   $row = $this->Sg_rolModulo_mdl->obt_subModulo_x_modulo($idRolModulo);
   $html= $this->generarDatatable_rolProceso($row, $idRolModulo);
    $data = array(
        "registro"=>$html);
    echo json_encode($data);    
}//Fin val session 
}


function ajax_addProceso($id_subModulo){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){  

    $data = array(
        'id_rol_modulo'=>$this->session->userdata('idModuloRol'),
        'id_modulo'=>$this->session->userdata('idModulo'),
        'id_sub_modulo'=>$id_subModulo,
        'id_create'=>$this->session->userdata('idUsuario'),
    );
    $this->Sg_rolModulo_mdl->asociarProceso($data); 
    $row = $this->Sg_rolModulo_mdl->obt_subModulo_x_modulo($this->session->userdata('idModuloRol'));
    $html= $this->generarDatatable_rolProceso($row, $this->session->userdata('idModuloRol'));
    $data = array(
        "registro"=>$html);
    echo json_encode($data);    

}//Fin val session   
}





} //final del controller
