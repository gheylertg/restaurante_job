<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sg_usuario extends CI_Controller {

	
	public function __construct() {
         parent::__construct();
         $this->load->model(array('Sg_usuario_mdl'));
         $this->load->library(array('Clssession')); 
    }


	public function index($opcion=0){
	//Validar session usuario
	$valSession = $this->clssession->valSession();
	if($valSession==true){	



		
        //accion 0 vista sin datatable, accion 1  activar datatable 
		$data=array(
			'accion'=>1,
			'opcion'=>$opcion
		);

	    $this->load->view('header');
		$this->load->view('menu');
		$this->load->view('seguridad/usuario/act_usuario',$data);
		$this->load->view('footer/footer', $data);
		$this->load->view('seguridad/usuario/footer_usuario', $data);
		$this->load->view('footer/lib_numerica');
		
	}//fin val session
    }
	
	function ajax_datatable(){
	//Validar session usuario
	$valSession = $this->clssession->valSession();
	if($valSession==true){	

		//$this->load->model('sg_usuario_mdl');
		$row = $this->Sg_usuario_mdl->obt_dataTable();
		$html= $this->generarDatatable($row);
		$data = array(
		    "registro"=>$html);
        echo json_encode($data);	
	}//fin val session	
    }
	


function ajax_modificar($id){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){	
    $row = $this->Sg_usuario_mdl->obtModificar($id);

    $data = array(
    	'id'=>$row->id,
    	'run'=>$row->run,
    	'nombre'=>$row->nombre,
    	'apellido'=>$row->apellido,    	
    	'email'=>$row->email,    	
    	'sexo'=>$row->sexo,    	
    );
    echo json_encode($data);
}//fin val session	
}


function ajax_ver($id ){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){	
	
	
    $row = $this->Sg_usuario_mdl->obtModificar($id);
    if($row->foto==null){
	   	if($row->sexo == "F"){
    		$foto = "mujer_sinFoto.jpeg";
	   	}else{
        	$foto = "hombre_sinFoto.jpeg";
    	}
    	$foto = base_url()."assets/images/".$foto;
    }else{
        $foto = $row->foto;
        $foto = base_url()."assets/foto/".$foto;
    }
    
    $nombreApellido = trim($row->nombre) . ", " .trim($row->nombre);
    
    $html = '<img src="'.$foto.'" style="width:200px; height:240px" alt="Foto">';


    $data = array(
    	'id'=>$row->id,
        'run'=>$row->run,
    	'nombre'=>$row->nombre,
    	'apellido'=>$row->apellido,    	
    	'nombreApellido'=>$nombreApellido,
    	'email'=>$row->email,    	
    	'sexo'=>$row->sexo,    	
    	'imagen'=>$html
    );
    echo json_encode($data);
}//fin val session
}

function ajax_guardar_add(){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){	
    $status = 0;
	$run = $this->input->post('m_run');
    $foto = $_FILES['m_foto']['name'];
    $nombreFoto="";

    $opcion=0;
    if(!empty($foto)){
	    $nombreFoto = $this->session->userdata('idEmpresa')."-".$this->session->userdata('idsucursal')."-";
	    $nombreFoto.= $nombreFoto . $run . "-" .$foto;

	    $config['upload_path']          = './assets/foto/'; // Carpeta a subr
	    $config['allowed_types']        = 'gif|jpg|png|jpeg'; // Tipos permitidos
	    $config['max_size']             = 2000; // Max 2Mb
	    $config['max_width']            = 0; // Sin límite de width
	    $config['max_height']           = 0; // Sin límite de height
	    $config['file_name'] = $nombreFoto; // Nombre de la imagen
	    $this->load->library('upload', $config);
	    if(!$this->upload->do_upload('m_foto')) {    
	   		$opcion=2;
			$this->index($opcion);

	    }

	}
	
	if($opcion==0){    
	  	//Guardar registro
	   	$nombre = $this->input->post('m_nombre');
	   	$apellido = $this->input->post('m_apellido');
	   	$sexo = $this->input->post('m_sexo'); 
	   	$email = $this->input->post('m_correo'); 
	   	$clave = $this->input->post('m_password'); 
        

	   	$clave = md5($this->input->post('m_password'));

		$data = array(
			'password'=>$clave,
			'run'=>$this->input->post('m_run'),
		    'nombre'=>$this->input->post('m_nombre'),
		    'apellido'=>$this->input->post('m_apellido'),
		    'email'=>$this->input->post('m_correo'),
            'administrador'=> $this->session->userdata('administrador'),
            'id_empresa'=> $this->session->userdata('idEmpresa'),
            'id_sucursal'=> $this->session->userdata('idSucursal'),
		    'id_create'=>$this->session->userdata('idUsuario'),
		    'foto'=>$nombreFoto,
		    'sexo'=>$this->input->post('id_cbo_sexo'),
		);
		$respuesta = $this->Sg_usuario_mdl->guardar_add($data);
		$opcion=1;
		$this->index($opcion);
	}	
}//fin val session	
}


function ajax_guardar_mod(){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){	

    $status = 0;

	$run = $this->input->post('m_run');
	$run_orig = $this->input->post('run_orig');

    if(strtoupper($run) != strtoupper($run_orig)){
	    $status = $this->ajax_validar_duplicidad_modificar($run);
	}

    if($status==0){
    	//$this->load->model('Es_subModulo_mdl');
		$nombre = $this->input->post('m_nombre');
		$apellido = $this->input->post('m_apellido');
		$sexo = $this->input->post('id_cbo_sexo'); 
		$email = $this->input->post('m_correo'); 

		$id = $this->input->post('id_mod');

		$data = array(
			//'login'=>$login,
			'run'=>$this->input->post('m_run'),
	        'nombre'=>$nombre,
	        'apellido'=>$apellido,
	        'sexo'=>$sexo,
	        'email'=>$email,
	  	    'id_update'=>$this->session->userdata('idUsuario'),
	  	    'date_update'=>date('Y-m-d H:i:s')        
		);
		$respuesta = $this->Sg_usuario_mdl->guardar_mod($id,$data);

	    $row = $this->Sg_usuario_mdl->obt_dataTable();
		$html= $this->generarDatatable($row);
		$data = array(
		    "registro"=>$html,
		    "status"=>$status);
	}else{
		$data = array(
		    "status"=>1);
	}	

    echo json_encode($data);
}//fin val session	
}
	


function ajax_guardar_modClave(){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){	
    $status = 0;
    $idUsuario = $this->session->userdata('idUsuario');
    $claveActual = $this->input->post('m_passwordAct');
    
    $resultado=0;
    $resultado_= $this->Sg_usuario_mdl->val_claveActual($idUsuario, $claveActual);
    
    if($resultado==0){
		$claveNueva = md5($this->input->post('m_passwordNew'));
		$data = array(
			'password'=>$claveNueva,
	  	    'id_update'=>$this->session->userdata('idUsuario'),
	  	    'date_update'=>date('Y-m-d H:i:s')        
		);
		$respuesta = $this->Sg_usuario_mdl->guardar_mod($idUsuario,$data);
		$data = array(
		    "status"=>0
		);
	}else{
		$data = array(
		    "status"=>1
		);
	}	
    echo json_encode($data);
}//fin val session	
}






function ajax_guardar_modFoto(){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){	
    $status = 0;

    $foto = $_FILES['mod_foto']['name'];
    $id = $this->input->post('id_mod_foto');
    $rowUsuario = $this->Sg_usuario_mdl->obtModificar($id);
    $run = $rowUsuario->run;

    $nombreFoto="";

    $opcion=0;
    $nombreFoto = $this->session->userdata('idEmpresa')."-".$this->session->userdata('idsucursal')."-";
    $nombreFoto.= $nombreFoto . $run . "-mod-" .$foto;
    $config['upload_path']          = './assets/foto/'; // Carpeta a subr
    $config['allowed_types']        = 'gif|jpg|png|jpeg'; // Tipos permitidos
    $config['max_size']             = 2000; // Max 2Mb
    $config['max_width']            = 0; // Sin límite de width
    $config['max_height']           = 0; // Sin límite de height
    $config['file_name'] = $nombreFoto; // Nombre de la imagen
    $this->load->library('upload', $config);
    if(!$this->upload->do_upload('mod_foto')) {    
   		$opcion=1;
		$this->index($opcion);
    }
	$data = array(
		'foto'=>$nombreFoto,
  	    'id_update'=>$this->session->userdata('idUsuario'),
  	    'date_update'=>date('Y-m-d H:i:s')        
	);

	$respuesta = $this->Sg_usuario_mdl->guardar_mod($id,$data);

	$opcion=1;
	$this->index($opcion);

}//fin val session	
}




function ajax_desactivar($id){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){	
	$data = array(
  	    'activo'=>0,
  	    'id_update'=>$this->session->userdata('idUsuario'),
  	    'date_update'=>date('Y-m-d H:i:s')
    );

	$respuesta = $this->Sg_usuario_mdl->desactivar($id,$data);
	$row = $this->Sg_usuario_mdl->obt_dataTable();
	$html= $this->generarDatatable($row);
	$data = array(
	    "registro"=>$html);
    echo json_encode($data);
}//fin val session    
}



function ajax_reactivar($id){

//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){	

	//$this->load->model('Es_subModulo_mdl');
	$data = array(
  	    'activo'=>1,
  	    'id_update'=>$this->session->userdata('idUsuario'),
  	    'date_update'=>date('Y-m-d H:i:s')
    );


	$respuesta = $this->Sg_usuario_mdl->desactivar($id,$data);
	
	$row = $this->Sg_usuario_mdl->obt_dataTable();
	$html= $this->generarDatatable($row);
	$data = array(
	    "registro"=>$html);
    echo json_encode($data);

}//fin val session
}

	
	
	
	
	

	function generarDatatable($row){
		//obtener la información de la tabla seleccionada
		$modificar = base_url() . "assets/images/modificar02.jpeg";
		$eliminar = base_url() . "assets/images/eliminar.jpeg";
		$reactivar = base_url() . "assets/images/reactivar.jpeg";
		$lectura = base_url() . "assets/images/ver.png";
		$modificarFoto = base_url() . "assets/images/hombre_sinFoto.jpeg";

        $html = '<table id="basic-datatables" class="display table table-striped table-hover">';
        $html.= '    <thead>';
        $html.= '        <tr class="tr-datatable">';
		$html.= '           <th width="12%">RUN</th>';	        
		$html.= '           <th width="30%">Apellido(s), nombre(s)</th>';	
		$html.= '           <th width="18%">Email</th>';
		$html.= '           <th width="10%">Sexo</th>';
		$html.= '           <th width="10%">Foto</th>';	                
        $html.= '           <th width="20%">Acción</th>';
        $html.= '        </tr>';
        $html.= '    </thead>';
        $html.= '    <tfoot>';
        $html.= '        <tr class="tr-datatable">';
		$html.= '           <th width="12%">RUN</th>';	        
		$html.= '           <th width="30%">Apellido(s), nombre(s)</th>';	
		$html.= '           <th width="18%">Email</th>';
		$html.= '           <th width="10%">Sexo</th>';
		$html.= '           <th width="10%">Foto</th>';	                
        $html.= '           <th width="20%">Acción</th>';
        $html.= '        </tr>';
        $html.= '    </tfoot>';
        $html.= '    <tbody>';
        if($row==false){
			$html.= '      <tr>';
			$html.= '          <td></td><td></td><td></td><td></td><td></td>';
			$html.= '      </tr>';
		}else{
		    foreach($row as $key){ 
				$html.= '  <tr>';
				$html.= '      <td>' . $key->run.'</td>';
                $apellido = trim($key->apellido).", " . trim($key->nombre);
                $html.= '      <td>' . $apellido.'</td>';
				$html.= '      <td>' . $key->email.'</td>';	
				$sexo = "MASCULINO";
                if($key->sexo=="F"){
                	$sexo = "FEMENINO";
                } 
				$html.= '      <td>' . $sexo.'</td>';	
                //mostrar fotos.
                if($key->foto==null || empty($key->foto)){
                	if($key->sexo=="F"){
                        $foto = base_url() . "assets/images/mujer_sinFoto.jpeg";
                	}else{
                		$foto = base_url() . "assets/images/hombre_sinFoto.jpeg";
                	}
                }else{
                     $foto = base_url() . "assets/foto/" . $key->foto;

                }
				
	
				$html.= '<td style="text-align="center">';
				$html.= '<img src="'.$foto.'" style="width:40px; height:40px" alt="Foto"></a>';
				
				$html.= '</td>';

				$html.= '<td>';		

				if($key->id_empresa==$this->session->userdata('idEmpresa') or $this->session->userdata('administrador')==1){
					if($key->activo==1){

						if($this->clssession->accion(3,1)==1){ 
							$html.= '<a href="javascript:void(0)" onclick="javascript:ver_form('.$key->id.')">';
							$html.= '<img src="'.$lectura.'" style="width:30px; height:30px" alt="Ver" title="Ver Usuario"></a>';
						}	

						if($this->clssession->accion(3,3)==1){ 
							$html.= '&nbsp;<a href="javascript:void(0)" onclick="javascript:modificar_form('.$key->id.')">';

							$html.= '<img src="'.$modificar.'" style="width:30px; height:30px" alt="Modificar" title="Modificar Usuario"></a>';
	                    } 

						if($this->clssession->accion(3,3)==1){ 
							$html.= '&nbsp;<a href="javascript:void(0)" onclick="javascript:modificarFoto_form('.$key->id.')">';

							$html.= '<img src="'.$modificarFoto.'" style="width:30px; height:30px" alt="ModificarFoto" title="Modificar Foto"></a>';
	                    } 
	                    

						if($this->clssession->accion(3,4)==1){                     
							$html.= '&nbsp;<a href="javascript:void(0)" onclick="javascript:eliminar_form('.$key->id.')">';
							$html.= '<img src="'.$eliminar.'" style="width:30px; height:30px" alt="Desactivar" title="Desactivar Usuario"></a>';
						}	
					}else{
						if($this->clssession->accion(3,5)==1){                     					
							$html.= '<a href="javascript:void(0)" onclick="javascript:reactivar_form('.$key->id.')">';
							$html.= '<img src="'.$reactivar.'" style="width:30px; height:30px" alt="Activar" title="Reactivar Usuario"></a>';
						}	
					}	
				} //fin del if administrador	
				$html.= '       </td>';				
				$html.= '</tr>';
			}
		}	
		$html.= '    </tbody>';
		$html.= '</table>';
		return $html;    				
	}


/*	function ajax_validar_duplicidad(){
        $login = $_GET['login']; 

		$nro = $this->Sg_usuario_mdl->valLogin($login);
		$status = 0;
		if($nro>0){
		    $status = 1;	
		}

		$data = array(
		    "status"=> $status
		);
	    echo json_encode($data);		
	}
*/	
	

	function ajax_validar_duplicidad(){
        $run = $_GET['run']; 

		//$nro = $this->Sg_usuario_mdl->valLogin($run);
		$nro = $this->Sg_usuario_mdl->valRun($run);
		$status = 0;
		if($nro>0){
		    $status = 1;	
		}

		$data = array(
		    "status"=> $status
		);
	    echo json_encode($data);		


	}
	


	
/*	function ajax_validar_duplicidad_modificar($login){

		$nro = $this->Sg_usuario_mdl->valLogin($login);
		$status = 0;
		if($nro>0){
		    $status = 1;	
		}
		
		return $status;
	}	
*/


	function ajax_validar_duplicidad_modificar($run){

		$nro = $this->Sg_usuario_mdl->valRun($run);
		$status = 0;
		if($nro>0){
		    $status = 1;	
		}
		
		return $status;
	}	


	

}
