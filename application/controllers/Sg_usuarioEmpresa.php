<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sg_usuarioEmpresa extends CI_Controller {

	
	public function __construct() {
         parent::__construct();
         $this->load->model(array('Sg_usuarioEmpresa_mdl','Sg_usuario_mdl','Sg_rolModulo_mdl','Sg_usuarioEmpresaModulo_mdl','Sg_permiso_mdl','Es_subModulo_mdl'));
         $this->load->library(array('Clssession')); 
    }


	public function index()
	{
    //Validar session usuario
    $valSession = $this->clssession->valSession();
    if($valSession==true){			
		
        //accion 0 vista sin datatable, accion 1  activar datatable 
		$data=array(
			'accion'=>1,
			'modulo'=>1,
		);

	    $this->load->view('header');
		$this->load->view('menu');
		$this->load->view('seguridad/usuarioEmpresa/act_usuarioEmpresa',$data);
		$this->load->view('footer/footer', $data);
		$this->load->view('seguridad/usuarioEmpresa/footer', $data);
	} //fin val session
	}



	function ajax_datatable($id){
		$row = $this->Sg_usuarioEmpresa_mdl->obt_dataTable();
		$html= $this->generarDatatable($row);
		$data = array(
		    "registro"=>$html);
        echo json_encode($data);	

	}	

	function generarDatatable($row){

        //La variable rol trae los diferentes roles sin estar asociados a los modulos. 

		//obtener la información de la tabla seleccionada
		$modificar = base_url() . "assets/images/modificar02.jpeg";
		$eliminar = base_url() . "assets/images/eliminar.jpeg";
		$reactivar = base_url() . "assets/images/reactivar.jpeg";
		$lectura = base_url() . "assets/images/ver.png";		



        $html = '<table id="basic-datatables" class="display table table-striped table-hover">';
        $html.= '    <thead>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="15%">RUN</th>';
        $html.= '           <th width="30%">Apellido(s), Nombre(s)</th>';        
		$html.= '           <th width="45%">Empresa(s)</th>';        
        $html.= '           <th width="10%">Acción</th>';
        $html.= '        </tr>';
        $html.= '    </thead>';
        $html.= '    <tfoot>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="15%">RUN</th>';
        $html.= '           <th width="30%">Apellido(s), Nombre(s)</th>';        
		$html.= '           <th width="45%">Empresa(s)</th>';        
        $html.= '           <th width="10%">Acción</th>';
        $html.= '        </tr>';
        $html.= '    </tfoot>';
        $html.= '    <tbody>';
        if($row==false){
			$html.= '      <tr>';
			$html.= '          <td></td><td></td><td></td><td></td>';
			$html.= '      </tr>';
		}else{
		    foreach($row as $key){ 
				$html.= '  <tr>';
				$html.= '      <td>' . $key->run.'</td>';
				$nombre = trim($key->apellido) . ", " . trim($key->nombre);
				$html.= '      <td>' . $nombre.'</td>';

                //Configurar los modulos por roles
				$rowEmpresa = $this->Sg_usuarioEmpresa_mdl->obt_empresa_x_usuario($key->id);
				$empresa = "";
				$swEmpresa=0;

                
                $arregloEmpresa=[];
                $ind=0;
                foreach($rowEmpresa as $key2){
					if($swEmpresa == 0){
					   $arregloEmpresa[0] = $key2->empresa;
					   $swEmpresa=1;
					   $empresa = trim($key2->empresa); 
					}else{
						$n = 0;
						$swEncontrado=0;
						while($n <= $ind && $swEncontrado==0){
							if($arregloEmpresa[$n]==$key2->empresa){
							    $swEncontrado=1;	
							}
							$n+=1;
						}
						if($swEncontrado==0){
							$ind=$ind + 1; 
						   	$arregloEmpresa[$n] = $key2->empresa;
						   	$empresa = $empresa . " -- " . trim($key2->empresa); 
						}
					}
					
                }  
                if($swEmpresa==0){
                	$empresa = "SIN EMPRESA ASOCIADA";
                }
                $html.= '      <td>' . $empresa.'</td>';
           		$html.= '      <td>';	

           		if($this->clssession->accion(3,3)==1){ 			
                    $html.= '<a href="javascript:void(0)" onclick="javascript:usuarioEmpresaAct('.$key->id.')">';


					//$html.= '<a href="' . base_url('usuarioEmpresaActualizar').'/'.$key->id. '">';
										
					$html.= '<img src="'.$modificar.'" style="width:30px; height:30px" alt="Asignar Empresa al Usuario"></a>';
                }
				$html.= '       </td>';				
				$html.= '</tr>';
			}
		}	
		$html.= '    </tbody>';
		$html.= '</table>';
		return $html;    				
	}


public function actualizarEmpresa(){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){				
    //accion 0 vista sin datatable, accion 1  activar datatable 
    $idUsuario = $this->session->userdata('idUsuarioEmp');

	$data=array(
		'accion'=>1,
		'modulo'=>2,
		'idUsuario'=>$idUsuario,
	);

    $this->load->view('header');
	$this->load->view('menu');
	$this->load->view('seguridad/usuarioEmpresa/act_empresa',$data);
	
	//$this->load->view('seguridad/usuarioEmpresa/footer', $data);
    $this->load->view('footer/footer', $data);
    $this->load->view('seguridad/usuarioEmpresa/footer', $data);

} //fin val session    
}


public function ajax_datatable_empresa($idUsuario){
	//$this->load->model('sg_rolesUsuario_mdl');

	$row = $this->Sg_usuarioEmpresa_mdl->obt_empresa_x_usuario($idUsuario);
	$rowUsuario = $this->Sg_usuario_mdl->obtModificar($idUsuario);


	$nobUsuario = trim($rowUsuario->login) . " / " . trim($rowUsuario->apellido) . ", " . trim($rowUsuario->nombre);

	$html= $this->generarDatatable_empresa($row, $idUsuario);
		$data = array(
			"idUsuario"=>$idUsuario,
			"nobUsuario"=>$nobUsuario,
		    "registro"=>$html);
        echo json_encode($data);	
	}



public function generarDatatable_empresa($row, $idUsuario){ 
   //La variable rol trae los diferentes roles sin estar asociados a los modulos. 

	//obtener la información de la tabla seleccionada
	$modificar = base_url() . "assets/images/modificar02.jpeg";
	$eliminar = base_url() . "assets/images/eliminar.jpeg";
	$reactivar = base_url() . "assets/images/reactivar.jpeg";
	$lectura = base_url() . "assets/images/ver.png";

    $html = '<table id="basic-datatables" class="display table table-striped table-hover">';
    $html.= '    <thead>';
    $html.= '        <tr class="tr-datatable">';
    $html.= '           <th width="30%">Empresa</th>';
    $html.= '           <th width="30%">Sucursal</th>';
	$html.= '           <th width="30%">Rol</th>';        
    $html.= '           <th width="10%">Acción</th>';
    $html.= '        </tr>';
    $html.= '    </thead>';
    $html.= '    <tfoot>';
    $html.= '        <tr class="tr-datatable">';
    $html.= '           <th width="30%">Empresa</th>';
    $html.= '           <th width="30%">Sucursal</th>';
	$html.= '           <th width="30%">Rol</th>';        
    $html.= '           <th width="10%">Acción</th>';
    $html.= '        </tr>';
    $html.= '    </tfoot>';
    $html.= '    <tbody>';
    if($row==false){
		$html.= '      <tr>';
		$html.= '          <td></td><td></td><td></td><td></td>';
		$html.= '      </tr>';
	}else{
	    foreach($row as $key){ 
			$html.= '  <tr>';
			$html.= '      <td>' . $key->empresa.'</td>';
			$html.= '      <td>' . $key->sucursal.'</td>';
			$html.= '      <td>' . $key->rol.'</td>';
			$html.= '      <td>';				

            if($this->clssession->accion(3,3)==1){ 
				    
				    $html.= '<a href="javascript:void(0)" onclick="javascript:usuarioEmpresaAM('.$key->id.')">';
				    //$html.= '<a href="' . base_url('usuarioEmpresa_AM').'/'.$key->id. '">';
				    $html.= '<img src="'.$modificar.'" style="width:30px; height:30px" alt="Actualizar" title="Actualizar Usuario - Empresa"></a>';
			}


            /*
            if($this->clssession->accion(3,4)==1){ 
				$html.= '<a href="javascript:void(0)" onclick="javascript:eliminar_form('.$key->id.')">';
				$html.= '<img src="'.$eliminar.'" style="width:30px; height:30px" alt="Desactivar" title="Quitar empresa al usuario"></a>';
			}
			*/


			$html.= '       </td>';				
			$html.= '</tr>';
		}
	}	
	$html.= '    </tbody>';
	$html.= '</table>';
	return $html;    				
}



function ajax_guardar_add(){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){		
     
    $status = 0;
    $idUsuario   = $this->input->post('idUsuario_add');
    $idEmpresa   = $this->input->post('id_cbo_empresas');
    $idSucursal   = $this->input->post('id_cbo_sucursal');
    $idRol   = $this->input->post('id_cbo_rol');

    //validar si existe un usuario en la misma empresa
    //s$status = $this->Sg_usuarioEmpresa_mdl->valEmpresa($idEmpresa, $idUsuario);
   if($status==0){    

	    //guardar usuario empresa
	    $data = array(
	       'id_rol'=>$idRol,
	       'id_usuario'=>$idUsuario,
	       'id_empresa'=>$idEmpresa,
	       'id_sucursal'=>$idSucursal,

	       'id_create'=>$this->session->userdata('idUsuario')
	    );

	    $idUsuarioEmpresa = $this->Sg_usuarioEmpresa_mdl->guardar_add_usuarioEmpresa($data);

	    //Guardar los modulos del usuario por el rol y la empresa
	    //obtener los modulos asociados al rol

	    $rowModulo = $this->Sg_rolModulo_mdl->obt_modulo_x_rol($idRol);

        foreach ($rowModulo as $key) {
            $data = array(
            	'id_usuario_empresa'=>$idUsuarioEmpresa,
            	'id_modulo'=>$key->id_modulo,
            	'id_create'=>$this->session->userdata('idUsuario')
            );

            $id_UEM = $this->Sg_usuarioEmpresa_mdl->guardar_add_usuarioEmpresaModulo($data);             

        	//guardo el modulo y recorro los permisos
        	$rowPermiso = $this->Sg_rolModulo_mdl->obt_permiso_x_modulo($key->id); 




	        foreach ($rowPermiso as $key2) {
	        	/// guardar los permisos asociados a usuario - empresa - modulo
	        	$data = array(
                    'id_uem'=>$id_UEM,
                    'id_modulo'=>$key2->id_modulo,
                    'id_permiso'=>$key2->id_permiso,
                    'id_create'=>$this->session->userdata('idUsuario')   
	        	); 
                $this->Sg_usuarioEmpresa_mdl->guardar_add_usuarioEmpresaModuloPermiso($data);             
	        }
	        
	        
	        $rowProceso = $this->Sg_rolModulo_mdl->obt_proceso_x_modulo($key->id); 
	        foreach($rowProceso as $key3){
				$data = array(
				    'id_uem'=>$id_UEM,
				    'id_modulo'=>$key3->id_modulo,
				    'id_sub_modulo'=>$key3->id_sub_modulo,
				    'id_create'=>$this->session->userdata('idUsuario')   
				);
				$this->Sg_usuarioEmpresa_mdl->guardar_add_usuarioEmpresaModuloProceso($data);             
			}
        }


	    $row = $this->Sg_usuarioEmpresa_mdl->obt_empresa_x_usuario($idUsuario);
		$html= $this->generarDatatable_empresa($row, $idUsuario);
		$data = array(
		    "registro"=>$html,

		    "status"=>$status);
	}else{
		$data = array(
		    "status"=>1);
	}	


	 echo json_encode($data);

} //fin val session
}	



function ajax_desactivar($id){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){		
	//$this->load->model('Es_subModulo_mdl');
    $arreglo = explode('a', $id);
    $id = $arreglo[0];
    $idUsuario = $arreglo[1];	
     
   
    //eliminqar sg_uuario_empresa
   	
   	$this->Sg_usuarioEmpresa_mdl->eliminar_usuario_empresa($id);

   	//obtener todos los modulo de usuario empresa aborrar
   	$row = $this->Sg_usuarioEmpresa_mdl->obt_usuario_empresa_modulo($id);


   	foreach ($row as $key) {
   		$this->Sg_usuarioEmpresa_mdl->eliminar_usuario_empresa_modulo_permiso($key->id);
  	}

    //eliminqar sg_uuario_empresa_modulo
   	$this->Sg_usuarioEmpresa_mdl->eliminar_usuario_empresa_modulo($id);

    $row = $this->Sg_usuarioEmpresa_mdl->obt_empresa_x_usuario($idUsuario);
	$html= $this->generarDatatable_empresa($row, $idUsuario);
	$data = array(
	    "registro"=>$html);
    echo json_encode($data);
} //fin val session  
}




public function actualizarEmpresaModulo(){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){	

    $idUsuarioEmpresa = $this->session->userdata('idUsuarioEmpresa');
	$data=array(
		'accion'=>1,
		'modulo'=>3,
		'idUsuarioEmpresa'=>$idUsuarioEmpresa,
		'swPermisoModulo'=>$this->session->userdata('swPermisoModulo')
	);

    $this->load->view('header');
	$this->load->view('menu');
	$this->load->view('seguridad/usuarioEmpresa/act_moduloUsuario',$data);
    $this->load->view('footer/footer', $data);
    $this->load->view('seguridad/usuarioEmpresa/footer', $data);


}//fin val session
}

public function ajax_datatable_moduloEmpresa($idUsuarioEmpresa){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){	
	$row = $this->Sg_usuarioEmpresaModulo_mdl->obtModulo_x_usuario($idUsuarioEmpresa);
	$rowUsuario = $this->Sg_usuarioEmpresa_mdl->obtUsuarioEmpresa($idUsuarioEmpresa);
	$nobUsuario = trim($rowUsuario->empresa) . " / " . trim($rowUsuario->apellido) . ", " . trim($rowUsuario->usuario);

	$html= $this->generarDatatable_empresaModulo($row, $idUsuarioEmpresa);



	$botonRegresar = '<a href="javascript:void(0)" onclick="javascript:regresarUsuarioEmpresa()"';
    $botonRegresar.= 'class="btn btn-primary btn-sm item_edit2" ';
    $botonRegresar.= 'style="border-radius: 15px; font-size:16px; font-weight:500; ';
    $botonRegresar.= 'height:45px;">Regresar</a>';
    
    

		$data = array(
			"idUsuarioEmpresa"=>$idUsuarioEmpresa,
			"nobUsuario"=>$nobUsuario,
		    "registro"=>$html,
		    "boton"=>$botonRegresar);
        echo json_encode($data);

}//fin val session
}

public function generarDatatable_empresaModulo($row, $idUsuarioEmpresa){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){	
    //La variable rol trae los diferentes roles sin estar asociados a los modulos. 

	//obtener la información de la tabla seleccionada
	$modificar = base_url() . "assets/images/modificar02.jpeg";
	$eliminar = base_url() . "assets/images/eliminar.jpeg";
	$reactivar = base_url() . "assets/images/reactivar.jpeg";
	$lectura = base_url() . "assets/images/ver.png";
    $proceso = base_url() . "assets/images/proceso.jpeg";	

    $html = '<table id="basic-datatables" class="display table table-striped table-hover">';
    $html.= '    <thead>';
    $html.= '        <tr class="tr-datatable">';
    $html.= '           <th width="45%">Modulo</th>';
	$html.= '           <th width="40%">Permisología</th>';        
    $html.= '           <th width="15%">Acción</th>';
    $html.= '        </tr>';
    $html.= '    </thead>';
    $html.= '    <tfoot>';
    $html.= '        <tr class="tr-datatable">';
    $html.= '           <th width="45%">Modulo</th>';
	$html.= '           <th width="40%">Permisología</th>';        
    $html.= '           <th width="15%">Acción</th>';
    $html.= '        </tr>';
    $html.= '    </tfoot>';
    $html.= '    <tbody>';
    if($row==false){
		$html.= '      <tr>';
		$html.= '          <td></td><td></td><td></td>';
		$html.= '      </tr>';
	}else{
	    foreach($row as $key){ 
			$html.= '  <tr>';
			$html.= '      <td>' . $key->nombre.'</td>';

			$rowPermiso = $this->Sg_usuarioEmpresaModulo_mdl->obtAccionesMod($key->id, $key->id_modulo);
            $accion = "";
            foreach ($rowPermiso as $key2) {
            	if($accion == ""){
            		$accion = trim($key2->accion);
            	}else{
            		$accion = $accion . " - " . trim($key2->accion);
            	}
            }
			$html.= '      <td>' . $accion . '</td>';
			$html.= '      <td>';				

         
            if($this->session->userdata('swPermisoModulo')==1){
	            if($this->clssession->accion(3,3)==1){ 
				}
					$html.= '<a href="javascript:void(0)" onclick="javascript:modificarModulo_form('.$key->id.')">';
					$html.= '<img src="'.$modificar.'" style="width:30px; height:30px" alt="Modificar" title="Establecer Permisología"></a>';
	            if($this->clssession->accion(3,4)==1){ 
					$html.= '<a href="javascript:void(0)" onclick="javascript:eliminarModulo_form('.$key->id.')">';
					$html.= '<img src="'.$eliminar.'" style="width:30px; height:30px" alt="Desactivar" title="Quitar Módulo"></a>';
				}
			}	

            if($this->clssession->accion(3,3)==1){  
				
				$html.= '<a href="javascript:void(0)" onclick="javascript:usuarioModuloProceso('.$key->id.')">';
                //$html.= '<a href="' . base_url('usuarioModuloProceso').'/'.$key->id. '">';
                $html.= '<img src="'.$proceso.'" style="width:30px; height:30px" alt="Sub Modelo Asociado" title="Asociar Submódulo"></a>';
            } 			




			$html.= '       </td>';				
			$html.= '</tr>';
		}
	}	
	$html.= '    </tbody>';
	$html.= '</table>';
	return $html;    				
}//fin val session
}



function ajax_desactivar_modulo($idUem){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){	
    $arreglo = explode('a', $idUem);
    $id = $arreglo[0];
    $idUsuarioEmpresa = $arreglo[1];	
     
   
    $this->Sg_usuarioEmpresa_mdl->eliminar_usuario_empresa_modulo_permiso($id);
   	$this->Sg_usuarioEmpresa_mdl->eliminar_usuario_empresa_modulo_ind($id);



    $row = $this->Sg_usuarioEmpresaModulo_mdl->obtModulo_x_usuario($idUsuarioEmpresa);
	$html= $this->generarDatatable_empresaModulo($row, $idUsuarioEmpresa);



	$data = array(
	    "registro"=>$html);
    echo json_encode($data);

}//fin val session
}



function ajax_guardarModulo_add(){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){	
    $status = 0;
    $idUsuarioEmpresa  = $this->input->post('idUsuario_add');
    $idModulo   = $this->input->post('id_cbo_moduloUsuario');

    if($status==0){    

        //obtenet el rol de eusuario_empresa
        $rowUE = $this->Sg_usuarioEmpresa_mdl->obtUsuarioEmpresa($idUsuarioEmpresa);
        $idRol = $rowUE->id_rol;

	    //guardar usuario modulo
	    $data = array(
	       'id_usuario_empresa'=>$idUsuarioEmpresa,
	       'id_modulo'=>$idModulo,
	       'id_create'=>$this->session->userdata('idUsuario')
	    );
	    $id_UEM = $this->Sg_usuarioEmpresa_mdl->guardar_add_usuarioEmpresaModulo($data);

        //guardar procesos asociados.


     ///guardar submodulo
     //obtener submodulo asociado al modulo.
    $rowSubModulo = $this->Es_subModulo_mdl->obtSubModulo($idModulo);

    foreach ($rowSubModulo as $key) {
        $dataSub= array(
            'id_uem'=>$id_UEM,
            'id_modulo'=>$idModulo,
            'id_sub_modulo'=>$key->id,
            'id_create'=>$this->session->userdata('idUsuario'),
        );
        $this->Sg_usuarioEmpresa_mdl->add_subModulo($dataSub);   
    }        
         
      	//guardo el modulo y recorro los permisos
      	$rowPermiso = $this->Sg_rolModulo_mdl->obt_permiso_x_moduloEmpresa($idModulo, $idRol); 


	        foreach ($rowPermiso as $key2) {
	        	/// guardar los permisos asociados a usuario - empresa - modulo
	        	$data = array(
                    'id_uem'=>$id_UEM,
                    'id_modulo'=>$key2->id_modulo,
                    'id_permiso'=>$key2->id_permiso,
                    'id_create'=>$this->session->userdata('idUsuario')   
	        	); 
                $this->Sg_usuarioEmpresa_mdl->guardar_add_usuarioEmpresaModuloPermiso($data);             
	        }	

	    $row = $this->Sg_usuarioEmpresaModulo_mdl->obtModulo_x_usuario($idUsuarioEmpresa);
		$html= $this->generarDatatable_empresaModulo($row, $idUsuarioEmpresa);

		$data = array(
		    "registro"=>$html,
		    "status"=>$status
		);
	}else{
		$data = array(
		    "status"=>1);
	}	

	 echo json_encode($data);

}//fin val session
}



//----------- permisologia 


function ajax_modificarPermiso($idUEM){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){	
   $row = $this->Sg_usuarioEmpresaModulo_mdl->obtDatos_UEM($idUEM);
     
    $nombreModulo = $row->modulo;
    $idModulo = $row->id_modulo;
    $idRol = $row->id_rol;
    
    $rowPermiso = $this->Sg_permiso_mdl->obt_permiso();
    $rowPermisoModulo = $this->Sg_usuarioEmpresaModulo_mdl->obt_permiso_modulo($idUEM);
    
    $html = "";
    $i=0;
    $cadenaArreglo="";
    $columna = 0;
        foreach ($rowPermiso as $key) {
        	$permiso = 0;
        	foreach ($rowPermisoModulo as $key2) {
                if($key->id==$key2->id_permiso){
	                $permiso = 1;
	                break; 
				}
        	}	

        	$i+=1;
        	$columna+=1 ;
        	$permiso_new = "permiso" .$i;

        	$check = "";
        	if($permiso==1){
                $check = "checked"; 
        	}

        	if($cadenaArreglo==""){
        		$cadenaArreglo=$cadenaArreglo . $key->id;
        	}else{
        		$cadenaArreglo= $cadenaArreglo . "**" . $key->id;
        	}	
            if($columna==1){
                $html.='<div class="row">';                
            } 
                $html.='<div class="col-md-3">';                

			$html.='<div class="custom-control custom-checkbox">';        	
        	$html.='<input type="checkbox" class="custom-control-input" id="'.$permiso_new.'" name="permiso'.$i.'" readonly="false" enabled="false" ' .$check. '>';
	    	$html.='<label class="custom-control-label" for="'.$permiso_new.'">'.$key->nombre.'</label>';
	    	$html.= "</div>";
	    	$html.= "</div>"; //fin del col

	    	if($columna==4){
               $html.= "</div>"; //fin del row
               $columna=0;

	    	}
        }

		$data = array(
		    "registro"=>$html,
		    "nroPermiso"=>$i,
		    "arregloPermiso"=>$cadenaArreglo,
		    "nombreModulo"=>$nombreModulo,
		    "idUEM"=>$idUEM,
		    "idModulo"=>$idModulo,
		);
		
        echo json_encode($data);	
}//fin val session	
}


function ajax_guardarPermisologia_mod(){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){	
    $status = 0;
    $idModulo = $this->input->post('idModulo');
    $idUEM =  $this->input->post('idUEM');
	$rowUsuarioEmpresa = $this->Sg_usuarioEmpresaModulo_mdl->obtener_usuarioEmpresa($idUEM);

    $idUsuarioEmpresa = $rowUsuarioEmpresa->id_usuario_empresa;

    //Obtener modulo
    $arregloPermiso = $this->input->post('arregloPermiso');
    $arreglo = explode( '**', $arregloPermiso);

    $nroPermiso     = $this->input->post('nroPermiso');
    //Guardar permiso del modulo
    $i = 0;

    $rowPermiso = $this->Sg_usuarioEmpresaModulo_mdl->obt_permiso_modulo($idUEM);

    while($i < $nroPermiso){
    	$i+=1;
    	$check  = "permiso" . $i;
        $j = $i-1; 
        $idPermiso = $arreglo[$j]; 
        $swActualizar = 0;  
       	if($this->input->post("$check")){
            foreach ($rowPermiso as $key) {
            	if($key->id_permiso==$idPermiso){
                    $swActualizar = 1;     
                    //$idModulo=$key->id_modulo;
                    break;
            	}
            }

            if($swActualizar == 0){
	    		$data = array(
	                'id_uem'=>$idUEM,
	                'id_permiso'=>$idPermiso,
	                'id_create' =>$this->session->userdata('idUsuario'),
                    'id_modulo' =>$idModulo,
	    		);
	    		
                $this->Sg_usuarioEmpresaModulo_mdl->guardar_add_permiso($data);            
            }
        }else{
            foreach ($rowPermiso as $key) {
            	if($key->id_permiso==$idPermiso){
                    $swActualizar = 1;     
                    break;
            	}
            }


            if($swActualizar == 1){
                $this->Sg_usuarioEmpresaModulo_mdl->eliminar_permiso($idPermiso, $idUEM);            
            }
        }    
    } //find del while  

    $row = $this->Sg_usuarioEmpresaModulo_mdl->obtModulo_x_usuario($idUsuarioEmpresa);
	$html= $this->generarDatatable_empresaModulo($row, $idUsuarioEmpresa);

	$data = array(
	    "registro"=>$html,
	    "status"=>$status
	);
    echo json_encode($data);
}//fin val session	
}	

///////////////////////////////////////////////////////////////////
// //////////////// proceso del modulo rol ///////////////////////
//////////////////////////////////////////////////////////////////


function actualizarProceso(){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){	

    $idEmpresaModulo = $this->session->userdata('idUsuarioEmpresaModulo');
     
    //Obtener el nombre del modulo
    $rowModulo = $this->Sg_usuarioEmpresa_mdl->obtModificar_empresaModulo($idEmpresaModulo);

    $dataSession =array(
        'idEmpresaModulo'=>$idEmpresaModulo,
        'nobModulo'=>$rowModulo->nombre,
        'idModulo'=>$rowModulo->id,

    );
    $this->session->set_userdata($dataSession);

    $data=array(
        'accion'=>1,
        'modulo'=>4,
        'idEmpresaModulo'=>$idEmpresaModulo,
        'nobModulo'=>$rowModulo->nombre,
        'idUsuarioEmpresa'=>$this->session->userdata("idUsuarioEmpresa"),
    );

    $this->load->view('header');
    $this->load->view('menu');
    $this->load->view('seguridad/usuarioEmpresa/act_ProcesoEmpresaModulo',$data);
    
    //$this->load->view('seguridad/usuarioEmpresa/footer', $data);
    $this->load->view('footer/footer', $data);
    $this->load->view('seguridad/usuarioEmpresa/footer', $data);
}//fin val session
}


function ajax_datatable_proceso($idEmpresaModulo){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){	
    $row = $this->Sg_usuarioEmpresa_mdl->obt_subModulo_x_modulo($idEmpresaModulo);
    $html= $this->generarDatatable_empresaProceso($row, $idEmpresaModulo);
    $data = array(
        "idEmpresaModulo"=>$idEmpresaModulo,
        "nobModulo"=>$this->session->userdata('nobModulo'),
        'idUsuarioEmpresa'=>$this->session->userdata("idUsuarioEmpresa"),
        "registro"=>$html);
    echo json_encode($data);    
}//fin val session
}



function generarDatatable_empresaProceso($row, $idEmpresaModulo){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){	
        //La variable rol trae los diferentes roles sin estar asociados a los modulos.         
        ///obtenet los submodulo completos asociado al modelo
        $rowProceso = $this->Es_subModulo_mdl->obtSubModulo($this->session->userdata('idModulo'));
        //obtener la información de la tabla seleccionada
        $modificar = base_url() . "assets/images/modificar02.jpeg";
        $incluir = base_url() . "assets/images/nuevo.png";
        $eliminar = base_url() . "assets/images/eliminar.jpeg";
        $reactivar = base_url() . "assets/images/reactivar.jpeg";
        $lectura = base_url() . "assets/images/ver.png";

        $html = '<table id="basic-datatables" class="display table table-striped table-hover">';
        $html.= '    <thead>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="70%">Sub Modulo</th>';
        $html.= '           <th width="15%">Asociado</th>';
        $html.= '           <th width="15%">Acción</th>';
        $html.= '        </tr>';
        $html.= '    </thead>';
        $html.= '    <tfoot>';
        $html.= '        <tr class="tr-datatable">';
        $html.= '           <th width="70%">Sub Modulo</th>';
        $html.= '           <th width="15%">Asociado</th>';
        $html.= '           <th width="15%">Acción</th>';
        $html.= '        </tr>';
        $html.= '    </tfoot>';
        $html.= '    <tbody>';
        if($rowProceso==false){
            $html.= '      <tr>';
            $html.= '          <td colspan="3">La Tabla no posee registros asociados</td>';
            $html.= '      </tr>';
        }else{
            foreach($rowProceso as $key){ 
                $html.= '  <tr>';
                $html.= '      <td>' . $key->nombre.'</td>';
                $sw = 0;
                $asoaciado = "NO ASOCIADO";
                $idModuloProceso = 0;
                foreach($row as $key2){
                    if($key->id == $key2->id){
                         $asoaciado = "ASOCIADO";
                         $idModuloProceso = $key2->id_empresa_proceso;
                         $sw = 1;
                         break;
                    }
                } 

                if ($sw == 1){
                    $html.= '      <td style="background-color:#0000ff; color:white">';               
                }else{    
                    $html.= '      <td>';
                }    
                $html.= $asoaciado;

                $html.= '       </td>';         

                $html.= '      <td>';               

                if($this->session->userdata('swPermisoModulo')==1){ 
	                if($sw==0){
	                    if($this->clssession->accion(3,3)==1){                  
	                        $html.= '&nbsp;<a href="javascript:void(0)" onclick="javascript:asociar_form('.$key->id.')">';
	                        $html.= '<img src="'.$incluir.'" style="width:30px; height:30px" alt="Asociar" title="Asociar Submódulo"></a>';
	                    }    
	                }


	                if($sw==1){  
	                    if($this->clssession->accion(3,4)==1){                  
	                        $html.= '&nbsp;<a href="javascript:void(0)" onclick="javascript:eliminarProceso_form('.$idModuloProceso.')">';
	                        $html.= '<img src="'.$eliminar.'" style="width:30px; height:30px" alt="Eliminar" title="Quitar Submódulo"></a>';
	                    }
	                }    
	            }    
                $html.= '       </td>';     
                $html.= '</tr>';
            }
        }   
        $html.= '    </tbody>';
        $html.= '</table>';
        return $html;                   
}//fin val session
}



function ajax_desactivarProceso($id){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){	
   $this->Sg_usuarioEmpresa_mdl->eliminarProceso($id);
   $idEmpresaModulo = $this->session->userdata('idEmpresaModulo');
    $row = $this->Sg_usuarioEmpresa_mdl->obt_subModulo_x_modulo($idEmpresaModulo); 

    $html= $this->generarDatatable_empresaProceso($row, $idEmpresaModulo);
    $data = array(
        "idEmpresaModulo"=>$this->session->userdata('idEmpresaModulo'),
        "nobModulo"=>$this->session->userdata('nobModulo'),
        "registro"=>$html);
    echo json_encode($data);    
}//fin val session 
}


function ajax_addProceso($id_subModulo){
//Validar session usuario
$valSession = $this->clssession->valSession();
if($valSession==true){	
    $data = array(
        'id_uem'=>$this->session->userdata('idEmpresaModulo'),
        'id_modulo'=>$this->session->userdata('idModulo'),
        'id_sub_modulo'=>$id_subModulo,
        'id_create'=>$this->session->userdata('idUsuario'),
    );

    $this->Sg_usuarioEmpresa_mdl->asociarProceso($data);
    $row = $this->Sg_usuarioEmpresa_mdl->obt_subModulo_x_modulo($this->session->userdata('idEmpresaModulo')); 


    $html= $this->generarDatatable_empresaProceso($row, $this->session->userdata('idEmpresaModulo'));
    $data = array(
        "idEmpresaModulo"=>$this->session->userdata('idEmpresaModulo'),
        "nobModulo"=>$this->session->userdata('nobModulo'),
        "registro"=>$html);
    echo json_encode($data);    

}//fin val session   
}


public function ajax_actualizarEmpresa($idUsuario){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){  
    $dataSession=array(
        'idUsuarioEmp'=>$idUsuario
    );
    $this->session->set_userdata($dataSession);
    $data = array(
        'status'=>0
    );
    echo json_encode($data);
}
} 


public function ajax_actualizarUsuarioEmpresa(){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){  
	$idUsuario = $this->session->userdata('idUsuarioEmp');
    $data = array(
        'status'=>0
    );
    echo json_encode($data);
}
} 


public function ajax_actualizarUsuarioEmpresaAM($idUsuarioEmpresa){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){  

    $rowUsuarioEmpresa = $this->Sg_usuarioEmpresa_mdl->obtUsuarioEmpresa($idUsuarioEmpresa);
    $swPermisoModulo = 0;
    if($rowUsuarioEmpresa->id_empresa_create == $this->session->userdata('idEmpresa') &&
    	$rowUsuarioEmpresa->id_sucursal_create == $this->session->userdata('idSucursal')){
        $swPermisoModulo = 1; 
    }

    $dataSession=array(
        'idUsuarioEmpresa'=>$idUsuarioEmpresa,
        'swPermisoModulo'=>$swPermisoModulo
    );

    $this->session->set_userdata($dataSession);
    $data = array(
        'status'=>0
    );
    echo json_encode($data);
}
} 







public function ajax_actualizarUsuarioEmpresaProceso($idUsuarioEmpresaModulo){
//validar variables de session.
$valSession = $this->clssession->valSession();
if($valSession==true){  
    $dataSession=array(
        'idUsuarioEmpresaModulo'=>$idUsuarioEmpresaModulo
    );
    $this->session->set_userdata($dataSession);
    $data = array(
        'status'=>0
    );
    echo json_encode($data);
}
} 








}  //fin de la clase

