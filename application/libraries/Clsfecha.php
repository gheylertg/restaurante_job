<?php if ( ! defined('BASEPATH')) exit('No se permite el acceso directo al script');

class Clsfecha {
   
   //Formatea una fecha 00/00/0000 al formato 0000/00/00
   public function formato_amd($fecha){
	  $formato = substr($fecha,6,4) . "-". substr($fecha,3,2) ."-". substr($fecha,0,2);
      return $formato;
   }
   
   //Formatea una fecha 0000/00/00 al formato 00/00/0000
   public function formato_dma($fecha){
	  $formato = substr($fecha,8,2) . "-". substr($fecha,5,2) ."-". substr($fecha,0,4);
      return $formato;	   
   }


   public function formato_dma_barra($fecha){
	  $formato = substr($fecha,8,2) . "/". substr($fecha,5,2) ."/". substr($fecha,0,4);
      return $formato;	   
   }
   
   public function compararFechas($fecInicial, $fecFinal, $comparacion){
	   //1 = f. inicial >= final;  
	   //2=f final >= inicial; 
	   //3=f. inicial = f final, 
	   //4 = inicial mayor final
	   //5 = inicial menor final
	   $valoresInicial = explode ("-", $fecInicial);
	   $valoresFinal = explode ("-", $fecFinal);
	   $diaInicial  = $valoresInicial[0];  
	   $mesInicial  = $valoresInicial[1];  
       $anyoInicial   = $valoresInicial[2]; 
  
	   $diaFinal   = $valoresFinal[0];  
	   $mesFinal = $valoresFinal[1];  
       $anyoFinal  = $valoresFinal[2];
       
       if(empty($diaFinal)){
       	  $diaFinal = 0;
       } 

       if(empty($mesFinal)){
       	  $mesFinal = 0;
       } 

       if(empty($anyoFinal)){
       	  $anyoFinal = 0;
       } 


       $diasInicialJuliano = gregoriantojd($mesInicial, $diaInicial, $anyoInicial);  
       $diasFinalJuliano = gregoriantojd($mesFinal, $diaFinal, $anyoFinal);
       $resultado = $diasInicialJuliano - $diasFinalJuliano;
       $opcion = 0;
       switch($comparacion){
       case 1: 
           if($resultado >=  0){
			   $opcion=1;
		   }
		   break;
       
       case 2: 
           if($resultado <=  0){
			   $opcion=1;
		   }
           break;      

       case 3: 
           if($resultado ==  0){
			   $opcion=1;
		   }
           break;       
       
       case 4: 
           if($resultado >  0){
			   $opcion=1;
		   }
           break; 
            
       case 5: 
           if($resultado <  0){
			   $opcion=1;
		   }
           break;	  
       } 
       return $opcion;
       
    }       
    
	public function calcularEdad($fecNacimiento){  // 0000/00/00
        if (count(explode("/", $fechaNacimiento)) != 3) {
              // Si no está bien construida la fecha te la pongo a 0000/00/00
			$nacimiento = "0000/00/00";
		} else {
			$nacimiento = $fechaNacimiento;
		}
		$fnacimiento = explode("/", $nacimiento);
		$nYear = intval($fnacimiento[0]);
		$nMes  = intval($fnacimiento[1]);
		$nDia  = intval($fnacimiento[2]);
		$Year  = intval(date('Y'));
		$Mes   = intval(date('m'));
		$Dia   = intval(date('d'));
		$rMes  = 0;
		$rYear = 0;
		if ($Dia > $nDia) {
			$rMes = 1;
		}

		if ($Mes > $nMes) {
			$rYear = 1;
		} elseif ($Mes == $nMes) {
			if ($rMes == 1) {
				$rYear = 1;
			}
		}

		if ($Dia == $nDia and $Mes == $nMes) {
			$rYear = 1;
		}
		$edad = $Year - $nYear + $rYear - 1;		
		return $edad;
	}    
 
 	public function calcularEdad_otro($fecNacimiento){  // 0000/00/00
		$fecha_nac = strtotime($fecha_nac);
		$edad = date('Y', $fecha_nac);
		if (($mes = (date('m') - date('m', $fecha_nac))) < 0) {
			$edad++;
		} elseif ($mes == 0 && date('d') - date('d', $fecha_nac) < 0) {
			$edad++;
		}
		return date('Y') - $edad;
	}
	
 	public function mes_letra($mes){  
		$letra = '';
		switch($mes){
		case 1:
		    $letra = "ENERO";
		    break;	
		case 2:
		    $letra = "FEBRERO";
		    break;	
		case 3:
		    $letra = "MARZO";
		    break;	
		case 4:
		    $letra = "ABRIL";
		    break;	
		case 5:
		    $letra = "MAYO";
		    break;	
		case 6:
		    $letra = "JUNIO";
		    break;	
		case 7:
		    $letra = "JULIO";
		    break;	
		case 8:
		    $letra = "AGOSTO";
		    break;	
		case 9:
		    $letra = "SEPTIEMBRE";
		    break;	
		case 10:
		    $letra = "OCTUBRE";
		    break;	
		case 11:
		    $letra = "NOVIEMBRE";
		    break;	
		case 12:
		    $letra = "DICIEMBRE";
		    break;	
		}
		return $letra;
	}
	
	
	public function dia_mes($mes, $anio){
	$sw_dia = 0;
	if($anio % 4 == 0){
	    $sw_dia = 1;	
	}	
	switch($mes){
	case 1:
	    return 31;
	    break;	
	case 2:
	     if($sw_dia==0){return 28;}else{return 31;}
	    break;	
	case 3:
	    return 31;
	    break;	
	case 4:
	    return 30;
	    break;	
	case 5:
	    return 31;
	    break;	
	case 6:
	    return 30;
	    break;	
	case 7:
	    return 31;
	    break;	
	case 8:
	    return 31;
	    break;	
	case 9:
	    return 30;
	    break;	
	case 10:
	    return 31;
	    break;	
	case 11:
	    return 30;
	    break;	
	case 12:
	    return 31;
	    break;	
	}
	}
	
	
    
    
   
} //Fin de la clase

?> 
