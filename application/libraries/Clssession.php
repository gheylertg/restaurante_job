<?php if ( ! defined('BASEPATH')) exit('No se permite el acceso directo al script');

class Clssession {

public function valSession(){
	$ci = &get_instance();
	if($ci->session->userdata('idUsuario')==false){ //id usuario
	    $mensaje = "La sesión del usuario, ha expirado.";
		$mensaje.= "<br><br>Para ingresar de nuevo debe loguearse.";
		$data = array('mensaje'=>$mensaje);
		$ci->load->view('header');
		$ci->load->view('login/plantillaExpirar',$data);
		$ci->load->view('footer/footer');
		return false;
	}else{
		return true;
	}
}

public function accion($idModulo, $idAccion){
	$ci = &get_instance();
    $nroFila =  $ci->session->userdata('nroFilaAccion');
    $arreglo =  $ci->session->userdata('arregloAccion');

    $i = 0;
    $parar = 0;
 
    //Nota: la ñosición del arreglo $arreglo[$i][3] se refiere al campo identificador que e reconocido
    //        para activar las permisologias en las vista de acuerdo al modulo predefinidoen las vistas.
    //1: Mantenimiento del sistema
    //2: Administrador del sistema
    //3: Modulo de seguridad.
    //4.- Estructura del sistema
    
    
    while($i <= $nroFila && $parar==0){
        if($arreglo[$i][0]==$idModulo){
           if($arreglo[$i][1]==$idAccion){
               $parar=1;
           } 
        } 
        $i+=1;
    } 
    if($parar==1){
    	return 1; 
    }else{
        return 0; 
    }
}



public function eliminarVariableSession(){
     $ci = &get_instance();
}

public function eliminarVariableRep_General(){
     $ci = &get_instance();
}




} //Fin de la clase

?>
