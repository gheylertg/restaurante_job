<?php
class Ad_alimento_mdl extends CI_Model {

	
	public function __construct(){
        parent::__construct();
       
    }
    

  public function obt_dataTable(){
		$sql = "select  ad_alimento.* ";
		$sql.= "from ad_alimento ";
		$sql.= "where ad_alimento.activo = 1 and ad_alimento.id_empresa=" . $this->session->userdata('idEmpresa');
		$sql.= " and ad_alimento.id_sucursal = " . $this->session->userdata('idSucursal');
    $sql.= " and ad_alimento.id_categoria_alimento = 1";
		$sql.= " order by ad_alimento.nombre ";
		$sq= $this->db->query($sql);
  	    return $sq->result();
  }
  

  public function guardar_alimentoSB($data){
    $this->db->insert('ad_alimento', $data);
    return $this->db->insert_id();
  }

  public function guardar_detalleIngrediente($data){
    $this->db->insert('ad_alimento_ingrediente', $data);
  }  


   public function obtModificarAlimento($id){
      $this->db->select('*');
      $this->db->where('id',$id);
      return $this->db->get('ad_alimento')->row();
   }


   public function obtModificarIngredienteAlimento($id){
      $this->db->select('*');
      $this->db->where('id',$id);
      return $this->db->get('ad_alimento_ingrediente')->row();
   }   


  public function guardar_mod_alimento($id,$data){
    $this->db->where('id',$id);
    $this->db->update('ad_alimento', $data);
    return $this->db->affected_rows();
  }


    public function valNombre($nombre){

    $sql ="select * ";
    $sql.="from ad_alimento ";
    $sql.="where nombre = '" . $nombre . "' ";
    $sql.=" and id_empresa = " . $this->session->userdata('idEmpresa') . " ";
    $sql.=" and id_sucursal = " . $this->session->userdata('idSucursal');
    $sq= $this->db->query($sql);
    $row = $sq->row();    
        $nro = $sq->num_rows(); 
        if($nro==1){
            return 1;
        }else{
          return 0;
        }  
    }

   
   public function verAlimento($id){
       $sql = "select ad_alimento.* ";
       $sql.= "from ad_alimento ";
       $sql.= "where ad_alimento.id = " . $id;
      $sq= $this->db->query($sql);
      return $sq->row();    
   }  


  function valIngrediente($idAlimento, $idIngrediente){
     $sql ="select * ";
     $sql.="from ad_alimento_ingrediente ";
     $sql.="where id_alimento = " . $idAlimento;
     $sql.=" and id_ingrediente = " . $idIngrediente;

     //die($sql);
     $sq= $this->db->query($sql);
     $row = $sq->row();    
     $nro = $sq->num_rows(); 
     if($nro==1){
         return 1;
     }else{
         return 0;
     }  
  }


  
  public function guardar_ingrediente_add($data){
      $this->db->insert('ad_alimento_ingrediente', $data);	  
      return $this->db->affected_rows();

  }	  
  

  public function guardar_mod_ingredienteAlimento($id,$data){
    $this->db->where('id',$id);
    $this->db->update('ad_alimento_ingrediente', $data);
    return $this->db->affected_rows();
  }


    public function obt_dataTableIngrediente($id){
    $sql = "select  si.*, ing.nombre as ingrediente, um.nombre as unidad_medida ";
    $sql.= "from ad_alimento_ingrediente si ";
    $sql.= "left join ad_ingrediente ing on si.id_ingrediente = ing.id ";
    $sql.= "left join mt_unidad_medida um on si.id_unidad_medida = um.id ";
    $sql.= "where si.id_alimento = " .$id;
    $sql.= " order by ing.nombre ";
    $sq= $this->db->query($sql);
    $nro = $sq->num_rows(); 
    if($nro>0){
       return $sq->result();
      }else{
       return false;
      }      
   }


  public function eliminarIngrediente($id){
    $this->db->where('id', $id);
    $this->db->delete('ad_alimento_ingrediente'); 
    return true;
     
  }







}
