<?php
class Ad_envoltura_mdl extends CI_Model {

	
	public function __construct(){
        parent::__construct();
       
  }


public function obt_dataTable(){
      $idEmpresa = $this->session->userdata('idEmpresa');
      $idSucursal = $this->session->userdata('idSucursal');

      $sql = "select ad_envoltura.*, mt_unidad_medida.nombre as unidad_medida, ";
      $sql.= "mn_ingrediente.nombre as ingrediente ";
      $sql.= "from ad_envoltura ";
      $sql.= "inner join mt_unidad_medida on ad_envoltura.id_unidad_medida = mt_unidad_medida.id ";
      $sql.= "inner join mn_ingrediente on ad_envoltura.id_ingrediente = mn_ingrediente.id ";
      switch($this->session->userdata('administrador')){
      case 1:
          $sql.="where ad_envoltura.administrador=1 ";
          break;
      case 2:
          $sql.="where ad_envoltura.administrador=2 and ad_envoltura.id_empresa = " . $idEmpresa;
          break;
      default:
          $sql.="where ad_envoltura.id_sucursal = " . $idSucursal;
          break;
      }
      
      
    $sql.= " order by ad_envoltura.nombre";
    $sq= $this->db->query($sql);
    return $sq->result();
  }


public function obt_envoltorio(){
      $idEmpresa = $this->session->userdata('idEmpresa');
      $idSucursal = $this->session->userdata('idSucursal');

      $sql = "select ad_envoltura.*, mt_unidad_medida.nombre as unidad_medida, ";
      $sql.= "mn_ingrediente.nombre as ingrediente ";
      $sql.= "from ad_envoltura ";
      $sql.= "inner join mt_unidad_medida on ad_envoltura.id_unidad_medida = mt_unidad_medida.id ";
      $sql.= "inner join mn_ingrediente on ad_envoltura.id_ingrediente = mn_ingrediente.id ";
      switch($this->session->userdata('administrador')){
      case 1:
          $sql.="where ad_envoltura.administrador=1 ";
          break;
      case 2:
          $sql.="where ad_envoltura.administrador=2 and ad_envoltura.id_empresa = " . $idEmpresa;
          break;
      default:
          $sql.="where ad_envoltura.id_sucursal = " . $idSucursal;
          break;
      }

    $sql.= " and ad_envoltura.activo = 1 "; 
    $sql.= " order by ad_envoltura.nombre";
    $sq= $this->db->query($sql);
    return $sq->result();
  }





// ********************************************************************
   
    public function obtModificar($id){
    $this->db->select('*');
    $this->db->where('id',$id);
    return $this->db->get('ad_envoltura')->row();
    }


  public function guardar_add($data){
    $this->db->insert('ad_envoltura', $data);
    return $this->db->affected_rows();
  }




  public function guardar_mod($id,$data){
    $this->db->where('id',$id);
    $this->db->update('ad_envoltura', $data);
    return $this->db->affected_rows();
  }


  public function desactivar($id,$data){
      $this->db->where('id',$id);
      $this->db->update('ad_envoltura', $data);
      return $this->db->affected_rows();
  }


   
    public function valNombre($nombre){

    $sql ="select * ";
    $sql.="from ad_envoltura ";
    $sql.="where nombre = '" . $nombre . "' ";
    $sql.=" and administrador = " . $this->session->userdata('administrador') . " ";
    $sql.=" and id_empresa = " . $this->session->userdata('idEmpresa') . " ";
    $sql.=" and id_sucursal = " . $this->session->userdata('idSucursal');
    $sq= $this->db->query($sql);
    $row = $sq->row();    
        $nro = $sq->num_rows(); 
        if($nro==1){
            return 1;
        }else{
          return 0;
        }  
    }



}
