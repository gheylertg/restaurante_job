<?php
class Ad_ingrediente_mdl extends CI_Model {

	
	public function __construct(){
        parent::__construct();
       
    }






  public function obt_dataTable(){
      $idEmpresa = $this->session->userdata('idEmpresa');
      $idSucursal = $this->session->userdata('idSucursal');

  	  $sql = "select ad_alimento.*, mt_unidad_medida.nombre as unidad_medida ";
	    $sql.= "from ad_alimento ";
      $sql.= "inner join mt_unidad_medida on ad_alimento.id_unidad_medida = mt_unidad_medida.id ";
      switch($this->session->userdata('administrador')){
      case 1:
          $sql.="where ad_alimento.administrador=1 ";
          break;
      case 2:
          $sql.="where ad_alimento.administrador=2 and ad_alimento.id_empresa = " . $idEmpresa;
          break;
      case 0:
          $sql.="where ad_alimento.administrador=0 and ad_alimento.id_sucursal = " . $idSucursal;
          break;
      }
    $sql.= " and ad_alimento.id_categoria_alimento = 2 ";  
	  $sql.= " order by ad_alimento.nombre";
	  $sq= $this->db->query($sql);
	  return $sq->result();
  }


   
    public function obtModificar($id){
		$this->db->select('*');
		$this->db->where('id',$id);
		return $this->db->get('ad_alimento')->row();
    }
    
	public function obtIngredienteSB($idIngrediente){
		$sql = "select ad_alimento.*, mt_unidad_medida.nombre as unidad_medida ";
	    $sql.= "from ad_alimento ";
		$sql.= "inner join mt_unidad_medida on ad_alimento.id_unidad_medida = mt_unidad_medida.id ";
		$sql.= "where ad_alimento.id=" . $idIngrediente;
		$sq= $this->db->query($sql);
		return $sq->row();
		
	}



/*

  public function obtSaborIngrediente($idSabor){
    $sql = "select ad_sabor_ingrediente.*, mt_unidad_medida.nombre as unidad_medida,  ";
    $sql.= "ad_ingrediente.nombre as ingrediente ";
    $sql.= "from ad_sabor_ingrediente ";
    $sql.= "inner join mt_unidad_medida on ad_sabor_ingrediente.id_unidad_medida = mt_unidad_medida.id ";
    $sql.= "inner join ad_ingrediente on ad_sabor_ingrediente.id_ingrediente = ad_ingrediente.id ";
    $sql.= "where ad_sabor_ingrediente.id_sabor=" . $idSabor;
    $sq= $this->db->query($sql);
    return $sq->result();
    
  }

  */



	    
    


  public function guardar_add($data){
    $this->db->insert('ad_alimento', $data);
    return $this->db->affected_rows();
  }




  public function guardar_mod($id,$data){
    $this->db->where('id',$id);
    $this->db->update('ad_alimento', $data);
    return $this->db->affected_rows();
  }


  public function desactivar($id,$data){
      $this->db->where('id',$id);
      $this->db->update('ad_alimento', $data);
      return $this->db->affected_rows();
  }


   
    public function valNombre($nombre){

		$sql ="select * ";
		$sql.="from ad_alimento ";
		$sql.="where nombre = '" . $nombre . "' ";
    $sql.=" and administrador = " . $this->session->userdata('administrador') . " ";
    $sql.=" and id_empresa = " . $this->session->userdata('idEmpresa') . " ";
    $sql.=" and id_sucursal = " . $this->session->userdata('idSucursal');
    $sql.=" and id_categoria_alimento = 2 ";

		$sq= $this->db->query($sql);
		$row = $sq->row();		
        $nro = $sq->num_rows(); 
        if($nro==1){
            return 1;
        }else{
        	return 0;
        }  
    }
}
