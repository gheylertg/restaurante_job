<?php
class Ad_mesa_mdl extends CI_Model {

	
	public function __construct(){
        parent::__construct();
       
    }

   
   

   
   public function obt_dataTable(){
        $idEmpresa = $this->session->userdata('idEmpresa');
        $idSucursal = $this->session->userdata('idSucursal');		
  		$sql = "select ad_mesa.*, ad_sala.nombre as sala  ";
 	    $sql.= "from ad_mesa  ";
		$sql.= "inner join ad_sala on ad_sala.id = ad_mesa.id_sala ";
      
		switch($this->session->userdata('administrador')){
        case 1:
            $sql.="where ad_mesa.administrador=1 ";
          break;

      case 2:
          $sql.="where ad_mesa.administrador=2 and ad_mesa.id_empresa = " . $idEmpresa;
          break;

      default:
          $sql.="where ad_mesa.id_sucursal = " . $idSucursal;
          break;
      }
	  $sql.= " order by ad_mesa.nombre ";
	  $sq= $this->db->query($sql);
	  return $sq->result();
   }


    public function obtProducto($id){
        $sql = "select ad_mesa.*, ad_sala.nombre as sala ";
        $sql.= "from ad_mesa ";
        $sql.= "inner join ad_mesa on ad_sala.id = ad_mesa.id_sala ";
        $sql.= "where ad_mesa.id=" .  $id;
        $sq= $this->db->query($sql);
        return $sq->row();
    }


  
    public function obtModificar($id){
		$this->db->select('*');
		$this->db->where('id',$id);
		return $this->db->get('ad_mesa')->row();

    }


  public function guardar_add($data){
      $this->db->insert('ad_mesa', $data);
      return $this->db->affected_rows();
  }


  public function guardar_add_id($data){
      $this->db->insert('ad_mesa', $data);
      return $this->db->insert_id();    
  }


  public function guardar_mod($id,$data){
      $this->db->where('id',$id);
      $this->db->update('ad_mesa', $data);
      return $this->db->affected_rows();
  }


  public function desactivar($id,$data){
      $this->db->where('id',$id);
      $this->db->update('ad_mesa', $data);
      return $this->db->affected_rows();
  }


   
    public function valNombre($nombre, $idSala){
	    $sql ="select * ";
		$sql.="from ad_mesa ";
		$sql.="where nombre = '" . $nombre . "' ";
		//$sql.=" and administrador = " . $this->session->userdata('administrador') . " ";
		//$sql.=" and id_empresa = " . $this->session->userdata('idEmpresa') . " ";    
		$sql.=" and id_sucursal = " . $this->session->userdata('idSucursal') . " ";        
		$sql.=" and id_sala = " . $idSala . " ";        

		$sq= $this->db->query($sql);
		$row = $sq->row();		
        $nro = $sq->num_rows(); 
        if($nro==1){
            return 1;
        }else{
        	return 0;
        }  
    }
    

}
