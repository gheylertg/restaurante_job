<?php
class Cf_configuracion_mdl extends CI_Model {

	
	public function __construct(){
        parent::__construct();
       
    }


  public function obt_dataTable(){
      $idEmpresa = $this->session->userdata('idEmpresa');
      $idSucursal = $this->session->userdata('idSucursal');

  	  $sql = "select * ";
	  $sql.= "from cf_configuracion ";
      switch($this->session->userdata('administrador')){
      case 1:
          $sql.="where administrador=1 ";
          break;
      case 2:
          $sql.="where administrador=2 and id_empresa = " . $idEmpresa;
          break;
      default:
          $sql.="where id_sucursal = " . $idSucursal;
          break;
      }
	  $sq= $this->db->query($sql);
	  return $sq->result();
  }


  public function guardar_add($data){
    $this->db->insert('cf_configuracion', $data);
    return $this->db->affected_rows();  
  }


   
    public function obtDatosAdministrador($identificador){
    		$this->db->select('*');
    		$this->db->where('identificador',$id);
        $this->db->where('administrador',1);
  		return $this->db->get('cf_configuracion')->row();
    }

  public function obtDatosEmpresa($identificador){
        $this->db->select('*');
        $this->db->where('identificador',$id);
        $this->db->where('id_empresa',$this->session->userdata('idEmpresa'));
        $this->db->where('administrador',2);
      return $this->db->get('cf_configuracion')->row();
    }

  public function obtDatosSucursal(){
        $idEmpresa = $this->session->userdata('idEmpresa');
        $idSucursal = $this->session->userdata('idSucursal'); 

        $this->db->select('*');
        $this->db->where('id_empresa',$this->session->userdata('idEmpresa'));
        $this->db->where('id_Sucursal',$this->session->userdata('idSucursal'));
        return $this->db->get('cf_configuracion')->row();
  }

}  //fin del modelo
  
