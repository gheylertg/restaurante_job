<?php
class Cf_empresa_mdl extends CI_Model {

	
	public function __construct(){
        parent::__construct();
       
    }

  public function obt_dataTable(){
  		$sql = "select * ";
		  $sql.= "from cf_empresa ";
		  $sql.= "where sw_administrador = 0 ";
		  $sql.= "order by nombre";
		  $sq= $this->db->query($sql);
		  return $sq->result();
  }



   
    public function obtModificar($id){
		$this->db->select('*');
		$this->db->where('id',$id);
		return $this->db->get('cf_empresa')->row();
    }


  public function guardar_add($data){
    $this->db->insert('cf_empresa', $data);
    return $this->db->insert_id();
  }



  public function guardar_mod($id,$data){
    $this->db->where('id',$id);
    $this->db->update('cf_empresa', $data);
    return $this->db->affected_rows();
  }


  public function desactivar($id,$data){
      $this->db->where('id',$id);
      $this->db->update('cf_empresa', $data);
      return $this->db->affected_rows();
  }


   
    public function valNombre($nombre){

		$sql ="select * ";
		$sql.="from cf_empresa ";
		$sql.="where nombre = '" . $nombre . "' ";

		$sq= $this->db->query($sql);
		$row = $sq->row();		
        $nro = $sq->num_rows(); 
        if($nro==1){
            return 1;
        }else{
        	return 0;
        }  
    }
}
