<?php
class Cf_sucursal_mdl extends CI_Model {

	
	public function __construct(){
        parent::__construct();
       
    }

  public function sucursalPrincipal_add($id_empresa){
    $data = array(
       'nombre'=>'PRINCIPAL',
       'id_empresa'=>$id_empresa,
       'id_create'=>$this->session->userdata('idUsuario')
    );

    $this->db->insert('cf_sucursal', $data);
    return $this->db->affected_rows();
  } 


  public function obt_dataTable(){
    $sql = "select su.*, em.nombre as empresa, re.nombre as region  ";
      $sql.= "from cf_sucursal su ";
      $sql.= "inner join cf_empresa em on em.id = su.id_empresa ";
      $sql.= "inner join cf_region re on re.id = su.id_region ";
      $sql.= "where su.sw_sucursal=1 and ";
      $sql.= "su.id_empresa=" . $this->session->userdata("idEmpresa");             
      $sql.= " order by su.nombre ";
      $sq= $this->db->query($sql);
      return $sq->result();
    }


    
    public function obtModificar($id){
      $this->db->select('*');
      $this->db->where('id',$id);
      return $this->db->get('cf_sucursal')->row();

    }


   public function obtVer($id){
      $sql = "select su.*, em.nombre as empresa, re.nombre as region  ";
      $sql.= "from cf_sucursal su ";
      $sql.= "inner join cf_empresa em on em.id = su.id_empresa ";
      $sql.= "inner join cf_region re on re.id = su.id_region ";
      $sql.= "where su.id = " . $id;
      $sq= $this->db->query($sql);
      return $sq->row();


    }

  public function guardar_add($data){
    $this->db->insert('cf_sucursal', $data);
    return $this->db->insert_id();
  }



  public function guardar_mod($id,$data){
    $this->db->where('id',$id);
    $this->db->update('cf_sucursal', $data);
    return $this->db->affected_rows();
  }


  public function desactivar($id,$data){
      $this->db->where('id',$id);
      $this->db->update('cf_sucursal', $data);
      return $this->db->affected_rows();
  }


   
    public function valNombre($nombre, $idEmpresa){

      $sql ="select * ";
      $sql.="from cf_sucursal ";
      $sql.="where nombre = '" . $nombre . "' and id_empresa = " . $idEmpresa;
      $sq= $this->db->query($sql);
      $row = $sq->row();    

      $nro = $sq->num_rows(); 
      if($nro==1){
          return 1;
      }else{
          return 0;
      }  

    }
    
   public function sucursalAdministradorEmpresa($idEmpresa){
      $sql = "select su.* ";
      $sql.= "from cf_sucursal su ";
      $sql.= "where su.sw_sucursal=0 and su.activo=1 and su.id_empresa = " . $idEmpresa;

     // die($sql);

      $sq= $this->db->query($sql);
      return $sq->row();


    }

    
    

}
