<?php
class Cj_aperturaCaja_mdl extends CI_Model {

	
	public function __construct(){
        parent::__construct();
       
    }



//Validar apertura de caja.
 public function val_apertura($idusuario, $idEmpresa, $idSucursal){
    $sql = "select * ";
    $sql.= "from cj_caja ";
    $sql.= "where id_empresa = " . $idEmpresa;
    $sql.= " and id_sucursal = " . $idSucursal;
    $sql.= " and id_usuario = " . $idusuario;
    $sql.= " and estado = 1";
    $sq= $this->db->query($sql);
    $nro = $sq->num_rows(); 
    if($nro>0){
        return 1;
    }else{
      return 0;
    }    

 }


 public function aperturarCaja($data){
    $this->db->insert('cj_caja', $data);
    return $this->db->insert_id();
 }



  public function obt_datosApertura($idusuario, $idEmpresa, $idSucursal){
    $sql = "select * ";
    $sql.= "from cj_caja ";
    $sql.= "where id_empresa = " . $idEmpresa;
    $sql.= " and id_sucursal = " . $idSucursal;
    $sql.= " and id_usuario = " . $idusuario;
    $sql.= " and estado = 1";
    $sq= $this->db->query($sql);

    $nro = $sq->num_rows(); 

     if($nro>0){
       return $sq->row();
    }else{
       return false;
    }
 }   


 public function obt_datosCaja($idCaja){
    $sql = "select * ";
    $sql.= "from cj_caja ";
    $sql.= "where id = " . $idCaja;
    $sq= $this->db->query($sql);
    return $sq->row();
    }
  


}
