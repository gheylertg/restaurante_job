<?php
class Cj_aperturaCaja_mdl extends CI_Model {

	
	public function __construct(){
        parent::__construct();
       
    }


//Validar apertura de caja.
 public function val_apertura($idusuario, $idEmpresa, $idSucursal){
    $sql = "select * ";
    $sql.= "from cj_caja ";
    $sql.= "where id_empresa = " . $idEmpresa;
    $sql.= " and id_sucursal = " . $idSucursal;
    $sql.= " and id_usuario = " . $idusuario;
    $sql.= " and estado = 1";
    $sq= $this->db->query($sql);

    $nro = $sq->num_rows(); 
    if($nro>0){
        return 1;
    }else{
      return 0;
    }    

 }


 public function aperturarCaja($data){
    $this->db->insert('cj_caja', $data);
    return $this->db->insert_id();
 }



  public function obt_datosApertura($idusuario, $idEmpresa, $idSucursal){
    $sql = "select * ";
    $sql.= "from cj_caja ";
    $sql.= "where id_empresa = " . $idEmpresa;
    $sql.= " and id_sucursal = " . $idSucursal;
    $sql.= " and id_usuario = " . $idusuario;
    $sql.= " and estado = 1";
    $sq= $this->db->query($sql);
    return $sq->row();    

  }





//************************************
  public function obt_dataTable(){
  		$sql = "select * ";
		  $sql.= "from cj_caja ";
		  $sql.= "order by nombre";
		  $sq= $this->db->query($sql);
		  return $sq->result();
  }


   
    public function obtModificar($id){
		$this->db->select('*');
		$this->db->where('id',$id);
		return $this->db->get('cf_region')->row();
    }


  public function guardar_add($data){
    $this->db->insert('cf_region', $data);
    return $this->db->affected_rows();
  }



  public function guardar_mod($id,$data){
    $this->db->where('id',$id);
    $this->db->update('cf_region', $data);
    return $this->db->affected_rows();
  }


  public function desactivar($id,$data){
      $this->db->where('id',$id);
      $this->db->update('cf_region', $data);
      return $this->db->affected_rows();
  }


   
    public function valNombre($nombre){

		$sql ="select * ";
		$sql.="from cf_region ";
		$sql.="where nombre = '" . $nombre . "' ";
		$sq= $this->db->query($sql);
		$row = $sq->row();		
        $nro = $sq->num_rows(); 
        if($nro==1){
            return 1;
        }else{
        	return 0;
        }  
    }
}
