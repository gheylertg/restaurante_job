<?php
class Cj_cierreCaja_mdl extends CI_Model {

	
	public function __construct(){
        parent::__construct();
       
  }


  public function obt_dataTable(){

  		$sql = "select cj_caja.*, sg_usuario.nombre, sg_usuario.apellido,  ";
      $sql.= "(select sum(monto) as ingreso from cj_movimiento_caja where ";
      $sql.= "id_tipo_operacion=1 and reverso=0 and activo=1 and id_caja = cj_caja.id) as ingreso, ";
      $sql.= "(select sum(monto) as ingreso from cj_movimiento_caja where ";
      $sql.= "id_tipo_operacion=2 and reverso=0 and activo=1 and id_caja = cj_caja.id) as egreso ";
		  $sql.= "from cj_caja ";
      $sql.= "inner join sg_usuario on cj_caja.id_usuario = sg_usuario.id ";
      $sql.= "where cj_caja.estado=1 and cj_caja.id_empresa =" . $this->session->userdata('idEmpresa');
      $sql.= " and cj_caja.id_sucursal =" . $this->session->userdata('idSucursal');
      $sql.= " order by fecha desc ";

		  $sq= $this->db->query($sql);
		  return $sq->result();
  }


  public function obt_cajaCierre($idUsuario){
      $sql = "select cj_caja.* ";
      $sql.= "from cj_caja ";
      $sql.= "where cj_caja.estado=1 and cj_caja.id_empresa =" . $this->session->userdata('idEmpresa');
      $sql.= " and cj_caja.id_sucursal =" . $this->session->userdata('idSucursal');
      $sql.= " and cj_caja.id_usuario =" . $idUsuario;
      $sq= $this->db->query($sql);
      $row = $sq->row();    
      $nro = $sq->num_rows(); 
      if($nro>0){
        return 1;
      }else{
        return 0;
      }  
  }


  public function obt_cajaCierreGlobal(){
      $sql = "select cj_caja.* ";
      $sql.= "from cj_caja ";
      $sql.= "where cj_caja.estado=1 and cj_caja.id_empresa =" . $this->session->userdata('idEmpresa');
      $sql.= " and cj_caja.id_sucursal =" . $this->session->userdata('idSucursal');
      $sq= $this->db->query($sql);
      $row = $sq->row();    
      $nro = $sq->num_rows(); 
      if($nro>0){
        return 1;
      }else{
        return 0;
      }  
  }



  function guardarCierre($id,$data){
      $this->db->where('id',$id);
      $this->db->update('cj_caja', $data);
     return $this->db->affected_rows();
  }



  public function arqueoMovimiento($idCaja){

    $sql = "select * ";
    $sql.="from cj_movimiento_caja ";
    $sql.="where reverso=0 and id_caja =" .$idCaja;
     $sq= $this->db->query($sql);
     return $sq->result();
  }  




//////////////////////////////////////////////////////////////////////////////////////////////////

  public function guardar_add($data){
    $this->db->insert('cj_movimiento_caja', $data);
    return $this->db->affected_rows();
  }

    public function obtModificar($id){
      $this->db->select('*');
      $this->db->where('id',$id);
      return $this->db->get('cj_movimiento_caja')->row();
    }


  public function guardar_mod($id,$data){
    $this->db->where('id',$id);
    $this->db->update('cj_movimiento_caja', $data);
    return $this->db->affected_rows();
  }


  public function obt_ver($id){
      $sql = "select mc.*, tm.nombre as movimiento,  td.nombre as dinero ";
      $sql.= "from cj_movimiento_caja mc ";
      $sql.= "inner join mt_tipo_movimiento_caja tm on mc.id_tipo_movimiento = tm.id ";
      $sql.= "inner join mt_tipo_dinero td on mc.id_tipo_dinero = td.id ";
      $sql.= "where mc.id =  " . $id;
      $sql.= " order by mc.fecha desc ";
      $sq= $this->db->query($sql);
      return $sq->row();
  }

/************************* */

   










  public function desactivar($id,$data){
      $this->db->where('id',$id);
      $this->db->update('cf_region', $data);
      return $this->db->affected_rows();
  }


   
    public function valNombre($nombre){

		$sql ="select * ";
		$sql.="from cf_region ";
		$sql.="where nombre = '" . $nombre . "' ";
		$sq= $this->db->query($sql);
		$row = $sq->row();		
        $nro = $sq->num_rows(); 
        if($nro==1){
            return 1;
        }else{
        	return 0;
        }  
    }
}
