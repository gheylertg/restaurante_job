<?php
class Cj_cobroCaja_mdl extends CI_Model {

	
	public function __construct(){
        parent::__construct();
       
  }


  public function guardarMovimiento($data){
    $this->db->insert('cj_movimiento_caja', $data);
    return $this->db->affected_rows();
  }



  public function obt_dataTable(){
  		$sql = "select md.*, sa.nombre as sala, me.nombre as mesa, us.nombre as usuario, us.apellido ";
		  $sql.= "from dp_mesa_despacho md ";
      $sql.= "inner join ad_sala sa on md.id_sala = sa.id ";
      $sql.= "inner join ad_mesa me on md.id_mesa = dp.id ";
      $sql.= "inner join sg_usuario us on md.id_mesonero = us.id ";
      $sql.= "where md.id_empresa = " . $this->session->userdata('idEmpresa');
      $sql.= " and md.id_sucursal = " . $this->session->userdata('idSucursal');
      $sql.= " and md.activo = 1 ";
      $sql.= " order by sa.nombre, me.nombre ";
		  $sq= $this->db->query($sql);
		  return $sq->result();
  }


  public function guardarCobro($data){
    $this->db->insert('cj_cobrar', $data);
    return $this->db->insert_id();
  }

 public function guardarCobroDinero($data){
    $this->db->insert('cj_cobrar_dinero', $data);
  }


  public function obtCobro($idCaja){
     $sql = "select sum(cj_cobrar.total_cobrar) as total_cobrar ";
     $sql.= "from  cj_cobrar ";
     $sql.= "where cj_cobrar.id_caja = " . $idCaja;
     $sq= $this->db->query($sql);
     return $sq->row();    
  }
  
  
  public function obtCobrosDetallados($idCaja){
     //$sql = "select cj_cobrar.*, vt_venta.documento, dm_tipo_despacho.nombre as despacho ";
     $sql = "select cj_cobrar.*, dm_tipo_despacho.nombre as despacho ";
     $sql.= "from  cj_cobrar ";
     $sql.= "inner join dm_tipo_despacho on cj_cobrar.id_tipo_despacho = dm_tipo_despacho.id ";
     $sql.= "where cj_cobrar.reverso=0 and cj_cobrar.id_caja = " . $idCaja;
     $sql.= " order by cj_cobrar.id ";
     $sq= $this->db->query($sql);
     return $sq->result();
  }


  public function obtCobroInd($idCobro){
     $sql = "select cj_cobrar.* ";
     $sql.= "from  cj_cobrar ";
     $sql.= "where cj_cobrar.id = " . $idCobro;
     $sq= $this->db->query($sql);
     return $sq->row();
  }


  public function obtArqueoCobro($idCaja){
     $sql = "select cj_cobrar_dinero.* ";
     $sql.= "from  cj_cobrar_dinero ";
     $sql.= "where cj_cobrar_dinero.id_caja = " . $idCaja;
     $sq= $this->db->query($sql);
     return $sq->result();
  }



}
