<?php
class Cj_movimientoCaja_mdl extends CI_Model {

	
	public function __construct(){
        parent::__construct();
       
  }


  public function guardarMovimiento($data){
    $this->db->insert('cj_movimiento_caja', $data);
    return $this->db->affected_rows();
  }


  public function obt_dataTable(){
  		$sql = "select mc.*, tm.nombre as movimiento ";
		  $sql.= "from cj_movimiento_caja mc ";
      $sql.= "inner join mt_tipo_movimiento_caja tm on mc.id_tipo_movimiento = tm.id ";
      $sql.= "where mc.id_caja =  " . $this->session->userdata('idCaja');
      $sql.= " order by mc.fecha desc ";
		  $sq= $this->db->query($sql);
		  return $sq->result();
  }


  public function guardar_add($data){
    $this->db->insert('cj_movimiento_caja', $data);
    return $this->db->affected_rows();
  }

    public function obtModificar($id){
      $this->db->select('*');
      $this->db->where('id',$id);
      return $this->db->get('cj_movimiento_caja')->row();
    }


  public function guardar_mod($id,$data){
    $this->db->where('id',$id);
    $this->db->update('cj_movimiento_caja', $data);
    return $this->db->affected_rows();
  }


  public function obt_ver($id){
      $sql = "select mc.*, tm.nombre as movimiento ";
      $sql.= "from cj_movimiento_caja mc ";
      $sql.= "inner join mt_tipo_movimiento_caja tm on mc.id_tipo_movimiento = tm.id ";
      $sql.= "where mc.id =  " . $id;
      $sql.= " order by mc.fecha desc ";
      $sq= $this->db->query($sql);
      return $sq->row();
  }
  
  
  public function obt_totalesMovimiento($idCaja){
      $sql = "select (select mc1.monto from cj_movimiento_caja mc1 where mc1.id_tipo_movimiento=1 and ";
      $sql.= "mc1.reverso=0 and mc1.id_caja = " . $idCaja . ") as apertura, ";
	  $sql.= "(select sum(mc2.monto) from cj_movimiento_caja mc2 where mc2.id_tipo_movimiento<>1 and ";
      $sql.= "mc2.reverso=0 and mc2.id_tipo_operacion=1 and mc2.id_caja = " . $idCaja . ") as ingreso, ";      
	  $sql.= "(select sum(mc3.monto) from cj_movimiento_caja mc3 where ";
      $sql.= "mc3.reverso=0 and mc3.id_tipo_operacion=2 and mc3.id_caja = " . $idCaja . ") as egreso ";      
      $sql.= "from  cj_movimiento_caja";      
      $sq= $this->db->query($sql);
      return $sq->row();
  }
  

  public function obt_ingreso($idCaja, $tipo){
      $sql = "select mc.*, tm.nombre as movimiento ";
      $sql.= "from cj_movimiento_caja mc ";
      $sql.= "inner join mt_tipo_movimiento_caja tm on mc.id_tipo_movimiento = tm.id ";
      $sql.= "where mc.id_tipo_operacion=".$tipo. " and mc.id_caja =  " . $idCaja;
      $sql.= " and mc.reverso=0 order by mc.id_tipo_movimiento ";
      $sq= $this->db->query($sql);
      return $sq->result();
  }

  
  

/************************* */

}
