<?php
class Co_contenedor_mdl extends CI_Model {
	public function __construct(){
        parent::__construct();
       
    }

   
   public function obt_dataTable(){
      $idEmpresa = $this->session->userdata('idEmpresa');
      $idSucursal = $this->session->userdata('idSucursal');		
  	  $sql = "select co.*, ali.nombre as sabor ";
 	  $sql.= "from co_contenedor co ";
      $sql.= "inner join mn_alimento ali on ali.id = co.id_alimento ";
      $sql.= "where co.id_empresa = " . $idEmpresa;
      $sql.= " and co.id_sucursal = " . $idSucursal;
      $sql.= " order by ali.nombre";
  	  $sq= $this->db->query($sql);
	    return $sq->result();
   }
   
   public function resetearContenedor($id, $data){
      $this->db->where('id',$id);
      $this->db->update('co_contenedor', $data);
      return true;
   }   
   
   public function obtModificar($id){
    $this->db->select('*');
    $this->db->where('id',$id);
    return $this->db->get('co_contenedor')->row();
    }   
   
   public function obt_alimentoContenedor($id){
    $this->db->select('*');
    $this->db->where('id_alimento',$id);
    return $this->db->get('co_contenedor')->row();
    }   


  public function guardar_mod($id,$data){
      $this->db->where('id',$id);
      $this->db->update('co_contenedor', $data);
      return $this->db->affected_rows();
  } 
  
  public function guardarContenedor($data){
	  $this->db->insert('co_contenedor', $data);
	  
  }
  
  public function act_contenedor($id,$data){
        $this->db->where('id_alimento',$id);
      $this->db->update('co_contenedor', $data);
      return $this->db->affected_rows();
  } 
  
  
  public function val_existenciaContenedor($idAlimento){
	$sql = "select * ";
	$sql.= "from co_contenedor ";
	$sql.= "where id_alimento = " . $idAlimento;
	$sq= $this->db->query($sql);
	$nro = $sq->num_rows(); 
	if($nro>0){
	   return 1;
    }else{
	   return 0;
	}        
  }
  
  public function borrarContenedor($id){
      $this->db->where('id_alimento',$id);
      $this->db->delete('co_contenedor');
      return $this->db->affected_rows();
  }   
  
    
   public function obt_alimento_contenedor_reversar($idAlimento){
      $idEmpresa = $this->session->userdata('idEmpresa');
      $idSucursal = $this->session->userdata('idSucursal');		

		$this->db->select('*');
		$this->db->where('id_alimento',$idAlimento);
		$this->db->where('id_empresa',$idEmpresa);
		$this->db->where('id_sucursal',$idSucursal);
		return $this->db->get('co_contenedor')->row();
   }

   
   

}
