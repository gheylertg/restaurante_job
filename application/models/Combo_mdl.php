<?php
class Combo_mdl extends CI_Model {

	
	public function __construct(){
        parent::__construct();
       
    }

	public function combo_modulo(){
 		$this->db->select('*');
		$this->db->where('activo',1);
		$this->db->order_by('nombre');
		$row = $this->db->get('sg_modulo')->result();
		return $row;
   }
   
   
	function combo_rolModulo($idRol){
     $sql = "select  m.* ";
     $sql.= "from sg_modulo m ";
     $sql.= "where m.id not in(select rm.id_modulo from sg_rol_modulo rm where rm.id_rol=" . $idRol .") ";
     $sql.= "and m.activo = 1 ";
     $sql.= " order by m.nombre ";
          
     
     
     $sq= $this->db->query($sql);
     return $sq->result();		
   }

	function combo_rol(){
     $sql = "select  * ";
     $sql.= "from sg_rol ";
     $sql.= "where sg_rol.activo = 1 ";
     $sql.= " order by sg_rol.nombre ";
          
     
     
     $sq= $this->db->query($sql);
     return $sq->result();		
   }


	function combo_empresaUsuario($idUsuario){
     $sql = "select cf_empresa.* ";
     $sql.= "from cf_empresa ";
     $sql.= "where cf_empresa.id not in(select ue.id_empresa from sg_usuario_empresa ue where ue.id_usuario=" . $idUsuario .") ";
     $sql.= "and cf_empresa.activo = 1 ";
     //$sql.= "where cf_empresa.activo = 1 ";
     $sql.= " order by cf_empresa.nombre ";
     
     
     $sq= $this->db->query($sql);
     return $sq->result();		
   }

	function combo_moduloUsuario($idUsuarioEmpresa){
     $sql = "select  sg_modulo.* ";
     $sql.= "from sg_modulo ";
     $sql.= "where sg_modulo.id not in(select uem.id_modulo from sg_usuario_empresa_modulo uem where uem.id_usuario_empresa=" . $idUsuarioEmpresa .") ";
     $sql.= "and sg_modulo.activo = 1 ";
     $sql.= " order by sg_modulo.nombre ";
     $sq= $this->db->query($sql);
     return $sq->result();		
   }
   
	public function combo_categoriaProducto(){
		$sql = "select * from mt_categoria_producto ";
        switch($this->session->userdata('administrador')){
		case 1:
		    $sql.= "where administrador = 1 ";
		    break;
		case 2:    
		    $sql.= "where administrador = 1 or (administrador = 2 and id_empresa=" . $this->session->userdata('idEmpresa').")";
		    break;
		default:
		    $sql.= "where administrador = 1 or (administrador = 2 and id_empresa=" . $this->session->userdata('idEmpresa'). ") or ";
		    $sql.= " id_sucursal =" . $this->session->userdata('idSucursal');
		    break;
		    
		} 		
		$sql.= " and activo = 1 order by nombre";
		$sq= $this->db->query($sql);
		return $sq->result();		
   }

	public function combo_unidadMedida(){
		$sql = "select * from mt_unidad_medida ";
        switch($this->session->userdata('administrador')){
		case 1:
		    $sql.= "where administrador = 1 ";
		    break;
		case 2:    
		    $sql.= "where administrador = 1 or (administrador = 2 and id_empresa=" . $this->session->userdata('idEmpresa').")";
		    break;
		default:
		    $sql.= "where administrador = 1 or (administrador = 2 and id_empresa=" . $this->session->userdata('idEmpresa'). ") or ";
		    $sql.= " id_sucursal =" . $this->session->userdata('idSucursal');
		    break;
		    
		} 		
		$sql.= " and activo = 1 order by nombre";
		$sq= $this->db->query($sql);
		return $sq->result();		
   }
   
	public function combo_unidadMedida_presentacion(){
		$sql = "select * from mt_unidad_medida ";
        switch($this->session->userdata('administrador')){
		case 1:
		    $sql.= "where administrador = 1 ";
		    break;
		case 2:    
		    $sql.= "where (administrador = 1 or (administrador = 2 and id_empresa=" . $this->session->userdata('idEmpresa')."))";
		    break;
		default:
		    $sql.= "where (administrador = 1 or (administrador = 2 and id_empresa=" . $this->session->userdata('idEmpresa'). ") or ";
		    $sql.= " id_sucursal =" . $this->session->userdata('idSucursal').")";
		    break;
		    
		} 		
		$sql.= " and activo = 1 and presentacion = 1 order by nombre";
		
		$sq= $this->db->query($sql);
		return $sq->result();		
   }   
   
   
	public function combo_empresa(){
		$sql = "select * from cf_empresa ";
        switch($this->session->userdata('administrador')){
		case 1:
		    $sql.= "where administrador = 1 ";
		    break;
		case 2:    
		    $sql.= "where id=" . $this->session->userdata('idEmpresa');
		    break;
		default:
		    $sql.= "where id_empresa=" . $this->session->userdata('idEmpresa');
		    break;
		} 		

		
		$sql.= " and activo = 1 order by nombre";
		$sq= $this->db->query($sql);
		return $sq->result();		
   }   

   
	public function combo_region(){
		$sql = "select * from cf_region ";
        switch($this->session->userdata('administrador')){
		case 1:
		    $sql.= "where administrador = 1 ";
		    break;
		case 2:    
		    $sql.= "where administrador = 1 or id_empresa = " . $this->session->userdata('idEmpresa');
		    break;
		default:
		    $sql.= "where administrador = 1 or id_empresa=" . $this->session->userdata('idEmpresa');
		    break;
		    
		} 		
		$sql.= " and activo = 1 order by nombre";
		$sq= $this->db->query($sql);
		return $sq->result();		
   }
   
   
   
    // Combo municipio
    public function combo_sucursalUsuario($id_empresa, $idUsuario){
     $sql = "select su.* ";
     $sql.= "from cf_sucursal su ";
     $sql.= "where su.id not in(select ue.id_sucursal from sg_usuario_empresa ue where ue.id_usuario=" . $idUsuario ." and ";
     $sql.= "ue.id_empresa = " . $id_empresa . ") ";
     $sql.= "and su.activo = 1 and su.sw_sucursal=1 and su.id_empresa = " . $id_empresa;
     $sql.= " order by su.nombre ";
     
     //die($sql);
     $sq= $this->db->query($sql);
     return $sq->result();		
    }   
    
    //concepti proveedor inventario
	public function combo_proveedorIn(){
		$sql = "select * from mt_proveedor ";
        switch($this->session->userdata('administrador')){
		case 1:
		    $sql.= "where administrador = 1 ";
		    break;
		case 2:    
		    $sql.= "where administrador = 1 or (administrador = 2 and id_empresa=" . $this->session->userdata('idEmpresa').")";
		    break;
		default:
		    $sql.= "where administrador = 1 or (administrador = 2 and id_empresa=" . $this->session->userdata('idEmpresa'). ") or ";
		    $sql.= " id_sucursal =" . $this->session->userdata('idSucursal');
		    break;
		    
		} 		
		$sql.= " and activo = 1 order by nombre";
		$sq= $this->db->query($sql);
		return $sq->result();		

   }    
   
    //concepti tipo operacion 
	public function combo_conceptoIn($tipo){
		$sql = "select * from in_concepto_inventario ";
        switch($this->session->userdata('administrador')){
		case 1:
		    $sql.= "where administrador = 1 ";
		    break;
		case 2:    
		    $sql.= "where administrador = 1 or (administrador = 2 and id_empresa=" . $this->session->userdata('idEmpresa').")";
		    break;
		default:
		    $sql.= "where administrador = 1 or (administrador = 2 and id_empresa=" . $this->session->userdata('idEmpresa'). ") or ";
		    $sql.= " id_sucursal =" . $this->session->userdata('idSucursal');
		    break;
		    
		} 		
		$sql.= " and activo = 1 and id > 3 order by nombre";
		$sq= $this->db->query($sql);
		return $sq->result();		


   }    


    //Producto inventario 
	public function combo_productoIn(){
		$sql = "select * from mt_producto ";
        switch($this->session->userdata('administrador')){
		case 1:
		    $sql.= "where administrador = 1 ";
		    break;
		case 2:    
		    $sql.= "where administrador = 1 or (administrador = 2 and id_empresa=" . $this->session->userdata('idEmpresa').")";
		    break;
		default:
		    $sql.= "where administrador = 1 or (administrador = 2 and id_empresa=" . $this->session->userdata('idEmpresa'). ") or ";
		    $sql.= " id_sucursal =" . $this->session->userdata('idSucursal');
		    break;
		    
		} 		
		$sql.= " and activo = 1 order by nombre";
		$sq= $this->db->query($sql);
		return $sq->result();		
   } 
   
	function combo_tipoProducto(){
		$sql = "select * from mt_tipo_producto ";
        switch($this->session->userdata('administrador')){
		case 1:
		    $sql.= "where administrador = 1 ";
		    break;
		case 2:    
		    $sql.= "where administrador = 1 or (administrador = 2 and id_empresa=" . $this->session->userdata('idEmpresa').")";
		    break;
		default:
		    $sql.= "where administrador = 1 or (administrador = 2 and id_empresa=" . $this->session->userdata('idEmpresa'). ") or ";
		    $sql.= " id_sucursal =" . $this->session->userdata('idSucursal');
		    break;
		    
		} 		
		$sql.= " and activo = 1 order by nombre";
		$sq= $this->db->query($sql);
		return $sq->result();		
   }   
   
   
   
	function combo_proveedorCp(){
    //proveedor compra
		$sql = "select * from mt_proveedor ";
        switch($this->session->userdata('administrador')){
		case 1:
		    $sql.= "where administrador = 1 ";
		    break;
		case 2:    
		    $sql.= "where administrador = 1 or (administrador = 2 and id_empresa=" . $this->session->userdata('idEmpresa').")";
		    break;
		default:
		    $sql.= "where administrador = 1 or (administrador = 2 and id_empresa=" . $this->session->userdata('idEmpresa'). ") or ";
		    $sql.= " id_sucursal =" . $this->session->userdata('idSucursal');
		    break;
		    
		} 		
		$sql.= " and activo = 1 order by nombre";
		$sq= $this->db->query($sql);
		return $sq->result();		
   }
   
    //Producto compra
/*	public function combo_productoCp(){
    //producto compra
		$sql = "select * from mt_producto ";
        switch($this->session->userdata('administrador')){
		case 0:
		    $sql.= "where administrador = 1 or (administrador = 2 and id_empresa=" . $this->session->userdata('idEmpresa'). ") or ";
		    $sql.= " id_sucursal =" . $this->session->userdata('idSucursal');
		    break;
		case 1:
		    $sql.= "where administrador = 1 ";
		    break;
		case 2:    
		    $sql.= "where administrador = 2 and id_empresa=" . $this->session->userdata('idEmpresa');
		    break;
		} 		
		$sql.= " order by nombre";
		$sq= $this->db->query($sql);
		return $sq->result();		
   }
    
   */ 
   
   
   
   
   
   
    //Producto SALA
	public function combo_sala(){
    //producto compra
		$sql = "select * from ad_sala ";
        switch($this->session->userdata('administrador')){
		case 1:
		    $sql.= "where administrador = 1 ";
		    break;
		case 2:    
		    $sql.= "where administrador = 2 and id_empresa=" . $this->session->userdata('idEmpresa');
		    break;
		default:
		    $sql.= "where administrador = 1 or (administrador = 2 and id_empresa=" . $this->session->userdata('idEmpresa'). ") or ";
		    $sql.= " id_sucursal =" . $this->session->userdata('idSucursal');
		    break;
		    
		} 		
		$sql.= " order by nombre";
		$sq= $this->db->query($sql);
		return $sq->result();		
   }
   

   public function combo_anid_mesa($id_sala){
	 $inSelect = "select id_mesa from dp_despacho where id_sucursal = " . $this->session->userdata('idSucursal');
	 $inSelect.= " and id_estado_despacho=1";
     $sql = "select ad_mesa.* ";
     $sql.= "from ad_mesa ";
     $sql.= "where ad_mesa.id_sala = " . $id_sala;
     $sql.= " and ad_mesa.activo = 1 and ";
     $sql.= " ad_mesa.id not in (" . $inSelect .") ";
     $sql.= " order by ad_mesa.nombre ";
     $sq= $this->db->query($sql);
     return $sq->result();		
    }   
    
    //Producto mesonero
	public function combo_mesonero(){
    //producto compra
		$sql = "select * from ad_mesonero ";
        switch($this->session->userdata('administrador')){
		case 1:
		    $sql.= "where administrador = 1 ";
		    break;
		case 2:    
		    $sql.= "where administrador = 2 and id_empresa=" . $this->session->userdata('idEmpresa');
		    break;
		default:
		    $sql.= "where administrador = 1 or (administrador = 2 and id_empresa=" . $this->session->userdata('idEmpresa'). ") or ";
		    $sql.= " id_sucursal =" . $this->session->userdata('idSucursal');
		    break;
		    
		} 		
		$sql.= " order by nombre";
		$sq= $this->db->query($sql);
		return $sq->result();		
   }


  
   
   public function combo_anid_tipoMovimiento($id_tipoOperacion){
     $sql = "select tm.* ";
     $sql.= "from mt_tipo_movimiento_caja tm ";
     $sql.= "where tm.id_tipo_operacion = " . $id_tipoOperacion;
     $sql.= " and tm.activo = 1 and tm.id<>1 ";
     $sql.= " order by tm.nombre ";
     $sq= $this->db->query($sql);
     return $sq->result();		
    }   
    
    
	public function combo_tipoDinero(){
 		$this->db->select('*');
		$this->db->where('activo',1);
		$this->db->order_by('nombre');
		$row = $this->db->get('mt_tipo_dinero')->result();
		return $row;
   }    

    //Producto inventario  ultimo
	public function combo_productoInventario(){
		$idEmpresa = $this->session->userdata('idEmpresa');
		$idSucursal = $this->session->userdata('idSucursal');
		
		$sql = "select * from mt_producto p ";
		$sql.= "where ";
        switch($this->session->userdata('administrador')){
		case 1:
		    $sql.= " p.administrador = 1 ";
		    break;
		case 2:    
		    $sql.= " p.administrador = 1 or (p.administrador = 2 and p.id_empresa=" . $this->session->userdata('idEmpresa').")";
		    break;
		default:
		    $sql.= "p.administrador = 1 or (p.administrador = 2 and p.id_empresa=" . $this->session->userdata('idEmpresa'). ") or ";
		    $sql.= " p.id_sucursal =" . $this->session->userdata('idSucursal');
		    break;
		    
		} 		
		
		$sql.=" and p.id not in (select inv.id_producto from in_inventario inv where inv.activo = 1 and ";
		$sql.= "inv.id_empresa = " . $idEmpresa . " and inv.id_sucursal = " . $idSucursal . ") ";
		
		$sql.= " and p.activo = 1 order by p.nombre";
		
	
		$sq= $this->db->query($sql);
		return $sq->result();		
   } 

    //Producto inventario  ultimo
	public function combo_productoInEn(){
		$idEmpresa = $this->session->userdata('idEmpresa');
		$idSucursal = $this->session->userdata('idSucursal');
		
		$sql = "select * from mt_producto p ";
		$sql.= "where ";
        switch($this->session->userdata('administrador')){
		case 1:
		    $sql.= " p.administrador = 1 ";
		    break;
		case 2:    
		    $sql.= " p.administrador = 1 or (p.administrador = 2 and p.id_empresa=" . $this->session->userdata('idEmpresa').")";
		    break;
		default:
		    $sql.= "p.administrador = 1 or (p.administrador = 2 and p.id_empresa=" . $this->session->userdata('idEmpresa'). ") or ";
		    $sql.= " p.id_sucursal =" . $this->session->userdata('idSucursal');
		    break;
		    
		} 		
		
		$sql.=" and p.id in (select inv.id_producto from in_inventario inv where inv.activo = 1 and ";
		$sql.= "inv.id_empresa = " . $idEmpresa . " and inv.id_sucursal = " . $idSucursal . ") ";
		
		$sql.= " and p.activo = 1 order by p.nombre";
		
	
		$sq= $this->db->query($sql);
		return $sq->result();		
   } 


    //concepti tipo operacion 
	public function combo_conceptoInEn(){
		$sql = "select * from in_concepto_inventario ";
		$sql.= "where activo = 1 and id > 3 and id_tipo_operacion = 1 and (";
		
        switch($this->session->userdata('administrador')){
		case 1:
		    $sql.= " administrador = 1 ";
		    break;
		case 2:    
		    $sql.= " administrador = 1 or (administrador = 2 and id_empresa=" . $this->session->userdata('idEmpresa').")";
		    break;
		default:
		    $sql.= " administrador = 1 or (administrador = 2 and id_empresa=" . $this->session->userdata('idEmpresa'). ") or ";
		    $sql.= " id_sucursal =" . $this->session->userdata('idSucursal');
		    break;
		    
		} 		
		$sql.= "  ) order by nombre";

		
		$sq= $this->db->query($sql);
		return $sq->result();		


   }    
   
   
    //concepti tipo operacion 
	public function combo_conceptoInSa(){
		$sql = "select * from in_concepto_inventario ";
		$sql.= "where activo = 1 and id > 3 and id_tipo_operacion = 2 and (";
		
        switch($this->session->userdata('administrador')){
		case 1:
		    $sql.= " administrador = 1 ";
		    break;
		case 2:    
		    $sql.= " administrador = 1 or (administrador = 2 and id_empresa=" . $this->session->userdata('idEmpresa').")";
		    break;
		default:
		    $sql.= " administrador = 1 or (administrador = 2 and id_empresa=" . $this->session->userdata('idEmpresa'). ") or ";
		    $sql.= " id_sucursal =" . $this->session->userdata('idSucursal');
		    break;
		    
		} 		
		$sql.= "  ) order by nombre";

		
		$sq= $this->db->query($sql);
		return $sq->result();		
   }   


	public function combo_ingrediente(){
		$sql = "select * from mn_ingrediente ";
        switch($this->session->userdata('administrador')){
		case 1:
		    $sql.= "where administrador = 1 ";
		    break;
		case 2:    
		    $sql.= "where administrador = 1 or (administrador = 2 and id_empresa=" . $this->session->userdata('idEmpresa').")";
		    break;
		default:
		    $sql.= "where administrador = 1 or (administrador = 2 and id_empresa=" . $this->session->userdata('idEmpresa'). ") or ";
		    $sql.= " id_sucursal =" . $this->session->userdata('idSucursal');
		    break;
		} 		
		$sql.= " and activo = 1 order by nombre";
		$sq= $this->db->query($sql);
		return $sq->result();		
   }

	public function combo_ingredienteAlim(){
		$idAlimento = $this->session->userdata('idAlimento');
		$sql = "select * from mn_ingrediente ";
        switch($this->session->userdata('administrador')){
		case 1:
		    $sql.= "where administrador = 1 ";
		    break;
		case 2:    
		    $sql.= "where (administrador = 1 or (administrador = 2 and id_empresa=" . $this->session->userdata('idEmpresa')."))";
		    break;
		default:
		    $sql.= "where (administrador = 1 or (administrador = 2 and id_empresa=" . $this->session->userdata('idEmpresa'). ") or ";
		    $sql.= " id_sucursal =" . $this->session->userdata('idSucursal') . ") ";
		    break;
		} 		
		$sql.= " and activo = 1 and id not in(select mai.id_ingrediente from mn_alimento_ingrediente mai where mai.id_alimento = " . $idAlimento . ") ";
		$sql.= " order by nombre";
		$sq= $this->db->query($sql);
		return $sq->result();		
   }


//Producto inventario  ultimo
	public function combo_ingredienteSabor($id){
		$idEmpresa = $this->session->userdata('idEmpresa');
		$idSucursal = $this->session->userdata('idSucursal');
		
		$sql = "select * from ad_ingrediente ing ";
		$sql.= "where ";

		$sql.=" ing.id not in (select si.id_ingrediente from ad_sabor_ingrediente si where si.activo = 1 and  ";
		$sql.=" id_sabor = " . $id . ") ";
		
		$sql.= " order by ing.nombre";
		
	
		$sq= $this->db->query($sql);
		return $sq->result();		
   } 


	public function combo_ingredienteAdicional(){
		$sql = "select ing.* ";
		$sql.= "from mn_ingrediente ing ";
        switch($this->session->userdata('administrador')){
		case 1:
		    $sql.= "where ing.administrador = 1 ";
		    break;
		case 2:    
		    $sql.= "where (ing.administrador = 1 or (ing.administrador = 2 and ing.id_empresa=" . $this->session->userdata('idEmpresa')."))";
		    break;
		default:
		    $sql.= "where (ing.administrador = 1 or (ing.administrador = 2 and ing.id_empresa=" . $this->session->userdata('idEmpresa'). ") or ";
		    $sql.= " ing.id_sucursal =" . $this->session->userdata('idSucursal') . ") ";
		    break;
		    
		} 		
		$sql.= " and ing.activo = 1 and ";
        $sql.= " ing.id not in (select id_ingrediente from mn_alimento mal where mal.activo=1 and (mal.administrador = 1 ";
        $sql.= " or (mal.administrador = 2 and mal.id_empresa= " . $this->session->userdata('idEmpresa') . ") ";
        $sql.= " or mal.id_sucursal =" . $this->session->userdata('idSucursal') . ")) "; 
        $sql.= " order by ing.nombre";
		$sq= $this->db->query($sql);
		return $sq->result();		
   }




	public function combo_categoriaMenu(){
		$sql = "select * from mn_categoria_menu ";
        switch($this->session->userdata('administrador')){
		case 1:
		    $sql.= "where administrador = 1 ";
		    break;
		case 2:    
		    $sql.= "where administrador = 1 or (administrador = 2 and id_empresa=" . $this->session->userdata('idEmpresa').")";
		    break;
		default:
		    $sql.= "where administrador = 1 or (administrador = 2 and id_empresa=" . $this->session->userdata('idEmpresa'). ") or ";
		    $sql.= " id_sucursal =" . $this->session->userdata('idSucursal');
		    break;
		} 		
		$sql.= " and activo = 1 order by nombre";
		
		
		$sq= $this->db->query($sql);
		return $sq->result();		
   }
   
   
	public function combo_tipoPresentacion(){
		$sql = "select * from mn_tipo_presentacion ";
        switch($this->session->userdata('administrador')){
		case 1:
		    $sql.= "where administrador = 1 ";
		    break;
		case 2:    
		    $sql.= "where administrador = 1 or (administrador = 2 and id_empresa=" . $this->session->userdata('idEmpresa').")";
		    break;
		default:
		    $sql.= "where administrador = 1 or (administrador = 2 and id_empresa=" . $this->session->userdata('idEmpresa'). ") or ";
		    $sql.= " id_sucursal =" . $this->session->userdata('idSucursal');
		    break;
		    
		} 		
		$sql.= " and activo = 1 order by nombre";
		$sq= $this->db->query($sql);
		return $sq->result();		
   }   

   public function combo_administradorUsuario(){
		$sql = "select * ";
		$sql.= "from sg_usuario ";	
		$sql.= "where sg_usuario.id not in (select id_usuario from sg_usuario_administrador where administrador=1 and activo=1) and ";
		$sql.= "sg_usuario.activo=1 ";
		$sql.= "order by sg_usuario.nombre ";
		$sq= $this->db->query($sql);
		return $sq->result();		
   }


   public function combo_administradorEmpresa($idEmpresa){
		$sql = "select * ";
		$sql.= "from sg_usuario ";	
		
		if($idEmpresa==1){
		    $idEmpresa = $this->session->userdata('idEmpresa');	
		}
		
		if($idEmpresa==0){
			$sql.= "where sg_usuario.id not in (select id_usuario from sg_usuario_administrador where administrador=2 and activo=1) and ";
			$sql.= "sg_usuario.activo=1 and sg_usuario.id = 0 and sg_usuario.id_empresa = " . $this->session->userdata('idEmpresa');
		}else{
			$sql.= "where sg_usuario.id not in (select id_usuario from sg_usuario_administrador where administrador=2 and activo=1 and ";
			$sql.= "id_empresa = " . $idEmpresa . "	) and ";
			$sql.= "sg_usuario.activo=1 and sg_usuario.id <> 1 and sg_usuario.id_empresa = " . $this->session->userdata('idEmpresa');
		}	
		$sql.= " order by sg_usuario.nombre ";
		$sq= $this->db->query($sql);
		return $sq->result();		
   }
   
   
   public function combo_administradorSucursal($idSucursal){
	    //$idEmpresa = $this->session->userdata('idEmpresa');

	    
		$sql = "select * ";
		$sql.= "from sg_usuario ";	
		
		if($idSucursal==0){
			$idSucursal = $this->session->userdata('idSucursal');
		}	
		
		$sql.= "where sg_usuario.id not in (select id_usuario from sg_usuario_administrador where administrador=3 and activo=1 and ";
		$sql.= "id_sucursal = " .$idSucursal. ") and ";
		
		
		if($idSucursal==0){
			$sql.= "sg_usuario.activo=1 and sg_usuario.id = 0 ";
		}else{	
			$sql.= "sg_usuario.activo=1 and sg_usuario.id <> 1 ";
		}	
		$sql.= "order by sg_usuario.nombre ";
		$sq= $this->db->query($sql);
		return $sq->result();		
   }   



   public function combo_EmpresaAdministrador(){
		$sql = "select cf_empresa.* ";
		$sql.= "from cf_empresa ";	
		$sql.= "where cf_empresa.sw_administrador<>1 and activo=1 ";
		if($this->session->userdata('administrador')<>1){
			$sql.= "and cf_empresa.id=" . $this->session->userdata('idEmpresa');
		}	
		$sql.= " order by cf_empresa.nombre ";
		//die($sql);

		$sq= $this->db->query($sql);
		return $sq->result();		
   }
   
   
   public function combo_EmpresaAdministrador_login(){
		$sql = "select cf_empresa.* ";
		$sql.= "from cf_empresa ";	
		$sql.= "where cf_empresa.sw_administrador<>1 and activo=1 ";
		$sql.= " order by cf_empresa.nombre ";
		//die($sql);

		$sq= $this->db->query($sql);
		return $sq->result();		
   }  
   
   
    // Combo sucursal_adm
    public function combo_sucursalAdm(){
	 $idEmpresa = $this->session->userdata('idEmpresa'); 	
		
     $sql = "select su.* ";
     $sql.= "from cf_sucursal su ";
     $sql.= "where su.sw_sucursal = 1 and su.activo=1 and su.id_empresa = " . $idEmpresa;
     $sql.= " order by su.nombre ";
     $sq= $this->db->query($sql);
     return $sq->result();		
    }   
    
   public function combo_sucursalLoginAdm($id_empresa){
     $sql = "select su.* ";
     $sql.= "from cf_sucursal su ";
     $sql.= "where su.sw_sucursal = 1 and su.activo=1 and su.id_empresa = " . $id_empresa;
     $sql.= " order by su.nombre ";
     $sq= $this->db->query($sql);
     return $sq->result();		
    } 


   //////////////// personal de sucursal  /////////////////////
   public function combo_personalSucursal($id){
	    $idEmpresa = $this->session->userdata('idEmpresa');
	    $idSucursal = $this->session->userdata('idSucursal');
		$sql = "select * ";
		$sql.= "from sg_usuario ";	
		$sql.= "where sg_usuario.id not in (select id_usuario from sg_usuario_administrador where  ";
		$sql.= "administrador > 2 and activo=1 and id_empresa = " .$idEmpresa. " and id_sucursal = " . $idSucursal . ") ";
		$sql.= " and sg_usuario.activo=1 and sg_usuario.id <> 1 ";
		$sql.= "order by sg_usuario.nombre ";
		$sq= $this->db->query($sql);
		return $sq->result();	
    } 

    //Rol personal sucursal ////
    public function combo_rolSucursal(){
     $sql = "select  * ";
     $sql.= "from sg_rol ";
     $sql.= "where sg_rol.activo = 1 and sg_rol.id > 2 ";
     $sql.= " order by sg_rol.nombre ";
     $sq= $this->db->query($sql);
     return $sq->result();		
   }
   
   //////////////// Fin personal de sucursal  /////////////////////
		


   
}
