<?php
class Cp_factura_mdl extends CI_Model {

	
	public function __construct(){
        parent::__construct();
       
    }
    

  public function obt_dataTable(){
		$sql = "select  fa.*, pr.nombre as proveedor ";
		$sql.= "from cp_factura fa ";
        $sql.= "inner join mt_proveedor pr on fa.id_proveedor = pr.id ";
		$sql.= "where fa.activo = 1 and fa.id_empresa=" . $this->session->userdata('idEmpresa');
		$sql.= " and fa.id_sucursal = " . $this->session->userdata('idSucursal');
		$sql.= " order by fa.id ";
		$sq= $this->db->query($sql);
  	    return $sq->result();
  }
  
  
  
  
  public function guardar_facturaCp($data){
    $this->db->insert('cp_factura', $data);
    return $this->db->insert_id();
  }

  public function guardar_detalleProducto($data){
    $this->db->insert('cp_detalle_factura', $data);
  }
  
  public function guardar_producto_add($data){
      $this->db->insert('cp_detalle_factura', $data);	  
  }	  
  
  
  function valProducto($idFactura, $idProducto){
     $sql ="select * ";
     $sql.="from cp_detalle_factura ";
     $sql.="where id_factura = " . $idFactura;
     $sql.=" and id_producto = " . $idProducto;
     $sq= $this->db->query($sql);
     $row = $sq->row();    
     $nro = $sq->num_rows(); 
     if($nro==1){
         return 1;
     }else{
         return 0;
     }  
  }
  
  

  public function eliminarDetalle($id){
    $this->db->where('id_factura', $id);
    $this->db->delete('cp_detalle_factura');  
  }
  
   public function obtModificarFactura($id){
      $this->db->select('*');
      $this->db->where('id',$id);
      return $this->db->get('cp_factura')->row();
   }
   
   public function verFactura($id){
       $sql = "select fa.*, pr.nombre as proveedor ";
       $sql.= "from cp_factura fa ";
       $sql.= "inner join mt_proveedor pr on fa.id_proveedor=pr.id ";
       $sql.= "where fa.id = " . $id;
      $sq= $this->db->query($sql);
      return $sq->row();	  
   }	   



  public function eliminarProducto($id){
    $this->db->where('id', $id);
    $this->db->delete('cp_detalle_factura'); 
    return true;
     
  }



  public function eliminarFactura($id){
    $this->db->where('id', $id);
    $this->db->delete('cp_factura');  
}







    public function obtModificar($id){
        $this->db->select('*');
        $this->db->where('id',$id);
        return $this->db->get('cp_detalle_factura')->row();
    }

    public function obtModificarProducto($id){
       $sql = "select do.*, pr.nombre as producto ";
       $sql.= "from cp_detalle_factura do ";
       $sql.= "inner join mt_producto pr on do.id_producto=pr.id ";
       $sql.= "where do.id = " . $id;
      $sq= $this->db->query($sql);
      return $sq->row();
    }
    
  public function guardar_mod_factura($id,$data){
    $this->db->where('id',$id);
    $this->db->update('cp_factura', $data);
    return $this->db->affected_rows();
  }


  public function guardar_mod_producto($id,$data){
    $this->db->where('id',$id);
    $this->db->update('cp_detalle_factura', $data);
    return $this->db->affected_rows();
  }


    public function obt_dataTableProducto($id){
		$sql = "select  do.*, pr.nombre as producto, pr.cod_producto,  um.nombre as unidad_medida ";
		$sql.= "from cp_detalle_factura do ";
		$sql.= "left join mt_producto pr on do.id_producto = pr.id ";
		$sql.= "left join mt_unidad_medida um on pr.id_unidad_medida = um.id ";
		$sql.= "where do.id_factura = " .$id;
		$sql.= " order by pr.nombre ";
		$sq= $this->db->query($sql);
		$nro = $sq->num_rows(); 
		if($nro>0){
		   return $sq->result();
 	    }else{
		   return false;
	    }      
   }





}
