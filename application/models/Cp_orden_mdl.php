<?php
class Cp_orden_mdl extends CI_Model {

	
	public function __construct(){
        parent::__construct();
       
    }

  public function obt_dataTable(){
		$sql = "select  co.*, pr.nombre as proveedor ";
		$sql.= "from cp_orden co ";
        $sql.= "inner join mt_proveedor pr on co.id_proveedor = pr.id ";
		$sql.= "where co.activo = 1 and co.id_empresa=" . $this->session->userdata('idEmpresa');
		$sql.= " and co.id_sucursal = " . $this->session->userdata('idSucursal');
		$sql.= " order by co.id ";
		$sq= $this->db->query($sql);
  	    return $sq->result();
  }
  
  
  public function guardar_ordenCp($data){
    $this->db->insert('cp_orden', $data);
    return $this->db->insert_id();
  }

  public function guardar_detalleProducto($data){
    $this->db->insert('cp_detalle_orden', $data);
  }
  
  public function guardar_producto_add($data){
      $this->db->insert('cp_detalle_orden', $data);	  
  }	  
  
  
  function valProducto($idOrden, $idProducto){
     $sql ="select * ";
     $sql.="from cp_detalle_orden ";
     $sql.="where id_orden = " . $idOrden;
     $sql.=" and id_producto = " . $idProducto;
     $sq= $this->db->query($sql);
     $row = $sq->row();    
     $nro = $sq->num_rows(); 
     if($nro==1){
         return 1;
     }else{
         return 0;
     }  
  }
  
  

  public function eliminarDetalle($id){
    $this->db->where('id_orden', $id);
    $this->db->delete('cp_detalle_orden');  
  }
  
   public function obtModificarOrden($id){
      $this->db->select('*');
      $this->db->where('id',$id);
      return $this->db->get('cp_orden')->row();
   }
   
   public function verOrden($id){
       $sql = "select co.*, pr.nombre as proveedor ";
       $sql.= "from cp_orden co ";
       $sql.= "inner join mt_proveedor pr on co.id_proveedor=pr.id ";
       $sql.= "where co.id = " . $id;
      $sq= $this->db->query($sql);
      return $sq->row();	  
   }	   



  public function eliminarProducto($id){
    $this->db->where('id', $id);
    $this->db->delete('cp_detalle_orden');  
  }



  public function eliminarOrden($id){
    $this->db->where('id', $id);
    $this->db->delete('cp_orden');  
}







    public function obtModificar($id){
        $this->db->select('*');
        $this->db->where('id',$id);
        return $this->db->get('cp_detalle_orden')->row();
    }

    public function obtModificarProducto($id){
       $sql = "select do.*, pr.nombre as producto ";
       $sql.= "from cp_detalle_orden do ";
       $sql.= "inner join mt_producto pr on do.id_producto=pr.id ";
       $sql.= "where do.id = " . $id;
      $sq= $this->db->query($sql);
      return $sq->row();
    }
    
  public function guardar_mod_orden($id,$data){
    $this->db->where('id',$id);
    $this->db->update('cp_orden', $data);
    return $this->db->affected_rows();
  }


  public function guardar_mod_producto($id,$data){
    $this->db->where('id',$id);
    $this->db->update('cp_detalle_orden', $data);
    return $this->db->affected_rows();
  }


    public function obt_dataTableProducto($id){
    $sql = "select  do.*, pr.nombre as producto, pr.cod_producto,  um.nombre as unidad_medida ";
    $sql.= "from cp_detalle_orden do ";
    $sql.= "left join mt_producto pr on do.id_producto = pr.id ";
    $sql.= "left join mt_unidad_medida um on pr.id_unidad_medida = um.id ";
    
    $sql.= "where do.id_orden = " .$id;
    $sql.= " order by pr.nombre ";
    $sq= $this->db->query($sql);
      return $sq->result();
    }





}
