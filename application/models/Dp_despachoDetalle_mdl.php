<?php
class Dp_despachoDetalle_mdl extends CI_Model {

	
	public function __construct(){
        parent::__construct();
       
  }
  

  public function incluir_despachoDetalle($data){
    $this->db->insert('dp_despacho_detalle', $data);
    return $this->db->insert_id();
  }
  
  
  public function incluir_despachoDetalle_ingrediente($data){
	  $this->db->insert('dp_despacho_detalle_ingrediente', $data);
  }
  
  public function obtDespachoDetalle($id){
      $this->db->select('*');
      $this->db->where('id',$id);
      return $this->db->get('dp_despacho_detalle')->row();
  }
  


  public function eliminar_detalleIngrediente($id){
      $this->db->where('id_despacho_detalle', $id);
      $this->db->delete('dp_despacho_detalle_ingrediente'); 		
  }		


  public function eliminar_detallePedido($id){
      $this->db->where('id', $id);
      $this->db->delete('dp_despacho_detalle');
  }	
  
  
  public function guardar_mod_detallePedido($id,$data){
    $this->db->where('id',$id);
    $this->db->update('dp_despacho_detalle', $data);
    return $this->db->affected_rows();
  }  
  
  public function obtDespachoDetalleIngrediente($id){
      $this->db->select('*');
      $this->db->where('id_despacho_detalle',$id);
      return $this->db->get('dp_despacho_detalle_ingrediente')->result();
  }
  
  
    public function obtDespachoDetalleIngredienteAdicional($id){
      $this->db->select('*');
      $this->db->where('id_despacho_detalle',$id);
      return $this->db->get('dp_despacho_detalle_ingrediente_adicional')->result();
  }
  
  
  
  public function mod_DespachoDetalleIngrediente($id,$data){
    $this->db->where('id',$id);
    $this->db->update('dp_despacho_detalle_ingrediente', $data);
    return $this->db->affected_rows();
  }    

  public function mod_DespachoDetalleIngredienteAdicional($id,$data){
    $this->db->where('id',$id);
    $this->db->update('dp_despacho_detalle_ingrediente_adicional', $data);
    return $this->db->affected_rows();
  }    


public function totalDespacho($idDespacho){
    $sql = "select sum(precio) as total from dp_despacho_detalle ";
    $sql.="where activo = 1 and id_despacho = " . $idDespacho;
    $sq= $this->db->query($sql);
    $row = $sq->row();

    $total = $row->total;

    $data = array(
        'monto'=>$total
    );
    $this->db->where('id',$idDespacho);
    $this->db->update('dp_despacho', $data);
    return true;
}

  
  
  	

  public function obt_dataTable(){
  	  $sql = "select desp.*, sa.nombre as sala, me.nombre as mesa, ms.nombre as nob_mesonero, ms.apellido as ape_mesonero, ";
  	  $sql.= "td.nombre as tipo_despacho, ed.nombre as estado_despacho ";
	  $sql.= "from dp_despacho desp ";
      $sql.= "left join ad_sala sa on desp.id_sala = sa.id ";
      $sql.= "left join ad_mesa me on desp.id_mesa = me.id ";
      $sql.= "left join ad_mesonero ms on desp.id_mesonero = ms.id ";
      $sql.= "inner join dp_tipo_despacho td on desp.id_tipo_despacho = td.id ";
      $sql.= "inner join dp_estado_despacho ed on desp.id_estado_despacho = ed.id ";
      $sql.= "where desp.id_empresa = " . $this->session->userdata('idEmpresa');
      $sql.= " and desp.id_sucursal = " . $this->session->userdata('idSucursal');
      $sql.= " and desp.activo = 1 ";
      $sql.= " order by desp.fecha desc ";
	  $sq= $this->db->query($sql);
	  return $sq->result();
  }

  public function infDespacho($idDespacho){
    $sql = "select desp.*, sa.nombre as sala, me.nombre as mesa, ms.nombre as nob_mesonero, ms.apellido as ape_mesonero, ";
    $sql.= "td.nombre as tipo_despacho, ed.nombre as estado_despacho ";
    $sql.= "from dp_despacho desp ";
    $sql.= "left join ad_sala sa on desp.id_sala = sa.id ";
    $sql.= "left join ad_mesa me on desp.id_mesa = me.id ";
    $sql.= "left join ad_mesonero ms on desp.id_mesonero = ms.id ";
    $sql.= "inner join dp_tipo_despacho td on desp.id_tipo_despacho = td.id ";
    $sql.= "inner join dp_estado_despacho ed on desp.id_estado_despacho = ed.id ";
    $sql.= "where desp.id = " . $idDespacho;
    $sq= $this->db->query($sql);
    return $sq->row();    
  } 

}
