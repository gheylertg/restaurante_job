<?php
class Dp_despacho_mdl extends CI_Model {

	
	public function __construct(){
        parent::__construct();
       
  }
  

  public function obt_dataTable($tipoDespacho){
  	  $sql = "select desp.*, sa.nombre as sala, me.nombre as mesa, ms.nombre as nob_mesonero, ms.apellido as ape_mesonero, ";
  	  $sql.= "td.nombre as tipo_despacho, ed.nombre as estado_despacho ";
	  $sql.= "from dp_despacho desp ";
      $sql.= "left join ad_sala sa on desp.id_sala = sa.id ";
      $sql.= "left join ad_mesa me on desp.id_mesa = me.id ";
      $sql.= "left join ad_mesonero ms on desp.id_mesonero = ms.id ";
      $sql.= "inner join dp_tipo_despacho td on desp.id_tipo_despacho = td.id ";
      $sql.= "inner join dp_estado_despacho ed on desp.id_estado_despacho = ed.id ";
      $sql.= "where desp.id_empresa = " . $this->session->userdata('idEmpresa');
      $sql.= " and desp.id_sucursal = " . $this->session->userdata('idSucursal');
      $sql.= " and desp.activo = 1 and desp.despachado=0 and ";
      $sql.= " desp.id_tipo_despacho = " . $tipoDespacho;
      $sql.= " order by desp.fecha desc ";

	  $sq= $this->db->query($sql);
	  return $sq->result();
  }


  public function obt_nroDespacho(){
	  $sql = "select desp.id_tipo_despacho, count(desp.id) as cantidad ";
	  $sql.= "from dp_despacho desp ";
      $sql.= "where desp.id_empresa = " . $this->session->userdata('idEmpresa');
      $sql.= " and desp.id_sucursal = " . $this->session->userdata('idSucursal');
      $sql.= " and desp.activo = 1 and desp.despachado=0 ";
      $sql.= " group by desp.id_tipo_despacho ";
	  $sq= $this->db->query($sql);
	  return $sq->result();    
  
  }


  public function generarDespacho($data){
    $this->db->insert('dp_despacho', $data);
    return $this->db->insert_id();
  }
  

  public function infDespacho($idDespacho){
    $sql = "select desp.*, sa.nombre as sala, me.nombre as mesa, ms.nombre as nob_mesonero, ms.apellido as ape_mesonero, ";
    $sql.= "td.nombre as tipo_despacho, ed.nombre as estado_despacho ";
    $sql.= "from dp_despacho desp ";
    $sql.= "left join ad_sala sa on desp.id_sala = sa.id ";
    $sql.= "left join ad_mesa me on desp.id_mesa = me.id ";
    $sql.= "left join ad_mesonero ms on desp.id_mesonero = ms.id ";
    $sql.= "inner join dp_tipo_despacho td on desp.id_tipo_despacho = td.id ";
    $sql.= "inner join dp_estado_despacho ed on desp.id_estado_despacho = ed.id ";
    $sql.= "where desp.id = " . $idDespacho;
   
    $sq= $this->db->query($sql);
    return $sq->row();    
  } 
  
  function val_nroDetalle($idDetalle){
		$sql ="select * ";
		$sql.="from dp_despacho_detalle ";
		$sql.="where id_despacho = " . $idDetalle;
		$sq= $this->db->query($sql);
		$row = $sq->row();		
		$nro = $sq->num_rows(); 
		if($nro>0){
			return 1;
		}else{
			return 0;
		}  
  }
  
  function cerrarDespacho($idDespacho, $data){
    $this->db->where('id',$idDespacho);
    $this->db->update('dp_despacho', $data);
    return true;
  }
  
  function modificarEstadoDespacho($idDespacho, $data){  
    $this->db->where('id',$idDespacho);
    $this->db->update('dp_despacho', $data);
    return true;
  }
  
  function obt_detalleFactura($idDespacho){
    $sql = "select dpd.*, ali.nombre_menu as alimento, tp.nombre as presentacion, envo.nombre as envoltura ";
    $sql.= "from dp_despacho_detalle dpd ";
    $sql.= "inner join mn_alimento ali on dpd.id_alimento = ali.id ";
    $sql.= "inner join mn_alimento_presentacion alip on dpd.id_presentacion = alip.id ";
    $sql.= "inner join mn_tipo_presentacion tp on alip.id_tipo_presentacion = tp.id ";
    $sql.= "left join ad_envoltura envo on dpd.id_envoltura = envo.id ";
    $sql.= "where dpd.id_despacho = " . $idDespacho;
    $sql.= " order by dpd.id ";
    //$sql.= " order by dpd.id_categoria_menu ";
    $sq= $this->db->query($sql);
    return $sq->result();      
  }


  public function obt_despachoCobro_new(){
      $sql = "select desp.* ";
	  $sql.= "from dp_despacho desp ";
      $sql.= "where desp.id_empresa = " . $this->session->userdata('idEmpresa');
      $sql.= " and desp.id_sucursal = " . $this->session->userdata('idSucursal');
      $sql.= " and desp.activo = 1 and desp.id_estado_despacho=4 and desp.cobrar_nuevo = 1";
	  $sq= $this->db->query($sql);
	  $row = $sq->row();		
	  $nro = $sq->num_rows(); 
	  if($nro>0){
		return 1;
	  }else{
		return 0;
	  }  
  }


  public function act_cobro_new(){
	  $data = array(
	      'cobrar_nuevo'=>0
	  );
      $this->db->where('id_empresa',$this->session->userdata('idEmpresa'));
      $this->db->where('id_sucursal',$this->session->userdata('idSucursal'));
	  $this->db->update('dp_despacho', $data);
  } 




  public function obt_despachoCobro(){
      $sql = "select desp.*, sa.nombre as sala, me.nombre as mesa, ms.nombre as nob_mesonero, ms.apellido as ape_mesonero, ";
      $sql.= "td.nombre as tipo_despacho, ed.nombre as estado_despacho ";
    $sql.= "from dp_despacho desp ";
      $sql.= "left join ad_sala sa on desp.id_sala = sa.id ";
      $sql.= "left join ad_mesa me on desp.id_mesa = me.id ";
      $sql.= "left join ad_mesonero ms on desp.id_mesonero = ms.id ";
      $sql.= "inner join dp_tipo_despacho td on desp.id_tipo_despacho = td.id ";
      $sql.= "inner join dp_estado_despacho ed on desp.id_estado_despacho = ed.id ";
      $sql.= "where desp.id_empresa = " . $this->session->userdata('idEmpresa');
      $sql.= " and desp.id_sucursal = " . $this->session->userdata('idSucursal');
      $sql.= " and desp.activo = 1 and desp.cobrado=0 and id_estado_despacho=4";
      $sql.= " order by desp.fecha desc ";
      $sq= $this->db->query($sql);
      return $sq->result();
  }

  public function DetalleDespacho_acum($idDespacho){
	  $sql = "SELECT id_despacho, id_categoria_menu, id_alimento, id_presentacion, id_envoltura, count(cantidad) as cantidad, sum(precio) as precio ";
	  $sql.= "FROM `dp_despacho_detalle` WHERE id_despacho=" . $idDespacho;
	  $sql.= " group by id_despacho, id_categoria_menu, id_alimento, id_presentacion, id_envoltura ";
      $sq= $this->db->query($sql);
      return $sq->result();
  }
  
  
  public function DetalleDespacho_ind($idDespacho, $idAlimento, $idPresentacion, $idEnvoltura){
	  $sql = "SELECT dd.*, ali.nombre as alimento, ali.id_categoria_menu, envo.nombre as envoltura, um.nombre as unidad_medida "; 
	  $sql.= "from dp_despacho_detalle dd ";
	  $sql.= "inner join mn_alimento ali on dd.id_alimento=ali.id ";
	  $sql.= "left join ad_envoltura envo on dd.id_envoltura = envo.id ";
	  $sql.= "inner join mt_unidad_medida um on dd.id_unidad_medida = um.id ";
	  $sql.= "WHERE dd.id_despacho=" . $idDespacho . " and dd.id_alimento=" . $idAlimento;
	  $sql.= " and dd.id_presentacion = " .$idPresentacion . " and dd.id_envoltura = " . $idEnvoltura;
      $sq= $this->db->query($sql);
      return $sq->row();
  }
  
  
  public function obt_despachoDetalle($idDespacho){
	  $sql = "SELECT dd.* "; 
	  $sql.= "from dp_despacho_detalle dd ";
	  $sql.= "WHERE dd.id_despacho=" . $idDespacho;
      $sq= $this->db->query($sql);
      return $sq->result();
  }
  
  
  



  function reversarDespacho($idDespacho, $data){
    $this->db->where('id',$idDespacho);
    $this->db->update('dp_despacho', $data);
    return true;
  }




/************************* */

}
