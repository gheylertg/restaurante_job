<?php
class Dp_mesaDespacho_mdl extends CI_Model {

	
	public function __construct(){
        parent::__construct();
       
  }


  public function obt_dataTable(){
  		$sql = "select md.*, sa.nombre as sala, me.nombre as mesa, ame.nombre as nombre_mesonero, ";
      $sql.= "ame.apellido as apellido_mesonero, td.nombre as tipo_despacho ";
		  $sql.= "from dm_mesa_despacho md ";
      $sql.= "inner join dm_tipo_despacho td on md.id_tipo_despacho = td.id ";
      $sql.= "left join ad_sala sa on md.id_sala = sa.id ";
      $sql.= "left join ad_mesa me on md.id_mesa = me.id ";
      $sql.= "left join ad_mesonero ame on md.id_mesonero = ame.id ";      
      $sql.= "where md.id_empresa = " . $this->session->userdata('idEmpresa');
      $sql.= " and md.id_sucursal = " . $this->session->userdata('idSucursal');
      $sql.= " and md.estado_despacho = 1 ";
      $sql.= " order by sa.nombre, me.nombre ";
		  $sq= $this->db->query($sql);
		  return $sq->result();
  }


  public function obt_infMesa($id){
      $sql = "select md.*, sa.nombre as sala, me.nombre as mesa ";
      $sql.= "from dm_mesa_despacho md ";
      $sql.= "left join ad_sala sa on md.id_sala = sa.id ";
      $sql.= "left join ad_mesa me on md.id_mesa = me.id ";
      $sql.= "where md.id = " . $id;
      $sq= $this->db->query($sql);
      return $sq->row();
      


  }


  public function modificarEstadoDespacho($id, $data){
      $this->db->where('id',$id);
      $this->db->update('dm_mesa_despacho', $data);
      return $this->db->affected_rows();
  }



/* ************************************************************* */
    public function guardarDespacho($data){
    $this->db->insert('cj_movimiento_caja', $data);
    return $this->db->affected_rows();
  }



  public function guardar_add($data){
    $this->db->insert('cj_movimiento_caja', $data);
    return $this->db->affected_rows();
  }

    public function obtModificar($id){
      $this->db->select('*');
      $this->db->where('id',$id);
      return $this->db->get('cj_movimiento_caja')->row();
    }


  public function guardar_mod($id,$data){
    $this->db->where('id',$id);
    $this->db->update('cj_movimiento_caja', $data);
    return $this->db->affected_rows();
  }


  public function obt_ver($id){
      $sql = "select mc.*, tm.nombre as movimiento,  td.nombre as dinero ";
      $sql.= "from cj_movimiento_caja mc ";
      $sql.= "inner join mt_tipo_movimiento_caja tm on mc.id_tipo_movimiento = tm.id ";
      $sql.= "inner join mt_tipo_dinero td on mc.id_tipo_dinero = td.id ";
      $sql.= "where mc.id =  " . $id;
      $sql.= " order by mc.fecha desc ";
      $sq= $this->db->query($sql);
      return $sq->row();
  }

/************************* */

   










  public function desactivar($id,$data){
      $this->db->where('id',$id);
      $this->db->update('cf_region', $data);
      return $this->db->affected_rows();
  }


   
    public function valNombre($nombre){

		$sql ="select * ";
		$sql.="from cf_region ";
		$sql.="where nombre = '" . $nombre . "' ";
		$sq= $this->db->query($sql);
		$row = $sq->row();		
        $nro = $sq->num_rows(); 
        if($nro==1){
            return 1;
        }else{
        	return 0;
        }  
    }
}
