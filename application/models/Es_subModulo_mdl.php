<?php
class Es_subModulo_mdl extends CI_Model {

	
	public function __construct(){
        parent::__construct();
       
    }

   
   
    public function obt_dataTable(){
		$sql = "select sm.*, m.nombre as modulo ";
		  $sql.= "from sg_sub_modulo sm ";
		  $sql.= "inner join sg_modulo m on m.id = sm.id_modulo ";
		  $sql.= " order by m.nombre, sm.orden ";
		  $sq= $this->db->query($sql);
		  return $sq->result();
    }


    
    public function obtModificar($id){
		$this->db->select('*');
		$this->db->where('id',$id);
		return $this->db->get('sg_sub_modulo')->row();
    }


    public function obtSubModulo($id){
    $this->db->select('*');
    $this->db->where('id_modulo',$id);
    
    
    
    return $this->db->get('sg_sub_modulo')->result();
    }


  public function guardar_add($data){
    $this->db->insert('sg_sub_modulo', $data);
    return $this->db->affected_rows();
  }



  public function guardar_mod($id,$data){
    $this->db->where('id',$id);
    $this->db->update('sg_sub_modulo', $data);
    return $this->db->affected_rows();
  }


  public function desactivar($id,$data){
      $this->db->where('id',$id);
      $this->db->update('sg_sub_modulo', $data);
      return $this->db->affected_rows();
  }


   
    public function valNombre($nombre){

		$sql ="select * ";
		$sql.="from sg_sub_modulo ";
		$sql.="where nombre = '" . $nombre . "' ";

		$sq= $this->db->query($sql);
		$row = $sq->row();		
        $nro = $sq->num_rows(); 
        

        if($nro==1){
            return 1;
        }else{
        	return 0;
        }  

    }
    

}
