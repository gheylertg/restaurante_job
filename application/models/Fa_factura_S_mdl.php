<?php
class Fa_factura_S_mdl extends CI_Model {

	
	public function __construct(){
        parent::__construct();
       
    }


  public function consultaSucursal($idSucursal, $swDocumento, $nroDocumento, $swFecha, $fechaInicio, $fechaFinal){

     $swCadena = 0;
     $cadConsulta="";

      if($swDocumento==1){
           $cadConsulta.= " and  vt_venta.documento='" . $nroDocumento ."' ";
           $swCadena = 1;
      }

      if($swFecha==1){
           $fechaInicio = str_replace("/","-",$fechaInicio);
           $fecha1 = $fechaInicio . " 00-00-00";
           $fecha2 = $fechaInicio . " 23-59-59";
           $swCadena = 1;
           $cadConsulta.= " and (vt_venta.fecha>='" . $fecha1 . "' and vt_venta.fecha<='" . $fecha2."') ";
      }
           
      if($swFecha==2){
           $fechaInicio = str_replace("/","-",$fechaInicio);
           $fechaInicio = $fechaInicio . " 00-00-00";
           $fechaFinal = str_replace("/","-",$fechaFinal);
           $fechaFinal = $fechaFinal . " 23-59-59";
           $swCadena = 1;
           $cadConsulta.= " and (vt_venta.fecha>='" . $fechaInicio . "' and vt_venta.fecha<='" . $fechaFinal ."') ";
      } 
      $sql = "select vt_venta.*, dp_tipo_despacho.nombre as tipo_despacho, cj_cobrar.nombre_cliente ";
      $sql.= "from vt_venta ";
      $sql.= "inner join cj_cobrar on vt_venta.id_cobro = cj_cobrar.id ";
      $sql.= "inner join dp_tipo_despacho on cj_cobrar.id_tipo_despacho = dp_tipo_despacho.id ";
      $sql.= "where vt_venta.id_sucursal = " . $idSucursal;
      if($swCadena>0){
           $sql.=$cadConsulta;
      } 
      $sql.= " order by vt_venta.id";
      $sq= $this->db->query($sql);
      return $sq->result();      
  } 


public function obtDatoFactura($idSucursal, $idFactura){
    $sql = "select vt_venta.*, dp_tipo_despacho.nombre as tipo_despacho, cj_cobrar.nombre_cliente, cj_cobrar.telefono ";
    $sql.= "from vt_venta ";
    $sql.= "inner join cj_cobrar on vt_venta.id_cobro = cj_cobrar.id ";
    $sql.= "inner join dp_tipo_despacho on cj_cobrar.id_tipo_despacho = dp_tipo_despacho.id ";
    $sql.= "where vt_venta.id = " . $idFactura;
    $sq= $this->db->query($sql);
    return $sq->row();      

}

public function obtDetalleFactura($idFactura){
    $sql = "select vt_venta_detalle.* ";
    $sql.= "from vt_venta_detalle ";
    $sql.= "where vt_venta_detalle.id_venta = " . $idFactura;
    $sql.= " order by vt_venta_detalle.id ";
    $sq= $this->db->query($sql);
    return $sq->result();      

}


////////////////////////////////////////////////////////////////////////////////////////////  

                     
public function consultaTotalesFactura($swFecha, $fechaInicio, $fechaFinal, $idSucursal){
      if($swFecha==1){
           $fechaInicio = str_replace("/","-",$fechaInicio);
           $fecha1 = $fechaInicio . " 00-00-00";
           $fecha2 = $fechaInicio . " 23-59-59";
           $cadFecha= " and (vt_venta.fecha>='" . $fecha1 . "' and vt_venta.fecha<='" . $fecha2."') ";
      }
           
      if($swFecha==2){
           $fechaInicio = str_replace("/","-",$fechaInicio);
           $fechaInicio = $fechaInicio . " 00-00-00";
           $fechaFinal = str_replace("/","-",$fechaFinal);
           $fechaFinal = $fechaFinal . " 23-59-59";
           $cadFecha= " and (vt_venta.fecha>='" . $fechaInicio . "' and vt_venta.fecha<='" . $fechaFinal ."') ";
      } 
      $idEmpresa = $this->session->userdata('idEmpresa');

      $sql = "select cf_sucursal.nombre as sucursal, count(vt_venta.id) as cantidad_factura, ";
      $sql.="sum(vt_venta.monto_cobrar) as total_despachado, sum(vt_venta.descuento) as total_descuento,";
      $sql.= "sum(vt_venta.monto_venta) as total_factura ";
      $sql.= "from vt_venta ";
      $sql.= "inner join cf_sucursal on vt_venta.id_sucursal = cf_sucursal.id ";
      $sql.= "where vt_venta.id_sucursal =". $idSucursal . " and vt_venta.reverso=0 ";
      if($swFecha>0){
          $sql.=$cadFecha;
      } 
      $sql.= " group by cf_sucursal.nombre ";
      $sql.= " order by cf_sucursal.nombre";
      $sq= $this->db->query($sql);
      return $sq->result();      
  } 





////////////////////////////////////////////////////////////////////

  public function obt_dataTable(){
      $idEmpresa = $this->session->userdata('idEmpresa');
      $idSucursal = $this->session->userdata('idSucursal');

  	  $sql = "select * ";
	  $sql.= "from ad_sala ";
      switch($this->session->userdata('administrador')){
      case 1:
          $sql.="where administrador=1 ";
          break;
      case 2:
          $sql.="where administrador=2 and id_empresa = " . $idEmpresa;
          break;
      default:
          $sql.="where id_sucursal = " . $idSucursal;
          break;
      }
	  $sql.= " order by nombre";
	  $sq= $this->db->query($sql);
	  return $sq->result();
  }


   
    public function obtModificar($id){
		$this->db->select('*');
		$this->db->where('id',$id);
		return $this->db->get('ad_sala')->row();
    }


  public function guardar_add($data){
    $this->db->insert('ad_sala', $data);
    return $this->db->affected_rows();
  }




  public function guardar_mod($id,$data){
    $this->db->where('id',$id);
    $this->db->update('ad_sala', $data);
    return $this->db->affected_rows();
  }


  public function desactivar($id,$data){
      $this->db->where('id',$id);
      $this->db->update('ad_sala', $data);
      return $this->db->affected_rows();
  }


   
    public function valNombre($nombre){

		$sql ="select * ";
		$sql.="from ad_sala ";
		$sql.="where nombre = '" . $nombre . "' ";
		//$sql.=" and administrador = " . $this->session->userdata('administrador') . " ";
		//$sql.=" and id_empresa = " . $this->session->userdata('idEmpresa') . " ";
		$sql.=" and id_sucursal = " . $this->session->userdata('idSucursal');
		$sq= $this->db->query($sql);
		$row = $sq->row();		
        $nro = $sq->num_rows(); 
        if($nro==1){
            return 1;
        }else{
        	return 0;
        }  
    }
}
