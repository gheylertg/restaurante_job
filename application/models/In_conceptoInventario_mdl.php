<?php
class In_conceptoInventario_mdl extends CI_Model {

	
	public function __construct(){
        parent::__construct();
       
    }




  public function obt_dataTable(){
  		$sql = "select * ";
		  $sql.= "from in_concepto_inventario ";
		  $sql.= "order by nombre";
		  $sq= $this->db->query($sql);
		  return $sq->result();
  }


   
    public function obtModificar($id){
		$this->db->select('*');
		$this->db->where('id',$id);
		return $this->db->get('in_concepto_inventario')->row();
    }


  public function guardar_add($data){
    $this->db->insert('in_concepto_inventario', $data);
    return $this->db->affected_rows();
  }



  public function guardar_mod($id,$data){
    $this->db->where('id',$id);
    $this->db->update('in_concepto_inventario', $data);
    return $this->db->affected_rows();
  }


  public function desactivar($id,$data){
      $this->db->where('id',$id);
      $this->db->update('in_concepto_inventario', $data);
      return $this->db->affected_rows();
  }


   
    public function valNombre($nombre){

		$sql ="select * ";
		$sql.="from in_concepto_inventario ";
		$sql.="where nombre = '" . $nombre . "' ";
		$sq= $this->db->query($sql);
		$row = $sq->row();		
        $nro = $sq->num_rows(); 
        if($nro==1){
            return 1;
        }else{
        	return 0;
        }  
    }
}
