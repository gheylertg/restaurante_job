<?php
class In_entrada_mdl extends CI_Model {

	
	public function __construct(){
        parent::__construct();
       
    }



    public function obt_dataTable(){
		$sql = "select  tr.*, pd.nombre as producto, ci.nombre as concepto ";
		$sql.= "from in_transaccion tr ";
    $sql.= "left join mt_producto pd on tr.id_producto = pd.id ";
    $sql.= "inner join in_concepto_inventario ci on tr.id_concepto = ci.id ";
    $sql.= "where tr.activo = 1 and tr.id_tipo_operacion = 1 and ";
    $sql.= "tr.id_empresa = " . $this->session->userdata("idEmpresa");
    $sql.= " and tr.id_sucursal=" . $this->session->userdata("idSucursal");
    $sql.= " and tr.id_concepto > 3 ";

	  $sql.= " order by di.id desc ";
		$sq= $this->db->query($sql);
		  return $sq->result();
    }

  public function guardar_entrada($data){
    $this->db->insert('in_transaccion', $data);
  }













  ////////////////////////////////////////////////////////////////////////////////////

  public function in_actTotProducto($idEmpresa, $idSucursal, $idProd, $data){
      $this->db->where('id_producto',$idProd);
      $this->db->where('id_empresa',$idEmpresa);
      $this->db->where('id_sucursal',$idSucursal);
      $this->db->update('in_inventario', $data);
  }


  public function obtInventarioTotal($idEmpresa, $idSucursal, $idProducto){
      $this->db->select('*');
      $this->db->where('id_producto',$idProducto);
      $this->db->where('id_empresa',$idEmpresa);
      $this->db->where('id_sucursal',$idSucursal);
      return $this->db->get('in_inventario')->row();

  }








///////////////////////////////////////////////////////////////////////////////


 


  

  public function eliminarDetalle($id){
    $this->db->where('id_inventario', $id);
    $this->db->delete('in_detalle_inventario');  
}


  public function eliminarProducto($id){
    $this->db->where('id', $id);
    $this->db->delete('in_detalle_inventario');  
}



  public function eliminarEntrada($id){
    $this->db->where('id', $id);
    $this->db->delete('in_inventario');  
}


    public function obtModificar($id){
        $this->db->select('*');
        $this->db->where('id',$id);
        return $this->db->get('in_inventario')->row();
    }


    public function obtModificarProducto($id){
       $sql = "select di.*, pr.nombre as producto ";
       $sql.= "from in_detalle_inventario di ";
       $sql.= "inner join mt_producto pr on di.id_producto=pr.id ";
       $sql.= "where di.id = " . $id;
      $sq= $this->db->query($sql);
      return $sq->row();
    }
    




  public function guardar_mod_entrada($id,$data){
    $this->db->where('id',$id);
    $this->db->update('in_inventario', $data);
    return $this->db->affected_rows();
  }


  public function guardar_mod_producto($id,$data){
    $this->db->where('id',$id);
    $this->db->update('in_detalle_inventario', $data);
    return $this->db->affected_rows();
  }






    public function obt_dataTableProducto($id){
    $sql = "select  di.*, pr.nombre as producto, pr.cod_producto ";
    $sql.= "from in_detalle_inventario di ";
    $sql.= "left join mt_producto pr on di.id_producto = pr.id ";
    $sql.= "where di.id_inventario = " .$id;
    $sql.= " order by pr.nombre ";
    $sq= $this->db->query($sql);
      return $sq->result();
    }


    public function valProductoInventario($idInventario, $idProducto){

        $sql ="select * ";
        $sql.="from in_detalle_inventario ";
        $sql.="where id_inventario = " . $idInventario;
        $sql.=" and id_producto=".$idProducto;
        $sq= $this->db->query($sql);
        $row = $sq->row();    
        $nro = $sq->num_rows(); 
        if($nro==1){
           return 1;
        }else{
           return 0;
        }  
    }

    //registrar inventario total
    public function guardar_inventario_total($data){
        $this->db->insert('in_inventario_total', $data);
    }










}
