<?php
class In_inventario_mdl extends CI_Model {
	public function __construct(){
        parent::__construct();
       
    }

   
   public function obt_dataTable(){
      $idEmpresa = $this->session->userdata('idEmpresa');
      $idSucursal = $this->session->userdata('idSucursal');		
  		$sql = "select inv.*, p.cod_producto, p.nombre ";
 	    $sql.= "from in_inventario inv ";
      $sql.= "inner join mt_producto p on inv.id_producto = p.id ";
      $sql.= "where inv.id_empresa = " . $idEmpresa;
      $sql.= " and inv.id_sucursal = " . $idSucursal;
      $sql.= " order by p.nombre";
  	  $sq= $this->db->query($sql);
	    return $sq->result();
   }


  public function guardar_add($data){
    $this->db->insert('in_inventario', $data);
    return $this->db->affected_rows();
  }


   public function obtModificar($id){
    $this->db->select('*');
    $this->db->where('id',$id);
    return $this->db->get('in_inventario')->row();

    }


  public function guardar_mod_stock($id,$data){
      $this->db->where('id',$id);
      $this->db->update('in_inventario', $data);
      return $this->db->affected_rows();
  }
  
  
  function obtInventarioProducto($idEmpresa, $idSucursal, $idProducto){
    $this->db->select('*');
    $this->db->where('id_empresa',$idEmpresa);
    $this->db->where('id_sucursal',$idSucursal);
    $this->db->where('id_producto',$idProducto);
    return $this->db->get('in_inventario')->row();
    
  /*public function in_actTotProducto($idEmpresa, $idSucursal, $idProd, $data){
      $this->db->where('id_producto',$idProd);
      $this->db->where('id_empresa',$idEmpresa);
      $this->db->where('id_sucursal',$idSucursal);
      $this->db->update('in_inventario', $data);
  } */   
    
    
    
  }
  
  function in_actTotProducto($idEmpresa, $idSucursal, $idProducto, $data){
    $this->db->where('id_empresa',$idEmpresa);
    $this->db->where('id_sucursal',$idSucursal);
    $this->db->where('id_producto',$idProducto);
    $this->db->update('in_inventario', $data);
    return $this->db->affected_rows();
	  
	  
  }	  
  
  
  


/////////////////////////////////////////////////////////////////

    public function obtVerProducto($id){
        $sql = "select p.*, cp.nombre as categoria_producto, um.nombre as unidad_medida, tp.nombre as tipo  ";
        $sql.= "from mt_producto p ";
        $sql.= "inner join mt_categoria_producto cp on cp.id = p.id_categoria_producto ";
        $sql.= "inner join mt_unidad_medida um on um.id = p.id_unidad_medida ";
        $sql.= "inner join mt_tipo_producto tp on tp.id = p.tipo_producto ";      
        $sql.= "where p.id=".$id;
        $sq= $this->db->query($sql);
    return $sq->row();


    }


    public function obtProductoIn($id){
        $sql = "select p.*, um.nombre as unidad_medida  ";
        $sql.= "from mt_producto p ";
        $sql.= "inner join mt_unidad_medida um on um.id = p.id_unidad_medida ";
        $sql.= "where p.id=" .  $id;
        $sq= $this->db->query($sql);
        return $sq->row();
    }


    //Producto de compra
    public function obtProductoCp($id){
        $sql = "select p.*, um.nombre as unidad_medida  ";
        $sql.= "from mt_producto p ";
        $sql.= "inner join mt_unidad_medida um on um.id = p.id_unidad_medida ";
        $sql.= "where p.id=" .  $id;
        $sq= $this->db->query($sql);
        return $sq->row();
    }




  public function guardar_add_id($data){
      $this->db->insert('mt_producto', $data);
      return $this->db->insert_id();    
  }


  public function guardar_mod($id,$data){
      $this->db->where('id',$id);
      $this->db->update('mt_producto', $data);
      return $this->db->affected_rows();
  }


  public function desactivar($id,$data){
      $this->db->where('id',$id);
      $this->db->update('mt_producto', $data);
      return $this->db->affected_rows();
  }


   
    public function valNombre($nombre){
	    $sql ="select * ";
		$sql.="from mt_producto ";
		$sql.="where nombre = '" . $nombre . "' ";
		$sq= $this->db->query($sql);
		$row = $sq->row();		
        $nro = $sq->num_rows(); 
        if($nro==1){
            return 1;
        }else{
        	return 0;
        }  
    }
    

}
