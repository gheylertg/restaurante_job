<?php
class In_transaccion_mdl extends CI_Model {
	public function __construct(){
        parent::__construct();
       
    }



    // viene de inventario inicial
	public function add_transaccion($data){
		$this->db->insert('in_transaccion', $data);
		return $this->db->affected_rows();
	} 



    //************* Entrada
    public function obt_dataTableEntrada(){
        $sql = "select  tr.*, pd.nombre as producto, ci.nombre as concepto ";
        $sql.= "from in_transaccion tr ";
        $sql.= "left join mt_producto pd on tr.id_producto = pd.id ";
        $sql.= "inner join in_concepto_inventario ci on tr.id_concepto = ci.id ";
        $sql.= "where tr.activo = 1 and tr.id_tipo_operacion = 1 and ";
        $sql.= "tr.id_empresa = " . $this->session->userdata("idEmpresa");
        $sql.= " and tr.id_sucursal=" . $this->session->userdata("idSucursal");
        $sql.= " and tr.id_concepto > 3 ";
        $sql.= " order by tr.id desc ";
        $sq= $this->db->query($sql);
    return $sq->result();
    }
    
    
    public function guardar_entrada($data){
		$this->db->insert('in_transaccion', $data);
	}



    //************* Salida
    public function obt_dataTableSalida(){
        $sql = "select  tr.*, pd.nombre as producto, ci.nombre as concepto ";
        $sql.= "from in_transaccion tr ";
        $sql.= "left join mt_producto pd on tr.id_producto = pd.id ";
        $sql.= "inner join in_concepto_inventario ci on tr.id_concepto = ci.id ";
        $sql.= "where tr.activo = 1 and tr.id_tipo_operacion = 2 and ";
        $sql.= "tr.id_empresa = " . $this->session->userdata("idEmpresa");
        $sql.= " and tr.id_sucursal=" . $this->session->userdata("idSucursal");
        $sql.= " and tr.id_concepto > 3 ";
        $sql.= " order by tr.id desc ";
        $sq= $this->db->query($sql);
    return $sq->result();
    }
    
    
    public function guardar_salida($data){
		$this->db->insert('in_transaccion', $data);
	}
     



}
