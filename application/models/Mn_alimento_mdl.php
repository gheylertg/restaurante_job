<?php
class Mn_alimento_mdl extends CI_Model {

	
	public function __construct(){
        parent::__construct();
       
    }
    


  public function obt_dataTable(){
		$sql = "select  mn_alimento.*, mn_categoria_menu.nombre as categoria_menu ";
		$sql.= "from mn_alimento ";
    $sql.= "left join mn_categoria_menu on mn_alimento.id_categoria_menu = mn_categoria_menu.id  ";
		$sql.= "where mn_alimento.id_empresa=" . $this->session->userdata('idEmpresa');
		$sql.= " and mn_alimento.id_sucursal = " . $this->session->userdata('idSucursal');
    $sql.= " and mn_alimento.id_ingrediente = 0 ";
		$sql.= " order by mn_alimento.nombre ";
		$sq= $this->db->query($sql);
  	    return $sq->result();
  }
  


  public function obt_alimentoCategoria($idCategoria){
		$sql = "select  mn_alimento.*, ";
		$sql.= "(select count(map.id) from mn_alimento_presentacion map where map.id_alimento=mn_alimento.id and map.activo=1) as presentacion ";
		$sql.= " from mn_alimento ";
		$sql.= "where mn_alimento.activo = 1 and mn_alimento.id_empresa=" . $this->session->userdata('idEmpresa');
		$sql.= " and mn_alimento.id_sucursal = " . $this->session->userdata('idSucursal');
		$sql.= " and mn_alimento.id_categoria_menu=" . $idCategoria; 
		$sql.= " order by mn_alimento.orden ";
		$sq= $this->db->query($sql);
  	    return $sq->result();
  }



  public function valNombre($nombre){
      $sql ="select * ";
      $sql.="from mn_alimento ";
      $sql.="where nombre = '" . $nombre . "' ";
      $sql.=" and id_empresa = " . $this->session->userdata('idEmpresa') . " ";
      $sql.=" and id_sucursal = " . $this->session->userdata('idSucursal');
      $sq= $this->db->query($sql);
      $row = $sq->row();    
      $nro = $sq->num_rows(); 
      if($nro>0){
          return 1;
      }else{
        return 0;
      }  
  }



  public function guardar_alimentoSB($data){
    $this->db->insert('mn_alimento', $data);
    return $this->db->insert_id();
  }

  public function guardar_detalleIngrediente($data){
    $this->db->insert('mn_alimento_ingrediente', $data);
  }  


   public function obtModificarAlimento($id){
      $this->db->select('*');
      $this->db->where('id',$id);
      return $this->db->get('mn_alimento')->row();
   }


   public function obtModificarIngredienteAlimento($id){
      $this->db->select('*');
      $this->db->where('id',$id);
      return $this->db->get('mn_alimento_ingrediente')->row();
   }   


/*   public function obtMIngredienteAlimento($idAlimentoPresentacion){
      $sql = "select  api.*, mn_ingrediente.nombre as ingrediente ";
      $sql.= "from mn_alimento_presentacion_ingrediente api ";
      $sql.= "inner join mn_ingrediente on api.id_ingrediente = mn_ingrediente.id ";
      $sql.= "where api.activo = 1 and api.id_alimento_presentacion = " . $idAlimentoPresentacion;
      $sql.= " order by mn_ingrediente.nombre ";
     
      $sq= $this->db->query($sql);
      return $sq->result();
   }   
*/ 


  public function obtMIngredienteAlimento($idAlimento){
      $sql = "select  mn_alimento_ingrediente.*, mn_ingrediente.nombre as ingrediente ";
      $sql.= "from mn_alimento_ingrediente ";
      $sql.= "inner join mn_ingrediente on mn_alimento_ingrediente.id_ingrediente = mn_ingrediente.id ";
      $sql.= "where mn_alimento_ingrediente.activo = 1 and id_alimento = " . $idAlimento;
      $sql.= " order by mn_ingrediente.nombre ";
      $sq= $this->db->query($sql);
      return $sq->result();
   }   




  public function guardar_mod_alimento($id,$data){
    $this->db->where('id',$id);
    $this->db->update('mn_alimento', $data);
    return $this->db->affected_rows();
  }
  
  
    public function desactivar_alimento($id,$data){
    $this->db->where('id',$id);
    $this->db->update('mn_alimento', $data);
    return $this->db->affected_rows();
  }

 
   public function verAlimento($id){
       $sql = "select mn_alimento.* ";
       $sql.= "from mn_alimento ";
       $sql.= "where mn_alimento.id = " . $id;
      $sq= $this->db->query($sql);
      return $sq->row();    
   }  


  function valIngrediente($idAlimento, $idIngrediente){
     $sql ="select * ";
     $sql.="from mn_alimento_ingrediente ";
     $sql.="where id_alimento = " . $idAlimento;
     $sql.=" and id_ingrediente = " . $idIngrediente;
     $sq= $this->db->query($sql);
     $row = $sq->row();    
     $nro = $sq->num_rows(); 
     if($nro==1){
         return 1;
     }else{
         return 0;
     }  
  }


  
  public function guardar_ingrediente_add($data){
      $this->db->insert('mn_alimento_ingrediente', $data);	  
      return $this->db->insert_id();

  }	  
  

  public function guardar_mod_ingredienteAlimento($id,$data){
    $this->db->where('id',$id);
    $this->db->update('mn_alimento_ingrediente', $data);
    return $this->db->affected_rows();
  }


    public function obt_dataTableIngrediente($id){
    $sql = "select  mai.*, ing.nombre as ingrediente, um.nombre as unidad_medida ";
    $sql.= "from mn_alimento_ingrediente mai ";
    $sql.= "left join mn_ingrediente ing on mai.id_ingrediente = ing.id ";
    $sql.= "left join mt_unidad_medida um on mai.id_unidad_medida = um.id ";
    $sql.= "where mai.id_alimento = " .$id;
    $sql.= " order by ing.nombre ";
    $sq= $this->db->query($sql);
    $nro = $sq->num_rows(); 
    if($nro>0){
       return $sq->result();
      }else{
       return false;
      }      
   }


  public function eliminarIngrediente($idIngredienteD){
 	   $idAlimento = $this->session->userdata('idAlimento');  
 	   //obtenere el ingrediente a eliminar de alimento - ingrediente
	   $sql = "select ali.* from mn_alimento_ingrediente ali where id= " . $idIngredienteD;
	   $sq= $this->db->query($sql);
	   $row = $sq->row();
	   $idIngrediente = $row->id_ingrediente;
	  
        //eliminar alimento - ingrediente
		$this->db->where('id', $idIngredienteD);
		$this->db->delete('mn_alimento_ingrediente'); 
		
		//obtener todas las presentaciones con el ingrediente a eliminar para borrar de las presentaciones
		// solo ese ingrediente
		
		$sql = "select api.* from mn_alimento_presentacion_ingrediente api ";
		$sql.= "inner join mn_alimento_presentacion ap on api.id_alimento_presentacion = ap.id and ap.id_alimento=" . $idAlimento;
		$sql.= " where api.id_ingrediente=" . $idIngrediente;
		$sq= $this->db->query($sql);
		$row = $sq->result();
		
		//por cada presentacion eliminar el ingrediente seleccionado
		foreach($row as $key){ 
			$id = $key->id;
			$this->db->where('id', $id);
			$this->db->delete('mn_alimento_presentacion_ingrediente'); 
		}	
		return true;
  }




////////////// Presentacion /////////////////
public function obt_dataTablePresentacion($id){
	
$sql = "select  ap.*, tp.nombre as tipo_presentacion ";
$sql.= "from mn_alimento_presentacion ap ";
$sql.= "inner join mn_tipo_presentacion tp on ap.id_tipo_presentacion = tp.id ";
$sql.= "where ap.id_alimento = " .$id;
$sql.= " order by ap.orden ";
$sq= $this->db->query($sql);
return $sq->result();
$nro = $sq->num_rows(); 
if($nro>0){
   return $sq->result();
  }else{
   return false;
  }      
}


public function obt_presentacionIngrediente($id){
	$sql = "select  ap.*, tp.nombre as tipo_presentacion ";
	$sql.= "from mn_alimento_presentacion ap ";
	$sql.= "inner join mn_tipo_presentacion tp on ap.id_tipo_presentacion = tp.id ";
	$sql.= "where ap.id_alimento = " .$id;
	$sql.= " order by ap.orden ";
	$sq= $this->db->query($sql);
	return $sq->result();
}


public function obtModificarPresentacion($id){
  $sql = "select  ap.*, tp.nombre as tipo_presentacion ";
  $sql.= "from mn_alimento_presentacion ap ";
  $sql.= "inner join mn_tipo_presentacion tp on ap.id_tipo_presentacion = tp.id ";
  $sql.= "where ap.id = " .$id;
  $sq= $this->db->query($sql);
  return $sq->row();
}


public function obtPresentacion_x_alimento($idAlimento){
  $sql = "select  ap.* ";
  $sql.= "from mn_alimento_presentacion ap ";
  $sql.= "where ap.id_alimento = " .$idAlimento;
  $sq= $this->db->query($sql);
  return $sq->row();
}


public function obtIngrediente_x_presentacion($idPresentacion){
  $sql = "select  api.* ";
  $sql.= "from mn_alimento_presentacion_ingrediente api ";
  $sql.= "where api.id_alimento_presentacion = " .$idPresentacion;
  $sq= $this->db->query($sql);
  return $sq->row();
}



public function guardar_add_presentacion($data){
      $this->db->insert('mn_alimento_presentacion', $data);    
      return $this->db->insert_id();
}

public function guardar_mod_presentacion($id, $data){
    $this->db->where('id',$id);
    $this->db->update('mn_alimento_presentacion', $data);
    return $this->db->affected_rows();
}


public function obtIngredienteAlimento($idAlimento){
   $sql = "select mn_alimento_ingrediente.* ";
   $sql.= "from mn_alimento_ingrediente ";
   $sql.= "where mn_alimento_ingrediente.id_alimento = " . $idAlimento;
 
   //die($sql); 

   $sq= $this->db->query($sql);
   $nro = $sq->num_rows(); 
   if($nro>0){
       return $sq->result();
   }else{
       return false;
   } 
}  


public function guardarIngredientePresentacion($data){
      $this->db->insert('mn_alimento_presentacion_ingrediente', $data);    
      return $this->db->affected_rows();
}


////////////// Presentacion /////////////////
public function obt_dataTablePresentacionIngrediente($id){

$sql = "select api.*, ing.nombre as ingrediente, um.nombre as unidad_medida ";
$sql.= "from mn_alimento_presentacion_ingrediente api ";
$sql.= "inner join mn_ingrediente ing on api.id_ingrediente = ing.id ";
$sql.= "inner join mt_unidad_medida um on ing.id_unidad_medida = um.id ";
$sql.= "where api.id_alimento_presentacion = " . $id;
$sql.= " order by ing.nombre ";
$sq= $this->db->query($sql);
$nro = $sq->num_rows(); 
if($nro>0){
   return $sq->result();
  }else{
   return false;
  }      
}


public function valTipoPresentacion($idAlimento, $idTipoPresentacion){
     $sql ="select * ";
     $sql.="from mn_alimento_presentacion ";
     $sql.="where id_alimento = " . $idAlimento;
     $sql.=" and id_tipo_presentacion = " . $idTipoPresentacion;

     $sq= $this->db->query($sql);
     $row = $sq->row();    
     $nro = $sq->num_rows(); 
     if($nro==1){
         return 1;
     }else{
         return 0;
     }  

}


  public function guardar_mod_presentacion_adicional($id,$data){
    $this->db->where('id_alimento',$id);
    $this->db->update('mn_alimento_presentacion', $data);
    return $this->db->affected_rows();
  }

  public function guardar_mod_presentacion_ingrediente($id,$data){
    $this->db->where('id_alimento_presentacion',$id);
    $this->db->update('mn_alimento_presentacion_ingrediente', $data);
    return $this->db->affected_rows();
  }





}  //fin modal
