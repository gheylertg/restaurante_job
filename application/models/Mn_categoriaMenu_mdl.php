<?php
class Mn_categoriaMenu_mdl extends CI_Model {

	
	public function __construct(){
        parent::__construct();
       
    }


  public function obt_dataTable(){
      $idEmpresa = $this->session->userdata('idEmpresa');
      $idSucursal = $this->session->userdata('idSucursal');
  	  $sql = "select * ";
	    $sql.= "from mn_categoria_menu ";
      switch($this->session->userdata('administrador')){
      case 1: 
        $sql.="where administrador=1 ";
        break;
      case 2: 
        $sql.="where administrador=2 and id_empresa=" . $idEmpresa;
        break;
        
      default:
        $sql.="where id_sucursal = " . $idSucursal . " or administrador = 1 or ";
        $sql.="(administrador = 2 and id_empresa=" . $idEmpresa . ") ";
        break;
      }  
   	  $sql.= " order by nombre";

  	  $sq= $this->db->query($sql);
  	  return $sq->result();
  }


  public function obt_categoriaOrden(){
      $idEmpresa = $this->session->userdata('idEmpresa');
      $idSucursal = $this->session->userdata('idSucursal');
  	  $sql = "select * ";
	    $sql.= "from mn_categoria_menu ";
      switch($this->session->userdata('administrador')){
      case 1: 
        $sql.="where administrador=1 ";
        break;
      case 2: 
        $sql.="where administrador=2 and id_empresa=" . $idEmpresa;
        break;
        
      default:
        $sql.="where (id_sucursal = " . $idSucursal . " or administrador = 1 or ";
        $sql.="(administrador = 2 and id_empresa=" . $idEmpresa . ")) ";
        break;
      }  
      $sql.=" and activo = 1 and mostrar_menu = 1 ";
      
   	  $sql.= " order by orden";
   	  
   	  //die($sql);
   	  
  	  $sq= $this->db->query($sql);
  	  return $sq->result();
  }



   
    public function obtModificar($id){
		$this->db->select('*');
		$this->db->where('id',$id);
		return $this->db->get('mn_categoria_menu')->row();
    }


  public function guardar_add($data){
    $this->db->insert('mn_categoria_menu', $data);
    return $this->db->affected_rows();
  }




  public function guardar_mod($id,$data){
    $this->db->where('id',$id);
    $this->db->update('mn_categoria_menu', $data);
    return $this->db->affected_rows();
  }


  public function desactivar($id,$data){
      $this->db->where('id',$id);
      $this->db->update('mn_categoria_menu', $data);
      return $this->db->affected_rows();
  }
   
  public function valNombre($nombre){

	$sql ="select * ";
	$sql.="from mn_categoria_menu ";
	$sql.="where nombre = '" . $nombre . "' ";

  switch($this->session->userdata('administrador')){
  case 1:
      $sql.=" and administrador = 1 ";
      break;
  case 2:   
      $sql.=" and (administrador = 1 or (administrador = 2 and id_empresa= " . $this->session->userdata('idEmpresa').")) "; 
      break;
  default:
      $sql.=" and (administrador = 1 or (administrador = 2 and id_empresa = " . $this->session->userdata('idEmpresa') . ") ";
      $sql.=" or id_sucursal = " . $this->session->userdata('idSucursal') . ") ";
  }

	$sq= $this->db->query($sql);
	$row = $sq->row();		
      $nro = $sq->num_rows(); 
      if($nro==1){
          return 1;
      }else{
      	return 0;
      }  
  }


}
