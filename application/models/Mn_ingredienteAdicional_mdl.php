<?php
class Mn_ingredienteAdicional_mdl extends CI_Model {

  
  public function __construct(){
        parent::__construct();
       
    }

  public function obt_dataTable(){
	  
	  
	  
	  
	  
      $idEmpresa = $this->session->userdata('idEmpresa');
      $idSucursal = $this->session->userdata('idSucursal');
      $sql = "select mn_alimento.*, api.cantidad as cantidad_ingrediente, um.nombre as unidad_medida,  ap.precio ";
      $sql.= "from mn_alimento ";
      $sql.= "inner join mn_alimento_presentacion ap on mn_alimento.id = ap.id_alimento ";
      $sql.= "inner join mn_alimento_presentacion_ingrediente api on ap.id = api.id_alimento_presentacion ";
      $sql.= "inner join mn_ingrediente ing on api.id_ingrediente = ing.id ";
      $sql.= "inner join mt_unidad_medida um on ing.id_unidad_medida = um.id ";
      
      
      switch($this->session->userdata('administrador')){
      case 1:
          $sql.="where mn_alimento.administrador=1 ";
          break;
      case 2:
          $sql.="where mn_alimento.administrador=2 and mn_alimento.id_empresa = " . $idEmpresa;
          break;
      default:
          $sql.="where mn_alimento.id_sucursal = " . $idSucursal;
          break;
      }
    $sql.= " and mn_alimento.id_categoria_menu=2 ";  
    $sql.= " order by mn_alimento.nombre";
    $sq= $this->db->query($sql);
    return $sq->result();
  }
  
  
  public function obtener_ingrediente(){
      $idEmpresa = $this->session->userdata('idEmpresa');
      $idSucursal = $this->session->userdata('idSucursal');
      $sql = "select mn_alimento.*, api.cantidad as cantidad_ingrediente, um.nombre as unidad_medida,  ap.precio,  ";
      $sql.= "ing.nombre as ingrediente, um.id as id_unidad_medida ";
      $sql.= "from mn_alimento ";
      $sql.= "inner join mn_alimento_presentacion ap on mn_alimento.id = ap.id_alimento ";
      $sql.= "inner join mn_alimento_presentacion_ingrediente api on ap.id = api.id_alimento_presentacion ";
      $sql.= "inner join mn_ingrediente ing on api.id_ingrediente = ing.id ";
      $sql.= "inner join mt_unidad_medida um on ing.id_unidad_medida = um.id ";
      
      switch($this->session->userdata('administrador')){
      case 1:
          $sql.="where mn_alimento.administrador=1 ";
          break;
      case 2:
          $sql.="where mn_alimento.administrador=2 and mn_alimento.id_empresa = " . $idEmpresa;
          break;
      default:
          $sql.="where mn_alimento.id_sucursal = " . $idSucursal;
          break;
      }
    $sql.= " and mn_alimento.id_categoria_menu=2 and mn_alimento.activo=1";  
    $sql.= " order by mn_alimento.nombre";
    
    //die($sql);
    
    
    $sq= $this->db->query($sql);
    return $sq->result();
	  
	  
	  
  }
  
  
  public function incluir_ingredienteAdicional($data){
    $this->db->insert('dp_despacho_detalle_ingrediente_adicional', $data);
    return $this->db->affected_rows();	  
  }
  
    public function eliminar_IngredienteAdicional($id){
    $this->db->where('id_despacho_detalle', $id);
    $this->db->delete('dp_despacho_detalle_ingrediente_adicional'); 
  }

  
  
  
  public function desactivar($id,$data){
      $this->db->where('id',$id);
      $this->db->update('mn_alimento', $data);
      return $this->db->affected_rows();
  }

  public function reactivar($id,$data){
      $this->db->where('id',$id);
      $this->db->update('mn_alimento', $data);
      return $this->db->affected_rows();
  }
  
  
  
}//fin del modelos

   
