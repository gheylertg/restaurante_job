<?php
class Mn_ingrediente_mdl extends CI_Model {

  
  public function __construct(){
        parent::__construct();
       
    }





  public function obt_dataTable(){
      $idEmpresa = $this->session->userdata('idEmpresa');
      $idSucursal = $this->session->userdata('idSucursal');

      $sql = "select mn_ingrediente.*, mt_unidad_medida.nombre as unidad_medida ";
      $sql.= "from mn_ingrediente ";
      $sql.= "inner join mt_unidad_medida on mn_ingrediente.id_unidad_medida = mt_unidad_medida.id ";
      switch($this->session->userdata('administrador')){
      case 1:
          $sql.="where mn_ingrediente.administrador=1 ";
          break;
      case 2:
          $sql.="where mn_ingrediente.administrador=2 and mn_ingrediente.id_empresa = " . $idEmpresa;
          break;
      default:
          $sql.="where mn_ingrediente.id_sucursal = " . $idSucursal;
          break;
      }
    $sql.= " order by mn_ingrediente.nombre";
    $sq= $this->db->query($sql);
    return $sq->result();
  }


   
    public function obtModificar($id){
    $this->db->select('*');
    $this->db->where('id',$id);
    return $this->db->get('mn_ingrediente')->row();
    }
    
  public function obtIngredienteSB($idIngrediente){
    $sql = "select mn_ingrediente.*, mt_unidad_medida.nombre as unidad_medida, mt_unidad_medida.sigla ";
    $sql.= "from mn_ingrediente ";
    $sql.= "inner join mt_unidad_medida on mn_ingrediente.id_unidad_medida = mt_unidad_medida.id ";
    $sql.= "where mn_ingrediente.id=" . $idIngrediente;
    $sq= $this->db->query($sql);
    return $sq->row();
  }
  

      
    


  public function guardar_add($data){
    $this->db->insert('mn_ingrediente', $data);
    return $this->db->affected_rows();
  }




  public function guardar_mod($id,$data){
    $this->db->where('id',$id);
    $this->db->update('mn_ingrediente', $data);
    return $this->db->affected_rows();
  }


  public function desactivar($id,$data){
      $this->db->where('id',$id);
      $this->db->update('mn_ingrediente', $data);
      return $this->db->affected_rows();
  }


   
    public function valNombre($nombre){

    $sql ="select * ";
    $sql.="from mn_ingrediente ";
    $sql.="where nombre = '" . $nombre . "' ";
    //$sql.=" and administrador = " . $this->session->userdata('administrador') . " ";
    //$sql.=" and id_empresa = " . $this->session->userdata('idEmpresa') . " ";
    $sql.=" and id_sucursal = " . $this->session->userdata('idSucursal');
    $sq= $this->db->query($sql);
    $row = $sq->row();    
        $nro = $sq->num_rows(); 
        if($nro==1){
            return 1;
        }else{
          return 0;
        }  
    }
}
