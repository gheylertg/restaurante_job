<?php
class Mn_presentacionIngrediente_mdl extends CI_Model {

	
	public function __construct(){
        parent::__construct();
       
    }

public function mostrarPresentacion($idAlimento){
$sql = "select ap.*, um.nombre as unidad_medida, ";
$sql.= "tp.nombre as tipo_presentacion ";
$sql.= "from mn_alimento_presentacion ap ";
$sql.= "inner join mn_tipo_presentacion tp on ap.id_tipo_presentacion = tp.id ";
$sql.= "inner join mt_unidad_medida um on ap.id_unidad_medida = um.id ";
$sql.= "where ap.id_alimento = " . $idAlimento;
$sql.= " order by ap.orden ";
$sq= $this->db->query($sql);
return $sq->result();

}

public function obtPresentacion($idPresentacion){
$sql = "select ap.* ";
$sql.= "from mn_alimento_presentacion ap ";
$sql.= "where ap.id = " . $idPresentacion;
$sq= $this->db->query($sql);
return $sq->row();

}



public function obtModificarPresentacionIngrediente($idPresentacionIngrediente){
    $sql = "select  api.* ";
    $sql.= "from mn_alimento_presentacion_ingrediente api ";
    $sql.= "where api.id = " . $idPresentacionIngrediente;
    $sq= $this->db->query($sql);
    return $sq->row();
}

public function obt_cantidadIngrediente($idPresentacion, $idIngrediente){
    $sql = "select  api.* ";
    $sql.= "from mn_alimento_presentacion_ingrediente api ";
    $sql.= "where api.id_alimento_presentacion = " . $idPresentacion;
    $sql.= " and api.id_ingrediente = " . $idIngrediente;
    $sq= $this->db->query($sql);
    return $sq->row();
}


public function obt_presentacionIngrediente($idPresentacion){
    $sql = "select  api.* ";
    $sql.= "from mn_alimento_presentacion_ingrediente api ";
    $sql.= "where api.id_alimento_presentacion = " . $idPresentacion;
    $sq= $this->db->query($sql);
    return $sq->result();
}








////////////////////////////////  Verificar  /////////////////////////////////



public function obt_dataTablePresentacionIngrediente($id){
$sql = "select api.*, ing.nombre as ingrediente, um.nombre as unidad_medida ";
$sql.= "from mn_alimento_presentacion_ingrediente api ";
$sql.= "inner join mn_ingrediente ing on api.id_ingrediente = ing.id ";
$sql.= "inner join mt_unidad_medida um on ing.id_unidad_medida = um.id ";
$sql.= "where api.id_alimento_presentacion = " . $id;
$sql.= " order by ing.nombre ";
$sq= $this->db->query($sql);
$nro = $sq->num_rows(); 
if($nro>0){
   return $sq->result();
  }else{
   return false;
  }      
}






public function guardar_mod($id,$data){
  $this->db->where('id',$id);
  $this->db->update('mn_alimento_presentacion_ingrediente', $data);
  return $this->db->affected_rows();
}



/////////////////////////////////////////////////////////////////////////////////////////////////
  public function obt_dataTable(){
		$sql = "select  mn_alimento.*, mn_categoria_menu.nombre as categoria_menu ";
		$sql.= "from mn_alimento ";
    $sql.= "left join mn_categoria_menu on mn_alimento.id_categoria_menu = mn_categoria_menu.id  ";
		$sql.= "where mn_alimento.activo = 1 and mn_alimento.id_empresa=" . $this->session->userdata('idEmpresa');
		$sql.= " and mn_alimento.id_sucursal = " . $this->session->userdata('idSucursal');
		$sql.= " order by mn_alimento.nombre ";
		$sq= $this->db->query($sql);
  	    return $sq->result();
  }
  

  public function guardar_alimentoSB($data){
    $this->db->insert('mn_alimento', $data);
    return $this->db->insert_id();
  }

  public function guardar_detalleIngrediente($data){
    $this->db->insert('mn_alimento_ingrediente', $data);
  }  


   public function obtModificarAlimento($id){
      $this->db->select('*');
      $this->db->where('id',$id);
      return $this->db->get('mn_alimento')->row();
   }


   public function obtModificarIngredienteAlimento($id){
      $this->db->select('*');
      $this->db->where('id',$id);
      return $this->db->get('mn_alimento_ingrediente')->row();
   }   


  public function guardar_mod_alimento($id,$data){
    $this->db->where('id',$id);
    $this->db->update('mn_alimento', $data);
    return $this->db->affected_rows();
  }


    public function valNombre($nombre){

    $sql ="select * ";
    $sql.="from mn_alimento ";
    $sql.="where nombre = '" . $nombre . "' ";
    $sql.=" and id_empresa = " . $this->session->userdata('idEmpresa') . " ";
    $sql.=" and id_sucursal = " . $this->session->userdata('idSucursal');
    $sq= $this->db->query($sql);
    $row = $sq->row();    
        $nro = $sq->num_rows(); 
        if($nro==1){
            return 1;
        }else{
          return 0;
        }  
    }

   
   public function verAlimento($id){
       $sql = "select mn_alimento.* ";
       $sql.= "from mn_alimento ";
       $sql.= "where mn_alimento.id = " . $id;
      $sq= $this->db->query($sql);
      return $sq->row();    
   }  


  function valIngrediente($idAlimento, $idIngrediente){
     $sql ="select * ";
     $sql.="from mn_alimento_ingrediente ";
     $sql.="where id_alimento = " . $idAlimento;
     $sql.=" and id_ingrediente = " . $idIngrediente;

     //die($sql);
     $sq= $this->db->query($sql);
     $row = $sq->row();    
     $nro = $sq->num_rows(); 
     if($nro==1){
         return 1;
     }else{
         return 0;
     }  
  }


  
  public function guardar_ingrediente_add($data){
      $this->db->insert('mn_alimento_ingrediente', $data);	  
      return $this->db->affected_rows();

  }	  
  

  public function guardar_mod_ingredienteAlimento($id,$data){
    $this->db->where('id',$id);
    $this->db->update('mn_alimento_ingrediente', $data);
    return $this->db->affected_rows();
  }


    public function obt_dataTableIngrediente($id){
    $sql = "select  mai.*, ing.nombre as ingrediente, um.nombre as unidad_medida ";
    $sql.= "from mn_alimento_ingrediente mai ";
    $sql.= "left join mn_ingrediente ing on mai.id_ingrediente = ing.id ";
    $sql.= "left join mt_unidad_medida um on mai.id_unidad_medida = um.id ";
    $sql.= "where mai.id_alimento = " .$id;
    $sql.= " order by ing.nombre ";
    $sq= $this->db->query($sql);
    $nro = $sq->num_rows(); 
    if($nro>0){
       return $sq->result();
      }else{
       return false;
      }      
   }


  public function eliminarIngrediente($id){
    $this->db->where('id', $id);
    $this->db->delete('mn_alimento_ingrediente'); 
    return true;
     
  }




////////////// Presentacion /////////////////
public function obt_dataTablePresentacion($id){
$sql = "select  ap.*, tp.nombre as tipo_presentacion ";
$sql.= "from mn_alimento_presentacion ap ";
$sql.= "inner join mn_tipo_presentacion tp on ap.id_tipo_presentacion = tp.id ";
$sql.= "where ap.id_alimento = " .$id;
$sql.= " order by ap.orden ";
$sq= $this->db->query($sql);
$nro = $sq->num_rows(); 
if($nro>0){
   return $sq->result();
  }else{
   return false;
  }      
}





public function guardar_add_presentacion($data){
      $this->db->insert('mn_alimento_presentacion', $data);    
      return $this->db->insert_id();
}

public function guardar_mod_presentacion($id, $data){
    $this->db->where('id',$id);
    $this->db->update('mn_alimento_presentacion', $data);
    return $this->db->affected_rows();
}


public function obtIngredienteAlimento($idAlimento){
   $sql = "select mn_alimento_ingrediente.* ";
   $sql.= "from mn_alimento_ingrediente ";
   $sql.= "where mn_alimento_ingrediente.id_alimento = " . $idAlimento;
 
   //die($sql); 

   $sq= $this->db->query($sql);
   $nro = $sq->num_rows(); 
   if($nro>0){
       return $sq->result();
   }else{
       return false;
   } 
}  


public function guardarIngredientePresentacion($data){
      $this->db->insert('mn_alimento_presentacion_ingrediente', $data);    
      return $this->db->affected_rows();
}


////////////// Presentacion /////////////////



public function valTipoPresentacion($idAlimento, $idTipoPresentacion){
     $sql ="select * ";
     $sql.="from mn_alimento_presentacion ";
     $sql.="where id_alimento = " . $idAlimento;
     $sql.=" and id_tipo_presentacion = " . $idTipoPresentacion;

     $sq= $this->db->query($sql);
     $row = $sq->row();    
     $nro = $sq->num_rows(); 
     if($nro==1){
         return 1;
     }else{
         return 0;
     }  

}



}  //fin modal
