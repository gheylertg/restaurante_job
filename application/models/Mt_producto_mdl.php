<?php
class Mt_producto_mdl extends CI_Model {

	
	public function __construct(){
        parent::__construct();
       
    }

   
   

   
   public function obt_dataTable(){
        $idEmpresa = $this->session->userdata('idEmpresa');
        $idSucursal = $this->session->userdata('idSucursal');		
  		$sql = "select p.*, cp.nombre as categoria_producto, um.nombre as unidad_medida  ";
 	    $sql.= "from mt_producto p ";
		$sql.= "inner join mt_categoria_producto cp on cp.id = p.id_categoria_producto ";
		$sql.= "inner join mt_unidad_medida um on um.id = p.id_unidad_medida ";
      
		switch($this->session->userdata('administrador')){
        case 1:
            $sql.="where p.administrador=1 ";
          break;

      case 2:
          $sql.="where p.administrador=2 and p.id_empresa = " . $idEmpresa;
          break;

      case 0:
          $sql.="where p.administrador=0 and p.id_sucursal = " . $idSucursal;
          break;
      }
	  $sql.= " order by p.nombre ";
	  $sq= $this->db->query($sql);
	  return $sq->result();
   }


    public function obtVerProducto($id){
        $sql = "select p.*, cp.nombre as categoria_producto, um.nombre as unidad_medida  ";
        $sql.= "from mt_producto p ";
        $sql.= "inner join mt_categoria_producto cp on cp.id = p.id_categoria_producto ";
        $sql.= "inner join mt_unidad_medida um on um.id = p.id_unidad_medida ";
        $sql.= "where p.id=".$id;
        $sq= $this->db->query($sql);
    return $sq->row();


    }


    public function obtProductoIn($id){
        $sql = "select p.*, um.nombre as unidad_medida  ";
        $sql.= "from mt_producto p ";
        $sql.= "inner join mt_unidad_medida um on um.id = p.id_unidad_medida ";
        $sql.= "where p.id=" .  $id;
        $sq= $this->db->query($sql);
        return $sq->row();
    }


    //Producto de compra
    public function obtProductoCp($id){
        $sql = "select p.*, um.nombre as unidad_medida  ";
        $sql.= "from mt_producto p ";
        $sql.= "inner join mt_unidad_medida um on um.id = p.id_unidad_medida ";
        $sql.= "where p.id=" .  $id;
        $sq= $this->db->query($sql);
        return $sq->row();
    }

    
    public function obtModificar($id){
		$this->db->select('*');
		$this->db->where('id',$id);
		return $this->db->get('mt_producto')->row();

    }


  public function guardar_add($data){
      $this->db->insert('mt_producto', $data);
      return $this->db->affected_rows();
  }


  public function guardar_add_id($data){
      $this->db->insert('mt_producto', $data);
      return $this->db->insert_id();    
  }


  public function guardar_mod($id,$data){
      $this->db->where('id',$id);
      $this->db->update('mt_producto', $data);
      return $this->db->affected_rows();
  }


  public function desactivar($id,$data){
      $this->db->where('id',$id);
      $this->db->update('mt_producto', $data);
      return $this->db->affected_rows();
  }


   
    public function valNombre($nombre){
	    $sql ="select * ";
		$sql.="from mt_producto ";
		$sql.="where nombre = '" . $nombre . "' ";
		$sq= $this->db->query($sql);
		$row = $sq->row();		
        $nro = $sq->num_rows(); 
        if($nro==1){
            return 1;
        }else{
        	return 0;
        }  
    }
    

}
