<?php
class Mt_tipoDinero_mdl extends CI_Model {

	
	public function __construct(){
        parent::__construct();
       
    }





  public function obt_dataTable(){
  		$sql = "select * ";
  	    $sql.= "from mt_tipo_dinero ";
	    $sql.= " order by nombre";
	    $sq= $this->db->query($sql);
	    return $sq->result();
  }


  public function obt_TipoDineroOrden(){
  		$sql = "select * ";
  	    $sql.= "from mt_tipo_dinero ";
  	    $sql.= "where mt_tipo_dinero.activo=1 ";
	    $sql.= " order by orden";
	    $sq= $this->db->query($sql);
	    return $sq->result();
  }

   
    public function obtModificar($id){
		$this->db->select('*');
		$this->db->where('id',$id);
		return $this->db->get('mt_tipo_dinero')->row();
    }


  public function guardar_add($data){
    $this->db->insert('mt_tipo_dinero', $data);
    return $this->db->affected_rows();
  }



  public function guardar_mod($id,$data){
    $this->db->where('id',$id);
    $this->db->update('mt_tipo_dinero', $data);
    return $this->db->affected_rows();
  }


  public function desactivar($id,$data){
      $this->db->where('id',$id);
      $this->db->update('mt_tipo_dinero', $data);
      return $this->db->affected_rows();
  }


   
    public function valNombre($nombre){

		$sql ="select * ";
		$sql.="from mt_tipo_dinero ";
		$sql.="where nombre = '" . $nombre . "' ";
		$sq= $this->db->query($sql);
		$row = $sq->row();		
        $nro = $sq->num_rows(); 
        if($nro==1){
            return 1;
        }else{
        	return 0;
        }  
    }
}
