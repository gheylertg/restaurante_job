<?php
class Mt_tipoProducto_mdl extends CI_Model {

	
	public function __construct(){
        parent::__construct();
       
    }




  public function obt_dataTable(){
      $idEmpresa = $this->session->userdata('idEmpresa');
      $idSucursal = $this->session->userdata('idSucursal');

  	  $sql = "select * ";
	  $sql.= "from mt_tipo_producto ";
      switch($this->session->userdata('administrador')){
      case 1:
          $sql.="where administrador=1 ";
          break;
      case 2:
          $sql.="where administrador=2 and id_empresa = " . $idEmpresa;
          break;
      case 0:
          $sql.="where administrador=0 and id_sucursal = " . $idSucursal;
          break;
      }
	  $sql.= " order by nombre";
	  $sq= $this->db->query($sql);
	  return $sq->result();
  }


   
    public function obtModificar($id){
		$this->db->select('*');
		$this->db->where('id',$id);
		return $this->db->get('mt_tipo_producto')->row();
    }


  public function guardar_add($data){
    $this->db->insert('mt_tipo_producto', $data);
    return $this->db->affected_rows();
  }



  public function guardar_mod($id,$data){
    $this->db->where('id',$id);
    $this->db->update('mt_tipo_producto', $data);
    return $this->db->affected_rows();
  }


  public function desactivar($id,$data){
      $this->db->where('id',$id);
      $this->db->update('mt_tipo_producto', $data);
      return $this->db->affected_rows();
  }


   
    public function valNombre($nombre){

		$sql ="select * ";
		$sql.="from mt_tipo_producto ";
		$sql.="where nombre = '" . $nombre . "' ";
		$sq= $this->db->query($sql);
		$row = $sq->row();		
        $nro = $sq->num_rows(); 
        if($nro==1){
            return 1;
        }else{
        	return 0;
        }  
    }
}
