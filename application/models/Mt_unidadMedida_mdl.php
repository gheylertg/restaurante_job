<?php
class Mt_unidadMedida_mdl extends CI_Model {

	
	public function __construct(){
        parent::__construct();
       
    }



  public function obt_dataTable(){
  		$sql = "select * ";
		  $sql.= "from mt_unidad_medida ";
		  $sql.= "order by nombre";
		  $sq= $this->db->query($sql);
		  return $sq->result();
  }


   
    public function obtModificar($id){
		$this->db->select('*');
		$this->db->where('id',$id);
		return $this->db->get('mt_unidad_medida')->row();
    }


  public function guardar_add($data){
    $this->db->insert('mt_unidad_medida', $data);
    return $this->db->affected_rows();
  }



  public function guardar_mod($id,$data){
    $this->db->where('id',$id);
    $this->db->update('mt_unidad_medida', $data);
    return $this->db->affected_rows();
  }


  public function desactivar($id,$data){
      $this->db->where('id',$id);
      $this->db->update('mt_unidad_medida', $data);
      return $this->db->affected_rows();
  }


   
    public function valNombre($nombre){

		$sql ="select * ";
		$sql.="from mt_unidad_medida ";
		$sql.="where nombre = '" . $nombre . "' ";
		$sq= $this->db->query($sql);
		$row = $sq->row();		
        $nro = $sq->num_rows(); 
        if($nro==1){
            return 1;
        }else{
        	return 0;
        }  
    }
}
