<?php
class Sg_administradorEmpresa_mdl extends CI_Model {

	
	public function __construct(){
        parent::__construct();
       
    }



    public function valLoginEmp($run,$password){
        $sql ="select sg_usuario.* ";
        $sql.="from sg_usuario ";
        $sql.="inner join sg_usuario_administrador ua on sg_usuario.id = ua.id_usuario and ua.activo=1 and ua.administrador=2 "; 
        $sql.=" where sg_usuario.run = '" . $run . "' ";
        $sql.="and sg_usuario.password = '" . $password . "' ";
        $sql.="and sg_usuario.activo = 1 ";
        $sq= $this->db->query($sql);
        $nro = $sq->num_rows(); 
        return $nro;
    }

    public function infUsuarioEmpresas($run,$password){
        $sql ="select sg_usuario.*, cf_empresa.id as id_empresa, cf_empresa.nombre as empresa, cf_empresa.logo ";
        $sql.="from sg_usuario ";
        $sql.="inner join sg_usuario_administrador ua on sg_usuario.id = ua.id_usuario and ua.activo=1 and ua.administrador=2 "; 
        $sql.="inner join cf_empresa on ua.id_empresa = cf_empresa.id ";         
        $sql.=" where sg_usuario.run = '" . $run . "' ";
        $sql.="and sg_usuario.password = '" . $password . "' ";
        $sql.="and sg_usuario.activo = 1 ";
        $sq= $this->db->query($sql);
        return $sq->result();
    }





    public function informacionUnicaEmp($run,$password){
        $sql ="select sg_usuario.* ";
        $sql.="from sg_usuario ";
        $sql.="inner join sg_usuario_administrador ua on sg_usuario.id = ua.id_usuario and ua.activo=1 and ua.administrador=2 "; 
        $sql.=" where sg_usuario.run = '" . $run . "' ";
        $sql.="and sg_usuario.password = '" . $password . "' ";
        $sql.="and sg_usuario.activo = 1 ";
        $sq= $this->db->query($sql);
        $row = $sq->row();
        return $row;
    }





    public function obtInfUsuario($usuario, $password){
        $sql ="select sg_usuario.*, ua.id_empresa, ua.id_sucursal ";
        $sql.="from sg_usuario ";
        $sql.="inner join sg_usuario_administrador ua on sg_usuario.id = ua.id_usuario and ua.administrador=2 ";
        $sql.="where sg_usuario.login = '" . $usuario . "' ";
        $sql.="and sg_usuario.password = '" . $password . "' ";
        $sq= $this->db->query($sql);
        $row = $sq->row();
        return $row;    
    }


    public function obtInfUsuarioId($idUsuario){
        $sql ="select sg_usuario.*, ua.id_empresa, ua.id_sucursal ";
        $sql.="from sg_usuario ";
        $sql.="inner join sg_usuario_administrador ua on sg_usuario.id = ua.id_usuario and ua.administrador=2 ";
        $sql.="where sg_usuario.id = " . $idUsuario;
        $sq= $this->db->query($sql);
        $row = $sq->row();
        return $row;    
    }


    public function obtInfUsuarioEmpresa($idUsuario, $idEmpresa){
        $sql ="select sg_usuario.*, ua.id_empresa, ua.id_sucursal ";
        $sql.="from sg_usuario ";
        $sql.="inner join sg_usuario_administrador ua on sg_usuario.id = ua.id_usuario and ";
        $sql.="ua.administrador=2 and ua.id_empresa = " . $idEmpresa;
        $sql.=" where sg_usuario.id = " . $idUsuario;
        $sq= $this->db->query($sql);
        $row = $sq->row();
        return $row;    
    }





  public function obt_dataTable(){
      $sql = "select sg_usuario_administrador.*, sg_usuario.run, sg_usuario.nombre, sg_usuario.apellido,  ";
      $sql.= "cf_empresa.nombre as empresa ";
      $sql.= "from sg_usuario_administrador ";
      $sql.= "inner join sg_usuario on sg_usuario_administrador.id_usuario = sg_usuario.id ";
      $sql.= "inner join cf_empresa on sg_usuario_administrador.id_empresa = cf_empresa.id ";


      if($this->session->userdata('administrador')==1){
          $sql.= "where sg_usuario_administrador.administrador=2 and sg_usuario_administrador.id_empresa_create=1";
      }else{
          $sql.= "where sg_usuario_administrador.administrador=2 and ";
          $sql.= "sg_usuario_administrador.id_empresa=".$this->session->userdata('idEmpresa');
      }  
      $sql.= " order by sg_usuario.nombre ";
      $sq= $this->db->query($sql);
      return $sq->result();
  }

  
  public function guardar_add($data){
    $this->db->insert('sg_usuario_administrador', $data);
    return $this->db->insert_id();
  }

  public function desactivar($id,$data){
      $this->db->where('id',$id);
      $this->db->update('sg_usuario_administrador', $data);
      return $this->db->affected_rows();
  }  


}

