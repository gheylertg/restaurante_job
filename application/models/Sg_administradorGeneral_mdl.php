<?php
class Sg_administradorGeneral_mdl extends CI_Model {

	
	public function __construct(){
        parent::__construct();
       
    }
    


    public function valLoginAdm($run,$password){
        $sql ="select sg_usuario.* ";
        $sql.="from sg_usuario ";
        $sql.="inner join sg_usuario_administrador ua on sg_usuario.id = ua.id_usuario and ua.activo=1 and ua.administrador=1 "; 
        $sql.="where sg_usuario.run = '" . $run . "' ";
        $sql.="and sg_usuario.password = '" . $password . "' ";
        $sql.="and sg_usuario.activo = 1 ";

        $sq= $this->db->query($sql);
        $row = $sq->row();    
        $nro = $sq->num_rows(); 
        if($nro>0){
            return $row;
        }else{
          return false;
        }  
    }


    public function obtInfUsuario($idUsuario){
        $sql ="select sg_usuario.* ";
        $sql.="from sg_usuario ";
        $sql.="where sg_usuario.id = " . $idUsuario;
        $sq= $this->db->query($sql);
        $row = $sq->row();
        return $row;    
    }


  public function obt_dataTable(){
      $sql = "select sg_usuario_administrador.*, sg_usuario.run, sg_usuario.nombre, sg_usuario.apellido ";
      $sql.= "from sg_usuario_administrador ";
      $sql.= "inner join sg_usuario on sg_usuario_administrador.id_usuario = sg_usuario.id ";
      $sql.= "where sg_usuario_administrador.administrador=1 ";
      $sql.= "order by sg_usuario.nombre ";
      $sq= $this->db->query($sql);
      return $sq->result();
  }

  
  public function guardar_add($data){
    $this->db->insert('sg_usuario_administrador', $data);
    return $this->db->insert_id();
  }

  public function desactivar($id,$data){
      $this->db->where('id',$id);
      $this->db->update('sg_usuario_administrador', $data);
      return $this->db->affected_rows();
  }  


}

