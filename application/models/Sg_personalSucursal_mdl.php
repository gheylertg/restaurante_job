<?php
class Sg_personalSucursal_mdl extends CI_Model {

	
	public function __construct(){
        parent::__construct();
       
    }



    public function valLoginSuc($idEmpresa, $idSucursal, $login,$password){
        $sql ="select sg_usuario.* ";
        $sql.="from sg_usuario ";
        $sql.="inner join sg_usuario_administrador ua on sg_usuario.id = ua.id_usuario and ";
        $sql.="ua.activo=1 and ua.administrador>2 and "; 
        $sql.="ua.id_empresa = " . $idEmpresa . " and ua.id_sucursal=".$idSucursal;
        $sql.=" where sg_usuario.login = '" . $login . "' ";
        $sql.="and sg_usuario.password = '" . $password . "' ";
        $sql.="and sg_usuario.activo = 1 ";


        $sq= $this->db->query($sql);
        $row = $sq->row();    
        $nro = $sq->num_rows(); 
        if($nro>0){
            return 1;
        }else{
          return 0;
        }  
    }


    public function obtInfUsuario($usuario, $password){
        $sql ="select sg_usuario.*, ua.id_empresa, ua.id_sucursal, ua.id_rol, ua.administrador ";
        $sql.="from sg_usuario ";
        $sql.="inner join sg_usuario_administrador ua on sg_usuario.id = ua.id_usuario and ua.administrador>2 ";
        $sql.="where sg_usuario.login = '" . $usuario . "' ";
        $sql.="and sg_usuario.password = '" . $password . "' ";
        $sq= $this->db->query($sql);
        $row = $sq->row();
        return $row;    
    }



  public function obt_dataTable(){
      $sql = "select sg_usuario_administrador.*, sg_usuario.nombre, sg_usuario.apellido,  ";
      $sql.= "cf_empresa.nombre as empresa, cf_sucursal.nombre as sucursal, sg_rol.nombre as rol ";
      $sql.= "from sg_usuario_administrador ";
      $sql.= "inner join sg_usuario on sg_usuario_administrador.id_usuario = sg_usuario.id ";
      $sql.= "inner join cf_empresa on sg_usuario_administrador.id_empresa = cf_empresa.id ";
      $sql.= "inner join cf_sucursal on sg_usuario_administrador.id_sucursal = cf_sucursal.id ";
      $sql.= "inner join sg_rol on sg_usuario_administrador.id_rol = sg_rol.id ";
      $sql.= "where sg_usuario_administrador.administrador > 2 and ";
      $sql.= "sg_usuario_administrador.id_empresa=".$this->session->userdata('idEmpresa');
      $sql.= " and sg_usuario_administrador.id_sucursal=".$this->session->userdata('idSucursal');
      $sql.= " order by sg_usuario.nombre ";
      $sq= $this->db->query($sql);
      return $sq->result();
  }

   public function guardar_add($data){
    $this->db->insert('sg_usuario_administrador', $data);
    return $this->db->insert_id();
  }


 ////////////////////////////////////////////////////////////////////////////////// 

  public function desactivar($id,$data){
      $this->db->where('id',$id);
      $this->db->update('sg_usuario_administrador', $data);
      return $this->db->affected_rows();
  }  


}

