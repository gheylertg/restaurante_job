<?php
class Sg_rolModulo_mdl extends CI_Model {

	
	public function __construct(){
        parent::__construct();
       
    }

    public function obt_rolModulo(){
		$sql = "select  rm.*, sg_rol.nombreas rol, sg_modulo.nombre as modulo ";
		$sql.= "from sg_rol_modulo rm ";
    $sql.= "inner join sg_rol on sg.id_rol = sg_rol.id and sg_rol.activo=1 ";
    $sql.= "inner join sg_modulo on sg.id_modulo = sg_modulo.id and sg_modulo.activo=1 ";
    $sql.= "where sg_rol_modulo.activo = 1 ";
	  $sql.= "order by sg_rol.nombre, sg_modulo.nombre ";
		  $sq= $this->db->query($sql);
		  return $sq->result();
    }

    public function obt_modulo_x_rol($id){
        //La varoable $id = tiene el id del rol
        //Aqui se obtiene todos los modulo asociados al rol y activo

    $sql = "select  rm.*, sg_modulo.nombre as modulo ";
    $sql.= "from sg_rol_modulo rm ";
    $sql.= "inner join sg_modulo on rm.id_modulo = sg_modulo.id and sg_modulo.activo=1 ";
    $sql.= "where rm.id_rol = " . $id;
    $sql.= " order by sg_modulo.nombre ";
      $sq= $this->db->query($sql);
      return $sq->result();
    }

    public function obt_permiso_x_modulo($id){
        //La varoable $id = tiene el id del rol_modulo
        //Aqui se obtiene todos los permiso asociados al al rol, modulo activo

    $sql = "select  rmp.*, sg_permiso.nombre as permiso ";
    $sql.= "from sg_rol_modulo_permiso rmp ";
    $sql.= "inner join sg_permiso on rmp.id_permiso = sg_permiso.id and sg_permiso.activo=1 ";
    $sql.= "where rmp.id_rol_modulo = " . $id; 
    $sql.= " order by sg_permiso.orden ";
    $sq= $this->db->query($sql);
      return $sq->result();
    }


    public function obt_proceso_x_modulo($id){
        //La varoable $id = tiene el id del rol_modulo
        //Aqui se obtiene todos los permiso asociados al al rol, modulo activo
		$sql = "select  rmp.*, sg_sub_modulo.nombre as proceso ";
		$sql.= "from sg_rol_modulo_proceso rmp ";
		$sql.= "inner join sg_sub_modulo on rmp.id_sub_modulo = sg_sub_modulo.id and sg_sub_modulo.activo=1 ";
		$sql.= "where rmp.id_rol_modulo = " . $id; 
		$sql.= " order by sg_sub_modulo.orden ";
		$sq= $this->db->query($sql);
        return $sq->result();
    }




    function add_subModulo($data){
        $this->db->insert('sg_rol_modulo_proceso', $data);
    }
    


  public function obt_permiso_x_moduloEmpresa($idModulo, $idRol){
          //La varoable $id = tiene el id del rol_modulo
          //Aqui se obtiene todos los permiso asociados al al rol, modulo activo

      $sql = "select  rmp.*, sg_permiso.nombre as permiso ";
      $sql.= "from sg_rol_modulo_permiso rmp ";
      $sql.= "inner join sg_permiso on rmp.id_permiso = sg_permiso.id and sg_permiso.activo=1 ";
      $sql.= "inner join sg_rol_modulo rm on rmp.id_rol_modulo = rm.id and rm.id_rol = " . $idRol;
      $sql.= " and rm.id_modulo = " . $idModulo; 
      $sql.= " order by sg_permiso.orden ";
      $sq= $this->db->query($sql);
      return $sq->result();
  }



  public function guardar_add($data){

    $this->db->insert('sg_rol_modulo', $data);

    return $this->db->insert_id();
  }


  public function guardar_add_permiso($data){
    $this->db->insert('sg_rol_modulo_permiso', $data);
    //return true;
  }


    public function obtModificar($id){
    $this->db->select('*');
    $this->db->where('id',$id);
    return $this->db->get('sg_rol_modulo')->row();
    }


public function obtModificar_rolModulo($id_rolModulo){
    $sql = "select sg_modulo.* ";
    $sql.= "from sg_rol_modulo rm ";
    $sql.= "inner join sg_modulo on rm.id_modulo = sg_modulo.id ";
    $sql.= "where rm.id = " . $id_rolModulo;
    $sq= $this->db->query($sql);
    return $sq->row();    

}

public function obt_permiso_modulo($id_rolModulo){
    $this->db->select('*');
    $this->db->where('id_rol_modulo',$id_rolModulo);
    return $this->db->get('sg_rol_modulo_permiso')->result();
}

public function eliminar_permiso($idPermiso, $id_rolModulo){
    $this->db->where('id_permiso', $idPermiso);
    $this->db->where('id_rol_modulo', $id_rolModulo);
    $this->db->delete('sg_rol_modulo_permiso');  
}



 public function eliminar($id){
      //borrar rol_modulo
      $this->db->where('id',$id);
      $this->db->delete('sg_rol_modulo'); 

      //borrar rol_modulo_permiso
      $this->db->where('id_rol_modulo',$id);
      $this->db->delete('sg_rol_modulo_permiso');

      //borrar rol_modulo_proceso
      $this->db->where('id_rol_modulo',$id);
      $this->db->delete('sg_rol_modulo_proceso');





  }


function obt_subModulo_x_modulo($idRolModulo){
    $sql = "select sg_sub_modulo.*, rp.id as id_modulo_proceso ";
    $sql.= "from sg_rol_modulo_proceso rp ";
    $sql.= "inner join sg_sub_modulo on rp.id_sub_modulo = sg_sub_modulo.id ";
    $sql.= "where rp.id_rol_modulo = " . $idRolModulo;
    $sq= $this->db->query($sql);
    return $sq->result();    
}



 public function eliminarProceso($id){
      //borrar rol_modulo
      $this->db->where('id',$id);
      $this->db->delete('sg_rol_modulo_proceso'); 

  }


 public function asociarProceso($data){
    $this->db->insert('sg_rol_modulo_proceso', $data);

  }
    


}
