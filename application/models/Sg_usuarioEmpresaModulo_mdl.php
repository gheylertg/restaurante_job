<?php
class Sg_usuarioEmpresaModulo_mdl extends CI_Model {

	
	public function __construct(){
        parent::__construct();
       
    }
    

    
   function obtPermiso($idUsuarioEmpresa){
        $sql = "select uem.*, sg_modulo.nombre, sg_modulo.icono_fas_fa, sg_modulo.id as id_modulo, sg_modulo.identificador "; 
        $sql.= "from sg_usuario_empresa_modulo as uem ";
        $sql.= " inner join sg_modulo on uem.id_modulo = sg_modulo.id and sg_modulo.activo=1 ";
        $sql.= " where uem.activo = 1 and id_usuario_empresa=" . $idUsuarioEmpresa;
        $sql.= " order by sg_modulo.orden ";
        $rows = $this->db->query($sql);
        return $rows->result();   		
   }


/*   function obtPermiso($idUsuario, $idEmpresa, $idSucursal){
        $sql = "select uem.*, sg_modulo.nombre, sg_modulo.icono_fas_fa, sg_modulo.id as id_modulo, sg_modulo.identificador, ue.id as id_usuario_empresa "; 
        $sql.= "from sg_usuario_empresa_modulo as uem ";
        $sql.= "inner join sg_usuario_empresa ue on uem.id_usuario_empresa = ue.id and ";
        $sql.= "ue.id_usuario=". $idUsuario . " and ue.id_empresa = " . $idEmpresa;
        $sql.= " and ue.id_sucursal = " . $idSucursal;
        $sql.= " inner join sg_modulo on uem.id_modulo = sg_modulo.id and sg_modulo.activo=1 ";
        $sql.= " where uem.activo = 1 ";
        $sql.= " order by sg_modulo.orden ";
        $rows = $this->db->query($sql);
        return $rows->result();       
   }
  */ 



   public function obtAcciones($idUEM, $idModulo){
        $sql = "select uemp.*, sg_permiso.accion "; 
        $sql.= "from sg_usuario_empresa_modulo_permiso as uemp ";
        $sql.= "inner join sg_permiso on uemp.id_permiso = sg_permiso.id ";
        $sql.= " where uemp.id_uem = " . $idUEM . " and uemp.id_modulo =" . $idModulo;

        $rows = $this->db->query($sql);
        
        return $rows->result();   



   }

public function obtAccionesMod($idUEM, $idModulo){
        $sql = "select uemp.*, sg_permiso.accion, sg_permiso.nombre as accion "; 
        $sql.= "from sg_usuario_empresa_modulo_permiso as uemp ";
        $sql.= "inner join sg_permiso on uemp.id_permiso = sg_permiso.id ";
        $sql.= " where uemp.id_uem = " . $idUEM . " and uemp.id_modulo =" . $idModulo;
        $sql.= " order by sg_permiso.accion ";

        $rows = $this->db->query($sql);
        
        return $rows->result();         

   }


   


   
   function obtPermisoSM($idUsuarioEmpresa){

      $sql = "SELECT uemp.*, sm.nombre as sub_modulo, sm.id as id_sub_modulo, sm.url_sub_modulo ";
      $sql.= "FROM sg_usuario_empresa_modulo_proceso uemp ";
      $sql.= "inner join sg_sub_modulo sm on uemp.id_sub_modulo = sm.id ";
      $sql.= "inner join sg_usuario_empresa_modulo uem on uemp.id_uem = uem.id ";
      $sql.= "and uem.id_usuario_empresa = " . $idUsuarioEmpresa;
      $sql.= " where uemp.activo = 1 ";
      $sql.= "order by sm.orden ";

      $rows = $this->db->query($sql);
      return $rows->result();   		
   }



   function obtModulo_x_usuario($idUsuarioEmpresa){
        $sql = "select  uem.*, sg_modulo.nombre ";
        $sql.= "from sg_usuario_empresa_modulo uem ";
        $sql.= "inner join sg_modulo on uem.id_modulo = sg_modulo.id ";
        $sql.= "where uem.id_usuario_empresa = " . $idUsuarioEmpresa;
        $sql.= " order by sg_modulo.nombre";


        $rows = $this->db->query($sql);
        return $rows->result();         
   }
   
   function obtDatos_UEM($idUEM){
	  $sql = "select uem.*, sg_modulo.nombre as modulo, ue.id_rol ";
	  $sql.= "from sg_usuario_empresa_modulo uem ";
	  $sql.= "inner join sg_modulo on uem.id_modulo = sg_modulo.id ";
	  $sql.= "inner join sg_usuario_empresa ue on uem.id_usuario_empresa = ue.id ";
	  $sql.= "where uem.id = " . $idUEM ;

      $rows = $this->db->query($sql);
      return $rows->row();         	  
	   
	   
	   
	   
   }
   
	public function obt_permiso_modulo($id_uem){
		$this->db->select('*');
		$this->db->where('id_uem',$id_uem);
		return $this->db->get('sg_usuario_empresa_modulo_permiso')->result();
	}
	
	

  public function guardar_add_permiso($data){
    $this->db->insert('sg_usuario_empresa_modulo_permiso', $data);
    //return true;
  }
  
  
  public function eliminar_permiso($idPermiso, $idUEM){            
      $this->db->where('id_uem', $idUEM);
      $this->db->where('id_permiso', $idPermiso);
      $this->db->delete('sg_usuario_empresa_modulo_permiso');
   
   
  }
  
  
  public function obtener_usuarioEmpresa($idUEM){

	  $this->db->select('*');
	  $this->db->where('id', $idUEM);
  	  return $this->db->get('sg_usuario_empresa_modulo')->row();
  }


}
