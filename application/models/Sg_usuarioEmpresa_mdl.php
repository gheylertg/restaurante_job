<?php
class Sg_usuarioEmpresa_mdl extends CI_Model {

	
	public function __construct(){
        parent::__construct();
       
    }

    public function obt_dataTable(){

    //  $this->db->select('*');
    //  $this->db->where('activo',1);
      //$this->db->where('no_modificar',0);

    $sql = "select sg_usuario.* ";
    $sql.= "from sg_usuario ";

    if($this->session->userdata('idRol')==1){
       $sql.="inner join sg_usuario_administrador ua on sg_usuario.id = ua.id_usuario and ";
       $sql.="ua.activo=1 and ua.id_empresa_create = " . $this->session->userdata('idEmpresa');
    }else{
       $sql.="inner join sg_usuario_administrador ua on sg_usuario.id = ua.id_usuario and ";
       $sql.="ua.activo=1 and (ua.id_empresa = " . $this->session->userdata('idEmpresa') . " or ";
       $sql.="ua.id_empresa_create = " . $this->session->userdata('idEmpresa') . ")";
    }  

    $sql.= " group by sg_usuario.id ";
    $sql.= "order by sg_usuario.nombre ";

    //echo "Id Rol: " . $this->session->userdata('idRol');
    //die("<br>".$sql);    
    $sq= $this->db->query($sql);
    return $sq->result();
    }
    

    public function obtUsuarioEmpresa($idUsuarioEmpresa){
    $sql = "select  ue.*, cf_empresa.nombre as empresa, sg_usuario.nombre as usuario, sg_usuario.apellido, ";
    $sql.= "sg_usuario.login, sg_usuario.id as id_usuario1, cf_sucursal.nombre as sucursal ";
    $sql.= "from sg_usuario_empresa ue ";
    $sql.= "inner join cf_empresa on ue.id_empresa = cf_empresa.id ";
    $sql.= "inner join sg_usuario on ue.id_usuario = sg_usuario.id ";
    $sql.= "inner join cf_sucursal on ue.id_sucursal = cf_sucursal.id ";
    $sql.= "where ue.id = " . $idUsuarioEmpresa;
    $sq= $this->db->query($sql);
    return $sq->row();
    }

    public function obtDatosUsuarioEmpresa($idUsuario, $idEmpresa, $idSucursal){
    $sql = "select  ue.*, cf_empresa.nombre as empresa, sg_usuario.nombre as usuario, sg_usuario.apellido, sg_rol.nombre as rol,  ";
    $sql.= "sg_usuario.login, sg_usuario.id as id_usuario1, cf_sucursal.nombre as sucursal, cf_empresa.logo, cf_empresa.rut, cf_empresa.imagen, ";
    $sql.= "cf_empresa.direccion, cf_sucursal.rut as sucursal_rut, cf_sucursal.direccion as sucursal_direccion, ";
    $sql.= "cf_sucursal.correo as sucursal_correo, cf_sucursal.telefono_local as sucursal_telefono ";
    
    
    $sql.= "from sg_usuario_empresa ue ";
    $sql.= "inner join cf_empresa on ue.id_empresa = cf_empresa.id ";
    $sql.= "inner join sg_usuario on ue.id_usuario = sg_usuario.id ";
    $sql.= "inner join cf_sucursal on ue.id_sucursal = cf_sucursal.id ";
    $sql.= "inner join sg_rol on ue.id_rol = sg_rol.id ";

    $sql.= "where ue.id_usuario = " . $idUsuario;
    $sql.= " and ue.id_empresa = " . $idEmpresa;
    $sql.= " and ue.id_sucursal = " . $idSucursal;
   
    $sq= $this->db->query($sql);
    return $sq->row();
    }




    public function obt_empresa_x_usuario($id){
        //La varoable $id = tiene el id del rol
        //Aqui se obtiene todos las empresas asociados al usuario

    $sql = "select  ue.*, em.nombre as empresa, sg_rol.nombre as rol, su.nombre as sucursal ";
    $sql.= "from sg_usuario_empresa ue ";
    $sql.= "inner join cf_empresa em on ue.id_empresa=em.id and em.activo=1 ";
    $sql.= "inner join cf_sucursal su on ue.id_sucursal=su.id and su.activo=1 ";
    $sql.= "inner join sg_rol on ue.id_rol = sg_rol.id ";
    $sql.= "where ue.id_usuario = " . $id;
    $sql.= " order by em.nombre ";
      $sq= $this->db->query($sql);
      return $sq->result();
    }


    public function obt_usuario_empresa_modulo($id){
      $this->db->select('*');
      $this->db->where('id_usuario_empresa',$id);
      return $this->db->get('sg_usuario_empresa_modulo')->result();
    }





  public function guardar_add_usuarioEmpresa($data){
    $this->db->insert('sg_usuario_empresa', $data);
    return $this->db->insert_id();
  }


    public function valEmpresa($idEmpresa, $idUsuario){
    $sql ="select * ";
    $sql.="from sg_usuario_empresa ";
    $sql.="where id_usuario = " . $idUsuario . " and id_empresa=" . $idEmpresa;

    $sq= $this->db->query($sql);
    $row = $sq->row();    
        $nro = $sq->num_rows(); 
        if($nro==1){
            return 1;
        }else{
          return 0;
        }  
    }


  public function guardar_add_usuarioEmpresaModulo($data){
    $this->db->insert('sg_usuario_empresa_modulo', $data);
    return $this->db->insert_id();
  }

  public function guardar_add_usuarioEmpresaModuloPermiso($data){
    $this->db->insert('sg_usuario_empresa_modulo_permiso', $data);
  }
  
  
  public function guardar_add_usuarioEmpresaModuloProceso($data){
      $this->db->insert('sg_usuario_empresa_modulo_proceso', $data);
  }  


  public function add_subModulo($data){
    $this->db->insert('sg_usuario_empresa_modulo_proceso', $data);
  }


 public function eliminar_usuario_empresa($id){
      //borrar rol_modulo
      $this->db->where('id',$id);
      $this->db->delete('sg_usuario_empresa'); 

  }


  public function eliminar_usuario_empresa_modulo_permiso($id){
      $this->db->where('id_uem',$id);
      $this->db->delete('sg_usuario_empresa_modulo_permiso'); 
    }

  public function eliminar_usuario_empresa_modulo($id){
      $this->db->where('id_usuario_empresa',$id);
      $this->db->delete('sg_usuario_empresa_modulo'); 
  }


  public function eliminar_usuario_empresa_modulo_ind($id){
      $this->db->where('id',$id);
      $this->db->delete('sg_usuario_empresa_modulo'); 
  }



public function obtModificar_empresaModulo($id_empresaModulo){
    $sql = "select sg_modulo.* ";
    $sql.= "from sg_usuario_empresa_modulo uem ";
    $sql.= "inner join sg_modulo on uem.id_modulo = sg_modulo.id ";
    $sql.= "where uem.id = " . $id_empresaModulo;
    $sq= $this->db->query($sql);
    return $sq->row();    

}



function obt_subModulo_x_modulo($idEmpresaModulo){
    $sql = "select sg_sub_modulo.*, uemp.id as id_empresa_proceso ";
    $sql.= "from sg_usuario_empresa_modulo_proceso uemp ";
    $sql.= "inner join sg_sub_modulo on uemp.id_sub_modulo = sg_sub_modulo.id ";
    $sql.= "where uemp.id_uem = " . $idEmpresaModulo;
    $sq= $this->db->query($sql);
    return $sq->result();    
}



 public function asociarProceso($data){
    $this->db->insert('sg_usuario_empresa_modulo_proceso', $data);

  }


   public function eliminarProceso($id){
      //borrar empresa_modulo
      $this->db->where('id',$id);
      $this->db->delete('sg_usuario_empresa_modulo_proceso'); 

  }


    

}
