<?php
class Sg_usuario_mdl extends CI_Model {

	
	public function __construct(){
        parent::__construct();
       
    }

   
    public function obt_dataTable(){
		  $sql = "select * ";
		  $sql.= "from sg_usuario ";

      if($this->session->userdata('administrador') < 3){ 
        $sql.= "where id_empresa = " . $this->session->userdata('idEmpresa');
      }else{
        $sql.= "where id_sucursal = " . $this->session->userdata('idSucursal');
      }  


		  $sql.= " order by run ";
		  $sq= $this->db->query($sql);
		  return $sq->result();
    }


    
    public function obtModificar($id){
  		$this->db->select('*');
  		$this->db->where('id',$id);
  		return $this->db->get('sg_usuario')->row();
    }


    public function obtInfUsuario($id){
      $this->db->select('*');
      $this->db->where('id',$id);
      return $this->db->get('sg_usuario')->row();
    }





  public function guardar_add($data){
    $this->db->insert('sg_usuario', $data);
    return $this->db->affected_rows();
  }



  public function guardar_mod($id,$data){
    $this->db->where('id',$id);
    $this->db->update('sg_usuario', $data);
    return $this->db->affected_rows();
  }


  public function desactivar($id,$data){
      $this->db->where('id',$id);
      $this->db->update('sg_usuario', $data);
      return $this->db->affected_rows();
  }


   /*
    public function valLogin($login){

		$sql ="select * ";
		$sql.="from sg_usuario ";
		$sql.="where login = '" . $login . "' ";
		$sq= $this->db->query($sql);
		$row = $sq->row();		
        $nro = $sq->num_rows(); 
        if($nro>0){
            return 1;
        }else{
        	return 0;
        }  
    }
   */ 
    

    public function valRun($run){
    $sql ="select * ";
    $sql.="from sg_usuario ";
    $sql.="where run = '" . $run . "' and id_sucursal = " .$this->session->userdata('idSucursal');
    $sq= $this->db->query($sql);
    $row = $sq->row();    
        $nro = $sq->num_rows(); 
        if($nro>0){
            return 1;
        }else{
          return 0;
        }  
    }

    
    
    public function val_claveActual($idUsuario, $claveActual){
		$claveActual = md5($claveActual);
		$sql ="select * ";
		$sql.="from sg_usuario ";
		$sql.="where password = '" . $claveActual . "' and id = " . $idUsuario;
		$sq= $this->db->query($sql);
		$row = $sq->row();		
        $nro = $sq->num_rows(); 
        if($nro > 0){
            return 0;
        }else{
        	return 1;
        }  
    }    
    
    

}

