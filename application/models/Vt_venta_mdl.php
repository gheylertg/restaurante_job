<?php
class Vt_venta_mdl extends CI_Model {

	
	public function __construct(){
        parent::__construct();
       
    }



  public function obt_dataTable(){
  		$sql = "select * ";
		  $sql.= "from vt_venta ";
		  $sq= $this->db->query($sql);
		  return $sq->result();
  }


   
    public function obtModificar($id){
		$this->db->select('*');
		$this->db->where('id',$id);
		return $this->db->get('vt_venta')->row();
    }


  public function guardar_add($data){
    $this->db->insert('vt_venta', $data);
    return $this->db->insert_id();
  }


  public function guardarDetalle($data){
    $this->db->insert('vt_venta_detalle', $data);
    return true;
  }



////////////////////////////////////////////
  public function guardar_mod($id,$data){
    $this->db->where('id',$id);
    $this->db->update('mt_unidad_medida', $data);
    return $this->db->affected_rows();
  }






  public function desactivar($id,$data){
      $this->db->where('id',$id);
      $this->db->update('mt_unidad_medida', $data);
      return $this->db->affected_rows();
  }


   
}
