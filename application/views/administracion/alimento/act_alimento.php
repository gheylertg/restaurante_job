<?php
$incluir = base_url() . "assets/images/nuevo.png";
$asterisco = ' style="color=red; font-size:16px"';
$ci = &get_instance();
$ci->load->library('Clssession');
$accion= $ci->clssession->accion(11,2);



?>

        <br>            
        <div class="main-panel">
            <div class="content">
                <div class="page-inner mt--5">
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <div class="card card-stats card-round" style="padding-top: 5px">
                                <div class="card-body">
									 <h1><span style="color:blue">Alimentos</span></h1>
                                </div>
                            </div>
                        </div>
                    </div>


                <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <?php
                                        if($accion==1){
                                    ?>
                                    <div  style="text-align:right">

                                    <a href="javascript:void(0)" onclick="javascript:incAlimento()">
                                       <img src="<?=$incluir;?>" style="width:35px; height:35px" alt="Incluir">
                                    </a>
                                    </div>
                                    <?php
                                        }
                                    ?>

                                    <div class="table-responsive" style="padding-top: 10px">
										<div id="bodyTabla">
										</div>
                                    </div>
                                </div>
                            </div>
                        </div>    
                </div>
            </div>
<?php
$fechaMaxima = date("Y-m-d");


?>


<!-- Modificar Alimento ----->


<div class="modal fade" id="modificar_alimento" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width:800px; margin-left:-100px">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel" style="color:blue">
                    <div>Modificar Alimento</div>
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                
                <form name="formularioModAlimento" method="post" id="formularioModAlimento" autocomplete="off">   

                <div class="row">
                    <div class="col-md-3">
                        <label>
                        <span style="color:#0000ff; font-size:16px; font-weight:600">
                            Nombre <span style="color:#ff0000;">*</span>
                        </span>
                        </label>
                    </div>
                    <div class="col-md-9">
                        <input type="text" id="mo_nombre" name="mo_nombre"
                               class="form-control"/>
                        <div id="lab_nombre_mo" style="color:red; margin-left:9px; display:none">
                        </div>
                    </div>
                </div>    


                <div class="row" style="padding-top:5px">
                    <div class="col-md-3">
                        <label>
                        <span style="color:#0000ff; font-size:16px; font-weight:600">
                            Descripción <span style="color:#ff0000;">*</span>
                        </span>
                        </label>
                    </div>

                    <div class="col-md-9">
                        <textarea  id="mo_descripcion" name="mo_descripcion"
                               class="form-control" rows="3"></textarea>
                        <div id="lab_descripcion_mo" style="color:red; margin-left:9px; display:none">
                        </div>
                    </div>
                </div>    


                <br>  
                <span style="color:#ff0000; font-size:16px; text-align:left">Campo obligatorio (*)</span>

                <input type="hidden" id="id_mod_mo" name="id_mod_mo">
                <input type="hidden" id="nombreOrig" name="nombreOrig">

                </form>  
              
            </div>
            <div class="modal-footer">

                   
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrar05">Cerrar</button>                                                               


                <button type="button" class="btn btn-primary" id="guardar05"
                        onclick="guardar_mod_alimento();">
                      Guardar
                </button>
            </div>
        </div>
    </div>
</div>
