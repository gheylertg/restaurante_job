<?php
$incluir = base_url() . "assets/images/nuevo.png";
$asterisco = ' style="color=red; font-size:16px"';
$ci = &get_instance();
$ci->load->library('Clssession');
$accion= $ci->clssession->accion(11,2); 

?>
        <br>            
        <div class="main-panel">
            <div class="content">
                <div class="page-inner mt--5">
                    <div class="row row-card-no-pd mt--2">
                        <div class="col-sm-12 col-md-12">
                            <div class="card card-stats card-round">
                                <div class="card-body ">
									 <h2><span style="color:blue">Actualizar Ingredientes asociados </span></h2>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row">
						<div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
									<div class="row">
                                        <div class="col-md-1">
   										<a href="<?=base_url('sabor');?>" class="btn btn-primary btn-sm item_edit2" 
											style="border-radius: 15px; font-size:16px; font-weight:500;
											height:45px;">Regresar
										</a>
										</div>
										<div class="col-md-10"></div>
										<div class="col-md-1">
											<a href="javascript:void(0)" onclick="javascript:add_ingredienteSabor()">
											   <img src="<?=$incluir;?>" style="width:35px; height:30px" alt="Incluir">
											</a>
										</div>
									</div>
									<hr>
                                    <div class="table-responsive">
										<div id="bodyTablaIngrediente">
										</div>
						            </div>
                                </div>
                            </div>
                        </div>    
                    </div>    
                </div>
            </div>
         </div>   

         <input type="hidden" id="id_sabor" name="id_sabor">

    




<!-- Modal Incluir producto-->
<div class="modal fade" id="mod_ingredienteSabor" tabindex="-1" role="dialog" 
                                
      aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width:800px; margin-left:-100px">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel" style="color:blue">
                    <span>Asociar Ingrediente</span>
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                
            <form name="formularioIngrediente" method="post" id="formularioIngrediente" autocomplete="off">   
                
              <div class="container">
                <div class="row" style="padding-top: 10px">
                    <div class="col-md-3">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
                             <span style="color:#0000ff; font-size:16px; font-weight:600">
                                 Ingrediente <span style="color:#ff0000;">*</span></span>
                             </label>
                    </div>
                   
                    <div class="col-md-9">
                        <div id="cbo_ingredienteSabor"></div>

                        <div id="lab_ingrediente" style="color:red; margin-left:9px; display:none"></div>       
                    </div>
                </div>  

                <div class="row" style="padding-top: 10px">
                    <div class="col-md-3">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
                             <span style="color:#0000ff; font-size:16px; font-weight:600">
                                 Cantidad <span style="color:#ff0000;">*</span></span>
                             </label>
                    </div>
                   
                    <div class="col-md-4">
                    <input type="text" name="cantidad" id="cantidad" 
                           class="form-control" value="0,00" style="text-align:right;" > 
                    <script type="text/javascript">
                    $("#cantidad").maskMoney({symbol:'R$ ', thousands:'.', decimal:',', symbolStay: true}); 
                    </script>
                        <div id="lab_cantidad" style="color:red; margin-left:9px; display:none"></div>       
                    </div>
                    
                    <div class="col-md-5">
						          <div id="disponible" style="color:blue; font-size:16px; font-weight:600"></div>
                    </div>  
                    
                    
                </div>  

                <br>  
                <span style="color:#ff0000; font-size:16px; text-align:left">Campo obligatorio (*)</span>
              </div>                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrar02">Cerrar</button>
                <button type="button" class="btn btn-primary" 
                        onclick="guardar_add_ingredienteSabor();" id="guardar02">
                      Guardar
                </button>
            </div>
            <input type="hidden" id="accion" name="accion">
            <input type="hidden" id="id_mod" name="id_mod">
            <input type="hidden" id="idIngredienteOrig" name="idIngredienteOrig">
            <input type="hidden" id="cantidad_original" name="cantidad_original">
            
            </form>  

        </div>
    </div>
</div>



<!-- Modal Incluir producto-->
<div class="modal fade" id="modificar_ingredienteSabor" tabindex="-1" role="dialog" 
      aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width:800px; margin-left:-100px">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel" style="color:blue">
                    <span>Modificar Ingrediente</span>
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                
            <form name="formularioIngredienteSabor" method="post" id="formularioIngredienteSabor" autocomplete="off">   
                
              <div class="container">
                <div class="row" style="padding-top: 10px">
                    <div class="col-md-3">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
                             <span style="color:#0000ff; font-size:16px; font-weight:600">
                                 Cantidad <span style="color:#ff0000;">*</span></span>
                             </label>
                    </div>
                   
                    <div class="col-md-4">
                    <input type="text" name="m_cantidad" id="m_cantidad" 
                           class="form-control" style="text-align:right;" > 
                    <script type="text/javascript">
                    $("#m_cantidad").maskMoney({symbol:'R$ ', thousands:'.', decimal:',', symbolStay: true}); 
                    </script>
                        <div id="lab_cantidad_mo" style="color:red; margin-left:9px; display:none"></div>       
                    </div>

                    
                    
                </div>  

                <br>  
                <span style="color:#ff0000; font-size:16px; text-align:left">Campo obligatorio (*)</span>
              </div>                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrar03">Cerrar</button>
                <button type="button" class="btn btn-primary" 
                        onclick="guardar_mod_ingredienteSabor();" id="guardar03">
                      Guardar
                </button>
            </div>
            <input type="hidden" id="accion" name="accion">
            <input type="hidden" id="id_mod_mo" name="id_mod_mo">
            
            </form>  

        </div>
    </div>
</div>



