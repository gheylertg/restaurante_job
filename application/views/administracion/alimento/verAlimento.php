<?php
$incluir = base_url() . "assets/images/nuevo.png";
$asterisco = ' style="color=red; font-size:16px"';

$ruta = base_url() . "Ad_sabor/index"; 


$fechaMaxima = date("Y-m-d");




?>

<br>            
<div class="main-panel" style="padding-top: 10px">
    <div class="content">
        <div class="page-inner mt--5">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="card card-stats card-round" >
                        <div class="card-body" style="padding-bottom: 5px">
							 <h2><span style="color:blue;">Visualizar Sabor</span></h2>
                        </div>
                    </div>
                </div>
            </div>


            <form name="formulario" method="post" id="formulario" autocomplete="off" 
                  enctype="multipart/form-data">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                <div class="card card-stats card-round">
                <div class="card-body">




                <div class="row">
                    <div class="col-md-3">
                        <label>
                        <span style="color:#0000ff; font-size:16px; font-weight:600">
                            Nombre 
                        </span>
                        </label>
                        
                        <input type="text" id="m_nombre" name="m_fecha" 
                               class="form-control" disabled="true"/>
                    </div>

                    <div class="col-md-9">
                        <label>
                        <span style="color:#0000ff; font-size:16px; font-weight:600">
                            Descripción
                        </span>
                        </label>
                        
                        <textarea id="m_descripcion" name="m_descripcion" 
                               class="form-control" disabled="true"></textarea>
                    </div>
                </div>  

            <hr>

            <div class="row">
                <div class="col-md-12">
                    <label>
                    <span style="color:#0000ff; font-size:20px; font-weight:600">
                        Ingredientes<span style="color:#ff0000;">*</span>
                    </span>
                    </label>
                </div>  

            </div>
                    
            <div class="row">
                <div class="col-md-12">
                    <div id="tablaIngrediente"></div> 

                </div>
            </div>  





            <div class="row">
                <div class="col-md-12" style="text-align: right;">
                    <button type="button" class="btn btn-danger" onclick="cerrar_form()" 
                            id="cerrar01">Cerrar</button>

                </div>    
            </div> 
            
                </div>     
                </div> <!-- Fin card-body-->
                </div> <!-- card card-stats -->
            </div> <!-- col principal -->

            <input type="hidden" id="ruta_guardar_producto" id="ruta_guardar_producto" 
                   value="<?=$ruta;?>">

            </form>


        </div>
    </div>
</div>    
