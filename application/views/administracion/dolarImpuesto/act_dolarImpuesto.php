<?php
$incluir = base_url() . "assets/images/nuevo.png";
$asterisco = ' style="color=red; font-size:16px"';

$ci = &get_instance();
$ci->load->library('Clssession');
$accion= $ci->clssession->accion(9,2); 





?>
        <br>            
        <div class="main-panel">
            <div class="content">
                <div class="page-inner mt--5">
                    <div class="row row-card-no-pd mt--2">
                        <div class="col-sm-12 col-md-12">
                            <div class="card card-stats card-round">
                                <div class="card-body ">
									 <h2><span style="color:blue">Actualizar Dolar / Impuesto</span></h2>
                                </div>
                            </div>
                        </div>
                    </div>

             

                <div class="col-md-12">
                                <div class="card-body">
                                    <div class="table-responsive">
										<div id="bodyTabla">
										</div>
										
										
                                    </div>
                                </div>
                            </div>
                        </div>    
                </div>
            </div>
    




<!-- Modal -->
<div class="modal fade" id="modDolar_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content" style="width:800px; margin-left:-100px">
			<div class="modal-header bg-primary text-white">
				<h4 class="modal-title" id="exampleModalLabel" style="color:blue">
					<div id="titulo_mostrar" style="font-weight:700"></div>
				</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true" style="color:white; font-size:30px">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				
			<form name="formularioDolar" method="post" id="formularioDolar" autocomplete="off">	
				
				
			  <div class="container">
				<div class="row">
					<div class="col-md-3">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							     Dolar <span style="color:#ff0000;">*</span></span>
							 </label>

					</div>
					<div class="col-md-3">					
                        <input type="text" name="m_dolar" id="m_dolar" 
                               class="form-control" 
                               value="0,00" style="text-align:right; padding-top: 5px;margin-right:-20px;margin-top: 12px;" > 
                        <script type="text/javascript">
                            $("#m_dolar").maskMoney({symbol:'R$ ', thousands:'.', decimal:',', symbolStay: true}); 
                        </script>

                        <div id="lab_dolar" style="color:red; margin-left:9px; display:none"></div>
       				</div>	
       			</div>


                <br>  
				<span style="color:#ff0000; font-size:16px; text-align:left">Campo obligatorio (*)</span>
				<input type="text" name="sin_dato" id="sin_dato" style="border: 0">
			  </div>				
			</div>
			<div class="modal-footer">

                   
				<button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrar01"><span style="font-weight:700">Cerrar</span></button>


		        <button type="button" class="btn btn-primary" onclick="guardar_dolar();" 
		                id="guardar01">
		              <span style="font-weight:700">Guardar</span>
		        </button>

			</div>


            <input type="hidden" id="id_mod" name="id_mod">
            <input type="hidden" id="val_error" name="val_error" value="0">
            <input type="hidden" id="accion_formulario" name="accion_formulario" value="0">
			</form>  

		</div>
	</div>
</div>

 
<!-- impuesto -->

<!-- Modal -->
<div class="modal fade" id="modImpuesto_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content" style="width:800px; margin-left:-100px">
			<div class="modal-header bg-primary text-white">
				<h4 class="modal-title" id="exampleModalLabel" style="color:blue">
					<div id="titulo_mostrarImp" style="font-weight:700"></div>
				</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true" style="color:white; font-size:30px">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				
			<form name="formularioImpuesto" method="post" id="formularioImpuesto" autocomplete="off">	
				
				
			  <div class="container">
				<div class="row">
					<div class="col-md-3">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							     Impuesto <span style="color:#ff0000;">*</span></span>
							 </label>

					</div>
					<div class="col-md-3">					
                        <input type="text" name="m_impuesto" id="m_impuesto" 
                               class="form-control" 
                               value="0,00" style="text-align:right; padding-top: 5px;margin-right:-20px;margin-top: 12px;" > 
                        <script type="text/javascript">
                            $("#m_impuesto").maskMoney({symbol:'R$ ', thousands:'.', decimal:',', symbolStay: true}); 
                        </script>

                        <div id="lab_impuesto" style="color:red; margin-left:9px; display:none"></div>
       				</div>	
       			</div>


                <br>  
				<span style="color:#ff0000; font-size:16px; text-align:left">Campo obligatorio (*)</span>
				<input type="text" name="sin_dato" id="sin_dato" style="border: 0">
			  </div>				
			</div>
			<div class="modal-footer">

                   
				<button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrar02"><span style="font-weight:700">Cerrar</span></button>


		        <button type="button" class="btn btn-primary" onclick="guardar_impuesto();" 
		                id="guardar02">
		              <span style="font-weight:700">Guardar</span>
		        </button>

			</div>


            <input type="hidden" id="id_modImp" name="id_modImp">
            <input type="hidden" id="val_error" name="val_error" value="0">
            <input type="hidden" id="accion_formularioImp" name="accion_formularioImp" value="0">
			</form>  

		</div>
	</div>
</div>

 
