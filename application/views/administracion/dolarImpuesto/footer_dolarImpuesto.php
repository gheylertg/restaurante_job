


<!--cargar datatable-->
<script>
    $(document).ready(function(){
		 cargarDataTable(0);
	});





function cargarDataTable(id){
    $.ajax({
		url : "<?php echo site_url('Ad_dolarImpuesto/ajax_datatable')?>/"+id,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			 document.getElementById('bodyTabla').innerHTML = data.registro;

			 $(document).ready(function() {
				$('#basic-datatables').DataTable({
			    });	
			 });

		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}


//cargar combos


function modificar_dolar(id){
    //cbo_modelo(ruta,data.id_proyecto_local,data.id_proyecto_local);   //Llenar combo    
    document.getElementById('accion_formulario').value="2";
    document.getElementById('titulo_mostrar').innerHTML = "Modificar Monto del Dolar";
    document.getElementById('guardar01').disabled=false;
	document.getElementById('cerrar01').disabled=false;     

    $.ajax({
		url : "<?php echo site_url('Ad_dolarImpuesto/ajax_modificarDolar')?>/"+id,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			 document.getElementById('m_dolar').value = data.dolar;
			 document.getElementById('id_mod').value = data.id;
			 document.getElementById('val_error').value = "0";
			 document.getElementById('lab_dolar').style.display = "none";
             $('#modDolar_modal').modal('show');
		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}

function guardar_dolar(){

	//validar campos formulario
   	document.getElementById('guardar01').disabled=true;
	document.getElementById('cerrar01').disabled=true; 		
        $.ajax({
            url : "<?=base_url();?>Ad_dolarImpuesto/ajax_guardar_dolar",
            type: "POST",
            data: $('#formularioDolar').serialize(),
            dataType: "JSON",
            success: function(data)
            {
            	if(data.status==0){ 
					 document.getElementById('bodyTabla').innerHTML = data.registro;
					 $('#modDolar_modal').modal('hide');
					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
				 	});            	
					swal("Registro almacenado Correctamente!", "", {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					});
				}	
	        },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error Insertando / modificanco registro, Comuniquese con el administrador');
            }
        });
  
}


////////////////// modificar impusto
function modificar_impuesto(id){
    document.getElementById('titulo_mostrarImp').innerHTML = "Modificar Monto del Impuesto";
    document.getElementById('guardar02').disabled=false;
	document.getElementById('cerrar02').disabled=false;     

    $.ajax({
		url : "<?php echo site_url('Ad_dolarImpuesto/ajax_modificarImpuesto')?>/"+id,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			 document.getElementById('m_impuesto').value = data.impuesto;
			 document.getElementById('id_modImp').value = data.id;
			 //document.getElementById('val_error').value = "0";
             $('#modImpuesto_modal').modal('show');
		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}

function guardar_impuesto(){

	//validar campos formulario
   	document.getElementById('guardar02').disabled=true;
	document.getElementById('cerrar02').disabled=true; 		
        $.ajax({
            url : "<?=base_url();?>Ad_dolarImpuesto/ajax_guardar_impuesto",
            type: "POST",
            data: $('#formularioImpuesto').serialize(),
            dataType: "JSON",
            success: function(data)
            {
            	if(data.status==0){ 
					 document.getElementById('bodyTabla').innerHTML = data.registro;
					 $('#modImpuesto_modal').modal('hide');
					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
				 	});            	
					swal("Registro almacenado Correctamente!", "", {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					});
				}	
	        },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error Insertando / modificanco registro, Comuniquese con el administrador');
            }
        });
  
}







</script>
