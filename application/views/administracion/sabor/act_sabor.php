<?php
$incluir = base_url() . "assets/images/nuevo.png";
$asterisco = ' style="color=red; font-size:16px"';
$ci = &get_instance();
$ci->load->library('Clssession');
$accion= $ci->clssession->accion(11,2);


?>

        <br>            
        <div class="main-panel">
            <div class="content">
                <div class="page-inner mt--5">
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <div class="card card-stats card-round" style="padding-top: 5px">
                                <div class="card-body">
									 <h1><span style="color:blue">Sabores</span></h1>
                                </div>
                            </div>
                        </div>
                    </div>


                <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <?php
                                        if($accion==1){
                                    ?>
                                    <div  style="text-align:right">

                                    <a href="javascript:void(0)" onclick="javascript:incSabor()">
                                       <img src="<?=$incluir;?>" style="width:35px; height:35px" alt="Incluir">
                                    </a>
                                    </div>
                                    <?php
                                        }
                                    ?>

                                    <div class="table-responsive" style="padding-top: 10px">
										<div id="bodyTabla">
										</div>
                                    </div>
                                </div>
                            </div>
                        </div>    
                </div>
            </div>
<?php
$fechaMaxima = date("Y-m-d");


?>

<!-- Modal Incluir producto-->
<div class="modal fade" id="act_mod_factura" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width:1000px; margin-left:-200px">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel" style="color:blue">
                    <div>Actualizar Factura de Compra</div>
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                
                <form name="formulario" method="post" id="formulario" autocomplete="off" 
                      enctype="multipart/form-data">
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                    <div class="card card-stats card-round">
                    <div class="card-body">

                        <div class="row">
                            <div class="col-md-3">
                                <label>
                                <span style="color:#0000ff; font-size:16px; font-weight:600">
                                    Fecha Factura <span style="color:#ff0000;">*</span>
                                </span>
                                </label>
                                
                                <input type="date" id="m_fecha" name="m_fecha" 
                                       max="<?=$fechaMaxima;?>" class="form-control"/>
                                <div id="lab_fecha" style="color:red; margin-left:9px; display:none">
                                </div>
                            </div>


                            <div class="col-md-3">
                                <label>
                                <span style="color:#0000ff; font-size:16px; font-weight:600">
                                    Nro Factura
                                </span>
                                </label>
                                <input type="text" name="m_nroFactura" id="m_nroFactura" 
                                       class="form-control"
                                       maxlength="100" minlength="0">
                                <div id="lab_nroFactura" style="color:red; margin-left:9px; display:none">       
                                </div>
                            </div>    

                            <div class="col-md-3">
                                <label>
                                <span style="color:#0000ff; font-size:16px; font-weight:600">
                                    Monto Factura
                                </span>
                                </label>
                                <input type="text" name="m_monto" id="m_monto" 
                                   class="form-control" value="0,00" style="padding-top: 5px; text-align:right;" > 
                                <script type="text/javascript">
                                $("#m_monto").maskMoney({symbol:'R$ ', thousands:'.', decimal:',', symbolStay: true}); 
                                </script>
                                <div id="lab_monto" style="color:red; margin-left:9px; display:none">
                                </div>
                            </div>    

                        </div>        
                        
                        <div class="row">

                            <div class="col-md-9">
                                <label>
                                <span style="color:#0000ff; font-size:16px; font-weight:600">
                                    Proveedor
                                </span>
                                </label>
                                <div id="cbo_proveedorCp"></div>
                                <div id="lab_proveedorCp" style="color:red; margin-left:9px; display:none">
                                </div>
                                
                            </div>
                        </div>    
                        <br>  
                        <span style="color:#ff0000; font-size:16px; text-align:left">Campo obligatorio (*)</span>
                    </div>                
                    </div>
                    </div>
                </div>  

                     <input type="hidden" id="id_mod" name="id_mod">  
                     <input type="hidden" id="fechaProducto" name="fechaProducto">  
    
                </form> 
             </div>    
    

            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrar02">Cerrar</button>


                <button type="button" class="btn btn-primary" 
                        onclick="guardar_mod_factura();" id="guardar02">
                          Guardar
                </button>

            </div>

                  
            
        </div>
    </div>
</div>


<!-- Modificar sabior ----->


<div class="modal fade" id="modificar_sabor" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width:800px; margin-left:-100px">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel" style="color:blue">
                    <div>Modificar Sabor</div>
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                
                <form name="formularioModSabor" method="post" id="formularioModSabor" autocomplete="off">   

                <div class="row">
                    <div class="col-md-3">
                        <label>
                        <span style="color:#0000ff; font-size:16px; font-weight:600">
                            Nombre <span style="color:#ff0000;">*</span>
                        </span>
                        </label>
                    </div>
                    <div class="col-md-9">
                        <input type="text" id="mo_nombre" name="mo_nombre"
                               class="form-control"/>
                        <div id="lab_nombre_mo" style="color:red; margin-left:9px; display:none">
                        </div>
                    </div>
                </div>    


                <div class="row" style="padding-top:5px">
                    <div class="col-md-3">
                        <label>
                        <span style="color:#0000ff; font-size:16px; font-weight:600">
                            Descripción <span style="color:#ff0000;">*</span>
                        </span>
                        </label>
                    </div>

                    <div class="col-md-9">
                        <textarea  id="mo_descripcion" name="mo_descripcion"
                               class="form-control" rows="3"></textarea>
                        <div id="lab_descripcion_mo" style="color:red; margin-left:9px; display:none">
                        </div>
                    </div>
                </div>    


                <br>  
                <span style="color:#ff0000; font-size:16px; text-align:left">Campo obligatorio (*)</span>

                <input type="hidden" id="id_mod_mo" name="id_mod_mo">
                <input type="hidden" id="nombreOrig" name="nombreOrig">

                </form>  
              
            </div>
            <div class="modal-footer">

                   
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrar05">Cerrar</button>                                                               


                <button type="button" class="btn btn-primary" id="guardar05"
                        onclick="guardar_mod_sabor();">
                      Guardar
                </button>
            </div>
        </div>
    </div>
</div>
