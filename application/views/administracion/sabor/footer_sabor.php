



<!--cargar datatable-->
<script>
    $(document).ready(function(){
         <?php
         if($modulo==1){  
         ?>	
			 cargarDataTable(0);
		 <?php
		 }
		 ?>

		 <?php
         if($modulo==2){  
         ?>	
			 addSabor();
		 <?php
		 }
		 ?>

		 <?php
         if($modulo==3){  
         ?>	
			 cargar_dataTableIngrediente(<?=$idSabor;?>);
		 <?php
		 }
		 ?>

		 <?php
         if($modulo==4){  
         ?>	
			 verSabor(<?=$idSabor;?>);
		 <?php
		 }
		 ?>



    });
    
// construye el datatable principal para actualizar la ordenes
function cargarDataTable(id){
    $.ajax({
		url : "<?php echo site_url('Ad_sabor/ajax_datatable')?>/"+id,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			 document.getElementById('bodyTabla').innerHTML = data.registro;
			 $(document).ready(function() {
				$('#basic-datatables').DataTable({
                    "order": [[ 0, "desc" ]]
			    });	
			 });

		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}


// llama el controllador que ingresa la información de la orden
function incSabor(){
	document.location.href = "<?php echo base_url('incSabor')?>";	
}





function addSabor(){
   	document.getElementById('guardar01').disabled=false;
	document.getElementById('cerrar01').disabled=false; 

    $.ajax({
		url : "<?php echo site_url('Ad_sabor/ajax_datatable_sabor')?>/",
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			 document.getElementById('tablaIngrediente').innerHTML = data.registro;
			 $(document).ready(function() {
			 });


		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}


function add_ingrediente(){
	ruta = "<?php echo site_url('Combo_ctrl/ajax_combo_ingrediente')?>";
	cbo_ingrediente(ruta,0); 
    document.getElementById('cantidad').value = "0,00";

    document.getElementById('lab_ingrediente').style.display = "none";
    document.getElementById('lab_cantidad').style.display = "none";
    $('#mod_ingrediente').modal('show');
}


function add_ingredienteSabor(){
	ruta = "<?php echo site_url('Combo_ctrl/ajax_combo_ingredienteSabor')?>";
	cbo_ingredienteSabor(ruta,0); 
    document.getElementById('cantidad').value = "0,00";
    document.getElementById('lab_ingrediente').style.display = "none";
    document.getElementById('lab_cantidad').style.display = "none";

    $('#mod_ingredienteSabor').modal('show');
}


function modificar_ingrediente(id){
    document.getElementById('guardar03').disabled=false;
	document.getElementById('cerrar03').disabled=false;     
     
    $.ajax({
		url : "<?php echo site_url('Ad_sabor/ajax_modificar_ingrediente')?>/"+id,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			 document.getElementById('x_cantidad').value = data.cantidad;
			 document.getElementById('id_mod_x').value = data.id;
			 document.getElementById('lab_cantidadx').style.display = "none";

			 //ruta = "<?php //echo site_url('Combo_ctrl/ajax_combo_modulo')?>";
	         //cbo_modulo(ruta,data.id_modulo);   //Llenar combo    
             $('#modificar_ingrediente').modal('show');

		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});

}


function modificar_sabor(id){
    document.getElementById('cerrar05').disabled=false;  
	document.getElementById('guardar05').disabled=false;  
    $.ajax({
		url : "<?php echo site_url('Ad_sabor/ajax_act_modificar_sabor')?>/"+id,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
             document.getElementById('mo_nombre').value = data.nombre;	 
             document.getElementById('mo_descripcion').value = data.descripcion;	 
			 document.getElementById('id_mod_mo').value = data.id;
			 document.getElementById('lab_nombre_mo').style.display = "none";
			 document.getElementById('lab_descripcion_mo').style.display = "none";			 
			 document.getElementById('nombreOrig').value = data.nombre;
             $('#modificar_sabor').modal('show');  
            

		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});

}



function guardar_add_ingrediente(){
$validar = valIngrediente(1);
if($validar==0){
	$.ajax({
		url : "<?=base_url();?>Ad_sabor/ajax_guardar_ingredienteSB",
		type: "POST",
		data: $('#formularioIngrediente').serialize(),
		dataType: "JSON",
		success: function(data)
		{
			if(data.status==0){ 
				document.getElementById('tablaIngrediente').innerHTML = data.registro;
				document.getElementById('cantidad').value = "0,00";
				document.getElementById('nroIngrediente').value = data.nroIngrediente;

				$('#mod_ingrediente').modal('hide');
			}else{
				swal("No se actualizo el registro", "Ya el Ingrediente esta agregado", {
					icon : "error",
					buttons: {        			
						confirm: {
							className : 'btn btn-danger'
						}
					},
				});

			}	 
		},
		error: function (jqXHR, textStatus, errorThrown)
		{
			alert('Error Insertando / modificanco registro, Comuniquese con el administrador');
		}
	});
}else{
	//document.getElementById('cantidad').value = "0,00";
	swal("No se actualizo el registro", "Falta Información", {
		icon : "error",
		buttons: {        			
			confirm: {
				className : 'btn btn-danger'
			}
		},
	});
}    

}



function guardar_mod_ingrediente(){

	//validar campos formulario
	formulario = "formularioModificar";
	error=0;
	error = valIngredienteMod(2);
	if(error==0){
    	document.getElementById('guardar03').disabled=true;
		document.getElementById('cerrar03').disabled=true; 		
	}
	

	if(error == 0){
        $.ajax({
            url : "<?=base_url();?>Ad_sabor/ajax_guardar_ingrediente_mod",
            type: "POST",
            data: $('#formularioModificar').serialize(),
            dataType: "JSON",
            success: function(data)
            {
				document.getElementById('tablaIngrediente').innerHTML = data.registro;
				document.getElementById('x_cantidad').value = "0,00";
				document.getElementById('nroIngrediente').value = data.nroIngrediente;
				$('#modificar_ingrediente').modal('hide');


	        },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error Insertando / modificanco registro, Comuniquese con el administrador');
            }
        });
    }else{
		swal("No se actualizo el registro", "", {
			icon : "error",
			buttons: {        			
				confirm: {
					className : 'btn btn-danger'
				}
			},
		});
    }    
}



/*function eliminar_ingrediente(id){
		    $.ajax({
				url : "<?php //echo site_url('Ad_sabor/ajax_eliminar_ingrediente')?>/"+id,
				type: "GET",                     
				dataType: "JSON",
				success: function(data){
                    document.getElementById('tablaIngrediente').innerHTML = data.registro;
                    document.getElementById('cantidad').value = "0,00";
                    document.getElementById('nroIngrediente').value = data.nroIngrediente;

				},
				error: function (jqXHR, textStatus, errorThrown){
					alert('Error consultando la Base de Datos');
				}
			});

}*/


function eliminar_ingrediente(id){
	swal({
		title: 'Esta seguro de eliminar el Ingrediente?',
			type: 'warning',
			buttons:{
				cancel: {
					visible: true,
						text : 'No, Cancelar!',
								className: 'btn btn-danger'
				},        			
				confirm: {
					text : 'Si, Eliminar!',
					className : 'btn btn-success'
				}
			}
	}).then((willDelete) => {
		if (willDelete) {

		    $.ajax({
				url : "<?php echo site_url('Ad_sabor/ajax_eliminar_ingrediente')?>/"+id,
				type: "GET",                     
				dataType: "JSON",
				success: function(data){
                    document.getElementById('tablaIngrediente').innerHTML = data.registro;
                    document.getElementById('cantidad').value = "0,00";
                    document.getElementById('nroIngrediente').value = data.nroIngrediente;

				},
				error: function (jqXHR, textStatus, errorThrown){
					alert('Error consultando la Base de Datos');
				}
			});
			swal("El Ingrediente fué eliminado!", {
				icon: "success",
				buttons : {
					confirm : {
						className: 'btn btn-success'
					}
				}
			});
		}
	});
}




function guardar_form(){
    formulario = "formulario";
    $validar = validar_form(formulario,0);
    document.getElementById('guardar01').disabled=true;
	document.getElementById('cerrar01').disabled=true;

     if($validar==0){
        $.ajax({
            url : "<?=base_url();?>Ad_sabor/ajax_guardar_saborSB",
            type: "POST",
            data: $('#formulario').serialize(),
            dataType: "JSON",
            success: function(data)
            {
                if(data.status==0){ 

					swal("Sabor almacenado Correctamente!", "", {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					});


                    
					document.location.href = "<?php echo base_url('sabor')?>";	

                }    
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error Insertando / modificanco registro, Comuniquese con el dministrador');
            }
        });         

	}else{
	    document.getElementById('guardar01').disabled=false;
		document.getElementById('cerrar01').disabled=false;



		swal("No se actualizo el registro", "Falta Información", {
				icon : "error",
				buttons: {        			
					confirm: {
						className : 'btn btn-danger'
					}
				},
		});
	}    

}



function prueba_form(){
        $.ajax({
            url : "<?=base_url();?>Ad_sabor/ajax_prueba_form",
            type: "POST",
            data: $('#formulario').serialize(),
            dataType: "JSON",
            success: function(data)
            {
                if(data.status==0){ 
                	alert("aqui");

                }    
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error Insertando / modificanco registro, Comuniquese con el dministrador');
            }
        });         

}




function valIngrediente(accion){

formulario = "formularioIngrediente";
error=0;

if(accion != 3){
	objeto = "id_cbo_ingrediente";
	campo = document.forms[formulario].elements[objeto].value;
	document.getElementById('lab_ingrediente').style.display = "none";
	if (campo==0){
		document.getElementById('lab_ingrediente').style.display = "inline";
	   	document.getElementById('lab_ingrediente').innerHTML = "Seleccione el Ingrediente"; 	
	   	error = 1;
	}
}

objeto = "cantidad";
campo = document.forms[formulario].elements[objeto].value;
campo = campo.replace(/(^\s*)|(\s*$)/g,"");
document.getElementById('lab_cantidad').style.display = "none";

if (campo=="0,00" || campo.length==0){	
	document.getElementById('lab_cantidad').style.display = "inline";
    document.getElementById('lab_cantidad').innerHTML = "Ingrese la cantidad del Ingrediente";
    document.getElementById('cantidad').value = "0,00";
    error = 1;
}


return error;
}




function valIngredienteMod(accion){

formulario = "formularioModificar";
error=0;

objeto = "x_cantidad";
campo = document.forms[formulario].elements[objeto].value;
campo = campo.replace(/(^\s*)|(\s*$)/g,"");
document.getElementById('lab_cantidadx').style.display = "none";

if (campo=="0,00" || campo.length==0){	
	document.getElementById('lab_cantidadx').style.display = "inline";
    document.getElementById('lab_cantidadx').innerHTML = "Ingrese la cantidad del Ingrediente"; 	
    document.getElementById('x_cantidad').value = "0,00";
    error = 1;
}


return error;
}


function cerrar_form(){
    document.location.href = "<?php echo base_url('sabor')?>"; 

	/*ruta = 	document.getElementById('ruta_guardar_producto').value;
	var Form = $('#formulario');
	Form.attr("action", ruta);
	Form.submit();  
	*/
	
}



function guardar_mod_sabor(){
    formulario = "formularioModSabor";
    $validar = validar_form_sabor(formulario,1);
    document.getElementById('guardar05').disabled=true;
	document.getElementById('cerrar05').disabled=true;

	if($validar == 0){
        $.ajax({
            url : "<?=base_url();?>Ad_sabor/ajax_guardar_mod_sabor",
            type: "POST",
            data: $('#formularioModSabor').serialize(),
            dataType: "JSON",
            success: function(data)
            {
            	if(data.status==0){ 

					 document.getElementById('bodyTabla').innerHTML = data.registro;
					 $('#modificar_sabor').modal('hide');

					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
				 	});            	


					swal("Registro almacenado Correctamente!", "", {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					});
				}else{
					document.getElementById('lab_nombre_mo').style.display = "inline";
					document.getElementById('lab_nombre_mo').innerHTML = "El Nombre ingresado, ya se encuentra registrado.";
			    	document.getElementById('guardar05').disabled=false;
					document.getElementById('cerrar05').disabled=false; 		
					swal("No se actualizo el registro", "", {

						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});








				}





	        },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error Insertando / modificanco registro, Comuniquese con el administrador');
            }
        });
    }else{
		swal("No se actualizo el registro", "", {
			icon : "error",
			buttons: {        			
				confirm: {
					className : 'btn btn-danger'
				}
			},
		});
		document.getElementById('guardar05').disabled=false;
		document.getElementById('cerrar05').disabled=false  ;		
    }    
}


function validar_form_sabor(formulario,accion){
//accion 1= incluir
//accion 2= modificar
error=0;

objeto = "mo_nombre";
campo = document.forms[formulario].elements[objeto].value;
campo = campo.replace(/(^\s*)|(\s*$)/g,"");
document.getElementById('lab_nombre_mo').style.display = "none";

if (campo.length==0){	
	document.getElementById('lab_nombre_mo').style.display = "inline";
    document.getElementById('lab_nombre_mo').innerHTML = "Ingrese Nombre del Sabor"; 	
    error = 1;
}

objeto = "mo_descripcion";
campo = document.forms[formulario].elements[objeto].value;
campo = campo.replace(/(^\s*)|(\s*$)/g,"");
document.getElementById('lab_descripcion_mo').style.display = "none";

if (campo.length==0){	
	document.getElementById('lab_descripcion_mo').style.display = "inline";
    document.getElementById('lab_descripcion_mo').innerHTML = "Ingrese Descripción del Sabor"; 	
    error = 1;
}

return error;
}



function validar_form(formulario,accion){
//accion 1= incluir
//accion 2= modificar
error=0;

objeto = "m_nombre";
campo = document.forms[formulario].elements[objeto].value;
campo = campo.replace(/(^\s*)|(\s*$)/g,"");
document.getElementById('lab_nombre').style.display = "none";

if (campo.length==0){	
	document.getElementById('lab_nombre').style.display = "inline";
    document.getElementById('lab_nombre').innerHTML = "Ingrese Nombre del Sabor"; 	
    error = 1;
}

objeto = "m_descripcion";
campo = document.forms[formulario].elements[objeto].value;
campo = campo.replace(/(^\s*)|(\s*$)/g,"");
document.getElementById('lab_descripcion').style.display = "none";

if (campo.length==0){	
	document.getElementById('lab_descripcion').style.display = "inline";
    document.getElementById('lab_descripcion').innerHTML = "Ingrese Descripción del Sabor"; 	
    error = 1;
}


if(accion==0){
	objeto = "nroIngrediente";
	campo = document.forms[formulario].elements[objeto].value;
	document.getElementById('lab_detalleSabor').style.display = "none";
	if (campo==0){
		document.getElementById('lab_detalleSabor').style.display = "inline";
	   	document.getElementById('lab_detalleSabor').innerHTML = "Ingrese los ingredientes del sabor"; 	
	   	error = 1;
	}
}	

return error;
}



function verSabor(){


    $.ajax({
		url : "<?php echo site_url('Ad_sabor/ajax_datatable_verSabor')?>/",
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			 document.getElementById('tablaIngrediente').innerHTML = data.registro;
			 document.getElementById('m_nombre').value = data.nombre;
			 document.getElementById('m_descripcion').value = data.descripcion;
			 $(document).ready(function() {
			 });


		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}




////////////////////////////////////////////////////////////////////////////////

function eliminar_factura(id){
	swal({
		title: 'Esta seguro de eliminar la Factura de Compra?',
			type: 'warning',
			buttons:{
				cancel: {
					visible: true,
						text : 'No, Cancelar!',
								className: 'btn btn-danger'
				},        			
				confirm: {
					text : 'Si, Eliminar!',
					className : 'btn btn-success'
				}
			}
	}).then((willDelete) => {
		if (willDelete) {
		    $.ajax({
				url : "<?php echo site_url('Cp_factura/ajax_eliminar_factura')?>/"+id,
				type: "GET",                     
				dataType: "JSON",
				success: function(data){
					 document.getElementById('bodyTabla').innerHTML = data.registro;
					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
					 }); 
				},
				error: function (jqXHR, textStatus, errorThrown){
					alert('Error consultando la Base de Datos');
				}
			});
			swal("La Factura de Compra fué eliminada!", {
				icon: "success",
				buttons : {
					confirm : {
						className: 'btn btn-success'
					}
				}
			});
		}
	});

}






//GUARDAR ENTRADA



function guardar_mod_factura(){
    formulario = "formulario";
    $validar = validar_form(formulario,1);
    document.getElementById('guardar02').disabled=true;
	document.getElementById('cerrar02').disabled=true;

	if($validar == 0){
        $.ajax({
            url : "<?=base_url();?>Cp_factura/ajax_guardar_mod_factura",
            type: "POST",
            data: $('#formulario').serialize(),
            dataType: "JSON",
            success: function(data)
            {
            	if(data.status==0){ 

					 document.getElementById('bodyTabla').innerHTML = data.registro;
					 $('#act_mod_factura').modal('hide');

					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
				 	});            	


					swal("Registro almacenado Correctamente!", "", {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					});
				}	
	        },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error Insertando / modificanco registro, Comuniquese con el administrador');
            }
        });
    }else{
		swal("No se actualizo el registro", "", {
			icon : "error",
			buttons: {        			
				confirm: {
					className : 'btn btn-danger'
				}
			},
		});
		document.getElementById('guardar02').disabled=false;
		document.getElementById('cerrar02').disabled=false  ;		
    }    
}

function ver_form(id){
	document.location.href = "<?php echo base_url('verSabor')?>/"+id;
}







function mod_ingrediente(id){
	document.location.href = "<?php echo base_url('act_mIngrediente')?>/"+id;
/*	
	ruta = "<?php //echo site_url('cp_orden/act_modificar_producto')?>/"+id
	var Form = $('#formulario');
	Form.attr("action", ruta);
	Form.submit(); 
*/	
}



function cargar_dataTableIngrediente(id){
    $.ajax({
		url : "<?php echo site_url('Ad_sabor/ajax_datatableSabor')?>/"+id,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			 document.getElementById('bodyTablaIngrediente').innerHTML = data.registro;
			 document.getElementById('id_sabor').value=id;
			 $(document).ready(function() {
				$('#basic-datatables').DataTable({
			    });	
			 });

		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}






function guardar_add_ingredienteSabor(){
	
    validar = valIngrediente(1);
    if(validar==0){
    	document.getElementById('cerrar02').disabled=true;
    	document.getElementById('guardar02').disabled=true;
    	id = document.getElementById('id_sabor').value;
        $.ajax({
            url : "<?php echo site_url('Ad_sabor/ajax_guardar_add_ingredienteSabor')?>/"+id,
            type: "POST",
            data: $('#formularioIngrediente').serialize(),
            dataType: "JSON",
            success: function(data)
            {
		    	document.getElementById('cerrar02').disabled=false;
				document.getElementById('guardar02').disabled=false;
            	if(data.status==0){ 
                    document.getElementById('bodyTablaIngrediente').innerHTML = data.registro;
                    
					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
				 	});                      
                    
                    
					swal("Registro almacenado Correctamente!", "", {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					});
                    document.getElementById('cantidad').value = "0,00";
					$('#mod_ingredienteSabor').modal('hide');

				}else{
					document.getElementById('lab_ingrediente').style.display = "inline";
					document.getElementById('lab_ingrediente').innerHTML = "El Ingrediente ingresado, ya se encuentra asociado.";

					swal("No se actualizo el registro", "Ya el Ingrediente esta asociado", {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});

				}	 
	        },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error Insertando / modificanco registro, Comuniquese con el administrador');
            }
        });
    }else{
        //document.getElementById('cantidad').value = "0,00";
		swal("No se actualizo el registro", "Falta Información", {
			icon : "error",
			buttons: {        			
				confirm: {
					className : 'btn btn-danger'
				}
			},
		});
    }    

}


				
				
				
				

function elim_ingrediente(id){
	idSabor = document.getElementById('id_sabor').value;
	swal({
		title: 'Esta seguro de quitar el Ingrediente?',
			type: 'warning',
			buttons:{
				cancel: {
					visible: true,
						text : 'No, Cancelar!',
								className: 'btn btn-danger'
				},        			
				confirm: {
					text : 'Si, Eliminar!',
					className : 'btn btn-success'
				}
			}
	}).then((willDelete) => {
		if (willDelete) {
			$.ajax({
				url : "<?php echo site_url('Ad_sabor/ajax_borrar_ingrediente')?>",
				type: "GET",                     
				data:{'id':id, 'idSabor':idSabor},
				dataType: "JSON",
				success: function(data){
					if(data.status==0){
						document.getElementById('bodyTablaIngrediente').innerHTML = data.registro;
						$(document).ready(function() {
						   $('#basic-datatables').DataTable({
						   });	
					   });
						swal("El Ingrediente fué eliminado!", {
							icon: "success",
							buttons : {
								confirm : {
									className: 'btn btn-success'
								}
							}
						});
  				   }	
				},
				error: function (jqXHR, textStatus, errorThrown){
					alert('Error consultando la Base de Datos');
				}
			});
		}
	});

	
}






function mod_ingredienteSabor(id){
    document.getElementById('guardar03').disabled=false;
	document.getElementById('cerrar03').disabled=false;     
     
    $.ajax({
		url : "<?php echo site_url('Ad_sabor/ajax_modificar_ingredienteSabor')?>/"+id,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			 document.getElementById('m_cantidad').value = data.cantidad;
			 document.getElementById('id_mod_mo').value = data.id;
			 document.getElementById('lab_cantidad_mo').style.display = "none";
			 //ruta = "<?php //echo site_url('Combo_ctrl/ajax_combo_modulo')?>";
	         //cbo_modulo(ruta,data.id_modulo);   //Llenar combo    
             $('#modificar_ingredienteSabor').modal('show');

		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});

}


function guardar_mod_ingredienteSabor(){
	//validar campos formulario
	formulario = "formularioIngredienteSabor";
	idSabor = document.getElementById('id_sabor').value;
	error=0;
	error = valIngredienteSaborMod(2);
	if(error==0){
    	document.getElementById('guardar03').disabled=true;
		document.getElementById('cerrar03').disabled=true; 		
	}
	

	if(error == 0){
        $.ajax({
            url : "<?=base_url();?>Ad_sabor/ajax_guardar_ingredienteSabor_mod/"+idSabor,
            type: "POST",
            data: $('#formularioIngredienteSabor').serialize(),
            dataType: "JSON",
            success: function(data)
            {
				document.getElementById('bodyTablaIngrediente').innerHTML = data.registro;
				document.getElementById('m_cantidad').value = "0,00";
				$('#modificar_ingredienteSabor').modal('hide');


	        },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error Insertando / modificanco registro, Comuniquese con el administrador');
            }
        });
    }else{
		swal("No se actualizo el registro", "", {
			icon : "error",
			buttons: {        			
				confirm: {
					className : 'btn btn-danger'
				}
			},
		});
    }    
}



function valIngredienteSaborMod(accion){

formulario = "formularioIngredienteSabor";
error=0;

objeto = "m_cantidad";
campo = document.forms[formulario].elements[objeto].value;
campo = campo.replace(/(^\s*)|(\s*$)/g,"");
document.getElementById('lab_cantidad_mo').style.display = "none";

if (campo=="0,00" || campo.length==0){	
	document.getElementById('lab_cantidad_mo').style.display = "inline";
    document.getElementById('lab_cantidad_mo').innerHTML = "Ingrese la cantidad del Ingrediente"; 	
    document.getElementById('m_cantidad').value = "0,00";
    error = 1;
}


return error;
}




</script>
