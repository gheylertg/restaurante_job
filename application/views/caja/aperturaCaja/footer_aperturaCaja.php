


<!--cargar datatable-->
<script>


    $(document).ready(function(){
        
        apertura = "<?=$apertura;?>";
        //aperturas=0;
        if (apertura == 0){
    	    aperturaCaja();
    	}

        if (apertura == 1){
    	    error_aperturaCaja();
    	}
	});


	function aperturaCaja()
	{
    $.ajax({
		url : "<?php echo site_url('Cj_aperturaCaja/ajax_obtDatosUsuario')?>",
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
            document.getElementById('div-imagen').innerHTML = data.fotoUsuario;  
			document.getElementById('c_nombre').value = data.nombreUsuario;
			document.getElementById('c_apellido').value = data.apellidoUsuario;
            $('#apertura_modal').modal('show');
		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
	}



   function guardar_form(){

	    document.getElementById('guardar01').disabled=true;
		document.getElementById('cerrar01').disabled=true;
        $.ajax({
            url : "<?=base_url();?>Cj_aperturaCaja/ajax_guardar_aperturaCaja",
            type: "POST",
            data: $('#formulario').serialize(),
            dataType: "JSON",
            success: function(data)
            {
                if(data.status==0){ 
					swal("Caja Aperturada Correctamente!", "", {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					});
					$('#apertura_modal').modal('hide');
                }    
                
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error Insertando / modificanco registro, Comuniquese con el administrador');
            }
        });         
   }


	function error_aperturaCaja()
	{
    $.ajax({
		url : "<?php echo site_url('Cj_aperturaCaja/ajax_errorApertura')?>",
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
            document.getElementById('div-imagenE').innerHTML = data.fotoUsuario;  
			document.getElementById('c_nombreE').value = data.nombreUsuario;
			document.getElementById('c_apellidoE').value = data.apellidoUsuario;
			document.getElementById('mensajeError').innerHTML = data.mensajeError;  
            $('#errorApertura_modal').modal('show');
		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
	}




</script>
