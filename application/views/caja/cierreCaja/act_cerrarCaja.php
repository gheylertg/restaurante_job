<?php
$ci = &get_instance();
$ci->load->library('Clssession');


?>



<!-- Modal -->
<div class="modal fade" id="apertura_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content" style="width:800px; margin-left:-100px">

            <div class="modal-header bg-primary text-white">
                <h4 class="modal-title" id="exampleModalLabel">
                    <div style="font-weight:700">Aperturar Caja.</div>
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color:white; font-size:30px">&times;</span>
                </button>
            </div>

			<div class="modal-body">

			    <div class="container">
					<div class="row">
						<div class="col-md-9">
	                        <label ><span style="color:#0000ff; font-size:16px; font-weight:600">
								     Nombre(s)</span>
							</label>
	                        <input type="text" name="c_nombre" id="c_nombre" class="form-control"
	                               readonly="true">

	                        <br>
	                         <label><span style="color:#0000ff; font-size:16px; font-weight:600">
								 Apellido(s)</span>
						     </label>
	                        <input type="text" name="c_apellido" id="c_apellido" class="form-control" 
	                               readonly="true">
	                        <br>
						</div>

						<div class="col-md-3">
							<div id="div-imagen"></div>	
						</div>
					</div>				
                </div> 
                <hr>
				
				<form name="formulario" method="post" id="formulario" autocomplete="off">	
                <div class="row" style="padding-top: 10px">
                    <div class="col-md-3">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
                             <span style="color:#0000ff; font-size:16px; font-weight:600">
                                 Monto apertura</span>
                             </label>
                    </div>
                   
                    <div class="col-md-4">
	                    <input type="text" name="m_monto" id="m_monto" 
	                           class="form-control" value="0,00" style="text-align:right;" > 
	                    <script type="text/javascript">
	                    $("#m_monto").maskMoney({symbol:'R$ ', thousands:'.', decimal:',', symbolStay: true}); 
	                    </script>
	                               
                    </div>
                </div> 


                <div class="row" style="padding-top: 10px">
                    <div class="col-md-3">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
                             <span style="color:#0000ff; font-size:16px; font-weight:600">
                                 Observación / Nota <span style="color:#ff0000;">*</span></span>
                             </label>
                    </div>
                    <div class="col-md-9">
                        <textarea style="padding-top: 5px;margin-right:-20px;margin-top: 0px;" 
                               name="m_observacion" id="m_observacion" 
                               class="form-control" rows="3"></textarea>
					</div>
				</div>	                

				</form>	
			</div>		



			<div class="modal-footer">

                   
				<button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrar01">Cerrar
				</button>


		        <button type="button" class="btn btn-primary" onclick="guardar_form();" 
		                id="guardar01">
		              <span style="font-weight:700">Guardar</span>
		        </button>

			</div>

 

		</div>
	</div>
</div>





<!-- Modal -->
<div class="modal fade" id="errorApertura_modal" tabindex="-2" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content" style="width:800px; margin-left:-100px">


            <div class="modal-header bg-primary text-white">
                <h4 class="modal-title" id="exampleModalLabel">
                    <div style="font-weight:700">Error Aperturar Caja.</div>
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color:white; font-size:30px">&times;</span>
                </button>
            </div>

			<div class="modal-body">

			    <div class="container">
					<div class="row">
						<div class="col-md-9">
	                        <label ><span style="color:#0000ff; font-size:16px; font-weight:600">
								     Nombre(s)</span>
							</label>
	                        <input type="text" name="c_nombreE" id="c_nombreE" class="form-control"
	                               readonly="true">

	                        <br>
	                         <label><span style="color:#0000ff; font-size:16px; font-weight:600">
								 Apellido(s)</span>
						     </label>
	                        <input type="text" name="c_apellidoE" id="c_apellidoE" class="form-control" 
	                               readonly="true">
	                        <br>
						</div>

						<div class="col-md-3">
							<div id="div-imagenE"></div>	
						</div>
					</div>				
                </div> 
                <hr>

               <div id="mensajeError" class=" col-md-12 alert alert-danger" 
                    style="text-align:center; font-size: 18px"></div>

               <br>
            </div>   




			<div class="modal-footer">

                   
				<button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrar01"><span style="font-weight:700">Cerrar</span>
				</button>

			</div>

 

		</div>
	</div>
</div>
 
