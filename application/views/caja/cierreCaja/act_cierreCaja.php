<?php
$incluir = base_url() . "assets/images/nuevo.png";
$asterisco = ' style="color=red; font-size:16px"';
$ci = &get_instance();
$ci->load->library('Clssession');
$accion= $ci->clssession->accion(3,2); 



?>
        <br>            
        <div class="main-panel">
            <div class="content">
                <div class="page-inner mt--5">
                    <div class="row row-card-no-pd mt--2">
                        <div class="col-sm-12 col-md-12">
                            <div class="card card-stats card-round">
                                <div class="card-body ">
									 <h2><span style="color:blue">Cierre de Caja</span></h2>
		                             <h4>
		                             	<span>
		                             	<b><div id="nombreUsuario" name="nombreUsuario"></div></b></span>
		                             </h4>
                                </div>
                            </div>
                        </div>
                    </div>


                <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="table-responsive">
										<div id="bodyTabla">
										</div>
										
										
                                    </div>
                                </div>
                            </div>
                        </div>    
                </div>
            </div>

            <input type="hidden" id="idUsuario" name="idUsuario">
            <input type="hidden" id="nobUsuario" name="nobUsuario">





<!-- Modal -->
<div class="modal fade" id="cierre_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content" style="width:1100px; margin-left:-260px">


            <div class="modal-header bg-primary text-white">
                <h4 class="modal-title" id="exampleModalLabel">
                    <div id="titulo_mostrar" style="font-weight:700">Cerrar caja</div>
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color:white; font-size:30px">&times;</span>
                </button>
            </div>			

			<div class="modal-body">
				
			<form name="formulario" method="post" id="formulario" autocomplete="off">	
				
				
			  <div class="container">

				<div class="row">
					<div class="col-md-3">
                         <label style="padding-top: 0px; margin-top: 10px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:14px; font-weight:600">Fecha Apertura</span>
						 </label><br>
	                     <input type="text" name="fecha" id="fecha" 
	                           class="form-control" disabled=true> 
					</div>
					
					
					<div class="col-md-9">
                         <label style="padding-top: 0px; margin-top: 10px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:14px; font-weight:600">Observación</span>
						 </label><br>
	                     <textarea name="observacion" id="observacion" 
	                           class="form-control" disabled=true> 
	                     </textarea>      
					</div>
				</div>	
                <hr>  

				<div class="row">
					<div class="col-sm-2 col-md-2">
						
						<div class="card card-stats card-warning card-round">
							<a href="javascript:void(0)" onclick="javascript:detalleCaja_form(0)">
							<div class="card-body">
								<div class="row">
									
										<div class="col-5">
											<div class="icon-big text-center">
												<i class="flaticon-coins"></i>
											</div>
										</div>
										<div class="col-7 col-stats">

											<div class="numbers">
												
												<p class="card-category">Resumen Caja</p>
										
											</div>
										</div>

								</div>
							</div>
							</a>
							
						</div> 
						
					</div>

              


					<div class="col-sm-2 col-md-2">
						<div class="card card-stats card-primary card-round">
							<div class="card-body">
								<div class="row">
									<div class="col-12">
										<div class="numbers" style="text-align:center">
											<p class="card-category"><b>Apertura</b></p>
											<h4 class="card-title"><b><div id="apertura"></b></h4>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>


					<div class="col-sm-2 col-md-2">
						<div class="card card-stats card-primary card-round">
							<div class="card-body">
								<div class="row">
									<div class="col-12">
										<div class="numbers" style="text-align:center">
											<p class="card-category"><b>Venta</b></p>
											<h4 class="card-title"><b><div id="venta"></b></h4>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="col-sm-2 col-md-2">
						<div class="card card-stats card-primary card-round">
							<div class="card-body">
								<div class="row">
									<div class="col-12">
										<div class="numbers"  style="text-align:center">
											<p class="card-category"><b>Ingreso</b></p>
											<h4 class="card-title"><b><div id="ingreso"></b></h4>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="col-sm-2 col-md-2">
						<div class="card card-stats card-danger card-round">
							<div class="card-body">
								<div class="row">
									<div class="col-12">
										<div class="numbers" style="text-align:center">
											<p class="card-category"><b>Egreso</b></p>
											<h4 class="card-title"><b><div id="egreso"></b></h4>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>


					<div class="col-sm-2 col-md-2">
						<div class="card card-stats card-success card-round">
							<div class="card-body">
								<div class="row">
									<div class="col-12">
										<div class="numbers"  style="text-align:center">
											<p class="card-category"><b>Total Caja</b></p>
											<h4 class="card-title"><b><div id="totalCaja"></b></h4>

										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>	
				<hr>
                <div class="row">

                </div> 

				<div class="row">
					<div class="col-md-2">
                         <label style="padding-top: 0px; margin-top: 10px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:14px; font-weight:600">Observación Cierre</span>
						 </label>
					</div>
					
					<div class="col-md-10">
	                     <textarea name="observacionCierre" id="observacionCierre" 
	                           class="form-control"></textarea>
					</div>
				</div>	


			  </div>				
			</div>
			<div class="modal-footer">
                   
				<button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrar01"><span style="font-weight: 700">Cerrar</span></button>
		        <button type="button" class="btn btn-primary" onclick="guardarCierre_form()" id="guardar01">
		              <span style="font-weight: 700">Cerrar Caja</span> 
		        </button>

			</div>

            <input type="hidden" id="id_mod" name="id_mod">
            <input type="hidden" id="monto_totalCaja" name="monto_totalCaja">            


			</form>  

		</div>
	</div>
</div>



