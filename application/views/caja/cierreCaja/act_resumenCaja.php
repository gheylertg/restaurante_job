<?php
$incluir = base_url() . "assets/images/nuevo.png";
$asterisco = ' style="color=red; font-size:16px"';
$ci = &get_instance();
$ci->load->library('Clssession');
$accion= $ci->clssession->accion(3,2); 




?>


<!-- Modal -->
<div class="modal fade" id="cierre_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content" style="width:1100px; margin-left:-260px">


            <div class="modal-header bg-primary text-white">
                <h4 class="modal-title" id="exampleModalLabel">
                    <div style="font-weight:700">Cerrar caja</div>
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color:white; font-size:30px">&times;</span>
                </button>
            </div>

			<div class="modal-body">
				
			<form name="formulario" method="post" id="formulario" autocomplete="off">	
				
				
			  <div class="container">

				<div class="row">
					<div class="col-md-3">
                         <label style="padding-top: 0px; margin-top: 10px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:14px; font-weight:600">Fecha Apertura</span>
						 </label><br>
	                     <input type="text" name="fecha" id="fecha" 
	                           class="form-control" disabled=true> 
					</div>
					
					
					<div class="col-md-9">
                         <label style="padding-top: 0px; margin-top: 10px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:14px; font-weight:600">Observación</span>
						 </label><br>
	                     <textarea name="observacion" id="observacion" 
	                           class="form-control" disabled=true> 
	                     </textarea>      
					</div>
				</div>	
                <hr>  

				<div class="row">
					<div class="col-sm-2 col-md-2">
						
						<div class="card card-stats card-warning card-round">
							<a href="javascript:void(0)" onclick="javascript:detalleCaja_form(0)">
							<div class="card-body">
								<div class="row">
									
										<div class="col-5">
											<div class="icon-big text-center">
												<i class="flaticon-coins"></i>
											</div>
										</div>
										<div class="col-7 col-stats">

											<div class="numbers">
												
												<p class="card-category">Resumen Caja</p>
										
											</div>
										</div>

								</div>
							</div>
							</a>
							
						</div> 
						
					</div>

              


					<div class="col-sm-2 col-md-2">
						<div class="card card-stats card-primary card-round">
							<div class="card-body">
								<div class="row">
									<div class="col-12">
										<div class="numbers" style="text-align:center">
											<p class="card-category"><b>Apertura</b></p>
											<h4 class="card-title"><b><div id="apertura"></b></h4>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>


					<div class="col-sm-2 col-md-2">
						<div class="card card-stats card-primary card-round">
							<div class="card-body">
								<div class="row">
									<div class="col-12">
										<div class="numbers" style="text-align:center">
											<p class="card-category"><b>Venta</b></p>
											<h4 class="card-title"><b><div id="venta"></b></h4>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="col-sm-2 col-md-2">
						<div class="card card-stats card-primary card-round">
							<div class="card-body">
								<div class="row">
									<div class="col-12">
										<div class="numbers"  style="text-align:center">
											<p class="card-category"><b>Ingreso</b></p>
											<h4 class="card-title"><b><div id="ingreso"></b></h4>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="col-sm-2 col-md-2">
						<div class="card card-stats card-danger card-round">
							<div class="card-body">
								<div class="row">
									<div class="col-12">
										<div class="numbers" style="text-align:center">
											<p class="card-category"><b>Egreso</b></p>
											<h4 class="card-title"><b><div id="egreso"></b></h4>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>


					<div class="col-sm-2 col-md-2">
						<div class="card card-stats card-success card-round">
							<div class="card-body">
								<div class="row">
									<div class="col-12">
										<div class="numbers"  style="text-align:center">
											<p class="card-category"><b>Total Caja</b></p>
											<h4 class="card-title"><b><div id="totalCaja"></b></h4>

										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>	
				<hr>
                <div class="row">
                	<div class="col-md-12">
                		<span style="color:#0000ff; font-weight:600"><h3><b>Cierre de Caja</b></h3></span>

                	</div>	

                </div> 

				<div class="row">
					<div class="col-md-3">
                         <label style="padding-top: 0px; margin-top: 10px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:14px; font-weight:600">Observación Cierre</span>
						 </label>
					</div>
					
					<div class="col-md-9">
	                     <textarea name="observacionCierre" id="observacionCierre" 
	                           class="form-control"></textarea>
					</div>
				</div>	


			  </div>				
			</div>
			<div class="modal-footer">
                   
				<button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrar01"><span style="font-weight: 600">Cerrar</span></button>
		        <button type="button" class="btn btn-primary" onclick="guardarCierre_form()" id="guardar01">
		              <span style="font-weight: 600">Cerrar Caja</span> 
		        </button>

			</div>

            <input type="hidden" id="id_mod" name="id_mod">
            <input type="hidden" id="monto_totalCaja" name="monto_totalCaja">            


			</form>  

		</div>
	</div>
</div>


<!------ Resumen de caja  --------------------------------------- -->
<!-- Modal -->
<div class="modal fade" id="resumenCierre_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content" style="width:1100px; margin-left:-260px">
            <div class="modal-header bg-primary text-white">
                <h4 class="modal-title" id="exampleModalLabel">
                    <div id="titulo_mostrar" style="font-weight:700">Resumen caja</div>
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color:white; font-size:30px">&times;</span>
                </button>
            </div>			
			<div class="modal-body">

            <div class="row">
                <div class="col-md-12">
                    <span style="color:blue; font-weight: 700">USUARIO CAJA: </span><span id="datoUsuario"></span>
                </div>  
            </div>	
            <hr>



				<div class="row">
					<div class="col-md-3">
						<span style="color:#00f; font-weight: 600">INGRESOS</span>
						<div id="tabla-ingreso"></div>	
						<span style="color:#00f; font-weight: 600">TOTAL INGRESOS: </span>&nbsp;&nbsp;&nbsp;<span id="total-ingreso"></span>
                            <br><br>

						<span style="color:#00f; font-weight: 600">EGRESOS</span>
						<div id="tabla-egreso"></div>	
						<span style="color:#00f; font-weight: 600">TOTAL EGRESOS: </span>&nbsp;&nbsp;&nbsp;<span id="total-egreso"></span>
                            <br>
					</div>
					<div class="col-md-9">
						<span style="color:#00f; font-weight: 600; text-align: center">VENTAS</span>
						<div id="tabla-venta"></div>
						<span id="total-venta" style="color:#00f; font-weight: 600; text-align: right">
						</span>
						<hr>
                        
                        
                        <div class="row" style="border: 1px solid #00f; padding-bottom: 15px; margin-right:8px; margin-left:5px">
                        	<div class="col-md-3" style="text-align: center">
                        		<span style="color:#00f; font-weight: 600;">	
                        		TOTAL VENTAS</span><br>	
                        		<span id="tVenta" style="font-weight: 600;"></span>
                        	</div>	
                        	<div class="col-md-3" style="text-align: center">
                        		<span style="color:#00f; font-weight: 600;">	
                        		TOTAL INGRESO</span><br>	
                        		<span id="tIngreso" style="font-weight: 600;"></span>

                        	</div>	
                        	<div class="col-md-3" style="text-align: center">
                        		<span style="color:#00f; font-weight: 600;">	
                        		TOTAL EGRESO</span><br>	
								<span id="tEgreso" style="font-weight: 600;"></span>                        		
                        	</div>	
                        	<div class="col-md-3" style="text-align: center">
                        		<span style="color:#00f; font-weight: 600;">	
                        		TOTAL EN CAJA</span><br>	
                        		<span id="tCaja" style="font-weight: 600;"></span>	


                        	</div>	


					</div>	
					<br>
					<button type="button" class="btn btn-success" onclick="arqueo_form()" id="guardar04">
			       		 <span 	style="font-weight: 700">Arqueo de Dinero</span>
			        </button>

				</div>	


			</div>
		</div>
			<div class="modal-footer">
                   
				<button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrar03"><span style="font-weight: 700">Cerrar</span></button>

			</div>

		</div>
	</div>
</div>



<!------ Arqueo de moneda  --------------------------------------- -->
<!-- Modal -->
<div class="modal fade" id="arqueo_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content" style="width:1200px; margin-left:-300px">

            <div class="modal-header bg-primary text-white">
                <h4 class="modal-title" id="exampleModalLabel">
                    <div id="titulo_mostrar" style="font-weight:700">Arqueo de Moneda</div>
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color:white; font-size:30px">&times;</span>
                </button>
            </div>			

			<div class="modal-body">
                <div class="row">
                	<div class="col-md-12">
                		<span style="color:blue">USUARIO CAJA: </span><span id="usuarioCaja" style="color:black"></span>
                		<br><br>

                	</div>


                </div> 


				<div class="row">
					    <div class="col-md-1 pl-md-0 pr-md-0"></div>
						<div class="col-md-2 pl-md-0 pr-md-0">
							<div class="card card-pricing card-pricing-focus card-primary">
								<div class="card-header">
									<span class="card-title" style="font-size:14px; font-weight: 600">PESO</span>
									<div class="card-price">
										<span class="price"><span style="font-size:16px" id="a_totPeso"></span></span>
									</div>
								</div>
								<div class="card-body">
									<ul class="specification-list">
										<li>
											<span class="name-specification">PESO</span>
											<span class="status-specification"><span id="a_peso"></span>
										</li>
										<li>
											<span class="name-specification">DELIVERY</span>
											<span class="status-specification"><span id="a_pesoDel"></span>
										</li>
										<li>
											<span class="name-specification">VUELTO</span>
											<span class="status-specification"><span id="a_pesoVuelto"></
										</li>
									</ul>
								</div>
							</div>
						</div>

						<div class="col-md-2 pl-md-0 pr-md-0">
							<div class="card card-pricing card-pricing-focus card-info">
								<div class="card-header">
									<span class="card-title" style="font-size:14px; font-weight: 600">DOLAR</span>
									<div class="card-price">
										<span class="price"><span style="font-size:16px">$</span><span style="font-size:16px" id="a_totDolar"></span></span>
									</div>
								</div>
								<div class="card-body">
									<ul class="specification-list">
										<li>
											<span class="name-specification">DOLAR</span>
											<span class="status-specification"><span id="a_dolar"></span>
										</li>
										<li>
											<span class="name-specification">DELIVERY</span>
											<span class="status-specification"><span id="a_dolarDel"></span>
										</li>
										<li>
											<span class="name-specification">VUELTO</span>
											<span class="status-specification"><span id="a_dolarVuelto"></
										</li>
									</ul>
								</div>
							</div>
						</div>

						<div class="col-md-2 pl-md-0 pr-md-0">
							<div class="card card-pricing card-pricing-focus card-primary">
								<div class="card-header">
									<span class="card-title" style="font-size:14px; font-weight: 600">TARJ. DEBITO</span>
									<div class="card-price">
										<span class="price"><span style="font-size:16px" id="a_totDebito"></span></span>
									</div>
								</div>
								<div class="card-body">
									<ul class="specification-list">
										<li>
											<span class="name-specification">PESO</span>
											<span class="status-specification"><span id="a_debito"></span>
										</li>
										<li>
											<span class="name-specification">DELIVERY</span>
											<span class="status-specification"><span id="a_debitoDel"></span>
										</li>
										<li>
                                            <span class="name-specification">&nbsp;</span>
										</li>

									</ul>
								</div>
							</div>
						</div>

						<div class="col-md-2 pl-md-0 pr-md-0">
							<div class="card card-pricing card-pricing-focus card-info">
								<div class="card-header">
									<span class="card-title" style="font-size:14px; font-weight: 600">TARJ. CRÉDITO</span>
									<div class="card-price">
										<span class="price"><span style="font-size:16px" id="a_totCredito"></span></span>
									</div>
								</div>
								<div class="card-body">
									<ul class="specification-list">
										<li>
											<span class="name-specification">PESO</span>
											<span class="status-specification"><span id="a_credito"></span>
										</li>
										<li>
											<span class="name-specification">DELIVERY</span>
											<span class="status-specification"><span id="a_creditoDel"></span>
										</li>
										<li>
                                            <span class="name-specification">&nbsp;</span>
										</li>

									</ul>
								</div>
							</div>
						</div>

						<div class="col-md-2 pl-md-0 pr-md-0">
							<div class="card card-pricing card-pricing-focus card-primary">
								<div class="card-header">
									<span class="card-title" style="font-size:14px; font-weight: 600">TRANSFERENCIA</span>
									<div class="card-price">
										<span class="price"><span style="font-size:16px" id="a_totTransferencia"></span></span>
									</div>
								</div>
								<div class="card-body">
									<ul class="specification-list">
										<li>
											<span class="name-specification">PESO</span>
											<span class="status-specification"><span id="a_transferencia"></span>
										</li>
										<li>
											<span class="name-specification">DELIVERY</span>
											<span class="status-specification"><span id="a_transferenciaDel"></span>
										</li>
										<li>
                                            <span class="name-specification">&nbsp;</span>
										</li>

									</ul>
								</div>
							</div>
						</div>
						<div class="col-md-1 pl-md-0 pr-md-0"></div>


					</div>	
					<br>
            
            <input type="hidden" id="idCaja" name="idCaja">

			<div class="modal-footer">
                   


				<button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrar03"><span style="font-weight: 700">Cerrar</span></button>
				<br><br>

			</div>

		</div>
	</div>
</div>

