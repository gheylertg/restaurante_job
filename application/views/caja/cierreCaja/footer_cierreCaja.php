



<!--cargar datatable-->
<script>
    $(document).ready(function(){
         <?php  
         if($accion==1){  
         ?>
	    	cargarDataTable(0);

	     <?php  	
	     }
	     ?>
	    
	    <?php 
    	if($accion==2){
    	?>	
			swal("Debe existir una Caja Aperturada, para poder Cerrarla", "", {
					icon : "error",
					buttons: {        			
						confirm: {
							className : 'btn btn-danger'
						}
					},
			});
        <?php 
	    }	
	    ?>
	    
         <?php  
         if($accion==3){  
         ?>
	    	detalleCaja_form("a");
	     <?php  	
	     }
	     ?>

         <?php  
         if($accion==4){  
         ?>
			swal("Debe existir una caja aperturada para visualizar un Resumen de Caja", "", {
					icon : "error",
					buttons: {        			
						confirm: {
							className : 'btn btn-danger'
						}
					},
			});

	     <?php  	
	     }
	     ?>

    
	});



function cargarDataTable(id){
    $.ajax({
		url : "<?php echo site_url('Cj_cierreCaja/ajax_datatable')?>/"+id,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			 document.getElementById('bodyTabla').innerHTML = data.registro;

			 $(document).ready(function() {
                document.getElementById('nombreUsuario').innerHTML = data.nombreEmpresa;

				$('#basic-datatables').DataTable({
			    });	
			 });

		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}


function cerrarCaja_form(idCaja){

    $.ajax({
		url : "<?php echo site_url('Cj_cierreCaja/ajax_cerrarCaja')?>/"+idCaja,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			 $(document).ready(function() {
				document.getElementById('guardar01').disabled=false;
				document.getElementById('cerrar01').disabled=false;				 
				 
				 
			 	document.getElementById('id_mod').value = data.idCaja;
                document.getElementById('fecha').value = data.fecha;
                document.getElementById('observacion').value = data.observacion;
                document.getElementById('apertura').innerHTML = data.apertura;
                document.getElementById('venta').innerHTML = data.cobro;
                document.getElementById('ingreso').innerHTML = data.ingreso;
                document.getElementById('egreso').innerHTML = data.egreso;
                document.getElementById('totalCaja').innerHTML = data.totalCaja;
                document.getElementById('monto_totalCaja').value = data.monto_totalCaja;
                $('#cierre_modal').modal('show');
			 });

		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});

}


function guardarCierre_form(){

	//validar campos formulario
	swal({
		title: 'Esta seguro de Cerrar la Caja?',
			type: 'warning',
			buttons:{
				cancel: {
					visible: true,
						text : 'No, Cancelar!',
								className: 'btn btn-danger'
				},        			
				confirm: {
					text : 'Si, Cerrar Caja!',
					className : 'btn btn-success'
				}
			}
	}).then((willDelete) => {
		if (willDelete) {
			document.getElementById('guardar01').disabled=true;
			document.getElementById('cerrar01').disabled=true;	
				$.ajax({
					url : "<?=base_url();?>Cj_cierreCaja/ajax_guardarCierre",
					type: "POST",
					data: $('#formulario').serialize(),
					dataType: "JSON",
					success: function(data)
					{
						if(data.status==0){ 
							 document.getElementById('bodyTabla').innerHTML = data.registro;
							 $('#cierre_modal').modal('hide');

							 $(document).ready(function() {
								$('#basic-datatables').DataTable({
								});	
							});            	


							swal("La Caja fué cerrada satisfactoriamente!", "", {
								icon : "success",
								buttons: {        			
									confirm: {
										className : 'btn btn-success'
									}
								},
							});
						}	
					},
					error: function (jqXHR, textStatus, errorThrown)
					{
						alert('Error Insertando / modificanco registro, Comuniquese con el administrador');
					}
				});

		}
	});
}



function detalleCaja_form(idCaja){
 

	if(idCaja=="0"){
	    idCaja = document.getElementById('id_mod').value;	
	}
	
	if(idCaja=="a"){
	    idCaja = "0";	
	}
	
	
    $.ajax({
		url : "<?php echo site_url('Cj_cierreCaja/ajax_detalleCaja')?>/"+idCaja,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			 $(document).ready(function() {
			 	document.getElementById('tabla-ingreso').innerHTML = data.tablaIngreso;
			 	document.getElementById('total-ingreso').innerHTML = data.totalIngreso;
			 	document.getElementById('tabla-egreso').innerHTML = data.tablaEgreso;
			 	document.getElementById('total-egreso').innerHTML = data.totalEgreso;
			 	document.getElementById('tabla-venta').innerHTML = data.tablaVenta;
			 	document.getElementById('total-venta').innerHTML = data.totalVenta;

			 	document.getElementById('tVenta').innerHTML = data.tVenta;
			 	document.getElementById('tIngreso').innerHTML = data.totalIngreso;
			 	document.getElementById('tEgreso').innerHTML = data.totalEgreso;
				document.getElementById('tCaja').innerHTML = data.tCaja;
				document.getElementById('datoUsuario').innerHTML = data.nombreUsuario;



				document.getElementById('idCaja').value = data.idCaja;


			 	
                $('#resumenCierre_modal').modal('show'); 
			 });
		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
	
}



function arqueo_form(){
    idCaja = document.getElementById('idCaja').value;
    $.ajax({
		url : "<?php echo site_url('Cj_cierreCaja/ajax_arqueoCaja')?>/"+idCaja,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			 $(document).ready(function() {
				document.getElementById('a_peso').innerHTML = data.peso;
				document.getElementById('a_pesoDel').innerHTML = data.pesoDel;
				document.getElementById('a_pesoVuelto').innerHTML = data.pesoVuelto;
				document.getElementById('a_totPeso').innerHTML = data.totPeso;

				document.getElementById('a_dolar').innerHTML = data.dolar;
				document.getElementById('a_dolarDel').innerHTML = data.dolarDel;
				document.getElementById('a_dolarVuelto').innerHTML = data.dolarVuelto;
				document.getElementById('a_totDolar').innerHTML = data.totDolar;

				document.getElementById('a_debito').innerHTML = data.debito;
				document.getElementById('a_debitoDel').innerHTML = data.debitoDel;
				document.getElementById('a_totDebito').innerHTML = data.totDebito;				

				document.getElementById('a_credito').innerHTML = data.credito;
				document.getElementById('a_creditoDel').innerHTML = data.creditoDel;
				document.getElementById('a_totCredito').innerHTML = data.totCredito;				

				document.getElementById('a_transferencia').innerHTML = data.transferencia;
				document.getElementById('a_transferenciaDel').innerHTML = data.transferenciaDel;
				document.getElementById('a_totTransferencia').innerHTML = data.totTransferencia;	
				document.getElementById('usuarioCaja').innerHTML = data.nombreApellido;				

                $('#arqueo_modal').modal('show'); 
			 });
		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}





</script>
