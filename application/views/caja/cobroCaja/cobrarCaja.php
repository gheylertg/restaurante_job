<?php
$incluir = base_url() . "assets/images/nuevo.png";
$asterisco = ' style="color=red; font-size:16px"';
$ci = &get_instance();
$ci->load->library('Clssession');
$accion= $ci->clssession->accion(3,2); 
?>


<form name="formulario" method="post" id="formulario" autocomplete="off">	
    <div class="container">
       	<div class="card card-stats card-round">
        <div class="card-body ">
		    <div class="row">
			    <div class="col-md-4">
                    <div id="cliente"></div>
           	    </div>    


				<div class="col-md-2">
		            <label style="padding-top: 0px; margin-top: 0px;margin-left:0px;">
					    <span style="color:#0000ff; font-size:16px; font-weight:600">TASA CAMBIO:&nbsp;&nbsp; </span>
	  				</label><br>
                    <label style="padding-top: 0px; margin-top: 0px;margin-left:0px;">
					    <span style="font-size:14px; font-weight:600">Dolar:&nbsp;&nbsp;&nbsp; </span>
	  				</label>	  						    

	       	        <span id="tasaDolarF"  
	       	            style="padding-top: 0px; margin-top: 5px;margin-left:0px; font-size:14px;">
	                </span><br>

		            <label style="padding-top: 0px; margin-top: 0px;margin-left:0px;">
						<span style="font-size:14px; font-weight:600">Impuesto:&nbsp;&nbsp;&nbsp; </span>
	  				</label>	  						    

	       	        <span id="tasaImpuestoF" 
	       	            style="padding-top: 0px; margin-top: 5px;margin-left:0px; font-size:14px;"> 
	                </span>%<br>
          	    </div>        

			    <div class="col-md-2">
                    <label style="padding-top: 0px; margin-top: 0px;margin-left:0px;">
					    <span style="color:#0000ff; font-size:14px; font-weight:600">MONTO PEDIDO</span>
	  				</label><br>
		            <label style="padding-top: 0px; margin-top: 0px;margin-left:0px;">
						<span style="font-size:14px; font-weight:700">Monto:&nbsp;&nbsp;&nbsp; </span>
	  				</label>&nbsp;&nbsp;	  						    

	       	        <span id="mtoPedido"  
	       	            style="padding-top: 0px; margin-top: 5px;margin-left:0px; font-size:14px;">
	                </span><br>

		            <label style="padding-top: 0px; margin-top: 0px;margin-left:0px;">
					    <span style="font-size:14px; font-weight:600">Impuesto:&nbsp;&nbsp;&nbsp; </span>
	  				</label>	  						    

	       	        <span id="mtoImpuestoF" 
	       	              style="padding-top: 0px; margin-top: 5px;margin-left:0px; font-size:14px;">
	                </span><br>
           	    </div>  


			    <div class="col-md-2" style="text-align: center">
                    <label style="padding-top: 0px; margin-top: 0px;margin-left:0px;">
				    <span style="color:#0000ff; font-size:16px; font-weight:600">TOTAL A PAGAR</span>
	  				</label><br>
	       	            <span id="totalPagar"  
	       	                  style="padding-top: 0px; margin-top: 5px;margin-left:0px; font-size:16px;   font-weight: 700">
	                	</span><br>
           	    </div>  

			    <div class="col-md-2">
                    <label style="padding-top: 0px; margin-top: 0px;margin-left:0px;">
					    <span style="color:#0000ff; font-size:14px; font-weight:600">DESCUENTO %</span>
	  				</label><br>
					<input type="text" name="m_descuento" id="m_descuento" 
               		  	class="form-control" onblur="calcularPago();" onkeypress="validar_monto(event)"
               			style="text-align:right; padding:10px; font-size:14px"> 
        				<script type="text/javascript">
        					$("#m_descuento").maskMoney({symbol:'R$ ', thousands:'.', decimal:',', symbolStay: true}); 
        				</script>               							
           	    </div>  
			</div>
        </div>
        </div> <!--fin del card-->


        <div class="row">
		    <div class="col-md-6">
                <div class="row">
                   	<div class="col_md-12">
        				<span style="color:#0000ff; font-size:18px; font-weight:600; padding-left:20px">TIPO DE PAGO</span> 	
        			</div>	
                </div>	

		    	<table class="table-bordered table-head-bg-info table-bordered-bd-info mt-4">
				<thead>
				    <tr>
						<th scope="col" style="width:50%; height:50px; padding-left: 10px;">FORMA DE PAGO</th>
							<th scope="col" width="50%" style="text-align:center">MONTO</th>
						</tr>
        		</thead>

        		<tbody>
					<tr>
            			<td><span style="padding-left: 10px; color:blue; font-weight: 700">PESO</span>
            			</td>
            			<td style="padding: 0px">
							<input  type="text" name="m_moneda" id="m_moneda" 
	                    			class="form-control" onblur="calcularPago();" onkeypress="
	                    			validar_monto(event)" 
	                           		style="text-align:right; padding:10px;"> 
	                    			<script type="text/javascript">
	                    				$("#m_moneda").maskMoney({symbol:'R$ ', thousands:'.', decimal:',', symbolStay: true}); 
	                    			</script>            									
						</td>
    				</tr>	

					<tr>
						<td><span style="padding-left: 10px; color:blue; font-weight: 700">DOLAR</span>
						</td>
            			<td>
							<input  type="text" name="m_dolar" id="m_dolar" 
                			    class="form-control" onblur="calcularPago();" onkeypress="validar_monto(event)" 
                           	    style="text-align:right; padding:10px;">
                    			<script type="text/javascript">
                    				$("#m_dolar").maskMoney({symbol:'R$ ', thousands:'.', decimal:',', symbolStay: true}); 
                    			</script>            									
						</td>
					</tr>	

					<tr>
    					<td><span style="padding-left: 10px ; color:blue; font-weight: 700">TARJETA DEBITO</span>
    					</td>
    					<td>
							<input type="text" name="m_debito" id="m_debito" 
	                    		class="form-control" onblur="calcularPago();" onkeypress="validar_monto(event)"  style="text-align:right; padding:10px;"> 
	                    		<script type="text/javascript">
	                    			$("#m_debito").maskMoney({symbol:'R$ ', thousands:'.', decimal:',', symbolStay: true}); 
	                    		</script>            									
            			</td>
            		</tr>	

					<tr>
            			<td><span style="padding-left: 10px; color:blue; font-weight: 700">TARJETA CRÉDITO</span>
            			</td>
            			
            			<td>
							<input type="text" name="m_credito" id="m_credito" 
       							class="form-control" onblur="calcularPago();" onkeypress="validar_monto(event)" 
       							style="text-align:right; padding:10px;"> 
	    						<script type="text/javascript">
	    							$("#m_credito").maskMoney({symbol:'R$ ', thousands:'.', decimal:',', symbolStay: true}); 
	    						</script>            									
						</td>
					</tr>	

					<tr>
						<td><span style="padding-left: 10px; color:blue; font-weight: 700">TRANSFERENCIA</span></td>
						<td>
							<input type="text" name="m_transferencia" 
							       id="m_transferencia" 
           						   class="form-control" onblur="calcularPago();" onkeypress="validar_monto(event)" 
           						   style="text-align:right; padding:10px;"> 
    						<script type="text/javascript">
    							$("#m_transferencia").maskMoney({symbol:'R$ ', thousands:'.', decimal:',', symbolStay: true}); 
    						</script>            									
							
						</td>
					</tr>	
				</tbody>
        		</table>	
            </div>	

       		<div class="col-md-6">
                <div class="row">
                   	<div class="col-md-12">
                        <span style="color:#0000ff; font-size:18px; font-weight:600; ">RESUMEN DE PAGO </span>
                        <br><br>
                   	</div>	
                </div>	

                <div class="row">
                    <div class="col-md-12">
                      	<div class="card card-stats card-round">
                        <div class="card-body ">
							<div class="row">
	                          	<div class="col-md-12">
                                    <span style="color:#0000ff; font-size:20px; font-weight:600; ">POR COBRAR</span>
                               	</div>
                            </div>
	                               
                            <div class="row">
                            	<div class="col-md-12">
		                        	<label style="padding-top: 0px; margin-top: 0px;margin-left:0px;">
										 <span style="font-size:20px; font-weight:600">Peso:&nbsp;&nbsp;&nbsp; </span>
	  						    	</label>&nbsp;&nbsp;	  						    

	       	                    	<span id="porPagar"  
	       	                        	 style="padding-top: 0px; margin-top: 5px;margin-left:0px; font-size:20px;">
	                	        	</span><br>

		                        	<label style="padding-top: 0px; margin-top: 0px;margin-left:0px;">
										 <span style="font-size:20px; font-weight:600">Dolar:&nbsp;&nbsp;&nbsp; </span>
	  						    	</label>	  						    

	       	                    	<span id="porPagarDolar" 
	       	                        	 style="padding-top: 0px; margin-top: 5px;margin-left:0px; font-size:20px;">
	                	        	</span><br>
	                	        </div>
	                	    </div>   
                        </div>
                        </div>    
                    </div>
               	</div>

                <div id="etiquetaVuelto"> 
                    <div class="row">
                        <div class="col-md-12">
                           	<div class="card card-stats card-round">
                            <div class="card-body ">
								<div class="row">
                                	<div class="col-md-12">
                                        <span style="color:#0000ff; font-size:20px; font-weight:600; ">VUELTO</span>
                                	</div>
                                </div>


                                <div class="row">
                                	<div class="col-md-12"> 
                                        
			                        	<label style="padding-top: 0px; margin-top: 0px;margin-left:0px;">
											 <span style="font-size:20px; font-weight:600">Peso:&nbsp;&nbsp;&nbsp; </span>
		  						    	</label>&nbsp;&nbsp;	  						    

		       	                    	<span id="vuelto"  
		       	                        	 style="padding-top: 0px; margin-top: 5px;margin-left:0px; font-size:20px;">
		                	        	</span><br>

                                        <div id="etiquetaVueltoDolar" style="display:none">
			                        	<label style="padding-top: 0px; margin-top: 0px;margin-left:0px;">
											 <span style="font-size:20px; font-weight:600">Dolar:&nbsp;&nbsp;&nbsp; </span>
		  						    	</label>	  						    

		       	                    	<span id="vueltoDolar" 
		       	                        	 style="padding-top: 0px; margin-top: 5px;margin-left:0px; font-size:20px;">
		                	        	</span><br>
		                	            </div> <!--final etiqueta dolar-->
		                	        </div>
		                	    </div> 
                            </div>
                          	</div>
                        </div>
                    </div>	
                </div>	
            </div> <!--fin div columna de 6 de arriba -->
		</div>  <!-- Container -->
	</div>		

    <div class="row">
    	<div 
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrar01"><span style="font-weight:700">Cerrar</span></button>
		        <button type="button" class="btn btn-primary" onclick="guardar_form()" 
		                id="guardar01" disabled="true">
		              <span style="font-weight:700">Cobrar</span>
		        </button>

			</div>

            
            <input type="hidden" id="idDespacho" name="idDespacho">  
            <input type="hidden" id="idTipoDespacho" name="idTipoDespacho">  

            
            <input type="hidden" id="tasaDolar" name="tasaDolar">   
            <input type="hidden" id="tasaImpuesto" name="tasaImpuesto"> 

			<input type="hidden" id="montoImpuesto" name="montoImpuesto">   

            <!--<input type="hidden" id="euro" name="euro">   -->
            <input type="hidden" id="monto" name="monto">
            <input type="hidden" id="montoDolar" name="montoDolar">


            <input type="hidden" id="montoPorPagar" name="montoPorPagar">  
            <input type="hidden" id="montoPorPagarDolar" name="montoPorPagarDolar">  

			<input type="hidden" id="totalVuelto" name="totalVuelto" value = 0.00>  
			<input type="hidden" id="totalVueltoDolar" name="totalVueltoDolar" value="0.00"> 

			<input type="hidden" id="totalVueltoF" name="totalVueltoF">  
			<input type="hidden" id="totalVueltoDolarF" name="totalVueltoDolarF"> 
			<input type="hidden" id="tipoVuelto" name="tipoVuelto" value="0"> 



			</form>  

		</div>
	</div>
</div>





<!-- Ver modal -->

<!-- Modal -->
<div class="modal fade" id="ver_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content" style="width:900px; margin-left:-140px">


            <div class="modal-header bg-primary text-white">
                <h4 class="modal-title" id="exampleModalLabel">
                    <div id="titulo_mostrar" style="font-weight:700"></div>
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color:white; font-size:30px">&times;</span>
                </button>
            </div>

			<div class="modal-body">
			<form name="v_formulario" method="post" id="v_formulario" autocomplete="off">	
				
				
			  <div class="container">

				<div class="row">
					<div class="col-md-4">
                         <label style="padding-top: 0px; margin-top: 10px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:14px; font-weight:600">
							 	Tipo Operación <span style="color:#ff0000;">*</span></span>
						 </label>
					</div>
					
					<div class="col-md-8">
						<input type="text" name="v_tipoOperacion" id="v_tipoOperacion" class="form-control"
	                           readonly="true">
					</div>
				</div>	

				<div class="row">
					<div class="col-md-4">
                         <label style="padding-top: 0px; margin-top: 10px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:14px; font-weight:600">
							 	Tipo Movimiento <span style="color:#ff0000;">*</span></span>
						 </label>
					</div>
					
					<div class="col-md-8">
						<input type="text" name="v_tipoMovimiento" id="v_tipoMovimiento" class="form-control"
	                           readonly="true">
					</div>
				</div>	


				<div class="row">
					<div class="col-md-4">
                         <label style="padding-top: 0px; margin-top: 10px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:14px; font-weight:600">
							 	Tipo dinero <span style="color:#ff0000;">*</span></span>
						 </label>
					</div>
					
					<div class="col-md-8">
						<input type="text" name="v_tipoDinero" id="v_tipoDinero" class="form-control"
	                           readonly="true">
					</div>
				</div>	

				<div class="row" style="padding-top: 10px">
					<div class="col-md-4">
                         <label style="padding-top: 0px; margin-top: 10px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:14px; font-weight:600">Monto <span style="color:#ff0000;">*</span></span>
						 </label>

					</div>
					
					<div class="col-md-4">
	                    <input type="text" name="v_monto" id="v_monto" 
	                           class="form-control" style="text-align:right;" disabled="true"> 
	                    <script type="text/javascript">
	                    $("#v_monto").maskMoney({symbol:'R$ ', thousands:'.', decimal:',', symbolStay: true}); 
	                    </script>
					</div>
				</div>	


                <div class="row" style="padding-top: 10px">
                    <div class="col-md-4">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
                             <span style="color:#0000ff; font-size:16px; font-weight:600">
                                 Observación / Nota</span>
                             </label>
                    </div>
                    <div class="col-md-8">
                        <textarea style="padding-top: 5px;margin-right:-20px;margin-top: 0px;" 
                               name="v_observacion" id="v_observacion" 
                               class="form-control" rows="3" readonly="true"></textarea>
					</div>
				</div>				


                <div id="reverso" style="display: none">
                <div class="row" style="padding-top: 10px">
                    <div class="col-md-4">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
                             <span style="color:#0000ff; font-size:16px; font-weight:600">
                                 Fecha Reverso</span>
                             </label>
	                      <input type="text" name="v_fecha" id="v_fecha" 
	                           class="form-control" disabled="true"> 

                    </div>
                    <div class="col-md-8">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
                             <span style="color:#0000ff; font-size:16px; font-weight:600">
                                 Motivo Reverso</span>
                             </label>

                        <textarea style="padding-top: 5px;margin-right:-20px;margin-top: 0px;" 
                               name="v_motivo" id="v_motivo" 
                               class="form-control" rows="3" readonly="true"></textarea>
					</div>
				</div>				
			    </div>


                <br>  
				<span style="color:#ff0000; font-size:16px; text-align:left">Campo obligatorio (*)</span>
			  </div>				
			</div>
			<div class="modal-footer">
                   
				<button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrar01">Cerrar</button>
			</div>

			</form>  

		</div>
	</div>
</div>




<!---   Reverso de movimiento ---->

<div class="modal fade" id="reverso_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content" style="width:900px; margin-left:-140px">

            <div class="modal-header bg-primary text-white">
                <h4 class="modal-title" id="exampleModalLabel">
                    <div id="r_titulo_mostrar" style="font-weight:700"></div>
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color:white; font-size:30px">&times;</span>
                </button>
            </div>

			<div class="modal-body">
				
			<form name="r_formulario" method="post" id="r_formulario" autocomplete="off">	
				
				
			  <div class="container">

				<div class="row">
					<div class="col-md-4">
                        <label style="padding-top: 0px; margin-top: 10px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:14px; font-weight:600">
							 	Tipo Operación <span style="color:#ff0000;">*</span></span>
						</label>
						<input type="text" name="r_tipoOperacion" id="r_tipoOperacion" class="form-control"
	                           readonly="true">
					</div>
					
					<div class="col-md-4">
                         <label style="padding-top: 0px; margin-top: 10px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:14px; font-weight:600">
							 	Tipo Movimiento <span style="color:#ff0000;">*</span></span>
						 </label>
						<input type="text" name="r_tipoMovimiento" id="r_tipoMovimiento" class="form-control"
	                           readonly="true">
					</div>
					<div class="col-md-4">
                         <label style="padding-top: 0px; margin-top: 10px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:14px; font-weight:600">
							 	Tipo dinero <span style="color:#ff0000;">*</span></span>
						 </label>
						<input type="text" name="r_tipoDinero" id="r_tipoDinero" class="form-control"
	                           readonly="true">
					</div>
				</div>	

				<div class="row" style="padding-top: 10px">
					<div class="col-md-4">
                         <label style="padding-top: 0px; margin-top: 10px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:14px; font-weight:600">Monto <span style="color:#ff0000;">*</span></span>
						 </label>
	                    <input type="text" name="r_monto" id="r_monto" 
	                           class="form-control" style="text-align:right;" disabled="true"> 
	                    <script type="text/javascript">
	                    $("#r_monto").maskMoney({symbol:'R$ ', thousands:'.', decimal:',', symbolStay: true}); 
	                    </script>
					</div>
					
                    <div class="col-md-8">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
                             <span style="color:#0000ff; font-size:16px; font-weight:600">
                                 Observación / Nota</span>
                             </label>
                        <textarea style="padding-top: 5px;margin-right:-20px;margin-top: 0px;" 
                               name="r_observacion" id="r_observacion" 
                               class="form-control" rows="3" readonly="true"></textarea>

                    </div>
				</div>				
                <hr>


                <div class="row" style="padding-top: 10px">
                    <div class="col-md-3">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
                             <span style="color:#0000ff; font-size:16px; font-weight:600">
                                 Motivo Reverso <span style="color:#ff0000;">*</span></span>
                             </label>
                    </div>
                    <div class="col-md-9">
                        <textarea style="padding-top: 5px;margin-right:-20px;margin-top: 0px;" 
                               name="r_motivo" id="r_motivo" 
                               class="form-control" rows="3"></textarea>
                        <div id="lab_motivo" style="color:red; margin-left:9px; display:none"></div>       
                    </div>
				</div>				


				<span style="color:#ff0000; font-size:16px; text-align:left">Campo obligatorio (*)</span>
			  </div>				
			</div>
			<div class="modal-footer">
                   
				<button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrar02">Cerrar</button>

		        <button type="button" class="btn btn-primary" onclick="reversar_form()" id="guardar02">
		              Reversar 
		        </button>				
			</div>
			<input type="hidden" id="id_mod_reverso" name="id_mod_reverso">

			</form>  

		</div>
	</div>
</div>




<!-- Modal -->
<div class="modal fade" id="vuelto_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content" style="width:1000px; margin-left:-245px">


            <div class="modal-header bg-primary text-white">
                <h4 class="modal-title" id="exampleModalLabel">
                    <div style="font-weight:700">Forma de vuelto del pago</div>
                </h4>
                <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color:white; font-size:30px">&times;</span>
                </button>-->
            </div>

	
			<div class="modal-body">
                <br><br><br><br><br><br>  

				    <div class="container">
                        <div class="row">
						    <div class="col-md-12">
		                    <div class="card">
	                        <div class="card-header" style="text-align:left">
                                <div style="text-align: center"> 
          							<span style="color:#0000ff; font-size:18px; font-weight:700; padding-left:20px">SELECCIONE LA MONEDA DEL VUELTO</span> 	
          						</div>	
                            </div>
                            </div>
                            </div>
                        </div>        
                        <br><br>  

                        <div class="row">
							<div class="col-md-12">                        	
		                    <div class="card">
	                        <div class="card-header" style="text-align:center">
                            <div class="row">   
						    <div class="col-md-6" style="text-align: center">
						        <button type="button" class="btn btn-primary" onclick="tipoVueltos('1')"> 
						        	<br><br>&nbsp;&nbsp;&nbsp;&nbsp;
		              				<span id="vueltoPesoV"  
		       	                        	 style="padding-top: 0px; margin-top: 5px;margin-left:0px; font-size:20px;">
		                	        </span>
		                	        &nbsp;&nbsp;&nbsp;&nbsp;<br><br>
		        				</button>
						    </div>

						    <div class="col-md-6" style="text-align: center">
						        <button type="button" class="btn btn-success" onclick="tipoVueltos('2')" 
		        				    >
		        					<br><br>&nbsp;&nbsp;&nbsp;
		              				<span id="vueltoDolarV"  
		       	                        	 style="padding-top: 0px; margin-top: 5px;margin-left:0px; font-size:20px;">

		                	        </span>
		                	        &nbsp;&nbsp;<br><br>
		        				</button>
						    </div>

						</div>
						</div>
						</div>  
						</div>  

						<div class="row">
							<div class="col-md-12">

                                <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
							</div>	

						</div>	

                    </div>  
                    



			</div>		
		</div>
	</div>
</div>


