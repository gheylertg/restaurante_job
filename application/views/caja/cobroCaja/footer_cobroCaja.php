



<!--cargar datatable-->
<script>
	
    $(document).ready(function(){
        setInterval(function() {
            mostrarNewCobro(0);        
            },15000);


	    <?php 
    	if($accion==1){
    	?>	
			cargarDataTable(0);        
		<?php 
	    }	
	    ?>
		
	    <?php 
    	if($accion==2){
    	?>	
			swal("Debe aperturar una caja, para poder cobrar un despacho", "", {
					icon : "error",
					buttons: {        			
						confirm: {
							className : 'btn btn-danger'
						}
					},
			});
        <?php 
	    }	
	    ?>
		
		
		
		
		
	    
	});


//Validar boton enter de seleccionar beneficiario en hcm
function validar_monto(e,opcion) {
  tecla = (document.all) ? e.keyCode : e.which;
  if (tecla==13) {
		calcularPago();
        return false;
  }
  if (tecla==27) {
        alert("Escape");
        //calcularPago();
        return false;
  }

}


function cargarDataTable(id){
    $.ajax({
		url : "<?php echo site_url('Cj_cobroCaja/ajax_datatable')?>/"+id,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			 document.getElementById('bodyTabla').innerHTML = data.registro;

			 $(document).ready(function() {
                document.getElementById('nombreUsuario').innerHTML = data.nombreEmpresa;

				$('#basic-datatables').DataTable({
			    });	
			 });

		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}


function mostrarNewCobro(){
    $.ajax({
        url : "<?php echo site_url('Cj_cobroCaja/ajax_datatable_new')?>",
        type: "GET",                     
        dataType: "JSON",
        success: function(data){
            if(data.status==1){
                 document.getElementById('bodyTabla').innerHTML = data.registro;
                 $(document).ready(function() {
                    $('#basic-datatables').DataTable({
                    }); 
                 });
            }    
        },
        error: function (jqXHR, textStatus, errorThrown){
            alert('Error consultando la Base de Datos');
        }
    });
}




function cobrar_form(id){
	document.getElementById('guardar01').disabled=false;
	document.getElementById('cerrar01').disabled=false;  

    $.ajax({
		url : "<?php echo site_url('Cj_cobroCaja/ajax_cobrar')?>/"+id,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){

            document.getElementById('idDespacho').value=data.idDespacho;  
            document.getElementById('idTipoDespacho').value=data.idTipoDespacho;  
                          

			document.getElementById('cliente').innerHTML = data.cliente;	  
			//document.getElementById('mesa').innerHTML = data.mesa;	  

            //document.getElementById('mesonero').innerHTML = data.mesonero;	  

            //tasa del dolar
            document.getElementById('tasaDolarF').innerHTML = data.tasaDolarF;	              
            document.getElementById('tasaDolar').value = data.tasaDolar;    

            document.getElementById('tasaImpuestoF').innerHTML = data.impuestoF;                

            document.getElementById('tasaImpuesto').value = data.impuesto;
            document.getElementById('montoImpuesto').value = data.mtoImpuesto;

            document.getElementById('totalPagar').innerHTML = data.totalF;



            document.getElementById('mtoPedido').innerHTML = data.montoF; 
            document.getElementById('mtoImpuestoF').innerHTML = data.mtoImpuestoF;


            //document.getElementById('mtoPesoF').innerHTML = data.montoF;                
            //document.getElementById('mtoDolarF').innerHTML = data.montoDolarF;                

            document.getElementById('monto').value = data.monto;
            document.getElementById('montoDolar').value = data.montoDolar;


		    document.getElementById('porPagar').innerHTML = data.totalF;
            document.getElementById('porPagarDolar').innerHTML = data.montoDolarF;

            document.getElementById('montoPorPagar').innerHTML = data.monto;
            document.getElementById('montoPorPagarDolar').innerHTML = data.montoDolar;
            
            document.getElementById('guardar01').disabled=true;	
            document.getElementById('cerrar01').disabled=false; 
            //document.getElementById('calcular01').disabled=false; 
            //document.getElementById('limpiar01').disabled=false; 

            document.getElementById('m_moneda').value = 0.00;            
            document.getElementById('m_dolar').value = 0.00;            
            document.getElementById('m_debito').value = 0.00;
            document.getElementById('m_credito').value = 0.00;                        
            document.getElementById('m_descuento').value = 0.00;
            document.getElementById('m_transferencia').value = 0.00;

            document.getElementById('tipoVuelto').value = 0; 
            document.getElementById('totalVuelto').value = 0; 
            document.getElementById('totalVueltoDolar').value = 0; 



            document.getElementById('etiquetaVuelto').style.display = "none"; 

		    $('#mod_modal').modal('show'); 

		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}



function guardarPago(){
    

    $.ajax({
        url : "<?=base_url();?>Cj_cobroCaja/ajax_guardar_cobro",
        type: "POST",
        data: $('#formulario').serialize(),
        dataType: "JSON",
        success: function(data)
        {
            if(data.status==0){ 
                 document.getElementById('bodyTabla').innerHTML = data.registro;
                 $('#mod_modal').modal('hide');

                 $(document).ready(function() {
                    $('#basic-datatables').DataTable({
                    }); 
                });             


                swal("Cobro despacho correctamente!", "", {
                    icon : "success",
                    buttons: {                  
                        confirm: {
                            className : 'btn btn-success'
                        }
                    },
                });
            }   
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error Insertando / modificanco registro, Comuniquese con el administrador');
        }
    });
 
}


function guardar_form(){
    totalVuelto = parseFloat(document.getElementById('totalVuelto').value);    
    totalVuelto = totalVuelto.toFixed(2);
    if(totalVuelto > 0){
        mtoDolar = document.getElementById('m_dolar').value;     
        mtoDolar = mtoDolar.split('.').join(''); 
        mtoDolar = mtoDolar.replace(/,/g,'.');
        mtoDolar = parseFloat(mtoDolar);
        if(mtoDolar>0){
             totalVueltoF = document.getElementById('totalVueltoF').value;     
             totalVueltoDolarF = document.getElementById('totalVueltoDolarF').value;     
             document.getElementById('vueltoPesoV').innerHTML="PESO ( " + totalVueltoF + " )";
             document.getElementById('vueltoDolarV').innerHTML="DOLAR ( " + totalVueltoDolarF + " )";
             document.getElementById('tipoVuelto').value="2";     
             $('#vuelto_modal').modal('show');    
        }else{
            document.getElementById('guardar01').disabled=false;
            document.getElementById('cerrar01').disabled=false;    
            document.getElementById('tipoVuelto').value="1";     
            guardarPago();
        }     
    }else{
        document.getElementById('guardar01').disabled=true;
        document.getElementById('cerrar01').disabled=true;    
        guardarPago();
    }
}  

function tipoVueltos(tipo){
    document.getElementById('guardar01').disabled=false;
    document.getElementById('cerrar01').disabled=false;    
    document.getElementById('tipoVuelto').value=tipo;     
    $('#vuelto_modal').modal('hide');    
    guardarPago();
}    







///////////////////////////////////////////////////////////////////////////
function formatearNumero(numero){
    separador= "."; // separador para los miles
    sepDecimal= ','; // separador para los decimales
    simbol = "$";
    num = numero.split('.').join(''); 
    num = num.replace(/,/g,'.');

    num +='';
    var splitStr = num.split('.');
    var splitLeft = splitStr[0];
    var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
    var regx = /(\d+)(\d{3})/;
    while (regx.test(splitLeft)) {
        splitLeft = splitLeft.replace(regx, '$1' + separador + '$2');
    }

    montoF = splitLeft +splitRight;
    return montoF;

 }


function calcularPago(){

    monto = parseFloat(document.getElementById('monto').value);
    montoDescuento = 0.00;    
    descuento = document.getElementById('m_descuento').value;

    if(descuento=="" || descuento=="0,00" || descuento=="0.00"){
        v_descuento = 0.00;
        document.getElementById('m_descuento').value="0";

    }else{
        v_descuento = descuento.split('.').join(''); 
        v_descuento = v_descuento.replace(/,/g,'.');
        v_descuento = parseFloat(v_descuento);
        if(v_descuento < 100){
            montoDescuento = (v_descuento * monto)/100;
        }else{

            document.getElementById('m_descuento').value="0";           
            swal("El porcentaje de decuento no puede ser igual o mayor a 100%", "", {
                icon : "error",
                buttons: {                  
                    confirm: {
                        className : 'btn btn-danger'
                    }
                },
            });
            return false;
        }    
    }
    mtoPagar = monto - montoDescuento;


    impuesto = document.getElementById('tasaImpuesto').value;
    totalImp = 0.00;

    

    if(impuesto!="0.00" && impuesto!="0,00"){
        //v_impuesto = impuesto.split('.').join(''); 
        //v_impuesto = v_impuesto.replace(/,/g,'.');
        v_impuesto = parseFloat(impuesto);
        totalImp = mtoPagar * (v_impuesto / 100);
    }    


    mtoPagar = mtoPagar + totalImp;


    

    //pago en moneda
    moneda = document.getElementById('m_moneda').value;

    if(moneda=="" || moneda=="0,00" || moneda=="0.00"){
        v_moneda = 0.00;
        document.getElementById('m_moneda').value="0";
    }else{
        v_moneda = moneda.split('.').join(''); 
        v_moneda = v_moneda.replace(/,/g,'.');
        v_moneda = parseFloat(v_moneda);
    }

    dolar = document.getElementById('m_dolar').value;

    if(dolar=="" || dolar=="0,00" || dolar=="0.00"){
        v_dolar = 0.00;
        document.getElementById('m_dolar').value="0";
    }else{
        v_dolar = dolar.split('.').join(''); 
        v_dolar = v_dolar.replace(/,/g,'.');
        v_dolar = parseFloat(v_dolar);

        tasaDolar = document.getElementById('tasaDolar').value;
        v_tasaDolar = parseFloat(tasaDolar);
        v_dolar = tasaDolar * v_dolar;
    }

    debito = document.getElementById('m_debito').value;
    if(debito=="" || debito=="0,00" || debito=="0.00"){
        v_debito = 0.00;
        document.getElementById('m_debito').value="0";
    }else{
        v_debito = debito.split('.').join(''); 
        v_debito = v_debito.replace(/,/g,'.');
        v_debito = parseFloat(v_debito);
    }

    credito = document.getElementById('m_credito').value;
    if(credito=="" || credito=="0,00" || credito=="0.00"){
        v_credito = 0.00;
        document.getElementById('m_credito').value="0";
    }else{
        v_credito = credito.split('.').join(''); 
        v_credito = v_credito.replace(/,/g,'.');
        v_credito = parseFloat(v_credito);
    }


    transferencia = document.getElementById('m_transferencia').value;
    if(transferencia=="" || transferencia=="0,00" || transferencia=="0.00"){
        v_transferencia = 0.00;
        document.getElementById('m_transferencia').value="0";
    }else{
        v_transferencia = transferencia.split('.').join(''); 
        v_transferencia = v_transferencia.replace(/,/g,'.');
        v_transferencia = parseFloat(v_transferencia);
    }



    subTotal = 0.00;
    subTotal = v_moneda + v_dolar + v_debito + v_credito + v_transferencia;

    vuelto = 0.00; 
    vueltoDolar=0.00;
    porPagar = 0.00;

    if(subTotal > mtoPagar){
        vuelto = subTotal - mtoPagar;
        vuelto = vuelto.toFixed(2);
        vueltoDolar = vuelto / tasaDolar;
        vueltoDolar = vueltoDolar.toFixed(2);
        formato = "0,00";
        formatoD = "0,00";

        document.getElementById('totalVuelto').value=vuelto;
        document.getElementById('totalVueltoDolar').value=vueltoDolar;

        //formatoV = vuelto.toFixed(2);
        formatoV = vuelto;
        formatoV = formatoV.split('.').join(',');
        formatoV = formatearNumero(formatoV); 
        //formatoDolarV = vueltoDolar.toFixed(2);
        formatoDolarV = vueltoDolar;
        formatoDolarV = formatoDolarV.split('.').join(',');
        formatoDolarV = formatearNumero(formatoDolarV); 
        document.getElementById('porPagar').innerHTML = "0,00";
        document.getElementById('porPagarDolar').innerHTML = "0,00";
        document.getElementById('vuelto').innerHTML = formatoV;
        document.getElementById('vueltoDolar').innerHTML = formatoDolarV;
        document.getElementById('totalVueltoF').value = formatoV;
        document.getElementById('totalVueltoDolarF').value = formatoDolarV;
        document.getElementById('guardar01').disabled = false;

        document.getElementById('etiquetaVuelto').style.display="inline";

        document.getElementById('vuelto').innerHTML = formatoV;

        if(v_dolar > 0){
            document.getElementById('vueltoDolar').innerHTML = formatoDolarV;
            document.getElementById('etiquetaDolar').style.display="inline";            
        }else{
            document.getElementById('vueltoDolar').innerHTML = "0,00";
            document.getElementById('etiquetaVueltoDolar').style.display="none";            
        }    
    }else{
        vuelto = 0.00;
        porPagarDolar = 0.00;
        porPagar = mtoPagar - subTotal;  


        //alert("porPagar: " + porPagar);  


        if(porPagar==0){
           document.getElementById('guardar01').disabled = false;
        }else{
           document.getElementById('guardar01').disabled = true;

        }   

        porPagarDolar = porPagar / tasaDolar;
        porPagar = porPagar.toFixed(2);
        porPagarF = porPagar.split('.').join(',');
        formato = porPagarF;
        document.getElementById('porPagar').innerHTML = formato;
        porPagarDolar = porPagarDolar.toFixed(2);
        porPagarDolarF = porPagarDolar.split('.').join(',');
        formatoDolar = porPagarDolarF;
        document.getElementById('porPagarDolar').innerHTML = formatoDolar;
        document.getElementById('etiquetaVuelto').style.display="none";
        document.getElementById('vuelto').innerHTML ="0.00";
        document.getElementById('vueltoDolar').innerHTML ="0.00";
        document.getElementById('totalVuelto').value=0.00;
        document.getElementById('totalVueltoDolar').value=0.00;
    }
}






</script>
