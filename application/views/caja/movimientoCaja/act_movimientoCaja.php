<?php
$incluir = base_url() . "assets/images/nuevo.png";
$asterisco = ' style="color=red; font-size:16px"';
$ci = &get_instance();
$ci->load->library('Clssession');
$accion= $ci->clssession->accion(10,2); 

?>
        <br>            
        <div class="main-panel">
            <div class="content">
                <div class="page-inner mt--5">
                    <div class="row row-card-no-pd mt--2">
                        <div class="col-sm-12 col-md-12">
                            <div class="card card-stats card-round">
                                <div class="card-body ">
									 <h2><span style="color:blue">Movimiento de Caja</span></h2>
		                             <h4>
		                             	<span>
		                             	<b><div id="nombreUsuario" name="nombreUsuario"></div></b></span>
		                             </h4>
                                </div>
                            </div>
                        </div>
                    </div>


                <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
	                                <?php
	                                if($accion==1){
	                                ?>
	                                <div  style="text-align:right">
	                                    <a href="javascript:void(0)" onclick="javascript:add_form()"> 
	                                       <img src="<?=$incluir;?>" style="width:35px; height:35px" 
	                                            alt="Incluir" title="Incluir movimiento de caja">
	                                    </a>
	                                </div>
	                                <?php
	                                }
	                                ?>
									<br>
                                    <div class="table-responsive">
										<div id="bodyTabla">
										</div>
										
										
                                    </div>
                                </div>
                            </div>
                        </div>    
                </div>
            </div>

            <input type="hidden" id="idUsuario" name="idUsuario">
            <input type="hidden" id="nobUsuario" name="nobUsuario">





<!-- Modal -->
<div class="modal fade" id="mod_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content" style="width:900px; margin-left:-140px">


            <div class="modal-header bg-primary text-white">
                <h4 class="modal-title" id="exampleModalLabel">
                    <div id="titulo_mostrar" style="font-weight:700"></div>
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color:white; font-size:30px">&times;</span>
                </button>
            </div>

			<div class="modal-body">
				
			<form name="formulario" method="post" id="formulario" autocomplete="off">	
				
				
			  <div class="container">

				<div class="row">
					<div class="col-md-4">
                         <label style="padding-top: 0px; margin-top: 10px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:14px; font-weight:600">Tipo Operación <span style="color:#ff0000;">*</span></span>
						 </label>

					</div>
					
					
					<div class="col-md-8">
						<div id="cbo_anid_tipoOperacion"></div>
						<div id="lab_tipoOperacion" style="color:red; margin-left:9px; display:none"></div>
					</div>
				</div>	


				<div class="row" style="padding-top: 10px">
					<div class="col-md-4">
                         <label style="padding-top: 0px; margin-top: 10px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:14px; font-weight:600">Tipo Movimiento <span style="color:#ff0000;">*</span></span>
						 </label>

					</div>
					
					
					<div class="col-md-8">
						<div id="cbo_anid_tipoMovimiento"></div>
						<div id="lab_tipoMovimiento" style="color:red; margin-left:9px; display:none"></div>
					</div>
				</div>	




				<div class="row" style="padding-top: 10px">
					<div class="col-md-4">
                         <label style="padding-top: 0px; margin-top: 10px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:14px; font-weight:600">Monto <span style="color:#ff0000;">*</span></span>
						 </label>

					</div>
					
					<div class="col-md-4">
	                    <input type="text" name="m_monto" id="m_monto" 
	                           class="form-control" value="0,00" style="text-align:right;" > 
	                    <script type="text/javascript">
	                    $("#m_monto").maskMoney({symbol:'R$ ', thousands:'.', decimal:',', symbolStay: true}); 
	                    </script>
	                    <div id="lab_monto" style="color:red; margin-left:9px; display:none"></div>

					</div>
				</div>	


                <div class="row" style="padding-top: 10px">
                    <div class="col-md-3">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
                             <span style="color:#0000ff; font-size:16px; font-weight:600">
                                 Observación / Nota</span>
                             </label>
                    </div>
                    <div class="col-md-9">
                        <textarea style="padding-top: 5px;margin-right:-20px;margin-top: 0px;" 
                               name="m_observacion" id="m_observacion" 
                               class="form-control" rows="3"></textarea>
					</div>
				</div>				

                <br>  
				<span style="color:#ff0000; font-size:16px; text-align:left">Campo obligatorio (*)</span>
				<input type="text" name="no_tocar" id="no_tocar" style="border: 0">
			  </div>				
			</div>
			<div class="modal-footer">
                   
				<button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrar01"><span style="font-weight:700">Cerrar</span></button>
		        <button type="button" class="btn btn-primary" onclick="actualizar_form()" id="guardar01">
		              <span style="font-weight: 700">Guardar</span> 
		        </button>

			</div>



            <input type="hidden" id="accion_formulario" name="accion_formulario" value="0">
            <input type="hidden" id="id_mod" name="id_mod">
            <input type="hidden" id="id_tm" name="id_tm">



			</form>  

		</div>
	</div>
</div>








<!---   Reverso de movimiento ---->

<div class="modal fade" id="reverso_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content" style="width:900px; margin-left:-140px">


            <div class="modal-header bg-primary text-white">
                <h4 class="modal-title" id="exampleModalLabel">
                    <div id="r_titulo_mostrar" style="font-weight:700"></div>
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color:white; font-size:30px">&times;</span>
                </button>
            </div>


			<div class="modal-body">
				
			<form name="r_formulario" method="post" id="r_formulario" autocomplete="off">	
				
				
			  <div class="container">

				<div class="row">
					<div class="col-md-4">
                        <label style="padding-top: 0px; margin-top: 10px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:14px; font-weight:600">
							 	Tipo Operación <span style="color:#ff0000;">*</span></span>
						</label>
						<input type="text" name="r_tipoOperacion" id="r_tipoOperacion" class="form-control"
	                           readonly="true">
					</div>
					
					<div class="col-md-4">
                         <label style="padding-top: 0px; margin-top: 10px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:14px; font-weight:600">
							 	Tipo Movimiento <span style="color:#ff0000;">*</span></span>
						 </label>
						<input type="text" name="r_tipoMovimiento" id="r_tipoMovimiento" class="form-control"
	                           readonly="true">
					</div>
				</div>	

				<div class="row" style="padding-top: 10px">
					<div class="col-md-4">
                         <label style="padding-top: 0px; margin-top: 10px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:14px; font-weight:600">Monto <span style="color:#ff0000;">*</span></span>
						 </label>
	                    <input type="text" name="r_monto" id="r_monto" 
	                           class="form-control" style="text-align:right;" disabled="true"> 
	                    <script type="text/javascript">
	                    $("#r_monto").maskMoney({symbol:'R$ ', thousands:'.', decimal:',', symbolStay: true}); 
	                    </script>
					</div>
					
                    <div class="col-md-8">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
                             <span style="color:#0000ff; font-size:16px; font-weight:600">
                                 Observación / Nota</span>
                             </label>
                        <textarea style="padding-top: 5px;margin-right:-20px;margin-top: 0px;" 
                               name="r_observacion" id="r_observacion" 
                               class="form-control" rows="3" readonly="true"></textarea>

                    </div>
				</div>				
                <hr>

                <div class="row" style="padding-top: 10px">
                    <div class="col-md-3">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
                             <span style="color:#0000ff; font-size:16px; font-weight:600">
                                 Motivo Reverso <span style="color:#ff0000;">*</span></span>
                             </label>
                    </div>
                    <div class="col-md-9">
                        <textarea style="padding-top: 5px;margin-right:-20px;margin-top: 0px;" 
                               name="r_motivo" id="r_motivo" 
                               class="form-control" rows="3"></textarea>
                        <div id="lab_motivo" style="color:red; margin-left:9px; display:none"></div>       
                    </div>
				</div>				


				<span style="color:#ff0000; font-size:16px; text-align:left">Campo obligatorio (*)</span>
			  </div>				
			</div>
			<div class="modal-footer">
                   
				<button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrar02"><span style="font-weight: 700">Cerrar</span></button>

		        <button type="button" class="btn btn-primary" onclick="reversar_form()" id="guardar02">
		              <span style="font-weight: 700">Reversar</span>
		        </button>				
			</div>


			<input type="hidden" id="id_mod_reverso" name="id_mod_reverso">

			</form>  

		</div>
	</div>
</div>





<!---  Modificar Apertura ---->

<div class="modal fade" id="modApertura_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content" style="width:900px; margin-left:-140px">

            <div class="modal-header bg-primary text-white">
                <h4 class="modal-title" id="exampleModalLabel">
                    <div id="r_titulo_mostrar_aper" style="font-weight:700"></div>
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color:white; font-size:30px">&times;</span>
                </button>
            </div>

			<div class="modal-body">
				
			<form name="aper_formulario" method="post" id="aper_formulario" autocomplete="off">	
				
			

			  <div class="container">

				<div class="row">
					<div class="col-md-4">
                        <label style="padding-top: 0px; margin-top: 10px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:14px; font-weight:600">
							 	Tipo Operación <span style="color:#ff0000;">*</span></span>
						</label>
						<input type="text" name="a_tipoOperacion" id="a_tipoOperacion" class="form-control"
	                           readonly="true">
					</div>
					
					<div class="col-md-4">
                         <label style="padding-top: 0px; margin-top: 10px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:14px; font-weight:600">
							 	Tipo Movimiento <span style="color:#ff0000;">*</span></span>
						 </label>
						<input type="text" name="a_tipoMovimiento" id="a_tipoMovimiento" class="form-control"
	                           readonly="true">
					</div>
				</div>	
               <hr>
				<div class="row" style="padding-top: 10px">
					<div class="col-md-4">
                         <label style="padding-top: 0px; margin-top: 10px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:14px; font-weight:600">Monto <span style="color:#ff0000;">*</span></span>
						 </label>
                    </div>
                    <div class="col-md-4">
	                    <input type="text" name="aper_monto" id="aper_monto" 
	                           class="form-control" style="text-align:right;"> 
	                    <script type="text/javascript">
	                    $("#aper_monto").maskMoney({symbol:'R$ ', thousands:'.', decimal:',', symbolStay: true}); 
	                    </script>
					</div>
				</div>


				<div class="row" style="padding-top: 10px">					
                    <div class="col-md-4">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
                             <span style="color:#0000ff; font-size:16px; font-weight:600">
                                 Observación / Nota</span>
                             </label>
                    </div>
                    <div class="col-md-8">
                        <textarea style="padding-top: 5px;margin-right:-20px;margin-top: 0px;" 
                               name="aper_observacion" id="aper_observacion" 
                               class="form-control" rows="3"></textarea>

                    </div>
				</div>				
                <hr>

				<span style="color:#ff0000; font-size:16px; text-align:left">Campo obligatorio (*)</span>
			  </div>				
			</div>
			<div class="modal-footer">
                   
				<button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrar04"><span style="font-weight: 700">Cerrar</span></button>

		        <button type="button" class="btn btn-primary" onclick="guardar_modApertura_form()" id="guardar04">
		              <span style="font-weight: 700">Guardar</span>
		        </button>				
			</div>


			<input type="hidden" id="id_mod_apertura" name="id_mod_apertura">

			</form>  

		</div>
	</div>
</div>
