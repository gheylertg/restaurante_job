



<!--cargar datatable-->
<script>
    $(document).ready(function(){
         <?php  
         if($accion==1){  
         ?>
	    	cargarDataTable(0);

	     <?php  	
	     }
	     ?>
	    
	    <?php 
    	if($accion==2){
    	?>	
			swal("Debe aperturar una caja, para poder registrar un moviento", "", {
					icon : "error",
					buttons: {        			
						confirm: {
							className : 'btn btn-danger'
						}
					},
			});
        <?php 
	    }	
	    ?>
	});



function cargarDataTable(id){
    $.ajax({
		url : "<?php echo site_url('Cj_movimientoCaja/ajax_datatable')?>/"+id,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			 document.getElementById('bodyTabla').innerHTML = data.registro;

			 $(document).ready(function() {
                document.getElementById('nombreUsuario').innerHTML = data.nombreEmpresa;

				$('#basic-datatables').DataTable({
			    });	
			 });

		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}




function add_form(){

	$('#formulario')[0].reset(); // reset form on modals
   	document.getElementById('guardar01').disabled=false;
	document.getElementById('cerrar01').disabled=false; 


    document.getElementById('lab_tipoOperacion').style.display="none";
    document.getElementById('lab_tipoMovimiento').style.display="none";
    //document.getElementById('lab_tipoDinero').style.display="none";
    document.getElementById('lab_monto').style.display="none";

    document.getElementById('titulo_mostrar').innerHTML = "Incluir Movimiento Caja";

	ruta = "<?php echo site_url('Combo_ctrl/ajax_anid_tipoOperacion')?>";
	cbo_anid_tipoOperacion(ruta,0);   //Llenar combo    

	ruta = "<?php echo site_url('Combo_ctrl/ajax_anid_tipoMovimiento')?>";
	cbo_anid_tipoMovimiento(ruta,0,0);   //Llenar combo    

	//ruta = "<?php echo site_url('Combo_ctrl/ajax_combo_tipoDinero')?>";
	//cbo_tipoDinero(ruta,0,0);   //Llenar combo  

    document.getElementById('accion_formulario').value = "1";

    $('#mod_modal').modal('show');

}



function cbo_sel_tipoMovimiento(){
	idTipoOperacion = document.getElementById('id_cbo_tipoOperacion').value;
    
	ruta = "<?php echo site_url('Combo_ctrl/ajax_anid_tipoMovimiento')?>";
    cbo_anid_tipoMovimiento(ruta,0,idTipoOperacion);
}



function actualizar_form(){
    accion = document.getElementById('accion_formulario').value;
    if(accion==1){
        guardar_add_form();
    }else{
        guardar_mod_form();
    } 


}


function guardar_add_form(){

    formulario = "formulario";
    validar = validar_form(formulario,0);


    document.getElementById('guardar01').disabled=true;
	document.getElementById('cerrar01').disabled=true;


     if(validar==0){
        $.ajax({
            url : "<?=base_url();?>Cj_movimientoCaja/ajax_guardar_movimientoCaja",
            type: "POST",
            data: $('#formulario').serialize(),
            dataType: "JSON",
            success: function(data)
            {
                if(data.status==0){ 
                	document.getElementById('bodyTabla').innerHTML = data.registro;

					swal("Movimiento de Caja almacenado Correctamente!", "", {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					});
					$('#mod_modal').modal('hide');
                }    
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error Insertando / modificando registro, Comuniquese con el dministrador');
            }
        });         

	}else{
	    document.getElementById('guardar01').disabled=false;
		document.getElementById('cerrar01').disabled=false;
		swal("No se actualizo el registro", "Falta Información", {
				icon : "error",
				buttons: {        			
					confirm: {
						className : 'btn btn-danger'
					}
				},
		});
	}    
}


function modificar_form(id){

	document.getElementById('guardar01').disabled=false;
	document.getElementById('cerrar01').disabled=false;  

    $.ajax({
		url : "<?php echo site_url('Cj_movimientoCaja/ajax_modificar')?>/"+id,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){

             document.getElementById('m_monto').value = data.monto;	 
			 document.getElementById('m_observacion').value = data.observacion;
			 document.getElementById('id_mod').value = data.id;
			 document.getElementById('id_tm').value = data.id_tipo_movimiento;
 		     document.getElementById('lab_tipoOperacion').style.display="none";
		     document.getElementById('lab_tipoMovimiento').style.display="none";
		    // document.getElementById('lab_tipoDinero').style.display="none";
		     document.getElementById('lab_monto').style.display="none";
	  	     document.getElementById('titulo_mostrar').innerHTML = "Modificar Movimiento Caja";

			ruta = "<?php echo site_url('Combo_ctrl/ajax_anid_tipoOperacion')?>";
			cbo_anid_tipoOperacion(ruta,data.id_tipo_operacion);   //Llenar combo    

			ruta = "<?php echo site_url('Combo_ctrl/ajax_anid_tipoMovimiento')?>";
			cbo_anid_tipoMovimiento(ruta,data.id_tipo_movimiento,data.id_tipo_operacion);   //Llenar combo    
			//ruta = "<?php echo site_url('Combo_ctrl/ajax_combo_tipoDinero')?>";
			//cbo_tipoDinero(ruta,data.id_tipo_dinero);   //Llenar combo  
		    document.getElementById('accion_formulario').value = "2";

		    $('#mod_modal').modal('show'); 
		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});

}


function ver_form(id){

    $.ajax({
		url : "<?php echo site_url('Cj_movimientoCaja/ajax_ver')?>/"+id,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
             document.getElementById('v_monto').value = data.monto;	 
			 document.getElementById('v_observacion').value = data.observacion;
			 document.getElementById('v_tipoOperacion').value = data.tipo_operacion;
			 document.getElementById('v_tipoMovimiento').value = data.tipo_movimiento;
			 //document.getElementById('v_tipoDinero').value = data.tipo_dinero;

			 document.getElementById('v_motivo').value = data.motivo_reverso;
			 document.getElementById('v_fecha').value = data.fecha_reverso;
			 if(data.reverso==1){
                 document.getElementById('reverso').style.display="inline"
			 }




	  	     document.getElementById('titulo_mostrar_ver').innerHTML = "Visualizar Datos Del Movimiento Caja";
		    $('#ver_modal').modal('show'); 

		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});

}



function modApertura_form(id){
    $.ajax({
		url : "<?php echo site_url('Cj_movimientoCaja/ajax_ver')?>/"+id,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
             document.getElementById('aper_monto').value = data.monto;	 
			 document.getElementById('aper_observacion').value = data.observacion;
			 document.getElementById('a_tipoOperacion').value = data.tipo_operacion;
			 document.getElementById('a_tipoMovimiento').value = data.tipo_movimiento;
			 //document.getElementById('a_tipoDinero').value = data.tipo_dinero;
			 document.getElementById('id_mod_apertura').value =	data.id;
	  	     document.getElementById('r_titulo_mostrar_aper').innerHTML = "Modificar Datos De Apertura De La Caja";

			document.getElementById('guardar04').disabled=false;
			document.getElementById('cerrar04').disabled=false;	  	     
		    $('#modApertura_modal').modal('show'); 

		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});

}





function reverso_form(id){
	document.getElementById('guardar02').disabled=false;
	document.getElementById('cerrar02').disabled=false;

    $.ajax({
		url : "<?php echo site_url('Cj_movimientoCaja/ajax_ver')?>/"+id,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
             document.getElementById('r_monto').value = data.monto;	 
			 document.getElementById('r_observacion').value = data.observacion;
			 document.getElementById('r_tipoOperacion').value = data.tipo_operacion;
			 document.getElementById('r_tipoMovimiento').value = data.tipo_movimiento;
			 //document.getElementById('r_tipoDinero').value = data.tipo_dinero;
			 document.getElementById('r_motivo').value = "";

	  	     document.getElementById('r_titulo_mostrar').innerHTML = "Reversar Movimiento Caja";
			 document.getElementById('lab_motivo').style.display="none";
			 document.getElementById('id_mod_reverso').value=data.id;	

		    $('#reverso_modal').modal('show'); 

		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});

}


function guardar_mod_form(){

	//validar campos formulario
	formulario = "formulario";
	error=0;

	error = validar_form(formulario,2);
    if(error==0){
		document.getElementById('guardar01').disabled=true;
		document.getElementById('cerrar01').disabled=true;
    }	

	if(error == 0){
        $.ajax({
            url : "<?=base_url();?>Cj_movimientoCaja/ajax_guardar_mod",
            type: "POST",
            data: $('#formulario').serialize(),
            dataType: "JSON",
            success: function(data)
            {
            	if(data.status==0){ 
					 document.getElementById('bodyTabla').innerHTML = data.registro;
					 $('#mod_modal').modal('hide');

					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
				 	});            	


					swal("Registro almacenado Correctamente!", "", {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					});
				}	
	        },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error Insertando / modificanco registro, Comuniquese con el administrador');
            }
        });
    }else{
		swal("No se actualizo el registro", "", {
			icon : "error",
			buttons: {        			
				confirm: {
					className : 'btn btn-danger'
				}
			},
		});
    }    
}




function guardar_modApertura_form(){

	//validar campos formulario
	formulario = "aper_formulario";
	error=0;

	error = validarApertura_form(formulario,2);

    if(error==0){
		document.getElementById('guardar04').disabled=true;
		document.getElementById('cerrar04').disabled=true;
    }	

	if(error == 0){
        $.ajax({
            url : "<?=base_url();?>Cj_movimientoCaja/ajax_guardarApertura_mod",
            type: "POST",
            data: $('#aper_formulario').serialize(),
            dataType: "JSON",
            success: function(data)
            {
            	if(data.status==0){ 
					 document.getElementById('bodyTabla').innerHTML = data.registro;
					 $('#modApertura_modal').modal('hide');

					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
				 	});            	


					swal("Registro almacenado Correctamente!", "", {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					});
				}	
	        },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error Insertando / modificanco registro, Comuniquese con el administrador');
            }
        });
    }else{
		swal("No se actualizo el registro", "", {
			icon : "error",
			buttons: {        			
				confirm: {
					className : 'btn btn-danger'
				}
			},
		});
    }    
}





function reversar_form(){

	//validar campos formulario
	formulario = "r_formulario";
	error=0;

	error = validar_reverso_form(formulario,2);
    if(error==0){
		document.getElementById('guardar02').disabled=true;
		document.getElementById('cerrar02').disabled=true;
    }	

	if(error == 0){
        id = document.getElementById('id_mod_reverso').value 


        $.ajax({
            url : "<?=base_url();?>Cj_movimientoCaja/ajax_guardar_reverso",
            type: "POST",
            data: $('#r_formulario').serialize(),
            dataType: "JSON",
            success: function(data)
            {
            	if(data.status==0){ 
					 document.getElementById('bodyTabla').innerHTML = data.registro;
					 $('#reverso_modal').modal('hide');

					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
				 	});            	


					swal("Movimiento Reversado Correctamente!", "", {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					});
				}	
	        },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error Insertando / modificanco registro, Comuniquese con el administrador');
            }
        });
    }else{
    	document.getElementById('guardar02').disabled=false;
		document.getElementById('cerrar02').disabled=false;
		swal("No se actualizo el registro", "", {
			icon : "error",
			buttons: {        			
				confirm: {
					className : 'btn btn-danger'
				}
			},
		});
    }    
}







function validar_form(formulario,accion){
//accion 1= incluir
//accion 2= modificar

error=0;

objeto = "id_cbo_tipoOperacion";
campo = document.forms[formulario].elements[objeto].value;
document.getElementById('lab_tipoOperacion').style.display = "none";
if (campo==0){
	document.getElementById('lab_tipoOperacion').style.display = "inline";
   	document.getElementById('lab_tipoOperacion').innerHTML = "Seleccione el Tipo de Operación"; 	
   	error = 1;
}

objeto = "id_cbo_tipoMovimiento";
campo = document.forms[formulario].elements[objeto].value;
document.getElementById('lab_tipoMovimiento').style.display = "none";
if (campo==0){
	document.getElementById('lab_tipoMovimiento').style.display = "inline";
   	document.getElementById('lab_tipoMovimiento').innerHTML = "Seleccione el Tipo de Movimiento"; 	
   	error = 1;
}

/*
objeto = "id_cbo_tipoDinero";
campo = document.forms[formulario].elements[objeto].value;
document.getElementById('lab_tipoDinero').style.display = "none";
if (campo==0){
	document.getElementById('lab_tipoDinero').style.display = "inline";
   	document.getElementById('lab_tipoDinero').innerHTML = "Seleccione el Tipo de Dinero"; 	
   	error = 1;
}
*/
objeto = "m_monto";
campo = document.forms[formulario].elements[objeto].value;
campo = campo.replace(/(^\s*)|(\s*$)/g,"");
document.getElementById('lab_monto').style.display = "none";

if (campo=="0,00" || campo.length==0){	
    id_tm= document.forms[formulario].elements['id_tm'].value;
    if(id_tm!="1"){
		document.getElementById('lab_monto').style.display = "inline";
	    document.getElementById('lab_monto').innerHTML = "Ingrese el Monto del Movimeinto"; 	
	    document.getElementById('m_monto').value = "0,00";
	    error = 1;
	}else{
       if(campo.length==0){
       	  document.getElementById('m_monto').value = "0,00";
       }

	}    
    
}
return error;
}


function validarApertura_form(formulario,accion){
//accion 1= incluir
//accion 2= modificar

error=0;



objeto = "aper_monto";
campo = document.forms[formulario].elements[objeto].value;
campo = campo.replace(/(^\s*)|(\s*$)/g,"");
document.getElementById('lab_monto').style.display = "none";

if (campo=="0,00" || campo.length==0){	
    id_tm= document.forms[formulario].elements['id_tm'].value;
    if(id_tm!="1"){
		document.getElementById('lab_monto').style.display = "inline";
	    document.getElementById('lab_monto').innerHTML = "Ingrese el Monto de la Apertura"; 	
	    document.getElementById('m_monto').value = "0,00";
	    error = 1;
	}else{
       if(campo.length==0){
       	  document.getElementById('m_monto').value = "0,00";
       }

	}    
    
}

return error;
}



function validar_reverso_form(formulario,accion){
//accion 1= incluir
//accion 2= modificar

error=0;

objeto = "r_motivo";
campo = document.forms[formulario].elements[objeto].value;
campo = campo.replace(/(^\s*)|(\s*$)/g,"");
document.getElementById('lab_motivo').style.display = "none";

if (campo.length==0){	
	document.getElementById('lab_motivo').style.display = "inline";
	    document.getElementById('lab_motivo').innerHTML = "Ingrese el Motivo del reverso"; 	
	    error = 1;
}

return error;
}


</script>
