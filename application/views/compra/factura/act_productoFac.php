<?php
$incluir = base_url() . "assets/images/nuevo.png";
$asterisco = ' style="color=red; font-size:16px"';
$ci = &get_instance();
$ci->load->library('Clssession');
$accion= $ci->clssession->accion(7,2); 

?>
        <br>            
        <div class="main-panel">
            <div class="content">
                <div class="page-inner mt--5">
                    <div class="row row-card-no-pd mt--2">
                        <div class="col-sm-12 col-md-12">
                            <div class="card card-stats card-round">
                                <div class="card-body ">
									 <h2><span style="color:blue">Actualizar Productos </span></h2>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row">
						<div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
									<div class="row">
                                        <div class="col-md-1">
   										<a href="<?=base_url('actFactura');?>" class="btn btn-primary btn-sm item_edit2" 
											style="border-radius: 15px; font-size:16px; font-weight:500;
											height:45px;">Regresar
										</a>
										</div>
										<div class="col-md-10"></div>
										<div class="col-md-1">
											<a href="javascript:void(0)" onclick="javascript:add_productoCp()">
											   <img src="<?=$incluir;?>" style="width:35px; height:30px" alt="Incluir producto"
											   title="Incluir producto">
											</a>
										</div>
									</div>
									<hr>
                                    <div class="table-responsive">
										<div id="bodyTablaProducto">
										</div>
						            </div>
                                </div>
                            </div>
                        </div>    
                    </div>    
                </div>
            </div>
         </div>   

         <input type="hidden" id="id_factura" name="id_factura">

    




<!-- Modal Incluir producto-->
<div class="modal fade" id="mod_productoFactura" tabindex="-1" role="dialog" 
      aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width:800px; margin-left:-100px">
            <div class="modal-header bg-primary text-white">
              <h4 class="modal-title" id="exampleModalLabel" style="color:#fff; font-weight: 600">
                <div style="font-weight:700" id="titulo"></div>
              </h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true" style="color:white; font-size:30px">&times;</span>
              </button>
            </div>

            <div class="modal-body">
                
            <form name="formularioProducto" method="post" id="formularioProducto" autocomplete="off">   
                
              <div class="container">
                <div class="row" style="padding-top: 10px">
                    <div class="col-md-3">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
                             <span style="color:#0000ff; font-size:16px; font-weight:600">
                                 Producto <span style="color:#ff0000;">*</span></span>
                             </label>
                    </div>
                   
                    <div class="col-md-9">
                        <div id="cbo_productoCp"></div>

                        <div id="lab_producto" style="color:red; margin-left:9px; display:none"></div>       
                    </div>
                </div>  

                <div class="row" style="padding-top: 10px">
                    <div class="col-md-3">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
                             <span style="color:#0000ff; font-size:16px; font-weight:600">
                                 Cantidad <span style="color:#ff0000;">*</span></span>
                             </label>
                    </div>
                   
                    <div class="col-md-4">
                    <input type="text" name="cantidad" id="cantidad" 
                           class="form-control" value="0,00" style="text-align:right;" > 
                    <script type="text/javascript">
                    $("#cantidad").maskMoney({symbol:'R$ ', thousands:'.', decimal:',', symbolStay: true}); 
                    </script>
                        <div id="lab_cantidad" style="color:red; margin-left:9px; display:none"></div>       
                    </div>
                    
                    <div class="col-md-5">
						          <div id="disponible" style="color:blue; font-size:16px; font-weight:600"></div>
                    </div>  
                    
                    
                </div>  

                <div class="row" style="padding-top: 10px">
                    <div class="col-md-3">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
                             <span style="color:#0000ff; font-size:16px; font-weight:600">
                                 Costo Unitario <span style="color:#ff0000;">*</span></span>
                             </label>
                    </div>
                   
                    <div class="col-md-4">
                    <input type="text" name="montoF" id="montoF" 
                           class="form-control" value="0,00" style="text-align:right;" > 
                    <script type="text/javascript">
                    $("#montoF").maskMoney({symbol:'R$ ', thousands:'.', decimal:',', symbolStay: true}); 
                    </script>
                        <div id="lab_montoF" style="color:red; margin-left:9px; display:none"></div>       
                    </div>
                </div>  


                <br>  
                <span style="color:#ff0000; font-size:16px; text-align:left">Campo obligatorio (*)</span>
              </div>                
            </div>
            <div class="modal-footer">

                   
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrar02"><span style="font-weight:700">Cerrar</span></button>


                <button type="button" class="btn btn-primary" 
                        onclick="actualizar_productoCp();" id="guardar02">
                      <span style="font-weight:700">Guardar</span>
                </button>
            </div>
            <input type="hidden" id="accion" name="accion">
            
            
            <input type="hidden" id="id_mod" name="id_mod">
            <input type="hidden" id="idProductoOrig" name="idProductoOrig">
            <input type="hidden" id="cantidad_original" name="cantidad_original">
            
            
            
            </form>  

        </div>
    </div>
</div>




