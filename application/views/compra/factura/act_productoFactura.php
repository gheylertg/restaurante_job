<?php
$incluir = base_url() . "assets/images/nuevo.png";
$asterisco = ' style="color=red; font-size:16px"';
$ci = &get_instance();
$ci->load->library('Clssession');
$accion= $ci->clssession->accion(7,2); 

?>


        <br>            
        <div class="main-panel">
            <div class="content">
                <div class="page-inner mt--5">
                    <div class="row row-card-no-pd mt--2">
                        <div class="col-sm-12 col-md-12">
                            <div class="card card-stats card-round">
                                <div class="card-body ">
									 <h2><span style="color:blue">Actualizar Ingredientes / Productos por Entrada </span></h2>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
		                <div class="col-md-2">
		                    <div class="card">
		                        <div class="card-header" style="text-align:center; height:65px; padding-top: 10px">
										<a href="<?=base_url('entradaInventario');?>" class="btn btn-primary btn-sm item_edit2" 
											style="border-radius: 15px; font-size:16px; font-weight:500;
											height:45px;">Regresar
										</a>
								</div>
							</div>
						</div>				
						<div class="col-md-8">
		                    <div class="card">
		                        <div class="card-header" style="text-align:center; height:65px; padding-top: 10px">
								</div>
							</div>							
						</div>	
						<div class="col-md-2">
							<div class="card">
		                        <div class="card-header" style="text-align:center; height:65px; padding-top: 10px">
									<a href="javascript:void(0)" onclick="javascript:add_productoEnt()">
									   <img src="<?=$incluir;?>" style="width:35px; height:30px" alt="Incluir">
									</a>
								</div>
							</div>	
		                </div>                     
		            </div>  <!-- fin del row-->


                    <div class="row">
						<div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="table-responsive">
										<div id="bodyTablaProducto">
										</div>
						            </div>
                                </div>
                            </div>
                        </div>    
                    </div>    
                </div>
            </div>
         </div>   

         <input type="hidden" id="id_inventario" name="id_inventario">

    




<!-- Modal Incluir producto-->
<div class="modal fade" id="mod_productoEnt" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width:800px; margin-left:-100px">
            <div class="modal-header bg-primary text-white">
              <h4 class="modal-title" id="exampleModalLabel" style="color:#fff; font-weight: 600">
                <div style="font-weight:700">Ingresar producto</div>
              </h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true" style="color:white; font-size:30px">&times;</span>
              </button>
            </div>

            <div class="modal-body">
                
            <form name="formularioProducto" method="post" id="formularioProducto" autocomplete="off">   
                
                
              <div class="container">
                <div class="row" style="padding-top: 10px">
                    <div class="col-md-3">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
                             <span style="color:#0000ff; font-size:16px; font-weight:600">
                                 Producto <span style="color:#ff0000;">*</span></span>
                             </label>
                    </div>
                   
                    <div class="col-md-9">
                        <div id="cbo_productoIn"></div>

                        <div id="lab_producto" style="color:red; margin-left:9px; display:none"></div>       
                    </div>
                </div>  

                <div class="row" style="padding-top: 10px">
                    <div class="col-md-3">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
                             <span style="color:#0000ff; font-size:16px; font-weight:600">
                                 Cantidad <span style="color:#ff0000;">*</span></span>
                             </label>
                    </div>
                   
                    <div class="col-md-4">

                    <input type="text" name="cantidad" id="cantidad" 
                           class="form-control" value="0,00" style="text-align:right;" > 

                    <script type="text/javascript">
                    $("#cantidad").maskMoney({symbol:'R$ ', thousands:'.', decimal:',', symbolStay: true}); 
                    </script>

                        <div id="lab_cantidad" style="color:red; margin-left:9px; display:none"></div>       
                    </div>
                </div>  

                <div class="row" style="padding-top: 10px">
                    <div class="col-md-3">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
                             <span style="color:#0000ff; font-size:16px; font-weight:600">
                                 Precio Unitario <span style="color:#ff0000;">*</span></span>
                             </label>
                    </div>
                   
                    <div class="col-md-4">
                        <input type="text" name="precio" id="precio" 
                               class="form-control" value="0,00" style="text-align:right;">  
                        <script type="text/javascript">
                        $("#precio").maskMoney({symbol:'R$ ', thousands:'.', decimal:',', symbolStay: true}); 
                        </script>
                    </div>
                </div>  



                <br>  
                <span style="color:#ff0000; font-size:16px; text-align:left">Campo obligatorio (*)</span>
              </div>                
            </div>
            <div class="modal-footer">

                   
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrar02"><span style="font-weight:700">Cerrar</span></button>


                <button type="button" class="btn btn-primary" 
                        onclick="guardar_add_productoEnt();" id="guardar02">
                      <span style="font-weight:700">Guardar</span>
                </button>




            </div>



            </form>  

        </div>
    </div>
</div>




<!-- Modal Incluir producto-->
<div class="modal fade" id="act_mod_productoModEnt" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width:800px; margin-left:-100px">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel" style="color:blue">
                    <div>Modificar producto</div>
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                
            <form name="formularioProductoMod" method="post" id="formularioProductoMod" autocomplete="off">   
                
                
              <div class="container">
                <div class="row" style="padding-top: 10px">
                    <div class="col-md-12">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
                             <span style="color:#0000ff; font-size:16px; font-weight:600">
                                 <span>Producto: <div id="idenProducto"></div></span>
                             </label>
                    </div>
                   
                </div>  

                <div class="row" style="padding-top: 10px">
                    <div class="col-md-3">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
                             <span style="color:#0000ff; font-size:16px; font-weight:600">
                                 Cantidad <span style="color:#ff0000;">*</span></span>
                             </label>
                    </div>
                   
                    <div class="col-md-4">

                    <input type="text" name="cantidadMod" id="cantidadMod" 
                           class="form-control" value="0,00" style="text-align:right;" > 

                    <script type="text/javascript">
                    $("#cantidadMod").maskMoney({symbol:'R$ ', thousands:'.', decimal:',', symbolStay: true}); 
                    </script>

                        <div id="lab_cantidadMod" style="color:red; margin-left:9px; display:none"></div>       
                    </div>
                </div>  

                <div class="row" style="padding-top: 10px">
                    <div class="col-md-3">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
                             <span style="color:#0000ff; font-size:16px; font-weight:600">
                                 Precio Unitario <span style="color:#ff0000;">*</span></span>
                             </label>
                    </div>
                   
                    <div class="col-md-4">
                        <input type="text" name="precioMod" id="precioMod" 
                               class="form-control" value="0,00" style="text-align:right;">  
                        <script type="text/javascript">
                        $("#precioMod").maskMoney({symbol:'R$ ', thousands:'.', decimal:',', symbolStay: true}); 
                        </script>
                    </div>
                </div>  



                <br>  
                <span style="color:#ff0000; font-size:16px; text-align:left">Campo obligatorio (*)</span>
              </div>                
            </div>
            <div class="modal-footer">

                   
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrar03">Cerrar</button>


                <button type="button" class="btn btn-primary" 
                        onclick="guardar_mod_productoEnt();" id="guardar03">
                      Guardar
                </button>




            </div>

                <input type="hidden" id="id_mod" name="id_mod">
                <input type="hidden" id="id_inventarioMod" name="id_inventarioMod">

            </form>  

        </div>
    </div>
</div>
