

<!--cargar datatable-->
<script>
    $(document).ready(function(){
         <?php
         if($modulo==1){  
         ?>	
			 cargarDataTable(0);
		 <?php
		 }
		 ?>

		 <?php
         if($modulo==2){  
         ?>	
			 addFactura();
		 <?php
		 }
		 ?>

		 <?php
         if($modulo==3){  
         ?>	
			 cargar_dataTableProducto(<?=$idFactura;?>);
		 <?php
		 }
		 ?>

		 <?php
         if($modulo==4){  
         ?>	
			 verFactura(<?=$idFactura;?>);
		 <?php
		 }
		 ?>



    });
    
// construye el datatable principal para actualizar la ordenes
function cargarDataTable(id){
    $.ajax({
		url : "<?php echo site_url('Cp_factura/ajax_datatable')?>/"+id,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			 document.getElementById('bodyTabla').innerHTML = data.registro;
			 $(document).ready(function() {
				$('#basic-datatables').DataTable({
                    "order": [[ 0, "desc" ]]
			    });	
			 });

		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}


// llama el controllador que ingresa la información de la orden
function incFactura(){
	document.location.href = "<?php echo base_url('incFactura')?>";	
}


function addFactura(){
   	document.getElementById('guardar01').disabled=false;
	document.getElementById('cerrar01').disabled=false; 
	ruta = "<?php echo site_url('Combo_ctrl/ajax_combo_proveedorCp')?>";
	cbo_proveedorCp(ruta,0);   //Llenar combo   

	//carga detalle producto
    $.ajax({
		url : "<?php echo site_url('Cp_factura/ajax_datatable_factura')?>/",
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			 document.getElementById('tablaProducto').innerHTML = data.registro;
			 $(document).ready(function() {
			 });


		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}

function verFactura(){

	//carga detalle producto
    $.ajax({
		url : "<?php echo site_url('Cp_factura/ajax_datatable_verFactura')?>/",
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			 document.getElementById('tablaProducto').innerHTML = data.registro;
			 document.getElementById('m_fecha').value = data.fecha;
			 document.getElementById('m_proveedor').value = data.proveedor;
			 document.getElementById('m_nroFactura').value = data.factura;
			 document.getElementById('m_monto').value = data.monto;


			 $(document).ready(function() {
			 });


		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}





function add_producto(){
	ruta = "<?php echo site_url('Combo_ctrl/ajax_combo_productoCp')?>";
	cbo_productoCp(ruta,0); 
    document.getElementById('cantidad').value = "0,00";
    document.getElementById('montoF').value = "0,00";

    document.getElementById('lab_producto').style.display = "none";
    document.getElementById('lab_cantidad').style.display = "none";
    document.getElementById('lab_montoF').style.display = "none";

    $('#mod_producto').modal('show');
}


function guardar_add_producto(){
$validar = valProducto(1);
if($validar==0){
	$.ajax({
		url : "<?=base_url();?>Cp_factura/ajax_guardar_productoCp",
		type: "POST",
		data: $('#formularioProducto').serialize(),
		dataType: "JSON",
		success: function(data)
		{
			if(data.status==0){ 
				document.getElementById('tablaProducto').innerHTML = data.registro;
				document.getElementById('cantidad').value = "0,00";
				document.getElementById('montoF').value = "0,00";
				document.getElementById('nroProducto').value = data.nroProducto;

				$('#mod_producto').modal('hide');
			}else{
				swal("No se actualizo el registro", "Ya el Producto esta agregado", {
					icon : "error",
					buttons: {        			
						confirm: {
							className : 'btn btn-danger'
						}
					},
				});

			}	 
		},
		error: function (jqXHR, textStatus, errorThrown)
		{
			alert('Error Insertando / modificanco registro, Comuniquese con el administrador');
		}
	});
}else{
	//document.getElementById('cantidad').value = "0,00";
	swal("No se actualizo el registro", "Falta Información", {
		icon : "error",
		buttons: {        			
			confirm: {
				className : 'btn btn-danger'
			}
		},
	});
}    

}

////////////////////////////////////////////////////////////////////////////////////////////



function eliminar_factura(id){
	swal({
		title: 'Esta seguro de eliminar la Factura de Compra?',
			type: 'warning',
			buttons:{
				cancel: {
					visible: true,
						text : 'No, Cancelar!',
								className: 'btn btn-danger'
				},        			
				confirm: {
					text : 'Si, Eliminar!',
					className : 'btn btn-success'
				}
			}
	}).then((willDelete) => {
		if (willDelete) {
		    $.ajax({
				url : "<?php echo site_url('Cp_factura/ajax_eliminar_factura')?>/"+id,
				type: "GET",                     
				dataType: "JSON",
				success: function(data){
					 document.getElementById('bodyTabla').innerHTML = data.registro;
					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
					 }); 
				},
				error: function (jqXHR, textStatus, errorThrown){
					alert('Error consultando la Base de Datos');
				}
			});
			swal("La Factura de Compra fué eliminada!", {
				icon: "success",
				buttons : {
					confirm : {
						className: 'btn btn-success'
					}
				}
			});
		}
	});

}


function modificar_factura(id){
	document.getElementById('guardar02').disabled=false;
	document.getElementById('cerrar02').disabled=false;  
    $.ajax({
		url : "<?php echo site_url('Cp_factura/ajax_act_modificar_factura')?>/"+id,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){

             document.getElementById('m_nroFactura').value = data.factura;	 
			 document.getElementById('m_fecha').value = data.fecha;
			 document.getElementById('m_monto').value = data.monto;
			 document.getElementById('id_mod').value = data.id;
			 document.getElementById('lab_fecha').style.display = "none";
			 document.getElementById('lab_monto').style.display = "none";			 
			 document.getElementById('lab_proveedorCp').style.display = "none";
			 document.getElementById('lab_nroFactura').style.display = "none";

			ruta = "<?php echo site_url('Combo_ctrl/ajax_combo_proveedorCp')?>";
			cbo_proveedorCp(ruta,data.id_proveedor);   //Llenar combo   
            $('#act_mod_factura').modal('show');  
            

		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});

}

function eliminar_producto(id){
	swal({
		title: 'Esta seguro de eliminar el Producto?',
			type: 'warning',
			buttons:{
				cancel: {
					visible: true,
						text : 'No, Cancelar!',
								className: 'btn btn-danger'
				},        			
				confirm: {
					text : 'Si, Eliminar!',
					className : 'btn btn-success'
				}
			}
	}).then((willDelete) => {
		if (willDelete) {
		    $.ajax({
				url : "<?php echo site_url('Cp_factura/ajax_eliminar_producto')?>/"+id,
				type: "GET",                     
				dataType: "JSON",
				success: function(data){
                    document.getElementById('tablaProducto').innerHTML = data.registro;
                    document.getElementById('cantidad').value = "0,00";
                    document.getElementById('nroProducto').value = data.nroProducto;

					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
					 }); 
				},
				error: function (jqXHR, textStatus, errorThrown){
					alert('Error consultando la Base de Datos');
				}
			});
			swal("El Producto fué eliminado!", {
				icon: "success",
				buttons : {
					confirm : {
						className: 'btn btn-success'
					}
				}
			});
		}
	});
}


function valProducto(accion){

formulario = "formularioProducto";
error=0;

if(accion != 3){
	objeto = "id_cbo_productoCp";
	campo = document.forms[formulario].elements[objeto].value;
	document.getElementById('lab_producto').style.display = "none";
	if (campo==0){
		document.getElementById('lab_producto').style.display = "inline";
	   	document.getElementById('lab_producto').innerHTML = "Seleccione el Producto"; 	
	   	error = 1;
	}
}

objeto = "cantidad";
campo = document.forms[formulario].elements[objeto].value;
campo = campo.replace(/(^\s*)|(\s*$)/g,"");
document.getElementById('lab_cantidad').style.display = "none";

if (campo=="0,00" || campo.length==0){	
	document.getElementById('lab_cantidad').style.display = "inline";
    document.getElementById('lab_cantidad').innerHTML = "Ingrese la cantidad de Producto"; 	
    document.getElementById('cantidad').value = "0,00";
    error = 1;
}

objeto = "montoF";
campo = document.forms[formulario].elements[objeto].value;
campo = campo.replace(/(^\s*)|(\s*$)/g,"");

document.getElementById('lab_montoF').style.display = "none";


if (campo=="0,00" || campo.length==0){	
	document.getElementById('lab_montoF').style.display = "inline";
    document.getElementById('lab_montoF').innerHTML = "Ingrese El Costo Unitario"; 	
    document.getElementById('montoF').value = "0,00";
    error = 1;
}



return error;
}


//GUARDAR ENTRADA

function guardar_form(){
    formulario = "formulario";
    $validar = validar_form(formulario,0);
    document.getElementById('guardar01').disabled=true;
	document.getElementById('cerrar01').disabled=true;

     if($validar==0){
        $.ajax({
            url : "<?=base_url();?>Cp_factura/ajax_guardar_facturaCp",
            type: "POST",
            data: $('#formulario').serialize(),
            dataType: "JSON",
            success: function(data)
            {
                if(data.status==0){ 

					swal("Factura de Compra almacenada Correctamente!", "", {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					});


                    
					document.location.href = "<?php echo base_url('actFactura')?>";	


	    			/*ruta = 	document.getElementById('ruta_guardar_producto').value;
	    			var Form = $('#formulario');
	    			Form.attr("action", ruta);
	    			Form.submit();                	*/
                }    
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error Insertando / modificanco registro, Comuniquese con el dministrador');
            }
        });         

	}else{
	    document.getElementById('guardar01').disabled=false;
		document.getElementById('cerrar01').disabled=false;



		swal("No se actualizo el registro", "Falta Información", {
				icon : "error",
				buttons: {        			
					confirm: {
						className : 'btn btn-danger'
					}
				},
		});
	}    

}


function guardar_mod_factura(){
    formulario = "formulario";
    $validar = validar_form(formulario,1);
    document.getElementById('guardar02').disabled=true;
	document.getElementById('cerrar02').disabled=true;

	if($validar == 0){
        $.ajax({
            url : "<?=base_url();?>Cp_factura/ajax_guardar_mod_factura",
            type: "POST",
            data: $('#formulario').serialize(),
            dataType: "JSON",
            success: function(data)
            {
            	if(data.status==0){ 

					 document.getElementById('bodyTabla').innerHTML = data.registro;
					 $('#act_mod_factura').modal('hide');

					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
				 	});            	


					swal("Registro almacenado Correctamente!", "", {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					});
				}	
	        },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error Insertando / modificanco registro, Comuniquese con el administrador');
            }
        });
    }else{
		swal("No se actualizo el registro", "", {
			icon : "error",
			buttons: {        			
				confirm: {
					className : 'btn btn-danger'
				}
			},
		});
		document.getElementById('guardar02').disabled=false;
		document.getElementById('cerrar02').disabled=false  ;		
    }    
}

function ver_form(id){
	document.location.href = "<?php echo base_url('verFactura')?>/"+id;
}



function validar_form(formulario,accion){
//accion 1= incluir
//accion 2= modificar
error=0;

//fecha 
var date = new Date($('#m_fecha').val());
day = date.getDate();
month = date.getMonth() + 1;
year = date.getFullYear();
fecha = [year, month, day].join('/');

document.getElementById('lab_fecha').style.display = "none";
if(isNaN(day)){
    error=1;
	document.getElementById('lab_fecha').style.display = "inline";
   	document.getElementById('lab_fecha').innerHTML="Ingrese la Fecha"; 	
}else{   
	document.getElementById('lab_fecha').style.display = "none";	
	document.getElementById('lab_fecha').innerHTML=""; 	
	document.getElementById('fechaProducto').value=fecha;
}


objeto = "id_cbo_proveedorCp";
campo = document.forms[formulario].elements[objeto].value;
document.getElementById('lab_proveedorCp').style.display = "none";
if (campo==0){
	document.getElementById('lab_proveedorCp').style.display = "inline";
   	document.getElementById('lab_proveedorCp').innerHTML = "Seleccione el Proveedor"; 	
   	error = 1;
}


objeto = "m_nroFactura";
campo = document.forms[formulario].elements[objeto].value;
campo = campo.replace(/(^\s*)|(\s*$)/g,"");
document.getElementById('lab_nroFactura').style.display = "none";

if (campo.length==0){	
	document.getElementById('lab_nroFactura').style.display = "inline";
    document.getElementById('lab_nroFactura').innerHTML = "Ingrese el Monto de la Factura"; 	
    error = 1;
}




objeto = "m_monto";
campo = document.forms[formulario].elements[objeto].value;
campo = campo.replace(/(^\s*)|(\s*$)/g,"");
document.getElementById('lab_monto').style.display = "none";

if (campo=="0,00" || campo.length==0){	
	document.getElementById('lab_monto').style.display = "inline";
    document.getElementById('lab_monto').innerHTML = "Ingrese el Monto de la Factura"; 	
    document.getElementById('m_monto').value = "0,00";
    error = 1;
}

if(accion==0){
	objeto = "nroProducto";
	campo = document.forms[formulario].elements[objeto].value;
	document.getElementById('lab_detalleCp').style.display = "none";
	if (campo==0){
		document.getElementById('lab_detalleCp').style.display = "inline";
	   	document.getElementById('lab_detalleCp').innerHTML = "Ingrese los Productos de la entrada"; 	
	   	error = 1;
	}
}	

return error;
}


function cerrar_form(){
    document.location.href = "<?php echo base_url('actFactura')?>"; 

	/*ruta = 	document.getElementById('ruta_guardar_producto').value;
	var Form = $('#formulario');
	Form.attr("action", ruta);
	Form.submit();  
	*/
	
}

function mod_producto(id){
	document.location.href = "<?php echo base_url('act_mpf')?>/"+id;
/*	
	ruta = "<?php //echo site_url('cp_orden/act_modificar_producto')?>/"+id
	var Form = $('#formulario');
	Form.attr("action", ruta);
	Form.submit(); 
*/	
}



function cargar_dataTableProducto(id){
    $.ajax({
		url : "<?php echo site_url('Cp_factura/ajax_datatableFactura')?>/"+id,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			 document.getElementById('bodyTablaProducto').innerHTML = data.registro;
			 document.getElementById('id_factura').value=id;
			 $(document).ready(function() {
				$('#basic-datatables').DataTable({
			    });	
			 });

		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}


function add_productoCp(){
	ruta = "<?php echo site_url('Combo_ctrl/ajax_combo_productoCp')?>";
	cbo_productoCp(ruta,0);

	document.getElementById('titulo').innerHTML = "Incluir Produto de la Factura"; 
    document.getElementById('cantidad').value = "0,00";
    document.getElementById('montoF').value = "0,00";
    document.getElementById('lab_producto').style.display = "none";
    document.getElementById('lab_cantidad').style.display = "none";
    document.getElementById('lab_montoF').style.display = "none";
	document.getElementById('accion').value = "1";	    
    $('#mod_productoFactura').modal('show');
}


function actualizar_productoCp(){
if(document.getElementById('accion').value == "1"){
    guardar_add_productoCp();	
}else{
	guardar_mod_productoFactura();
}	
	
}



function guardar_add_productoCp(){
	

	
	
    validar = valProducto(1);
    if(validar==0){
    	document.getElementById('cerrar02').style.value="none";
    	document.getElementById('guardar02').style.value="none";
    	id = document.getElementById('id_factura').value;
    	


        $.ajax({
            url : "<?php echo site_url('Cp_factura/ajax_guardar_add_productoCp')?>/"+id,
            type: "POST",
            data: $('#formularioProducto').serialize(),
            dataType: "JSON",
            success: function(data)
            {
		    	document.getElementById('cerrar02').style.value="inline";
				document.getElementById('guardar02').style.value="inline";
            	if(data.status==0){ 
                    document.getElementById('bodyTablaProducto').innerHTML = data.registro;
                    
					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
				 	});                      
                    
                    
					swal("Registro almacenado Correctamente!", "", {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					});
                    document.getElementById('cantidad').value = "0,00";
                    document.getElementById('montoF').value = "0,00";
					$('#mod_productoFactura').modal('hide');

				}else{
					swal("No se actualizo el registro", "Ya el Producto esta agregado", {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});

				}	 
	        },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error Insertando / modificanco registro, Comuniquese con el administrador');
            }
        });
    }else{
        //document.getElementById('cantidad').value = "0,00";
		swal("No se actualizo el registro", "Falta Información", {
			icon : "error",
			buttons: {        			
				confirm: {
					className : 'btn btn-danger'
				}
			},
		});
    }    

}


				
				
				
				

function elim_producto(id){
	idFactura = document.getElementById('id_factura').value;
	swal({
		title: 'Esta seguro de eliminar el Producto?',
			type: 'warning',
			buttons:{
				cancel: {
					visible: true,
						text : 'No, Cancelar!',
								className: 'btn btn-danger'
				},        			
				confirm: {
					text : 'Si, Eliminar!',
					className : 'btn btn-success'
				}
			}
	}).then((willDelete) => {
		if (willDelete) {
			$.ajax({
				url : "<?php echo site_url('Cp_factura/ajax_borrar_producto')?>",
				type: "GET",                     
				data:{'id':id, 'idFactura':idFactura},
				dataType: "JSON",
				success: function(data){
					if(data.status==0){
						document.getElementById('bodyTablaProducto').innerHTML = data.registro;
						$(document).ready(function() {
						   $('#basic-datatables').DataTable({
						   });	
					   });
						swal("El Producto fué eliminado!", {
							icon: "success",
							buttons : {
								confirm : {
									className: 'btn btn-success'
								}
							}
						});
  				   }else{
						swal("El producto en el inventario, No posee disponibilidad para retirarlo", "", {
							icon : "error",
							buttons: {        			
								confirm: {
									className : 'btn btn-danger'
								}
							},
						});
				   }	
				},
				error: function (jqXHR, textStatus, errorThrown){
					alert('Error consultando la Base de Datos');
				}
			});
		}
	});

	
}


function mod_productoFactura(id){

	
	
	document.getElementById('guardar02').disabled=false;
	document.getElementById('cerrar02').disabled=false;       
    $.ajax({
		url : "<?php echo site_url('Cp_factura/ajax_act_modificar_producto')?>/"+id,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			 document.getElementById('titulo').innerHTML = "Modificar Produto de la Factura" ;
			 document.getElementById('id_mod').value = data.id_mod;
			 document.getElementById('cantidad').value = data.cantidad;
			 document.getElementById('montoF').value = data.montoF;
			 document.getElementById('idProductoOrig').value = data.idProducto;	
			 document.getElementById('accion').value = "2";	
			 document.getElementById('cantidad_original').value = data.cantidad_original;	
			 
			 
			 		 
			 
			 document.getElementById('lab_producto').style.display = "none";
			 document.getElementById('lab_cantidad').style.display = "none";
			 document.getElementById('lab_montoF').style.display = "none";

			 producto = "<input type='text' disabled='true' value='" + data.producto + "' class='form-control'>";

			 document.getElementById('cbo_productoCp').innerHTML = producto;
			 
			 disponible = "Disponibilidad Actual (" + data.disponible + ")";
			 document.getElementById('disponible').innerHTML = disponible;
			 
			 
			 
			 

			 
			//ruta = "<?php //echo site_url('Combo_ctrl/ajax_combo_productoCp')?>";
			//cbo_productoCp(ruta,data.idProducto); 			
			 
            $('#mod_productoFactura').modal('show');  
		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});

}	

function guardar_mod_productoFactura(){
 	formulario = "formularioProducto";
    id = document.getElementById('id_mod').value;
	validar = valProducto(3);    
	if(validar == 0){
		document.getElementById('guardar02').disabled=true;
		document.getElementById('cerrar02').disabled=true;  
		id = document.getElementById('id_factura').value;
	         
        $.ajax({
            url : "<?=base_url();?>Cp_factura/guardar_mod_productoFactura/"+id,
            type: "POST",
            data: $('#formularioProducto').serialize(),
            dataType: "JSON",
            success: function(data)
            {
            	if(data.status==0){ 

					 document.getElementById('bodyTablaProducto').innerHTML = data.registro;
					 $('#mod_productoFactura').modal('hide');

					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
				 	});            	


					swal("Registro almacenado Correctamente!", "", {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					});
				}else{
					if(data.status==1){
						$('#mod_productoFactura').modal('hide');
						swal("No se actualizo el registro", "Ya el Producto esta agregado", {
							icon : "error",
							buttons: {        			
								confirm: {
									className : 'btn btn-danger'
								}
							},
						});					
					}else{
						$('#mod_productoFactura').modal('hide');
						swal("No se actualizo el registro", "Ya el Producto esta agregado", {
							icon : "error",
							buttons: {        			
								confirm: {
									className : 'btn btn-danger'
								}
							},
						});					
						
						
					}	
					
				}
					
	        },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error Insertando / modificanco registro, Comuniquese con el administrador');
            }
        });
    }else{
		swal("No se actualizo el registro", "", {
			icon : "error",
			buttons: {        			
				confirm: {
					className : 'btn btn-danger'
				}
			},
		});
    }    




}



</script>
