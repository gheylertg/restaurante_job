<?php
$incluir = base_url() . "assets/images/nuevo.png";
$asterisco = ' style="color=red; font-size:16px"';

$ruta = base_url() . "Cp_factura/index"; 


$fechaMaxima = date("Y-m-d");




?>

<br>            
<div class="main-panel" style="padding-top: 10px">
    <div class="content">
        <div class="page-inner mt--5">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="card card-stats card-round" >
                        <div class="card-body" style="padding-bottom: 5px">
							 <h2><span style="color:blue;">Visualizar Factura De Compra</span></h2>
                        </div>
                    </div>
                </div>
            </div>


            <form name="formulario" method="post" id="formulario" autocomplete="off" 
                  enctype="multipart/form-data">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                <div class="card card-stats card-round">
                <div class="card-body">




                <div class="row">
                    <div class="col-md-3">
                        <label>
                        <span style="color:#0000ff; font-size:16px; font-weight:600">
                            Fecha Factura <span style="color:#ff0000;">*</span>
                        </span>
                        </label>
                        
                        <input type="text" id="m_fecha" name="m_fecha" 
                               class="form-control" disabled="true"/>
                    </div>

                    <div class="col-md-3">
                        <label>
                        <span style="color:#0000ff; font-size:16px; font-weight:600">
                            Nro Factura <span style="color:#ff0000;">*</span>
                        </span>
                        </label>
                        
                        <input type="text" id="m_nroFactura" name="m_nroFactura" 
                               class="form-control" disabled="true"/>
                    </div>


                    <div class="col-md-3">
                        <label>
                        <span style="color:#0000ff; font-size:16px; font-weight:600">
                            Monto Factura <span style="color:#ff0000;">*</span>
                        </span>
                        </label>
                        
                        <input type="text" name="m_monto" id="m_monto"  style="text-align:right;"
                               class="form-control" disabled="true">

                    </div>
                </div>  



                <div class="row">   

                    <div class="col-md-9" style="padding-top: 5px">
                        <label>
                        <span style="color:#0000ff; font-size:16px; font-weight:600">
                            Proveedor
                        </span>
                        </label>
                        <input type="text" id="m_proveedor" name="m_proveedor" 
                               class="form-control" disabled="true"/>
                        
                    </div>
                </div> <!--fin row-->

            <hr>

            <div class="row">
                <div class="col-md-12">
                    <label>
                    <span style="color:#0000ff; font-size:20px; font-weight:600">
                        Productos de la Factura Compra<span style="color:#ff0000;">*</span>
                    </span>
                    </label>
                    <div id="lab_detalleCp" 
                         style="color:red; margin-left:9px; display:none">
                    </div>    
                    


                </div>  

            </div>
                    
            <div class="row">
                <div class="col-md-12">
                    <div id="tablaProducto"></div> 

                </div>
            </div>  





            <div class="row">
                <div class="col-md-12" style="text-align: right;">
                    <button type="button" class="btn btn-danger" onclick="cerrar_form()" 
                            id="cerrar01">Cerrar</button>

                </div>    
            </div> 
            
                </div>     
                </div> <!-- Fin card-body-->
                </div> <!-- card card-stats -->
            </div> <!-- col principal -->

            <input type="hidden" id="ruta_guardar_producto" id="ruta_guardar_producto" 
                   value="<?=$ruta;?>">

            </form>


        </div>
    </div>
</div>    
