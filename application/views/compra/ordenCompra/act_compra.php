<?php
$incluir = base_url() . "assets/images/nuevo.png";
$asterisco = ' style="color=red; font-size:16px"';
$ci = &get_instance();
$ci->load->library('Clssession');
$accion= $ci->clssession->accion(8,2);


?>

        <br>            
        <div class="main-panel">
            <div class="content">
                <div class="page-inner mt--5">
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <div class="card card-stats card-round" style="padding-top: 5px">
                                <div class="card-body">
									 <h1><span style="color:blue">Ordenes de Compra</span></h1>
                                </div>
                            </div>
                        </div>
                    </div>


                <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <?php
                                        if($accion==1){
                                    ?>
                                    <div  style="text-align:right">

                                    <a href="javascript:void(0)" onclick="javascript:incOrden()">
                                       <img src="<?=$incluir;?>" style="width:35px; height:35px" alt="Incluir">
                                    </a>
                                    </div>
                                    <?php
                                        }
                                    ?>

                                    <div class="table-responsive" style="padding-top: 10px">
										<div id="bodyTabla">
										</div>
                                    </div>
                                </div>
                            </div>
                        </div>    
                </div>
            </div>



<?php
$fechaMaxima = date("Y-m-d");


?>

<!-- Modal Incluir producto-->
<div class="modal fade" id="act_mod_orden" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width:1000px; margin-left:-200px">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel" style="color:blue">
                    <div>Actualizar Orden de Compra</div>
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                
                <form name="formulario" method="post" id="formulario" autocomplete="off" 
                      enctype="multipart/form-data">
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                    <div class="card card-stats card-round">
                    <div class="card-body">

                        <div class="row">
                            <div class="col-md-2">
                                <label>
                                <span style="color:#0000ff; font-size:16px; font-weight:600">
                                    Nro Orden
                                </span>
                                </label>
                                <input type="text" name="m_orden" id="m_orden" 
                                       class="form-control" disabled="true"
                                       maxlength="100" minlength="0">
                            </div>

                            <div class="col-md-3">
                                <label>
                                <span style="color:#0000ff; font-size:16px; font-weight:600">
                                    Fecha Orden <span style="color:#ff0000;">*</span>
                                </span>
                                </label>
                                
                                <input type="date" id="m_fecha" name="m_fecha" 
                                       max="<?=$fechaMaxima;?>" class="form-control"/>
                                <div id="lab_fecha" style="color:red; margin-left:9px; display:none">
                                </div>
                            </div>
                        </div>        
                        
                        <div class="row">

                            <div class="col-md-8">
                                <label>
                                <span style="color:#0000ff; font-size:16px; font-weight:600">
                                    Proveedor
                                </span>
                                </label>
                                <div id="cbo_proveedorCp"></div>
                                <div id="lab_proveedorCp" style="color:red; margin-left:9px; display:none">
                                </div>
                                
                            </div>
                        </div>    
                        <br>  
                        <span style="color:#ff0000; font-size:16px; text-align:left">Campo obligatorio (*)</span>
                    </div>                
                    </div>
                    </div>
                </div>  

                     <input type="hidden" id="id_mod" name="id_mod">  
                     <input type="hidden" id="fechaProducto" name="fechaProducto">  
    
                </form> 
             </div>    
    

            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrar02">Cerrar</button>


                <button type="button" class="btn btn-primary" 
                        onclick="guardar_mod_orden();" id="guardar02">
                          Guardar
                </button>

            </div>

                  
            
        </div>
    </div>
</div>


    





