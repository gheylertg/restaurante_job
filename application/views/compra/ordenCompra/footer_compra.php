


<!--cargar datatable-->
<script>
    $(document).ready(function(){
         <?php
         if($modulo==1){  
         ?>	
			 cargarDataTable(0);
		 <?php
		 }
		 ?>

		 <?php
         if($modulo==2){  
         ?>	
			 addOrden();
		 <?php
		 }
		 ?>

		 <?php
         if($modulo==3){  
         ?>	
			 cargar_dataTableProducto(<?=$idOrden;?>);
		 <?php
		 }
		 ?>

		 <?php
         if($modulo==4){  
         ?>	
			 verOrden(<?=$idOrden;?>);
		 <?php
		 }
		 ?>



    });
    
// construye el datatable principal para actualizar la ordenes
function cargarDataTable(id){
    $.ajax({
		url : "<?php echo site_url('Cp_orden/ajax_datatable')?>/"+id,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			 document.getElementById('bodyTabla').innerHTML = data.registro;
			 $(document).ready(function() {
				$('#basic-datatables').DataTable({
                    "order": [[ 0, "desc" ]]
			    });	
			 });

		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}


// llama el controllador que ingresa la información de la orden
function incOrden(){
	document.location.href = "<?php echo base_url('incOrden')?>";	
}


function addOrden(){
   	document.getElementById('guardar01').disabled=false;
	document.getElementById('cerrar01').disabled=false; 
	ruta = "<?php echo site_url('Combo_ctrl/ajax_combo_proveedorCp')?>";
	cbo_proveedorCp(ruta,0);   //Llenar combo   

	//carga detalle producto
    $.ajax({
		url : "<?php echo site_url('Cp_orden/ajax_datatable_orden')?>/",
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			 document.getElementById('tablaProducto').innerHTML = data.registro;
			 $(document).ready(function() {
			 });


		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}

function verOrden(){

	//carga detalle producto
    $.ajax({
		url : "<?php echo site_url('Cp_orden/ajax_datatable_verOrden')?>/",
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			 document.getElementById('tablaProducto').innerHTML = data.registro;
			 document.getElementById('m_fecha').value = data.fecha;
			 document.getElementById('m_proveedor').value = data.proveedor;
			 $(document).ready(function() {
			 });


		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}





function add_producto(){
	ruta = "<?php echo site_url('Combo_ctrl/ajax_combo_productoCp')?>";
	cbo_productoCp(ruta,0); 
    document.getElementById('cantidad').value = "0,00";
    document.getElementById('lab_producto').style.display = "none";
    document.getElementById('lab_cantidad').style.display = "none";
    $('#mod_producto').modal('show');
}


function guardar_add_producto(){
validar = valProducto();
if(validar==0){
	$.ajax({
		url : "<?=base_url();?>Cp_orden/ajax_guardar_productoCp",
		type: "POST",
		data: $('#formularioProducto').serialize(),
		dataType: "JSON",
		success: function(data)
		{
			if(data.status==0){ 
				document.getElementById('tablaProducto').innerHTML = data.registro;
				document.getElementById('cantidad').value = "0,00";
				document.getElementById('nroProducto').value = data.nroProducto;

				$('#mod_producto').modal('hide');
			}else{
				swal("No se actualizo el registro", "Ya el Producto esta agregado", {
					icon : "error",
					buttons: {        			
						confirm: {
							className : 'btn btn-danger'
						}
					},
				});

			}	 
		},
		error: function (jqXHR, textStatus, errorThrown)
		{
			alert('Error Insertando / modificanco registro, Comuniquese con el administrador');
		}
	});
}else{
	//document.getElementById('cantidad').value = "0,00";
	swal("No se actualizo el registro", "Falta Información", {
		icon : "error",
		buttons: {        			
			confirm: {
				className : 'btn btn-danger'
			}
		},
	});
}    

}

////////////////////////////////////////////////////////////////////////////////////////////



function eliminar_orden(id){
	swal({
		title: 'Esta seguro de eliminar la Orden de Compra?',
			type: 'warning',
			buttons:{
				cancel: {
					visible: true,
						text : 'No, Cancelar!',
								className: 'btn btn-danger'
				},        			
				confirm: {
					text : 'Si, Eliminar!',
					className : 'btn btn-success'
				}
			}
	}).then((willDelete) => {
		if (willDelete) {
		    $.ajax({
				url : "<?php echo site_url('Cp_orden/ajax_eliminar_orden')?>/"+id,
				type: "GET",                     
				dataType: "JSON",
				success: function(data){
					 document.getElementById('bodyTabla').innerHTML = data.registro;
					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
					 }); 
				},
				error: function (jqXHR, textStatus, errorThrown){
					alert('Error consultando la Base de Datos');
				}
			});
			swal("La orden de Compra fué eliminada!", {
				icon: "success",
				buttons : {
					confirm : {
						className: 'btn btn-success'
					}
				}
			});
		}
	});

}


function modificar_orden(id){
	document.getElementById('guardar02').disabled=false;
	document.getElementById('cerrar02').disabled=false;  

    $.ajax({
		url : "<?php echo site_url('Cp_orden/ajax_act_modificar_orden')?>/"+id,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
             document.getElementById('m_orden').value = data.orden;	 
			 document.getElementById('m_fecha').value = data.fecha;
			 document.getElementById('id_mod').value = data.id;
			 document.getElementById('lab_fecha').style.display = "none";
			 document.getElementById('lab_proveedorCp').style.display = "none";

			ruta = "<?php echo site_url('Combo_ctrl/ajax_combo_proveedorCp')?>";
			cbo_proveedorCp(ruta,data.id_proveedor);   //Llenar combo   

            $('#act_mod_orden').modal('show');  
            

		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});

}

function eliminar_producto(id){
	swal({
		title: 'Esta seguro de eliminar el Producto?',
			type: 'warning',
			buttons:{
				cancel: {
					visible: true,
						text : 'No, Cancelar!',
								className: 'btn btn-danger'
				},        			
				confirm: {
					text : 'Si, Eliminar!',
					className : 'btn btn-success'
				}
			}
	}).then((willDelete) => {
		if (willDelete) {
		    $.ajax({
				url : "<?php echo site_url('Cp_orden/ajax_eliminar_producto')?>/"+id,
				type: "GET",                     
				dataType: "JSON",
				success: function(data){
                    document.getElementById('tablaProducto').innerHTML = data.registro;
                    document.getElementById('cantidad').value = "0,00";
                    document.getElementById('nroProducto').value = data.nroProducto;

					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
					 }); 
				},
				error: function (jqXHR, textStatus, errorThrown){
					alert('Error consultando la Base de Datos');
				}
			});
			swal("El Producto fué eliminado!", {
				icon: "success",
				buttons : {
					confirm : {
						className: 'btn btn-success'
					}
				}
			});
		}
	});
}


function valProducto(){

formulario = "formularioProducto";
error=0;

objeto = "id_cbo_productoCp";
campo = document.forms[formulario].elements[objeto].value;
document.getElementById('lab_producto').style.display = "none";
if (campo==0){
	document.getElementById('lab_producto').style.display = "inline";
   	document.getElementById('lab_producto').innerHTML = "Seleccione el Producto"; 	
   	error = 1;
}


objeto = "cantidad";
campo = document.forms[formulario].elements[objeto].value;
campo = campo.replace(/(^\s*)|(\s*$)/g,"");

document.getElementById('lab_cantidad').style.display = "none";


if (campo=="0,00" || campo.length==0){	
	document.getElementById('lab_cantidad').style.display = "inline";
    document.getElementById('lab_cantidad').innerHTML = "Ingrese la cantidad de Producto"; 	
    document.getElementById('cantidad').value = "0,00";
    error = 1;
}

return error;
}


//GUARDAR ENTRADA

function guardar_form(){
    formulario = "formulario";
    validar = validar_form(formulario,0);
    document.getElementById('guardar01').disabled=true;
	document.getElementById('cerrar01').disabled=true;

     if(validar==0){
        $.ajax({
            url : "<?=base_url();?>Cp_orden/ajax_guardar_ordenCp",
            type: "POST",
            data: $('#formulario').serialize(),
            dataType: "JSON",
            success: function(data)
            {
                if(data.status==0){ 

					swal("Orden de Compra almacenada Correctamente!", "", {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					});

	    			ruta = 	document.getElementById('ruta_guardar_producto').value;
	    			var Form = $('#formulario');
	    			Form.attr("action", ruta);
	    			Form.submit();                	
                }    
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error Insertando / modificanco registro, Comuniquese con el dministrador');
            }
        });         

	}else{
	    document.getElementById('guardar01').disabled=false;
		document.getElementById('cerrar01').disabled=false;



		swal("No se actualizo el registro", "Falta Información", {
				icon : "error",
				buttons: {        			
					confirm: {
						className : 'btn btn-danger'
					}
				},
		});
	}    

}


function guardar_mod_orden(){
    formulario = "formulario";
    validar = validar_form(formulario,1);
    document.getElementById('guardar02').disabled=true;
	document.getElementById('cerrar02').disabled=true;

	if(validar == 0){
        $.ajax({
            url : "<?=base_url();?>Cp_orden/ajax_guardar_mod_orden",
            type: "POST",
            data: $('#formulario').serialize(),
            dataType: "JSON",
            success: function(data)
            {
            	if(data.status==0){ 

					 document.getElementById('bodyTabla').innerHTML = data.registro;
					 $('#act_mod_orden').modal('hide');

					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
				 	});            	


					swal("Registro almacenado Correctamente!", "", {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					});
				}	
	        },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error Insertando / modificanco registro, Comuniquese con el administrador');
            }
        });
    }else{
		swal("No se actualizo el registro", "", {
			icon : "error",
			buttons: {        			
				confirm: {
					className : 'btn btn-danger'
				}
			},
		});
		document.getElementById('guardar02').disabled=false;
		document.getElementById('cerrar02').disabled=false  ;		
    }    
}

function ver_form(id){
	document.location.href = "<?php echo base_url('verOrden')?>/"+id;
}



function validar_form(formulario,accion){
//accion 1= incluir
//accion 2= modificar
error=0;

//fecha 
var date = new Date($('#m_fecha').val());
day = date.getDate();
month = date.getMonth() + 1;
year = date.getFullYear();
fecha = [year, month, day].join('/');

document.getElementById('lab_fecha').style.display = "none";
if(isNaN(day)){
    error=1;
	document.getElementById('lab_fecha').style.display = "inline";
   	document.getElementById('lab_fecha').innerHTML="Ingrese la Fecha"; 	
}else{   
	document.getElementById('lab_fecha').style.display = "none";	
	document.getElementById('lab_fecha').innerHTML=""; 	
	document.getElementById('fechaProducto').value=fecha;
}


objeto = "id_cbo_proveedorCp";
campo = document.forms[formulario].elements[objeto].value;
document.getElementById('lab_proveedorCp').style.display = "none";
if (campo==0){
	document.getElementById('lab_proveedorCp').style.display = "inline";
   	document.getElementById('lab_proveedorCp').innerHTML = "Seleccione el Proveedor"; 	
   	error = 1;
}

if(accion==0){
	objeto = "nroProducto";
	campo = document.forms[formulario].elements[objeto].value;
	document.getElementById('lab_detalleCp').style.display = "none";
	if (campo==0){
		document.getElementById('lab_detalleCp').style.display = "inline";
	   	document.getElementById('lab_detalleCp').innerHTML = "Ingrese los Productos de la entrada"; 	
	   	error = 1;
	}
}	

return error;
}


function cerrar_form(){
    document.location.href = "<?php echo base_url('actCompra')?>"; 

	/*ruta = 	document.getElementById('ruta_guardar_producto').value;
	var Form = $('#formulario');
	Form.attr("action", ruta);
	Form.submit();  
	*/
	
}

function mod_producto(id){
	
	
	
	
	document.location.href = "<?php echo base_url('act_mp')?>/"+id;
/*	
	ruta = "<?php echo site_url('cp_orden/act_modificar_producto')?>/"+id
	var Form = $('#formulario');
	Form.attr("action", ruta);
	Form.submit(); 
*/	
}



function cargar_dataTableProducto(id){
    $.ajax({
		url : "<?php echo site_url('Cp_orden/ajax_datatableOrden')?>/"+id,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			 document.getElementById('bodyTablaProducto').innerHTML = data.registro;
			 document.getElementById('id_orden').value=id;
			 $(document).ready(function() {
				$('#basic-datatables').DataTable({
			    });	
			 });

		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}


function add_productoCp(){
	ruta = "<?php echo site_url('Combo_ctrl/ajax_combo_productoCp')?>";
	cbo_productoCp(ruta,0);
	document.getElementById('titulo').innerHTML = "Incluir Produto de la Orden"; 
    document.getElementById('cantidad').value = "0,00";
    document.getElementById('lab_producto').style.display = "none";
    document.getElementById('lab_cantidad').style.display = "none";
	document.getElementById('accion').value = "1";	    
    $('#mod_productoOrden').modal('show');

}


function actualizar_productoCp(){
if(document.getElementById('accion').value == "1"){
    guardar_add_productoCp();	
}else{
	guardar_mod_productoOrden();
}	
	
}



function guardar_add_productoCp(){
	
    validar = valProducto();
    if(validar==0){
    	document.getElementById('cerrar02').style.value="none";
    	document.getElementById('guardar02').style.value="none";
    	id = document.getElementById('id_orden').value;

        $.ajax({
            url : "<?php echo site_url('Cp_orden/ajax_guardar_add_productoCp')?>/"+id,
            type: "POST",
            data: $('#formularioProducto').serialize(),
            dataType: "JSON",
            success: function(data)
            {
		    	document.getElementById('cerrar02').style.value="inline";
				document.getElementById('guardar02').style.value="inline";
            	if(data.status==0){ 
                    document.getElementById('bodyTablaProducto').innerHTML = data.registro;
					swal("Registro almacenado Correctamente!", "", {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					});
                    document.getElementById('cantidad').value = "0,00";
					$('#mod_productoOrden').modal('hide');

				}else{
					swal("No se actualizo el registro", "Ya el Producto esta agregado", {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});

				}	 
	        },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error Insertando / modificanco registro, Comuniquese con el administrador');
            }
        });
    }else{
        //document.getElementById('cantidad').value = "0,00";
		swal("No se actualizo el registro", "Falta Información", {
			icon : "error",
			buttons: {        			
				confirm: {
					className : 'btn btn-danger'
				}
			},
		});
    }    

}


function elim_producto(id){
	idOrden = document.getElementById('id_orden').value;
	id = id + "aa" + idOrden;
	
	swal({
		title: 'Esta seguro de eliminar el Producto?',
			type: 'warning',
			buttons:{
				cancel: {
					visible: true,
						text : 'No, Cancelar!',
								className: 'btn btn-danger'
				},        			
				confirm: {
					text : 'Si, Eliminar!',
					className : 'btn btn-success'
				}
			}
	}).then((willDelete) => {
		if (willDelete) {
		    $.ajax({
				url : "<?php echo site_url('Cp_orden/ajax_borrar_producto')?>/"+id,
				type: "GET",                     
				dataType: "JSON",
				success: function(data){
					 document.getElementById('bodyTablaProducto').innerHTML = data.registro;
					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
					 }); 
				},
				error: function (jqXHR, textStatus, errorThrown){
					alert('Error consultando la Base de Datos');
				}
			});
			swal("El registro fué desactivado!", {
				icon: "success",
				buttons : {
					confirm : {
						className: 'btn btn-success'
					}
				}
			});
		}
	});

}


function mod_productoOrden(id){
	document.getElementById('guardar02').disabled=false;
	document.getElementById('cerrar02').disabled=false;       
    $.ajax({
		url : "<?php echo site_url('Cp_orden/ajax_act_modificar_producto')?>/"+id,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			 document.getElementById('titulo').innerHTML = "Modificar Produto de la Orden" ;
			 document.getElementById('id_mod').value = data.id_mod;
			 document.getElementById('cantidad').value = data.cantidad;
			 document.getElementById('idProductoOrig').value = data.idProducto;	
			 document.getElementById('accion').value = "2";	
			 		 
			 
			 document.getElementById('lab_producto').style.display = "none";
			 document.getElementById('lab_cantidad').style.display = "none";

			 
			ruta = "<?php echo site_url('Combo_ctrl/ajax_combo_productoCp')?>";
			cbo_productoCp(ruta,data.idProducto); 			
			 
            $('#mod_productoOrden').modal('show');  
		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});

}	

function guardar_mod_productoOrden(){
 	formulario = "formularioProducto";
    id = document.getElementById('id_mod').value;
	validar = valProducto();    
	if(validar == 0){
		document.getElementById('guardar02').disabled=true;
		document.getElementById('cerrar02').disabled=true;  
		id = document.getElementById('id_orden').value;
	         
        $.ajax({
            url : "<?=base_url();?>Cp_orden/guardar_mod_productoOrden/"+id,
            type: "POST",
            data: $('#formularioProducto').serialize(),
            dataType: "JSON",
            success: function(data)
            {
            	if(data.status==0){ 

					 document.getElementById('bodyTablaProducto').innerHTML = data.registro;
					 $('#mod_productoOrden').modal('hide');

					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
				 	});            	


					swal("Registro almacenado Correctamente!", "", {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					});
				}else{
					$('#mod_productoOrden').modal('hide');
					swal("No se actualizo el registro", "Ya el Producto esta agregado", {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});					
					
				}
					
	        },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error Insertando / modificanco registro, Comuniquese con el administrador');
            }
        });
    }else{
		swal("No se actualizo el registro", "", {
			icon : "error",
			buttons: {        			
				confirm: {
					className : 'btn btn-danger'
				}
			},
		});
    }    




}



function validar_formProducto(formulario){

error=0;

objeto = "cantidadMod";
campo = document.forms[formulario].elements[objeto].value;
campo = campo.replace(/(^\s*)|(\s*$)/g,"");

document.getElementById('lab_cantidadMod').style.display = "none";


if (campo=="0,00" || campo.length==0){	
	document.getElementById('lab_cantidadMod').style.display = "inline";
    document.getElementById('lab_cantidadMod').innerHTML = "Ingrese la cantidad de Producto"; 	
    document.getElementById('cantidadMod').value = "0,00";
    error = 1;
}

return error;
}




</script>
