<?php
$espera = base_url() . "assets/images/tenor.gif";
?>

<!-- Modal -->
<div class="modal fade" id="gifEspera_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"
     style="padding-top:200px">
	<div class="modal-dialog" role="document">
		<div class="modal-content" style="width:400px; margin-left:20px;">
			<div class="modal-body">
				<div class="row" style="padding-top: 5px">
					<div class="col-md-12" style="text-align:center">
						<img src="<?=$espera;?>" style="width:250px; height:250px"> 
					</div>
                </div> 
			</div>
		</div>
	</div>
</div>

