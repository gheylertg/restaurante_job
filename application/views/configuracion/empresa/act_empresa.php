<?php
$incluir = base_url() . "assets/images/nuevo.png";
$asterisco = ' style="color=red; font-size:16px"';

$ci = &get_instance();
$ci->load->library('Clssession');
$accion= $ci->clssession->accion(1,2); 

$ruta = base_url() . "Cf_empresa/ajax_guardar_add";
$rutaImagen = base_url() . "Cf_empresa/ajax_guardar_modImagen";
$rutaLogo = base_url() . "Cf_empresa/ajax_guardar_modLogo";

?>



        <br>            
        <div class="main-panel">
            <div class="content">
                <div class="page-inner mt--5">
                    <div class="row row-card-no-pd mt--2">
                        <div class="col-sm-12 col-md-12">
                            <div class="card card-stats card-round">
                                <div class="card-body ">
									 <h2><span style="color:blue">Actualizar Empresa</span></h2>
                                </div>
                            </div>
                        </div>
                    </div>

                <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                <?php
                                if($accion==1){
                                ?>
                                <div  style="text-align:right">
									<a href="javascript:void(0)" onclick="javascript:add_form()">
										<img src="<?=$incluir;?>" style="width:35px; height:35px" alt="Incluir" title="Incluir empresa">
									</a>	
                                </div>
                                <?php
                                }
                                ?>
                                <br>									
									
									
									
									
                                    <div class="table-responsive">
										<div id="bodyTabla">
										</div>
										
										
                                    </div>
                                </div>
                            </div>
                        </div>    
                </div>
            </div>
    




<!-- Modal -->
<div class="modal fade" id="mod_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content" style="width:1100px; margin-left:-250px">
			<div class="modal-header bg-primary text-white">
				<h4 class="modal-title" id="exampleModalLabel" style="color:blue">
					<div style="font-weight: 700" id="titulo_mostrar"></div>
				</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true" style="color:white; font-size:30px">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				
			<form name="formulario" method="post" id="formulario" autocomplete="off" 
			      enctype="multipart/form-data">	
				
			  <div class="container">
				<div class="row">
					<div class="col-md-8">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							     Nombre <span style="color:#ff0000;">*</span></span>
							 </label>
                        <input style="padding-top: 5px;margin-right:-20px;margin-top: 0px;" 
                               type="text" name="m_nombre" id="m_nombre" class="form-control" 
                               placeholder="Edite el nombre" maxlength="100" minlength="0">
                        <div id="lab_nombre" style="color:red; margin-left:9px; display:none"></div>       
					</div>

					<div class="col-md-4">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							     RUT <span style="color:#ff0000;">*</span></span>
							 </label>
                        <input style="padding-top: 5px;margin-right:-20px;margin-top: 0px;" 
                               type="text" name="m_rut" id="m_rut" class="form-control" 
                               maxlength="100" minlength="0">
                        <div id="lab_rut" style="color:red; margin-left:9px; display:none"></div>       
					</div>

				</div>	

				<div class="row">
					<div class="col-md-6">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							     Correo Electrónico</span>
							 </label>
                        <input style="padding-top: 5px;margin-right:-20px;margin-top: 0px;" 
                               type="text" name="m_correo" id="m_correo" class="form-control" 
                               maxlength="100" minlength="0">
                        <div id="lab_correo" style="color:red; margin-left:9px; display:none"></div>        

					</div>	

					<div class="col-md-3">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							     Teléfono Local</span>
							 </label>
                        <input style="padding-top: 5px;margin-right:-20px;margin-top: 0px;" 
                               type="text" name="m_telefono_local" id="m_telefono_local" class="form-control"
                               maxlength="100" minlength="0">
					</div>	

					<div class="col-md-3">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							     Teléfono Celular</span>
							 </label>
                        <input style="padding-top: 5px;margin-right:-20px;margin-top: 0px;" 
                               type="text" name="m_telefono_celular" id="m_telefono_celular" class="form-control" maxlength="100" minlength="0">
					</div>	
				</div>	

				<div class="row">
					<div class="col-md-12">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							     Dirección (Ubicación) </span>
							 </label>
                        <textarea style="padding-top: 5px;margin-right:-20px;margin-top: 0px;" 
                               name="m_direccion" id="m_direccion" class="form-control" rows="2"></textarea>
					</div>
				</div>		
				<div class="row">
					<div class="col-md-6">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							     Contacto </span>
							 </label>
                        <input style="padding-top: 5px;margin-right:-20px;margin-top: 0px;" 
                               type="text" name="m_contacto" id="m_contacto" class="form-control" 
                               maxlength="100" minlength="0">
					</div>
				</div>	

				<div class="row">
					<div class="col-md-6">					
						<div id="div-imagen">
	        	            <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
								 <span style="color:#0000ff; font-size:16px; font-weight:600">
								     Imagen de la empresa </span>
								 </label><br>

	                    	<input type="file" id="m_imagen" name="m_imagen"
	                        	   class="form-control"/>

	                        <div>Archivo con extensión JPEG, JPG, PNG</div>
      	   
						</div>
					</div>
                    <!-- 
					<div class="col-md-6">					
						<div id="div-logo">
	        	            <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
								 <span style="color:#0000ff; font-size:16px; font-weight:600">
								     Logo de la empresa </span>
								 </label><br>

	                    	<input type="file" id="m_logo" name="m_logo"
	                        	   class="form-control"/>

	                        <div>Archivo con extensión JPEG, JPG, PNG</div>
      	   
						</div>
					</div>
				    -->
				</div>		





                <br>  
				<span style="color:#ff0000; font-size:16px; text-align:left">Campo obligatorio (*)</span>
			  </div>				
			</div>
			<div class="modal-footer">

                   
				<button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrar01">Cerrar</button>


		        <button type="button" class="btn btn-primary" onclick="actualizar_form();" 
		                id="guardar01">
		              Guardar
		        </button>

			</div>


            <input type="hidden" id="id_mod" name="id_mod">
            <input type="hidden" id="nombre_orig" name="nombre_orig">
            <input type="hidden" id="val_error" name="val_error" value="0">
            <input type="hidden" id="accion_formulario" name="accion_formulario" value="0">
            <input type="hidden" id="ruta" name="ruta" value="<?=$ruta;?>">

			</form>  

		</div>
	</div>
</div>

 


 <!-- Modal -->
<div class="modal fade" id="ver_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content" style="width:1000px; margin-left:-200px">
			<div class="modal-header bg-primary text-white">
				<h4 class="modal-title" id="exampleModalLabel" style="color:blue">
					<div style="font-weight: 700">Datos de la Empresa</div>
				</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true" style="color:white; font-size:30px">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				
			<form name="formulario" method="post" id="formulario" autocomplete="off" 
			      enctype="multipart/form-data">	
				
			  <div class="container">
				<div class="row">
					<div class="col-md-6">
                        <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							     Nombre <span style="color:#ff0000;">*</span></span>
							 </label>
                        <input style="padding-top: 5px;margin-right:-20px;margin-top: 0px;" 
                               type="text" name="v_nombre" id="v_nombre" class="form-control" 
                               disabled="true">
                    </div>
                    <div class="col-md-2">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							     RUT <span style="color:#ff0000;">*</span></span>
							 </label>
                        <input style="padding-top: 5px;margin-right:-20px;margin-top: 0px;" 
                               type="text" name="v_rut" id="v_rut" class="form-control" 
                               disabled="true">
					</div>

					<div class="col-md-4">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							     Contacto </span>
							 </label>
                        <input style="padding-top: 5px;margin-right:-20px;margin-top: 0px;" 
                               type="text" name="v_contacto" id="v_contacto" class="form-control" 
                               disabled="true">
					</div>					




				</div>	


				<div class="row">
					<div class="col-md-6">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							     Correo Electrónico</span>
							 </label>
                        <input style="padding-top: 5px;margin-right:-20px;margin-top: 0px;" 
                               type="text" name="v_correo" id="v_correo" class="form-control" 
                               disabled="true">
                        
					</div>	

					<div class="col-md-3">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							     Teléfono Local</span>
							 </label>
                        <input style="padding-top: 5px;margin-right:-20px;margin-top: 0px;" 
                               type="text" name="v_telefono_local" id="v_telefono_local" class="form-control"
                               disabled="true">
					</div>	

					<div class="col-md-3">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							     Teléfono Celular</span>
							 </label>
                        <input style="padding-top: 5px;margin-right:-20px;margin-top: 0px;" 
                               type="text" name="v_telefono_celular" id="v_telefono_celular" 
                               class="form-control" disabled="true">
					</div>	
				</div>	

				<div class="row">
					<div class="col-md-12">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							     Dirección (Ubicación) </span>
							 </label>
                        <textarea style="padding-top: 5px;margin-right:-20px;margin-top: 0px;" 
                               name="v_direccion" id="v_direccion" class="form-control" 
                               rows="2" disabled="true"></textarea>
					</div>
				</div>		

				<div class="row">
					<div class="col-md-6">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							     Imagen de la Empresa </span>
							 </label>
                         <div id="div-imagenV"></div>
					</div>

					<div class="col-md-6">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							     Logo de la Empresa </span>
							 </label>
                         <div id="div-logoV"></div>
					</div>

				</div>	



                <br>  
				<span style="color:#ff0000; font-size:16px; text-align:left">Campo obligatorio (*)</span>
			  </div>				
			</div>
			<div class="modal-footer">

                   
				<button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrar01">Cerrar</button>



			</div>



			</form>  

		</div>
	</div>
</div>




<!-- ----------------- Modificar Nota ------------------------- -->

<!-- Modal -->
<div class="modal fade" id="modImagen_modal" tabindex="-1" role="dialog" 
     aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content" style="width:900px; margin-left:-180px">
			<div class="modal-header bg-primary text-white">
				<h4 class="modal-title" id="exampleModalLabel" style="color:white">
					<div style="font-weight: 700" id="titulo_mostrarImagen"></div>
				</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true" style="color:white; font-size:30px">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				
			<form name="formularioImagen" method="post" id="formularioImagen" 
			      enctype="multipart/form-data">	
				
				
			  <div class="container">
                
				<div class="row">
					<div class="col-md-8" style="padding-top: 10px">
                        <label>
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							 Imagen de la Empresa</span>
					    </label>

                        <input type="file" id="mod_imagen" name="mod_imagen"
                               class="form-control"	/>
                        <div style="color:blue">Archivo con extensión JPEG, JPG, PNG</div>
                        <div id="lab_imagen" style="color:red; margin-left:9px; display:none"></div>       
					</div>
				</div>	

                <br>  
				<span style="color:#ff0000; font-size:16px; text-align:left">Campo obligatorio (*)</span>
			  </div>				
			</div>
			<div class="modal-footer">

                   
				<button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrar02">Cerrar</button>


		        <button type="button" class="btn btn-primary" onclick="guardar_modImagen_form()" 
		                id="guardar02">
		              Guardar
		        </button>

			</div>

			<input type="hidden" id="id_mod_imagen" name="id_mod_imagen">
			<input type="hidden" id="ruta_imagen" name="ruta_imagen" value="<?=$rutaImagen;?>">

			</form>  

		</div>
	</div>
</div>



<!-- ----------------- Modificar Logo ------------------------- -->

<!-- Modal -->
<div class="modal fade" id="modLogo_modal" tabindex="-1" role="dialog" 
     aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content" style="width:900px; margin-left:-180px">
			<div class="modal-header bg-primary text-white">
				<h4 class="modal-title" id="exampleModalLabel" style="color:white">
					<div style="font-weight: 700" id="titulo_mostrarLogo"></div>
				</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true" style="color:white; font-size:30px">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				
			<form name="formularioLogo" method="post" id="formularioLogo" 
			      enctype="multipart/form-data">	
				
				
			  <div class="container">
                
				<div class="row">
					<div class="col-md-8" style="padding-top: 10px">
                        <label>
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							 Logo de la Empresa</span>
					    </label>

                        <input type="file" id="mod_logo" name="mod_logo"
                               class="form-control"	/>
                        <div style="color:blue">Archivo con extensión JPEG, JPG, PNG</div>
                        <div id="lab_logo" style="color:red; margin-left:9px; display:none"></div>       
					</div>
				</div>	

                <br>  
				<span style="color:#ff0000; font-size:16px; text-align:left">Campo obligatorio (*)</span>
			  </div>				
			</div>
			<div class="modal-footer">

                   
				<button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrar03">Cerrar</button>


		        <button type="button" class="btn btn-primary" onclick="guardar_modLogo_form()" 
		                id="guardar03">
		              Guardar
		        </button>

			</div>

			<input type="hidden" id="id_mod_logo" name="id_mod_logo">
			<input type="hidden" id="ruta_logo" name="ruta_logo" value="<?=$rutaLogo;?>">

			</form>  

		</div>
	</div>
</div>
 
