



<!--cargar datatable-->
<script>
    $(document).ready(function(){

	    <?php 
    	if($accion==3){
    	?>
    		document.location.href = "<?php echo base_url('actEmpresa')?>"; 

	    <?php 
		}else{
    	?>
			cargarDataTable(<?=$opcion;?>);	
	    <?php 
		}
    	?>
	});



function cargarDataTable(opcion){
    $.ajax({
		url : "<?php echo site_url('Cf_empresa/ajax_datatable')?>",
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			 document.getElementById('bodyTabla').innerHTML = data.registro;

			 $(document).ready(function() {
				$('#basic-datatables').DataTable({
			    });	
			 });

             if(opcion==1){
					swal("Registro almacenado Correctamente!", "", {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					});
             }

             if(opcion==2){
				swal("No se actualizo el registro", "La imagen no se adjunto correctamente, consulte si el archivo es una imagen con extension JPEG, JPG, PNG", {
					icon : "error",
					buttons: {        			
						confirm: {
							className : 'btn btn-danger'
						}
					},
				});                
             }	



		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}


//cargar combos





function actualizar_form(){
    accion = document.getElementById('accion_formulario').value;
    if(accion==1){
        guardar_add_form();
    }else{
        guardar_mod_form();
    } 


}


function add_form(){
	$('#formulario')[0].reset(); // reset form on modals

    document.getElementById('accion_formulario').value="1";
    document.getElementById('titulo_mostrar').innerHTML = "Incluir Empresa";

    document.getElementById('guardar01').disabled=false;
	document.getElementById('cerrar01').disabled=false;     	

    document.getElementById('lab_nombre').style.display="none";
    document.getElementById('lab_rut').style.display="none";
    document.getElementById('lab_correo').style.display="none";


    document.getElementById('div-imagen').style.display="inline";
    //document.getElementById('div-logo').style.display="inline";

    //document.getElementById('url_mostrar').style.display = "inline";    
	//ruta = "<?php //echo site_url('Combo_ctrl/ajax_combo_modulo')?>";
	//cbo_modulo(ruta,0);   //Llenar combo    
    document.getElementById('nombre_orig').value = "";
    $('#mod_modal').modal('show');


}


function modificar_form(id){

    //cbo_modelo(ruta,data.id_proyecto_local,data.id_proyecto_local);   //Llenar combo    

    document.getElementById('accion_formulario').value="2";
    document.getElementById('titulo_mostrar').innerHTML = "Modificar Empresa";
    //document.getElementById('url_mostrar').style.display = "none";

    document.getElementById('guardar01').disabled=false;
	document.getElementById('cerrar01').disabled=false;     
	document.getElementById('div-imagen').style.display="none";
	document.getElementById('div-logo').style.display="none";


    $.ajax({
		url : "<?php echo site_url('Cf_empresa/ajax_modificar')?>/"+id,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			 document.getElementById('m_nombre').value = data.nombre;
			 document.getElementById('m_rut').value = data.rut;
			 document.getElementById('m_correo').value = data.correo;
			 document.getElementById('m_contacto').value = data.contacto;
			 document.getElementById('m_direccion').value = data.direccion;
			 document.getElementById('m_telefono_local').value = data.telefonoLocal;			 
			 document.getElementById('m_telefono_celular').value = data.telefonoCelular;			 
			 document.getElementById('id_mod').value = data.id;
			 document.getElementById('val_error').value = "0";
			 document.getElementById('lab_nombre').style.display = "none";
			 document.getElementById('lab_rut').style.display = "none";
			 document.getElementById('lab_correo').style.display = "none";


			 document.getElementById('nombre_orig').value = data.nombre;
			 //ruta = "<?php //echo site_url('Combo_ctrl/ajax_combo_modulo')?>";
	         //cbo_modulo(ruta,data.id_modulo);   //Llenar combo    
             $('#mod_modal').modal('show');

		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}


function modificarImagen_form(id){
	$('#formularioImagen')[0].reset(); // reset form on modals
   	document.getElementById('guardar02').disabled=false;
	document.getElementById('cerrar02').disabled=false; 		
    document.getElementById('titulo_mostrarImagen').innerHTML = "Cambiar Imagen de la Empresa";
    document.getElementById('id_mod_imagen').value = id;
    document.getElementById('lab_imagen').style.display = "none"; 
    $('#modImagen_modal').modal('show');
}


function modificarLogo_form(id){
	$('#formularioLogo')[0].reset(); // reset form on modals
   	document.getElementById('guardar03').disabled=false;
	document.getElementById('cerrar03').disabled=false; 		
    document.getElementById('titulo_mostrarLogo').innerHTML = "Cambiar Logo de la Empresa";
    document.getElementById('id_mod_logo').value = id;
    document.getElementById('lab_logo').style.display = "none"; 
    $('#modLogo_modal').modal('show');
}



function ver_form(id){


    $.ajax({
		url : "<?php echo site_url('Cf_empresa/ajax_ver')?>/"+id,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			 document.getElementById('div-imagenV').innerHTML = data.imagen;  
			 document.getElementById('div-logoV').innerHTML = data.logo;  
			 document.getElementById('v_nombre').value = data.nombre;
			 document.getElementById('v_rut').value = data.rut;
			 document.getElementById('v_correo').value = data.correo;
			 document.getElementById('v_contacto').value = data.contacto;
			 document.getElementById('v_direccion').value = data.direccion;
			 document.getElementById('v_telefono_local').value = data.telefonoLocal;			 
			 document.getElementById('v_telefono_celular').value = data.telefonoCelular;			 
             $('#ver_modal').modal('show');

		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}




function guardar_add_form(){
	//validar campos formulario
	formulario = "formulario";
	error=0;
	error = validar_form(formulario,1);

	if(error==0){
    	document.getElementById('guardar01').disabled=true;
		document.getElementById('cerrar01').disabled=true; 		
	}



    nombre = document.getElementById('m_nombre').value;
    ruta = document.getElementById('ruta').value;



	if(error == 0){
	    $.ajax({
			url : "<?php echo site_url('cf_empresa/ajax_validar_duplicidad')?>",
			type: "GET",                     
			dataType: "JSON",
			data: {nombre:nombre},
			success: function(data){
				if(data.status==0){ 
					var Form = $('#formulario');
					Form.attr("action", ruta);
					Form.submit();
			    }else{
					document.getElementById('lab_nombre').style.display = "inline";
					document.getElementById('lab_nombre').innerHTML = "El nombre de la Empresa, ya se encuentra registrado.";
			    	document.getElementById('guardar01').disabled=false;
					document.getElementById('cerrar01').disabled=false; 					
					swal("No se actualizo el registro", "", {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});
				}	
			},
			error: function (jqXHR, textStatus, errorThrown){
				alert('Error consultando la Base de Datos');
			}
		});

    }    






} //Fin de add form	

function guardar_mod_form(){

	//validar campos formulario
	formulario = "formulario";
	error=0;
	error = validar_form(formulario,2);
	if(error==0){
    	document.getElementById('guardar01').disabled=true;
		document.getElementById('cerrar01').disabled=true; 		
	}
	

	if(error == 0){
        $.ajax({
            url : "<?=base_url();?>Cf_empresa/ajax_guardar_mod",
            type: "POST",
            data: $('#formulario').serialize(),
            dataType: "JSON",
            success: function(data)
            {
            	if(data.status==0){ 
					 document.getElementById('bodyTabla').innerHTML = data.registro;
					 $('#mod_modal').modal('hide');

					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
				 	});            	


					swal("Registro almacenado Correctamente!", "", {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					});
				}else{
					document.getElementById('lab_nombre').style.display = "inline";
					document.getElementById('lab_nombre').innerHTML = "El Nombre ingresado, ya se encuentra registrado.";
			    	document.getElementById('guardar01').disabled=false;
					document.getElementById('cerrar01').disabled=false; 		
					swal("No se actualizo el registro", "", {

						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});

				}	
	        },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error Insertando / modificanco registro, Comuniquese con el administrador');
            }
        });
    }else{
		swal("No se actualizo el registro", "", {
			icon : "error",
			buttons: {        			
				confirm: {
					className : 'btn btn-danger'
				}
			},
		});
    }    
}


function guardar_modImagen_form(){

	//validar campos formulario
	formulario = "formularioImagen";
	error=0;
	error = validarImagen_form(formulario,0);
	if(error==0){
	
    	document.getElementById('guardar02').disabled=true;
		document.getElementById('cerrar02').disabled=true; 		

        rutaImagen = document.getElementById('ruta_imagen').value;
      
		var Form = $('#formularioImagen');
		Form.attr("action", rutaImagen);
		Form.submit();
	}else{
		swal("No se actualizo el registro", "", {
			icon : "error",
			buttons: {        			
				confirm: {
					className : 'btn btn-danger'
				}
			},
		});
	}	

}


function guardar_modLogo_form(){

	//validar campos formulario

	formulario = "formularioLogo";
	error=0;
	error = validarLogo_form(formulario,0);
	if(error==0){
    	document.getElementById('guardar03').disabled=true;
		document.getElementById('cerrar03').disabled=true; 		
        rutaLogo = document.getElementById('ruta_logo').value;
		var Form = $('#formularioLogo');
		Form.attr("action", rutaLogo);
		Form.submit();
	}else{
		swal("No se actualizo el registro", "", {
			icon : "error",
			buttons: {        			
				confirm: {
					className : 'btn btn-danger'
				}
			},
		});
	}	

}





function validarImagen_form(formulario, accion){

campo = "mod_imagen";
campo = document.forms[formulario].elements[campo].value;

if (campo.length==0){
	document.getElementById('lab_imagen').style.display = "none";
	document.getElementById('lab_imagen').style.display = "inline";
    document.getElementById('lab_imagen').innerHTML = "Ingrese la Imagen de la Empresa";  
    error=1;
}else{   
    nombreArchivo = campo; 
    tam = campo.length;
    ext = tam - 3;
    cad = campo.substring(ext,tam);
    cad = cad.toUpperCase();
    if(cad == 'JPG' || cad == 'PNG' || cad == 'GIF' ){
    	error = 0;
    }else{
	    nombreArchivo = campo; 
	    tam = campo.length;
	    ext = tam - 4;
	    cad = campo.substring(ext,tam);
	    cad = cad.toUpperCase();
	    if(cad == 'JPEG'){
	    	error = 0;
	    }else{
	    	error = 1;
	    }	
    }	

    if(error == 1){
		document.getElementById('lab_imagen').style.display = "none";
		document.getElementById('lab_imagen').style.display = "inline";
	    document.getElementById('lab_imagen').innerHTML = "La extensión del archivo no corresponde a la solicitadas";    	
    }
}

return error;
}



function validarLogo_form(formulario, accion){

campo = "mod_logo";
campo = document.forms[formulario].elements[campo].value;

if (campo.length==0){
	document.getElementById('lab_logo').style.display = "none";
	document.getElementById('lab_logo').style.display = "inline";
    document.getElementById('lab_logo').innerHTML = "Ingrese el Logo de la Empresa";  
    error=1;
}else{   
    nombreArchivo = campo; 
    tam = campo.length;
    ext = tam - 3;
    cad = campo.substring(ext,tam);
    cad = cad.toUpperCase();
    if(cad == 'JPG' || cad == 'PNG' || cad == 'GIF' ){
    	error = 0;
    }else{
	    nombreArchivo = campo; 
	    tam = campo.length;
	    ext = tam - 4;
	    cad = campo.substring(ext,tam);
	    cad = cad.toUpperCase();
	    if(cad == 'JPEG'){
	    	error = 0;
	    }else{
	    	error = 1;
	    }	
    }	

    if(error == 1){
		document.getElementById('lab_logo').style.display = "none";
		document.getElementById('lab_logo').style.display = "inline";
	    document.getElementById('lab_logo').innerHTML = "La extensión del archivo no corresponde a la solicitadas";    	
    }
}

return error;
}







function eliminar_form(id){
	swal({
		title: 'Esta seguro de ?',
			type: 'warning',
			buttons:{
				cancel: {
					visible: true,
						text : 'No, Cancelar!',
								className: 'btn btn-danger'
				},        			
				confirm: {
					text : 'Si, Desactivar!',
					className : 'btn btn-success'
				}
			}
	}).then((willDelete) => {
		if (willDelete) {
		    $.ajax({
				url : "<?php echo site_url('Cf_empresa/ajax_desactivar')?>/"+id,
				type: "GET",                     
				dataType: "JSON",
				success: function(data){
					 document.getElementById('bodyTabla').innerHTML = data.registro;
					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
					 }); 
				},
				error: function (jqXHR, textStatus, errorThrown){
					alert('Error consultando la Base de Datos');
				}
			});
			swal("El registro fué desactivado!", {
				icon: "success",
				buttons : {
					confirm : {
						className: 'btn btn-success'
					}
				}
			});
		}
	});

}

function reactivar_form(id){
	swal({
		title: 'Esta seguro de ?',
			type: 'warning',
			buttons:{
				cancel: {
					visible: true,
						text : 'No, Cancelar!',
								className: 'btn btn-danger'
				},        			
				confirm: {
					text : 'Si, Reactivar!',
					className : 'btn btn-success'
				}
			}
	}).then((willDelete) => {
		if (willDelete) {
		    $.ajax({
				url : "<?php echo site_url('Cf_empresa/ajax_reactivar')?>/"+id,
				type: "GET",                     
				dataType: "JSON",
				success: function(data){
					 document.getElementById('bodyTabla').innerHTML = data.registro;
					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
					 }); 
				},
				error: function (jqXHR, textStatus, errorThrown){
					alert('Error consultando la Base de Datos');
				}
			});
			swal("El registro fué reactivado!", {
				icon: "success",
				buttons : {
					confirm : {
						className: 'btn btn-success'
					}
				}
			});
		}
	});
}







function validar_form(formulario,accion){
//accion 1= incluir
//accion 2= modificar

error=0;

objeto = "m_nombre";
campo = document.forms[formulario].elements[objeto].value;
campo = campo.replace(/(^\s*)|(\s*$)/g,"");
nombre = campo;

document.getElementById('lab_nombre').style.display = "none";

if (campo.length==0){
	document.getElementById('lab_nombre').style.display = "inline";
    document.getElementById('lab_nombre').innerHTML = "Edite el nombre"; 	
    error = 1;
}

objeto = "m_rut";
campo = document.forms[formulario].elements[objeto].value;
campo = campo.replace(/(^\s*)|(\s*$)/g,"");
nombre = campo;

document.getElementById('lab_rut').style.display = "none";

if (campo.length==0){
	document.getElementById('lab_rut').style.display = "inline";
    document.getElementById('lab_rut').innerHTML = "Edite el RUT"; 	
    error = 1;
}


objeto = "m_correo";
campo = document.forms[formulario].elements[objeto].value;
document.getElementById('lab_correo').style.display = "none";
if(campo.length!=0){
    if (/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(campo)){
	    x=0;
	}else{
		document.getElementById('lab_correo').style.display = "inline";
        document.getElementById('lab_correo').innerHTML = "El Formato del Correo Electronico es incorrecto";
        error = 1;
	}

}





return error;
}



</script>
