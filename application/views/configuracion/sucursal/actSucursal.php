<?php
$incluir = base_url() . "assets/images/nuevo.png";
$asterisco = ' style="color=red; font-size:16px"';

$ci = &get_instance();
$ci->load->library('Clssession');
$permiso_add= $ci->clssession->accion(1,2); 

?>
        <br>            
        <div class="main-panel">
            <div class="content">
                <div class="page-inner mt--5">
                    <div class="row row-card-no-pd mt--2">
                        <div class="col-sm-12 col-md-12">
                            <div class="card card-stats card-round">
                                <div class="card-body ">
									 <h2><span style="color:blue">Sucursal</span></h2>
                                </div>
                            </div>
                        </div>
                    </div>

            
                <?php
                    if($permiso_add==1){
                ?> 
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header" style="text-align:right">
							<a href="javascript:void(0)" onclick="javascript:add_form()">
							   <img src="<?=$incluir;?>" style="width:35px; height:35px" alt="Incluir" title="Incluir Sucursal">
							</a>
                        </div>
                    </div>
                </div>    
                <?php
                    }
                ?> 

               

                <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="table-responsive">
										<div id="bodyTabla">
										</div>
										
										
                                    </div>
                                </div>
                            </div>
                        </div>    
                </div>
            </div>
    



<!-- Modal -->
<div class="modal fade" id="mod_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content" style="width:900px; margin-left:-130px">
			<div class="modal-header">
				<h4 class="modal-title" id="exampleModalLabel" style="color:blue">
					<div id="titulo_mostrar"></div>
				</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				
			<form name="formulario" method="post" id="formulario" autocomplete="off">	
				
				
			  <div class="container">
				<div class="row">
					<div class="col-md-8">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							     Nombre <span style="color:#ff0000;">*</span></span>
							 </label>
                        <input style="padding-top: 5px;margin-right:-20px;margin-top: 0px;" 
                               type="text" name="m_nombre" id="m_nombre" class="form-control" 
                               placeholder="Edite el nombre" maxlength="100" minlength="0"                                >
                        <div id="lab_nombre" style="color:red; margin-left:9px; display:none"></div>       
					</div>
					
					<div class="col-md-4">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							     RUT <span style="color:#ff0000;">*</span></span>
							 </label>
                        <input style="padding-top: 5px;margin-right:-20px;margin-top: 0px;" 
                               type="text" name="m_rut" id="m_rut" class="form-control" 
                               placeholder="Edite el RUT" maxlength="100" minlength="0"                                >
                        <div id="lab_rut" style="color:red; margin-left:9px; display:none"></div>       

					</div>
				</div>	

<!--
                <div id="valEmpresa"> 
				<div class="row" style="padding-top: 10px">
					<div class="col-md-12">
                         <label style="padding-top: 0px; margin-top: 0px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">Empresa Asociada <span style="color:#ff0000;">*</span></span>
						 </label>
						<div id="cbo_empresa"></div>
						<div id="lab_empresa" style="color:red; margin-left:9px; display:none"></div>
					</div>
				</div>	
			    </div>   
-->
			    

				<div class="row" style="padding-top: 10px">
					<div class="col-md-6">
                         <label style="padding-top: 0px; margin-top: 0px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">Región <span style="color:#ff0000;">*</span></span>
						 </label>
						<div id="cbo_region"></div>
						<div id="lab_region" style="color:red; margin-left:9px; display:none"></div>
					</div>
					<div class="col-md-3">
                         <label style="padding-top: 0px; margin-top: 0px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">Teléfono Local</span>
						 </label>
                        <input style="padding-top: 5px;margin-right:-20px;margin-top: 0px;" 
                               type="text" name="m_telefono_local" id="m_telefono_local" 
                               class="form-control" maxlength="100" minlength="0">
					</div>
					<div class="col-md-3">
                         <label style="padding-top: 0px; margin-top: 0px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">Teléfono Celular
							 </span>
						 </label>
                        <input style="padding-top: 5px;margin-right:-20px;margin-top: 0px;" 
                               type="text" name="m_telefono_celular" id="m_telefono_celular" 
                               class="form-control" maxlength="100" minlength="0">
					</div>
				</div>	

				<div class="row">
					<div class="col-md-7">
                        <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							     Correo</span>
							 </label>
                        <input style="padding-top: 5px;margin-right:-20px;margin-top: 0px;" 
                               type="text" name="m_correo" id="m_correo" class="form-control" 
                               placeholder="Edite el Correo Electrónico" maxlength="100" minlength="0">
                        <div id="lab_correo" style="color:red; margin-left:9px; display:none"></div>       
					</div>
					<div class="col-md-5">
                        <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							     contacto</span>
							 </label>
                        <input style="padding-top: 5px;margin-right:-20px;margin-top: 0px;" 
                               type="text" name="m_contacto" id="m_contacto" class="form-control" 
                               placeholder="Edite el Contacto" maxlength="100" minlength="0">
					</div>
				</div>	

				<div class="row">
					<div class="col-md-12">
                        <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							     Dirección (Ubicación Física)</span>
							 </label>
                        <textarea style="padding-top: 5px;margin-right:-20px;margin-top: 0px;" 
                               name="m_direccion" id="m_direccion" class="form-control" 
                               rows="2"></textarea>

					</div>
				</div>	


                <br>  
				<span style="color:#ff0000; font-size:16px; text-align:left">Campo obligatorio (*)</span>
			  </div>				

            <input type="hidden" id="id_mod" name="id_mod">
            <input type="hidden" id="nombre_orig" name="nombre_orig">
           <!-- <input type="hidden" id="idEmpresa_orig" name="idEmpresa_orig">-->

            <input type="hidden" id="val_error" name="val_error" value="0">
            <input type="hidden" id="accion_formulario" name="accion_formulario" value="0">

			



			  
			</div>
			<div class="modal-footer">

                   
				<button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrar01">Cerrar</button>


		        <button type="button" class="btn btn-primary" onclick="actualizar_form()" id="guardar01">
		              Guardar
		        </button>

			</div>
			</form>  


		</div>
	</div>
</div>



<!-- Modal -->
<div class="modal fade" id="mod_ver" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content" style="width:900px; margin-left:-130px">
			<div class="modal-header">
				<h4 class="modal-title" id="exampleModalLabel" style="color:blue">
					Datos de Sucursal
				</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				
			<form name="formularioVer" method="post" id="formularioVer" autocomplete="off">	
				
				
			  <div class="container">
				<div class="row">
					<div class="col-md-8">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							     Nombre <span style="color:#ff0000;">*</span></span>
							 </label>
                        <input style="padding-top: 5px;margin-right:-20px;margin-top: 0px;" 
                               type="text" name="v_nombre" id="v_nombre" class="form-control" 
                               disabled="true">
					</div>
					
					<div class="col-md-4">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							     RUT <span style="color:#ff0000;">*</span></span>
							 </label>
                        <input style="padding-top: 5px;margin-right:-20px;margin-top: 0px;" 
                               type="text" name="v_rut" id="v_rut" class="form-control" 
                               disabled="true">
					</div>
				</div>	

				<div class="row" style="padding-top: 10px">
					<div class="col-md-12">
                         <label style="padding-top: 0px; margin-top: 0px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">Empresa Asociada <span style="color:#ff0000;">*</span></span>
						 </label>
                        <input style="padding-top: 5px;margin-right:-20px;margin-top: 0px;" 
                               type="text" name="v_empresa" id="v_empresa" class="form-control" 
                               disabled="true">
					</div>
				</div>	


			    

				<div class="row" style="padding-top: 10px">
					<div class="col-md-6">
                         <label style="padding-top: 0px; margin-top: 0px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">Región <span style="color:#ff0000;">*</span></span>
						 </label>
                        <input style="padding-top: 5px;margin-right:-20px;margin-top: 0px;" 
                               type="text" name="v_region" id="v_region" class="form-control" 
                               disabled="true">
					</div>
					<div class="col-md-3">
                         <label style="padding-top: 0px; margin-top: 0px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">Teléfono Local</span>
						 </label>
                        <input style="padding-top: 5px;margin-right:-20px;margin-top: 0px;" 
                               type="text" name="v_telefono_local" id="v_telefono_local" 
                               disabled="true">
					</div>
					<div class="col-md-3">
                         <label style="padding-top: 0px; margin-top: 0px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">Teléfono Celular
							 </span>
						 </label>
                        <input style="padding-top: 5px;margin-right:-20px;margin-top: 0px;" 
                               type="text" name="v_telefono_celular" id="v_telefono_celular" 
                               disabled="true">
					</div>
				</div>	

				<div class="row">
					<div class="col-md-7">
                        <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							     Correo</span>
							 </label>
                        <input style="padding-top: 5px;margin-right:-20px;margin-top: 0px;" 
                               type="text" name="v_correo" id="v_correo" class="form-control" 
                               disabled="true">
					</div>
					<div class="col-md-5">
                        <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							     contacto</span>
							 </label>
                        <input style="padding-top: 5px;margin-right:-20px;margin-top: 0px;" 
                               type="text" name="v_contactoV" id="v_contacto" class="form-control" 
                               disabled="true">
					</div>
				</div>	

				<div class="row">
					<div class="col-md-12">
                        <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							     Dirección (Ubicación Física)</span>
							 </label>
                        <textarea style="padding-top: 5px;margin-right:-20px;margin-top: 0px;" 
                               name="v_direccion" id="v_direccion" class="form-control" 
                               rows="2" disabled="true"></textarea>

					</div>
				</div>	


                <br>  
				<span style="color:#ff0000; font-size:16px; text-align:left">Campo obligatorio (*)</span>
			  </div>				
			</div>
			<div class="modal-footer">

                   
				<button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrar01">Cerrar</button>


			</div>



			</form>  

		</div>
	</div>
</div>


