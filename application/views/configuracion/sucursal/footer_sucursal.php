


<!--cargar datatable-->
<script>
    $(document).ready(function(){
		 cargarDataTable(0);
	});





function cargarDataTable(id){
    $.ajax({
		url : "<?php echo site_url('Cf_sucursal/ajax_datatable')?>/"+id,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			 document.getElementById('bodyTabla').innerHTML = data.registro;
			 $(document).ready(function() {
				$('#basic-datatables').DataTable({
			    });	
			 });

		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}


//cargar combos



function add_form(){
	$('#formulario')[0].reset(); // reset form on modals
   
    document.getElementById('accion_formulario').value="1";
    document.getElementById('titulo_mostrar').innerHTML = "Incluir Sucursal";
    //document.getElementById('url_mostrar').style.display = "inline";   

	document.getElementById('guardar01').disabled=false;
	document.getElementById('cerrar01').disabled=false;     

	 document.getElementById('lab_nombre').style.display = "none";
     //document.getElementById('lab_empresa').style.display = "none";
     document.getElementById('lab_region').style.display = "none";


	 document.getElementById('lab_rut').style.display = "none";
     document.getElementById('lab_correo').style.display = "none";

	 document.getElementById('nombre_orig').value = "";
	 //document.getElementById('idEmpresa_orig').value = "";

	 //document.getElementById('valEmpresa').style.display = "inline"; 

	//ruta = "<?php echo site_url('Combo_ctrl/ajax_combo_empresa')?>";
	//cbo_empresa(ruta,0);   

	ruta = "<?php echo site_url('Combo_ctrl/ajax_combo_region')?>";
	cbo_region(ruta,0);   

    document.getElementById('nombre_orig').value = "";
    //document.getElementById('idEmpresa_orig').value = "";

    $('#mod_modal').modal('show');
	
}


function modificar_form(id){

    //cbo_modelo(ruta,data.id_proyecto_local,data.id_proyecto_local);   //Llenar combo    

    document.getElementById('accion_formulario').value="2";
    document.getElementById('titulo_mostrar').innerHTML = "Modificar Sucursal";

	document.getElementById('guardar01').disabled=false;
	document.getElementById('cerrar01').disabled=false;       

    document.getElementById('valEmpresa').style.display = "none"; 
    //document.getElementById('url_mostrar').style.display = "none";

    $.ajax({
		url : "<?php echo site_url('Cf_sucursal/ajax_modificar')?>/"+id,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){

			 document.getElementById('m_nombre').value = data.nombre;
			 document.getElementById('m_rut').value = data.rut;			 
			 document.getElementById('m_contacto').value = data.contacto;
			 document.getElementById('m_telefono_local').value = data.telefono_local;
			 document.getElementById('m_telefono_celular').value = data.telefono_celular;
			 document.getElementById('m_correo').value = data.correo;
			 document.getElementById('m_direccion').value = data.direccion;
			 document.getElementById('id_mod').value = data.id;
			 document.getElementById('val_error').value = "0";
			 document.getElementById('lab_nombre').style.display = "none";
             document.getElementById('lab_region').style.display = "none";
             document.getElementById('lab_correo').style.display = "none";
             document.getElementById('lab_rut').style.display = "none";
			 document.getElementById('nombre_orig').value = data.nombre;
			 //document.getElementById('idEmpresa_orig').value = data.id_empresa;
			 ruta = "<?php echo site_url('Combo_ctrl/ajax_combo_region')?>";
	         cbo_region(ruta,data.id_region);   //Llenar combo    
             $('#mod_modal').modal('show');

		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}



function actualizar_form(){
    accion = document.getElementById('accion_formulario').value;
    if(accion==1){
        guardar_add_form();
    }else{
        guardar_mod_form();
    } 


}




function ver_form(id){


    $.ajax({
		url : "<?php echo site_url('Cf_sucursal/ajax_ver')?>/"+id,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			 document.getElementById('v_nombre').value = data.nombre;
			 document.getElementById('v_rut').value = data.rut;			 
			 document.getElementById('v_contacto').value = data.contacto;
			 document.getElementById('v_telefono_local').value = data.telefono_local;
			 document.getElementById('v_telefono_celular').value = data.telefono_celular;
			 document.getElementById('v_correo').value = data.correo;
			 document.getElementById('v_direccion').value = data.direccion;
			 document.getElementById('v_empresa').value = data.empresa;
			 document.getElementById('v_region').value = data.region;

             $('#mod_ver').modal('show');

		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}





function guardar_add_form(){

	//validar campos formulario
	formulario = "formulario";
	error=0;
	error = validar_form(formulario,1);
    if(error==0){
		document.getElementById('guardar01').disabled=true;
		document.getElementById('cerrar01').disabled=true;
    }



	if(error == 0){
        $.ajax({
            url : "<?=base_url();?>Cf_sucursal/ajax_guardar_add",
            type: "POST",
            data: $('#formulario').serialize(),
            dataType: "JSON",
            success: function(data)
            {
            	if(data.status==0){ 
					 document.getElementById('bodyTabla').innerHTML = data.registro;
					 $('#mod_modal').modal('hide');

					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
				 	});            	


					swal("Registro almacenado Correctamente!", "", {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					});
				}else{
					document.getElementById('lab_nombre').style.display = "inline";
					document.getElementById('lab_nombre').innerHTML = "El Nombre ingresado, ya se encuentra registrado para esta empresa.";
			    	document.getElementById('guardar01').disabled=false;
					document.getElementById('cerrar01').disabled=false; 					
				
					swal("No se actualizo el registro", "", {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});

				}	
	        },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error Insertando / modificanco registro, Comuniquese con el administrador');
            }
        });
    }else{
		swal("No se actualizo el registro", "", {
			icon : "error",
			buttons: {        			
				confirm: {
					className : 'btn btn-danger'
				}
			},
		});
    }    

} //Fin de add form	

function guardar_mod_form(){

	//validar campos formulario
	formulario = "formulario";
	error=0;
	error = validar_form(formulario,2);

    if(error==0){
		document.getElementById('guardar01').disabled=true;
		document.getElementById('cerrar01').disabled=true;
    }	

	if(error == 0){
        $.ajax({
            url : "<?=base_url();?>Cf_sucursal/ajax_guardar_mod",
            type: "POST",
            data: $('#formulario').serialize(),
            dataType: "JSON",
            success: function(data)
            {
            	if(data.status==0){ 
					 document.getElementById('bodyTabla').innerHTML = data.registro;
					 $('#mod_modal').modal('hide');

					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
				 	});            	


					swal("Registro almacenado Correctamente!", "", {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					});
				}else{
					document.getElementById('lab_nombre').style.display = "inline";
					document.getElementById('lab_nombre').innerHTML = "El Nombre ingresado, ya se encuentra registrado para esta empresa.";
					document.getElementById('guardar01').disabled=false;
					document.getElementById('cerrar01').disabled=false;
					swal("No se actualizo el registro", "", {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});

				}	
	        },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error Insertando / modificanco registro, Comuniquese con el administrador');
            }
        });
    }else{
		swal("No se actualizo el registro", "", {
			icon : "error",
			buttons: {        			
				confirm: {
					className : 'btn btn-danger'
				}
			},
		});
    }    
}







function eliminar_form(id){
	swal({
		title: 'Esta seguro de ?',
			type: 'warning',
			buttons:{
				cancel: {
					visible: true,
						text : 'No, Cancelar!',
								className: 'btn btn-danger'
				},        			
				confirm: {
					text : 'Si, Desactivar!',
					className : 'btn btn-success'
				}
			}
	}).then((willDelete) => {
		if (willDelete) {
		    $.ajax({
				url : "<?php echo site_url('Cf_sucursal/ajax_desactivar')?>/"+id,
				type: "GET",                     
				dataType: "JSON",
				success: function(data){
					 document.getElementById('bodyTabla').innerHTML = data.registro;
					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
					 }); 
				},
				error: function (jqXHR, textStatus, errorThrown){
					alert('Error consultando la Base de Datos');
				}
			});
			swal("El registro fué desactivado!", {
				icon: "success",
				buttons : {
					confirm : {
						className: 'btn btn-success'
					}
				}
			});
		}
	});

}

function reactivar_form(id){
	swal({
		title: 'Esta seguro de ?',
			type: 'warning',
			buttons:{
				cancel: {
					visible: true,
						text : 'No, Cancelar!',
								className: 'btn btn-danger'
				},        			
				confirm: {
					text : 'Si, Reactivar!',
					className : 'btn btn-success'
				}
			}
	}).then((willDelete) => {
		if (willDelete) {
		    $.ajax({
				url : "<?php echo site_url('Cf_sucursal/ajax_reactivar')?>/"+id,
				type: "GET",                     
				dataType: "JSON",
				success: function(data){
					 document.getElementById('bodyTabla').innerHTML = data.registro;
					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
					 }); 
				},
				error: function (jqXHR, textStatus, errorThrown){
					alert('Error consultando la Base de Datos');
				}
			});
			swal("El registro fué reactivado!", {
				icon: "success",
				buttons : {
					confirm : {
						className: 'btn btn-success'
					}
				}
			});
		}
	});
}







function validar_form(formulario,accion){
//accion 1= incluir
//accion 2= modificar

error=0;

objeto = "m_nombre";
campo = document.forms[formulario].elements[objeto].value;
campo = campo.replace(/(^\s*)|(\s*$)/g,"");
nombre = campo;

document.getElementById('lab_nombre').style.display = "none";

if (campo.length==0){
	document.getElementById('lab_nombre').style.display = "inline";
    document.getElementById('lab_nombre').innerHTML = "Edite el nombre"; 	
    error = 1;
}


objeto = "m_rut";
campo = document.forms[formulario].elements[objeto].value;
campo = campo.replace(/(^\s*)|(\s*$)/g,"");

document.getElementById('lab_rut').style.display = "none";

if (campo.length==0){
	document.getElementById('lab_rut').style.display = "inline";
    document.getElementById('lab_rut').innerHTML = "Edite el RUT"; 	
    error = 1;
}


/*if(accion != 2){
	objeto = "id_cbo_empresa";
	campo = document.forms[formulario].elements[objeto].value;
	document.getElementById('lab_empresa').style.display = "none";
	if (campo==0){
		document.getElementById('lab_empresa').style.display = "inline";
	   	document.getElementById('lab_empresa').innerHTML = "Seleccione la Empresa"; 	
	   	error = 1;
	}
}
*/

objeto = "id_cbo_region";
campo = document.forms[formulario].elements[objeto].value;
document.getElementById('lab_region').style.display = "none";
if (campo==0){
	document.getElementById('lab_region').style.display = "inline";
   	document.getElementById('lab_region').innerHTML = "Seleccione Región"; 	
   	error = 1;
}


objeto = "m_correo";
campo = document.forms[formulario].elements[objeto].value;
document.getElementById('lab_correo').style.display = "none";
if(campo.length!=0){
    if (/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(campo)){
	    x=0;
	}else{
		document.getElementById('lab_correo').style.display = "inline";
        document.getElementById('lab_correo').innerHTML = "El Formato del Correo Electronico es incorrecto";
        error = 1;
	}

}


return error;
}



</script>
