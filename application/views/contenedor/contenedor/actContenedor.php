<?php
$incluir = base_url() . "assets/images/nuevo.png";
$asterisco = ' style="color=red; font-size:16px"';

?>
        <br>            
        <div class="main-panel">
            <div class="content">
                <div class="page-inner mt--5">
                	<div class="col-md-12">
						<div class="card">
							<div class="card-body">
								
								<h1><span style="color:blue; text-align:left">Contenedor de sabores de sushi</span></h1> 
								<div style="text-align:right">
									<a href="javascript:void(0)" class="btn btn-warning btn-sm item_edit2"  
										style="border-radius: 15px; font-size:16px; font-weight:600;
										height:35px;" onclick="javascript:resetearContenedor()">Resetear contenedores</a>
								</div>		
								<hr>
								<div class="table-responsive">
									<div id="bodyTabla"></div>
								</div>
							</div>
						</div>
                    </div>    
                </div>
            </div>
        </div>    



<!-- Modal -->
<div class="modal fade" id="resetear_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content" style="width:800px; margin-left:-100px">

            <div class="modal-header bg-primary text-white">
                <h4 class="modal-title" id="exampleModalLabel">
                    <div style="font-weight:700">Actualizar cantidad del contenedor</div>
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color:white; font-size:30px">&times;</span>
                </button>
            </div>

			<div class="modal-body">
			<form name="formulario" method="post" id="formulario" autocomplete="off">	
			  <div class="container">
				<div class="row" style="padding-top: 5px">
					<div class="col-md-3">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							     Cantidad </span>
						</label>
       
					</div>

					<div class="col-md-3">
						<input style="padding-top: 5px;margin-right:-20px;margin-top: 12px;text-align:right" 
							   type="text" name="m_cantidad" id="m_cantidad" class="form-control" 
							   maxlength="100" 
							   minlength="0" onkeyup="nro_entero('m_cantidad');">
					</div>
				</div>
					
				<input type="text" name="no_tocar" id="no_tocar" style="border: 0">
				<input type="hidden" id="idContenedor" name="idContenedor">
			  </div>				
            </form>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrar02"><span style="font-weight: 700">Cerrar</span></button>
		        <button type="button" class="btn btn-primary" onclick="guardar_form()" id="guardar02">
		              <span style="font-weight: 700">Guardar</span>
		        </button>
			</div>
		</div>
	</div>
</div>