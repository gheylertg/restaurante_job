



<!--cargar datatable-->
<script>
    $(document).ready(function(){
		 cargarDataTable(0);
	});

function nro_entero(objeto){
//valida si el nro es entero sin caracteres diferentes a numero
//formulario = nombre del formulario origen
//Objeto = nombre del campo u objeto

var valor = document.getElementById(objeto).value;
var tamano=valor.length;
i=0;
cad_numero="";
while(i<tamano){
    var numero = valor.charAt(i);
    if (isNaN(numero)==false)
        cad_numero=cad_numero+numero
    i=i+1;    
}
document.getElementById(objeto).value = cad_numero;
}



function cargarDataTable(id){
    $.ajax({
		url : "<?php echo site_url('Co_contenedor/ajax_datatable')?>/"+id,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			 document.getElementById('bodyTabla').innerHTML = data.registro;
			 $(document).ready(function() {
				$('#basic-datatables').DataTable({
			    });	
			 });

		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}


function resetearContenedor(){
	swal({
		title: 'Esta seguro de Resetear los contenedores de Sabores e Sushi?',
			type: 'warning',
			buttons:{
				cancel: {
					visible: true,
						text : 'No, Cancelar!',
								className: 'btn btn-danger'
				},        			
				confirm: {
					text : 'Si, Resetear!',
					className : 'btn btn-success'
				}
			}
	}).then((willDelete) => {
		if (willDelete) {
		    $.ajax({
				url : "<?php echo site_url('Co_contenedor/ajax_resetear_contenedores')?>",
				type: "GET",                     
				dataType: "JSON",
				success: function(data){
                    document.getElementById('bodyTabla').innerHTML = data.registro;
					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
						});	
					 });                    
				},
				error: function (jqXHR, textStatus, errorThrown){
					alert('Error consultando la Base de Datos');
				}
			});
			swal("Los contenedores fueron reseteado!", {
				icon: "success",
				buttons : {
					confirm : {
						className: 'btn btn-success'
					}
				}
			});
		}
	});
}

function actContenedor(idContenedor){
	document.getElementById('guardar02').disabled=false;
	document.getElementById('cerrar02').disabled=false;       

    $.ajax({
		url : "<?php echo site_url('Co_contenedor/ajax_modificar')?>/"+idContenedor,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			 document.getElementById('idContenedor').value = data.id;
			 document.getElementById('m_cantidad').value = data.cantidad;
             $('#resetear_modal').modal('show');
		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}


function guardar_form(){
	//validar campos formulario
	formulario = "formulario";
	error=0;
	error = validar_form(formulario,2);

	if(error == 0){
		document.getElementById('guardar02').disabled=true;
		document.getElementById('cerrar02').disabled=true;
        $.ajax({
            url : "<?=base_url();?>Co_contenedor/ajax_guardar_mod",
            type: "POST",
            data: $('#formulario').serialize(),
            dataType: "JSON",
            success: function(data)
            {
            	if(data.status==0){ 
					 document.getElementById('bodyTabla').innerHTML = data.registro;
					 $('#resetear_modal').modal('hide');

					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
				 	});            	


					swal("Registro almacenado Correctamente!", "", {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					});
				}	
	        },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error Insertando / modificanco registro, Comuniquese con el administrador');
            }
        });
    }else{
		swal("No se actualizo el registro", "", {
			icon : "error",
			buttons: {        			
				confirm: {
					className : 'btn btn-danger'
				}
			},
		});
    }    
}

function validar_form(formulario,accion){
//accion 1= incluir
//accion 2= modificar

error=0;

objeto = "m_cantidad";
campo = document.forms[formulario].elements[objeto].value;
if (campo.length==0){
	document.getElementById('m_cantidad').value="0";
}
return error;
}









</script>
