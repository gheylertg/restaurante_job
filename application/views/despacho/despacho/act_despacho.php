<?php
$incluir = base_url() . "assets/images/nuevo.png";
$mesonero = base_url() . "assets/images/despacho/mesonero01.jpeg";
$retiro = base_url() . "assets/images/despacho/para_llevar01.png";

$asterisco = ' style="color=red; font-size:16px"';
$ci = &get_instance();
$ci->load->library('Clssession');


?>

	<br>            
	<div class="main-panel">
		<div class="content">
			<div class="page-inner mt--5">
				<div class="row row-card-no-pd mt--2">
					<div class="col-sm-12 col-md-12">
						<div class="card card-stats card-round">
							<div class="card-body" style="margin-bottom:-30px;">
								 <h2><span style="color:blue">Despachos</span></h2>
								 <h5>
									<span style="color:blue">
									<b><div id="nombreEmpresa" name="nombreEmpresa"></div></b></span>
								 </h5>
								 

                                <div class="row">
                                <div class="col-md-10">	
								<button class="btn btn-primary btn-border btn-round" 
								        onclick="actDespachoActual('1')" id="tipo_1">
									<span style="font-weight:600; font-size:16px">Despacho en mesa</span>&nbsp;&nbsp;
									<i class="fa fa-bell"></i>&nbsp;
                            			<span class="notification">
                            				<span id="nroDespachoMesa"></span>
                            			</span>     
                            		</span>	
								</button>


								<button class="btn btn-primary btn-border btn-round"  onclick="actDespachoActual('2')" id="tipo_2">
									<!--<span>
										<img src="<?php //echo $retiro;?>" style="width:30px; height:30px" title="Retiro en tienda" alt="Retiro en tienda">
									</span>-->											
									
									<span style="font-weight:600; font-size:16px">Retiro en tienda</span>&nbsp;&nbsp;
									<i class="fa fa-bell"></i>&nbsp;
                            			<span class="notification"><span id="nroDespachoRetiro"></span></span>     
                            	</button>		

                                <button class="btn btn-primary btn-border btn-round" onclick="actDespachoActual('3')" id="tipo_3">
										<span class="btn-label"><i class="fas fa-shipping-fast"></i></span>
										<span style="font-weight:600; font-size:16px">&nbsp;&nbsp;Delivery&nbsp;&nbsp;&nbsp;<i class="fa fa-bell"></i>&nbsp;&nbsp;&nbsp;<span class="notification"><span id="nroDespachoDelivery"></span></span>
								</button>
								</div>
								<div class="col-md-2" style="text-align: right">	
	                                <button class="btn btn-success btn-round" onclick="cobroCaja()">
											<span class="btn-label"><i class="fas fa-money-check-alt"></i></span>
											<span style="font-weight:600; font-size:16px">Ir a caja&nbsp;</span>
									</button>


								</div>	




								</div>
								<br>	


								<input type="hidden" id="tipoDespacho">
							</div>

						</div>
					</div>
				</div>
				
				<div class="row" style="margin-top:-20px">
					<div class="col-md-12">
						<div class="card">
							<div class="card-body">
								<div class="table-responsive">
	                                <div  style="text-align:right">

                                <button class="btn btn-warning btn-round" onclick="despacho()">
										<span style="font-weight:600; font-size:16px">&nbsp;&nbsp;Generar Despacho&nbsp;&nbsp;&nbsp;</span>
								</button>

	                                </div>
	                                <br>

									<div id="bodyTabla"></div>
								</div>
							</div>
						</div>
					</div>    
				</div>
			</div>
		</div>
	</div>    



<!---    Comienzo despacho en mesa    ---->

<div class="modal fade" id="generarDespacho_DM_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content" style="width:900px; margin-left:-140px">
			<div class="modal-header" style="background-color:#00f;">
				<h4 class="modal-title" id="exampleModalLabel" style="color:white">
					<scan>Generar despacho en mesa</scan>
				</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true" style="color:white; font-weight: 600">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				
			<form name="dm_formulario" method="post" id="dm_formulario" autocomplete="off">	
				
				
			  <div class="container">

				<div class="row">
  				    <div class="col-md-3" >
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">Mesonero <span style="color:#ff0000;">*</span></span>
						 </label>
					</div>
					<div class="col-md-9">	 
						<div id="cbo_mesonero" style="padding-top: 0px; margin-top: 14px;margin-left:0px;"></div>
						<div id="lab_mesonero" style="color:red; margin-left:9px; display:none"></div>
					</div>
				</div>



				<div class="row">
  				    <div class="col-md-3" >
                         <label style="padding-top: 5px; margin-top: 14px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">Sala <span style="color:#ff0000;">*</span></span>
						 </label>
					</div>
					<div class="col-md-9">	 
						<div id="cbo_sala" style="padding-top: 0px; margin-top: 14px;margin-left:0px;"></div>
						<div id="lab_sala" style="color:red; margin-left:9px; display:none"></div>
					</div>
				</div>


				<div class="row">
  				    <div class="col-md-3" >
                         <label style="padding-top: 5px; margin-top: 14px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">Mesa <span style="color:#ff0000;">*</span></span>
						 </label>
					</div>
					<div class="col-md-9">	 
						<div id="cbo_anid_mesa" style="padding-top: 0px; margin-top: 14px;margin-left:0px;"></div>
						<div id="lab_mesa" style="color:red; margin-left:9px; display:none"></div>
					</div>
				</div>



				<span style="color:#ff0000; font-size:16px; text-align:left">Campo obligatorio (*)</span>
			  </div>				
			</div>
			<div class="modal-footer">
                   
				<button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrar01">Cerrar</button>

		        <button type="button" class="btn btn-primary" onclick="generar_despachoDM()" id="guardar01">
		              Generar Despacho
		        </button>				
			</div>


			<input type="hidden" id="id_mod_reverso" name="id_mod_reverso">

			</form>  

		</div>
	</div>
</div>




<!---    Comienzo Retiro en tienda    ---->

<div class="modal fade" id="generarRT_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content" style="width:900px; margin-left:-140px">
			<div class="modal-header" style="background-color:#00f;">
				<h4 class="modal-title" id="exampleModalLabel" style="color:white">
					<scan>Generar despacho Retiro En Tienda</scan>
				</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true" style="color:white; font-weight: 600">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				
			<form name="rt_formulario" method="post" id="rt_formulario" autocomplete="off">	
				
				
			  <div class="container">



				<div class="row">
  				    <div class="col-md-3" >
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">Nombre del cliente <span style="color:#ff0000;">*</span></span>
						 </label>
					</div>
					<div class="col-md-9">	 
                        <input style="padding-top: 5px;margin-right:-20px;margin-top: 12px;" 
                               type="text" name="m_nombre" id="m_nombre" class="form-control" 
                               placeholder="Edite el nombre del Cliente" maxlength="100" 
                               minlength="0">
                        <div id="lab_nombre" style="color:red; margin-left:9px; display:none"></div>       
					</div>
				</div>

				<div class="row">
  				    <div class="col-md-3" >
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">Teléfono del cliente</span>
						 </label>
					</div>
					<div class="col-md-9">	 
                        <input style="padding-top: 5px;margin-right:-20px;margin-top: 12px;" 
                               type="text" name="m_telefono" id="m_telefono" class="form-control" 
                               placeholder="Edite el teléfono del Cliente" maxlength="100" 
                               minlength="0">
					</div>
				</div>

				<div class="row">
  				    <div class="col-md-3" >
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">Dirección del cliente</span>
						 </label>
					</div>
					<div class="col-md-9">	 
                        <textarea class="form-control" id="m_direccion" 
                                  name="m_direccion" rows="2"></textarea>
                    </div>
				</div>

				<div class="row">
  				    <div class="col-md-3" >
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">Descripción</span>
						 </label>
					</div>
					<div class="col-md-9">	 
                        <textarea class="form-control" id="m_descripcion" 
                                  name="m_descripcion" rows="2"></textarea>
                    </div>
				</div>



                <br> 
				<span style="color:#ff0000; font-size:16px; text-align:left">Campo obligatorio (*)</span>
			  </div>				
			</div>
			<div class="modal-footer">
                   
				<button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrar02">Cerrar</button>

		        <button type="button" class="btn btn-primary" onclick="generar_despachoRT()" id="guardar02">
		              Generar Despacho
		        </button>				
			</div>

			</form>  

		</div>
	</div>
</div>




<!---    Comienzo Delivery    ---->

<div class="modal fade" id="generarDL_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content" style="width:900px; margin-left:-140px">
			<div class="modal-header" style="background-color:#00f;">
				<h4 class="modal-title" id="exampleModalLabel" style="color:white">
					<scan>Generar despacho Delivery</scan>
				</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true" style="color:white; font-weight: 600">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				
			<form name="dl_formulario" method="post" id="dl_formulario" autocomplete="off">	
				
				
			  <div class="container">



				<div class="row">
  				    <div class="col-md-3" >
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">Nombre del cliente <span style="color:#ff0000;">*</span></span>
						 </label>
					</div>
					<div class="col-md-9">	 
                        <input style="padding-top: 5px;margin-right:-20px;margin-top: 12px;" 
                               type="text" name="d_nombre" id="d_nombre" class="form-control" 
                               placeholder="Edite el nombre del Cliente" maxlength="100" 
                               minlength="0">
                        <div id="lab_nombre_dl" style="color:red; margin-left:9px; display:none"></div>       
					</div>
				</div>

				<div class="row">
  				    <div class="col-md-3" >
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">Teléfono del cliente<span style="color:#ff0000;">*</span></span>
						 </label>
					</div>
					<div class="col-md-9">	 
                        <input style="padding-top: 5px;margin-right:-20px;margin-top: 12px;" 
                               type="text" name="d_telefono" id="d_telefono" class="form-control" 
                               placeholder="Edite el teléfono del Cliente" maxlength="100" 
                               minlength="0">
                        <div id="lab_telefono_dl" style="color:red; margin-left:9px; display:none"></div>       
					</div>
				</div>

				<div class="row">
  				    <div class="col-md-3" >
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">Dirección del cliente<span style="color:#ff0000;">*</span></span>
						 </label>
					</div>
					<div class="col-md-9">	 
                        <textarea class="form-control" id="d_direccion" 
                                  name="d_direccion" rows="2"></textarea>
                        <div id="lab_direccion_dl" style="color:red; margin-left:9px; display:none"></div>          
                    </div>

				</div>

				<div class="row">
  				    <div class="col-md-3" >
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">Descripción</span>
						 </label>
					</div>
					<div class="col-md-9">	 
                        <textarea class="form-control" id="d_descripcion" 
                                  name="d_descripcion" rows="2"></textarea>
                    </div>
				</div>



                <br> 
				<span style="color:#ff0000; font-size:16px; text-align:left">Campo obligatorio (*)</span>
			  </div>				
			</div>
			<div class="modal-footer">
                   
				<button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrar03">Cerrar</button>

		        <button type="button" class="btn btn-primary" onclick="generar_despachoDL()" id="guardar03">
		              Generar Despacho
		        </button>				
			</div>

			</form>  

		</div>
	</div>
</div>


<!---   Reverso de despacho ---->

<div class="modal fade" id="reverso_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content" style="width:900px; margin-left:-140px">

            <div class="modal-header bg-primary text-white">
                <h4 class="modal-title" id="exampleModalLabel">
                    <div id="r_titulo_mostrar" style="font-weight:700"></div>
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color:white; font-size:30px">&times;</span>
                </button>
            </div>

			<div class="modal-body">
				
			<form name="rev_formulario" method="post" id="rev_formulario" autocomplete="off">	
			  <div class="container">

				<div class="row">
					<div class="col-md-3">
                        <label style="padding-top: 0px; margin-top: 10px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:14px; font-weight:600">
							 	Código Venta</span>
						</label>
						<input type="text" name="r_codigo" id="r_codigo" class="form-control"
	                           readonly="true">
					</div>
					
					<div class="col-md-9">
                         <label style="padding-top: 0px; margin-top: 10px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:14px; font-weight:600">
							 	Tipo Despacho</span>
						 </label>
						<input type="text" name="r_tipo" id="r_tipo" class="form-control"
	                           readonly="true">
					</div>
				</div>	


			  	<hr>
                <div class="row" style="padding-top: 10px">
                    <div class="col-md-3">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
                             <span style="color:#0000ff; font-size:16px; font-weight:600">
                                 Motivo Reverso <span style="color:#ff0000;">*</span></span>
                             </label>
                    </div>
                    <div class="col-md-9">
                        <textarea style="padding-top: 5px;margin-right:-20px;margin-top: 0px;" 
                               name="r_motivo" id="r_motivo" 
                               class="form-control" rows="3"></textarea>
                        <div id="lab_motivo" style="color:red; margin-left:9px; display:none"></div>       
                    </div>
				</div>				

                <br>
				<span style="color:#ff0000; font-size:16px; text-align:left">Campo obligatorio (*)</span>
			  </div>				
			</div>
			<div class="modal-footer">
                   
				<button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrar04"><span style="font-weight: 700">Cerrar</span></button>

		        <button type="button" class="btn btn-primary" onclick="guardar_reversarDespacho()" id="guardar04">
		              <span style="font-weight: 700">Reversar</span> 
		        </button>				
			</div>


			<input type="hidden" id="id_reverso" name="id_reverso">

			</form>  

		</div>
	</div>
</div>
