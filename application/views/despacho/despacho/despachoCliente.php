<?php

$regresar = base_url() . "assets/images/regresar01.jpeg";

$asterisco = ' style="color=red; font-size:16px"';
$ci = &get_instance();
$ci->load->library('Clssession');

?>

	<br>            
	<div class="main-panel">
		<div class="content">
			<div class="page-inner mt--5">
				<div class="row row-card-no-pd mt--2">
					<div class="col-sm-12 col-md-12">
						<div class="card card-stats card-round">
							<div class="card-body" style="margin-bottom:-30px;">
								 <h2><span style="color:blue"><div id="titulo"></div></span></h2>
								 <h4>
									<span style="">
									<b><div id="datoDespacho" name="datoDespacho"></div></b></span>
								 </h4>
							</div>
						</div>
					</div>
				</div>

                <div class="row">
                    <div class="col-md-4">
						
						<div class="card card-stats card-round">
							<div class="card-body" style="margin-bottom:-30px; height:110px">
								<button class="btn btn-success btn-border btn-round" onclick="regresarDespacho()">
									<span style="font-weight:600; font-size:14px">Regresar</span>
								</button>
								
								<div id="boton02" style="display:none">
								<button class="btn btn-warning btn-border btn-round" onclick="precuentaDespacho(0)" id="preCuenta">
									<span style="font-weight:600; font-size:14px">Pre cuenta</span>
								</button>
								</div>
								
								<div id="boton03" style="display:none">
								<button class="btn btn-primary btn-border btn-round" onclick="cerrarDespacho(0)" id="cerrarDespacho">
									<span style="font-weight:600; font-size:14px">Cerrar Cuenta</span>
								</button>
								</div>
								
							</div>
						</div>	
						
						
						
						<div class="card card-stats card-round">
							<div class="card-body" style="margin-bottom:-30px;">
								 <h2><span style="color:blue">Pedido </span></h2>
								 <div id="factura"></div>
								 <br>
							</div>
						</div>	
						
						<div class="card card-stats card-round">
							<div class="card-body" style="margin-bottom:-30px;">
                                <table>
                                <tr>
                                	<td style="width:30%"><h2><span style="color:blue">Precio:</span></h2></td>
                                	<td style="width:10%; text-align: right"><h2><span style="color:blue" id="precioF"></span></h2></td>	
                                </tr>
                                <tr>
                                   <td style="width:30%"><h2><span style="color:blue">Impuesto (<span id="porcentajeI"></span>%):</span></h2></td>
                                	<td style="width:10%; text-align: right"><h2><span style="color:blue" id="impuestoF"></span></h2></td>	
                                </tr>
                                
                                <tr>
                                   <td style="width:30%"><h2><span style="color:blue">Total:</span></h2></td>
                                	<td style="width:10%; text-align: right"><hr><h2><span style="color:blue" id="totalF"></span></h2></td>	
                                </tr>
                                </table>	
 							    <br>
							</div>
						</div>	
                         
                    </div> 

                    <div class="col-md-8">
                    
                    	<div id="categoria"></div> 
                    	
                    	<div id="alimento"></div> 
                    </div> 


                </div>	


			</div>
		</div>
	</div>    




<!---    Comienzo despacho en mesa    ---->

<div class="modal fade" id="generar_sushi_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content" style="width:1000px; margin-left:-180px">
			 <div class="modal-header bg-primary text-white">
				<h4 class="modal-title" id="exampleModalLabel" style="color:white">
					<span style="font-weight:700">Preparar Sushi</span>
				</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true" style="color:white; font-weight: 700">&times;</span>
				</button>
			</div>
			<div class="modal-body">
			<form name="sushi_formulario" method="post" id="sushi_formulario" autocomplete="off">
				<div class="container">
			  	

				  	<hr>
					<div class="row">	
						<div class="col-md-12">	 
							<label style="padding-top: 0px; margin-top: 0px;margin-left:0px;">
								 <span style="color:#0000ff; font-size:18px; font-weight:700">Presentación</span><span style="font-size:14px; font-weight:400">&nbsp;&nbsp;Elige 1</span>
							 </label>
							 <br>
							<div id="presentacion" style="padding-top: 0px; margin-top: 0px;margin-left:0px;"></div>
							<div id="lab_presentacion" style="color:red; margin-left:9px; display:none; font-size: 16px"></div>
						</div>



					</div>
	                <hr>
					<div class="row">
	  				    <div class="col-md-12" >
							<label style="padding-top: 0px; margin-top: 0px;margin-left:0px;">
								 <span style="color:#0000ff; font-size:18px; font-weight:700">Envoltura</span><span style="font-size:14px; font-weight:400">&nbsp;&nbsp;Elige 1</span>
							 </label>
							 <br>
							<div id="envoltura" style="padding-top: 0px; margin-top: 0px;margin-left:0px;"></div>
							<div id="lab_envoltura" style="color:red; margin-left:9px; display:none;font-size: 16px"></div>
						</div>
					</div>
					<hr>


					<div class="row">	
						<div class="col-md-12">
	                         <label style="padding-top: 0px; margin-top: 0px;margin-left:0px;">
								 <span style="color:#0000ff; font-size:18px; font-weight:700">Ingrediente</span>
								 <span style="font-size:16px; font-weight:500"> ( Desmarque el / los ingrediente(s) no deseado del Sushi )</span>
							 </label>
							 <br><br>
							<div id="ingrediente" style="padding-top: 0px; margin-top: 0px;margin-left:0px;"></div>
							<div id="lab_ingrediente" style="color:red; margin-left:9px; display:none"></div>
						</div>
					</div>
					<hr>

	                <div id="val_ingredienteAdic">


					<div class="row">	
						<div class="col-md-12">	 
	                         <label style="padding-top: 0px; margin-top: 0px;margin-left:0px;">
								 <span style="color:#0000ff; font-size:18px; font-weight:700">Ingrediente adicionales</span>
								 <span style="font-size:16px;"> ( Seleccione el / los ingredientes adicionales )</span>
							 </label>
							 <br><br>
							<div id="ingredienteAdic" style="padding-top: 0px; margin-top: 0px;margin-left:0px;"></div>
						</div>
					</div>
					</div>  
					<hr>


					<div class="row">
	  				    <div class="col-md-1" >
							<label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
								 <span style="color:#0000ff; font-size:16px; font-weight:700">Cantidad</span>
							</label>
						</div>
						
	                    <div class="col-md-4">
			   				<select name="s_cantidad" id="s_cantidad" class="form-control">
	        				<option value = "1" selected>1</option>
	                        <?php 
	                            $i=1;
	                            for($i=2;$i<=99;$i++){ 
	                            ?>	
									<option value="<?=$i;?>"><?=$i;?>&nbsp;&nbsp;  </option>
								<?php
								}
								?>	
	        				</select>	
	                    </div>  




	                    <div class="col-md-7" style="text-align: right">
							<button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrar01"><span style="font-weight:700; font-size: 16px">Cancelar</span></button>
							&nbsp;&nbsp;

					        <button type="button" class="btn btn-primary" onclick="generar_alimento()" 
					               id="guardar01">
					              <span style="font-weight:700; font-size: 16px">Agregar a mi pedido</span>
					        </button>
	                    </div>  



					</div>				

               </div> 
			</form> 
		    </div> 	 

		</div>
	</div>
</div>




<!-- depacho alimento diferente a sabor sushi -->

<!---    Comienzo despacho en mesa    ---->
<div class="modal fade" id="generar_alimento_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content" style="width:900px; margin-left:-140px">
			<div class="modal-header bg-primary text-white">
				<h4 class="modal-title" id="exampleModalLabel" style="color:white">
					<span style="font-weight:700">Seleccionar Presentacion</span>
				</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true" style="color:white; font-size:30px">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				
			<form name="alimento_otro_formulario" method="post" id="alimento_otro_formulario" autocomplete="off">	
				
			  <div class="container">
				<div class="row">	
					<div class="col-md-12">	
						<label style="padding-top: 0px; margin-top: 0px;margin-left:0px;">
						    <span style="color:#0000ff; font-size:18px; font-weight:700">Presentación
						    </span>
						    <span style="font-size:14px; font-weight:400">&nbsp;&nbsp;Elige 1</span>
						</label>
						<br>
						<div id="presentacion_otro" style="padding-top: 0px; margin-top: 0px;margin-left:0px;"></div>
						<div id="lab_presentacion_otro" style="color:red; margin-left:9px; display:none; font-size: 16px"></div>
					</div>
				</div>
                <br>
                <hr>
                <br>

				<div class="row">
  				    <div class="col-md-1" >
						<label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:700">Cantidad</span>
						</label>
					</div>
					
                    <div class="col-md-4">
		   				<select name="m_cantidad" id="m_cantidad" class="form-control">
        				<option value = "1" selected>1</option>
                        <?php 
                            $i=1;
                            for($i=2;$i<=99;$i++){ 
                            ?>	
								<option value="<?=$i;?>"><?=$i;?>&nbsp;&nbsp;  </option>
							<?php
							}
							?>	
        				</select>	
                    </div>  




                    <div class="col-md-7" style="text-align: right">
						<button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrar02"><span style="font-weight:700; font-size: 16px">Cancelar</span></button>
						&nbsp;&nbsp;

				        <button type="button" class="btn btn-primary" onclick="generar_alimento_otro()" 
				               id="guardar02">
				              <span style="font-weight:700; font-size: 16px">Agregar a mi pedido</span>
				        </button>
                    </div>  



				</div>

				</div><!--container-->




			</form>  
		</div> <!--body-->

		</div>
	</div>
</div>


