<?php
$incluir = base_url() . "assets/images/nuevo.png";
$asterisco = ' style="color=red; font-size:16px"';

$ci = &get_instance();
$ci->load->library('Clssession');
$permiso_add= $ci->clssession->accion(4,2); 

?>
        <br>            
        <div class="main-panel">
            <div class="content">
                <div class="page-inner mt--5">
                    <div class="row row-card-no-pd mt--2">
                        <div class="col-sm-12 col-md-12">
                            <div class="card card-stats card-round">
                                <div class="card-body ">
									 <h2><span style="color:blue">Submódulos del Sistema</span></h2>
                                </div>
                            </div>
                        </div>
                    </div>

            
                <?php
                    if($permiso_add==1){
                ?> 
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header" style="text-align:right">
							<a href="javascript:void(0)" onclick="javascript:add_form()">
							   <img src="<?=$incluir;?>" style="width:35px; height:35px" alt="Incluir">
							</a>
                        </div>
                    </div>
                </div>    
                <?php
                    }
                ?> 

               

                <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="table-responsive">
										<div id="bodyTabla">
										</div>
										
										
                                    </div>
                                </div>
                            </div>
                        </div>    
                </div>
            </div>
    



<!-- Modal -->
<div class="modal fade" id="mod_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content" style="width:800px; margin-left:-100px">
			<div class="modal-header">
				<h4 class="modal-title" id="exampleModalLabel" style="color:blue">
					<div id="titulo_mostrar"></div>
				</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				
			<form name="formulario" method="post" id="formulario" autocomplete="off">	
				
				
			  <div class="container">
				<div class="row">
					<div class="col-md-3">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							     Nombre <span style="color:#ff0000;">*</span></span>
							 </label>

					</div>

					
					<div class="col-md-9">
                        <input style="padding-top: 5px;margin-right:-20px;margin-top: 12px;" 
                               type="text" name="m_nombre" id="m_nombre" class="form-control" 
                               placeholder="Edite el nombre" maxlength="100" minlength="0"                                >
                        <div id="lab_nombre" style="color:red; margin-left:9px; display:none"></div>       
					</div>
				</div>	


                <div id="url_mostrar"> <!--Se muestra si es incluir--> 
				<div class="row">
					<div class="col-md-3">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							 URL Sub modulo <span style="color:#ff0000;">*</span></span>
					     </label>

					</div>

					
					
					<div class="col-md-9">
                        <input style="padding-top: 5px;margin-right:-20px;margin-top: 12px;" 
                               type="text" name="m_url" id="m_url" class="form-control" 
                               placeholder="Edite el url" maxlength="100" minlength="0">
                        <div id="lab_url" style="color:red; margin-left:9px; display:none"></div>       
					</div>
				</div>	
                </div>  <!-- Fin del url-mostrar-->




				

				<div class="row" style="padding-top: 10px">
					<div class="col-md-3">
                         <label style="padding-top: 0px; margin-top: 10px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">Modulo asociado <span style="color:#ff0000;">*</span></span>
						 </label>

					</div>
					
					
					<div class="col-md-9">
						<div id="cbo_modulo"></div>
						<div id="lab_modulo" style="color:red; margin-left:9px; display:none"></div>
					</div>
				</div>	


				<div class="row">
					<div class="col-md-3">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">Orden de Visualización <span style="color:#ff0000;">*</span></span>
					  	 </label>

					</div>
					
					<div class="col-md-9">
                        <input style="padding-top: 5px;margin-right:-20px;margin-top: 12px;" 
                               type="text" id="m_orden" name="m_orden" class="form-control" 
                               placeholder="Ingrese el orden" maxlength="10" 
                               onkeyup="nro_entero('m_orden');">
                        <div id="lab_orden" style="color:red; margin-left:9px; display:none"></div>       
					</div>
				</div>	
                <br>  
				<span style="color:#ff0000; font-size:16px; text-align:left">Campo obligatorio (*)</span>
			  </div>				
			</div>
			<div class="modal-footer">

                   
				<button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrar01">Cerrar</button>


		        <button type="button" class="btn btn-primary" onclick="actualizar_form()" id="guardar01">
		              Guardar
		        </button>

			</div>


            <input type="hidden" id="id_mod" name="id_mod">
            <input type="hidden" id="nombre_orig" name="nombre_orig">
            <input type="hidden" id="val_error" name="val_error" value="0">
            <input type="hidden" id="accion_formulario" name="accion_formulario" value="0">

			</form>  

		</div>
	</div>
</div>

