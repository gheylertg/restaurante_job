<?php
$incluir = base_url() . "assets/images/nuevo.png";
$ci = &get_instance();
$ci->load->library('Clssession');
$fechaMaxima = date("Y-m-d");

?>

	<br>            
	<div class="main-panel">
		<div class="content">
			<div class="page-inner mt--5">
				<div class="row row-card-no-pd mt--2">
					<div class="col-sm-12 col-md-12">
						<div class="card card-stats card-round">
							<div class="card-body" style="margin-bottom:-30px;">
								 <h2><span style="color:blue">Consulta totales de facturación por empresa</span></h2>
							</div>
						</div>
					</div>    
				</div>	


				<form name="formulario" method="post" id="formulario">
            <div class="row"> 
            	<div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                        	<div class="row">
                        		<div class="col-md-1"></div>
                        		<div class="col-md-1">
            							      <span style="font-size:16px; color:blue; font-weight: 700">
							    	              Rango Fecha
                                </span>
                            </div>	
                        		<div class="col-md-3">
                        		<div class="form-inline">	
              							    <span style="font-size:16px; color:blue; font-weight: 700">
							                     	Desde: &nbsp;&nbsp; 
                                </span>
                                <input type="date" id="m_fechaI" name="m_fechaI" 
                                       min="2021-01-01" max="<?=$fechaMaxima;?>" 
                                       class="form-control" disabled="true"/>&nbsp;

              								  <div class="custom-control custom-checkbox">
                  									<input type="checkbox" class="custom-control-input" 
                  									       id="fecDesde" name="fecDesde" readonly="false" 
                  									       enabled="false" onclick="fechaDesde();" checked>&nbsp;
              										    <label class="custom-control-label" for="fecDesde">N/A
                										   </label><br>
              									</div>
              									<span id="lab_fecDesde" style="color:red; margin-left:9px; display:none"></span>	   
                       			</div>			
                       		    </div>	
                       		    <div class="col-md-1"></div>
                        		<div class="col-md-4">
                        		<div class="form-inline">	
							    <span style="font-size:16px; color:blue; font-weight: 700">
							    	Hasta: &nbsp;&nbsp; 
                                    </span>
                                    <input type="date" id="m_fechaF" name="m_fechaF" 
                                           min="2021-01-01" max="<?=$fechaMaxima;?>" 
                                           class="form-control" disabled="true"/>&nbsp;

								<div class="custom-control custom-checkbox">
    									<input type="checkbox" class="custom-control-input" 
    									       id="fecHasta" name="fecHasta" 
    									       disabled="true" onclick="fechaHasta();" checked>&nbsp;
										    <label class="custom-control-label" for="fecHasta">N/A
  										   </label>
  								</div>		   
  								<span id="lab_fecHasta" style="color:red; margin-left:9px; display:none">	   
  								</span>	
									</div>	   
                       			</div>			
                       		    </div>	
                          </div>    

			            <div class="row">
			                <div class="col-md-12" style="text-align: right;">
			                    <button type="button" class="btn btn-primary" onclick="generar_consultaTotales()" id="generar01">
			                          Generar Consulta
			                    </button>                
			                </div>    
			            </div> 
			        </div>
			    </div>
			    </div>            	
      <input type="hidden" id="sw_fecha1" name="sw_fecha1" value="0">
      <input type="hidden" id="sw_fecha2" name="sw_fecha2" value="0">    
			</form>	
			

                <div id="mostrarDt" style="display:none"> 
                <div class="row"> 
                	<div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
									                 <div id="bodyTabla">
									                 </div>
	                            </div>
                            </div>
                        </div>
                    </div>
                </div>            	
            	</div>
            </div>
		</div>
	</div> 
  </div>   



