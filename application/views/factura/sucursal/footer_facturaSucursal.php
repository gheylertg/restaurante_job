


<!--cargar datatable-->
<script>
    //ejcucion del timer promedio 10 segundo	
    $(document).ready(function(){
	    <?php 
    	if($accion==1){
    	?>	
			//consultaFactura(0);        
		<?php 
	    }	
	    ?>

	});


//Validar boton enter de seleccionar beneficiario en hcm


function generar_consulta(){
    error = 0;
    error = valSeleccion();
    if(error==0){
        $.ajax({
            url : "<?=base_url();?>Fa_sucursal/ajax_generarFactura",
            type: "POST",
            data: $('#formulario').serialize(),
            dataType: "JSON",
            success: function(data){
                if(data.status==0){ 
                    document.getElementById('mostrarDt').style.display="inline";                        

                     document.getElementById('bodyTabla').innerHTML = data.registro;
                     $(document).ready(function() {
                        $('#basic-datatables').DataTable({
                        }); 
                     });                    


                }else{
                   document.getElementById('mostrarDt').style.display="none"; 
                   document.getElementById('bodyTabla').innerHTML = data.registro;
                    swal("No existen facturas asociada a la consulta efectuada", {
                        icon : "error",
                        buttons: {                  
                            confirm: {
                                className : 'btn btn-danger'
                            }
                        },
                    });

                }    
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error Insertando / modificanco registro, Comuniquese con el administrador');
            }
        });

    }else{
        swal("No se genero la consulta", "Faltan datos", {
            icon : "error",
            buttons: {                  
                confirm: {
                    className : 'btn btn-danger'
                }
            },
        });
    }
}

function valSeleccion(){
error=0;
formulario="formulario";
var isChecked = document.getElementById('documento').checked;
if(isChecked==false){  
    objeto = "m_documento";
    campo = document.forms[formulario].elements[objeto].value;
    campo = campo.replace(/(^\s*)|(\s*$)/g,"");
    nombre = campo;
    document.getElementById('lab_documento').style.display = "none";
    document.getElementById('sw_documento').value="0"; 
    if (campo.length==0){
        document.getElementById('lab_documento').style.display = "inline";
        document.getElementById('lab_documento').innerHTML = "Ingrese el Nro de la Factura";    
        error = 1;
    }else{

        document.getElementById('sw_documento').value="1";     
    }
}

//validar fecha
document.getElementById('lab_fecDesde').style.display = "none";
document.getElementById('lab_fecHasta').style.display = "none";
document.getElementById('sw_fecha1').value="0"; 
document.getElementById('sw_fecha2').value="0"; 

var isChecked = document.getElementById('fecDesde').checked;
if(isChecked==false){  
    mfechaI = document.getElementById('m_fechaI').value;
    if (mfechaI.length<10){
        error=1;
        document.getElementById('lab_fecDesde').style.display = "inline";
        document.getElementById('lab_fecDesde').innerHTML = "Seleccione la fecha Inicial";    
    }else{
        var isChecked = document.getElementById('fecHasta').checked;
          

        if(isChecked==false){
            mfechaF = document.getElementById('m_fechaF').value;
            if (mfechaF.length<10){
                error=1;
                document.getElementById('lab_fecHasta').style.display = "inline";
                document.getElementById('lab_fecHasta').innerHTML = "Seleccione la fecha Final";    
            }else{
                anioI = mfechaI.substring(0,4);
                mesI = mfechaI.substring(5,7);
                diaI = mfechaI.substring(8,10);
                valuesInicial = diaI + "/" + mesI + "/" + anioI;
                valuesInicial=valuesInicial.split("/");
                var valuesInicial=new Date(valuesInicial[2],(valuesInicial[1]-1),valuesInicial[0]);

                anioF = mfechaF.substring(0,4);
                mesF = mfechaF.substring(5,7);
                diaF = mfechaF.substring(8,10);
                valuesFinal = diaF + "/" + mesF + "/" + anioF;
                valuesFinal=valuesFinal.split("/");
                var valuesFinal=new Date(valuesFinal[2],(valuesFinal[1]-1),valuesFinal[0]);

                if(valuesInicial > valuesFinal){
                    error = 1;
                    document.getElementById('lab_fecDesde').style.display = "inline";
                    document.getElementById('lab_fecDesde').innerHTML = "La Fecha Inicia no puede ser mayor a la Fecha Final";    
                }else{
                    document.getElementById('sw_fecha1').value="1";    
                    document.getElementById('sw_fecha2').value="1";
                }                
            }    
        }else{
            document.getElementById('sw_fecha1').value="1";   
        }
    }
}    







return error;
}


function generar_reporte(){
    id=0;
    $.ajax({
        url : "<?php echo site_url('Fa_empresa/ajax_imprimir_sucursalFactura')?>/"+id,
        type: "GET",                     
        dataType: "JSON",
        success: function(data){
            alert("impreso");

        },
        error: function (jqXHR, textStatus, errorThrown){
            alert('Error consultando la Base de Datos');
        }
    });
}


function actDocumento(){
   var isChecked = document.getElementById('documento').checked;
   if(isChecked){  
       document.getElementById('m_documento').disabled=true;
       document.getElementById('m_documento').value="";
       document.getElementById('lab_documento').style.display="none";
       document.getElementById('sw_documento').value="0";
   }else{

       document.getElementById('m_documento').disabled=false;       
   }    
}


function fechaDesde(){

   var isChecked = document.getElementById('fecDesde').checked;
   if(isChecked){  
       document.getElementById('m_fechaI').disabled=true;
       document.getElementById('m_fechaF').disabled=true;
       document.getElementById('fecHasta').checked=true;
       document.getElementById('fecHasta').disabled=true;
       document.getElementById('sw_fecha1').value="0";
       document.getElementById('sw_fecha2').value="0";
   }else{
       document.getElementById('m_fechaI').disabled=false;
       document.getElementById('fecHasta').disabled=false;
       document.getElementById('fecHasta').checked=true;
       //document.getElementById('fechaHasta').checked;
   }    
    
}


function fechaHasta(){
   var isChecked = document.getElementById('fecHasta').checked;
   if(isChecked){  
       document.getElementById('m_fechaF').disabled=true;
   }else{
       document.getElementById('m_fechaF').disabled=false;       
   }    
    
}


///////totales factura
function generar_consultaTotales(){
    error = 0;
    error = valSeleccionTotales();
    if(error==0){
        $.ajax({
            url : "<?=base_url();?>Fa_sucursal/ajax_generarTotalesFactura",
            type: "POST",
            data: $('#formulario').serialize(),
            dataType: "JSON",
            success: function(data){
                if(data.status==0){ 
                    document.getElementById('mostrarDt').style.display="inline";                        

                     document.getElementById('bodyTabla').innerHTML = data.registro;
                     $(document).ready(function() {
                        $('#basic-datatables').DataTable({
                        }); 
                     });                    


                }else{
                   document.getElementById('mostrarDt').style.display="none"; 
                   document.getElementById('bodyTabla').innerHTML = data.registro;
                    swal("No existen facturas asociada a la consulta efectuada", {
                        icon : "error",
                        buttons: {                  
                            confirm: {
                                className : 'btn btn-danger'
                            }
                        },
                    });

                }    
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error Insertando / modificanco registro, Comuniquese con el administrador');
            }
        });

    }else{
        swal("No se genero la consulta", "Faltan datos", {
            icon : "error",
            buttons: {                  
                confirm: {
                    className : 'btn btn-danger'
                }
            },
        });
    }
}



function valSeleccionTotales(){
error=0;
formulario="formulario";

//validar fecha

document.getElementById('lab_fecDesde').style.display = "none";
document.getElementById('lab_fecHasta').style.display = "none";

document.getElementById('sw_fecha1').value="0"; 
document.getElementById('sw_fecha2').value="0"; 

var isChecked = document.getElementById('fecDesde').checked;
if(isChecked==false){  
    mfechaI = document.getElementById('m_fechaI').value;
    if (mfechaI.length<10){
        error=1;
        document.getElementById('lab_fecDesde').style.display = "inline";
        document.getElementById('lab_fecDesde').innerHTML = "Seleccione la fecha Inicial";    
    }else{
        var isChecked = document.getElementById('fecHasta').checked;
          

        if(isChecked==false){
            mfechaF = document.getElementById('m_fechaF').value;
            if (mfechaF.length<10){
                error=1;
                document.getElementById('lab_fecHasta').style.display = "inline";
                document.getElementById('lab_fecHasta').innerHTML = "Seleccione la fecha Final";    
            }else{
                anioI = mfechaI.substring(0,4);
                mesI = mfechaI.substring(5,7);
                diaI = mfechaI.substring(8,10);
                valuesInicial = diaI + "/" + mesI + "/" + anioI;
                valuesInicial=valuesInicial.split("/");
                var valuesInicial=new Date(valuesInicial[2],(valuesInicial[1]-1),valuesInicial[0]);

                anioF = mfechaF.substring(0,4);
                mesF = mfechaF.substring(5,7);
                diaF = mfechaF.substring(8,10);
                valuesFinal = diaF + "/" + mesF + "/" + anioF;
                valuesFinal=valuesFinal.split("/");
                var valuesFinal=new Date(valuesFinal[2],(valuesFinal[1]-1),valuesFinal[0]);

                if(valuesInicial > valuesFinal){
                    error = 1;
                    document.getElementById('lab_fecDesde').style.display = "inline";
                    document.getElementById('lab_fecDesde').innerHTML = "La Fecha Inicia no puede ser mayor a la Fecha Final";    
                }else{
                    document.getElementById('sw_fecha1').value="1";    
                    document.getElementById('sw_fecha2').value="1";
                }                
            }    
        }else{
            document.getElementById('sw_fecha1').value="1";   
        }
    }
}    
return error;
}











///////////////////////////////////////////////////////////////////////////////////////////////
function cargarDataTable(id){
    $.ajax({
		url : "<?php echo site_url('Fa_factura/ajax_datatable')?>/"+id,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
            activarBotones(data.tipoDespacho); 
 		    document.getElementById('bodyTabla').innerHTML = data.registro;
			$(document).ready(function() {
                document.getElementById('nombreSucursal').innerHTML = data.nombreEmpresa;

				$('#basic-datatables').DataTable({
			    });	
			});

		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}





function activarBotones(tipoDespacho){

    tipoDespacho = parseInt(tipoDespacho);
    document.getElementById("tipo_1").className += " btn-border";
    document.getElementById("tipo_2").className += " btn-border";
    document.getElementById("tipo_3").className += " btn-border";

    switch(tipoDespacho){
        case 1:
            document.getElementById("tipo_1").className =document.getElementById("tipo_1").className.replace( /(?:^|\s)btn-border(?!\S)/g , '' );
        break;

        case 2:
            document.getElementById("tipo_2").className =document.getElementById("tipo_2").className.replace( /(?:^|\s)btn-border(?!\S)/g , '' );
        break;

        case 3:
            document.getElementById("tipo_3").className =document.getElementById("tipo_3").className.replace( /(?:^|\s)btn-border(?!\S)/g , '' );
        break;
    }
}




function actDespachoActual(tipoDespacho){
    $.ajax({
        url : "<?php echo site_url('Dp_despacho/ajax_despachoActual')?>/"+tipoDespacho,
        type: "GET",                     
        dataType: "JSON",
        success: function(data){
            activarBotones(data.tipoDespacho); 
            document.getElementById('tipoDespacho').value = data.tipoDespacho;
            document.getElementById('bodyTabla').innerHTML = data.registro;
            document.getElementById('nroDespachoMesa').innerHTML = data.tipo_1;
            document.getElementById('nroDespachoRetiro').innerHTML = data.tipo_2;
            document.getElementById('nroDespachoDelivery').innerHTML = data.tipo_3;
            $(document).ready(function() {
                $('#basic-datatables').DataTable({
                }); 
            });

        },
        error: function (jqXHR, textStatus, errorThrown){
            alert('Error consultando la Base de Datos');
        }
    });
}



//Segun el tipo de despacho activo al presionar el boton generar despacho
// comienza la generacion del despacho
function despacho(){
    tipoDespacho = document.getElementById('tipoDespacho').value;
    if(tipoDespacho=="1"){
        despachoMesa();
    }

    if(tipoDespacho=="2"){
        retiroTienda();
    }

    if(tipoDespacho=="3"){
        delivery();
    }


}


///////////////////////////////////////////////////////
////////////////  Despacho mesa ///////////////////////
///////////////////////////////////////////////////////


///comienzo generar despacho en mesa
function despachoMesa(){
    //////////////////////////////////////////
    /*  Validar si esta activa la session //*/
    //////////////////////////////////////////     

    ruta = "<?php echo site_url('Combo_ctrl/ajax_combo_sala_anid')?>";
    cbo_sala(ruta,0);

    ruta = "<?php echo site_url('Combo_ctrl/ajax_anid_mesa')?>";
    cbo_anid_mesa(ruta,0,0);

    ruta = "<?php echo site_url('Combo_ctrl/ajax_combo_mesonero')?>";
    cbo_mesonero(ruta,0,0);

    document.getElementById('lab_sala').style.display = "inline";
    document.getElementById('lab_mesa').style.display = "inline";
    document.getElementById('lab_mesonero').style.display = "inline";

    document.getElementById('guardar01').disabled=false;
    document.getElementById('cerrar01').disabled=false;

    $('#generarDespacho_DM_modal').modal('show');     
}


/// Crear combo mesa despues de seleecionado el sala en despacho mesa
function cbo_sel_mesa(){
    idSala = document.getElementById('id_cbo_sala').value;
    ruta = "<?php echo site_url('Combo_ctrl/ajax_anid_mesa')?>";
    cbo_anid_mesa(ruta,0,idSala);
}


// Guardar despacho en mesa
function generar_despachoDM(){
    //validar campos formulario
    formulario = "dm_formulario";
    error=0;
    error = validar_form(formulario,1);
    if(error==0){
        document.getElementById('guardar01').disabled=true;
        document.getElementById('cerrar01').disabled=true;
        $.ajax({
            url : "<?=base_url();?>Dp_despacho/ajax_generarDespachoDM",
            type: "POST",
            data: $('#dm_formulario').serialize(),
            dataType: "JSON",
            success: function(data)
            {
                $('#generarDespacho_DM_modal').modal('hide');
                document.location.href = "<?php echo base_url('despachoCliente')?>";
        
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error Insertando / modificanco registro, Comuniquese con el administrador');
            }
        });



    }else{
        swal("No se genero del Despacho", "Faltan datos", {
            icon : "error",
            buttons: {                  
                confirm: {
                    className : 'btn btn-danger'
                }
            },
        });
    }
}





/////////////// fin Despacho mesa ////////////////////// 



////////////////  Retiro en tienda ///////////////////////


//comienzo de generar despacho retiro en tienda
function retiroTienda(){
    //////////////////////////////////////////
    /*  Validar si esta activa la session //*/
    //////////////////////////////////////////     
    document.getElementById('guardar02').disabled=false;
    document.getElementById('cerrar02').disabled=false;
    document.getElementById('lab_nombre').style.display="none";

    $.ajax({
        url : "<?php echo site_url('Dp_despacho/ajax_inicializarRT')?>",
        type: "GET",                     
        dataType: "JSON",
        success: function(data){
            //llamar despacho cliente.
            $('#generarRT_modal').modal('show');; 
        },
        error: function (jqXHR, textStatus, errorThrown){
            alert('Error consultando la Base de Datos');
        }
    });
        
}


function generar_despachoRT(){
    //validar campos formulario
    formulario = "rt_formulario";
    error=0;
    error = validar_form_RT(formulario,1);

    if(error==0){
        document.getElementById('guardar02').disabled=true;
        document.getElementById('cerrar02').disabled=true;
        $.ajax({
            url : "<?=base_url();?>Dp_despacho/ajax_generarDespachoRT",
            type: "POST",
            data: $('#rt_formulario').serialize(),
            dataType: "JSON",
            success: function(data)
            {
                $('#generarRT_modal').modal('hide');
                document.location.href = "<?php echo base_url('despachoCliente')?>";
        
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error Insertando / modificanco registro, Comuniquese con el administrador');
            }
        });



    }else{
        swal("No se genero del Despacho", "Faltan datos", {
            icon : "error",
            buttons: {                  
                confirm: {
                    className : 'btn btn-danger'
                }
            },
        });
    }
}





/////////////// fin Retiro en tienda ////////////////////// 




//////////////////////////////////////////////////
////////////////  Delivery ///////////////////////
//////////////////////////////////////////////////

//comienzo de generar despacho delivery
function delivery(){
    //////////////////////////////////////////
    /*  Validar si esta activa la session //*/
    //////////////////////////////////////////     
    document.getElementById('guardar03').disabled=false;
    document.getElementById('cerrar03').disabled=false;
    document.getElementById('lab_nombre_dl').style.display="none";
    document.getElementById('lab_telefono_dl').style.display="none";
    document.getElementById('lab_direccion_dl').style.display="none";

    $.ajax({
        url : "<?php echo site_url('Dp_despacho/ajax_inicializarDelivery')?>",
        type: "GET",                     
        dataType: "JSON",
        success: function(data){
            //llamar despacho cliente.
            $('#generarDL_modal').modal('show');; 
        },
        error: function (jqXHR, textStatus, errorThrown){
            alert('Error consultando la Base de Datos');
        }
    });
        
}




//Al ejecutarse el timer muetra los delivery que llegaron via web y si esta el la session de delivery, actualiza el data table.
function estadoDelivery(){
    $.ajax({
        url : "<?php echo site_url('Dp_despacho/nroDelivery')?>",
        type: "GET",                     
        dataType: "JSON",
        success: function(data){
            document.getElementById('nroDespachoDelivery').innerHTML = data.tipo;
        },
        error: function (jqXHR, textStatus, errorThrown){
            alert('Error consultando la Base de Datos');
        }
    });
}


function generar_despachoDL(){
    //validar campos formulario
    formulario = "dl_formulario";
    error=0;
    error = validar_form_DL(formulario,1);

    if(error==0){
        document.getElementById('guardar03').disabled=true;
        document.getElementById('cerrar03').disabled=true;
        $.ajax({
            url : "<?=base_url();?>Dp_despacho/ajax_generarDespachoDL",
            type: "POST",
            data: $('#rt_formulario').serialize(),
            dataType: "JSON",
            success: function(data)
            {
                $('#generarDL_modal').modal('hide');
                document.location.href = "<?php echo base_url('despachoCliente')?>";
        
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error Insertando / modificanco registro, Comuniquese con el administrador');
            }
        });



    }else{
        swal("No se genero del Despacho", "Faltan datos", {
            icon : "error",
            buttons: {                  
                confirm: {
                    className : 'btn btn-danger'
                }
            },
        });
    }
}


///////////////////////////////////////////////////
/////////////// fin delivery //////////////////////
/////////////////////////////////////////////////// 




/////////////////////////////////////////////////////
///        Generar pedido despacho cliente        ///
/////////////////////////////////////////////////////
function regresarDespacho(){
    document.location.href = "<?php echo base_url('despachoActivo')?>";
}


function despachoCliente(idDespacho){
    $.ajax({
        url : "<?php echo site_url('Dp_despacho/ajax_inicializarVariableDespacho')?>/"+idDespacho,
        type: "GET",                     
        dataType: "JSON",
        success: function(data){
            //llamar despacho cliente.
            document.location.href = "<?php echo base_url('despachoCliente')?>"; 
        },
        error: function (jqXHR, textStatus, errorThrown){
            alert('Error consultando la Base de Datos');
        }
    });

}



function cerrarDespacho(idDespacho){
    swal({
        title: 'Esta seguro de cerrar la cuenta?',
            type: 'warning',
            buttons:{
                cancel: {
                    visible: true,
                        text : 'No, Cerrar!',
                                className: 'btn btn-danger'
                },                  
                confirm: {
                    text : 'Si, Cerrar Cuenta!',
                    className : 'btn btn-success'
                }
            }
    }).then((willDelete) => {
        if (willDelete) {
            $.ajax({
                url : "<?php echo site_url('Dp_despacho/ajax_cerrarDespacho')?>/" + idDespacho,
                type: "GET",                     
                dataType: "JSON",
                success: function(data){
                    document.location.href = "<?php echo base_url('despachoActivo')?>";
                },
                error: function (jqXHR, textStatus, errorThrown){
                    alert('Error consultando la Base de Datos');
                }

            });
        }
    });
}


/// Genera la pantalla para cargar el pedido
function generarDespachoCliente(){
$.ajax({
    url : "<?php echo site_url('Dp_despacho/ajax_despachoCliente')?>",
    type: "GET",                     
    dataType: "JSON",
    success: function(data){
        //llamar despacho cliente.
        //alert("Precio " + data.precio);

        document.getElementById('titulo').innerHTML = data.titulo;
        document.getElementById('datoDespacho').innerHTML = data.datoDespacho;
        document.getElementById('factura').innerHTML = data.factura;
        document.getElementById('precioF').innerHTML = data.precio;

        document.getElementById('categoria').innerHTML = data.categoria;    
        document.getElementById('alimento').innerHTML = data.alimento;    

        if(data.boton==0){
            document.getElementById('boton02').style.display="none";
            document.getElementById('boton03').style.display="none";
        }else{
            document.getElementById('boton02').style.display="inline";
            document.getElementById('boton03').style.display="inline";
        }
    },
    error: function (jqXHR, textStatus, errorThrown){
        alert('Error consultando la Base de Datos');
    }
});

}


// al seleccionar el boton de la categoria de menu, activas los alimentos
function categoriaMenu_seleccion(idCategoria, idBoton){
    $.ajax({
        url : "<?php echo site_url('Dp_despacho/ajax_mostrarAlimento')?>/"+idCategoria,
        type: "GET",                     
        dataType: "JSON",
        success: function(data){
            //llamar despacho cliente.
            if(data.status==0){
                document.getElementById('alimento').innerHTML = data.alimento;
            }else{
                document.getElementById('alimento').innerHTML = data.alimento;
                swal("No existen presentación de alimento asociados a la Categoría de Menú seleccionada", "Verifique", {
                    icon : "error",
                    buttons: {                  
                        confirm: {
                            className : 'btn btn-danger'
                        }
                    },
                });
            }    
        },
        error: function (jqXHR, textStatus, errorThrown){
            alert('Error consultando la Base de Datos');
        }
    });
}



//Despues de seleccionado el boton de categoria de menu se procede a cargar los alimentos
function cargarAlimento(id, idCategoria){
idCate = parseInt(idCategoria);
//if(idCate == 2){
//   alert("combo");
//} 

if(idCate > 2){
    cargar_alimento_otro(id);
}


if(idCate==1){
    $.ajax({
        url : "<?php echo site_url('Dp_despacho/ajax_prepara_sushi')?>/"+id,
        type: "GET",                     
        dataType: "JSON",
        success: function(data){
            if(data.status==0){
                if(data.validar==0){
                    //llamar despacho cliente.
                    //document.getElementById('factura').innerHTML = data.presentacion;
                    document.getElementById('presentacion').innerHTML = data.presentacion;
                    document.getElementById('envoltura').innerHTML = data.envoltura;
                    document.getElementById('ingrediente').innerHTML = data.ingrediente;
                    
                    
                    var adic = data.ingredienteAdic;
                    if(adic==""){
						document.getElementById('val_ingredienteAdic').style.display="none";
					}else{
						document.getElementById('val_ingredienteAdic').style.display="inline";
					}	
                   
                    //val_ingredienteAdic
                    document.getElementById('ingredienteAdic').innerHTML = data.ingredienteAdic;
                    
                    document.getElementById('s_cantidad').value = 1;

                    document.getElementById('guardar01').disabled=false;
                    document.getElementById('cerrar01').disabled=false;
                    $('#generar_sushi_modal').modal('show');
                }else{    
                    swal({
                        title: 'Falta existencia para el Producto seleccionado, desea continuar?',
                            type: 'warning',
                            buttons:{
                                cancel: {
                                    visible: true,
                                        text : 'No, Cancelar!',
                                                className: 'btn btn-danger'
                                },                  
                                confirm: {
                                    text : 'Si, Continuar!',
                                    className : 'btn btn-success'
                                }
                            }
                    }).then((willDelete) => {
                        if (willDelete) {
                            //llamar despacho cliente.
                            //document.getElementById('factura').innerHTML = data.presentacion;
                            document.getElementById('presentacion').innerHTML = data.presentacion;
                            document.getElementById('envoltura').innerHTML = data.envoltura;
                            document.getElementById('ingrediente').innerHTML = data.ingrediente;
                            document.getElementById('ingredienteAdic').innerHTML = data.ingredienteAdic;
                            document.getElementById('s_cantidad').value = 1;

                            document.getElementById('guardar01').disabled=false;
                            document.getElementById('cerrar01').disabled=false;
                            $('#generar_sushi_modal').modal('show');
                        }
                    });
                }  
            }      
        },
        error: function (jqXHR, textStatus, errorThrown){
            alert('Error consultando la Base de Datos');
        }
    });
}//fin del if(id==1)
}


function cargar_alimento_otro(id){
    //cargar un alimento diferente al sushi
    $.ajax({
        url : "<?php echo site_url('Dp_despacho/ajax_cargarAlimento')?>/"+id,
        type: "GET",                     
        dataType: "JSON",
        success: function(data){
            if(data.status==0){
                //llamar despacho cliente.
                //document.getElementById('factura').innerHTML = data.presentacion; 
                document.getElementById('presentacion_otro').innerHTML = data.presentacion; 
                document.getElementById('m_cantidad').value = 1; 


                document.getElementById('guardar02').disabled=false;
                document.getElementById('cerrar02').disabled=false;        
                $('#generar_alimento_modal').modal('show');   
            }else{
                swal("Falta existencia para el Producto seleccionado", "Verifique el contenedor", {
                    icon : "error",
                    buttons: {                  
                        confirm: {
                            className : 'btn btn-danger'
                        }
                    },
                });
            }
        },
        error: function (jqXHR, textStatus, errorThrown){
            alert('Error consultando la Base de Datos');
        }
    });

}


function generar_alimento(){

//validar presentacion
$error = 0;
error=val_presentacion('sushi_formulario');
if(error==0){
   //Guardar detalle del pedido.
    document.getElementById('guardar01').disabled=true;
    document.getElementById('cerrar01').disabled=true;
    $.ajax({
        url : "<?=base_url();?>Dp_despacho/ajax_guardarDetalle",
        type: "POST",
        data: $('#sushi_formulario').serialize(),
        dataType: "JSON",
        success: function(data)
        {
            if(data.status==0){



                document.getElementById('factura').innerHTML = data.factura; 
                document.getElementById('precioF').innerHTML = data.precio; 

                if(data.boton==0){
                    document.getElementById('boton02').style.display="none";
                    document.getElementById('boton03').style.display="none";
                }else{
                    document.getElementById('boton02').style.display="inline";
                    document.getElementById('boton03').style.display="inline";
                }



                $('#generar_sushi_modal').modal('hide');
            }else{
                document.getElementById('guardar01').disabled=false;
                document.getElementById('cerrar01').disabled=false;
                swal("Falta existencia para la Presentación del Producto seleccionado", "Verifique el contenedor", {
                    icon : "error",
                    buttons: {                  
                        confirm: {
                            className : 'btn btn-danger'
                        }
                    },
                });

            }    

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error Insertando / modificanco registro, Comuniquese con el administrador');
        }
    });   




}else{
    swal("Falto un Item en la preparación del Sushi", "Verifique", {
        icon : "error",
        buttons: {                  
            confirm: {
                className : 'btn btn-danger'
            }
        },
    });
}

}




//Generar alimento diferente a sushi
function generar_alimento_otro(){
//validar presentacion
$error = 0;
error=val_presentacion_otro('alimento_otro_formulario');
if(error==0){
   //Guardar detalle del pedido.
    document.getElementById('guardar02').disabled=true;
    document.getElementById('cerrar02').disabled=true;
    $.ajax({
        url : "<?=base_url();?>Dp_despacho/ajax_guardarDetalle_otro",
        type: "POST",
        data: $('#alimento_otro_formulario').serialize(),
        dataType: "JSON",
        success: function(data)
        {
            if(data.status==0){
                document.getElementById('factura').innerHTML = data.factura; 
                document.getElementById('precioF').innerHTML = data.precio; 
                if(data.boton==0){
                    document.getElementById('boton02').style.display="none";
                    document.getElementById('boton03').style.display="none";
                }else{
                    document.getElementById('boton02').style.display="inline";
                    document.getElementById('boton03').style.display="inline";
                }

                $('#generar_alimento_modal').modal('hide');
            }else{
                document.getElementById('guardar01').disabled=false;
                document.getElementById('cerrar01').disabled=false;
                swal("Falta existencia para la Presentación del Producto seleccionado", "Verifique el contenedor", {
                    icon : "error",
                    buttons: {                  
                        confirm: {
                            className : 'btn btn-danger'
                        }
                    },
                });

            }    

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error Insertando / modificanco registro, Comuniquese con el administrador');
        }
    });   




}else{
    swal("Falto un Item en la preparación del Sushi", "Verifique", {
        icon : "error",
        buttons: {                  
            confirm: {
                className : 'btn btn-danger'
            }
        },
    });
}

}


function modificar_itemFactura(id){
    $.ajax({
        url : "<?php echo site_url('Dp_despacho/ajax_modificar_itemProducto')?>/"+id,
        type: "GET",                     
        dataType: "JSON",
        success: function(data){
             //document.getElementById('m_cantidad').value = data.cantidad;
             document.getElementById('id_mod_mo').value = data.id;
             //document.getElementById('lab_cantidad_mo').style.display = "none";
             //ruta = "<?php //echo site_url('Combo_ctrl/ajax_combo_modulo')?>";
             //cbo_modulo(ruta,data.id_modulo);   //Llenar combo    
             $('#modificar_ingredienteAlimento').modal('show');

        },
        error: function (jqXHR, textStatus, errorThrown){
            alert('Error consultando la Base de Datos');
        }
    });
}


function mas_itemFactura(id, idCategoria){
    $.ajax({
        url : "<?php echo site_url('Dp_despacho/ajax_mas_itemProducto')?>",
        type: "GET",                     
        dataType: "JSON",
        data: {id:id,idCategoria:idCategoria},        
        success: function(data){
            document.getElementById('factura').innerHTML = data.factura;
            document.getElementById('precioF').innerHTML = data.precio;
            if(data.boton==0){
                document.getElementById('boton02').style.display="none";
                document.getElementById('boton03').style.display="none";
            }else{
                document.getElementById('boton02').style.display="inline";
                document.getElementById('boton03').style.display="inline";
            }

        },
        error: function (jqXHR, textStatus, errorThrown){
            alert('Error consultando la Base de Datos');
        }
    });
}    

function menos_itemFactura(id, idCategoria){
    $.ajax({
        url : "<?php echo site_url('Dp_despacho/ajax_menos_itemProducto')?>",
        type: "GET",                     
        dataType: "JSON",
        data: {id:id,idCategoria:idCategoria},   
        
        success: function(data){
            if(data.status==0){ 
                document.getElementById('factura').innerHTML = data.factura;
                document.getElementById('precioF').innerHTML = data.precio;
                if(data.boton==0){
                    document.getElementById('boton02').style.display="none";
                    document.getElementById('boton03').style.display="none";
                }else{
                    document.getElementById('boton02').style.display="inline";
                    document.getElementById('boton03').style.display="inline";
                }

            }    
        },
        error: function (jqXHR, textStatus, errorThrown){
            alert('Error consultando la Base de Datos');
        }
    });

}    


function eliminar_itemFactura(id){
$.ajax({
    url : "<?php echo site_url('Dp_despacho/ajax_eliminar_itemFactura')?>/"+id,
    type: "GET",                     
    dataType: "JSON",
    success: function(data){
        document.getElementById('factura').innerHTML = data.factura;
        document.getElementById('precioF').innerHTML = data.precio;
        if(data.boton==0){
            document.getElementById('boton02').style.display="none";
            document.getElementById('boton03').style.display="none";
        }else{
            document.getElementById('boton02').style.display="inline";
            document.getElementById('boton03').style.display="inline";
        }

    },
    error: function (jqXHR, textStatus, errorThrown){
        alert('Error consultando la Base de Datos');
    }
});

}



function val_presentacion(formulario){
error = 0

////////////////////////////////////  
//validar seleccion presentacion  //
////////////////////////////////////

valPresentacion = document.getElementById('valPresentacion').value;
if(valPresentacion==0){
    marcado=false;

    for ( var i = 0; i < sushi_formulario.presentacion.length; i++ ) {
        if (sushi_formulario.presentacion[i].checked)
            marcado = true;
    }

    document.getElementById('lab_presentacion').style.display = "none";
    if(!marcado){
        document.getElementById('lab_presentacion').style.display = "inline";
        document.getElementById('lab_presentacion').innerHTML = "Seleccione la Presentación";    
        error = 1;
    }
}


////////////////////////////////////  
//validar cantidad    //
////////////////////////////////////
objeto = "s_cantidad";
campo = document.forms[formulario].elements[objeto].value;
campo = campo.replace(/(^\s*)|(\s*$)/g,"");


document.getElementById('lab_cantidad').style.display = "none";
if (campo==0 || campo.length==0){
    document.getElementById('lab_cantidad').style.display = "inline";
    document.getElementById('lab_cantidad').innerHTML = "Indique la Cantidad a despachar";    
    error = 1;
}




////////////////////////////////////  
//validar seleccion envoltura    //
////////////////////////////////////
valEnvoltura = document.getElementById('valEnvoltura').value;
if(valEnvoltura==0){
    marcado=false;
    for ( var i = 0; i < sushi_formulario.envoltura.length; i++ ) {
        if (sushi_formulario.envoltura[i].checked)
            marcado = true;
    }

    document.getElementById('lab_envoltura').style.display = "none";
    if(!marcado){
        document.getElementById('lab_envoltura').style.display = "inline";
        document.getElementById('lab_envoltura').innerHTML = "Seleccione Envoltura";    
        error = 1;
    }
}

return error;
}


function val_presentacion_otro(formulario){
error = 0

////////////////////////////////////  
//validar seleccion presentacion  //
////////////////////////////////////
valPresentacion = document.getElementById('valPresentacion_otro').value;
if(valPresentacion==0){
    marcado=false;
    for ( var i = 0; i < alimento_otro_formulario.presentacion_otro.length; i++ ) {
        if (alimento_otro_formulario.presentacion_otro[i].checked)
            marcado = true;
    }
    document.getElementById('lab_presentacion_otro').style.display = "none";
    if(!marcado){
        document.getElementById('lab_presentacion_otro').style.display = "inline";
        document.getElementById('lab_presentacion_otro').innerHTML = "Seleccione la Presentación";    
        error = 1;
    }
}


////////////////////////////////////  
//validar cantidad    //
////////////////////////////////////
objeto = "m_cantidad";
campo = document.forms[formulario].elements[objeto].value;
campo = campo.replace(/(^\s*)|(\s*$)/g,"");


document.getElementById('lab_cantidad_otro').style.display = "none";
if (campo==0 || campo.length==0){
    document.getElementById('lab_cantidad_otro').style.display = "inline";
    document.getElementById('lab_cantidad_otro').innerHTML = "Indique la Cantidad a despachar";    
    error = 1;
}

return error;
}


function valExistencia(idPresentacion){
$.ajax({
    url : "<?php echo site_url('Dp_despacho/ajax_disponibilidad_sushi')?>/"+idPresentacion,
    type: "GET",                     
    dataType: "JSON",
    success: function(data){
        if(data.status==1){
            //document.getElementById('guardar01').disabled=true;
            document.getElementById('lab_presentacion').style.display = "inline";
            document.getElementById('lab_presentacion').innerHTML = "No hay disponibilidad para la Presentación seleccionada";    
            swal("Falta existencia para la Presentación del Producto seleccionado", "Verifique el contenedor", {
                icon : "error",
                buttons: {                  
                    confirm: {
                        className : 'btn btn-danger'
                    }
                },
            });
        }else{
            document.getElementById('guardar01').disabled=false; 
            document.getElementById('lab_presentacion').style.display = "none";
        }

    },
    error: function (jqXHR, textStatus, errorThrown){
        alert('Error consultando la Base de Datos');
    }
});
}

//////////////////////////////////////////////////////
///                 Fin generar pedido            ///
/////////////////////////////////////////////////////

// validar datos de despacho en mesa
function validar_form(formulario,accion){
//accion 1= incluir
//accion 2= modificar
error=0;

objeto = "id_cbo_sala";
campo = document.forms[formulario].elements[objeto].value;
document.getElementById('lab_sala').style.display = "none";
if (campo==0){
    document.getElementById('lab_sala').style.display = "inline";
    document.getElementById('lab_sala').innerHTML = "Seleccione la Sala";    
    error = 1;
}

objeto = "id_cbo_mesa";
campo = document.forms[formulario].elements[objeto].value;
document.getElementById('lab_mesa').style.display = "none";
if (campo==0){
    document.getElementById('lab_mesa').style.display = "inline";
    document.getElementById('lab_mesa').innerHTML = "Seleccione la Mesa";    
    error = 1;
}

objeto = "id_cbo_mesonero";
campo = document.forms[formulario].elements[objeto].value;
document.getElementById('lab_mesonero').style.display = "none";
if (campo==0){
    document.getElementById('lab_mesonero').style.display = "inline";
    document.getElementById('lab_mesonero').innerHTML = "Seleccione el Mesonero";    
    error = 1;
}
return error;
}



//// Validar los datos en retiroen tienda
function validar_form_RT(formulario,accion){
//accion 1= incluir
//accion 2= modificar
error=0;

objeto = "m_nombre";
campo = document.forms[formulario].elements[objeto].value;
campo = campo.replace(/(^\s*)|(\s*$)/g,"");
nombre = campo;

document.getElementById('lab_nombre').style.display = "none";
if (campo.length==0){
    document.getElementById('lab_nombre').style.display = "inline";
    document.getElementById('lab_nombre').innerHTML = "Edite el nombre del cliente";    
    error = 1;
}

return error;
}


//validar datos de delivery
function validar_form_DL(formulario,accion){
//accion 1= incluir
//accion 2= modificar
error=0;

objeto = "d_nombre";
campo = document.forms[formulario].elements[objeto].value;
campo = campo.replace(/(^\s*)|(\s*$)/g,"");
nombre = campo;

document.getElementById('lab_nombre_dl').style.display = "none";
if (campo.length==0){
    document.getElementById('lab_nombre_dl').style.display = "inline";
    document.getElementById('lab_nombre_dl').innerHTML = "Edite el nombre del cliente";    
    error = 1;
}


objeto = "d_telefono";
campo = document.forms[formulario].elements[objeto].value;
campo = campo.replace(/(^\s*)|(\s*$)/g,"");
nombre = campo;

document.getElementById('lab_telefono_dl').style.display = "none";
if (campo.length==0){
    document.getElementById('lab_telefono_dl').style.display = "inline";
    document.getElementById('lab_telefono_dl').innerHTML = "Edite el teléfono del cliente";    
    error = 1;
}


objeto = "d_direccion";
campo = document.forms[formulario].elements[objeto].value;
campo = campo.replace(/(^\s*)|(\s*$)/g,"");
nombre = campo;

document.getElementById('lab_direccion_dl').style.display = "none";
if (campo.length==0){
    document.getElementById('lab_direccion_dl').style.display = "inline";
    document.getElementById('lab_direccion_dl').innerHTML = "Edite la dirección del cliente";    
    error = 1;
}

return error;
}


function reversarDespacho(idDespacho){
$.ajax({
    url : "<?php echo site_url('Dp_despacho/ajax_reversoDespacho')?>/"+idDespacho,
    type: "GET",                     
    dataType: "JSON",
    success: function(data){
            document.getElementById('r_codigo').value = data.codigoVenta;
            document.getElementById('r_tipo').value = data.tipo_despacho;
            document.getElementById('r_motivo').value=""; 

            document.getElementById('guardar04').disabled=false;
            document.getElementById('cerrar04').disabled=false;
            document.getElementById('lab_motivo').style.display = "none";
            document.getElementById('r_titulo_mostrar').innerHTML = "Reversar Despacho";
            document.getElementById('id_reverso').value = idDespacho;
            $('#reverso_modal').modal('show');    
    },
    error: function (jqXHR, textStatus, errorThrown){
        alert('Error consultando la Base de Datos');
    }
});
}


function guardar_reversarDespacho(){
    formulario = "rev_formulario";
    error = validar_reverso(formulario,1);

    if(error==0){
        document.getElementById('guardar04').disabled=true;
        document.getElementById('cerrar04').disabled=true;
        $.ajax({
            url : "<?=base_url();?>Dp_despacho/ajax_guardar_reversoDespacho",
            type: "POST",
            data: $('#rev_formulario').serialize(),
            dataType: "JSON",
            success: function(data)
            {
            activarBotones(data.tipoDespacho); 
            document.getElementById('tipoDespacho').value = data.tipoDespacho;
            document.getElementById('bodyTabla').innerHTML = data.registro;
            document.getElementById('nroDespachoMesa').innerHTML = data.tipo_1;
            document.getElementById('nroDespachoRetiro').innerHTML = data.tipo_2;
            document.getElementById('nroDespachoDelivery').innerHTML = data.tipo_3;
            $(document).ready(function() {
//                document.getElementById('nombreEmpresa').innerHTML = data.nombreEmpresa;
                $('#basic-datatables').DataTable({
                }); 
            });
            $('#reverso_modal').modal('hide');
        
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error Insertando / modificanco registro, Comuniquese con el administrador');
            }
        });

        swal("El Despacho fué reversado!", {
            icon: "success",
            buttons : {
                confirm : {
                    className: 'btn btn-success'
                }
            }
        });




    }else{
        swal("No se genero el Reverso del Despacho", "Faltan datos", {
            icon : "error",
            buttons: {                  
                confirm: {
                    className : 'btn btn-danger'
                }
            },
        });
    }
}


//// Validar los datos en retiroen tienda
function validar_reverso(formulario,accion){
//accion 1= incluir
//accion 2= modificar
error=0;

objeto = "r_motivo";

campo = document.forms[formulario].elements[objeto].value;
campo = campo.replace(/(^\s*)|(\s*$)/g,"");
nombre = campo;

document.getElementById('lab_motivo').style.display = "none";
if (campo.length==0){
    document.getElementById('lab_motivo').style.display = "inline";
    document.getElementById('lab_motivo').innerHTML = "Edite el motivo de reverso";    
    error = 1;
}

return error;
}




////////////////////////////////////////////////////////////////////////////////////////






</script>
