<?php
$incluir = base_url() . "assets/images/nuevo.png";
$asterisco = ' style="color=red; font-size:16px"';
$ci = &get_instance();
$ci->load->library('Clssession');
$accion= $ci->clssession->accion(7,2);

$rutaInventario = base_url("inventario");
$rutaSalida = base_url("salidaInventario");



?>

        <br>            
        <div class="main-panel">
            <div class="content">
                <div class="page-inner mt--5">

                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <h1><span style="color:blue; text-align:left">Entrada al inventario</span></h1>  
                                <hr>

                                <div class="row">
                                    <div class="col-md-11">                               
                                    <div  style="text-align:left">
                                        <a href="<?=$rutaInventario;?>" class="btn btn-success btn-sm item_edit2" 
                                            style="border-radius: 15px; font-size:16px; font-weight:600;
                                            height:35px;">Inventario</a>
                                        &nbsp;&nbsp;    


                                        <a href="<?=$rutaSalida;?>" class="btn btn-info btn-sm item_edit2"  style="border-radius: 15px; font-size:16px; font-weight:600;
                                            height:35px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Salidas&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>

                                    </div>
                                    </div>


                                    <div class="col-md-1">                                


                                     <?php                                   
                                        if($accion==1){
                                        ?>
                                        <div  style="text-align:right">
                                            <a href="javascript:void(0)" onclick="javascript:add_entrada()"> 
                                               <img src="<?=$incluir;?>" style="width:35px; height:35px" 
                                                    alt="Incluir" title="Incluir entrada al inventario">
                                            </a>
                                        </div>
                                        <?php
                                        }
                                        ?>
                                    </div>
                                </div> 
                                    <br>

                                <div class="table-responsive" style="padding-top: 10px">
    							    <div id="bodyTabla"></div>
                                </div>
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
        </div>    



<?php
$fechaMaxima = date("Y-m-d");
?>



<!-- Modal Incluir producto-->
<div class="modal fade" id="mod_producto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width:800px; margin-left:-100px">

            <div class="modal-header bg-primary text-white">
                <h4 class="modal-title" id="exampleModalLabel" style="color:#fff; font-weight: 600">
                    <div style="font-weight:700" id="titulo_mostrar">Registrar Entrada Al Inventario</div>
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color:white; font-size:30px">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                
            <form name="formularioEntrada" method="post" id="formularioEntrada" autocomplete="off">   
                
                
              <div class="container">


                    <div class="row" style="padding-top: 10px">
                        <div class="col-md-3">
                             <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
                                 <span style="color:#0000ff; font-size:16px; font-weight:600">
                                     Concepto Entrada <span style="color:#ff0000;">*</span></span>
                                 </label>
                        </div>
                       
                        <div class="col-md-9">
                            <div id="cbo_conceptoInEn"></div>
                            <div id="lab_concepto" style="color:red; margin-left:9px; display:none"></div>       
                        </div>
                    </div>  


                    <div class="row" style="padding-top: 10px">
                        <div class="col-md-3">
                             <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
                                 <span style="color:#0000ff; font-size:16px; font-weight:600">
                                     Producto <span style="color:#ff0000;">*</span></span>
                                 </label>
                        </div>
                       
                        <div class="col-md-9">
                            <div id="cbo_productoInEn"></div>
                            <div id="lab_producto" style="color:red; margin-left:9px; display:none"></div>       
                        </div>
                    </div>  



                    <div class="row" style="padding-top: 10px">
                            <div class="col-md-3">
                                 <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
                                     <span style="color:#0000ff; font-size:16px; font-weight:600">
                                         Cantidad <span style="color:#ff0000;">*</span></span>
                                     </label>
                            </div>
                       
                            <div class="col-md-4">

                                <input type="text" name="cantidad" id="cantidad" 
                                       class="form-control" value="0,00" style="text-align:right;" > 

                                <script type="text/javascript">
                                $("#cantidad").maskMoney({symbol:'R$ ', thousands:'.', decimal:',', symbolStay: true}); 
                                </script>

                                    <div id="lab_cantidad" style="color:red; margin-left:9px; display:none"></div>       
                            </div>
                    </div> 

                    <div class="row" style="padding-top: 10px">
                            <div class="col-md-3">
                                 <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
                                     <span style="color:#0000ff; font-size:16px; font-weight:600">
                                         Observación / Nota <span style="color:#ff0000;">*</span></span>
                                     </label>
                            </div>
                       
                            <div class="col-md-9">
                                <textarea style="padding-top: 5px;margin-right:-20px;margin-top: 0px;" 
                                   name="observacion" id="observacion" class="form-control" 
                                   rows="3"></textarea>
                            </div>
                    </div> 





                    <br>  
                    <span style="color:#ff0000; font-size:16px; text-align:left">Campo obligatorio (*)</span><input type="text" name="no_tocar" id="no_tocar" style="border: 0">
                </div>                

            </div>
            <div class="modal-footer">

                   
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrar02"><span style="font-weight:700">Cerrar</span></button>


                <button type="button" class="btn btn-primary" 
                        onclick="guardar_add_entrada();" id="guardar02">
                      <span style="font-weight:700">Guardar</span>
                </button>
            </div>
            </form>  

        </div>
    </div>
</div>
