


<!--cargar datatable-->
<script>
    $(document).ready(function(){
         <?php
         if($modulo==1){  
         ?>	
			 cargarDataTable(0);
		 <?php
		 }
		 ?>

    });		 




function cargarDataTable(id){

    $.ajax({
		url : "<?php echo site_url('In_entrada/ajax_datatable')?>/"+id,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			 document.getElementById('bodyTabla').innerHTML = data.registro;
			 $(document).ready(function() {
				$('#basic-datatables').DataTable({
                    "order": [[ 0, "desc" ]]
			    });	
			 });

		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}


function add_entrada(){


	ruta = "<?php echo site_url('Combo_ctrl/ajax_combo_conceptoInEn')?>";
	cbo_conceptoInEn(ruta,0); 



	ruta = "<?php echo site_url('Combo_ctrl/ajax_combo_productoInEn')?>";
	cbo_productoInEn(ruta,0); 

    document.getElementById('cantidad').value = "0,00";
    
    document.getElementById('lab_producto').style.display = "none";
    document.getElementById('lab_cantidad').style.display = "none";
	document.getElementById('lab_concepto').style.display = "none";


    //document.getElementById('lab_concepto').style.display = "none";
  	document.getElementById('guardar02').disabled=false;
	document.getElementById('cerrar02').disabled=false;         

    $('#mod_producto').modal('show');
}


function guardar_add_entrada(){

    $validar = valEntrada();


    if($validar==0){
    	document.getElementById('guardar02').disabled=true;
		document.getElementById('cerrar02').disabled=true;         
        $.ajax({
            url : "<?=base_url();?>In_entrada/ajax_guardar_entrada",
            type: "POST",
            data: $('#formularioEntrada').serialize(),
            dataType: "JSON",
            success: function(data){
            	if(data.status==0){ 
                    document.getElementById('bodyTabla').innerHTML = data.registro;
					$('#mod_producto').modal('hide');
            }
	        },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error Insertando / modificanco registro, Comuniquese con el administrador');
            }
        });
    }else{
        //document.getElementById('cantidad').value = "0,00";
    	document.getElementById('guardar02').disabled=false;
		document.getElementById('cerrar02').disabled=false;         

		swal("No se actualizo el registro", "Falta de Información", {
			icon : "error",
			buttons: {        			
				confirm: {
					className : 'btn btn-danger'
				}
			},
		});
    }    

}


function valEntrada(){

formulario = "formularioEntrada";
error=0;

objeto = "id_cbo_productoIn";
campo = document.forms[formulario].elements[objeto].value;
document.getElementById('lab_producto').style.display = "none";
if (campo==0){
	document.getElementById('lab_producto').style.display = "inline";
   	document.getElementById('lab_producto').innerHTML = "Seleccione el Producto"; 	
   	error = 1;
}


objeto = "id_cbo_conceptoIn";
campo = document.forms[formulario].elements[objeto].value;
document.getElementById('lab_concepto').style.display = "none";
if (campo==0){
	document.getElementById('lab_concepto').style.display = "inline";
   	document.getElementById('lab_concepto').innerHTML = "Seleccione el Producto"; 	
   	error = 1;
}



objeto = "cantidad";
campo = document.forms[formulario].elements[objeto].value;
campo = campo.replace(/(^\s*)|(\s*$)/g,"");

document.getElementById('lab_cantidad').style.display = "none";


if (campo=="0,00" || campo.length==0){	
	document.getElementById('lab_cantidad').style.display = "inline";
    document.getElementById('lab_cantidad').innerHTML = "Ingrese la cantidad de Producto"; 	
    document.getElementById('cantidad').value = "0,00";
    error = 1;
}

return error;
}


function disponibilidadProducto(){
	//Esta funcion se ejecurara en salida del inventario
	//esta referencia es que cuando selecciones un producto en el combo de la disponibildad del mismo
	//pero para la salida
	
	
	//NO QUITAR
}



//////////////////////////////////////////////////////////////////////////////////////////












</script>
