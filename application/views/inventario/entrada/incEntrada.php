<?php
$incluir = base_url() . "assets/images/nuevo.png";
$asterisco = ' style="color=red; font-size:16px"';

$ruta = base_url() . "In_entrada/index"; 


$fechaMaxima = date("Y-m-d");




?>

<br>            
<div class="main-panel" style="padding-top: 10px">
    <div class="content">
        <div class="page-inner mt--5">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="card card-stats card-round" >
                        <div class="card-body" style="padding-bottom: 5px">
							 <h2><span style="color:blue;">Entrada al inventario</span></h2>
                        </div>
                    </div>
                </div>
            </div>


            <form name="formulario" method="post" id="formulario" autocomplete="off" 
                  enctype="multipart/form-data">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                <div class="card card-stats card-round">
                <div class="card-body">




                <div class="row">
                    <div class="col-md-2">
                        <label>
                        <span style="color:#0000ff; font-size:16px; font-weight:600">
                            Fecha Entrada <span style="color:#ff0000;">*</span>
                        </span>
                        </label>
                        
                        <input type="date" id="m_fecha" name="m_fecha" min="2018-03-25" 
                               max="<?=$fechaMaxima;?>" class="form-control"/>
                        <div id="lab_fecha" style="color:red; margin-left:9px; display:none">
                        </div>
                    </div>

                    <div class="col-md-2">
                        <label>
                        <span style="color:#0000ff; font-size:16px; font-weight:600">
                            Nro Documento
                        </span>
                        </label>
                        <input type="text" name="m_documento" id="m_documento" 
                               class="form-control"
                               maxlength="100" minlength="0">
                    </div>
                    <div class="col-md-5">
                        <label>
                        <span style="color:#0000ff; font-size:16px; font-weight:600">
                            Proveedor
                        </span>
                        </label>
                        <div id="cbo_proveedorIn"></div>
                    </div>
                    <div class="col-md-3">
                        <label>
                        <span style="color:#0000ff; font-size:16px; font-weight:600">
                            Concepto Entrada <span style="color:#ff0000;">*</span>
                        </span>
                        </label>
                        <div id="cbo_conceptoIn"></div>
                        <div id="lab_conceptoIn" style="color:red; margin-left:9px; display:none">
                        </div>
                    </div>
                </div> <!--fin row-->

                <div class="row">
                    <div class="col-md-12">
                        <label>
                        <span style="color:#0000ff; font-size:16px; font-weight:600">
                            Observación
                        </span>
                        </label>
                        <textarea class="form-control" id="m_observacion" 
                                  name="m_observacion" rows="2"></textarea>                                               

                    </div>
                </div>     



            <hr>

            <div class="row">
                <div class="col-md-11">
                    <label>
                    <span style="color:#0000ff; font-size:20px; font-weight:600">
                        Detalle de la entrada <span style="color:#ff0000;">*</span>
                    </span>
                    </label>
                    <div id="lab_detalleIn" 
                         style="color:red; margin-left:9px; display:none">
                    </div>    
                    


                </div>  
                <div class="col-md-1">
                    <a href="javascript:void(0)" onclick="javascript:add_producto()">
                       <img src="<?=$incluir;?>" style="width:35px; height:35px" alt="Incluir">
                    </a>   
                </div>  
            </div>
                    
            <div class="row">
                <div class="col-md-12">
                    <div id="tablaInventario"></div> 

                </div>
            </div>  





            <div class="row">
                <div class="col-md-12" style="text-align: right;">
                    <button type="button" class="btn btn-danger" onclick="cerrar_form()" 
                            id="cerrar01">Cerrar</button>

                    <button type="button" class="btn btn-primary" onclick="guardar_form()" id="guardar01">
                          Guardar
                    </button>                
                </div>    
            </div> 
            
                </div>     
                </div> <!-- Fin card-body-->
                </div> <!-- card card-stats -->
            </div> <!-- col principal -->

            <input type="hidden" id="nroProducto" name="nroProducto" value="0">
            <input type="hidden" id="fechaProducto" id="fechaProducto">
            <input type="hidden" id="ruta_guardar_producto" id="ruta_guardar_producto" 
                   value="<?=$ruta;?>">

            </form>


        </div>
    </div>
</div>    





<!-- Modal Incluir producto-->
<div class="modal fade" id="mod_producto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width:800px; margin-left:-100px">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel" style="color:blue">
                    <div>Ingresar producto</div>
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                
            <form name="formularioProducto" method="post" id="formularioProducto" autocomplete="off">   
                
                
              <div class="container">
                <div class="row" style="padding-top: 10px">
                    <div class="col-md-3">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
                             <span style="color:#0000ff; font-size:16px; font-weight:600">
                                 Producto <span style="color:#ff0000;">*</span></span>
                             </label>
                    </div>
                   
                    <div class="col-md-9">
                        <div id="cbo_productoIn"></div>

                        <div id="lab_producto" style="color:red; margin-left:9px; display:none"></div>       
                    </div>
                </div>  

                <div class="row" style="padding-top: 10px">
                    <div class="col-md-3">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
                             <span style="color:#0000ff; font-size:16px; font-weight:600">
                                 Cantidad <span style="color:#ff0000;">*</span></span>
                             </label>
                    </div>
                   
                    <div class="col-md-4">

                    <input type="text" name="cantidad" id="cantidad" 
                           class="form-control" value="0,00" style="text-align:right;" > 

                    <script type="text/javascript">
                    $("#cantidad").maskMoney({symbol:'R$ ', thousands:'.', decimal:',', symbolStay: true}); 
                    </script>

                        <div id="lab_cantidad" style="color:red; margin-left:9px; display:none"></div>       
                    </div>
                </div>  

                <div class="row" style="padding-top: 10px">
                    <div class="col-md-3">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
                             <span style="color:#0000ff; font-size:16px; font-weight:600">
                                 Precio Unitario <span style="color:#ff0000;">*</span></span>
                             </label>
                    </div>
                   
                    <div class="col-md-4">
                        <input type="text" name="precio" id="precio" 
                               class="form-control" value="0,00" style="text-align:right;">  
                        <script type="text/javascript">
                        $("#precio").maskMoney({symbol:'R$ ', thousands:'.', decimal:',', symbolStay: true}); 
                        </script>
                    </div>
                </div>  



                <br>  
                <span style="color:#ff0000; font-size:16px; text-align:left">Campo obligatorio (*)</span>
              </div>                
            </div>
            <div class="modal-footer">

                   
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrar02">Cerrar</button>


                <button type="button" class="btn btn-primary" 
                        onclick="guardar_add_producto();" id="guardar02">
                      Guardar
                </button>




            </div>

                

            </form>  

        </div>
    </div>
</div>
