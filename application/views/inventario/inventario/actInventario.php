<?php
$incluir = base_url() . "assets/images/nuevo.png";
$asterisco = ' style="color=red; font-size:16px"';

$ci = &get_instance();
$ci->load->library('Clssession');
$permiso_add= $ci->clssession->accion(7,2); 
$rutaEntrada = base_url('entradaInventario');
$rutaSalida = base_url('salidaInventario');



?>
        <br>            
        <div class="main-panel">
            <div class="content">
                <div class="page-inner mt--5">
               

                	<div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <h1><span style="color:blue; text-align:left">Inventario de Producto</span></h1> 


                                    <?php
                                        if($permiso_add==1){
                                    ?>
                                    <div  style="text-align:right">
										<a href="javascript:void(0)" class="btn btn-success btn-sm item_edit2" onclick="javascript:inventarioInicial()" 
											style="border-radius: 15px; font-size:16px; font-weight:600;
											height:35px;">Inventario Inicial</a>
										&nbsp;&nbsp;	


										<a href="<?=$rutaEntrada;?>" class="btn btn-warning btn-sm item_edit2"  
											style="border-radius: 15px; font-size:16px; font-weight:600;
											height:35px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Entradas&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
										&nbsp;&nbsp;	

										<a href="<?=$rutaSalida;?>" class="btn btn-info btn-sm item_edit2" style="border-radius: 15px; font-size:16px; font-weight:600;
											height:35px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Salidas&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>

                                    </div>
                                    <?php
                                        }
                                    ?>

                                    <hr>
                                    <div class="table-responsive">
										<div id="bodyTabla"></div>
										
                                    </div>
                                </div>
                            </div>
                    </div>    
                </div>
            </div>
        </div>    
    



<!-- Modal -->
<div class="modal fade" id="inventario_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content" style="width:800px; margin-left:-100px">
			<div class="modal-header bg-primary text-white">
				<h4 class="modal-title" id="exampleModalLabel" style="color:#fff; font-weight: 600">
					<div style="font-weight:700" id="titulo_mostrar"></div>
				</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true" style="color:white; font-size:30px">&times;</span>
				</button>
			</div>

			<div class="modal-body">
				
			<form name="formulario" method="post" id="formulario" autocomplete="off">	
			  <div class="container">
				<div class="row">
					<div class="col-md-3">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							     Producto <span style="color:#ff0000;">*</span></span>
						</label>
       
					</div>

					<div class="col-md-9">
                            <div id="cbo_productoInventario"></div>
                         <div id="lab_producto" style="color:red; margin-left:9px; display:none"></div>       
                    </div>
      
				</div>


				<div class="row" style="padding-top: 5px">
					<div class="col-md-3">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							     Cantidad</span>
						</label>
       
					</div>

					<div class="col-md-3">
                        <input type="text" name="cantidad" id="cantidad" 
                               class="form-control" value="0,00" style="text-align:right;" > 

                        <script type="text/javascript">
                        $("#cantidad").maskMoney({symbol:'R$ ', thousands:'.', decimal:',', symbolStay: true}); 
                        </script>
                    </div>
				</div>


				<div class="row" style="padding-top: 5px">
					<div class="col-md-3">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							     Stock Mínimo </span>
						</label>
       
					</div>

					<div class="col-md-3">
                        <input type="text" name="stock" id="stock" 
                               class="form-control" value="0,00" style="text-align:right;" > 
                        <script type="text/javascript">
                        $("#stock").maskMoney({symbol:'R$ ', thousands:'.', decimal:',', symbolStay: true}); 
                        </script>
                        </div>
				</div>

                <br>  
				<span style="color:#ff0000; font-size:16px; text-align:left">Campo obligatorio (*)</span>
			  </div>	

			   </form> 


			</div>
			<div class="modal-footer">

                   
				<button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrar01"><span style="font-weight:700">Cerrar</span></button>


		        <button type="button" class="btn btn-primary" onclick="inventario_form()" id="guardar01">
		              <span style="font-weight:700">Guardar</span>
		        </button>

			</div>

		</div>
	</div>
</div>


<!-- Modal -->
<div class="modal fade" id="stock_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content" style="width:800px; margin-left:-100px">
			<div class="modal-header bg-primary text-white">
				<h4 class="modal-title" id="exampleModalLabel" style="color:#fff; font-weight: 600">
					<div style="font-weight:700">Actualizar Stock Mínimo</div>
				</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true" style="color:white; font-size:30px">&times;</span>
				</button>
			</div>

			<div class="modal-body">
				
			<form name="formulario_stock" method="post" id="formulario_stock" autocomplete="off">	
				
				
			  <div class="container">

				<div class="row" style="padding-top: 5px">
					<div class="col-md-3">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							     Stock Mínimo </span>
						</label>
       
					</div>

					<div class="col-md-3">
                        <input type="text" name="stockM" id="stockM" 
                               class="form-control" style="text-align:right;" > 
                        <script type="text/javascript">
                        $("#stockM").maskMoney({symbol:'R$ ', thousands:'.', decimal:',', symbolStay: true}); 
                        </script>
                        </div>
				</div>
				<input type="text" name="no_tocar" id="no_tocar" style="border: 0">
				<input type="hidden" id="idInventario" name="idInventario">

			  </div>				
            </form>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrar02"><span style="font-weight:700">Cerrar</span></button>
		        <button type="button" class="btn btn-primary" onclick="stock_form()" id="guardar02">
		              <span style="font-weight:700">Guardar</span>
		        </button>
			</div>
			            </form>

		</div>
	</div>
</div>
