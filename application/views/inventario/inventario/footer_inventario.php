



<!--cargar datatable-->
<script>
    $(document).ready(function(){
		 cargarDataTable(0);
	});


function cargarDataTable(id){
    $.ajax({
		url : "<?php echo site_url('In_inventario/ajax_datatable')?>/"+id,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			 document.getElementById('bodyTabla').innerHTML = data.registro;

			 $(document).ready(function() {
				$('#basic-datatables').DataTable({
			    });	
			 });

		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}


function inventarioInicial(){

    document.getElementById('titulo_mostrar').innerHTML = "Inventario Inicial Del  Producto";
	document.getElementById('guardar01').disabled=false;
	document.getElementById('cerrar01').disabled=false;    

	document.getElementById('cantidad').value="0,00";
	document.getElementById('stock').value="0,00";

    document.getElementById('lab_producto').style.display = "none";
	ruta = "<?php echo site_url('Combo_ctrl/ajax_combo_productoInventario')?>";
	cbo_productoInventario(ruta,0);   
    $('#inventario_modal').modal('show');

}


function inventario_form(){

    //validar campos formulario
	formulario = "formulario";
	error=0;
	error = validar_form(formulario,1);
    if(error==0){
		document.getElementById('guardar01').disabled=true;
		document.getElementById('cerrar01').disabled=true;
    }

	if(error == 0){
        $.ajax({
            url : "<?=base_url();?>In_inventario/ajax_guardar_add",
            type: "POST",
            data: $('#formulario').serialize(),
            dataType: "JSON",
            success: function(data)
            {
            	if(data.status==0){ 
					 document.getElementById('bodyTabla').innerHTML = data.registro;
					 $('#inventario_modal').modal('hide');

					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
				 	});            	


					swal("Registro almacenado Correctamente!", "", {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					});
				}	
	        },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error Insertando / modificanco registro, Comuniquese con el administrador');
            }
        });
    }else{
		swal("No se actualizo el registro", "", {
			icon : "error",
			buttons: {        			
				confirm: {
					className : 'btn btn-danger'
				}
			},
		});
    }    

}



function stockMinimo(idInventario){

	document.getElementById('guardar02').disabled=false;
	document.getElementById('cerrar02').disabled=false;       
    //document.getElementById('url_mostrar').style.display = "none";

    $.ajax({
		url : "<?php echo site_url('In_inventario/ajax_modificar')?>/"+idInventario,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){

			 document.getElementById('idInventario').value = data.id;
			 document.getElementById('stockM').value = data.stock;

             $('#stock_modal').modal('show');
		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}


function stock_form(){
	//validar campos formulario
	formulario = "formulario_stock";
	error=0;
	//error = validar_form(formulario,2);

    //if(error==0){
		document.getElementById('guardar02').disabled=true;
		document.getElementById('cerrar02').disabled=true;
    //}	

	if(error == 0){
        $.ajax({
            url : "<?=base_url();?>In_inventario/ajax_guardar_stock_mod",
            type: "POST",
            data: $('#formulario_stock').serialize(),
            dataType: "JSON",
            success: function(data)
            {
            	if(data.status==0){ 
					 document.getElementById('bodyTabla').innerHTML = data.registro;
					 $('#stock_modal').modal('hide');

					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
				 	});            	


					swal("Registro almacenado Correctamente!", "", {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					});
				}	
	        },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error Insertando / modificanco registro, Comuniquese con el administrador');
            }
        });
    }else{
		swal("No se actualizo el registro", "", {
			icon : "error",
			buttons: {        			
				confirm: {
					className : 'btn btn-danger'
				}
			},
		});
    }    








}



function validar_form(formulario,accion){
//accion 1= incluir
//accion 2= modificar

error=0;


objeto = "id_cbo_productoIn";
campo = document.forms[formulario].elements[objeto].value;
document.getElementById('lab_producto').style.display = "none";
if (campo==0){
	document.getElementById('lab_producto').style.display = "inline";
   	document.getElementById('lab_producto').innerHTML = "Seleccione el Producto"; 	
   	error = 1;
}


return error;
}



</script>
