 	<script src="<?= base_url('assets/assets_login1/vendor/jquery/jquery-3.2.1.min.js') ?>"></script>
   	<script src="<?= base_url('assets/assets_login1/vendor/bootstrap/js/popper.js') ?>"></script>
  	<script src="<?= base_url('assets/assets_login1/vendor/bootstrap/js/bootstrap.min.js') ?>"></script>
   	<script src="<?= base_url('assets/assets_login1/vendor/animsition/js/animsition.min.js') ?>"></script>
 	<script src="<?= base_url('assets/assets_login1/vendor/countdowntime/countdowntime.js') ?>"></script>
  	<script src="<?= base_url('assets/assets_login1/js/main.js') ?>"></script>
  	<script src="<?php echo base_url()?>assets/assets_sistema/toastr.min.js"></script>
</body>
</html>

<script>

	
    $(document).ready(function(){
         <?php  
         if($administrador==2){  
         ?>
	    	loginEmpresa(0);
	     <?php  	
	     }
	     ?>
	    
         <?php  
         if($administrador==3){  
         ?>
	    	loginSucursal(0);
	     <?php  	
	     }
	     ?>

         <?php  
         if($administrador==10){  
         ?>
	    	menuEmpresa(0);
	     <?php  	
	     }
	     ?>


         <?php  
         if($administrador==11){  
         ?>
	    	menuSucursal(0);
	     <?php  	
	     }
	     ?>	     


	});
	
    function loginEmpresa(id){
		ruta = "<?php echo site_url('Combo_ctrl/ajax_combo_EmpresaAdministrador')?>/0";
		$.ajax({
			url : ruta,
			type: "GET",                     
			dataType: "JSON",
			success: function(data){
				//$('.cbo_modelo').html(response).fadeIn();
				document.getElementById('cbo_empresaAdministrador').innerHTML = data.combo;
			}
		});
	}



    function loginSucursal(id){
		ruta = "<?php echo site_url('Combo_ctrl/ajax_combo_EmpresaLoginAdministrador')?>/0";
		$.ajax({
			url : ruta,
			type: "GET",                     
			dataType: "JSON",
			success: function(data){
				//$('.cbo_modelo').html(response).fadeIn();
				document.getElementById('cbo_empresaLoginAdministrador').innerHTML = data.combo;
			}
		});

        
		ruta = "<?php echo site_url('Combo_ctrl/ajax_combo_SucursalLoginAdministrador')?>/0";
		$.ajax({
			url : ruta,
			type: "GET",                     
			dataType: "JSON",
			success: function(data){
				//$('.cbo_modelo').html(response).fadeIn();
				document.getElementById('cbo_sucursalLoginAdministrador').innerHTML = data.comboSuc;
			}
		});
		
	}


	function cbo_sel_sucursal(){
	    idEmpresa = document.getElementById('id_cbo_empresaLoginAdministrador').value;
		ruta = "<?php echo site_url('Combo_ctrl/ajax_combo_SucursalLoginAdministrador')?>/"+idEmpresa;
		$.ajax({
			url : ruta,
			type: "GET",                     
			dataType: "JSON",
			success: function(data){
				//$('.cbo_modelo').html(response).fadeIn();
				document.getElementById('cbo_sucursalLoginAdministrador').innerHTML = data.comboSuc;
			}
		});
	}	


	function menuEmpresa(id){
         document.location.href = "<?php echo base_url('generaMenuEmpresa')?>";   
	}



	function menuSucursal(id){
         document.location.href = "<?php echo base_url('generarMenu_adminSucursal')?>"; 
	}
	
	
</script>
