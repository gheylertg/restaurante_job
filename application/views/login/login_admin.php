<?php
$imagen = base_url() . "assets/images/sushi01.jpeg";
$titulo = "Sistema de Restautante Sushi";
$ruta = base_url("accesoAdmin");

?>


<link href="<?php echo base_url()?>assets/assets_login1/css/main.css" rel="stylesheet" type="text/css">

<!--===============================================================================================-->
</head>
<body style="background-color: #666666;">	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">

				 <form class="login100-form validate-form" id="formulario" autocomplete="off"> 
					<span class="login100-form-title p-b-43">
						<?=$titulo; ?>
					</span>

					<div class='row' >
	     				<div id="error_validar" style="display:none; " class=" col-md-12 alert alert-danger">
	     				</div>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Alerta Usuario es requerido">
						<input class="input100" type="text" name="usuario" id="usuario">
	            		<span class="focus-input100"></span>
	    		        <span class="label-input100">RUN</span>
					</div>		
					
					<div class="wrap-input100 validate-input" data-validate="Alerta Password es requerido">
						<input class="input100" type="password" name="pass" id="pass">
						<span class="focus-input100"></span>
						<span class="label-input100">Password</span>
					</div>

					<div class="container-login100-form-btn">
						<button type="button" class="login100-form-btn" onclick="validar()">
							Acceder
						</button>
					</div>

					<input type="hidden" id="ruta" name="ruta" value="<?=$ruta;?>">
				
				</form>

				<div class="login100-more" 
				     style="background-image: url('<?php echo base_url()?>assets/images/sushi01.jpeg?>');">
				</div>
			</div>
		</div>
	</div>
	
	
    <script>
    function validar(){
		error=0;
		formulario = "formulario";
		objeto = "usuario";
		campo = document.forms[formulario].elements[objeto].value;
		campo = campo.replace(/(^\s*)|(\s*$)/g,"");
		usuario = campo.replace(/(^\s*)|(\s*$)/g,"");
		if (campo.length==0){
		    error = 1;
		}
		objeto = "pass";
		campo = document.forms[formulario].elements[objeto].value;
		campo = campo.replace(/(^\s*)|(\s*$)/g,"");
		clave = campo.replace(/(^\s*)|(\s*$)/g,"");
		if (campo.length==0){
		    error = 1;
		}
		
		if(error==0){
			$.ajax({
				url : "<?=base_url();?>Acceso/accesoAdmin",
				type: "POST",
				data: $('#formulario').serialize(),
				dataType: "JSON",
				success: function(data){
					if(data.status==0){
						document.location.href = "<?php echo base_url('generarMenuAdmin')?>";
					}else{
						document.getElementById('error_validar').style.display = "inline";
						document.getElementById('error_validar').innerHTML = "Acceso denegado. Verifique el RUN y el Password ";
						swal("Acceso denegado", "Información incorrecta", {
							icon : "error",
							buttons: {        			
								confirm: {
									className : 'btn btn-danger'
								}
							},
						});
					}	
				},
				error: function (jqXHR, textStatus, errorThrown)
				{
					alert('Error Insertando / modificanco registro, Comuniquese con el administrador');
				}
			});
        }else{
            document.getElementById('error_validar').style.display = "inline";
            document.getElementById('error_validar').innerHTML = "Acceso denegado. Verifique el RUN y el Password ";
           
			swal("No se pudo validar el acceso", "Falta Información", {
				icon : "error",
				buttons: {        			
					confirm: {
						className : 'btn btn-danger'
					}
				},
			});

		}
    }    




 
    </script>
  
