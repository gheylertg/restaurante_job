<?php
$incluir = base_url() . "assets/images/nuevo.png";
$asterisco = ' style="color=red; font-size:16px"';

$ci = &get_instance();
$ci->load->library('Clssession');


?>

       <br>            
        <div class="main-panel">
            <div class="content">
                <div class="page-inner mt--5">
                    <div class="row row-card-no-pd mt--2">
                        <div class="col-sm-12 col-md-12">
                            <div class="card card-stats card-round">
                                <div class="card-body" style="text-align:center">
									 <h3><span style="color:blue;">Seleccione Empresa</span></h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
                
                <br>
                
                <?php 
                $fila = 0;
                $columna=0;
                foreach($empresa as $key){ 
					$imagen = base_url() . "assets/images/img_empresa/".$key->logo;
					$ruta = base_url() . "Acceso/empresaSeleccionada/".$key->id_empresa."/".$key->id;
					$columna+=1;
					if($fila==0){
						$fila = 1; 
				?>	
						<div class="row">
							<div class="col-md-1"></div>
							
					<?php
					}		
					?>
						<div class="col-md-3">
							<div class="card">
								<div class="card-body" style="text-align:center">
									
									<a href="<?=$ruta;?>">
									    <span><?=$key->empresa;?></span><br>
										<img src="<?=$imagen;?>" style="width:140px; height:140px" alt="Imagen" title="Modificar Imagen">
									</a>
								</div>
							</div>	
						</div>						

				<?php
					if($columna==3){
						$columna=0;
						$fila=0;
				?>		
						<div class="col-md-2"></div>
						</div>  				
					<?php	
				    }
				}
				if($columna!=3){
				?>	
				<div class="col-md-2"></div>
                </div>  				
                <?php
				}
				?>
			</div>
		</div>	

