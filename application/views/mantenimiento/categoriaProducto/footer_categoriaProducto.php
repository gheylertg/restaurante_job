


<!--cargar datatable-->
<script>
    $(document).ready(function(){
		 cargarDataTable(0);
	});





function cargarDataTable(id){
    $.ajax({
		url : "<?php echo site_url('Mt_categoriaProducto/ajax_datatable')?>/"+id,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			 document.getElementById('bodyTabla').innerHTML = data.registro;

			 $(document).ready(function() {
				$('#basic-datatables').DataTable({
			    });	
			 });

		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}


//cargar combos





function actualizar_form(){
    accion = document.getElementById('accion_formulario').value;
    if(accion==1){
        guardar_add_form();
    }else{
        guardar_mod_form();
    } 


}


function add_form(){
	$('#formulario')[0].reset(); // reset form on modals

    document.getElementById('accion_formulario').value="1";
    document.getElementById('titulo_mostrar').innerHTML = "Incluir Categoria de Producto";

    document.getElementById('guardar01').disabled=false;
	document.getElementById('cerrar01').disabled=false;     	


    //document.getElementById('url_mostrar').style.display = "inline";    
	//ruta = "<?php //echo site_url('Combo_ctrl/ajax_combo_modulo')?>";
	//cbo_modulo(ruta,0);   //Llenar combo    

    document.getElementById('nombre_orig').value = "";
    $('#mod_modal').modal('show');


}


function modificar_form(id){

    //cbo_modelo(ruta,data.id_proyecto_local,data.id_proyecto_local);   //Llenar combo    

    document.getElementById('accion_formulario').value="2";
    document.getElementById('titulo_mostrar').innerHTML = "Modificar Categoria Producto";
    //document.getElementById('url_mostrar').style.display = "none";

    document.getElementById('guardar01').disabled=false;
	document.getElementById('cerrar01').disabled=false;     

    $.ajax({
		url : "<?php echo site_url('Mt_categoriaProducto/ajax_modificar')?>/"+id,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			 document.getElementById('m_nombre').value = data.nombre;
			 document.getElementById('id_mod').value = data.id;
			 document.getElementById('val_error').value = "0";
			 document.getElementById('lab_nombre').style.display = "none";
			 document.getElementById('nombre_orig').value = data.nombre;
			 //ruta = "<?php //echo site_url('Combo_ctrl/ajax_combo_modulo')?>";
	         //cbo_modulo(ruta,data.id_modulo);   //Llenar combo    
             $('#mod_modal').modal('show');

		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}



function guardar_add_form(){
	//validar campos formulario
	formulario = "formulario";
	error=0;
	error = validar_form(formulario,1);

	if(error==0){
    	document.getElementById('guardar01').disabled=true;
		document.getElementById('cerrar01').disabled=true; 		
	}

	if(error == 0){
        $.ajax({
            url : "<?=base_url();?>Mt_categoriaProducto/ajax_guardar_add",
            type: "POST",
            data: $('#formulario').serialize(),
            dataType: "JSON",
            success: function(data)
            {
            	if(data.status==0){ 
					 document.getElementById('bodyTabla').innerHTML = data.registro;
					 $('#mod_modal').modal('hide');

					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
				 	});            	


					swal("Registro almacenado Correctamente!", "", {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					});
				}else{
					document.getElementById('lab_nombre').style.display = "inline";
					document.getElementById('lab_nombre').innerHTML = "El Nombre ingresado, ya se encuentra registrado.";
			    	document.getElementById('guardar01').disabled=false;
					document.getElementById('cerrar01').disabled=false; 		

					swal("No se actualizo el registro", "", {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});

				}	
	        },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error Insertando / modificanco registro, Comuniquese con el administrador');
            }
        });
    }else{
		swal("No se actualizo el registro", "", {
			icon : "error",
			buttons: {        			
				confirm: {
					className : 'btn btn-danger'
				}
			},
		});
    }    






} //Fin de add form	

function guardar_mod_form(){

	//validar campos formulario
	formulario = "formulario";
	error=0;
	error = validar_form(formulario,2);
	if(error==0){
    	document.getElementById('guardar01').disabled=true;
		document.getElementById('cerrar01').disabled=true; 		
	}
	

	if(error == 0){
        $.ajax({
            url : "<?=base_url();?>Mt_categoriaProducto/ajax_guardar_mod",
            type: "POST",
            data: $('#formulario').serialize(),
            dataType: "JSON",
            success: function(data)
            {
            	if(data.status==0){ 
					 document.getElementById('bodyTabla').innerHTML = data.registro;
					 $('#mod_modal').modal('hide');

					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
				 	});            	


					swal("Registro almacenado Correctamente!", "", {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					});
				}else{
					document.getElementById('lab_nombre').style.display = "inline";
					document.getElementById('lab_nombre').innerHTML = "El Nombre ingresado, ya se encuentra registrado.";
			    	document.getElementById('guardar01').disabled=false;
					document.getElementById('cerrar01').disabled=false; 		
					swal("No se actualizo el registro", "", {

						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});

				}	
	        },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error Insertando / modificanco registro, Comuniquese con el administrador');
            }
        });
    }else{
		swal("No se actualizo el registro", "", {
			icon : "error",
			buttons: {        			
				confirm: {
					className : 'btn btn-danger'
				}
			},
		});
    }    
}







function eliminar_form(id){
	swal({
		title: 'Esta seguro de ?',
			type: 'warning',
			buttons:{
				cancel: {
					visible: true,
						text : 'No, Cancelar!',
								className: 'btn btn-danger'
				},        			
				confirm: {
					text : 'Si, Desactivar!',
					className : 'btn btn-success'
				}
			}
	}).then((willDelete) => {
		if (willDelete) {
		    $.ajax({
				url : "<?php echo site_url('Mt_categoriaProducto/ajax_desactivar')?>/"+id,
				type: "GET",                     
				dataType: "JSON",
				success: function(data){
					 document.getElementById('bodyTabla').innerHTML = data.registro;
					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
					 }); 
				},
				error: function (jqXHR, textStatus, errorThrown){
					alert('Error consultando la Base de Datos');
				}
			});
			swal("El registro fué desactivado!", {
				icon: "success",
				buttons : {
					confirm : {
						className: 'btn btn-success'
					}
				}
			});
		}
	});

}

function reactivar_form(id){
	swal({
		title: 'Esta seguro de ?',
			type: 'warning',
			buttons:{
				cancel: {
					visible: true,
						text : 'No, Cancelar!',
								className: 'btn btn-danger'
				},        			
				confirm: {
					text : 'Si, Reactivar!',
					className : 'btn btn-success'
				}
			}
	}).then((willDelete) => {
		if (willDelete) {
		    $.ajax({
				url : "<?php echo site_url('Mt_categoriaProducto/ajax_reactivar')?>/"+id,
				type: "GET",                     
				dataType: "JSON",
				success: function(data){
					 document.getElementById('bodyTabla').innerHTML = data.registro;
					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
					 }); 
				},
				error: function (jqXHR, textStatus, errorThrown){
					alert('Error consultando la Base de Datos');
				}
			});
			swal("El registro fué reactivado!", {
				icon: "success",
				buttons : {
					confirm : {
						className: 'btn btn-success'
					}
				}
			});
		}
	});
}







function validar_form(formulario,accion){
//accion 1= incluir
//accion 2= modificar

error=0;

objeto = "m_nombre";
campo = document.forms[formulario].elements[objeto].value;
campo = campo.replace(/(^\s*)|(\s*$)/g,"");
nombre = campo;

document.getElementById('lab_nombre').style.display = "none";

if (campo.length==0){
	document.getElementById('lab_nombre').style.display = "inline";
    document.getElementById('lab_nombre').innerHTML = "Edite el nombre"; 	
    error = 1;
}

return error;
}



</script>
