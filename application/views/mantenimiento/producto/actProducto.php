<?php
$incluir = base_url() . "assets/images/nuevo.png";
$asterisco = ' style="color=red; font-size:16px"';

$ci = &get_instance();
$ci->load->library('Clssession');
$permiso_add= $ci->clssession->accion(5,2); 



?>
        <br>            
        <div class="main-panel">
            <div class="content">
                <div class="page-inner mt--5">
                    <div class="row row-card-no-pd mt--2">
                        <div class="col-sm-12 col-md-12">
                            <div class="card card-stats card-round">
                                <div class="card-body ">
									 <h2><span style="color:blue">Producto</span></h2>
                                </div>
                            </div>
                        </div>
                    </div>

            

               

                <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <?php
                                        if($permiso_add==1){
                                    ?>
                                    <div  style="text-align:right">
										<a href="javascript:void(0)" onclick="javascript:add_form()">
										   <img src="<?=$incluir;?>" style="width:35px; height:35px" alt="Incluir" title="Incluir Producto del Inventario">
										</a>
                                    </div>
                                    <?php
                                        }
                                    ?>
                                    <br>



                                    <div class="table-responsive">
										<div id="bodyTabla">
										</div>
										
										
                                    </div>
                                </div>
                            </div>
                        </div>    
                </div>
            </div>
    



<!-- Modal -->
<div class="modal fade" id="mod_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content" style="width:800px; margin-left:-100px">
			<div class="modal-header">
				<h4 class="modal-title" id="exampleModalLabel" style="color:blue">
					<div id="titulo_mostrar"></div>
				</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				
			<form name="formulario" method="post" id="formulario" autocomplete="off">	
				
				
			  <div class="container">
				<div class="row">
					<div class="col-md-3">
                         <label style="padding-top: 0px; margin-top: 5px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							     Codigo <span style="color:#ff0000;">*</span></span>
						</label>
					</div>
					<div class="col-md-9">
                        <input style="padding-top: 5px;margin-right:-20px;margin-top: 0px;" 
                               type="text" name="m_codigo" id="m_codigo" class="form-control" 
                               placeholder="Edite el Codigo" maxlength="100" minlength="0"                                >
                        <div id="lab_codigo" style="color:red; margin-left:9px; display:none"></div>       
					</div>
				</div>
					
                <div class="row" style="padding-top: 5px"> 
					<div class="col-md-3">
                         <label style="padding-top: 0px; margin-top: 5px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							     Nombre <span style="color:#ff0000;">*</span></span>
						</label>
					</div>
					<div class="col-md-9">
                        <input style="padding-top: 5px;margin-right:-20px;margin-top: 0px;" 
                               type="text" name="m_nombre" id="m_nombre" class="form-control" 
                               placeholder="Edite el nombre" maxlength="100" minlength="0"                                >
                        <div id="lab_nombre" style="color:red; margin-left:9px; display:none"></div>       
					</div>
				</div>
				
                <div class="row" style="padding-top: 5px">
					<div class="col-md-3">
                         <label style="padding-top: 0px; margin-top: 5px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">Categoría <span style="color:#ff0000;">*</span></span>
						 </label>
					</div>
					<div class="col-md-9">
						<div id="cbo_categoriaProducto"></div>
						<div id="lab_categoriaProducto" style="color:red; margin-left:9px; display:none"></div>
					</div>
				</div>	
				<div class="row" style="padding-top: 5px">
					<div class="col-md-3">
                         <label style="padding-top: 0px; margin-top: 5px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">Unidad Medida <span style="color:#ff0000;">*</span></span>
						 </label>
					</div>
					<div class="col-md-9">
						<div id="cbo_unidadMedida"></div>
						<div id="lab_unidadMedida" style="color:red; margin-left:9px; display:none"></div>
					</div>
				</div>	

                <br>  
				<span style="color:#ff0000; font-size:16px; text-align:left">Campo obligatorio (*)</span>
			  </div>				
			</div>
			<div class="modal-footer">

                   
				<button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrar01">Cerrar</button>


		        <button type="button" class="btn btn-primary" onclick="actualizar_form()" id="guardar01">
		              Guardar
		        </button>

			</div>


            <input type="hidden" id="id_mod" name="id_mod">
            <input type="hidden" id="nombre_orig" name="nombre_orig">
            <input type="hidden" id="val_error" name="val_error" value="0">
            <input type="hidden" id="accion_formulario" name="accion_formulario" value="0">

			</form>  

		</div>
	</div>
</div>



<!--- ver productos -->

<!-- Modal -->
<div class="modal fade" id="mod_ver" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content" style="width:800px; margin-left:-100px">
			<div class="modal-header">
				<h4 class="modal-title" id="exampleModalLabel" style="color:blue">
					Datos del Producto
				</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				
			<form name="formulario" method="post" id="formulario" autocomplete="off">	
				
				
			  <div class="container">
				<div class="row">
					<div class="col-md-3">
                         <label style="padding-top: 0px; margin-top: 5px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							     Codigo <span style="color:#ff0000;">*</span></span>
						</label>
					</div>	
					<div class="col-md-6">
                        <input style="padding-top: 5px;margin-right:-20px;margin-top: 0px;" 
                               type="text" name="v_codigo" id="v_codigo" class="form-control" 
                               maxlength="100" minlength="0" 
                               readonly="true">
					</div>
				</div>	
				
				<div class="row" style="padding-top: 5px">
					<div class="col-md-3">
                         <label style="padding-top: 0px; margin-top: 5px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							     Nombre <span style="color:#ff0000;">*</span></span>
						</label>
					</div>
					<div class="col-md-9">
                        <input style="padding-top: 5px;margin-right:-20px;margin-top: 0px;" 
                               type="text" name="v_nombre" id="v_nombre" class="form-control" 
                                maxlength="100" minlength="0"
                               readonly="true">
					</div>
				</div>
				
                <div class="row" style="padding-top: 5px">
					<div class="col-md-3">
                         <label style="padding-top: 0px; margin-top: 5px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">Categoría <span style="color:#ff0000;">*</span></span>
						 </label>
					</div>
					<div class="col-md-9">
                        <input style="padding-top: 5px;margin-right:-20px;margin-top: 0px;" 
                               type="text" name="v_categoria" id="v_categoria" class="form-control" 
                               maxlength="100" minlength="0"
                               readonly="true">
					</div>
				</div>	
				<div class="row" style="padding-top: 5px">
					<div class="col-md-3">
                         <label style="padding-top: 0px; margin-top: 5px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">Unidad Medida <span style="color:#ff0000;">*</span></span>
						 </label>
					</div>
					<div class="col-md-9">
                        <input style="padding-top: 5px;margin-right:-20px;margin-top: 0px;" 
                               type="text" name="v_unidad" id="v_unidad" class="form-control" 
                               maxlength="100" minlength="0"
                               readonly="true">
					</div>

				</div>	

                <br>  
				<span style="color:#ff0000; font-size:16px; text-align:left">Campo obligatorio (*)</span>
			  </div>				
			</div>
			<div class="modal-footer">

                   
				<button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrar01">Cerrar</button>



			</div>


            <input type="hidden" id="id_mod" name="id_mod">
            <input type="hidden" id="nombre_orig" name="nombre_orig">
            <input type="hidden" id="val_error" name="val_error" value="0">
            <input type="hidden" id="accion_formulario" name="accion_formulario" value="0">

			</form>  

		</div>
	</div>
</div>
