<?php
$incluir = base_url() . "assets/images/nuevo.png";
$asterisco = ' style="color=red; font-size:16px"';

$ci = &get_instance();
$ci->load->library('Clssession');
$accion= $ci->clssession->accion(5,2); 



?>


        <br>            
        <div class="main-panel">
            <div class="content">
                <div class="page-inner mt--5">
                    <div class="row row-card-no-pd mt--2">
                        <div class="col-sm-12 col-md-12">
                            <div class="card card-stats card-round">
                                <div class="card-body ">
									 <h2><span style="color:blue">Actualizar Proveedor</span></h2>
                                </div>
                            </div>
                        </div>
                    </div>

                <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                <?php
                                if($accion==1){
                                ?>
                                <div  style="text-align:right">
                                    <a href="javascript:void(0)" onclick="javascript:add_form()"> 
                                       <img src="<?=$incluir;?>" style="width:35px; height:35px" 
                                            alt="Incluir" title="Incluir Proveedor">
                                    </a>
                                </div>
                                <?php
                                }
                                ?>
                                <br>
                                <div class="table-responsive">
									<div id="bodyTabla">
									</div>
										
										
                                    </div>
                                </div>
                            </div>
                        </div>    
                </div>
            </div>
    




<!-- Modal -->
<div class="modal fade" id="mod_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content" style="width:800px; margin-left:-100px">

			<div class="modal-header bg-primary text-white">
				<h4 class="modal-title" id="exampleModalLabel" style="color:#fff; font-weight: 600">
					<div style="font-weight:700" id="titulo_mostrar"></div>
				</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true" style="color:white; font-size:30px">&times;</span>
				</button>
			</div>

			<div class="modal-body">
				
			<form name="formulario" method="post" id="formulario" autocomplete="off">	

			  <div class="container">
				<div class="row">
					<div class="col-md-3">
                        <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							     RUT <span style="color:#ff0000;">*</span></span>
							 </label>
                        <input style="padding-top: 5px;margin-right:-20px;margin-top: 12px;" 
                               type="text" name="m_codigo" id="m_codigo" class="form-control" 
                               placeholder="Edite el Código" maxlength="100" 
                               minlength="0">
                        <div id="lab_codigo" style="color:red; margin-left:9px; display:none"></div>       
					</div>

					<div class="col-md-9">
                        <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							     Nombre <span style="color:#ff0000;">*</span></span>
							 </label>
                        <input style="padding-top: 5px;margin-right:-20px;margin-top: 12px;" 
                               type="text" name="m_nombre" id="m_nombre" class="form-control" 
                               placeholder="Edite el nombre" maxlength="100" minlength="0"                                >
                        <div id="lab_nombre" style="color:red; margin-left:9px; display:none"></div>       
					</div>
				</div>	


				<div class="row">
					<div class="col-md-12">
                        <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							     Correo Electrónico <span style="color:#ff0000;">*</span></span>
							 </label>
                        <input style="padding-top: 5px;margin-right:-20px;margin-top: 12px;" 
                               type="text" name="m_correo" id="m_correo" class="form-control" 
                               placeholder="Edite el Correo Electrónico" maxlength="100" 
                               minlength="0">
                        <div id="lab_correo" style="color:red; margin-left:9px; display:none"></div>       
					</div>
				</div>	


				<div class="row">
					<div class="col-md-12">
                        <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							     Contacto</span>
							 </label>
                        <input style="padding-top: 5px;margin-right:-20px;margin-top: 12px;" 
                               type="text" name="m_contacto" id="m_contacto" class="form-control" 
                               placeholder="Edite el Contacto" maxlength="100" 
                               minlength="0">

					</div>
				</div>	

				<div class="row">
					<div class="col-md-12">
                        <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							     Dirección</span>
							 </label>
                                <textarea class="form-control" id="m_direccion" 
                                          name="m_direccion" rows="2"></textarea>                                               

					</div>
				</div>	

				<div class="row">
					<div class="col-md-6">
                        <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							     Teléfono Local</span>
							 </label>
                        <input style="padding-top: 5px;margin-right:-20px;margin-top: 12px;" 
                               type="text" name="m_telefono_local" id="m_telefono_local" class="form-control" 
                               placeholder="Edite el Teléfono Local" maxlength="100" 
                               minlength="0">
					</div>
					<div class="col-md-6">
                        <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							     Teléfono Celular</span>
							 </label>
                        <input style="padding-top: 5px;margin-right:-20px;margin-top: 12px;" 
                               type="text" name="m_telefono_celular" id="m_telefono_celular" class="form-control" 
                               placeholder="Edite el Teléfono Celular" maxlength="100" 
                               minlength="0">
					</div>

				</div>	



                <br>  
				<span style="color:#ff0000; font-size:16px; text-align:left">Campo obligatorio (*)</span>
			  </div>				
			</div>
			<div class="modal-footer">

                   
				<button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrar01"><span style="font-weight:700">Cerrar</span></button>


		        <button type="button" class="btn btn-primary" onclick="actualizar_form();" 
		                id="guardar01">
		              <span style="font-weight:700">Guardar</span>
		        </button>

			</div>


            <input type="hidden" id="id_mod" name="id_mod">
            <input type="hidden" id="nombre_orig" name="nombre_orig">
            <input type="hidden" id="val_error" name="val_error" value="0">
            <input type="hidden" id="accion_formulario" name="accion_formulario" value="0">

			</form>  

		</div>
	</div>
</div>

 
