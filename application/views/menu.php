<?php
$ci =& get_instance();



$idusuario = $ci->session->userdata("idUsuario");
$arregloPermisoModulo = $ci->session->userdata("arregloPermisoModulo");
$nroFilaModulo = $ci->session->userdata("nroFilaModulo");
$arregloPermisoSM = $ci->session->userdata("arregloPermisoSM");
$nroFilaSM = $ci->session->userdata("nroFilaSM");


$nombreUsuario = $ci->session->userdata("nombreUsuario");
$sexo = $ci->session->userdata("sexo");

$logoEmpresa = $ci->session->userdata("logoEmpresa");


$empresa = $ci->session->userdata("empresa");
$sucursal = $ci->session->userdata("sucursal");


$foto = $ci->session->userdata("foto");

if($foto==null || empty($foto)){
    if($sexo=="F"){
        $foto = base_url() . "assets/images/mujer_sinFoto.jpeg";
    }else{
        $foto = base_url() . "assets/images/hombre_sinFoto.jpeg";
    }
}else{
     $foto = base_url() . "assets/foto/" . $foto;
}

//$rutaFoto = base_url() . "fotos/" . $foto;
$rutaFoto = $foto;

$email = trim($ci->session->userdata("email"));
$nombreRol = trim($ci->session->userdata("nombreRol"));



if($logoEmpresa==""){
    $logoEmpresa = base_url() . "assets/images/logo.jpeg"; 
}else{
    $logoEmpresa = base_url() . "assets/images/img_empresa/" . $logoEmpresa;
}

?>




    <div class="wrapper overlay-sidebar">
        <div class="main-header">
            <!-- Logo Header -->
            <div class="logo-header" data-background-color="blue2">
                
                <a href="<?=base_url('acceso')?>" class="logo">
                    <img src="<?php echo $logoEmpresa;?>" 
                         alt="navbar brand" class="navbar-brand" style="width:140px; height:60px">
                </a>
                <button class="navbar-toggler sidenav-toggler ml-auto" type="button" data-toggle="collapse" data-target="collapse" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon">
                        <i class="icon-menu"></i>
                    </span>
                </button>
                <button class="topbar-toggler more"><i class="icon-options-vertical"></i></button>
                <div class="nav-toggle">
                    <button class="btn btn-toggle sidenav-overlay-toggler">
                        <i class="icon-menu"></i>
                    </button>
                </div>





            </div>
            <!-- End Logo Header -->

            <!-- Navbar Header -->
            <nav class="navbar navbar-header navbar-expand-lg" data-background-color="blue2">
                

                <div class="container-fluid">

                <div class="nav-item dropdown hidden-caret">
                    <div style="color:white; font-size:26px">
                    <?=$empresa;?>&nbsp;&nbsp; / &nbsp; <?=$sucursal;?>
                    </div>          

                </div>  


                    <div class="collapse" id="search-nav"  >
                    </div>




                    <ul class="navbar-nav topbar-nav ml-md-auto align-items-center">
                        <li class="nav-item toggle-nav-search hidden-caret">
                            <a class="nav-link" data-toggle="collapse" href="#search-nav" role="button" aria-expanded="false" aria-controls="search-nav">
                                <i class="fa fa-search"></i>
                            </a>
                        </li>




                        <li class="nav-item dropdown hidden-caret">
                            <ul class="dropdown-menu notif-box animated fadeIn" aria-labelledby="notifDropdown">
                                <li>
                                    <div class="notif-scroll scrollbar-outer">
                                        <div class="notif-center">
                                            <a href="#">
                                                <div class="notif-icon notif-success"> <i class="fa fa-comment"></i> </div>
                                                <div class="notif-content">
                                                    <span class="block">
                                                        Rahmad commented on Admin
                                                    </span>
                                                    <span class="time">12 minutes ago</span> 
                                                </div>
                                            </a>
                                            <a href="#">
                                                <div class="notif-img"> 
                                                    <img src="<?php echo base_url()?>assets/img/profile2.jpg" alt="Img Profile">
                                                </div>
                                                <div class="notif-content">
                                                    <span class="block">
                                                        Reza send messages to you
                                                    </span>
                                                    <span class="time">12 minutes ago</span> 
                                                </div>
                                            </a>
                                            <a href="#">
                                                <div class="notif-icon notif-danger"> <i class="fa fa-heart"></i> </div>
                                                <div class="notif-content">
                                                    <span class="block">
                                                        Farrah liked Admin
                                                    </span>
                                                    <span class="time">17 minutes ago</span> 
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <a class="see-all" href="javascript:void(0);">See all notifications<i class="fa fa-angle-right"></i> </a>
                                </li>
                            </ul>
                        </li>
                          



                        <li class="nav-item dropdown hidden-caret">
                            <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#" aria-expanded="false">
                                <div class="avatar-sm">
                                    <img src="<?php echo $rutaFoto?>" alt="..." class="avatar-img rounded-circle">
                                </div>
                            </a>
                            <ul class="dropdown-menu dropdown-user animated fadeIn">
                                <div class="dropdown-user-scroll scrollbar-outer">
                                    <li>
                                        <div class="user-box">
                                            <div class="avatar-lg"><img src="<?php echo $rutaFoto?>" alt="image profile" class="avatar-img rounded"></div>
                                            <div class="u-text">
                                                <h4><?=$nombreUsuario;?></h4>
                                                <p class="text-muted"><?=$email;?></p>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="javascript:void(0)" onclick="miPerfil(<?=$idusuario;?>)">Mi Perfil</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="javascript:void(0)" onclick="cambiarClave()">Cambiar clave</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="<?=base_url().'Login';?>">Cerrar sesión</a>
                                    </li>
                                </div>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
            <!-- End Navbar -->

                
        </div>

        <!-- Sidebar -->
        <div class="sidebar sidebar-style-2">           
            <div class="sidebar-wrapper scrollbar scrollbar-inner" >
                <div class="sidebar-content">
                    <div class="user">
                        <div class="avatar-sm float-left mr-2">
                            <img src="<?php echo $rutaFoto?>" alt="..." class="avatar-img rounded-circle">
                        </div>
                        
                        <span style="color:black; font-size: 12px">
                            <div style="color:blue; font-weight: 700"><?php echo $nombreUsuario?></div>
                            <br>
                            <span class="user-level"><?php echo $nombreRol?></span>
                            <!--<span class="caret"></span>-->
                        </span>



                        <div>
                           <!-- <a data-toggle="collapse" href="#collapseExample" aria-expanded="true">-->
                            <!--</a>--> 
                        </div>
                    </div>
                    <ul class="nav nav-primary">
						<li class="nav-item active submenu">
                            <a href="<?php echo base_url('dashboard');?>">
                                <span class="sub-item">Dashboard</span>
                            </a>
                        </li>

                        <li class="nav-section">
                            <span class="sidebar-mini-icon">
                                <i class="fa fa-ellipsis-h"></i>
                            </span>
                            <h4 class="text-section">Modulos del sistema</h4>
                        </li>
                        
                        
                      
                      <!-- Generar menu dinamico -->
                      
                      <?php
                          $i = 0;
                          while($i <= $nroFilaModulo){
							  $menu = "menu" . $arregloPermisoModulo[$i][0];
							  $idModulo = $arregloPermisoModulo[$i][0];
							  $titulo = $arregloPermisoModulo[$i][1];
							  $icono = $arregloPermisoModulo[$i][2];
							  //$permiso = $arregloPermisoModulo[$i][3];
					  ?>		  
							  <li class="nav-item">
								  <a data-toggle="collapse" href="#<?=$menu;?>">
									  <i class="<?=$icono;?>"></i>
									  <p><?=$titulo;?></p>
									  <span class="caret"></span>
								  </a>						
								  <div class="collapse" id="<?=$menu;?>">		  
									  <ul class="nav nav-collapse">
									      <?php
										  $j=0;
										  while($j <= $nroFilaSM){										     
											  $idModuloSM = $arregloPermisoSM[$j][3];
											  if($idModulo==$idModuloSM){
												  $urlSM = $arregloPermisoSM[$j][2];
												  $nombreSM = strtolower($arregloPermisoSM[$j][1]);
												  $nombreSM = ucwords($nombreSM);
												  $href = base_url($urlSM);
										  ?>
       											  <li>
												  <a href="<?=$href;?>">
													  <span class="sub-item"><?=$nombreSM;?></span>
												  </a>
										   	      </li>										  
		                                  <?php 
											  }
											  $j+=1;
										  }	  
										  ?>
									  </ul>
								  </div>		
							  </li>	  
 					 <?php  
							  $i+=1;
						  }
                      ?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- End Sidebar -->


<script>

function miPerfil(id){
    $.ajax({
        url : "<?php echo site_url('Sg_usuario/ajax_ver')?>/"+id,
        type: "GET",                     
        dataType: "JSON",
        success: function(data){
             document.getElementById('div-imagen').innerHTML = data.imagen;  
             document.getElementById('c_login').value = data.login;
             document.getElementById('c_nombre').value = data.nombreApellido;
             document.getElementById('c_correo').value = data.email;
             sexo = "Femenino";
             if(data.sexo=='M'){
                 sexo = "Masculino";
             }
             document.getElementById('c_sexo').value = sexo;
             $('#ver_modal').modal('show');
        },
        error: function (jqXHR, textStatus, errorThrown){
            alert('Error consultando la Base de Datos');
        }
    });
    //$('#ver_modal').modal('show');
}

function cambiarClave(){
    document.getElementById('m_passwordAct').value="";  
    document.getElementById('m_passwordNew').value="";  
    document.getElementById('m_passwordRep').value="";  

    document.getElementById('lab_passwordAct').style.display="none";  
    document.getElementById('lab_passwordNew').style.display="none";    
    document.getElementById('lab_passwordRep').style.display="none";  


    document.getElementById('guardar10').disabled=false;
    document.getElementById('cerrar10').disabled=false;      



    $('#clave_modal').modal('show');
}


function actualizar_clave(){
    //validar campos formulario
    formulario = "formulario10";
    error=0;
    error = validar_clave(formulario);
    if(error==0){
        document.getElementById('guardar10').disabled=true;
        document.getElementById('cerrar10').disabled=true;      
    }

    if(error == 0){
        $.ajax({
            url : "<?=base_url();?>Sg_usuario/ajax_guardar_modClave",
            type: "POST",
            data: $('#formulario10').serialize(),
            dataType: "JSON",
            success: function(data)
            {
                if(data.status==0){ 
                    $('#clave_modal').modal('hide');
                    swal("Clave cambiada con exito", "", {
                        icon : "success",
                        buttons: {                  
                            confirm: {
                                className : 'btn btn-success'
                            }
                        },
                    });
                }else{
                    document.getElementById('lab_passwordAct').style.display = "inline";
                    document.getElementById('lab_passwordAct').innerHTML = "La clave actual es incorrecta.";
                    document.getElementById('guardar10').disabled=false;
                    document.getElementById('cerrar10').disabled=false;                     
                    swal("No se actualizo el registro", "", {
                        icon : "error",
                        buttons: {                  
                            confirm: {
                                className : 'btn btn-danger'
                            }
                        },
                    });

                }   
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error Insertando / modificanco registro, Comuniquese con el administrador');
            }
        });
    }else{
        swal("No se actualizo el registro", "Verifique", {
            icon : "error",
            buttons: {                  
                confirm: {
                    className : 'btn btn-danger'
                }
            },
        });
    }    
}


function validar_clave(formulario){
    error = 0;


    swClave=0;
    objeto = "m_passwordAct";
    campo = document.forms[formulario].elements[objeto].value;
    campo = campo.replace(/(^\s*)|(\s*$)/g,"");
    clave=campo.trim();
    document.getElementById('lab_passwordAct').style.display = "none";
    if (campo.length==0){
        document.getElementById('lab_passwordAct').style.display = "inline";
        document.getElementById('lab_passwordAct').innerHTML = "Edite la clave actual";
        error = 1;
    }else{
        if(campo.length < 6){
            document.getElementById('lab_passwordAct').style.display = "inline";
            document.getElementById('lab_passwordAct').innerHTML = "La clave Actual debe tener una longuitud de seis(6) caracteres";
            error = 1;
        }    
    }

    objeto = "m_passwordNew";
    campo = document.forms[formulario].elements[objeto].value;
    campo = campo.replace(/(^\s*)|(\s*$)/g,"");
    clave=campo.trim();
    document.getElementById('lab_passwordNew').style.display = "none";
    document.getElementById('lab_passwordRep').style.display = "none";
    if (campo.length==0){
        document.getElementById('lab_passwordNew').style.display = "inline";
        document.getElementById('lab_passwordNew').innerHTML = "Edite la clave nueva";
        error = 1;
    }else{
        if(campo.length < 6){
            document.getElementById('lab_passwordNew').style.display = "inline";
            document.getElementById('lab_passwordNew').innerHTML = "La lave debe tener una longuitud de seis(6) caracteres";
            error = 1;
        }else{
            swClave = 0;
            objeto = "m_passwordRep";
            campo = document.forms[formulario].elements[objeto].value;
            campo = campo.replace(/(^\s*)|(\s*$)/g,"");
            claveRep = campo.trim();
            if (campo.length==0){
                document.getElementById('lab_passwordRep').style.display = "inline";
                document.getElementById('lab_passwordRep').innerHTML = "Repeta la clave";
                error = 1;
                swClave = 1;
            }else{
                if(campo.length < 6){
                    document.getElementById('lab_passwordRep').style.display = "inline";
                    document.getElementById('lab_passwordRep').innerHTML = "La clave debe tener una longuitud de seis(6) caracteres";
                    error = 1;
                }else{
                    if(clave != claveRep){
                        document.getElementById('lab_passwordNew').style.display = "inline";
                        document.getElementById('lab_passwordNew').innerHTML = "La clave ingresadas son diferentes";
                        error = 1;
                        swClave = 1;
                    }
                }    
            }
        }  
    }

return error;
}






</script>    

<!-- Ver usuarios  -->
<!-- Modal -->
<div class="modal fade" id="ver_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width:900px; margin-left:-200px">
            <div class="modal-header bg-primary text-white">
                <h4 class="modal-title" id="exampleModalLabel" style="color:white">
                    <div style="font-weight: 700">Información del usuario</div>
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color:white; font-size:30px">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                

              <div class="container">
                <div class="row">
                    <div class="col-md-9">
                        <label ><span style="color:#0000ff; font-size:16px; font-weight:600">
                                 Login <span style="color:#ff0000;">*</span></span>
                        </label>
                        <input type="text" name="c_login" id="c_login" class="form-control"
                               readonly="true">

                         <label><span style="color:#0000ff; font-size:16px; font-weight:600">
                             Nombre, apellido <span style="color:#ff0000;">*</span></span>
                         </label>
                        <input type="text" name="c_nombre" id="c_nombre" class="form-control" 
                               readonly="true">
                        <br>
                         <label>
                             <span style="color:#0000ff; font-size:16px; font-weight:600">
                             Correo electrónico</span>
                         </label>

                        <input type="text" name="c_correo" id="c_correo" class="form-control" 
                               readonly="true">
                        <br>       
                         <label>
                             <span style="color:#0000ff; font-size:16px; font-weight:600">
                             Sexo <span style="color:#ff0000;">*</span></span>
                         </label>
                        <input type="text" name="c_sexo" id="c_sexo" class="form-control" 
                               readonly="true">
                    </div>

                    <div class="col-md-3">
                        <div id="div-imagen"></div> 
                    </div>

                </div>  
                <br>  
              </div>                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><span style="font-weight: 700">Cerrar</span></button>
            </div>

        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="clave_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width:600px; margin-left:-50px">
            <div class="modal-header bg-primary text-white">
                <h4 class="modal-title" id="exampleModalLabel" style="color:white">
                    <div style="font-weight: 700">Cambiar clave</div>
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color:white; font-size:30px">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                
            <form name="formulario10" method="post" id="formulario10" 
                  enctype="multipart/form-data">    


                <div class="row">
                    <div class="col-md-3" style="padding-top: 10px">
                         <label>
                             <span style="color:#0000ff; font-size:16px; font-weight:600">
                             Clave actual <span style="color:#ff0000;">*</span></span>
                         </label>
                    </div>     

                    <div class="col-md-9" style="padding-top: 10px">
                        <input type="password" name="m_passwordAct" id="m_passwordAct" class="form-control" 
                               placeholder="Edite clave actual" maxlength="100" minlength="0">
                        <div id="lab_passwordAct" style="color:red; margin-left:9px; display:none"></div>
                    </div>
                </div>
                
                <hr>
                <div class="row">
                    <div class="col-md-3" style="padding-top: 10px">
                         <label>
                             <span style="color:#0000ff; font-size:16px; font-weight:600">
                             Nueva clave<span style="color:#ff0000;">*</span></span>
                         </label>
                    </div>     

                    <div class="col-md-9" style="padding-top: 10px">
                        <input type="password" name="m_passwordNew" id="m_passwordNew" class="form-control" 
                               placeholder="Edite clave nueva" maxlength="100" minlength="0">
                        <div id="lab_passwordNew" style="color:red; margin-left:9px; display:none"></div>
                    </div>
                </div>


                <div class="row">    
                    <div class="col-md-3" style="padding-top: 10px">
                         <label>
                             <span style="color:#0000ff; font-size:16px; font-weight:600">
                             Repetir clave <span style="color:#ff0000;">*</span></span>
                         </label>
                    </div>
                    <div class="col-md-9" style="padding-top: 10px">
                        <input type="password" name="m_passwordRep" id="m_passwordRep" 
                               class="form-control"
                               placeholder="Repita la clave" maxlength="100" minlength="0">
                        <div id="lab_passwordRep" style="color:red; margin-left:9px; display:none"></div>
                    </div>    
                </div>  
                <br><br>
            
                <span style="color:#ff0000; font-size:16px; text-align:left">Campo obligatorio (*)</span>
            </form>  
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrar10"><span style="font-weight: 700">Cerrar</span></button>
                <button type="button" class="btn btn-primary" onclick="actualizar_clave()" id="guardar10">
                      <span style="font-weight: 700">Cambiar</span>

            </div>

        </div>
    </div>
</div>

