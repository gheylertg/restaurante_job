<?php
$incluir = base_url() . "assets/images/nuevo.png";
$asterisco = ' style="color=red; font-size:16px"';
$ci = &get_instance();
$ci->load->library('Clssession');
$accion= $ci->clssession->accion(11,2);

$rutaImagen = base_url() . "Mn_alimento/ajax_guardar_modImagen";



?>

        <br>            
        <div class="main-panel">
            <div class="content">
                <div class="page-inner mt--5">
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <div class="card card-stats card-round" style="padding-top: 5px">
                                <div class="card-body">
									 <h1><span style="color:blue">Producto</span></h1>
                                </div>
                            </div>
                        </div>
                    </div>


                <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <?php
                                        if($accion==1){
                                    ?>
                                    <div  style="text-align:right">

                                    <a href="javascript:void(0)" onclick="javascript:incAlimento()">
                                       <img src="<?=$incluir;?>" style="width:35px; height:35px" alt="Incluir" title="Incluir producto del Menú">
                                    </a>
                                    </div>
                                    <?php
                                        }
                                    ?>

                                    <div class="table-responsive" style="padding-top: 10px">
										<div id="bodyTabla">
										</div>
                                    </div>
                                </div>
                            </div>
                        </div>    
                </div>
            </div>
<?php
$fechaMaxima = date("Y-m-d");


?>


<!-- Modificar Alimento ----->


<div class="modal fade" id="modificar_alimento" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width:800px; margin-left:-100px">
            <div class="modal-header bg-primary text-white">
                <h4 class="modal-title" id="exampleModalLabel">
                    <div style="font-weight:700">Modificar Producto</div>
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color:white; font-size:30px">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                
                <form name="formularioModAlimento" method="post" id="formularioModAlimento" autocomplete="off">   

                <div class="row">
                    <div class="col-md-3">
                        <label>
                        <span style="color:#0000ff; font-size:16px; font-weight:600">
                            Nombre <span style="color:#ff0000;">*</span>
                        </span>
                        </label>
                    </div>
                    <div class="col-md-9">
                        <input type="text" id="mo_nombre" name="mo_nombre"
                               class="form-control"/>
                        <div id="lab_nombre_mo" style="color:red; margin-left:9px; display:none">
                        </div>
                    </div>
                </div> 

                <div class="row" style="padding-top: 5px">
                    <div class="col-md-3">
                        <label>
                        <span style="color:#0000ff; font-size:16px; font-weight:600">
                            Etiqueta menú (Max. 20 Carácteres)<span style="color:#ff0000;">*</span>
                        </span>
                        </label>
                    </div>
                    <div class="col-md-9">
                        <input type="text" id="mo_etiqueta" name="mo_etiqueta"
                               class="form-control" maxlength="20"/>
                        <div id="lab_etiqueta_mo" style="color:red; margin-left:9px; display:none">
                        </div>
                    </div>
                </div> 



                <div class="row" style="padding-top: 5px">
                    <div class="col-md-3">
                         <label style="padding-top: 0px; margin-top: 5px;margin-left:0px;">
                             <span style="color:#0000ff; font-size:16px; font-weight:600">Categoría Menú <span style="color:#ff0000;">*</span></span>
                         </label>
                    </div>
                    <div class="col-md-9">
                        <div id="cbo_categoriaMenu"></div>
                        <div id="lab_categoriaMenu" style="color:red; margin-left:9px; display:none"></div>
                    </div>
                </div>     
                
				<div class="row">
					<div class="col-md-3">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">Orden de Visualización <span style="color:#ff0000;">*</span></span> </label>

					</div>
					
					<div class="col-md-9">
                        <input style="padding-top: 5px;margin-right:-20px;margin-top: 12px;" 
                               type="text" id="mo_orden" name="mo_orden" class="form-control" 
                               placeholder="Ingrese el orden" maxlength="10" 
                               onkeyup="nro_entero('mo_orden');">
                        <div id="lab_orden_mo" style="color:red; margin-left:9px; display:none"></div>       
					</div>
				</div>	
                
                                


                <div class="row" style="padding-top:5px">
                    <div class="col-md-3">
                        <label>
                        <span style="color:#0000ff; font-size:16px; font-weight:600">
                            Descripción <span style="color:#ff0000;">*</span>
                        </span>
                        </label>
                    </div>

                    <div class="col-md-9">
                        <textarea  id="mo_descripcion" name="mo_descripcion"
                               class="form-control" rows="3"></textarea>
                        <div id="lab_descripcion_mo" style="color:red; margin-left:9px; display:none">
                        </div>
                    </div>
                </div>    


                <br>  
                <span style="color:#ff0000; font-size:16px; text-align:left">Campo obligatorio (*)</span>

                <input type="hidden" id="id_mod_mo" name="id_mod_mo">
                <input type="hidden" id="nombreOrig" name="nombreOrig">

                </form>  
              
            </div>
            <div class="modal-footer">

                   
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrar05">
					<span style="font-weight:700">Cerrar</span></button> 


                <button type="button" class="btn btn-primary" id="guardar05"
                        onclick="guardar_mod_alimento();">
                      <span style="font-weight:700">Guardar</span>
                </button>
            </div>
        </div>
    </div>
</div>


<!-- ----------------- Modificar Foto ------------------------- -->

<!-- Modal -->
<div class="modal fade" id="modImagen_modal" tabindex="-1" role="dialog" 
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width:900px; margin-left:-180px">
            <div class="modal-header bg-primary text-white">
                <h4 class="modal-title" id="exampleModalLabel" style="color:white">
                    <div style="font-weight: 700" id="titulo_mostrarImagen"></div>
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color:white; font-size:30px">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                
            <form name="formularioImagen" method="post" id="formularioImagen" 
                  enctype="multipart/form-data">    
                
                
              <div class="container">
                
                <div class="row">


                    <div class="col-md-7" style="padding-top: 10px">
                        <label>
                             <span style="color:#0000ff; font-size:16px; font-weight:600">
                             Imagen del Producto</span>
                        </label>

                        <input type="file" id="mod_imagen" name="mod_imagen"
                               class="form-control" />
                        <div style="color:blue">Archivo con extensión JPEG, JPG, PNG</div>
                        <div id="lab_imagen" style="color:red; margin-left:9px; display:none"></div>     
                    </div>

                    <div class="col-md-5" style="padding-top: 10px">
                        <span style="font-size:18px; color:#00f; font-weight: 700">Imagen Actual</span><br> 

                        <div id="mostrarImagen"></div>
                    </div>




                </div>  



                <br>  
                <span style="color:#ff0000; font-size:16px; text-align:left">Campo obligatorio (*)</span>
              </div>                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrar06">Cerrar</button>
                <button type="button" class="btn btn-primary" onclick="guardar_modImagen_form()" 
                        id="guardar06">
                      Guardar
                </button>

            </div>

            <input type="hidden" id="id_mod_imagen" name="id_mod_imagen">
            <input type="hidden" id="ruta_imagen" name="ruta_imagen" value="<?=$rutaImagen;?>">

            </form>  

        </div>
    </div>
</div>