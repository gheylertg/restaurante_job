<!--cargar datatable-->
<script>

    $(document).ready(function(){
         <?php
         if($modulo==1){  
         ?>	
			 cargarDataTable(0);
		 <?php
		 }
		 ?>

		 <?php
         if($modulo==2){  
         ?>	
			 addAlimento();
		 <?php
		 }
		 ?>

		 <?php
         if($modulo==3){  
         ?>	
			 cargar_dataTableIngrediente(<?=$idAlimento;?>);
		 <?php
		 }
		 ?>

		 <?php
         if($modulo==4){  
         ?>	
			 verAlimento(<?=$idAlimento;?>);
		 <?php
		 }
		 ?>

		 <?php
         if($modulo==5){  
         ?>	
			 cargar_dataTablePresentacion(<?=$idAlimento;?>);
		 <?php
		 }
		 ?>

		 <?php
         if($modulo==6){  
         ?>	
			 addPresentacion();
		 <?php
		 }
		 ?>		 


		 <?php
         if($modulo==9){  
         ?>	
             cargar_dataTablePresentacionIngrediente(<?=$idPresentacionIngrediente;?>);

		 <?php
		 }
		 ?>	

		 <?php
         if($modulo==10){  
         ?>	
             llamarProducto();
		 <?php
		 }
		 ?>			 


    });

function nro_entero(objeto){
//valida si el nro es entero sin caracteres diferentes a numero
//formulario = nombre del formulario origen
//Objeto = nombre del campo u objeto

var valor = document.getElementById(objeto).value;
var tamano=valor.length;
i=0;
cad_numero="";
while(i<tamano){
    var numero = valor.charAt(i);
    if (isNaN(numero)==false)
        cad_numero=cad_numero+numero
    i=i+1;    
}
document.getElementById(objeto).value = cad_numero;
}



function llamarProducto(){
    document.location.href = "<?php echo base_url('alimento')?>";
}


    
// construye el datatable principal para actualizar la ordenes
function cargarDataTable(id){
    $.ajax({
		url : "<?php echo site_url('Mn_alimento/ajax_datatable')?>/"+id,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			 document.getElementById('bodyTabla').innerHTML = data.registro;
			 $(document).ready(function() {
				$('#basic-datatables').DataTable({
                    "order": [[ 0, "desc" ]]
			    });	
			 });

		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}


// llama el controllador que ingresa la información de la orden
function incAlimento(){
	document.location.href = "<?php echo base_url('incAlimento')?>";	
}





function addAlimento(){
   	document.getElementById('guardar01').disabled=false;
	document.getElementById('cerrar01').disabled=false; 
    $.ajax({
		url : "<?php echo site_url('Mn_alimento/ajax_datatable_alimento')?>/",
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			 document.getElementById('tablaIngrediente').innerHTML = data.registro;
			 $(document).ready(function() {
				ruta = "<?php echo site_url('Combo_ctrl/ajax_combo_categoriaMenu')?>";
				cbo_categoriaMenu(ruta,0);                  
			 });


		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}


function add_ingrediente(){
	ruta = "<?php echo site_url('Combo_ctrl/ajax_combo_ingrediente')?>";
	cbo_ingrediente(ruta,0); 
    //document.getElementById('cantidad').value = "0,00";

    document.getElementById('lab_ingrediente').style.display = "none";
    //document.getElementById('lab_cantidad').style.display = "none";
    $('#mod_ingrediente').modal('show');
}


function add_ingredienteAlimento(){
	ruta = "<?php echo site_url('Combo_ctrl/ajax_combo_ingredienteAlim')?>";
	cbo_ingredienteAlim(ruta,0);
    document.getElementById('lab_ingrediente').style.display = "none";
    $('#mod_ingredienteAlimento').modal('show');
}

function modificar_alimento(id){
    document.getElementById('cerrar05').disabled=false;  
	document.getElementById('guardar05').disabled=false;  
    $.ajax({
		url : "<?php echo site_url('Mn_alimento/ajax_act_modificar_alimento')?>/"+id,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
             document.getElementById('mo_nombre').value = data.nombre;	
             document.getElementById('mo_etiqueta').value = data.etiqueta;	
			 document.getElementById('mo_orden').value = data.orden;		
             document.getElementById('mo_descripcion').value = data.descripcion;	

			 ruta = "<?php echo site_url('Combo_ctrl/ajax_combo_categoriaMenu')?>";
			 cbo_categoriaMenu(ruta,data.id_categoriaMenu); 


			 document.getElementById('id_mod_mo').value = data.id;
			 document.getElementById('lab_nombre_mo').style.display = "none";
			 document.getElementById('lab_etiqueta_mo').style.display = "none";
			 document.getElementById('lab_orden_mo').style.display = "none";
			 

			 document.getElementById('lab_descripcion_mo').style.display = "none";	

			 document.getElementById('nombreOrig').value = data.nombre;
             $('#modificar_alimento').modal('show');  
            

		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});

}



function guardar_add_ingrediente(){

$validar = valIngrediente(1);
if($validar==0){
	$.ajax({
		url : "<?=base_url();?>Mn_alimento/ajax_guardar_ingredienteSB",
		type: "POST",
		data: $('#formularioIngrediente').serialize(),
		dataType: "JSON",
		success: function(data)
		{
			if(data.status==0){ 
				document.getElementById('tablaIngrediente').innerHTML = data.registro;
				//document.getElementById('cantidad').value = "0,00";
				document.getElementById('nroIngrediente').value = data.nroIngrediente;

				$('#mod_ingrediente').modal('hide');
			}else{
				swal("No se actualizo el registro", "Ya el Ingrediente esta agregado", {
					icon : "error",
					buttons: {        			
						confirm: {
							className : 'btn btn-danger'
						}
					},
				});

			}	 
		},
		error: function (jqXHR, textStatus, errorThrown)
		{
			alert('Error Insertando / modificanco registro, Comuniquese con el administrador');
		}
	});
}else{
	//document.getElementById('cantidad').value = "0,00";
	swal("No se actualizo el registro", "Falta Información", {
		icon : "error",
		buttons: {        			
			confirm: {
				className : 'btn btn-danger'
			}
		},
	});
}    

}



function guardar_mod_ingrediente(){

	//validar campos formulario
	formulario = "formularioModificar";
	error=0;
	error = valIngredienteMod(2);
	if(error==0){
    	document.getElementById('guardar03').disabled=true;
		document.getElementById('cerrar03').disabled=true; 		
	}
	
	
	alert("aqui");
	return false;
	

	if(error == 0){
        $.ajax({
            url : "<?=base_url();?>Mn_alimento/ajax_guardar_ingrediente_mod",
            type: "POST",
            data: $('#formularioModificar').serialize(),
            dataType: "JSON",
            success: function(data)
            {
				document.getElementById('tablaIngrediente').innerHTML = data.registro;
				document.getElementById('x_cantidad').value = "0,00";
				document.getElementById('nroIngrediente').value = data.nroIngrediente;
				$('#modificar_ingrediente').modal('hide');


	        },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error Insertando / modificanco registro, Comuniquese con el administrador');
            }
        });
    }else{
		swal("No se actualizo el registro", "", {
			icon : "error",
			buttons: {        			
				confirm: {
					className : 'btn btn-danger'
				}
			},
		});
    }    
}



/*function eliminar_ingrediente(id){
		    $.ajax({
				url : "<?php //echo site_url('Ad_sabor/ajax_eliminar_ingrediente')?>/"+id,
				type: "GET",                     
				dataType: "JSON",
				success: function(data){
                    document.getElementById('tablaIngrediente').innerHTML = data.registro;
                    document.getElementById('cantidad').value = "0,00";
                    document.getElementById('nroIngrediente').value = data.nroIngrediente;

				},
				error: function (jqXHR, textStatus, errorThrown){
					alert('Error consultando la Base de Datos');
				}
			});

}*/


function eliminar_ingrediente(id){
	swal({
		title: 'Esta seguro de eliminar el Ingrediente?',
			type: 'warning',
			buttons:{
				cancel: {
					visible: true,
						text : 'No, Cancelar!',
								className: 'btn btn-danger'
				},        			
				confirm: {
					text : 'Si, Eliminar!',
					className : 'btn btn-success'
				}
			}
	}).then((willDelete) => {
		if (willDelete) {

		    $.ajax({
				url : "<?php echo site_url('Mn_alimento/ajax_eliminar_ingrediente')?>/"+id,
				type: "GET",                     
				dataType: "JSON",
				success: function(data){
                    document.getElementById('tablaIngrediente').innerHTML = data.registro;
                    document.getElementById('nroIngrediente').value = data.nroIngrediente;

				},
				error: function (jqXHR, textStatus, errorThrown){
					alert('Error consultando la Base de Datos');
				}
			});
			swal("El Ingrediente fué eliminado!", {
				icon: "success",
				buttons : {
					confirm : {
						className: 'btn btn-success'
					}
				}
			});
		}
	});
}




function guardar_form(){
    formulario = "formulario";
    $validar = validar_form(formulario,0);
    document.getElementById('guardar01').disabled=true;
	document.getElementById('cerrar01').disabled=true;
    nombre = document.getElementById('m_nombre').value;
    ruta = document.getElementById('rutaProducto').value;

    if($validar==0){
	    $.ajax({
			url : "<?php echo site_url('Mn_alimento/ajax_validar_duplicidad')?>",
			type: "GET",                     
			dataType: "JSON",
			data: {nombre:nombre},
			success: function(data){
				if(data.status==0){ 
					var Form = $('#formulario');
					Form.attr("action", ruta);
					Form.submit();
			    }else{
					document.getElementById('lab_nombre').style.display = "inline";
					document.getElementById('lab_nombre').innerHTML = "El nombre del Producto, ya se encuentra registrado.";
			    	document.getElementById('guardar01').disabled=false;
					document.getElementById('cerrar01').disabled=false; 					
					swal("No se actualizo el registro", "", {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});
				}	
			},
			error: function (jqXHR, textStatus, errorThrown){
				alert('Error consultando la Base de Datos');
			}
		});
	}else{
	    document.getElementById('guardar01').disabled=false;
		document.getElementById('cerrar01').disabled=false;



		swal("No se actualizo el registro", "Falta Información", {
				icon : "error",
				buttons: {        			
					confirm: {
						className : 'btn btn-danger'
					}
				},
		});
	}    

}



function desactivarAlimento_form(id){
    swal({
        title: 'Esta seguro de Desactivar el Producto?',
            type: 'warning',
            buttons:{
                cancel: {
                    visible: true,
                        text : 'No, Desactivar!',
                                className: 'btn btn-danger'
                },                  
                confirm: {
                    text : 'Si, Desactivar!',
                    className : 'btn btn-success'
                }
            }
    }).then((willDelete) => {
        if (willDelete) {
            $.ajax({
                url : "<?php echo site_url('Mn_alimento/ajax_desactivarAlimento')?>/"+id,
                type: "GET",                     
                dataType: "JSON",
                success: function(data){
                     document.getElementById('bodyTabla').innerHTML = data.registro;
                     $(document).ready(function() {
                        $('#basic-datatables').DataTable({
                        }); 
                     }); 
                },
                error: function (jqXHR, textStatus, errorThrown){
                    alert('Error consultando la Base de Datos');
                }
            });
            swal("El registro fué desactivado!", {
                icon: "success",
                buttons : {
                    confirm : {
                        className: 'btn btn-success'
                    }
                }
            });
        }
    });

}



function reactivarAlimento_form(id){
    swal({
        title: 'Esta seguro de Reactivar el Producto?',
            type: 'warning',
            buttons:{
                cancel: {
                    visible: true,
                        text : 'No, Reactivar!',
                                className: 'btn btn-danger'
                },                  
                confirm: {
                    text : 'Si, Reactivar!',
                    className : 'btn btn-success'
                }
            }
    }).then((willDelete) => {
        if (willDelete) {
            $.ajax({
                url : "<?php echo site_url('Mn_alimento/ajax_reactivarAlimento')?>/"+id,
                type: "GET",                     
                dataType: "JSON",
                success: function(data){
                     document.getElementById('bodyTabla').innerHTML = data.registro;
                     $(document).ready(function() {
                        $('#basic-datatables').DataTable({
                        }); 
                     }); 
                },
                error: function (jqXHR, textStatus, errorThrown){
                    alert('Error consultando la Base de Datos');
                }
            });
            swal("El registro fué Reactivado!", {
                icon: "success",
                buttons : {
                    confirm : {
                        className: 'btn btn-success'
                    }
                }
            });
        }
    });

}






function valIngrediente(accion){

formulario = "formularioIngrediente";
error=0;

if(accion != 3){
	objeto = "id_cbo_ingrediente";
	campo = document.forms[formulario].elements[objeto].value;
	document.getElementById('lab_ingrediente').style.display = "none";
	if (campo==0){
		document.getElementById('lab_ingrediente').style.display = "inline";
	   	document.getElementById('lab_ingrediente').innerHTML = "Seleccione el Ingrediente"; 	
	   	error = 1;
	}
}

/*objeto = "cantidad";
campo = document.forms[formulario].elements[objeto].value;
campo = campo.replace(/(^\s*)|(\s*$)/g,"");
document.getElementById('lab_cantidad').style.display = "none";

if (campo=="0,00" || campo.length==0){	
	document.getElementById('lab_cantidad').style.display = "inline";
    document.getElementById('lab_cantidad').innerHTML = "Ingrese la cantidad del Ingrediente";
    document.getElementById('cantidad').value = "0,00";
    error = 1;
}*/


return error;
}



/*
function valIngredienteMod(accion){

formulario = "formularioModificar";
error=0;

objeto = "x_cantidad";
campo = document.forms[formulario].elements[objeto].value;
campo = campo.replace(/(^\s*)|(\s*$)/g,"");
document.getElementById('lab_cantidadx').style.display = "none";

if (campo=="0,00" || campo.length==0){	
	document.getElementById('lab_cantidadx').style.display = "inline";
    document.getElementById('lab_cantidadx').innerHTML = "Ingrese la cantidad del Ingrediente"; 	
    document.getElementById('x_cantidad').value = "0,00";
    error = 1;
}


return error;
}
*/


function cerrar_form(){
    document.location.href = "<?php echo base_url('alimento')?>"; 

	/*ruta = 	document.getElementById('ruta_guardar_producto').value;
	var Form = $('#formulario');
	Form.attr("action", ruta);
	Form.submit();  
	*/
	
}



function guardar_mod_alimento(){
    formulario = "formularioModAlimento";
    $validar = validar_form_alimento(formulario,1);
    document.getElementById('guardar05').disabled=true;
	document.getElementById('cerrar05').disabled=true;

	if($validar == 0){
        $.ajax({
            url : "<?=base_url();?>Mn_alimento/ajax_guardar_mod_alimento",
            type: "POST",
            data: $('#formularioModAlimento').serialize(),
            dataType: "JSON",
            success: function(data)
            {
            	if(data.status==0){ 

					 document.getElementById('bodyTabla').innerHTML = data.registro;
					 $('#modificar_alimento').modal('hide');

					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
				 	});            	


					swal("Registro almacenado Correctamente!", "", {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					});
				}else{
					document.getElementById('lab_nombre_mo').style.display = "inline";
					document.getElementById('lab_nombre_mo').innerHTML = "El Nombre ingresado, ya se encuentra registrado.";
			    	document.getElementById('guardar05').disabled=false;
					document.getElementById('cerrar05').disabled=false; 		
					swal("No se actualizo el registro", "", {

						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});





				}





	        },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error Insertando / modificanco registro, Comuniquese con el administrador');
            }
        });
    }else{
		swal("No se actualizo el registro", "", {
			icon : "error",
			buttons: {        			
				confirm: {
					className : 'btn btn-danger'
				}
			},
		});
		document.getElementById('guardar05').disabled=false;
		document.getElementById('cerrar05').disabled=false  ;		
    }    
}


function modificarImagen_form(id){
    $.ajax({
        url : "<?php echo site_url('Mn_alimento/ajax_modificarImagen')?>/"+id,
        type: "GET",                     
        dataType: "JSON",
        success: function(data){
             document.getElementById('mostrarImagen').innerHTML = data.imagen;
			$('#formularioImagen')[0].reset(); // reset form on modals
		   	document.getElementById('guardar06').disabled=false;
			document.getElementById('cerrar06').disabled=false; 		
		    document.getElementById('titulo_mostrarImagen').innerHTML = "Cambiar Imagen del Producto";
		    document.getElementById('id_mod_imagen').value = id;
		    document.getElementById('lab_imagen').style.display = "none"; 
		    $('#modImagen_modal').modal('show');
        },
        error: function (jqXHR, textStatus, errorThrown){
            alert('Error consultando la Base de Datos');
        }
    });
}


function guardar_modImagen_form(){
	//validar campos formulario
	formulario = "formularioImagen";
	error=0;
	error = validarImagen_form(formulario,0);
	if(error==0){
    	document.getElementById('guardar06').disabled=true;
		document.getElementById('cerrar06').disabled=true; 		
        rutaImagen = document.getElementById('ruta_imagen').value;
		var Form = $('#formularioImagen');
		Form.attr("action", rutaImagen);
		Form.submit();
	}else{
		swal("No se actualizo el registro", "", {
			icon : "error",
			buttons: {        			
				confirm: {
					className : 'btn btn-danger'
				}
			},
		});
	}	

}

function validarImagen_form(formulario, accion){
campo = "mod_imagen";
campo = document.forms[formulario].elements[campo].value;

if (campo.length==0){
	document.getElementById('lab_imagen').style.display = "none";
	document.getElementById('lab_imagen').style.display = "inline";
    document.getElementById('lab_imagen').innerHTML = "Ingrese la imagen del Producto";  
    error=1;
}else{   
    nombreArchivo = campo; 
    tam = campo.length;
    ext = tam - 3;
    cad = campo.substring(ext,tam);
    cad = cad.toUpperCase();
    if(cad == 'JPG' || cad == 'PNG'){
    	error = 0;
    }else{
	    nombreArchivo = campo; 
	    tam = campo.length;
	    ext = tam - 4;
	    cad = campo.substring(ext,tam);
	    cad = cad.toUpperCase();
	    if(cad == 'JPEG'){
	    	error = 0;
	    }else{
	    	error = 1;
	    }	
    }	

    if(error == 1){
		document.getElementById('lab_imagen').style.display = "none";
		document.getElementById('lab_imagen').style.display = "inline";
	    document.getElementById('lab_imagen').innerHTML = "La extensión del archivo no corresponde a la solicitadas";    	
    }
}

return error;
}


function validar_form_alimento(formulario,accion){
//accion 1= incluir
//accion 2= modificar
error=0;

objeto = "mo_nombre";
campo = document.forms[formulario].elements[objeto].value;
campo = campo.replace(/(^\s*)|(\s*$)/g,"");
document.getElementById('lab_nombre_mo').style.display = "none";

if (campo.length==0){	
	document.getElementById('lab_nombre_mo').style.display = "inline";
    document.getElementById('lab_nombre_mo').innerHTML = "Ingrese Nombre del Producto"; 	
    error = 1;
}

objeto = "mo_etiqueta";
campo = document.forms[formulario].elements[objeto].value;
campo = campo.replace(/(^\s*)|(\s*$)/g,"");
document.getElementById('lab_etiqueta_mo').style.display = "none";

if (campo.length==0){	
	document.getElementById('lab_etiqueta_mo').style.display = "inline";
    document.getElementById('lab_etiqueta_mo').innerHTML = "Ingrese etiqueta a mostrar en el menú"; 	
    error = 1;
}



objeto = "id_cbo_categoriaMenu";
campo = document.forms[formulario].elements[objeto].value;
document.getElementById('lab_categoriaMenu').style.display = "none";


if (campo==0){	
	document.getElementById('lab_categoriaMenu').style.display = "inline";
    document.getElementById('lab_categoriaMenu').innerHTML = "Ingrese Categoria del Menu"; 	
    error = 1;
}


objeto = "mo_orden";
campo = document.forms[formulario].elements[objeto].value;
campo = campo.replace(/(^\s*)|(\s*$)/g,"");

document.getElementById('lab_orden_mo').style.display = "none";
if (campo.length==0){
	document.getElementById('lab_orden_mo').style.display = "inline";
	document.getElementById('lab_orden_mo').innerHTML = "Edite el orden de visualización"; 	
    error = 1;
}




objeto = "mo_descripcion";
campo = document.forms[formulario].elements[objeto].value;
campo = campo.replace(/(^\s*)|(\s*$)/g,"");
document.getElementById('lab_descripcion_mo').style.display = "none";

if (campo.length==0){	
	document.getElementById('lab_descripcion_mo').style.display = "inline";
    document.getElementById('lab_descripcion_mo').innerHTML = "Ingrese Descripción del Producto"; 	
    error = 1;
}

return error;
}



function validar_form(formulario,accion){
//accion 1= incluir
//accion 2= modificar
error=0;

objeto = "m_nombre";
campo = document.forms[formulario].elements[objeto].value;
campo = campo.replace(/(^\s*)|(\s*$)/g,"");
document.getElementById('lab_nombre').style.display = "none";

if (campo.length==0){	
	document.getElementById('lab_nombre').style.display = "inline";
    document.getElementById('lab_nombre').innerHTML = "Ingrese Nombre del Producto"; 	
    error = 1;
}

objeto = "m_etiqueta";
campo = document.forms[formulario].elements[objeto].value;
campo = campo.replace(/(^\s*)|(\s*$)/g,"");
document.getElementById('lab_etiqueta').style.display = "none";

if (campo.length==0){	
	document.getElementById('lab_etiqueta').style.display = "inline";
    document.getElementById('lab_etiqueta').innerHTML = "Ingrese etiqueta a mostrar en el menú de producto"; 	
    error = 1;
}

objeto = "m_orden";
campo = document.forms[formulario].elements[objeto].value;
campo = campo.replace(/(^\s*)|(\s*$)/g,"");

document.getElementById('lab_orden').style.display = "none";
if (campo.length==0){
	document.getElementById('lab_orden').style.display = "inline";
	document.getElementById('lab_orden').innerHTML = "Edite el orden de visualización"; 	
    error = 1;
}

objeto = "m_descripcion";
campo = document.forms[formulario].elements[objeto].value;
campo = campo.replace(/(^\s*)|(\s*$)/g,"");
document.getElementById('lab_descripcion').style.display = "none";

if (campo.length==0){	
	document.getElementById('lab_descripcion').style.display = "inline";
    document.getElementById('lab_descripcion').innerHTML = "Ingrese Descripción del Producto"; 	
    error = 1;
}



objeto = "id_cbo_categoriaMenu";
campo = document.forms[formulario].elements[objeto].value;
document.getElementById('lab_categoriaMenu').style.display = "none";


if (campo==0){	
	document.getElementById('lab_categoriaMenu').style.display = "inline";
    document.getElementById('lab_categoriaMenu').innerHTML = "Ingrese Categoria del Menu"; 	
    error = 1;
}


campo = "m_imagen";
campo = document.forms[formulario].elements[campo].value;
if (campo.length>0){
    nombreArchivo = campo; 
    tam = campo.length;
    ext = tam - 3;
    cad = campo.substring(ext,tam);
    cad = cad.toUpperCase();
    if(cad == 'JPG' || cad == 'PNG'){
    	errorImagen = 0;
    }else{
	    nombreArchivo = campo; 
	    tam = campo.length;
	    ext = tam - 4;
	    cad = campo.substring(ext,tam);
	    cad = cad.toUpperCase();
	    if(cad == 'JPEG'){
	    	errorImagen = 0;
	    }else{
	    	errorImagen = 1;
	    }	
    }	

    if(errorImagen == 1){
		error=1;
		document.getElementById('lab_imagen').style.display = "none";
		document.getElementById('lab_imagen').style.display = "inline";
	    document.getElementById('lab_imagen').innerHTML = "La extensión del archivo no corresponde a la solicitadas";    	
    }else{
		document.getElementById('lab_imagen').style.display = "none";
	}
}






/*
if(accion==0){
	objeto = "nroIngrediente";
	campo = document.forms[formulario].elements[objeto].value;
	document.getElementById('lab_detalleAlimento').style.display = "none";
	if (campo==0){
		document.getElementById('lab_detalleAlimento').style.display = "inline";
	   	document.getElementById('lab_detalleAlimento').innerHTML = "Ingrese los ingredientes del Alimento"; 	
	   	error = 1;
	}
}	
*/

return error;
}



function verAlimento(idAlimento){
    $.ajax({
		url : "<?php echo site_url('Mn_alimento/ajax_datatable_verAlimento')?>/"+idAlimento,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			 document.getElementById('tablaIngrediente').innerHTML = data.registro;
			 document.getElementById('tablaPresentacion').innerHTML = data.registroP;
			 document.getElementById('m_nombre').value = data.nombre;
			 document.getElementById('m_descripcion').value = data.descripcion;
			 $(document).ready(function() {
			 });


		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}




////////////////////////////////////////////////////////////////////////////////

function eliminar_factura(id){
	swal({
		title: 'Esta seguro de eliminar la Factura de Compra?',
			type: 'warning',
			buttons:{
				cancel: {
					visible: true,
						text : 'No, Cancelar!',
								className: 'btn btn-danger'
				},        			
				confirm: {
					text : 'Si, Eliminar!',
					className : 'btn btn-success'
				}
			}
	}).then((willDelete) => {
		if (willDelete) {
		    $.ajax({
				url : "<?php echo site_url('Cp_factura/ajax_eliminar_factura')?>/"+id,
				type: "GET",                     
				dataType: "JSON",
				success: function(data){
					 document.getElementById('bodyTabla').innerHTML = data.registro;
					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
					 }); 
				},
				error: function (jqXHR, textStatus, errorThrown){
					alert('Error consultando la Base de Datos');
				}
			});
			swal("La Factura de Compra fué eliminada!", {
				icon: "success",
				buttons : {
					confirm : {
						className: 'btn btn-success'
					}
				}
			});
		}
	});

}






function ver_form(id){
   document.location.href = "<?php echo base_url('verAlimento')?>/"+id;
}







function mod_ingrediente(id){
	document.location.href = "<?php echo base_url('act_mIngrediente')?>/"+id;
/*	
	ruta = "<?php //echo site_url('cp_orden/act_modificar_producto')?>/"+id
	var Form = $('#formulario');
	Form.attr("action", ruta);
	Form.submit(); 
*/	
}



function cargar_dataTableIngrediente(id){
    $.ajax({
		url : "<?php echo site_url('Mn_alimento/ajax_datatableAlimento')?>/"+id,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
		    document.getElementById('bodyTablaIngrediente').innerHTML = data.registro;
			document.getElementById('nobAlimento').innerHTML = data.nombre;
			$(document).ready(function() {
				$('#basic-datatables').DataTable({
			    });	
			});

		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}






function guardar_add_ingredienteAlimento(){

    validar = valIngrediente(1);


    if(validar==0){
    	document.getElementById('cerrar02').disabled=true;
    	document.getElementById('guardar02').disabled=true;
    	id = document.getElementById('id_alimento').value;
        $.ajax({
            url : "<?php echo site_url('Mn_alimento/ajax_guardar_add_ingredienteAlimento')?>",
            type: "POST",
            data: $('#formularioIngrediente').serialize(),
            dataType: "JSON",
            success: function(data)
            {
		    	document.getElementById('cerrar02').disabled=false;
				document.getElementById('guardar02').disabled=false;
				
            	if(data.status==0){
                    document.getElementById('bodyTablaIngrediente').innerHTML = data.registro;
					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
				 	});         
                    
					swal("Registro almacenado Correctamente!", "", {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					});
					$('#mod_ingredienteAlimento').modal('hide');
				}else{
					document.getElementById('lab_ingrediente').style.display = "inline";
					document.getElementById('lab_ingrediente').innerHTML = "El Ingrediente ingresado, ya se encuentra asociado.";

					swal("No se actualizo el registro", "Ya el Ingrediente esta asociado", {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});

				}	 
	        },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error Insertando / modificanco registro, Comuniquese con el administrador');
            }
        });
    }else{
        //document.getElementById('cantidad').value = "0,00";
		swal("No se actualizo el registro", "Falta Información", {
			icon : "error",
			buttons: {        			
				confirm: {
					className : 'btn btn-danger'
				}
			},
		});
    }    

}


				
				
				
				

function elim_ingrediente(id){
	idAlimento = "";
	swal({
		title: 'Esta seguro de quitar el Ingrediente?',
			type: 'warning',
			buttons:{
				cancel: {
					visible: true,
						text : 'No, Cancelar!',
								className: 'btn btn-danger'
				},        			
				confirm: {
					text : 'Si, Eliminar!',
					className : 'btn btn-success'
				}
			}
	}).then((willDelete) => {
		if (willDelete) {
			$.ajax({
				url : "<?php echo site_url('Mn_alimento/ajax_borrar_ingrediente')?>",
				type: "GET",                     
				data:{'id':id, 'idAlimento':idAlimento},
				dataType: "JSON",
				success: function(data){
					if(data.status==0){
						document.getElementById('bodyTablaIngrediente').innerHTML = data.registro;
						$(document).ready(function() {
						   $('#basic-datatables').DataTable({
						   });	
					   });
						swal("El Ingrediente fué eliminado!", {
							icon: "success",
							buttons : {
								confirm : {
									className: 'btn btn-success'
								}
							}
						});
  				   }	
				},
				error: function (jqXHR, textStatus, errorThrown){
					alert('Error consultando la Base de Datos');
				}
			});
		}
	});

	
}







function mod_ingredienteAlimento(id){
    document.getElementById('guardar03').disabled=false;
	document.getElementById('cerrar03').disabled=false;     
     
    $.ajax({
		url : "<?php echo site_url('Mn_alimento/ajax_modificar_ingredienteAlimento')?>/"+id,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			 //document.getElementById('m_cantidad').value = data.cantidad;
			 document.getElementById('id_mod_mo').value = data.id;
			 //document.getElementById('lab_cantidad_mo').style.display = "none";
			 //ruta = "<?php //echo site_url('Combo_ctrl/ajax_combo_modulo')?>";
	         //cbo_modulo(ruta,data.id_modulo);   //Llenar combo    
             $('#modificar_ingredienteAlimento').modal('show');

		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});

}


function guardar_mod_ingredienteAlimento(){
	//validar campos formulario
	formulario = "formularioIngredienteAlimento";
	idAlimento = document.getElementById('id_alimento').value;
	error=0;
	error = valIngredienteAlimentoMod(2);
	if(error==0){
    	document.getElementById('guardar03').disabled=true;
		document.getElementById('cerrar03').disabled=true; 		
	}
	

	if(error == 0){
        $.ajax({
            url : "<?=base_url();?>Mn_alimento/ajax_guardar_ingredienteAlimento_mod/"+idAlimento,
            type: "POST",
            data: $('#formularioIngredienteAlimento').serialize(),
            dataType: "JSON",
            success: function(data)
            {
				document.getElementById('bodyTablaIngrediente').innerHTML = data.registro;
				//document.getElementById('m_cantidad').value = "0,00";
				$('#modificar_ingredienteAlimento').modal('hide');


	        },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error Insertando / modificanco registro, Comuniquese con el administrador');
            }
        });
    }else{
		swal("No se actualizo el registro", "", {
			icon : "error",
			buttons: {        			
				confirm: {
					className : 'btn btn-danger'
				}
			},
		});
    }    
}



/*function valIngredienteAlimentoMod(accion){

formulario = "formularioIngredienteAlimento";
error=0;

objeto = "m_cantidad";
campo = document.forms[formulario].elements[objeto].value;
campo = campo.replace(/(^\s*)|(\s*$)/g,"");
document.getElementById('lab_cantidad_mo').style.display = "none";

if (campo=="0,00" || campo.length==0){	
	document.getElementById('lab_cantidad_mo').style.display = "inline";
    document.getElementById('lab_cantidad_mo').innerHTML = "Ingrese la cantidad del Ingrediente"; 	
    document.getElementById('m_cantidad').value = "0,00";
    error = 1;
}


return error;
}
*/

////////// OPCIONES DE VENTA ////////////////////////



function act_presentacion(id){
	document.location.href = "<?php echo base_url('act_mPresentacion')?>/"+id;
}

function cargar_dataTablePresentacion(id){
    $.ajax({
		url : "<?php echo site_url('Mn_alimento/ajax_datatablePresentacion')?>/"+id,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			 document.getElementById('bodyTablaPresentacion').innerHTML = data.registro;
			 document.getElementById('nobAlimento').innerHTML = data.nobAlimento
			 document.getElementById('id_alimento').value=id;
			 $(document).ready(function() {

				$('#basic-datatables').DataTable({
			    });	
			 });

		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}


//Incluir presentacion del producto
function incPresentacion(){
	//document.location.href = "<?php //echo base_url('incPresentacion')?>";	

	ruta = "<?php echo site_url('Combo_ctrl/ajax_combo_tipoPresentacion')?>";
	cbo_tipoPresentacion(ruta,0);

	ruta = "<?php echo site_url('Combo_ctrl/ajax_combo_unidadMedida_presentacion')?>";
	cbo_unidadMedida_presentacion(ruta,0);

    document.getElementById('m_precio').value = "0,00";
    document.getElementById('m_pieza').value = "";
    document.getElementById('m_orden').value = "";


    document.getElementById('lab_tipoPresentacion').style.display = "none";
    document.getElementById('lab_precio').style.display = "none";
    document.getElementById('lab_pieza').style.display = "none";
    document.getElementById('lab_orden').style.display = "none";
    document.getElementById('lab_unidadMedida_presentacion').style.display = "none";

    //document.getElementById('id_cbo_tipoPresentacion').disabled=false;     
	document.getElementById('accion_formulario').value=1;    

	document.getElementById('cerrar01').disabled=false;     
	document.getElementById('guardar01').disabled=false;  

	document.getElementById('combo').style.display="inline";     
	document.getElementById('texto').style.display="none";     



    $('#inc_presentacion_modal').modal('show');
}


//Incluir presentacion del producto
function mod_presentacion(idPresentacion){
    document.getElementById('cerrar01').disabled=false;  
	document.getElementById('guardar01').disabled=false;  
    $.ajax({
		url : "<?php echo site_url('Mn_alimento/ajax_modificar_presentacion')?>/"+idPresentacion,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			document.getElementById('combo').style.display="none";     
			document.getElementById('texto').style.display="inline";     
             document.getElementById('m_nombre').value = data.nombre;
             document.getElementById('m_pieza').value = data.pieza;


			ruta = "<?php echo site_url('Combo_ctrl/ajax_combo_unidadMedida_presentacion')?>";
			cbo_unidadMedida_presentacion(ruta,data.unidadMedida);

 /*
			 ruta = "<?php echo site_url('Combo_ctrl/ajax_combo_unidadMedida')?>";
			 cbo_unidadMedida(ruta,data.unidadMedida);
*/			 
			 
             document.getElementById('m_precio').value = data.precio;
             document.getElementById('m_orden').value = data.orden;
			 document.getElementById('id_mod').value = data.id;
			 document.getElementById('lab_pieza').style.display = "none";
			 document.getElementById('lab_precio').style.display = "none";
			 document.getElementById('lab_orden').style.display = "none";
			 document.getElementById('lab_unidadMedida_presentacion').style.display = "none";

			 document.getElementById('accion_formulario').value="2"
             $('#inc_presentacion_modal').modal('show');

		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}	


/*function addPresentacion(){
  	//document.getElementById('guardar01').disabled=false;
	//document.getElementById('cerrar01').disabled=false; 
    idAlimento = document.getElementById('id_alimento').value;

    $.ajax({
		url : "<?php //echo site_url('Mn_alimento/ajax_datatable_alimento')?>/" + idAlimento,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			 //document.getElementById('tablaIngrediente').innerHTML = data.registro;
			 $(document).ready(function() {
                document.getElementById('id_alimento').value = data.idAlimento; 

				//ruta = "<?php //echo site_url('Combo_ctrl/ajax_combo_categoriaMenu')?>";
				//cbo_categoriaMenu(ruta,0);                  
			 });


		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}*/

function guardar_presentacion_form(){
    accion = document.getElementById('accion_formulario').value;
    if(accion==1){
        guardar_add_presentacion_form();    	
    }else{
        guardar_mod_presentacion_form();    	 

    }

}	


function guardar_add_presentacion_form(){
    formulario = "formulario_act_presentacion";
    $validar = validar_presentacion_form(formulario,0);
    if($validar==0){
  	    document.getElementById('guardar01').disabled=true;
		document.getElementById('cerrar01').disabled=true;

        $.ajax({
            url : "<?=base_url();?>Mn_alimento/ajax_guardar_presentacion",
            type: "POST",
            data: $('#formulario_act_presentacion').serialize(),
            dataType: "JSON",
            success: function(data)
            {
                if(data.status==0){ 
                    document.getElementById('bodyTablaPresentacion').innerHTML = data.registro;  
                    $('#inc_presentacion_modal').modal('hide');
					swal("Registro almacenado Correctamente!", "", {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					});


                }else{

					document.getElementById('lab_tipoPresentacion').style.display = "inline";
					document.getElementById('lab_tipoPresentacion').innerHTML = "El tipo de presentación, ya se encuentra registrado.";
			    	document.getElementById('guardar01').disabled=false;
					document.getElementById('cerrar01').disabled=false; 		

					swal("No se actualizo el registro", "", {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});

				}	

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error Insertando / modificanco registro, Comuniquese con el dministrador');
            }
        });         

	}else{
	    //document.getElementById('guardar01').disabled=false;
		//document.getElementById('cerrar01').disabled=false;
		swal("No se actualizo el registro", "Falta Información", {
				icon : "error",
				buttons: {        			
					confirm: {
						className : 'btn btn-danger'
					}
				},
		});
	}    

}

function guardar_mod_presentacion_form(){
    formulario = "formulario_act_presentacion";
    $validar = validar_presentacion_form(formulario,1);
	if($validar == 0){
   		document.getElementById('guardar01').disabled=true;
		document.getElementById('cerrar01').disabled=true; 		
        $.ajax({
            url : "<?=base_url();?>Mn_alimento/ajax_guardar_mod_presentacion",

            type: "POST",
            data: $('#formulario_act_presentacion').serialize(),
            dataType: "JSON",
            success: function(data)
            {
                document.getElementById('bodyTablaPresentacion').innerHTML = data.registro;  
                $('#inc_presentacion_modal').modal('hide');
				swal("Registro almacenado Correctamente!", "", {
					icon : "success",
					buttons: {        			
						confirm: {
							className : 'btn btn-success'
						}
					},
				});
	        },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error Insertando / modificanco registro, Comuniquese con el administrador');
            }
        });
    }else{
		swal("No se actualizo el registro", "Falta información", {
			icon : "error",
			buttons: {        			
				confirm: {
					className : 'btn btn-danger'
				}
			},
		});
    }    
}


function desactivar_presentacion(id){
	swal({
		title: 'Esta seguro de Desactivar la Presentación ?',
			type: 'warning',
			buttons:{
				cancel: {
					visible: true,
						text : 'No, Cancelar!',
								className: 'btn btn-danger'
				},        			
				confirm: {
					text : 'Si, Desactivar!',
					className : 'btn btn-success'
				}
			}
	}).then((willDelete) => {
		if (willDelete) {
		    $.ajax({
				url : "<?php echo site_url('Mn_alimento/ajax_desactivar_presentacion')?>/"+id,
				type: "GET",                     
				dataType: "JSON",
				success: function(data){
					 document.getElementById('bodyTablaPresentacion').innerHTML = data.registro;
					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
					 }); 
				},
				error: function (jqXHR, textStatus, errorThrown){
					alert('Error consultando la Base de Datos');
				}
			});
			swal("El registro fué desactivado!", {
				icon: "success",
				buttons : {
					confirm : {
						className: 'btn btn-success'
					}
				}
			});
		}
	});

}

function reactivar_presentacion(id){
	swal({
		title: 'Esta seguro de Reactivar la Presentación ?',
			type: 'warning',
			buttons:{
				cancel: {
					visible: true,
						text : 'No, Cancelar!',
								className: 'btn btn-danger'
				},        			
				confirm: {
					text : 'Si, Reactivar!',
					className : 'btn btn-success'
				}
			}
	}).then((willDelete) => {
		if (willDelete) {
		    $.ajax({
				url : "<?php echo site_url('Mn_alimento/ajax_reactivar_presentacion')?>/"+id,
				type: "GET",                     
				dataType: "JSON",
				success: function(data){
					 document.getElementById('bodyTablaPresentacion').innerHTML = data.registro;
					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
					 }); 
				},
				error: function (jqXHR, textStatus, errorThrown){
					alert('Error consultando la Base de Datos');
				}
			});
			swal("El registro fué desactivado!", {
				icon: "success",
				buttons : {
					confirm : {
						className: 'btn btn-success'
					}
				}
			});
		}
	});
}





function validar_presentacion_form(formulario,accion){
//accion 1= incluir
//accion 2= modificar
error=0;


if(accion==0){
	objeto = "id_cbo_tipoPresentacion";
	campo = document.forms[formulario].elements[objeto].value;
	document.getElementById('lab_tipoPresentacion').style.display = "none";
	if (campo==0){
		document.getElementById('lab_tipoPresentacion').style.display = "inline";
	   	document.getElementById('lab_tipoPresentacion').innerHTML = "Seleccione Presentación"; 	
	   	error = 1;
	}
}

objeto = "m_pieza";
campo = document.forms[formulario].elements[objeto].value;
campo = campo.replace(/(^\s*)|(\s*$)/g,"");

document.getElementById('lab_pieza').style.display = "none";
if (campo==0 || campo.length==0){
	document.getElementById('lab_pieza').style.display = "inline";
	document.getElementById('lab_pieza').innerHTML = "Edite el Nro Pieza / Porción"; 	
    error = 1;
}

objeto = "id_cbo_unidadMedida";
campo = document.forms[formulario].elements[objeto].value;
document.getElementById('lab_unidadMedida_presentacion').style.display = "none";
if (campo==0){
	document.getElementById('lab_unidadMedida_presentacion').style.display = "inline";
   	document.getElementById('lab_unidadMedida_presentacion').innerHTML = "Seleccione Unidad Medida"; 	
   	error = 1;
}




objeto = "m_precio";
campo = document.forms[formulario].elements[objeto].value;
campo = campo.replace(/(^\s*)|(\s*$)/g,"");

document.getElementById('lab_precio').style.display = "none";


if (campo=="0,00" || campo.length==0){	
	document.getElementById('lab_precio').style.display = "inline";
    document.getElementById('lab_precio').innerHTML = "Ingrese precio de venta "; 	
    document.getElementById('m_precio').value = "0,00";
    error = 1;
}


objeto = "m_orden";
campo = document.forms[formulario].elements[objeto].value;
campo = campo.replace(/(^\s*)|(\s*$)/g,"");

document.getElementById('lab_orden').style.display = "none";
if (campo==0 || campo.length==0){
	document.getElementById('lab_orden').style.display = "inline";
	document.getElementById('lab_orden').innerHTML = "Orden aparición"; 	
    error = 1;
}

return error;
}



function act_alimento_ingrediente(idPresentacion){
	document.location.href = "<?php echo base_url('act_pIngrediente')?>/"+idPresentacion;
}



/*
function cargar_dataTablePresentacionIngrediente(id){
    $.ajax({
		url : "<?php echo site_url('Mn_alimento/ajax_datatablePresentacionIngrediente')?>/"+id,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			 document.getElementById('bodyTablaPresentacionIngrediente').innerHTML = data.registro;
			 $(document).ready(function() {
				$('#basic-datatables').DataTable({
			    });	
			 });

		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}
*/







</script>
