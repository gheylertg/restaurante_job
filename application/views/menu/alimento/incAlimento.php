<?php
$incluir = base_url() . "assets/images/nuevo.png";
$asterisco = ' style="color=red; font-size:16px"';

$ruta = base_url() . "Mn_alimento/index"; 
$rutaProducto = base_url() . "Mn_alimento/ajax_guardar_alimentoSB";


$fechaMaxima = date("Y-m-d");




?>

<br>            
<div class="main-panel" style="padding-top: 10px">
    <div class="content">
        <div class="page-inner mt--5">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="card card-stats card-round" >
                        <div class="card-body" style="padding-bottom: 5px">
							 <h2><span style="color:blue;">Registrar Producto</span></h2>
                        </div>
                    </div>
                </div>
            </div>


            <form name="formulario" method="post" id="formulario" autocomplete="off" 
                  enctype="multipart/form-data">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                <div class="card card-stats card-round">
                <div class="card-body">

                <div class="row">
                    <div class="col-md-3">
                        <label>
                        <span style="color:#0000ff; font-size:16px; font-weight:600">
                            Nombre <span style="color:#ff0000;">*</span>
                        </span>
                        </label>
                    </div>
                    <div class="col-md-9">
                        <input type="text" id="m_nombre" name="m_nombre"
                               class="form-control"/>
                        <div id="lab_nombre" style="color:red; margin-left:9px; display:none">
                        </div>
                    </div>
                </div>  
                
                <div class="row"  style="padding-top: 5px">
                    <div class="col-md-3">
                        <label>
                        <span style="color:#0000ff; font-size:16px; font-weight:600">
                            Etiqueta menú (Máx. 20 Carácteres)<span style="color:#ff0000;">*</span>
                        </span>
                        </label>
                    </div>
                    <div class="col-md-3">
                        <input type="text" id="m_etiqueta" name="m_etiqueta"
                               class="form-control" maxlength="20"/>
                        <div id="lab_etiqueta" style="color:red; margin-left:9px; display:none">
                        </div>
                    </div>
                </div> 

                <div class="row" style="padding-top:5px">
                    <div class="col-md-3">
                        <label>
                        <span style="color:#0000ff; font-size:16px; font-weight:600">
                            Descripción <span style="color:#ff0000;">*</span>
                        </span>
                        </label>
                    </div>

                    <div class="col-md-9">
                        <textarea  id="m_descripcion" name="m_descripcion"
                               class="form-control" rows="3"></textarea>
                        <div id="lab_descripcion" style="color:red; margin-left:9px; display:none">
                        </div>
                    </div>
                </div>    


                
                


                <div class="row" style="padding-top: 5px">
                    <div class="col-md-3">
                         <label style="padding-top: 0px; margin-top: 5px;margin-left:0px;">
                             <span style="color:#0000ff; font-size:16px; font-weight:600">Categoría Menú <span style="color:#ff0000;">*</span></span>
                         </label>
                    </div>
                    <div class="col-md-9">
                        <div id="cbo_categoriaMenu"></div>
                        <div id="lab_categoriaMenu" style="color:red; margin-left:9px; display:none"></div>
                    </div>
                </div> 


                <div class="row" style="padding-top: 5px">
                    <div class="col-md-3">
                         <label style="padding-top: 0px; margin-top: 5px;margin-left:0px;">
                             <span style="color:#0000ff; font-size:16px; font-weight:600">Imagen</span>
                         </label>
                    </div>
                    <div class="col-md-6">
                        <input type="file" id="m_imagen" name="m_imagen"
                           class="form-control"/>
                        <div id="lab_imagen" style="color:red; margin-left:9px; display:none"></div>   
                    </div>
                    <div class="col-md-3">
                        <span style="color:#0000ff; font-size:16px;">Archivo con extensión JPEG, JPG, PNG</span>
                    </div>  
                </div>



                
 				<div class="row">
					<div class="col-md-3">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">Orden de Visualización <span style="color:#ff0000;">*</span></span> </label>

					</div>
					
					<div class="col-md-9">
                        <input style="padding-top: 5px;margin-right:-20px;margin-top: 12px;" 
                               type="text" id="m_orden" name="m_orden" class="form-control" 
                               placeholder="Ingrese el orden" maxlength="10" 
                               onkeyup="nro_entero('m_orden');" value="0">
                        <div id="lab_orden" style="color:red; margin-left:9px; display:none"></div>       
					</div>
				</div>               
                

            <hr>

            <div class="row">
                <div class="col-md-11">
                    <label>
                    <span style="color:#0000ff; font-size:20px; font-weight:600">
                        Ingredientes del Producto
                    </span>
                    </label>
                    <div id="lab_detalleAlimento" 
                         style="color:red; margin-left:9px; display:none">
                    </div>    
                    
                </div>  
                <div class="col-md-1">
                    <a href="javascript:void(0)" onclick="javascript:add_ingrediente()">
                       <img src="<?=$incluir;?>" style="width:35px; height:35px" alt="Incluir" title="Agregar Ingrediente">
                    </a>   
                </div>  
            </div>
                    
            <div class="row">
                <div class="col-md-12">
                    <div id="tablaIngrediente"></div> 

                </div>
            </div>  





            <div class="row">
                <div class="col-md-12" style="text-align: right;">
                    <button type="button" class="btn btn-danger" onclick="cerrar_form()" 
                            id="cerrar01">Cerrar</button>

                    <button type="button" class="btn btn-primary" onclick="guardar_form()" id="guardar01">
                          Guardar
                    </button>    

                </div>    
            </div> 
            
                </div>     
                </div> <!-- Fin card-body-->
                </div> <!-- card card-stats -->
            </div> <!-- col principal -->

            <input type="hidden" id="nroIngrediente" name="nroIngrediente" value="0">
            <input type="hidden" id="ruta_guardar_ingrediente" id="ruta_guardar_ingrediente" 
                   value="<?=$ruta;?>">
            <input type="hidden" id="rutaProducto" name="rutaProducto" value="<?=$rutaProducto;?>">       

            </form>
        </div>
    </div>
</div>    





<!-- ---------------------------------------------------------------------------- -->


<!-- Modal Incluir producto-->
<div class="modal fade" id="mod_ingrediente" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width:800px; margin-left:-100px">


            <div class="modal-header bg-primary text-white">
                <h4 class="modal-title" id="exampleModalLabel">
                    <div style="font-weight:700">Asociar Ingrediente</div>
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color:white; font-size:30px">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                
            <form name="formularioIngrediente" method="post" id="formularioIngrediente" autocomplete="off">   
                
                
              <div class="container">
                <div class="row" style="padding-top: 10px">
                    <div class="col-md-3">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
                             <span style="color:#0000ff; font-size:16px; font-weight:600">
                                 Ingrediente <span style="color:#ff0000;">*</span></span>
                             </label>
                    </div>
                   
                    <div class="col-md-9">
                        <div id="cbo_ingrediente"></div>

                        <div id="lab_ingrediente" style="color:red; margin-left:9px; display:none"></div>       
                    </div>
                </div>  


                <br>  
                <span style="color:#ff0000; font-size:16px; text-align:left">Campo obligatorio (*)</span>
              </div>                
            </div>
            <div class="modal-footer">

                   
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrar02"><span style="font-weight:700">Cerrar</span></button>


                <button type="button" class="btn btn-primary" 
                        onclick="guardar_add_ingrediente();" id="guardar02">
                      <span style="font-weight:700">Guardar</span>
                </button>




            </div>

                

            </form>  

        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="modificar_ingrediente" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width:800px; margin-left:-100px">


            <div class="modal-header bg-primary text-white">
                <h4 class="modal-title" id="exampleModalLabel">
                    <div style="font-weight:700">Modificar Ingrediente</div>
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color:white; font-size:30px">&times;</span>
                </button>
            </div>


            <div class="modal-body">
                
            <form name="formularioModificar" method="post" id="formularioModificar" autocomplete="off">   

                <div class="row" style="padding-top: 10px">
                    <div class="col-md-3">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
                             <span style="color:#0000ff; font-size:16px; font-weight:600">
                                 Cantidad <span style="color:#ff0000;">*</span></span>
                             </label>
                    </div>
                   
                    <div class="col-md-4">

                    <input type="text" name="x_cantidad" id="x_cantidad" 
                           class="form-control" style="text-align:right;" > 

                    <script type="text/javascript">
                    $("#x_cantidad").maskMoney({symbol:'R$ ', thousands:'.', decimal:',', symbolStay: true}); 
                    </script>

                        <div id="lab_cantidadx" style="color:red; margin-left:9px; display:none"></div>       
                    </div>
                </div>  

                <br>  
                <span style="color:#ff0000; font-size:16px; text-align:left">Campo obligatorio (*)</span>
                <input type="hidden" id="id_mod_x" name="id_mod_x">


                </form>      
              
            </div>
            <div class="modal-footer">

                   
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrar03"><span style="font-weight:700">Cerrar</span></button>


                <button type="button" class="btn btn-primary" 
                        onclick="guardar_mod_ingrediente();" id="guardar03">
                      <span style="font-weight:700">Guardar</span>
                </button>

                

            </div>

                

            

        </div>
    </div>
</div>



