<?php
$incluir = base_url() . "assets/images/nuevo.png";
$asterisco = ' style="color=red; font-size:16px"';
$ci = &get_instance();
$ci->load->library('Clssession');
$accion= $ci->clssession->accion(11,2); 


?>
        <br>            
        <div class="main-panel">
            <div class="content">
                <div class="page-inner mt--5">
                    <div class="row row-card-no-pd mt--2">
                        <div class="col-sm-12 col-md-12">
                            <div class="card card-stats card-round">
                                <<div class="card-body" style="margin-bottom:-30px;">
									 <h2><span style="color:blue">Actualizar Ingredientes Adicionales</span></h2>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row" style="margin-top:-20px">
						<div class="col-md-12">
                            <div class="card">
                                <div class="card-body" style="margin-bottom:-30px;">
                                    <?php
                                        if($accion==1){
                                    ?>
                                    <div  style="text-align:right">

                                    <a href="javascript:void(0)" onclick="javascript:incIngredienteAdicional()">
                                       <img src="<?=$incluir;?>" style="width:35px; height:35px" alt="Incluir" title="Agergar Ingrediente Adicional">
                                    </a>
                                    </div>
                                    <?php
                                        }
                                    ?>
                                    <br>
                                    <div class="table-responsive">
										<div id="bodyTabla">
										</div>
						            </div>
                                </div>
                            </div>
                        </div>    
                    </div>    
                </div>
            </div>
         </div>   

  


<!-- Modal Incluir producto-->
<div class="modal fade" id="act_ingredienteAdicional" tabindex="-1" role="dialog" 
                            
      aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width:800px; margin-left:-100px">

            <div class="modal-header bg-primary text-white">
                <h4 class="modal-title" id="exampleModalLabel">
                    <div id="titulo_mostrar" style="font-weight:700"></div>
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color:white; font-size:30px">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                
            <form name="formulario" method="post" id="formulario" autocomplete="off">   
                
              <div class="container">

                <div class="row">
                    <div class="col-md-3">
                         <label style="padding-top: 0px; margin-top: 5px;margin-left:0px;">
                             <span style="color:#0000ff; font-size:16px; font-weight:600">Ingrediente <span style="color:#ff0000;">*</span></span>
                         </label>
                    </div>
                    <div class="col-md-9">
                        <div id="cbo_ingredienteAdicional"></div>
                        <div id="m_nombre"></div>
                        <div id="lab_ingredienteAdicional" style="color:red; margin-left:9px; display:none"></div>
                    </div>
                </div>
                
                <div class="row"  style="padding-top: 5px">
                    <div class="col-md-3">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
                             <span style="color:#0000ff; font-size:16px; font-weight:600">Cantidad del Ingrediente<span style="color:#ff0000;">*</span></span>
                         </label>

                    </div>

                    <div class="col-md-4">
                        <input type="text" name="m_cantidad" id="m_cantidad" 
                               class="form-control" 
                               value="0,00" style="text-align:right; padding-top: 5px;margin-right:-20px;margin-top: 12px;" > 
                        <script type="text/javascript">
                            $("#m_cantidad").maskMoney({symbol:'R$ ', thousands:'.', decimal:',', symbolStay: true}); 
                        </script>

                        <div id="lab_cantidad" style="color:red; margin-left:9px; display:none"></div>       
                        
                    </div>                
                    <div class="col-md-1" >
						<div style="padding-top:15px">
						<div id="medida" style="color:blue; font-size:16px; font-weight: 600;"></div>
						</div>
					</div>
                    
                </div>
                
                <div class="row"  style="padding-top: 5px">
                    <div class="col-md-3">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
                             <span style="color:#0000ff; font-size:16px; font-weight:600">Precio de la Porción<span style="color:#ff0000;">*</span></span>
                         </label>

                    </div>

                    <div class="col-md-4">
                        <input type="text" name="m_precio" id="m_precio" 
                               class="form-control" 
                               value="0,00" style="text-align:right; padding-top: 5px;margin-right:-20px;margin-top: 12px;" > 
                        <script type="text/javascript">
                            $("#m_precio").maskMoney({symbol:'R$ ', thousands:'.', decimal:',', symbolStay: true}); 
                        </script>

                        <div id="lab_precio" style="color:red; margin-left:9px; display:none"></div>       
                    </div>                
                </div>
                

                <br>  
                <span style="color:#ff0000; font-size:16px; text-align:left">Campo obligatorio (*)</span>
              </div>                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrar01"><span style="font-weight:700">Cerrar</span></button>
                <button type="button" class="btn btn-primary" 
                        onclick="actualizar_form();" id="guardar01">
                      <span style="font-weight:700">Guardar</span>
                </button>
            </div>
            <input type="hidden" id="id_mod" name="id_mod">
            <input type="hidden" id="accion_formulario" name="accion_formulario" value="0">
            </form>  

        </div>
    </div>
</div>



