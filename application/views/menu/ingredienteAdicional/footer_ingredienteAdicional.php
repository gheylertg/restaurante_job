<!--cargar datatable-->
<script>

    $(document).ready(function(){
 		 <?php
         if($modulo==1){  
         ?>	
             cargar_dataTableIngredienteAdicional();

		 <?php
		 }
		 ?>	

    });


function nro_entero(objeto){
//valida si el nro es entero sin caracteres diferentes a numero
//formulario = nombre del formulario origen
//Objeto = nombre del campo u objeto

var valor = document.getElementById(objeto).value;
var tamano=valor.length;
i=0;
cad_numero="";
while(i<tamano){
    var numero = valor.charAt(i);
    if (isNaN(numero)==false)
        cad_numero=cad_numero+numero
    i=i+1;    
}
document.getElementById(objeto).value = cad_numero;
}


function cargar_dataTableIngredienteAdicional(){
    $.ajax({
		url : "<?php echo site_url('Mn_ingredienteAdicional/ajax_datatable')?>",
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			 document.getElementById('bodyTabla').innerHTML = data.registro;
			 $(document).ready(function() {
				$('#basic-datatables').DataTable({
			    });	
			 });

		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}





function incIngredienteAdicional(){
	$('#formulario')[0].reset(); // reset form on modals

    document.getElementById('accion_formulario').value="1";
    document.getElementById('titulo_mostrar').innerHTML = "Incluir Ingrediente Adicional";
    
    document.getElementById('cbo_ingredienteAdicional').style.display = "inline";
    document.getElementById('m_nombre').style.display = "none";
    
	ruta = "<?php echo site_url('Combo_ctrl/ajax_combo_ingredienteAdicional')?>";
	cbo_ingredienteAdicional(ruta,0);    

    document.getElementById('guardar01').disabled=false;
	document.getElementById('cerrar01').disabled=false;     	
    document.getElementById('lab_ingredienteAdicional').style.display = "none";
    document.getElementById('lab_cantidad').style.display = "none";
    document.getElementById('lab_precio').style.display = "none";
    document.getElementById('medida').style.display = "none";
    $('#act_ingredienteAdicional').modal('show');
}    


function obt_unidad(){
	id = document.getElementById('id_cbo_ingrediente').value;
    $.ajax({
		url : "<?php echo site_url('Mn_ingredienteAdicional/ajax_obt_medida')?>/"+id,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			 document.getElementById('medida').style.display = "inline";
			 document.getElementById('medida').innerHTML = data.medida;
		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}


function mod_ingrediente(idAlimento){
    document.getElementById('accion_formulario').value="2";
    document.getElementById('titulo_mostrar').innerHTML = "Modificar Ingrediente Adicional";
    document.getElementById('guardar01').disabled=false;
	document.getElementById('cerrar01').disabled=false;     

    $.ajax({
		url : "<?php echo site_url('Mn_ingredienteAdicional/ajax_modificar')?>/"+idAlimento,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
             document.getElementById('cbo_ingredienteAdicional').style.display = "none";
			 document.getElementById('m_nombre').style.display = "inline";
			 document.getElementById('m_nombre').innerHTML = data.nombre;
			 document.getElementById('m_precio').value = data.precio;
			 document.getElementById('m_cantidad').value = data.cantidad;
			 
			 document.getElementById('medida').style.display = "inline";
			 document.getElementById('medida').innerHTML = data.medida;			 
			 document.getElementById('id_mod').value = data.id;
		 	 //ruta = "<?php echo site_url('Combo_ctrl/ajax_combo_unidadMedida')?>";
	         //cbo_unidadMedida(ruta,data.id_unidad_medida); 
             $('#act_ingredienteAdicional').modal('show');
		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}





function actualizar_form(){

    accion = document.getElementById('accion_formulario').value;
    if(accion==1){
        guardar_add_form();
    }else{
        guardar_mod_form();
    } 
}


function guardar_add_form(){
formulario="formulario"
validar = validar_form(formulario, 1);
if(validar==0){
	$.ajax({
		url : "<?=base_url();?>Mn_ingredienteAdicional/ajax_guardar_ingrediente",
		type: "POST",
		data: $('#formulario').serialize(),
		dataType: "JSON",
		success: function(data)
		{
			if(data.status==0){ 
				document.getElementById('bodyTabla').innerHTML = data.registro;
				$('#act_ingredienteAdicional').modal('hide');
			}	 
		},
		error: function (jqXHR, textStatus, errorThrown)
		{
			alert('Error Insertando / modificanco registro, Comuniquese con el administrador');
		}
	});
}else{
	//document.getElementById('cantidad').value = "0,00";
	swal("No se actualizo el registro", "Falta Información", {
		icon : "error",
		buttons: {        			
			confirm: {
				className : 'btn btn-danger'
			}
		},
	});
}    

}



function guardar_mod_form(){

	//validar campos formulario
	formulario = "formulario";
	error=0;
	error = validar_form(formulario,2);
	if(error==0){
    	document.getElementById('guardar01').disabled=true;
		document.getElementById('cerrar01').disabled=true; 		
	}
	

	if(error == 0){
        $.ajax({
            url : "<?=base_url();?>Mn_ingredienteAdicional/ajax_guardar_mod",
            type: "POST",
            data: $('#formulario').serialize(),
            dataType: "JSON",
            success: function(data)
            {
            	if(data.status==0){ 
					 document.getElementById('bodyTabla').innerHTML = data.registro;
					 $('#act_ingredienteAdicional').modal('hide');

					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
				 	});            	


					swal("Registro almacenado Correctamente!", "", {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					});
				}	
	        },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error Insertando / modificanco registro, Comuniquese con el administrador');
            }
        });
    }else{
		swal("No se actualizo el registro", "", {
			icon : "error",
			buttons: {        			
				confirm: {
					className : 'btn btn-danger'
				}
			},
		});
    }    
}




function validar_form(formulario,accion){
//accion 1= incluir
//accion 2= modificar

error=0;

if(accion==1){
	objeto = "id_cbo_ingrediente";
	campo = document.forms[formulario].elements[objeto].value;
	document.getElementById('lab_ingredienteAdicional').style.display = "none";
	if (campo==0){
		document.getElementById('lab_ingredienteAdicional').style.display = "inline";
		document.getElementById('lab_ingredienteAdicional').innerHTML = "Seleccione el Ingrediente"; 	
		error = 1;
	}
}

objeto = "m_cantidad";
campo = document.forms[formulario].elements[objeto].value;
campo = campo.replace(/(^\s*)|(\s*$)/g,"");
document.getElementById('lab_cantidad').style.display = "none";

if (campo=="0,00" || campo.length==0){	
	document.getElementById('lab_cantidad').style.display = "inline";
    document.getElementById('lab_cantidad').innerHTML = "Ingrese la cantidad del Ingrediente";
    document.getElementById('m_cantidad').value = "0,00";
    error = 1;
}


objeto = "m_precio";
campo = document.forms[formulario].elements[objeto].value;
campo = campo.replace(/(^\s*)|(\s*$)/g,"");
document.getElementById('lab_precio').style.display = "none";

if (campo=="0,00" || campo.length==0){	
	document.getElementById('lab_precio').style.display = "inline";
    document.getElementById('lab_precio').innerHTML = "Ingrese el precio";
    document.getElementById('m_precio').value = "0,00";
    error = 1;
}

return error;
}


function desactivarIngrediente_form(idAlimento){
	swal({
		title: 'Esta seguro de ?',
			type: 'warning',
			buttons:{
				cancel: {
					visible: true,
						text : 'No, Cancelar!',
								className: 'btn btn-danger'
				},        			
				confirm: {
					text : 'Si, Desactivar!',
					className : 'btn btn-success'
				}
			}
	}).then((willDelete) => {
		if (willDelete) {
		    $.ajax({
				url : "<?php echo site_url('Mn_ingredienteAdicional/ajax_desactivar')?>/"+idAlimento,
				type: "GET",                     
				dataType: "JSON",
				success: function(data){
					 document.getElementById('bodyTabla').innerHTML = data.registro;
					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
					 }); 
				},
				error: function (jqXHR, textStatus, errorThrown){
					alert('Error consultando la Base de Datos');
				}
			});
			swal("El registro fué desactivado!", {
				icon: "success",
				buttons : {
					confirm : {
						className: 'btn btn-success'
					}
				}
			});
		}
	});
}


function reactivarIngrediente_form(idAlimento){
	swal({
		title: 'Esta seguro de ?',
			type: 'warning',
			buttons:{
				cancel: {
					visible: true,
						text : 'No, Cancelar!',
								className: 'btn btn-danger'
				},        			
				confirm: {
					text : 'Si, Reactivar!',
					className : 'btn btn-success'
				}
			}
	}).then((willDelete) => {
		if (willDelete) {
		    $.ajax({
				url : "<?php echo site_url('Mn_ingredienteAdicional/ajax_reactivar')?>/"+idAlimento,
				type: "GET",                     
				dataType: "JSON",
				success: function(data){
					 document.getElementById('bodyTabla').innerHTML = data.registro;
					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
					 }); 
				},
				error: function (jqXHR, textStatus, errorThrown){
					alert('Error consultando la Base de Datos');
				}
			});
			swal("El registro fué Reactivado!", {
				icon: "success",
				buttons : {
					confirm : {
						className: 'btn btn-success'
					}
				}
			});
		}
	});
	
	
}




</script>
