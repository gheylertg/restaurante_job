<?php
$incluir = base_url() . "assets/images/nuevo.png";
$asterisco = ' style="color=red; font-size:16px"';
$ci = &get_instance();
$ci->load->library('Clssession');
$accion= $ci->clssession->accion(11,2); 


?>
        <br>            
        <div class="main-panel">
            <div class="content">
                <div class="page-inner mt--5">
                    <div class="row row-card-no-pd mt--2">
                        <div class="col-sm-12 col-md-12">
                            <div class="card card-stats card-round">
                                <<div class="card-body" style="margin-bottom:-30px;">
									 <h2><span style="color:blue">Actualizar Ingredientes de la Presentación Alimento</span></h2>
                                     <h3><span id="nobPresentacion"></span></h3>

                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row" style="margin-top:-20px">
						<div class="col-md-12">
                            <div class="card">
                                <div class="card-body" style="margin-bottom:-30px;">
									<div class="row">
                                        <div class="col-md-1">
   										<a href="<?=base_url('act_mPresentacion');?>/0" class="btn btn-primary btn-sm item_edit2" 
											style="border-radius: 15px; font-size:16px; font-weight:500;
											height:45px;">Regresar
										</a>
										</div>
										<div class="col-md-11"></div>
									</div>
									<hr>
                                    <div class="table-responsive">
										<div id="bodyTablaPresentacionIngrediente">
										</div>
						            </div>
                                </div>
                            </div>
                        </div>    
                    </div>    
                </div>
            </div>
         </div>   

         <input type="hidden" id="id_presentacionIngrediente" name="id_PresentacionIngrediente">

    




<!-- Modal Incluir producto-->
<div class="modal fade" id="modificar_ingrediente" tabindex="-1" role="dialog" 
                                
      aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width:800px; margin-left:-100px">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel" style="color:blue">
                    <span>Modificar Ingrediente</span>
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                
            <form name="formularioIngrediente" method="post" id="formularioIngrediente" autocomplete="off">   
                
              <div class="container">


                <div class="row">
                    <div class="col-md-3">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
                             <span style="color:#0000ff; font-size:16px; font-weight:600">Cantidad<span style="color:#ff0000;">*</span></span>
                         </label>

                    </div>

                    <div class="col-md-3">
                        <input type="text" name="m_cantidad" id="m_cantidad" 
                               class="form-control" 
                               value="0,00" style="text-align:right; padding-top: 5px;margin-right:-20px;margin-top: 12px;" > 
                        <script type="text/javascript">
                            $("#m_cantidad").maskMoney({symbol:'R$ ', thousands:'.', decimal:',', symbolStay: true}); 
                        </script>

                        <div id="lab_cantidad" style="color:red; margin-left:9px; display:none"></div>       
                    </div>                
                </div>    

                <br>  
                <span style="color:#ff0000; font-size:16px; text-align:left">Campo obligatorio (*)</span>
              </div>                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrar01">Cerrar</button>
                <button type="button" class="btn btn-primary" 
                        onclick="guardar_mod_ingrediente();" id="guardar01">
                      Guardar
                </button>
            </div>
            <input type="hidden" id="id_mod" name="id_mod">
            </form>  

        </div>
    </div>
</div>



