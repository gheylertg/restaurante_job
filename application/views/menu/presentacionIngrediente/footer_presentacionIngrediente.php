<!--cargar datatable-->
<script>

    $(document).ready(function(){
 		 <?php
         if($modulo==1){  
         ?>	
             cargar_dataTablePresentacionIngrediente();

		 <?php
		 }
		 ?>	

    });


function nro_entero(objeto){
//valida si el nro es entero sin caracteres diferentes a numero
//formulario = nombre del formulario origen
//Objeto = nombre del campo u objeto

var valor = document.getElementById(objeto).value;
var tamano=valor.length;
i=0;
cad_numero="";
while(i<tamano){
    var numero = valor.charAt(i);
    if (isNaN(numero)==false)
        cad_numero=cad_numero+numero
    i=i+1;    
}
document.getElementById(objeto).value = cad_numero;
}


function cargar_dataTablePresentacionIngrediente(){
    $.ajax({
		url : "<?php echo site_url('Mn_presentacionIngrediente/ajax_datatablePresentacionIngrediente')?>",
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			 document.getElementById('bodyTablaPresentacionIngrediente').innerHTML = data.registro;
			 document.getElementById('nobPresentacion').innerHTML = data.nobPresentacion;
			 $(document).ready(function() {
				$('#basic-datatables').DataTable({
			    });	
			 });

		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}


function mod_ingrediente(idPresentacionIngrediente){
    document.getElementById('cerrar01').disabled=false;  
	document.getElementById('guardar01').disabled=false;  
    $.ajax({
		url : "<?php echo site_url('Mn_presentacionIngrediente/ajax_modificar_ingrediente')?>/"+idPresentacionIngrediente,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
             document.getElementById('m_cantidad').value = data.cantidad;
			 document.getElementById('id_mod').value = data.id;
			 document.getElementById('lab_cantidad').style.display = "none";
             $('#modificar_ingrediente').modal('show');

		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}	



function guardar_mod_ingrediente(){
	//validar campos formulario
	formulario = "formularioIngrediente";
   	document.getElementById('guardar01').disabled=true;
	document.getElementById('cerrar01').disabled=true; 		
	

    $.ajax({
        url : "<?=base_url();?>Mn_presentacionIngrediente/ajax_guardar_mod",
        type: "POST",
        data: $('#formularioIngrediente').serialize(),
        dataType: "JSON",
        success: function(data)
        {
			document.getElementById('bodyTablaPresentacionIngrediente').innerHTML = data.registro;
			$('#modificar_ingrediente').modal('hide');

		    $(document).ready(function() {
				$('#basic-datatables').DataTable({
			    });	
			});            	

			swal("Registro almacenado Correctamente!", "", {
					icon : "success",
					buttons: {        			
						confirm: {
							className : 'btn btn-success'
						}
					},
			});
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error Insertando / modificanco registro, Comuniquese con el administrador');
        }
    });
}





</script>
