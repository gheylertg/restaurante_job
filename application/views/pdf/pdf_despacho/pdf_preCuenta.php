<?php
//$logo = base_url() . "assets/images/img_empresa/" . $logoEmpresa;
$fecha = date("Y-m-d");

?>


<html>
<head>
    <style>

        img {
            height:60px;
            width:60px;

        }

        .linea-empresa{
           font-size: 16px;
           font-weight:700; 
        }
      
        .linea-subTitulo{
           font-size: 12px;
           font-weight:700; 

        }

        .titulo{
           font-size: 14px;
           font-weight:500; 
        }

        .linea-normal{
           font-size: 12px;
           ; 
        }


    </style>	

</head>	

<body>



<table width="7,60cm">
<tr>    
<td style="text-align: center">
    <span style="font-size: 10px; font-weight: 700"><?=strtoupper($empresa);?></span><br>
    <span style="font-size: 10px;">PRE CUENTA</span>
    <hr>
</td>
</tr>
</table>


<table width="7,60cm">  
<?php
if($rowDespacho->id_tipo_despacho==1){   
    $mesonero = trim($rowDespacho->nob_mesonero) .", ". trim($rowDespacho->ape_mesonero)
?>
<tr>    
<td style = "width:40%; vertical-align: top">
    <span style="font-size: 10px; font-weight: 700">MESA:<?=strtoupper($rowDespacho->mesa);?></span>
</td>
<td style = "width:60%; vertical-align: top">
    <span style="font-size: 8px; font-weight: 700">MESONERO: &nbsp;<?=strtoupper($mesonero);?></span>
</td>
</tr>
<?php
}else{
    if($rowDespacho->id_tipo_despacho==2){   
        $tipo = "Retiro en tienda";
    }else{
        $tipo = "Delivery";  
    }    
?>    

<tr>    
<td style = "vertical-align: top">
    <span style="font-size: 10px; font-weight: 700"><?=strtoupper($tipo);?></span>
</td>
<?php
}
?>

<tr>    
<td colspan="2" style = "vertical-align: top">
    <span style="font-size: 8px;">Venta No.:&nbsp;<?=strtoupper($rowDespacho->codigo_venta);?>&nbsp;/&nbsp;Fec. 
        <?=$rowDespacho->fecha;?></span>
    <hr>    
</td>
</tr>
</table>

<table width="7,60cm">  
<tr>    
<td style = "width:10%; vertical-align: top; text-align: center">
    <span style="font-size: 8px; font-weight: 700">Cant.</span>
</td>
<td style = "width:60%; vertical-align: top">
    <span style="font-size: 8px; font-weight: 700">Descripción.</span>
</td>
<td style = "width:15%; vertical-align: top; text-align: right ">
    <span style="font-size: 8px; font-weight: 700">Precio U.</span>
</td>
<td style = "width:15%; vertical-align: top; text-align: right">
    <span style="font-size: 8px; font-weight: 700">Precio.</span>
</td>
</tr>
</table>

<table width="7,60cm">  

<?php
foreach ($rowFactura as $key) {
    $nombre = trim($key->alimento) . " / " . trim($key->presentacion);
    if($key->id_categoria_menu==1){
        $nombre = trim($key->alimento)." / " . $key->nro_pieza . " PIEZAS ( PRECIO: ".$key->precio_sushi." )";
        $nombre.= " / ENVOLTURA: " . trim($key->envoltura);
        $nombre.= " ( PRECIO: ".$key->precio_envoltura." )";
        if($key->precio_adicional > 0.00){
            $nombre.= " / INGR. ADICIONAL: " . trim($key->descripcion_adicional);
        }
    }
?>
<tr>
<td width="10%" style="vertical-align: top; text-align: center">
    <span style="font-size: 8px;"><?=$key->cantidad;?></span>
</td>    
    
<td width="60%" style="vertical-align: top;">
    <span style="font-size: 8px;"><?=$nombre;?></span>
</td>    

<td width="15%" style="vertical-align: top; text-align: right">
    <span style="font-size: 8px;"><?=number_format($key->precio_unitario, 2,'.',',');?></span>
</td>    

<td width="15%" style="vertical-align: top; text-align: right">
    <span style="font-size: 8px;"><?=number_format($key->precio, 2,'.',',');?></span>
</td>    
</tr>
<?php
}  //fin del foreach
?>
<tr>
<td colspan="4">
    <hr>
</td>    
</tr>    

</table>


<?php
$totalParcial = number_format($rowDespacho->monto, 2,'.',',');
$etiquetaImpuesto = "Impuesto(".number_format($rowDespacho->impuesto, 2,'.',',').")";
$impuesto = $rowDespacho->monto * ($rowDespacho->impuesto/100);
$totalImpuesto = number_format($impuesto, 2,'.',',');
$totalFactura = $rowDespacho->monto + $impuesto;
$totalFactura = number_format($totalFactura, 2,'.',',');
?>



<table width="7,60cm" cellspacing="0">    
    <tr style="padding-top: 10px">
        <td style="width:75%;" ><span style="font-size: 10px; font-weight: 700">Total Parcial</span></td>
        <td style="width:25%; text-align: right"><span style="font-size: 10px"><?=$totalParcial;?></span>
        </td> 
    </tr>    
    <tr style="padding-top: 10px">
        <td style="width:75%;">
            <span style="font-size: 10px; font-weight: 700"><?=$etiquetaImpuesto;?></span>
        </td>  
        <td style="width:10%; text-align: right;">
             <span style="font-size: 10px"><?=$totalImpuesto;?></span>
             <hr>
        </td> 
    </tr>    

    <tr>
        <td style="width:75%;" ><span style="font-size: 10px; font-weight: 700">Total Pagar</span></td>
        <td style="width:10%;text-align: right" ><span style="font-size: 10px"><?=$totalFactura;?></span></td> 
    </tr>    
</table>    






