<?php
$logo = base_url() . "assets/images/img_empresa/" . $logoEmpresa;
$fecha = date("Y-m-d");

?>


<html>
<head>
    <style>

        img {
            height:60px;
            width:60px;

        }

        .linea-empresa{
           font-size: 16px;
           font-weight:700; 
        }
      
        .linea-subTitulo{
           font-size: 12px;
           font-weight:700; 

        }

        .titulo{
           font-size: 14px;
           font-weight:500; 
        }

        .linea-normal{
           font-size: 12px;
           ; 
        }


    </style>	

</head>	

<body>
<?php
$header=0;
$i = 0;
$linea=0;
$pagina = 0;
$fecha = substr($rowFactura->fecha, 7,2) ."/".substr($rowFactura->fecha, 5,2)."/".substr($rowFactura->fecha, 0,4);


foreach ($rowDetalleFactura as $key) {
    $i+=1;
    $linea+=1;

    if($header==0){
        $pagina+=1;
        $header = 1;
        $html = encabezado($pagina, $logo, $empresa, $sucursal_rut, $sucursal_direccion, $sucursal_correo, $sucursal_telefono, $nombreSucursal, $rowFactura);   
        echo $html;

?>    
    <?php
    }//fin del if del header
    ?>
    <tr>
        <td style="vertical-align: top; text-align: center">
            <span class="linea-normal"><?=$i;?></span>
        </td>
        <td style="vertical-align: top; padding-left:2px;text-align: left">
            <span class="linea-normal"><?=$key->descripcion;?></span>
        </td>
        <td style="vertical-align: top; text-align: center">
            <span class="linea-normal"><?=$key->cantidad;?></span>
        </td>
        <td style="vertical-align: top; text-align: right;padding-right: 2px">
            <span class="linea-normal"><?=$key->precio_unitario;?></span>
        </td>
        <td style="vertical-align: top; text-align: right; padding-right: 2px">
            <span class="linea-normal"><?=$key->precio;?></span>
        </td>

    <?php
    if($linea > 39){
        $header=0;
        $linea=0;
    ?>  
        </table> 
        <div style="page-break-after:always;"></div>
    <?php
    }



} //fin del foreach
?>
</table>

<?php
$linea+=1;
    if($linea > 39){
        $pagina+=1;
        $html = encabezado($pagina, $logo, $empresa, $sucursal_rut, $sucursal_direccion, $sucursal_correo, $sucursal_telefono, $nombreSucursal, $rowFactura);  
        echo $html;        
        echo "<hr>";
    }
?>  

<br>
<table width="100%"cellspacing="0">    
    <tr>
        <td style="width:75%;" ><span style="font-size: 12px"></span></td>
        <td style="width:15%; text-align: right" ><span style="font-size: 12px; font-weight: 600">Neto</span></td>  
        <td style="width:10%;text-align: right"  ><span style="font-size: 12px"><?=number_format($rowFactura->monto_cobrar, 2,'.',',');?></span></td> 
    </tr>    
    <tr>
        <td style="width:75%;" ><span style="font-size: 12px"></span></td>
        <td style="width:15%; text-align: right" ><span style="font-size: 12px; font-weight: 600">Descuento</span></td>  
        <td style="width:10%; text-align: right" ><span style="font-size: 12px"><?=number_format($rowFactura->descuento, 2,'.',',');?></span></td> 
    </tr>    

    <tr>
        <td style="width:75%;" ><span style="font-size: 12px"></span></td>
        <td style="width:15%; text-align: right" ><span style="font-size: 12px; font-weight: 600">Total</span></td>  
        <td style="width:10%;text-align: right" ><span style="font-size: 12px"><?=number_format($rowFactura->monto_venta, 2,'.',',');?></span></td> 
    </tr>    


</table>    






</body>


    
</html>


<?php
function encabezado($pagina, $logo, $empresa, $sucursal_rut, $sucursal_direccion, $sucursal_correo, $sucursal_telefono, $nombreSucursal,$row){ 
$fecha = substr($row->fecha,8,2)."/".substr($row->fecha,5,2)."/".substr($row->fecha,0,4);

$html = '<table style="width:100%">';
$html.= '<tr>';
$html.= '<td style="width:10%; vertical-align: top;">';
$html.= '   <img src="'.$logo.'">';
$html.= '</td>';
$html.= '<td style="width:40%">';
$html.= '    <span class="linea-empresa">'.$empresa.'</span><br>';
$html.= '    <span class="titulo">Sucursal: </span><span class="linea-normal">' . $nombreSucursal.'</span><br>';
$html.= '    <span class="linea-subTitulo">RUT: '.$sucursal_rut.'</span><br>';
$html.= '</td>';
$html.= '<td width="width:50%">';
$html.= '    <span class="linea-subTitulo">E-mail: '.$sucursal_correo.'</span><br>';
$html.= '    <span class="linea-subTitulo">Teléfono: '.$sucursal_telefono.'</span>';
$html.= '    <p class="linea-subTitulo">Dirección: '.$sucursal_direccion.'</p>';
$html.= '</td>';
$html.= '</tr>';
$html.= '</table>';
$html.= '<table style="width:100%">';
$html.= '<tr>';
$html.= '<td style="width:100%; text-align: right; vertical-align: top;">';
$html.= '    <span class="linea-subTitulo">Página: '.$pagina.'</span><br>';
$html.= '</td>';
$html.= '</tr>';
$html.= '</table>';


$html.= '<hr>';



$html.= '<table style="width:100%">';
$html.= '<tr>';
$html.= '<td style="width:50%">';
$html.= '    <span class="linea-empresa">Detalle Cliente</span><br>';


if($row->nombre_cliente=="" || $row->nombre_cliente==null){
    $nombre = "S / Información";
}else{    
    $nombre=$row->nombre_cliente;
}


$html.= '    <span class="linea-subTitulo">Cliente: </span><span class="linea-normal">' . $nombre . '</span><br>';
$html.= '    <span class="linea-subTitulo">Teléfono: </span><span class="linea-normal">'.$row->telefono.'</span>';
$html.= '</td>';
$html.= '<td style="width:50%">';
$html.= '    <span class="linea-empresa">Detalle factura</span><br>';
$html.= '    <span class="linea-subTitulo">Factura #: '.str_pad($row->documento, 6, "0", STR_PAD_LEFT).'</span><br>';
$html.= '    <span class="linea-subTitulo">Fecha: </span><span class="linea-normal">'.$fecha.'</span>';
$html.= '</td>';
$html.= '</table>';
$html.= '<br>';
$html.= '<table width="100%" border= "1px" cellspacing="0">';
$html.= '<tr>';
$html.= '   <th style="width:10%;" ><span style="font-size: 12px">Item</span></th>';
$html.= '   <th style="width:60%;" ><span style="font-size: 12px">Descripción</span></th>';
$html.= '   <th style="width:10%;" ><span style="font-size: 12px">Cantidad</span></th>';
$html.= '   <th style="width:10%;" ><span style="font-size: 12px">Precio Unitario</span></th>';
$html.= '   <th style="width:10%;" ><span style="font-size: 12px">Total</span></th>';
$html.= '</tr>';
return $html;
}



?>