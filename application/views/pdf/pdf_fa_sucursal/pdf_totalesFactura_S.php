<?php
$logo = base_url() . "assets/images/img_empresa/" . $logoEmpresa;
$fecha = date("Y-m-d");

?>


<html>
<head>
    <style>

        img {
            height:60px;
            width:60px;

        }

        .linea-empresa{
           font-size: 16px;
           font-weight:700; 
        }
      
        .linea-subTitulo{
           font-size: 12px;
           font-weight:700; 

        }

        .titulo{
           font-size: 14px;
           font-weight:500; 
        }

        .linea-normal{
           font-size: 12px;
           ; 
        }


    </style>	

</head>	
    
<body>

<?php

$header=0;
$i = 0;
$linea=0;
$pagina = 0;

foreach ($rowFactura as $key) {
  	$i+=1;
  	$linea+=1;

    $totalDespachado = number_format($key->total_despachado, 2,'.',',');
    $totalDescuento = number_format($key->total_descuento, 2,'.',',');
    $totalFactura = number_format($key->total_factura, 2,'.',',');

  	if($header==0){
        $pagina+=1;
        $header = 1;
?>

		<table style="width:100%">
			<tr>
		        <td style="width:10%; vertical-align: top;">
		        	<img src="<?=$logo;?>">
		        </td>
		        <td style="width:40%">
		        	<span class="linea-empresa"><?=$empresa;?></span><br>
		        	<span class="linea-subTitulo">Sucursal: <?=$sucursal;?></span><br>
		        </td>
		        <td width="width:30%"></td>
		        <td style="width:20%; text-align: right; vertical-align: top;">
		        	<span class="linea-subTitulo">Fecha: <?=$fecha;?></span><br>
		        	<span class="linea-subTitulo">Página: <?=$pagina;?></span><br>
		        </td>
			</tr>
		</table>	

		<table style="width:100%">
			<tr>
		        <td style="vertical-align: top;  text-align:center">
		        	<span class="titulo"><u>Listado de totales de facturación</u></span>
		        </td>
		    </tr>
		</table>   
		<br>


		<table style="width:100%">
            <?php
            if($swFecha>0){
                $fecInicial = substr($fecInicial, 8,2) ."-".substr($fecInicial, 5,2)."-".substr($fecInicial, 0,4);

            ?>
            <tr>
                <td style="vertical-align: top;">
                <?php
                if($swFecha==1){              
                ?>          
                    <span class="titulo">Dia: <span class="linea-normal"><?=$fecInicial;?></span></span>
                <?php
                }else{    
                    $fecFinal = substr($fecFinal, 8,2) ."-".substr($fecFinal, 5,2)."-".substr($fecFinal, 0,4);
                ?>          
                    <span class="titulo">Desde: </span><span class="linea-normal"><?=$fecInicial;?></span>
                    <span class="titulo">   Hasta: </span><span class="linea-normal"><?=$fecFinal;?></span>    


                <?php
                }
                ?>
                </td>
            </tr>
            <?php
            }
            ?>
		</table>  

		<table width="100%" border= "1px" cellspacing="0">
	    <tr>
	    	<th style="width:60%;" ><span style="font-size: 12px">Sucursal</span></th>
	    	<th style="width:10%;" ><span style="font-size: 12px">Cantidad factura</span></th>  
	    	<th style="width:10%;" ><span style="font-size: 12px">Monto despacho</span></th>  
	    	<th style="width:10%;" ><span style="font-size: 12px">Descuento</span></th>  
			<th style="width:10%;" ><span style="font-size: 12px">Total factura</span></th>  
		</tr>	
<?php
    }
?>
	<tr>
        <td style="vertical-align: top; text-align: left">
        	<span class="linea-normal"><?=$key->sucursal;?></span></span>
        </td>

        <td style="vertical-align: top; text-align: center;">
            <span class="linea-normal"><?=$key->cantidad_factura;?></span></span>
        </td>


        <td style="vertical-align: top; text-align: right; padding-right: 2px">
        	<span class="linea-normal"><?=$totalDespachado;?></span></span>
        </td>

        <td style="vertical-align: top; text-align: right; padding-right: 2px">
        	<span class="linea-normal"><?=$totalDescuento;?></span></span>
        </td>

        <td style="vertical-align: top; text-align: right; padding-right: 2px">
        	<span class="linea-normal"><?=$totalFactura;?></span></span>
        </td>
    </tr>
    <?php

    if($linea > 39){
    	$header=0;
    	$linea=0;
    ?>	
        </table> 
    	<div style="page-break-after:always;"></div>
    <?php
    }



    }
    ?>
</table>  






</body>
</html>
