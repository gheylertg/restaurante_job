


<!--cargar datatable-->
<script>
    $(document).ready(function(){
		 cargarDataTable(0);
	});


function cargarDataTable(id){
    $.ajax({
		url : "<?php echo site_url('Sg_administradorEmpresa/ajax_datatable')?>/"+id,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			 document.getElementById('bodyTabla').innerHTML = data.registro;
			 document.getElementById('administrador').value = data.administrador;


			 $(document).ready(function() {
				$('#basic-datatables').DataTable({
			    });	
			 });

		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}


function add_form(){
	$('#formulario')[0].reset(); // reset form on modals
    document.getElementById('titulo_mostrar').innerHTML = "Incluir Administrador Empresa";
	document.getElementById('guardar01').disabled=false;
	document.getElementById('cerrar01').disabled=false; 
    administrador = document.getElementById('administrador').value; 
    if(administrador=="1"){
        document.getElementById('mostrarEmpresa').style.display="inline";
 		ruta = "<?php echo site_url('Combo_ctrl/ajax_combo_EmpresaAdministrador')?>";
		cbo_empresaAdministrador(ruta,1);   
		ruta = "<?php echo site_url('Combo_ctrl/ajax_combo_administradorEmpresa')?>";
		cbo_administradorEmpresa(ruta,1);   
    }else{
        document.getElementById('mostrarEmpresa').style.display="none";    	
		ruta = "<?php echo site_url('Combo_ctrl/ajax_combo_administradorEmpresa')?>";
		cbo_administradorEmpresa(ruta,1);   
    }	
    $('#mod_modal').modal('show');
}    


function selUsuarioAdm(){
	idEmpresa = document.getElementById('id_cbo_empresaAdministrador').value;
	ruta = "<?php echo site_url('Combo_ctrl/ajax_combo_administradorEmpresa')?>";
	cbo_administradorEmpresa(ruta,idEmpresa);
}



function guardar_add_form(){
	//validar campos formulario

	formulario = "formulario";
	error=0;
	error = validar_form(formulario,1);
    if(error==0){
		document.getElementById('guardar01').disabled=true;
		document.getElementById('cerrar01').disabled=true;
		$('#mod_modal').modal('hide');
		$('#gifEspera_modal').modal('show');
    }

	if(error == 0){
        $.ajax({
            url : "<?=base_url();?>Sg_administradorEmpresa/ajax_guardar_add",
            type: "POST",
            data: $('#formulario').serialize(),
            dataType: "JSON",
            success: function(data)
            {
            	if(data.status==0){ 
					 document.getElementById('bodyTabla').innerHTML = data.registro;
					 $('#gifEspera_modal').modal('hide');

					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
				 	});            	


					swal("Registro almacenado Correctamente!", "", {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					});
				}	
	        },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error Insertando / modificanco registro, Comuniquese con el administrador');
            }
        });
    }else{
		swal("No se actualizo el registro", "", {
			icon : "error",
			buttons: {        			
				confirm: {
					className : 'btn btn-danger'
				}
			},
		});
    }    
}




function validar_form(formulario,accion){
//accion 1= incluir
//accion 2= modificar

error=0;





objeto = "id_cbo_administradorEmpresa";
campo = document.forms[formulario].elements[objeto].value;
document.getElementById('lab_administradorEmpresa').style.display = "none";
if (campo==0){
	document.getElementById('lab_administradorEmpresa').style.display = "inline";
   	document.getElementById('lab_administradorEmpresa').innerHTML = "Seleccione el Usuario"; 	
   	error = 1;
}

if(document.forms[formulario].elements['administrador']==1){
	objeto = "id_cbo_empresaAdministrador";
	campo = document.forms[formulario].elements[objeto].value;
	document.getElementById('lab_empresaAdministrador').style.display = "none";
	if (campo==0){
		document.getElementById('lab_empresaAdministrador').style.display = "inline";
	   	document.getElementById('lab_empresaAdministrador').innerHTML = "Seleccione la Empresa"; 	
	   	error = 1;
	}
}

return error;
}


function eliminar_form(id){
	swal({
		title: 'Esta seguro de ?',
			type: 'warning',
			buttons:{
				cancel: {
					visible: true,
						text : 'No, Cancelar!',
								className: 'btn btn-danger'
				},        			
				confirm: {
					text : 'Si, Desactivar el usuario!',
					className : 'btn btn-success'
				}
			}
	}).then((willDelete) => {
		if (willDelete) {
		    $.ajax({
				url : "<?php echo site_url('Sg_administradorEmpresa/ajax_desactivar')?>/"+id,
				type: "GET",                     
				dataType: "JSON",
				success: function(data){
					 document.getElementById('bodyTabla').innerHTML = data.registro;
					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
					 }); 
				},
				error: function (jqXHR, textStatus, errorThrown){
					alert('Error consultando la Base de Datos');
				}
			});
			swal("El registro fué desactivado!", {
				icon: "success",
				buttons : {
					confirm : {
						className: 'btn btn-success'
					}
				}
			});
		}
	});

}


function reactivar_form(id){
	swal({
		title: 'Esta seguro de ?',
			type: 'warning',
			buttons:{
				cancel: {
					visible: true,
						text : 'No, Cancelar!',
								className: 'btn btn-danger'
				},        			
				confirm: {
					text : 'Si, Reactivar Usuario!',
					className : 'btn btn-success'
				}
			}
	}).then((willDelete) => {
		if (willDelete) {
		    $.ajax({
				url : "<?php echo site_url('Sg_administradorEmpresa/ajax_reactivar')?>/"+id,
				type: "GET",                     
				dataType: "JSON",
				success: function(data){
					 document.getElementById('bodyTabla').innerHTML = data.registro;
					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
					 }); 
				},
				error: function (jqXHR, textStatus, errorThrown){
					alert('Error consultando la Base de Datos');
				}
			});
			swal("El registro fué reactivado!", {
				icon: "success",
				buttons : {
					confirm : {
						className: 'btn btn-success'
					}
				}
			});
		}
	});
}









</script>
