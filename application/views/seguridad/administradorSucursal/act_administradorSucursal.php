<?php
$incluir = base_url() . "assets/images/nuevo.png";
$asterisco = ' style="color=red; font-size:16px"';

$ci = &get_instance();
$ci->load->library('Clssession');
$permiso_add= $ci->clssession->accion(3,2); 


$espera = base_url() . "assets/images/tenor.gif";


?>
        <br>            
        <div class="main-panel">
            <div class="content">
                <div class="page-inner mt--5">
                    <div class="row row-card-no-pd mt--2">
                        <div class="col-sm-12 col-md-12">
                            <div class="card card-stats card-round">
                                <div class="card-body ">
									 <h2><span style="color:blue">Administrador Sucursal</span></h2>
                                </div>
                            </div>
                        </div>
                    </div>

            

                <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">

                                    <?php
                                        if($permiso_add==1){
                                    ?>
                                    <div  style="text-align:right">
										<a href="javascript:void(0)" onclick="javascript:add_form()">
										   <img src="<?=$incluir;?>" style="width:35px; height:35px" alt="Incluir" title="Incluir Administrador Sucursal">
										</a>
                                    </div>
                                    <?php
                                        }
                                    ?>
                                    <br>

                                    <div class="table-responsive">
										<div id="bodyTabla">
										</div>
										
										
                                    </div>
                                </div>
                            </div>
                        </div>    
                </div>

                <input type="hidden" id="idEmpresaAdm" name="idEmpresaAdm">


            </div>
    



<!-- Modal -->
<div class="modal fade" id="mod_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content" style="width:800px; margin-left:-100px">
			<div class="modal-header bg-primary text-white">
				<h4 class="modal-title" id="exampleModalLabel" style="color:#fff; font-weight: 600">
					<div style="font-weight:700" id="titulo_mostrar"></div>
				</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true" style="color:white; font-size:30px">&times;</span>
				</button>
			</div>

			<div class="modal-body">
				
			<form name="formulario" method="post" id="formulario" autocomplete="off">	
				
				
			<div class="container">

				<div class="row">
					<div class="col-md-3">
                         <label style="padding-top: 5px; margin-top: 14px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							     Sucursal <span style="color:#ff0000;">*</span></span>
							 </label>

					</div>
					
					<div class="col-md-9">
						<div id="cbo_sucursalAdm" style="padding-top: 5px; margin-left:0px;"></div>
						<div id="lab_sucursalAdm" style="color:red; margin-left:9px; display:none"></div>
					</div>
				</div>	

				<div class="row" style="padding-top: 5px">
					<div class="col-md-3">
                         <label style="padding-top: 0px; margin-top: 14px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							     Usuario <span style="color:#ff0000;">*</span></span>
							 </label>

					</div>
					
					<div class="col-md-9">
						<div id="cbo_administradorSucursal"></div>
						<div id="lab_administradorSucursal" style="color:red; margin-left:9px; display:none"></div>
					</div>
				</div>	




                <br>  
				<span style="color:#ff0000; font-size:16px; text-align:left">Campo obligatorio (*)</span>
			  </div>				
			</div>
			<div class="modal-footer">

                   
				<button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrar01"><span style="font-weight:700">Cerrar</span></button>


		        <button type="button" class="btn btn-primary" onclick="guardar_add_form()" id="guardar01">
		              <span style="font-weight:700">Guardar</span>
		        </button>
			</div>
			</form>  

		</div>
	</div>
</div>

