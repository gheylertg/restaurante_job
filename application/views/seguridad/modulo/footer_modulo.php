


<!--cargar datatable-->
<script>
    $(document).ready(function(){
		 cargarDataTable(0);
	});






function cargarDataTable(id){
    $.ajax({
		url : "<?php echo site_url('Sg_modulo/ajax_datatable')?>/"+id,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			 document.getElementById('bodyTabla').innerHTML = data.registro;

			 $(document).ready(function() {
				$('#basic-datatables').DataTable({
			    });	
			 });

		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}


function actualizar_form(){
    accion = document.getElementById('accion_formulario').value;
    if(accion==1){
        guardar_add_form();
    }else{
        guardar_mod_form();
    } 


}




function mostrar_lab(){
    document.getElementById('lab_nombre').style.display = "none";
    document.getElementById('lab_identificador').style.display="none";
    document.getElementById('lab_logo').style.display="none";
    document.getElementById('lab_orden').style.display="none";
}


function add_form(){
    document.getElementById('accion_formulario').value="1";
    document.getElementById('titulo_mostrar').innerHTML = "Incluir Modulo del Sistema";


	 document.getElementById('m_nombre').value = "";
	 document.getElementById('m_logo').value = "";
	 document.getElementById('m_identificador').value = "";
	 document.getElementById('m_orden').value = "";

	 document.getElementById('lab_nombre').style.display = "none";
	 document.getElementById('lab_logo').style.display = "none";
	 document.getElementById('lab_orden').style.display = "none";
	 document.getElementById('lab_identificador').style.display = "none";




    //document.getElementById('url_mostrar').style.display = "inline";   
    document.getElementById('guardar01').disabled=false;
	document.getElementById('cerrar01').disabled=false; 
	mostrar_lab();

    document.getElementById('nombre_orig').value = "";
    $('#mod_modal').modal('show');
}

	



function modificar_form(id){
    document.getElementById('accion_formulario').value="2";
    document.getElementById('titulo_mostrar').innerHTML = "Modificar Modulo del Sistema";
    //document.getElementById('url_mostrar').style.display = "inline";

	mostrar_lab();
    document.getElementById('guardar01').disabled=false;
	document.getElementById('cerrar01').disabled=false;  


    $.ajax({
		url : "<?php echo site_url('Sg_modulo/ajax_modificar')?>/"+id,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			 document.getElementById('m_nombre').value = data.nombre;
			 document.getElementById('m_logo').value = data.logo;
			 document.getElementById('m_identificador').value = data.identificador;
			 document.getElementById('m_orden').value = data.orden;
			 //document.getElementById('m_url').value = data.url;
			 document.getElementById('id_mod').value = data.id;
			 document.getElementById('val_error').value = "0";
			 document.getElementById('lab_nombre').style.display = "none";
			 //document.getElementById('lab_url').style.display = "none";
			 document.getElementById('lab_logo').style.display = "none";
			 document.getElementById('lab_orden').style.display = "none";
			 document.getElementById('lab_identificador').style.display = "none";


			 document.getElementById('nombre_orig').value = data.nombre;
             $('#mod_modal').modal('show');

		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}



function guardar_add_form(){
	//validar campos formulario
	formulario = "formulario";
	error=0;
	error = validar_form(formulario,1);

    if(error==0){
	    document.getElementById('guardar01').disabled=false;
		document.getElementById('cerrar01').disabled=false;  
    }


	if(error == 0){
        $.ajax({
            url : "<?=base_url();?>Sg_modulo/ajax_guardar_add",
            type: "POST",
            data: $('#formulario').serialize(),
            dataType: "JSON",
            success: function(data)
            {
            	if(data.status==0){ 
					 document.getElementById('bodyTabla').innerHTML = data.registro;
					 $('#mod_modal').modal('hide');

					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
				 	});            	


					swal("Registro almacenado Correctamente!", "", {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					});
				}else{
					document.getElementById('lab_nombre').style.display = "inline";
					document.getElementById('lab_nombre').innerHTML = "El Nombre ingresado, ya se encuentra registrado.";
			    	document.getElementById('guardar01').disabled=false;
					document.getElementById('cerrar01').disabled=false; 					
					swal("No se actualizo el registro", "", {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});

				}	
	        },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error Insertando / modificanco registro, Comuniquese con el administrador');
            }
        });
    }else{
		swal("No se actualizo el registro", "", {
			icon : "error",
			buttons: {        			
				confirm: {
					className : 'btn btn-danger'
				}
			},
		});
    }    






} //Fin de add form	





function guardar_mod_form(){

	//validar campos formulario
	formulario = "formulario";
	error=0;
	error = validar_form(formulario,2);

	if(error == 0){
        $.ajax({
            url : "<?=base_url();?>Sg_modulo/ajax_guardar_mod",
            type: "POST",
            data: $('#formulario').serialize(),
            dataType: "JSON",
            success: function(data)
            {
            	if(data.status==0){ 
					 document.getElementById('bodyTabla').innerHTML = data.registro;
					 $('#mod_modal').modal('hide');

					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
				 	});            	


					swal("Registro almacenado Correctamente!", "", {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					});
				}else{
					document.getElementById('lab_nombre').style.display = "inline";
					document.getElementById('lab_nombre').innerHTML = "El Nombre ingresado, ya se encuentra registrado.";
			    	document.getElementById('guardar01').disabled=false;
					document.getElementById('cerrar01').disabled=false; 					
					swal("No se actualizo el registro", "", {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});

				}	
	        },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error Insertando / modificanco registro, Comuniquese con el administrador');
            }
        });
    }else{
		swal("No se actualizo el registro", "", {
			icon : "error",
			buttons: {        			
				confirm: {
					className : 'btn btn-danger'
				}
			},
		});
    }    
}







function eliminar_form(id){
	swal({
		title: 'Esta seguro de ?',
			type: 'warning',
			buttons:{
				cancel: {
					visible: true,
						text : 'No, Cancelar!',
								className: 'btn btn-danger'
				},        			
				confirm: {
					text : 'Si, Desactivar!',
					className : 'btn btn-success'
				}
			}
	}).then((willDelete) => {
		if (willDelete) {
		    $.ajax({
				url : "<?php echo site_url('Sg_modulo/ajax_desactivar')?>/"+id,
				type: "GET",                     
				dataType: "JSON",
				success: function(data){
					 document.getElementById('bodyTabla').innerHTML = data.registro;
					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
					 }); 
				},
				error: function (jqXHR, textStatus, errorThrown){
					alert('Error consultando la Base de Datos');
				}
			});
			swal("El registro fué desactivado!", {
				icon: "success",
				buttons : {
					confirm : {
						className: 'btn btn-success'
					}
				}
			});
		}
	});

}

function reactivar_form(id){
	swal({
		title: 'Esta seguro de ?',
			type: 'warning',
			buttons:{
				cancel: {
					visible: true,
						text : 'No, Cancelar!',
								className: 'btn btn-danger'
				},        			
				confirm: {
					text : 'Si, Reactivar!',
					className : 'btn btn-success'
				}
			}
	}).then((willDelete) => {
		if (willDelete) {
		    $.ajax({
				url : "<?php echo site_url('Sg_modulo/ajax_reactivar')?>/"+id,
				type: "GET",                     
				dataType: "JSON",
				success: function(data){
					 document.getElementById('bodyTabla').innerHTML = data.registro;
					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
					 }); 
				},
				error: function (jqXHR, textStatus, errorThrown){
					alert('Error consultando la Base de Datos');
				}
			});
			swal("El registro fué reactivado!", {
				icon: "success",
				buttons : {
					confirm : {
						className: 'btn btn-success'
					}
				}
			});
		}
	});
}







function validar_form(formulario,accion){
//accion 1= incluir
//accion 2= modificar

error=0;

objeto = "m_nombre";
campo = document.forms[formulario].elements[objeto].value;
campo = campo.replace(/(^\s*)|(\s*$)/g,"");
nombre = campo;
document.getElementById('lab_nombre').style.display = "none";

if (campo.length==0){
	document.getElementById('lab_nombre').style.display = "inline";
    document.getElementById('lab_nombre').innerHTML = "Edite el nombre"; 	
    error = 1;
}


objeto = "m_identificador";
campo = document.forms[formulario].elements[objeto].value;
campo = campo.replace(/(^\s*)|(\s*$)/g,"");
nombre = campo;
document.getElementById('lab_identificador').style.display = "none";

if (campo.length==0){
	document.getElementById('lab_identificador').style.display = "inline";
    document.getElementById('lab_identificador').innerHTML = "Edite el Identificador del Modulo"; 	
    error = 1;
}


/*  No es obligatorio lo dejo por motivo por si se presenta el caso
if(accion==1){
	objeto = "m_url";
	campo = document.forms[formulario].elements[objeto].value;
	campo = campo.replace(/(^\s*)|(\s*$)/g,"");
	document.getElementById('lab_url').style.display = "none";

	if (campo.length==0){
		document.getElementById('lab_url').style.display = "inline";
    	document.getElementById('lab_url').innerHTML = "Edite el URL del Modulo"; 	
    	error = 1;
	}
}
*/


objeto = "m_logo";
campo = document.forms[formulario].elements[objeto].value;
campo = campo.replace(/(^\s*)|(\s*$)/g,"");

document.getElementById('lab_logo').style.display = "none";
if (campo.length==0){
	document.getElementById('lab_logo').style.display = "inline";
    document.getElementById('lab_logo').innerHTML = "Edite el logo"; 	
    error = 1;
}

objeto = "m_orden";
campo = document.forms[formulario].elements[objeto].value;
campo = campo.replace(/(^\s*)|(\s*$)/g,"");

document.getElementById('lab_orden').style.display = "none";
if (campo.length==0){
	document.getElementById('lab_orden').style.display = "inline";
	document.getElementById('lab_orden').innerHTML = "Edite el orden de vizualización"; 	
    error = 1;
}

return error;
}





</script>
