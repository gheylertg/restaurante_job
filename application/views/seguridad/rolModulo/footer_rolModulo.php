


<!--cargar datatable-->
<script>
    $(document).ready(function(){
         
        <?php
        if($modulo==1){  
        ?>	
			 cargarDataTable(0);
		 <?php
		 }
		 ?>

        <?php
        if($modulo==2){  
        ?>	
            cargarDataTable_rolModulo(<?=$idRol;?>);
        <?php
        }
        ?>

        <?php
        if($modulo==3){  
        ?>	
            cargarDataTable_rolProceso(<?=$idModuloRol;?>);
        <?php
        }
        ?>


	});


function cargarDataTable(id){
    $.ajax({
		url : "<?php echo site_url('Sg_rolModulo/ajax_datatable')?>/"+id,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			 document.getElementById('bodyTabla').innerHTML = data.registro;

			 $(document).ready(function() {
				$('#basic-datatables').DataTable({
			    });	
			 });
		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}

function cargarDataTable_rolModulo(id){
    $.ajax({
		url : "<?php echo site_url('Sg_rolModulo/ajax_datatable_rolModulo')?>/"+id,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			 document.getElementById('bodyTabla').innerHTML = data.registro;
			 $(document).ready(function() {
			 	document.getElementById('id_rol_principal').value = data.idRol;
			 	document.getElementById('nob_rol_principal').value = data.nobRol;
			 	document.getElementById('nombreRol').innerHTML = data.nobRol;
				$('#basic-datatables').DataTable({
			    });	
			 });
		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}



function add_form(){
    idRol = document.getElementById('id_rol_principal').value
    nombreRol = document.getElementById('nob_rol_principal').value


	$('#formulario')[0].reset(); // reset form on modals
  	document.getElementById('guardar01').disabled=false;
	document.getElementById('cerrar01').disabled=false; 		
    document.getElementById('accion_formulario').value="1";
    document.getElementById('rolNombre').innerHTML =nombreRol;
    document.getElementById('titulo_mostrar').innerHTML = "Asociar Modulo al Rol";
    document.getElementById('idRol').value = idRol;
    document.getElementById('section-modulo').style.display="inline";
	ruta = "<?php echo site_url('Combo_ctrl/ajax_combo_rolModulo')?>";
	cbo_rolModulo(ruta,idRol);   //Llenar combo    

    /// --------------  Armar permiso
    $.ajax({
		url : "<?php echo site_url('Sg_rolModulo/ajax_permiso_add')?>",
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			 $(document).ready(function() {
			 	  document.getElementById('permisologia').innerHTML = data.registro;
			 	  document.getElementById('nroPermiso').value = data.nroPermiso;
			 	  document.getElementById('arregloPermiso').value = data.arregloPermiso;
			 });
		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
    $('#mod_modal').modal('show');
}


function actualizar_form(){
    accion = document.getElementById('accion_formulario').value;
    if(accion=='1'){

        guardar_add_form();
    }else{
        guardar_mod_form();
    } 
}


function guardar_add_form(){
	//validar campos formulario
	formulario = "formulario";
	error=0;
	error = validar_form(formulario,1);

	if(error==0){
    	document.getElementById('guardar01').disabled=true;
		document.getElementById('cerrar01').disabled=true; 		
	}

	if(error == 0){
        $.ajax({
            url : "<?=base_url();?>Sg_rolModulo/ajax_guardar_add",
            type: "POST",
            data: $('#formulario').serialize(),
            dataType: "JSON",
            success: function(data)
            {

            	if(data.status==0){ 
					 document.getElementById('bodyTabla').innerHTML = data.registro;
					 $('#mod_modal').modal('hide');

					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
				 	});        	


					swal("Registro almacenado Correctamente!", "", {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					});
				}else{
					//document.getElementById('lab_nombre').style.display = "inline";
					//document.getElementById('lab_nombre').innerHTML = "El Nombre ingresado, ya se encuentra registrado.";
					swal("No se actualizo el registro", "", {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});

				}	
	        },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error Insertando / modificanco registro, Comuniquese con el administrador');
            }
        });
    }else{
		swal("No se actualizo el registro", "", {
			icon : "error",
			buttons: {        			
				confirm: {
					className : 'btn btn-danger'
				}
			},
		});
    }    
} //Fin de add form	


function rol_moduloAct(idRol){
        $.ajax({
		url : "<?php echo site_url('Sg_rolModulo/ajax_actualizarModulo')?>/"+idRol,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			document.location.href = "<?php echo base_url('rolModuloActualizar')?>"; 

		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}




//************* Modificar ******************
function modificar_form(id){

    
    //cbo_modelo(ruta,data.id_proyecto_local,data.id_proyecto_local);   //Llenar combo    
	idRol = document.getElementById('id_rol_principal').value;
	nombreRol = document.getElementById('nob_rol_principal').value;
	document.getElementById('guardar01').disabled=false;
	document.getElementById('cerrar01').disabled=false; 		

  	document.getElementById('id_mod').value = id;
    document.getElementById('accion_formulario').value="2";
    document.getElementById('idRol').value=idRol;
    document.getElementById('titulo_mostrar').innerHTML = "Modificar Permiso del y Modulo del Sistema";
    document.getElementById('section-modulo').style.display="none";
    
    $.ajax({
		url : "<?php echo site_url('Sg_rolModulo/ajax_modificarPermiso')?>/"+id,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			cadenaTitulo = nombreRol + " / " + data.nombreModulo;
			document.getElementById('rolNombre').innerHTML = cadenaTitulo;
	 	    document.getElementById('permisologia').innerHTML = data.registro;
	 	    document.getElementById('nroPermiso').value = data.nroPermiso;
	 	    document.getElementById('arregloPermiso').value = data.arregloPermiso;
            $('#mod_modal').modal('show');

		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}



function guardar_mod_form(){

  ///validar campos formulario
	formulario = "formulario";
	error=0;
	error = validar_form(formulario,2);

	if(error==0){
    	document.getElementById('guardar01').disabled=true;
		document.getElementById('cerrar01').disabled=true; 		
	}
	

	if(error == 0){
        $.ajax({
            url : "<?=base_url();?>Sg_rolModulo/ajax_guardar_mod",
            type: "POST",
            data: $('#formulario').serialize(),
            dataType: "JSON",
            success: function(data)
            {
            	if(data.status==0){ 
					 document.getElementById('bodyTabla').innerHTML = data.registro;
					 $('#mod_modal').modal('hide');

					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
				 	});            	


					swal("Registro almacenado Correctamente!", "", {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					});
				}else{
					swal("No se actualizo el registro", "", {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});

				}	
	        },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error Insertando / modificanco registro, Comuniquese con el administrador');
            }
        });
    }else{
		swal("No se actualizo el registro", "", {
			icon : "error",
			buttons: {        			
				confirm: {
					className : 'btn btn-danger'
				}
			},
		});
    } 

}


function eliminar_form(id){
	swal({
		title: 'Esta seguro de eliminar el modulo asociado al Rol?',
			type: 'warning',
			buttons:{
				cancel: {
					visible: true,
						text : 'No, Cancelar!',
								className: 'btn btn-danger'
				},        			
				confirm: {
					text : 'Si, Desactivar!',
					className : 'btn btn-success'
				}
			}
	}).then((willDelete) => {
		if (willDelete) {
		    $.ajax({
				url : "<?php echo site_url('Sg_rolModulo/ajax_desactivar')?>/"+id,
				type: "GET",                     
				dataType: "JSON",
				success: function(data){
					 document.getElementById('bodyTabla').innerHTML = data.registro;
					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
					 }); 
				},
				error: function (jqXHR, textStatus, errorThrown){
					alert('Error consultando la Base de Datos');
				}
			});
			swal("El registro fué desactivado!", {
				icon: "success",
				buttons : {
					confirm : {
						className: 'btn btn-success'
					}
				}
			});
		}
	});
}


function moduloProceso_form(idRolModulo){
$.ajax({
	url : "<?php echo site_url('Sg_rolModulo/ajax_actualizarProceso')?>/"+idRolModulo,
	type: "GET",                     
	dataType: "JSON",
	success: function(data){
		document.location.href = "<?php echo base_url('rolModuloProceso')?>"; 

	},
	error: function (jqXHR, textStatus, errorThrown){
		alert('Error consultando la Base de Datos');
	}
});
}




function validar_form(formulario,accion){
//accion 1= incluir
//accion 2= modificar

error=0;


if(accion==1){
	objeto = "id_cbo_rolModulo";
	campo = document.forms[formulario].elements[objeto].value;
	document.getElementById('lab_rolModulo').style.display = "none";
	if (campo==0){
		document.getElementById('lab_rolModulo').style.display = "inline";
	   	document.getElementById('lab_rolModulo').innerHTML = "Seleccione el modulo del Sistema"; 	
	   	error = 1;
	}
}


label = "lab_permiso"; 
checkbox = "permiso";
nroPermiso = document.forms[formulario].elements['nroPermiso'].value;   

swPermiso=0;
document.getElementById(label).style.display = "none";

i = 0;
while(i < nroPermiso && swPermiso==0){
    i+=1;
    objeto = checkbox+i.toString();
    var isChecked = document.getElementById(objeto).checked;
    if(isChecked){
        swPermiso=1;
    }    
} 

if (swPermiso==0){
	document.getElementById(label).style.display = "inline";
   	document.getElementById(label).innerHTML = "Seleccione al menos un permiso"; 	
   	error = 1;
}

return error;
}






/////////////////////////////////////////////////////
//////////////Procesos asociados ////////////////////
/////////////////////////////////////////////////////

function cargarDataTable_rolProceso(id){
    $.ajax({
		url : "<?php echo site_url('Sg_rolModulo/ajax_datatable_proceso')?>/"+id,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			 document.getElementById('bodyTabla').innerHTML = data.registro;

			 $(document).ready(function() {
			 	document.getElementById('id_rol_modulo_principal').value = data.idRolModulo;
			 	document.getElementById('nob_modulo_principal').value = data.nobModulo;
			 	document.getElementById('id_rol_principal').value = data.idRol;
			 	document.getElementById('nombreModulo').innerHTML = data.nobModulo;
				$('#basic-datatables').DataTable({
			    });	
			 });

		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}



function eliminarProceso_form(id){
	swal({
		title: 'Esta seguro de eliminar el Sub modulo asociado?',
			type: 'warning',
			buttons:{
				cancel: {
					visible: true,
						text : 'No, Cancelar!',
								className: 'btn btn-danger'
				},        			
				confirm: {
					text : 'Si, Desactivar!',
					className : 'btn btn-success'
				}
			}
	}).then((willDelete) => {
		if (willDelete) {
		    $.ajax({
				url : "<?php echo site_url('Sg_rolModulo/ajax_desactivarProceso')?>/"+id,
				type: "GET",                     
				dataType: "JSON",
				success: function(data){
					 document.getElementById('bodyTabla').innerHTML = data.registro;
					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
					 }); 
				},
				error: function (jqXHR, textStatus, errorThrown){
					alert('Error consultando la Base de Datos');
				}
			});
			swal("El registro fué eliminado satisfactoriamente!", {
				icon: "success",
				buttons : {
					confirm : {
						className: 'btn btn-success'
					}
				}
			});
		}
	});

}



function asociar_form(id){
	swal({
		title: 'Esta seguro de asociar el Sub modulo o Proceso?',
			type: 'warning',
			buttons:{
				cancel: {
					visible: true,
						text : 'No, Cancelar!',
								className: 'btn btn-danger'
				},        			
				confirm: {
					text : 'Si, Asociar!',
					className : 'btn btn-success'
				}
			}
	}).then((willDelete) => {
		if (willDelete) {
		    $.ajax({
				url : "<?php echo site_url('Sg_rolModulo/ajax_addProceso')?>/"+id,
				type: "GET",                     
				dataType: "JSON",
				success: function(data){
					 document.getElementById('bodyTabla').innerHTML = data.registro;
					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
					 }); 
				},
				error: function (jqXHR, textStatus, errorThrown){
					alert('Error consultando la Base de Datos');
				}
			});
			swal("El registro fué asociado satisfactoriamente!", {
				icon: "success",
				buttons : {
					confirm : {
						className: 'btn btn-success'
					}
				}
			});
		}
	});

}

function regresarRolModulo(){
	idRol = document.getElementById('id_rol_principal').value;
        $.ajax({
		url : "<?php echo site_url('Sg_rolModulo/ajax_actualizarModulo')?>/"+idRol,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			document.location.href = "<?php echo base_url('rolModuloActualizar')?>"; 

		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}





</script>
