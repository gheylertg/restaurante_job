<?php
$incluir = base_url() . "assets/images/nuevo.png";
$asterisco = ' style="color=red; font-size:16px"';
$ci = &get_instance();
$ci->load->library('Clssession');
$accion= $ci->clssession->accion(3,2); 

$ruta = base_url() . "Sg_usuario/ajax_guardar_add"; 
$rutaFoto = base_url() . "Sg_usuario/ajax_guardar_modFoto"; 



?>
        <br>            
        <div class="main-panel">
            <div class="content">
                <div class="page-inner mt--5">
                    <div class="row row-card-no-pd mt--2">
                        <div class="col-sm-12 col-md-12">
                            <div class="card card-stats card-round">
                                <div class="card-body ">
									 <h2><span style="color:blue">Usuarios del Sistema</span></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                


               

                <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
					                <?php
					                    if($accion==1){
					                ?>

					                <div class="col-md-12">
					                        <div class="card-header" style="text-align:right">
												<a href="javascript:void(0)" onclick="javascript:add_form()">
												   <img src="<?=$incluir;?>" style="width:35px; height:35px" alt="Incluir" title="Incluir usuario">
												</a>
						                    </div>
					                </div>    
					                <?php
					                    }
					                ?>
					                <br>

                                    <div class="table-responsive">
										<div id="bodyTabla">
										</div>
										
										
                                    </div>
                                </div>
                            </div>
                        </div>    
                </div>
            </div>
    



<!-- Modal -->
<div class="modal fade" id="mod_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content" style="width:1100px; margin-left:-260px">
			<div class="modal-header bg-primary text-white">
				<h4 class="modal-title" id="exampleModalLabel" style="color:white">
					<div style="font-weight: 700" id="titulo_mostrar"></div>
				</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true" style="color:white; font-size:30px">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				
			<form name="formulario" method="post" id="formulario" 
			      enctype="multipart/form-data">	
				
				
			  <div class="container">
				<div class="row">
					<div class="col-md-4">
                         <label><span style="color:#0000ff; font-size:16px; font-weight:600">
							 RUN<span style="color:#ff0000;">*</span></span>
					     </label>
                        <input type="text" name="m_run" id="m_run" class="form-control" 
                               placeholder="Edite el RUN" maxlength="100" minlength="0">
                        <div id="lab_run" style="color:red; margin-left:9px; display:none"></div>
					</div>
				</div>	

				<div class="row">
					<div class="col-md-6" style="padding-top: 10px">
                         <label><span style="color:#0000ff; font-size:16px; font-weight:600">
							 Nombre(s) <span style="color:#ff0000;">*</span></span>
					     </label>
                        <input type="text" name="m_nombre" id="m_nombre" class="form-control" 
                               placeholder="Edite el nombre" maxlength="100" minlength="0">
                        <div id="lab_nombre" style="color:red; margin-left:9px; display:none"></div>
					</div>

					<div class="col-md-6" style="padding-top: 10px">
                         <label><span style="color:#0000ff; font-size:16px; font-weight:600">
							Apellido(s) <span style="color:#ff0000;">*</span></span>
					     </label>
                        <input type="text" name="m_apellido" id="m_apellido" class="form-control" 
                               placeholder="Edite el apellido" maxlength="100" minlength="0">

                               
                        <div id="lab_apellido" style="color:red; margin-left:9px; display:none"></div>
					</div>
				</div>	

                
               
            <div id="div-clave"> 
				<div class="row">
					<div class="col-md-6" style="padding-top: 10px">
                         <label>
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							 Contraseña <span style="color:#ff0000;">*</span></span>
					     </label>

                        <input type="password" name="m_password" id="m_password" class="form-control" 
                               placeholder="Edite el password" maxlength="100" minlength="0">
                        <div id="lab_password" style="color:red; margin-left:9px; display:none"></div>
					</div>

					<div class="col-md-6" style="padding-top: 10px">
                         <label>
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							 Repetir Contraseña <span style="color:#ff0000;">*</span></span>
					     </label>

                        <input type="password" name="m_password_rep" id="m_password_rep" 
                               class="form-control"
                               placeholder="Repita el password" maxlength="100" minlength="0">
                        <div id="lab_password_rep" style="color:red; margin-left:9px; display:none"></div>
                    </div>    
				</div>	
				<div class="row">
					<div class="col-md-12">
                        <label><span  style="color: #00f" >
                            La contraseña debe contener al menos seis(6) caracteres</span>                  
                        </label>                                     

					</div>
				</div>		

			</div>

				<div class="row">
					<div class="col-md-12" style="padding-top: 10px">
						 <hr> 
                         <label>
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							 Correo electrónico</span>
					     </label>

                        <input type="text" name="m_correo" id="m_correo" class="form-control" 
                               placeholder="Edite el Correo Electrónico" maxlength="200" minlength="0">
                        <div id="lab_correo" style="color:red; margin-left:9px; display:none"></div>       
					</div>
				</div>	


				<div class="row">
					<div class="col-md-4" style="padding-top: 10px">
                         <label>
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							 Sexo <span style="color:#ff0000;">*</span></span>
					     </label>
						<div id="cbo_sexo"></div>
						<div id="lab_sexo" style="color:red; margin-left:9px; display:none"></div>
					</div>

            		<div id="div-foto"> 
					<div class="col-md-8" style="padding-top: 10px">
                        <label>
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							 Foto Usuario</span>
					    </label>

                        <input type="file" id="m_foto" name="m_foto"
                               class="form-control"	/>
                        <div>Archivo con extensión JPEG, JPG, PNG</div>

					</div>
					</div>		
				</div>	

                <br>  
				<span style="color:#ff0000; font-size:16px; text-align:left">Campo obligatorio (*)</span>
			  </div>				
			</div>
			<div class="modal-footer">

                   
				<button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrar01">Cerrar</button>


		        <button type="button" class="btn btn-primary" onclick="actualizar_form()" id="guardar01">
		              Guardar
		        </button>

			</div>


            <input type="hidden" id="id_mod" name="id_mod">
            <input type="hidden" id="run_orig" name="run_orig">
            <input type="hidden" id="val_error" name="val_error" value="0">
            <input type="hidden" id="accion_formulario" name="accion_formulario" value="0">
            <input type="hidden" id="ruta" name="ruta" value="<?=$ruta;?>">


			</form>  

		</div>
	</div>
</div>






<!-- Ver usuarios  -->
<!-- Modal -->
<div class="modal fade" id="ver_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content" style="width:900px; margin-left:-200px">
			<div class="modal-header bg-primary text-white">
				<h4 class="modal-title" id="exampleModalLabel" style="color:white">
					<div style="font-weight: 700" id="c_titulo_mostrar"></div>
				</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true" style="color:white; font-size:30px">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				

			  <div class="container">
				<div class="row">

					<div class="col-md-9">
                        <label ><span style="color:#0000ff; font-size:16px; font-weight:600">
							     RUN <span style="color:#ff0000;">*</span></span>
						</label>
                        <input type="text" name="c_run" id="c_run" class="form-control"
                               readonly="true">


                        <br>
                         <label><span style="color:#0000ff; font-size:16px; font-weight:600">
							 Nombre(s) <span style="color:#ff0000;">*</span></span>
					     </label>
                        <input type="text" name="c_nombre" id="c_nombre" class="form-control" 
                               readonly="true">
                        <br>
                         <label><span style="color:#0000ff; font-size:16px; font-weight:600">
							Apellido(s) <span style="color:#ff0000;">*</span></span>
					     </label>
                        <input type="text" name="c_apellido" id="c_apellido" class="form-control" 
                               readonly="true">                               
					</div>

					<div class="col-md-3">
						<div id="div-imagen"></div>	
					</div>

				</div>	
                


				<div class="row">
					<div class="col-md-12" style="padding-top: 10px">
						 <hr> 
                         <label>
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							 Correo electrónico</span>
					     </label>

                        <input type="text" name="c_correo" id="c_correo" class="form-control" 
                               readonly="true">
      
					</div>
				</div>	


				<div class="row">
					<div class="col-md-4" style="padding-top: 10px">
                         <label>
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							 Sexo <span style="color:#ff0000;">*</span></span>
					     </label>
                        <input type="text" name="c_sexo" id="c_sexo" class="form-control" 
                               readonly="true">
					</div>

					<!--<div class="col-md-9" style="padding-top: 10px">
                         <label>
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							 Foto Usuario</span>
					     </label>

                        <input type="file" id="m_foto" id="m_foto" name="userfile"
                               class="form-control"/>
					</div>
				    -->


				</div>	

                <br>  
			  </div>				
			</div>
			<div class="modal-footer">

                   
				<button type="button" class="btn btn-danger" data-dismiss="modal"><span >Cerrar</span></button>


			</div>

		</div>
	</div>
</div>




<!-- ----------------- Modificar Nota ------------------------- -->

<!-- Modal -->
<div class="modal fade" id="modFoto_modal" tabindex="-1" role="dialog" 
     aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content" style="width:900px; margin-left:-180px">
			<div class="modal-header bg-primary text-white">
				<h4 class="modal-title" id="exampleModalLabel" style="color:white">
					<div style="font-weight: 700" id="titulo_mostrarFoto"></div>
				</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true" style="color:white; font-size:30px">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				
			<form name="formularioFoto" method="post" id="formularioFoto" 
			      enctype="multipart/form-data">	
				
				
			  <div class="container">
                
				<div class="row">
					<div class="col-md-8" style="padding-top: 10px">
                        <label>
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							 Foto Usuario</span>
					    </label>

                        <input type="file" id="mod_foto" name="mod_foto"
                               class="form-control"	/>
                        <div style="color:blue">Archivo con extensión JPEG, JPG, PNG</div>
                        <div id="lab_foto" style="color:red; margin-left:9px; display:none"></div>       
					</div>
				</div>	

                <br>  
				<span style="color:#ff0000; font-size:16px; text-align:left">Campo obligatorio (*)</span>
			  </div>				
			</div>
			<div class="modal-footer">

                   
				<button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrar02">Cerrar</button>


		        <button type="button" class="btn btn-primary" onclick="guardar_modFoto_form()" 
		                id="guardar02">
		              Guardar
		        </button>

			</div>

			<input type="hidden" id="id_mod_foto" name="id_mod_foto">
			<input type="hidden" id="ruta_foto" name="ruta_foto" value="<?=$rutaFoto;?>">

			</form>  

		</div>
	</div>
</div>

