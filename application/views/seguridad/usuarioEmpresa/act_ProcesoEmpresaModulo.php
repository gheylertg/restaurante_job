<?php
$incluir = base_url() . "assets/images/nuevo.png";
$asterisco = ' style="color=red; font-size:16px"';
$ci = &get_instance();
$ci->load->library('Clssession');
$accion= $ci->clssession->accion(3,2); 

?>



        <br>            
        <div class="main-panel">
            <div class="content">
                <div class="page-inner mt--5">
                    <div class="row row-card-no-pd mt--2">
                        <div class="col-sm-12 col-md-12">
                            <div class="card card-stats card-round">
                                <div class="card-body ">
									 <h2><span style="color:blue">Asociar Sub Modulos al Modulo Seleccionado</span></h2>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
			            <div class="col-md-9">
		                    <div class="card">
		                        <div class="card-header" style="text-align:left">
		                             <h4>
		                             	<span style="color:blue">
		                             	<b><div id="nob_modulo_principal" name="nob_modulo_principal"></div></b></span>
		                             </h4>
		                        </div>
		                    </div>
		                </div>                     


		                <div class="col-md-2">
		                    <div class="card">
		                        <div class="card-header" style="text-align:center; height:65px; padding-top: 10px">

										<a href="javascript:void(0)" class="btn btn-primary btn-sm item_edit2" onclick="javascript:regresarEmpresaModulo()" 
											style="border-radius: 15px; font-size:16px; font-weight:500;
											height:45px;">Regresar</a>
								</div>
							</div>
						</div>				


                <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="table-responsive">
										<div id="bodyTabla">
										</div>
										
										
                                    </div>
                                </div>
                            </div>
                        </div>    
                </div>
            </div>

            <input type="hidden" id="id_rol_modulo_principal" name="id_rol_modulo_principal">
            <input type="hidden" id="nob_modulo_principal" name="nob_modulo_principal">
            <input type="hidden" id="id_usuario_empresa_principal" name="id_usuario_empresa_principal">

            
    


