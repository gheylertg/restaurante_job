<?php
$incluir = base_url() . "assets/images/nuevo.png";
$asterisco = ' style="color=red; font-size:16px"';
$ci = &get_instance();
$ci->load->library('Clssession');


if($swPermisoModulo==1){
	$accion= $ci->clssession->accion(3,2); 
}else{
    $accion=0;
}	


?>
        <br>            
        <div class="main-panel">
            <div class="content">
                <div class="page-inner mt--5">
                    <div class="row row-card-no-pd mt--2">
                        <div class="col-sm-12 col-md-12">
                            <div class="card card-stats card-round">
                                <div class="card-body ">
									 <h2><span style="color:blue">Asociar Modulo al Usuario</span></h2>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
			            <div class="col-md-9">
		                    <div class="card">
		                        <div class="card-header" style="text-align:left">
		                             <h4>
		                             	<span style="color:blue">
		                             	<b><div id="nombreUsuario" name="nombreUsuario"></div></b></span>
		                             </h4>
		                        </div>
		                    </div>
		                </div>                     


		                <div class="col-md-2">
		                    <div class="card">
		                        <div class="card-header" style="text-align:center; height:65px; 
                                     padding-top: 10px">

                                    <div id="botonRegresar"></div> 
								</div>
							</div>
						</div>				
                <?php
                    if($accion==1){
                ?> 
		                <div class="col-md-1">
		                    <div class="card">
		                        <div class="card-header" style="text-align:right">
									<a href="javascript:void(0)" onclick="javascript:addModulo_form()">
									   <img src="<?=$incluir;?>" style="width:35px; height:30px" alt="Incluir" title="Asociar Modulo al USuario">
									</a>
		                        </div>
		                    </div>
		                </div> 

                <?php
                    }
                ?> 

		            </div>    
		            


                <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="table-responsive">
										<div id="bodyTabla">
										</div>
										
										
                                    </div>
                                </div>
                            </div>
                        </div>    
                </div>
            </div>

            <input type="hidden" id="idUsuarioEmpresa" name="idUsuarioEmpresa">
            <input type="hidden" id="nobUsuario" name="nobUsuario">



<!-- Modal -->
<div class="modal fade" id="mod_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content" style="width:900px; margin-left:-140px">
			<div class="modal-header" style="background-color:#00f;">
				<h4 class="modal-title" id="exampleModalLabel" style="color:white">
					<div id="titulo_mostrar"></div>
				</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				
			<form name="formulario" method="post" id="formulario" autocomplete="off">	
				
				
			  <div class="container">
                 
                 <div class="row" style="padding-top: 10px">
                 	<div class="col-md-12">
                        <h3><span style="color:blue"><div id="ModuloUsuario" name="moduloUsuario"></span></h3> 
                        	<hr>
                 	</div> 
                 </div> 


				<div class="row">
					<div class="col-md-4">
                         <label style="padding-top: 0px; margin-top: 10px;margin-left:0px;">
							 <span style="color:#0000ff; font-size:14px; font-weight:600">Asociar Modulo <span style="color:#ff0000;">*</span></span>
						 </label>

					</div>
					
					
					<div class="col-md-8">
						<div id="cbo_moduloUsuario"></div>
						<div id="lab_moduloUsuario" style="color:red; margin-left:9px; display:none"></div>
					</div>
				</div>	


                <br>  
				<span style="color:#ff0000; font-size:16px; text-align:left">Campo obligatorio (*)</span>
			  </div>				
			</div>
			<div class="modal-footer">
                   
				<button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrar02">Cerrar</button>
		        <button type="button" class="btn btn-primary" onclick="actualizarModulo_form('1')" id="guardar02">
		              Guardar
		        </button>

			</div>


            <input type="hidden" id="idUsuario_add" name="idUsuario_add">
            <input type="hidden" id="accion_formulario" name="accion_formulario" value="0">


			</form>  

		</div>
	</div>
</div>


<!---- Permisologia ------------>
<!-- Modal -->
<div class="modal fade" id="permisologia_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content" style="width:900px; margin-left:-140px">
			<div class="modal-header" style="background-color:#00f;">
				<h4 class="modal-title" id="exampleModalLabel" style="color:white">
					<div id="titulo_mostrar_permiso"></div>
				</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				
			<form name="formularioPermiso" method="post" id="formularioPermiso" autocomplete="off">	
				
				
			  <div class="container">
                 
                 <div class="row" style="padding-top: 10px">
                 	<div class="col-md-12">
                        <h3><span style="color:blue"><div id="rolNombrePermiso" name="rolNombrePermiso"></span></h3> 
                        	<hr>
                 	</div> 
                 </div> 


                <div id="section-modulo">

			    </div>  <!-- fin section-modulo -->

                <div class="row">
		            <div class="col-md-12">
		                <div class="card">
		                    <div class="card-header" style="text-align:left">
								<h3><span style="color:blue">Permisología</span></h3> 
								<br>

                                     <div id="permisologia" name="permisologia"></div>     

                                     <div id="lab_permiso" style="color:red; margin-left:9px; display:none"></div>
	                        </div>
	                    </div>
	                </div> 
	             </div>  

                <br>  
				<span style="color:#ff0000; font-size:16px; text-align:left">Campo obligatorio (*)</span>
			  </div>				
			</div>
			<div class="modal-footer">

                   
				<button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrar01">Cerrar</button>


		        <button type="button" class="btn btn-primary" onclick="actualizarPermiso_form()" id="guardar01">
		              Guardar
		        </button>

			</div>


            <input type="hidden" id="idUEM" name="idUEM">
            <input type="hidden" id="idModulo" name="idModulo">

            <input type="hidden" id="arregloPermiso" name="arregloPermiso" value="">
            <input type="hidden" id="nroPermiso" name="nroPermiso">


			</form>  

		</div>
	</div>
</div>







