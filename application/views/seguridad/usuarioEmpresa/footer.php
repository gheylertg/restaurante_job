

<!--cargar datatable-->
<script>
    $(document).ready(function(){
         <?php
         if($modulo==1){  
         ?>	
			 cargarDataTable(0);
		 <?php
		 }

		 if($modulo==2){
		 ?>
            cargarDataTable_empresa(<?=$idUsuario;?>);
         <?php
         }

         if($modulo==3){  
         ?>	

            cargarDataTable_moduloEmpresa(<?=$idUsuarioEmpresa;?>);
         <?php
         }
         ?>

         <?php 

         if($modulo==4){  
         ?>	

            cargarDataTable_empresaProceso(<?=$idEmpresaModulo;?>);
         <?php
         }
         ?>




	});


function cargarDataTable(id){
    $.ajax({
		url : "<?php echo site_url('Sg_usuarioEmpresa/ajax_datatable')?>/"+id,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			 document.getElementById('bodyTabla').innerHTML = data.registro;

			 $(document).ready(function() {
				$('#basic-datatables').DataTable({
			    });	
			 });

		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}

function cargarDataTable_empresa(idUsuario){
    $.ajax({
		url : "<?php echo site_url('Sg_usuarioEmpresa/ajax_datatable_empresa')?>/"+idUsuario,
		type: "GET",                                                  
		dataType: "JSON",      
		success: function(data){
			 document.getElementById('bodyTabla').innerHTML = data.registro;
			 $(document).ready(function() {
			 	document.getElementById('idUsuario').value = data.idUsuario;
			 	document.getElementById('nobUsuario').value = data.nobUsuario;
			 	document.getElementById('nombreUsuario').innerHTML = "Usuario: " + data.nobUsuario;


				$('#basic-datatables').DataTable({
			    });	
			 });

		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});

}



function cargarDataTable_moduloEmpresa(idUsuarioEmpresa){

    $.ajax({
		url : "<?php echo site_url('Sg_usuarioEmpresa/ajax_datatable_moduloEmpresa')?>/"+idUsuarioEmpresa,
		type: "GET",                                                  
		dataType: "JSON",      
		success: function(data){


			 document.getElementById('bodyTabla').innerHTML = data.registro;
			 //$(document).ready(function() {
			 	document.getElementById('idUsuarioEmpresa').value = data.idUsuarioEmpresa;
			 	document.getElementById('nobUsuario').value = data.nobUsuario;
			 	document.getElementById('nombreUsuario').innerHTML = "Empresa / Usuario: " + data.nobUsuario;
                document.getElementById('botonRegresar').innerHTML = data.boton;
				$('#basic-datatables').DataTable({
			    });	
			 //});

		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});

}





function add_form(){
    //configura el modal donde se ingresar la empresa y el rol a asociar al usuario


	$('#formulario')[0].reset(); // reset form on modals
	idUsuario = document.getElementById('idUsuario').value;
	nobUsuario = document.getElementById('nobUsuario').value;
    document.getElementById('accion_formulario').value="1";
    document.getElementById('usuarioNombre').innerHTML =nobUsuario;
    document.getElementById('titulo_mostrar').innerHTML = "Asignar Empresa al Usuario";
    document.getElementById('idUsuario_add').value = idUsuario;
    document.getElementById('lab_empresa').style.display = "none";
    document.getElementById('lab_rol').style.display = "none";
    document.getElementById('lab_sucursal').style.display = "none";
	document.getElementById('guardar01').disabled=false;
	document.getElementById('cancelar01').disabled=false;

    //combo o lista donde mustra las empresas que no han sido asociado al usuario
	ruta = "<?php echo site_url('Combo_ctrl/ajax_combo_empresaUsuario')?>";
	cbo_empresaUsuario(ruta,idUsuario);   //Llenar combo    
     
    //combo o lista donde mustra las empresas que no han sido asociado al usuario
	ruta = "<?php echo site_url('Combo_ctrl/ajax_combo_sucursalUsuario')?>";
	cbo_sucursalUsuario(ruta,0,0,idUsuario);   //Llenar combo    



    //Combo que mustra los diferentes roles de un usuario.
	ruta = "<?php echo site_url('Combo_ctrl/ajax_combo_rol')?>";
	cbo_rol(ruta,0);   //Llenar combo    

    $('#mod_modal').modal('show');
}


function cbo_sel_sucursal(){
	idEmpresa = document.getElementById('id_cbo_empresas').value;
	idUsuario = document.getElementById('idUsuario_add').value;
	ruta = "<?php echo site_url('Combo_ctrl/ajax_combo_sucursalUsuario')?>";
    cbo_sucursalUsuario(ruta,0,idEmpresa, idUsuario);
}


function actualizar_form(accion){
    //Comienza el proceso de almacenar los datos segun la actualizacion
    //Incluir o modificar
    if(accion=='1'){
        guardar_add_form();
    }else{
        guardar_mod_form();
    } 
}


function guardar_add_form(){
	formulario = "formulario";
	error=0;
	error = validar_form(formulario,1);

    if(error == 0){ 	
		document.getElementById('guardar01').disabled=true;
		document.getElementById('cancelar01').disabled=true;
	}	
	
	if(error == 0){
        $.ajax({
            url : "<?=base_url();?>Sg_usuarioEmpresa/ajax_guardar_add",
            type: "POST",
            data: $('#formulario').serialize(),
            dataType: "JSON",
            success: function(data)
            {
            	if(data.status==0){ 
					 document.getElementById('bodyTabla').innerHTML = data.registro;
					 $('#mod_modal').modal('hide');

					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
				 	});            	


					swal("Registro almacenado Correctamente!", "", {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					});
				}else{
					document.getElementById('guardar01').disabled=false;
					document.getElementById('cancelar01').disabled=false;					
					swal("No se actualizo el registro, ya existe. El usuario ya posee esta empresa con el mismo Rol de usuario", "", {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});

				}	
	        },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error Insertando / modificanco registro, Comuniquese con el administrador');
            }
        });
    }else{
		swal("No se actualizo el registro", "", {
			icon : "error",
			buttons: {        			
				confirm: {
					className : 'btn btn-danger'
				}
			},
		});
    }    
} //Fin de add form	



function validar_form(formulario,accion){
//accion 1= incluir
//accion 2= modificar

error=0;
objeto = "id_cbo_empresas";
campo = document.forms[formulario].elements[objeto].value;
document.getElementById('lab_empresa').style.display = "none";
if (campo==0){
	document.getElementById('lab_empresa').style.display = "inline";
   	document.getElementById('lab_empresa').innerHTML = "Seleccione la Empresa asociada"; 	
   	error = 1;
}


error=0;
objeto = "id_cbo_sucursal";
campo = document.forms[formulario].elements[objeto].value;
document.getElementById('lab_sucursal').style.display = "none";
if (campo==0){
	document.getElementById('lab_sucursal').style.display = "inline";
   	document.getElementById('lab_sucursal').innerHTML = "Seleccione la Sucursal"; 	
   	error = 1;
}


error=0;
objeto = "id_cbo_rol";
campo = document.forms[formulario].elements[objeto].value;
document.getElementById('lab_rol').style.display = "none";
if (campo==0){
	document.getElementById('lab_rol').style.display = "inline";
   	document.getElementById('lab_rol').innerHTML = "Seleccione Rol del usuario"; 	
   	error = 1;
}

return error;
}



function eliminar_form(id){
	swal({
		title: 'Esta seguro de eliminar el Usuario de la Empresa seleccionada?',
			type: 'warning',
			buttons:{
				cancel: {
					visible: true,
						text : 'No, Cancelar!',
								className: 'btn btn-danger'
				},        			
				confirm: {
					text : 'Si, Eliminar!',
					className : 'btn btn-success'
				}
			}
	}).then((willDelete) => {
		if (willDelete) {

			idUsuario = document.getElementById('idUsuario').value;
			id = id + "a" + idUsuario;
		    $.ajax({
				url : "<?php echo site_url('Sg_usuarioEmpresa/ajax_desactivar')?>/"+id,
				type: "GET",                     
				dataType: "JSON",
				success: function(data){
					 document.getElementById('bodyTabla').innerHTML = data.registro;
					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
					 }); 
				},
				error: function (jqXHR, textStatus, errorThrown){
					alert('Error consultando la Base de Datos');
				}
			});
			swal("El registro fué Eliminado!", {
				icon: "success",
				buttons : {
					confirm : {
						className: 'btn btn-success'
					}
				}
			});
		}
	});

}





function eliminarModulo_form(id){
	swal({
		title: 'Esta seguro de eliminar el modulo asociado al usuario seleccionado?',
			type: 'warning',
			buttons:{
				cancel: {
					visible: true,
						text : 'No, Cancelar!',
								className: 'btn btn-danger'
				},        			
				confirm: {
					text : 'Si, Eliminar!',
					className : 'btn btn-success'
				}
			}
	}).then((willDelete) => {
		if (willDelete) {                                
			idUsuarioEmpresa = document.getElementById('idUsuarioEmpresa').value;

			id = id + "a" + idUsuarioEmpresa;
		    $.ajax({
				url : "<?php echo site_url('Sg_usuarioEmpresa/ajax_desactivar_modulo')?>/"+id,
				type: "GET",                     
				dataType: "JSON",
				success: function(data){
					 document.getElementById('bodyTabla').innerHTML = data.registro;
					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
					 }); 
				},
				error: function (jqXHR, textStatus, errorThrown){
					alert('Error consultando la Base de Datos');
				}
			});
			swal("El registro fué Eliminado!", {
				icon: "success",
				buttons : {
					confirm : {
						className: 'btn btn-success'
					}
				}
			});
		}
	});

}



function addModulo_form(){

   //configura el modal donde se ingresar la empresa y el rol a asociar al usuario




	$('#formulario')[0].reset(); // reset form on modals
	idUsuarioEmpresa = document.getElementById('idUsuarioEmpresa').value;
	nobUsuario = document.getElementById('nobUsuario').value;
    document.getElementById('accion_formulario').value="1";
    document.getElementById('ModuloUsuario').innerHTML =nobUsuario;
    document.getElementById('titulo_mostrar').innerHTML = "Asignar Modulo al Usuario";
    document.getElementById('idUsuario_add').value = idUsuarioEmpresa;
    

    
    document.getElementById('lab_moduloUsuario').style.display = "none";


	document.getElementById('guardar02').disabled=false;
	document.getElementById('cerrar02').disabled=false; 		
	



    //combo o lista donde mustra las empresas que no han sido asociado al usuario
	ruta = "<?php echo site_url('Combo_ctrl/ajax_combo_usuarioEmpresa')?>";
	cbo_moduloUsuario(ruta,idUsuarioEmpresa);   //Llenar combo    
	$('#mod_modal').modal('show');


}



function actualizarModulo_form(accion){
    //Comienza el proceso de almacenar los datos segun la actualizacion
    //Incluir o modificar
    guardar_addModulo_form();
}


function guardar_addModulo_form(){
	formulario = "formulario";
	error=0;
 
 
	error = validarModulo_form(formulario,1);
	if(error==0){
    	document.getElementById('guardar02').disabled=true;
		document.getElementById('cerrar02').disabled=true; 		
	}

	

 
	if(error == 0){
        $.ajax({
            url : "<?=base_url();?>Sg_usuarioEmpresa/ajax_guardarModulo_add",
            type: "POST",
            data: $('#formulario').serialize(),
            dataType: "JSON",
            success: function(data)
            {
            	if(data.status==0){ 
					 document.getElementById('bodyTabla').innerHTML = data.registro;
					 $('#mod_modal').modal('hide');

					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
				 	});            	


					swal("Registro almacenado Correctamente!", "", {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					});
				}else{
					document.getElementById('guardar02').disabled=false;
					document.getElementById('cerrar02').disabled=false; 
					swal("No se actualizo el registro, ya existe. El usuario ya posee esta empresa con el mismo Rol de usuario", "", {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});

				}	
	        },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error Insertando / modificanco registro, Comuniquese con el administrador');
            }
        });
    }else{
		swal("No se actualizo el registro", "", {
			icon : "error",
			buttons: {        			
				confirm: {
					className : 'btn btn-danger'
				}
			},
		});
    }    
} //Fin de add form	




function validarModulo_form(formulario,accion){
//accion 1= incluir
//accion 2= modificar

error=0;
objeto = "id_cbo_moduloUsuario";
campo = document.forms[formulario].elements[objeto].value;
document.getElementById('lab_moduloUsuario').style.display = "none";
if (campo==0){
	document.getElementById('lab_moduloUsuario').style.display = "inline";
   	document.getElementById('lab_moduloUsuario').innerHTML = "Seleccione el Modulo"; 	
   	error = 1;
}

return error;
}



function modificarModulo_form(idUEM){
    document.getElementById('guardar01').disabled=false;
    document.getElementById('cerrar01').disabled=false; 		
  	document.getElementById('idUEM').value=idUEM;
    document.getElementById('titulo_mostrar').innerHTML = "Modificar permiso del modulos asignado al Usuario";
    $.ajax({
		url : "<?php echo site_url('Sg_usuarioEmpresa/ajax_modificarPermiso')?>/"+idUEM,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			document.getElementById('idModulo').value=data.idModulo;
			document.getElementById('rolNombrePermiso').innerHTML = data.nombreModulo;
	 	    document.getElementById('permisologia').innerHTML = data.registro;
	 	    document.getElementById('nroPermiso').value = data.nroPermiso;
	 	    document.getElementById('arregloPermiso').value = data.arregloPermiso;
            $('#permisologia_modal').modal('show');
		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}	
	
	
function actualizarPermiso_form(){

  ///validar campos formulario
	formulario = "formularioPermiso";
	error=0;
	error = validarPermisologia_form(formulario,2);
	if(error==0){
    	document.getElementById('guardar01').disabled=true;
		document.getElementById('cerrar01').disabled=true; 		
	}
	
	if(error == 0){
        $.ajax({
            url : "<?=base_url();?>Sg_usuarioEmpresa/ajax_guardarPermisologia_mod",
            type: "POST",
            data: $('#formularioPermiso').serialize(),
            dataType: "JSON",
            success: function(data)
            {
            	if(data.status==0){ 
				
					 document.getElementById('bodyTabla').innerHTML = data.registro;
					 $('#permisologia_modal').modal('hide');

					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
				 	});            	


					swal("Registro almacenado Correctamente!", "", {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					});
				}else{
					document.getElementById('guardar01').disabled=false;
					document.getElementById('cerrar01').disabled=false; 		
					
					swal("No se actualizo el registro", "", {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});

				}	
	        },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error Insertando / modificanco registro, Comuniquese con el administrador');
            }
        });
    }else{
		swal("No se actualizo el registro", "", {
			icon : "error",
			buttons: {        			
				confirm: {
					className : 'btn btn-danger'
				}
			},
		});
    } 

		
		
		
}
	

function validarPermisologia_form(formulario,accion){
error=0;

label = "lab_permiso"; 
checkbox = "permiso";
nroPermiso = document.forms[formulario].elements['nroPermiso'].value;   



swPermiso=0;
document.getElementById(label).style.display = "none";


i = 0;
while(i < nroPermiso && swPermiso==0){
    i+=1;
    objeto = checkbox+i.toString();
    var isChecked = document.getElementById(objeto).checked;
    if(isChecked){
        swPermiso=1;
    }    
} 


if (swPermiso==0){
	document.getElementById(label).style.display = "inline";
   	document.getElementById(label).innerHTML = "Seleccione al menos un permiso"; 	
   	error = 1;
}


return error;
}






/////////////////////////////////////////////////////
//////////////Procesos asociados ////////////////////
/////////////////////////////////////////////////////

function cargarDataTable_empresaProceso(id){
    $.ajax({
		url : "<?php echo site_url('Sg_usuarioEmpresa/ajax_datatable_proceso')?>/"+id,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			 document.getElementById('bodyTabla').innerHTML = data.registro;

			 $(document).ready(function() {
			 	//document.getElementById('id_rol_modulo_principal').value = data.idRolModulo;
                
			 	document.getElementById('nob_modulo_principal').innerHTML = data.nobModulo;
			 	document.getElementById('id_usuario_empresa_principal').value = data.idUsuarioEmpresa;
				$('#basic-datatables').DataTable({
			    });	
			 });

		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}



function eliminarProceso_form(id){
	swal({
		title: 'Esta seguro de eliminar el Sub modulo asociado?',
			type: 'warning',
			buttons:{
				cancel: {
					visible: true,
						text : 'No, Cancelar!',
								className: 'btn btn-danger'
				},        			
				confirm: {
					text : 'Si, Desactivar!',
					className : 'btn btn-success'
				}
			}
	}).then((willDelete) => {
		if (willDelete) {
		    $.ajax({
				url : "<?php echo site_url('Sg_usuarioEmpresa/ajax_desactivarProceso')?>/"+id,
				type: "GET",                     
				dataType: "JSON",
				success: function(data){
					 document.getElementById('bodyTabla').innerHTML = data.registro;
					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
					 }); 
				},
				error: function (jqXHR, textStatus, errorThrown){
					alert('Error consultando la Base de Datos');
				}
			});
			swal("El registro fué eliminado satisfactoriamente!", {
				icon: "success",
				buttons : {
					confirm : {
						className: 'btn btn-success'
					}
				}
			});
		}
	});

}



function asociar_form(id){
	swal({
		title: 'Esta seguro de asociar el Sub modulo?',
			type: 'warning',
			buttons:{
				cancel: {
					visible: true,
						text : 'No, Cancelar!',
								className: 'btn btn-danger'
				},        			
				confirm: {
					text : 'Si, Asociar!',
					className : 'btn btn-success'
				}
			}
	}).then((willDelete) => {
		if (willDelete) {
		    $.ajax({
				url : "<?php echo site_url('Sg_usuarioEmpresa/ajax_addProceso')?>/"+id,
				type: "GET",                     
				dataType: "JSON",
				success: function(data){
					 document.getElementById('bodyTabla').innerHTML = data.registro;
					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
					 }); 
				},
				error: function (jqXHR, textStatus, errorThrown){
					alert('Error consultando la Base de Datos');
				}
			});
			swal("El registro fué asociado satisfactoriamente!", {
				icon: "success",
				buttons : {
					confirm : {
						className: 'btn btn-success'
					}
				}
			});
		}
	});

}

function regresarEmpresaModulo(){
	
	document.location.href = "<?php echo base_url('usuarioEmpresa_AM')?>";
	
/*	
        $.ajax({
		url : "<?php echo site_url('Sg_usuarioEmpresa/ajax_actualizarUsuarioEmpresa')?>",
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			document.location.href = "<?php echo base_url('usuarioEmpresaActualizar')?>";

		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});	
*/	
	
	
}



function usuarioEmpresaAct(idUsuario){
        $.ajax({
		url : "<?php echo site_url('Sg_usuarioEmpresa/ajax_actualizarEmpresa')?>/"+idUsuario,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			document.location.href = "<?php echo base_url('usuarioEmpresaActualizar')?>"; 

		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}


function usuarioEmpresaAM(idUsuarioEmpresa){
        $.ajax({
		url : "<?php echo site_url('Sg_usuarioEmpresa/ajax_actualizarUsuarioEmpresaAM')?>/"+idUsuarioEmpresa,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			document.location.href = "<?php echo base_url('usuarioEmpresa_AM')?>"; 

		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}

function regresarUsuarioEmpresa(){
        $.ajax({
		url : "<?php echo site_url('Sg_usuarioEmpresa/ajax_actualizarUsuarioEmpresa')?>",
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			document.location.href = "<?php echo base_url('usuarioEmpresaActualizar')?>";

		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}



function usuarioEmpresaAM(idUsuarioEmpresa){
        $.ajax({
		url : "<?php echo site_url('Sg_usuarioEmpresa/ajax_actualizarUsuarioEmpresaAM')?>/"+idUsuarioEmpresa,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			document.location.href = "<?php echo base_url('usuarioEmpresa_AM')?>"; 

		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}



function usuarioModuloProceso(idUsuarioEmpresaModulo){
        $.ajax({
		url : "<?php echo site_url('Sg_usuarioEmpresa/ajax_actualizarUsuarioEmpresaProceso')?>/"+idUsuarioEmpresaModulo,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			document.location.href = "<?php echo base_url('usuarioModuloProceso')?>"; 
		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}




</script>
