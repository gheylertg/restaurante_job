<?php
$incluir = base_url() . "assets/images/nuevo.png";
$asterisco = ' style="color=red; font-size:16px"';
$ci = &get_instance();
$ci->load->library('Clssession');
$accion= $ci->clssession->accion(3,2); 

$ruta = base_url() . "Sg_usuario/ajax_guardar_add";




?>

        <br>            
        <div class="main-panel">
            <div class="content">
                <div class="page-inner mt--5">
                    <div class="row row-card-no-pd mt--2">
                        <div class="col-sm-12 col-md-12">
                            <div class="card card-stats card-round">
                                <div class="card-body ">
									 <h2><span style="color:blue">Usuarios del Sistema</span></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                
                <?php
                    if($accion==1){
                ?>

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header" style="text-align:right">
							<a href="javascript:void(0)" onclick="javascript:add_form()">
							   <img src="<?=$incluir;?>" style="width:35px; height:35px" alt="Incluir">
							</a>
                        </div>
                    </div>
                </div>    
                <?php
                    }
                ?>


               

                <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="table-responsive">
										<div id="bodyTabla">
										</div>
										
										
                                    </div>
                                </div>
                            </div>
                        </div>    
                </div>
            </div>
    



<!-- Modal -->
<div class="modal fade" id="mod_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content" style="width:900px; margin-left:-200px">
			<div class="modal-header" style="background-color: #00f">
				<h4 class="modal-title" id="exampleModalLabel" style="color:white">
					<div id="titulo_mostrar"></div>
				</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				
			<form name="formulario" id="formulario" autocomplete="off" 
			      enctype="multipart/form-data">	
				
				
			  <div class="container">
				<div class="row">
					<div class="col-md-6">
                         <label ><span style="color:#0000ff; font-size:16px; font-weight:600">
							     Login <span style="color:#ff0000;">*</span></span>
						 </label>
                        <input type="text" name="m_login" id="m_login" class="form-control" 
                               placeholder="Edite el login" maxlength="100" minlength="0"                                >
                        <div id="lab_login" style="color:red; margin-left:9px; display:none"></div>
					</div>
				</div>	



                
				<div class="row">
					<div class="col-md-6" style="padding-top: 10px">
                         <label><span style="color:#0000ff; font-size:16px; font-weight:600">
							 Nombre(s) <span style="color:#ff0000;">*</span></span>
					     </label>
                        <input type="text" name="m_nombre" id="m_nombre" class="form-control" 
                               placeholder="Edite el nombre" maxlength="100" minlength="0">
                        <div id="lab_nombre" style="color:red; margin-left:9px; display:none"></div>
					</div>

					<div class="col-md-6" style="padding-top: 10px">
                         <label><span style="color:#0000ff; font-size:16px; font-weight:600">
							Apellido(s) <span style="color:#ff0000;">*</span></span>
					     </label>
                        <input type="text" name="m_apellido" id="m_apellido" class="form-control" 
                               placeholder="Edite el apellido" maxlength="100" minlength="0">
                        <div id="lab_apellido" style="color:red; margin-left:9px; display:none"></div>
					</div>
				</div>	


				<div class="row">
					<div class="col-md-6" style="padding-top: 10px">
                         <label>
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							 Contraseña <span style="color:#ff0000;">*</span></span>
					     </label>

                        <input type="password" name="m_password" id="m_password" class="form-control" 
                               placeholder="Edite el password" maxlength="100" minlength="0">
                        <div id="lab_password" style="color:red; margin-left:9px; display:none"></div>
					</div>

					<div class="col-md-6" style="padding-top: 10px">
                         <label>
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							 Repetir Contraseña <span style="color:#ff0000;">*</span></span>
					     </label>

                        <input type="password" name="m_password_rep" id="m_password_rep" 
                               class="form-control"
                               placeholder="Repita el password" maxlength="100" minlength="0">
                        <div id="lab_password_rep" style="color:red; margin-left:9px; display:none"></div>
                    </div>    
				</div>	
				<div class="row">
					<div class="col-md-12">
                        <label><span  style="color: #00f" >
                            La contraseña debe contener al menos seis(6) caracteres</span>                  
                        </label>                                     

					</div>
				</div>		

				<div class="row">
					<div class="col-md-12" style="padding-top: 10px">
						 <hr> 
                         <label>
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							 Correo electrónico</span>
					     </label>

                        <input type="text" name="m_correo" id="m_correo" class="form-control" 
                               placeholder="Edite el Correo Electrónico" maxlength="200" minlength="0">
                        <div id="lab_correo" style="color:red; margin-left:9px; display:none"></div>       
					</div>
				</div>	


				<div class="row">
					<div class="col-md-4" style="padding-top: 10px">
                         <label>
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							 Sexo <span style="color:#ff0000;">*</span></span>
					     </label>
						<div id="cbo_sexo"></div>
						<div id="lab_sexo" style="color:red; margin-left:9px; display:none"></div>
					</div>

					<div class="col-md-9" style="padding-top: 10px">
                         <label>
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							 Foto Usuario</span>
					     </label>

                        <input type="file" name="mfoto"
                               class="form-control"/>
					</div>
				    


				</div>	

                <br>  
				<span style="color:#ff0000; font-size:16px; text-align:left">Campo obligatorio (*)</span>
			  </div>				
			</div>
			<div class="modal-footer">

                   
				<button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrar01">Cerrar</button>


		        <button  class="btn btn-primary"  id="guardar01" onclick="actualizar_form()">
		              Guardar
		        </button>

			</div>


            <input type="hidden" id="id_mod" name="id_mod">
            <input type="hidden" id="login_orig" name="login_orig">
            <input type="hidden" id="val_error" name="val_error" value="0">
            <input type="hidden" id="accion_formulario" name="accion_formulario" value="0">

			<input type="hidden" id="ruta" name="ruta" value="<?=$ruta;?>">


			</form>  

		</div>
	</div>
</div>






<!-- Ver usuarios  -->
<!-- Modal -->
<div class="modal fade" id="ver_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content" style="width:900px; margin-left:-200px">
			<div class="modal-header" style="background-color: #00f">
				<h4 class="modal-title" id="exampleModalLabel" style="color:white">
					<div id="c_titulo_mostrar"></div>
				</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				

			  <div class="container">
				<div class="row">
					<div class="col-md-6">
                         <label ><span style="color:#0000ff; font-size:16px; font-weight:600">
							     Login <span style="color:#ff0000;">*</span></span>
						 </label>
                        <input type="text" name="c_login" id="c_login" class="form-control"
                               readonly="true">
					</div>
				</div>	
                
				<div class="row">
					<div class="col-md-6" style="padding-top: 10px">
                         <label><span style="color:#0000ff; font-size:16px; font-weight:600">
							 Nombre(s) <span style="color:#ff0000;">*</span></span>
					     </label>
                        <input type="text" name="c_nombre" id="c_nombre" class="form-control" 
                               readonly="true">
					</div>

					<div class="col-md-6" style="padding-top: 10px">
                         <label><span style="color:#0000ff; font-size:16px; font-weight:600">
							Apellido(s) <span style="color:#ff0000;">*</span></span>
					     </label>
                        <input type="text" name="c_apellido" id="c_apellido" class="form-control" 
                               readonly="true">
					</div>
				</div>	


				<div class="row">
					<div class="col-md-12" style="padding-top: 10px">
						 <hr> 
                         <label>
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							 Correo electrónico</span>
					     </label>

                        <input type="text" name="c_correo" id="c_correo" class="form-control" 
                               readonly="true">
      
					</div>
				</div>	


				<div class="row">
					<div class="col-md-4" style="padding-top: 10px">
                         <label>
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							 Sexo <span style="color:#ff0000;">*</span></span>
					     </label>
                        <input type="text" name="c_sexo" id="c_sexo" class="form-control" 
                               readonly="true">
					</div>

					<!--<div class="col-md-9" style="padding-top: 10px">
                         <label>
							 <span style="color:#0000ff; font-size:16px; font-weight:600">
							 Foto Usuario</span>
					     </label>

                        <input type="file" id="m_foto" id="m_foto" name="userfile"
                               class="form-control"/>
					</div>
				    -->


				</div>	

                <br>  
			  </div>				
			</div>
			<div class="modal-footer">

                   
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>


			</div>

		</div>
	</div>
</div>
