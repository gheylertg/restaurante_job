


<!--cargar datatable-->
<script>
    $(document).ready(function(){
		 cargarDataTable(0);
	});





function cargarDataTable(id){
    $.ajax({
		url : "<?php echo site_url('Sg_usuario/ajax_datatable')?>/"+id,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			 document.getElementById('bodyTabla').innerHTML = data.registro;

			 $(document).ready(function() {
				$('#basic-datatables').DataTable({
			    });	
			 });

		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}


//cargar combos

function ver_form(id){
    document.getElementById('c_titulo_mostrar').innerHTML = "Información del usuario del Sistema";

    $.ajax({
		url : "<?php echo site_url('Sg_usuario/ajax_modificar')?>/"+id,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){

			 document.getElementById('c_login').value = data.login;
			 document.getElementById('c_nombre').value = data.nombre;
			 document.getElementById('c_apellido').value = data.apellido;
             document.getElementById('c_correo').value = data.email;
             sexo = "Femenino";
             if(data.sexo=='M'){
                 sexo = "Masculino";
             }
             document.getElementById('c_sexo').value = sexo;

             $('#ver_modal').modal('show');
		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});

}





function add_form(){
	$('#formulario')[0].reset(); // reset form on modals

   	document.getElementById('guardar01').disabled=false;
	document.getElementById('cerrar01').disabled=false; 		



    document.getElementById('accion_formulario').value="1";
    document.getElementById('titulo_mostrar').innerHTML = "Incluir Usuario de Usuario";
	ruta = "<?php echo site_url('Combo_ctrl/ajax_combo_sexo')?>";
	cbo_sexo(ruta,0);   //Llenar combo    
    document.getElementById('login_orig').value = "";
    $('#mod_modal').modal('show');


}


function modificar_form(id){

    //cbo_modelo(ruta,data.id_proyecto_local,data.id_proyecto_local);   //Llenar combo    

    document.getElementById('accion_formulario').value="2";
    document.getElementById('titulo_mostrar').innerHTML = "Modificar Usuario del Sistema";

   	document.getElementById('guardar01').disabled=false;
	document.getElementById('cerrar01').disabled=false; 		




    //document.getElementById('url_mostrar').style.display = "none";

    $.ajax({
		url : "<?php echo site_url('Sg_usuario/ajax_modificar')?>/"+id,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){

			 document.getElementById('m_login').value = data.login;
			 document.getElementById('m_nombre').value = data.nombre;
			 document.getElementById('m_apellido').value = data.apellido;
             document.getElementById('m_password').value = data.password;

             document.getElementById('m_password_rep').value = data.password;
             document.getElementById('m_correo').value = data.email;
   		     document.getElementById('id_mod').value = data.id;

 			 document.getElementById('val_error').value = "0";
			 document.getElementById('lab_nombre').style.display = "none";
			 document.getElementById('lab_login').style.display = "none";
			 document.getElementById('lab_apellido').style.display = "none";


			 document.getElementById('lab_sexo').style.display = "none";
			 document.getElementById('lab_password').style.display = "none";
			 document.getElementById('lab_password_rep').style.display = "none";
			 document.getElementById('login_orig').value = data.login;

			 ruta = "<?php echo site_url('Combo_ctrl/ajax_combo_sexo')?>";
	         cbo_sexo(ruta,data.sexo);   //Llenar combo    
             $('#mod_modal').modal('show');
		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error consultando la Base de Datos');
		}
	});
}



function actualizar_form(){
    accion = document.getElementById('accion_formulario').value;
    if(accion==1){
        guardar_add_form();
    }else{
        guardar_mod_form();
    } 


}


function guardar_add_form(){
   //validar campos formulario
	formulario = "formulario";
	error=0;
	error = validar_form(formulario,1);

	if(error==0){
    	document.getElementById('guardar01').disabled=true;
		document.getElementById('cerrar01').disabled=true; 		
	}

    alert("wqq");
	if(error == 0){
		alert("222222222222222222");
        $.ajax({
            url : "<?=base_url();?>Sg_usuario/ajax_guardar_add",
            type: "POST",
            data: $('#formulario').serialize(),
            dataType: "JSON",
            success: function(data)
            {
            	if(data.status==0){ 
					 document.getElementById('bodyTabla').innerHTML = data.registro;
					 $('#mod_modal').modal('hide');

					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
				 	});            	


					swal("Registro almacenado Correctamente!", "", {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					});
				}else{
					document.getElementById('lab_login').style.display = "inline";
					document.getElementById('lab_login').innerHTML = "El Login ingresado, ya se encuentra registrado.";
			    	document.getElementById('guardar01').disabled=false;
					document.getElementById('cerrar01').disabled=false; 					
					swal("No se actualizo el registro", "", {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});

				}	
	        },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error Insertando / modificanco registro, Comuniquese con el administrador');
            }
        });
    }else{
		swal("No se actualizo el registro", "", {
			icon : "error",
			buttons: {        			
				confirm: {
					className : 'btn btn-danger'
				}
			},
		});
    }    



}	
	    






function guardar_mod_form(){

	//validar campos formulario
	formulario = "formulario";
	error=0;
	error = validar_form(formulario,2);

	if(error==0){
    	document.getElementById('guardar01').disabled=true;
		document.getElementById('cerrar01').disabled=true; 		
	}


	if(error == 0){
        $.ajax({
            url : "<?=base_url();?>Sg_usuario/ajax_guardar_mod",
            type: "POST",
            data: $('#formulario').serialize(),
            dataType: "JSON",
            success: function(data)
            {
            	if(data.status==0){ 
					 document.getElementById('bodyTabla').innerHTML = data.registro;
					 $('#mod_modal').modal('hide');

					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
				 	});            	


					swal("Registro almacenado Correctamente!", "", {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					});
				}else{
					document.getElementById('lab_nombre').style.display = "inline";
					document.getElementById('lab_nombre').innerHTML = "El login ingresado, ya se encuentra registrado.";
			    	document.getElementById('guardar01').disabled=false;
					document.getElementById('cerrar01').disabled=false; 					
					swal("No se actualizo el registro", "", {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});

				}	
	        },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error Insertando / modificanco registro, Comuniquese con el administrador');
            }
        });
    }else{
		swal("No se actualizo el registro", "", {
			icon : "error",
			buttons: {        			
				confirm: {
					className : 'btn btn-danger'
				}
			},
		});
    }    
}







function eliminar_form(id){
	swal({
		title: 'Esta seguro de ?',
			type: 'warning',
			buttons:{
				cancel: {
					visible: true,
						text : 'No, Cancelar!',
								className: 'btn btn-danger'
				},        			
				confirm: {
					text : 'Si, Desactivar!',
					className : 'btn btn-success'
				}
			}
	}).then((willDelete) => {
		if (willDelete) {
		    $.ajax({
				url : "<?php echo site_url('Sg_usuario/ajax_desactivar')?>/"+id,
				type: "GET",                     
				dataType: "JSON",
				success: function(data){
					 document.getElementById('bodyTabla').innerHTML = data.registro;
					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
					 }); 
				},
				error: function (jqXHR, textStatus, errorThrown){
					alert('Error consultando la Base de Datos');
				}
			});
			swal("El registro fué desactivado!", {
				icon: "success",
				buttons : {
					confirm : {
						className: 'btn btn-success'
					}
				}
			});
		}
	});

}

function reactivar_form(id){
	swal({
		title: 'Esta seguro de ?',
			type: 'warning',
			buttons:{
				cancel: {
					visible: true,
						text : 'No, Cancelar!',
								className: 'btn btn-danger'
				},        			
				confirm: {
					text : 'Si, Reactivar!',
					className : 'btn btn-success'
				}
			}
	}).then((willDelete) => {
		if (willDelete) {
		    $.ajax({
				url : "<?php echo site_url('Sg_usuario/ajax_reactivar')?>/"+id,
				type: "GET",                     
				dataType: "JSON",
				success: function(data){
					 document.getElementById('bodyTabla').innerHTML = data.registro;
					 $(document).ready(function() {
						$('#basic-datatables').DataTable({
					    });	
					 }); 
				},
				error: function (jqXHR, textStatus, errorThrown){
					alert('Error consultando la Base de Datos');
				}
			});
			swal("El registro fué reactivado!", {
				icon: "success",
				buttons : {
					confirm : {
						className: 'btn btn-success'
					}
				}
			});
		}
	});
}







function validar_form(formulario,accion){
//accion 1= incluir
//accion 2= modificar

error=0;
objeto = "m_login";
campo = document.forms[formulario].elements[objeto].value;
campo = campo.replace(/(^\s*)|(\s*$)/g,"");

document.getElementById('lab_login').style.display = "none";
if (campo.length==0){
	document.getElementById('lab_login').style.display = "inline";
    document.getElementById('lab_login').innerHTML = "Edite el login del usuario"; 	
    error = 1;
}




objeto = "m_nombre";
campo = document.forms[formulario].elements[objeto].value;
campo = campo.replace(/(^\s*)|(\s*$)/g,"");
document.getElementById('lab_nombre').style.display = "none";
if (campo.length==0){
	document.getElementById('lab_nombre').style.display = "inline";
    document.getElementById('lab_nombre').innerHTML = "Edite el/los nombre(s) del usuario"; 	
    error = 1;
}

objeto = "m_apellido";
campo = document.forms[formulario].elements[objeto].value;
campo = campo.replace(/(^\s*)|(\s*$)/g,"");
document.getElementById('lab_apellido').style.display = "none";
if (campo.length==0){
	document.getElementById('lab_apellido').style.display = "inline";
    document.getElementById('lab_apellido').innerHTML = "Edite el/los apellido(s) del usuario"; 	
    error = 1;
}

objeto = "m_correo";
campo = document.forms[formulario].elements[objeto].value;
document.getElementById('lab_correo').style.display = "none";
if(campo.length!=0){
    if (/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(campo)){
	    x=0;
	}else{
		document.getElementById('lab_correo').style.display = "inline";
        document.getElementById('lab_correo').innerHTML = "El Formato del Correo Electronico es incorrecto";
        error = 1;
	}

}





objeto = "id_cbo_sexo";
campo = document.forms[formulario].elements[objeto].value;
document.getElementById('lab_sexo').style.display = "none";
if (campo==0){
	document.getElementById('lab_sexo').style.display = "inline";
   	document.getElementById('lab_sexo').innerHTML = "Seleccione el Sexo del Usurio"; 	
   	error = 1;
}

swClave=0;
objeto = "m_password";
campo = document.forms[formulario].elements[objeto].value;
campo = campo.replace(/(^\s*)|(\s*$)/g,"");
clave=campo.trim();
document.getElementById('lab_password').style.display = "none";
document.getElementById('lab_password_rep').style.display = "none";
if (campo.length==0){
	document.getElementById('lab_password').style.display = "inline";
   	document.getElementById('lab_password').innerHTML = "Edite la contraseña";
   	error = 1;
}else{
    if(campo.length < 6){
		document.getElementById('lab_password').style.display = "inline";
   		document.getElementById('lab_password').innerHTML = "La Contrasena debe tener una longuitud de seis(6) caracteres";
   		error = 1;
    }else{
        swClave = 0;
        objeto = "m_password_rep";
        campo = document.forms[formulario].elements[objeto].value;
        campo = campo.replace(/(^\s*)|(\s*$)/g,"");
        claveRep = campo.trim();
        if (campo.length==0){
			document.getElementById('lab_password_rep').style.display = "inline";
   			document.getElementById('lab_password_rep').innerHTML = "Indique Contrasena Repetida del Usuario";
   			error = 1;
            swClave = 1;
        }else{
            if(clave != claveRep){
				document.getElementById('lab_password').style.display = "inline";
   				document.getElementById('lab_password').innerHTML = "La contrasenas ingresadas son diferentes";
   				error = 1;
                swClave = 1;
            }
        }
    }  
}

return error;
}



</script>
