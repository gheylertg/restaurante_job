//Combo tabla Modulo del sistema
function cbo_modulo(ruta,id){
ruta = ruta + "/"+id; 
	$.ajax({
		url : ruta,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
            //$('.cbo_modelo').html(response).fadeIn();
            document.getElementById('cbo_modulo').innerHTML = data.combo;
        }
    });
}


//Combo tabla rol-Modulo del sistema
function cbo_rolModulo(ruta,id){
ruta = ruta + "/"+id; 
	$.ajax({
		url : ruta,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
            //$('.cbo_modelo').html(response).fadeIn();
            document.getElementById('cbo_rolModulo').innerHTML = data.combo;
        }
    });
}

//Combo tabla roles - tipo de usuarios
function cbo_rol(ruta,id){

ruta = ruta + "/"+id; 
	$.ajax({
		url : ruta,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
            //$('.cbo_modelo').html(response).fadeIn();
            document.getElementById('cbo_rol').innerHTML = data.combo;
        }
    });
}






//Combo tabla empresa - usuario
function cbo_empresaUsuario(ruta,id){
ruta = ruta + "/"+id; 
	$.ajax({
		url : ruta,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
            //$('.cbo_modelo').html(response).fadeIn();
            document.getElementById('cbo_empresa').innerHTML = data.combo;
        }
    });
}



//Combo tabla Sexo del usuario
function cbo_sexo(ruta,id){

ruta = ruta + "/"+id; 
	$.ajax({
		url : ruta,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
            //$('.cbo_modelo').html(response).fadeIn();
            document.getElementById('cbo_sexo').innerHTML = data.combo;
        }
    });
}


//Combo tabla Debito / credito
function cbo_debitoCredito(ruta,id){

ruta = ruta + "/"+id; 
	$.ajax({
		url : ruta,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
            //$('.cbo_modelo').html(response).fadeIn();
            document.getElementById('cbo_debitoCredito').innerHTML = data.combo;
        }
    });
}




//Combo tabla sg_usuario_empresa_mosulo vs sg_modulo del sistema
function cbo_moduloUsuario(ruta,id){
ruta = ruta + "/"+id; 
	$.ajax({
		url : ruta,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
            //$('.cbo_modelo').html(response).fadeIn();
            document.getElementById('cbo_moduloUsuario').innerHTML = data.combo;
        }
    });
}


//Combo tabla categoria de producto
function cbo_categoriaProducto(ruta,id){

ruta = ruta + "/"+id; 
	$.ajax({
		url : ruta,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
            //$('.cbo_modelo').html(response).fadeIn();
            document.getElementById('cbo_categoriaProducto').innerHTML = data.combo;
        }
    });
}


//Combo tabla unidad medida
function cbo_unidadMedida(ruta,id){
ruta = ruta + "/"+id; 
	$.ajax({
		url : ruta,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
            //$('.cbo_modelo').html(response).fadeIn();
            document.getElementById('cbo_unidadMedida').innerHTML = data.combo;
        }
    });
}


//Combo tabla unidad medida para presentacion alimento
function cbo_unidadMedida_presentacion(ruta,id){
ruta = ruta + "/"+id; 
	$.ajax({
		url : ruta,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
            //$('.cbo_modelo').html(response).fadeIn();
            document.getElementById('cbo_unidadMedida_presentacion').innerHTML = data.combo;
        }
    });
}



//Combo tabla empresa
function cbo_empresa(ruta,id){

ruta = ruta + "/"+id; 
	$.ajax({
		url : ruta,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
            //$('.cbo_modelo').html(response).fadeIn();
            document.getElementById('cbo_empresa').innerHTML = data.combo;
        }
    });
}


//Combo tabla region
function cbo_region(ruta,id){

ruta = ruta + "/"+id; 
	$.ajax({
		url : ruta,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
            //$('.cbo_modelo').html(response).fadeIn();
            document.getElementById('cbo_region').innerHTML = data.combo;
        }
    });
}


//Combo tabla Tipo operacion inventario
function cbo_tipoOperacion(ruta,id){
ruta = ruta + "/"+id; 
	$.ajax({
		url : ruta,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
            //$('.cbo_tipoOperacion').html(response).fadeIn();
            document.getElementById('cbo_tipoOperacion').innerHTML = data.combo;
        }
    });
}





//Combo tabla Proveedor inventario
function cbo_proveedorIn(ruta,id){
ruta = ruta + "/"+id; 
	$.ajax({
		url : ruta,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
            //$('.cbo_modelo').html(response).fadeIn();
            document.getElementById('cbo_proveedorIn').innerHTML = data.combo;
        }
    });
}


//Combo tabla Proveedor inventario
function cbo_conceptoIn(ruta,id, tipo){
ruta = ruta + "/"+id+"/"+tipo; 
	$.ajax({
		url : ruta,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
            //$('.cbo_modelo').html(response).fadeIn();
            document.getElementById('cbo_conceptoIn').innerHTML = data.combo;
        }
    });
}


//Combo tabla Proveedor inventario
function cbo_productoIn(ruta,id){
ruta = ruta + "/"+id; 
	$.ajax({
		url : ruta,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
            //$('.cbo_modelo').html(response).fadeIn();
            document.getElementById('cbo_productoIn').innerHTML = data.combo;
        }
    });
}


//Combo tabla Sexo del usuario
function cbo_tipoProducto(ruta,id){

ruta = ruta + "/"+id; 
	$.ajax({
		url : ruta,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
            //$('.cbo_modelo').html(response).fadeIn();
            document.getElementById('cbo_tipoProducto').innerHTML = data.combo;
        }
    });
}


//compra
function cbo_proveedorCp(ruta,id){
	ruta = ruta + "/"+id; 
	$.ajax({
		url : ruta,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
			//$('.cbo_modelo').html(response).fadeIn();
			document.getElementById('cbo_proveedorCp').innerHTML = data.combo;
		}
	});
}
			
			
			//Combo tabla Proveedor inventario
function cbo_productoCp(ruta,id){
ruta = ruta + "/"+id; 
	$.ajax({
		url : ruta,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
            //$('.cbo_modelo').html(response).fadeIn();
            document.getElementById('cbo_productoCp').innerHTML = data.combo;
        }
    });
}



//Combo Administracion Sala
function cbo_sala(ruta,id){
ruta = ruta + "/"+id; 
	$.ajax({
		url : ruta,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
            //$('.cbo_modelo').html(response).fadeIn();
            document.getElementById('cbo_sala').innerHTML = data.combo;
        }
    });
}

function cbo_anid_mesa(ruta,id, idPertenece){
ruta = ruta + "/"+idPertenece; 
	$.ajax({
		url : ruta,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
            document.getElementById('cbo_anid_mesa').innerHTML = data.combo;
        }
    });
}



//Combo Administracion mesonero
function cbo_mesonero(ruta,id){
ruta = ruta + "/"+id; 
	$.ajax({
		url : ruta,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
            document.getElementById('cbo_mesonero').innerHTML = data.combo;
            
        }
    });
}



function cbo_sucursalUsuario(ruta,id,idPertenece,idUsuario){
/* ****************************************************** */
/* ******** verificar para acomodar pase de datos ajax ** */
/* ****************************************************** */

id = id + "aa" + idPertenece + "aa" + idUsuario;	
ruta = ruta + "/"+id; 
	$.ajax({
		url : ruta,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
            //$('.cbo_modelo').html(response).fadeIn();
            document.getElementById('cbo_sucursal').innerHTML = data.combo;
        }
    });	
	
}


//Combo tabla Tipo operacion caja
function cbo_anid_tipoOperacion(ruta,id){
ruta = ruta + "/"+id; 
	$.ajax({
		url : ruta,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
            //$('.cbo_tipoOperacion').html(response).fadeIn();
            document.getElementById('cbo_anid_tipoOperacion').innerHTML = data.combo;
        }
    });
}

function cbo_anid_tipoMovimiento(ruta,id, idPertenece){
parametro = id + "aa" + idPertenece;
ruta = ruta + "/"+parametro; 
	$.ajax({
		url : ruta,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
            //$('.cbo_tipoOperacion').html(response).fadeIn();
            document.getElementById('cbo_anid_tipoMovimiento').innerHTML = data.combo;
        }
    });
}


//Combo Tipo Dinero
function cbo_tipoDinero(ruta,id){
ruta = ruta + "/"+id; 
	$.ajax({
		url : ruta,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
            //$('.cbo_modelo').html(response).fadeIn();
            document.getElementById('cbo_tipoDinero').innerHTML = data.combo;
        }
    });
}


//Combo sub modulo por modulo
function cbo_rolProceso(ruta,id){
ruta = ruta + "/"+id; 
	$.ajax({
		url : ruta,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
            //$('.cbo_modelo').html(response).fadeIn();
            document.getElementById('cbo_rolProceso').innerHTML = data.combo;
        }
    });
}


//Combo tabla Producto inventario
function cbo_productoInventario(ruta,id){
ruta = ruta + "/"+id; 
	$.ajax({
		url : ruta,
		type: "GET",                    
		dataType: "JSON",
		success: function(data){
            //$('.cbo_modelo').html(response).fadeIn();
            document.getElementById('cbo_productoInventario').innerHTML = data.combo;
        }
    });
}

//Combo tabla Producto inventario entrada
function cbo_conceptoInEn(ruta,id){
ruta = ruta + "/"+id; 
	$.ajax({
		url : ruta,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
            //$('.cbo_modelo').html(response).fadeIn();
            document.getElementById('cbo_conceptoInEn').innerHTML = data.combo;
        }
    });
}


//Combo tabla Producto inventario entrada
function cbo_conceptoInSa(ruta,id){
ruta = ruta + "/"+id; 
	$.ajax({
		url : ruta,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
            //$('.cbo_modelo').html(response).fadeIn();
            document.getElementById('cbo_conceptoInSa').innerHTML = data.combo;
        }
    });
}



//Combo tabla Producto inventario
function cbo_productoInEn(ruta,id){
ruta = ruta + "/"+id; 
	$.ajax({
		url : ruta,
		type: "GET",                    
		dataType: "JSON",
		success: function(data){
            //$('.cbo_modelo').html(response).fadeIn();
            document.getElementById('cbo_productoInEn').innerHTML = data.combo;
        }
    });
}




//Combo Ingrediente

function cbo_ingrediente(ruta,id){
ruta = ruta + "/"+id; 
	$.ajax({
		url : ruta,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
            //$('.cbo_modelo').html(response).fadeIn();
            document.getElementById('cbo_ingrediente').innerHTML = data.combo;
        }
    });
}



//Combo Ingrediente
function cbo_ingredienteAlim(ruta,id){
ruta = ruta + "/"+id; 
	$.ajax({
		url : ruta,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
            //$('.cbo_modelo').html(response).fadeIn();
            document.getElementById('cbo_ingrediente').innerHTML = data.combo;
        }
    });
}

			//Combo tabla Proveedor inventario
function cbo_ingredienteSabor(ruta,id){
ruta = ruta + "/"+id; 
	$.ajax({
		url : ruta,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
            //$('.cbo_modelo').html(response).fadeIn();
            document.getElementById('cbo_ingredienteSabor').innerHTML = data.combo;
        }
    });
}


//Combo Ingrediente
function cbo_ingredienteAdicional(ruta,id){
ruta = ruta + "/"+id; 
	$.ajax({
		url : ruta,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
            //$('.cbo_modelo').html(response).fadeIn();
            document.getElementById('cbo_ingredienteAdicional').innerHTML = data.combo;
        }
    });
}



function cbo_categoriaMenu(ruta,id){
ruta = ruta + "/"+id; 
	$.ajax({
		url : ruta,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
            //$('.cbo_modelo').html(response).fadeIn();
            document.getElementById('cbo_categoriaMenu').innerHTML = data.combo;
        }
    });
}


function cbo_tipoPresentacion(ruta,id){
ruta = ruta + "/"+id; 
	$.ajax({
		url : ruta,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
            //$('.cbo_modelo').html(response).fadeIn();
            document.getElementById('cbo_tipoPresentacion').innerHTML = data.combo;
        }
    });
}

function cbo_administradorUsuario(ruta, id){
ruta = ruta + "/"+id; 
	$.ajax({
		url : ruta,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
            //$('.cbo_modelo').html(response).fadeIn();
            document.getElementById('cbo_administradorUsuario').innerHTML = data.combo;
        }
    });
	
	
	
}


function cbo_administradorEmpresa(ruta, id){
ruta = ruta + "/"+id; 
	$.ajax({
		url : ruta,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
            //$('.cbo_modelo').html(response).fadeIn();
            document.getElementById('cbo_administradorEmpresa').innerHTML = data.combo;
        }
    });
	
	
	
}

function cbo_empresaAdministrador(ruta, id){
ruta = ruta + "/"+id; 
    $.ajax({
		url : ruta,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
            //$('.cbo_modelo').html(response).fadeIn();
            //alert("final");
            document.getElementById('cbo_empresaAdministrador').innerHTML = data.combo;
        }
    });
	
	
	
}



function cbo_administradorSucursal(ruta, id){
ruta = ruta + "/"+id; 
	$.ajax({
		url : ruta,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
            //$('.cbo_modelo').html(response).fadeIn();
            document.getElementById('cbo_administradorSucursal').innerHTML = data.combo;
        }
    });
}


function cbo_sucursalAdm(ruta,id){
/* ****************************************************** */
/* ******** verificar para acomodar pase de datos ajax ** */
/* ****************************************************** */
ruta = ruta + "/"+id; 
	$.ajax({
		url : ruta,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
            //$('.cbo_modelo').html(response).fadeIn();
            document.getElementById('cbo_sucursalAdm').innerHTML = data.combo;
        }
    });	
	
}


function cbo_sucursalFactura(ruta,id){
/* ****************************************************** */
/* ******** verificar para acomodar pase de datos ajax ** */
/* ****************************************************** */
ruta = ruta + "/"+id; 
	$.ajax({
		url : ruta,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
            //$('.cbo_modelo').html(response).fadeIn();
            document.getElementById('cbo_sucursalFactura').innerHTML = data.combo;
        }
    });	
	
}







// personal de sucursal
function cbo_personalSucursal(ruta,id){
/* ****************************************************** */
/* ******** verificar para acomodar pase de datos ajax ** */
/* ****************************************************** */
ruta = ruta + "/"+id; 
	$.ajax({
		url : ruta,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
            document.getElementById('cbo_personalSucursal').innerHTML = data.combo;
        }
    });	
}

//Combo tabla roles sucursal
function cbo_rolSucursal(ruta,id){

ruta = ruta + "/"+id; 
	$.ajax({
		url : ruta,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
            //$('.cbo_modelo').html(response).fadeIn();
            document.getElementById('cbo_rolSucursal').innerHTML = data.combo;
        }
    });
}






