//Combo tabla Modulo del sistema
function cbo_modulo(ruta,id){

ruta = ruta + "/"+id; 
	$.ajax({
		url : ruta,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
            //$('.cbo_modelo').html(response).fadeIn();
            document.getElementById('cbo_modulo').innerHTML = data.combo;
        }
    });
}

//Combo tabla rol-Modulo del sistema
function cbo_rolModulo(ruta,id){
ruta = ruta + "/"+id; 
	$.ajax({
		url : ruta,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
            //$('.cbo_modelo').html(response).fadeIn();
            document.getElementById('cbo_rolModulo').innerHTML = data.combo;
        }
    });
}

//Combo tabla roles - tipo de usuarios
function cbo_rol(ruta,id){

ruta = ruta + "/"+id; 
	$.ajax({
		url : ruta,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
            //$('.cbo_modelo').html(response).fadeIn();
            document.getElementById('cbo_rol').innerHTML = data.combo;
        }
    });
}


//Combo tabla empresa - usuario
function cbo_empresaUsuario(ruta,id){
ruta = ruta + "/"+id; 
	$.ajax({
		url : ruta,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
            //$('.cbo_modelo').html(response).fadeIn();
            document.getElementById('cbo_empresa').innerHTML = data.combo;
        }
    });
}



//Combo tabla Sexo del usuario
function cbo_sexo(ruta,id){

ruta = ruta + "/"+id; 
	$.ajax({
		url : ruta,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
            //$('.cbo_modelo').html(response).fadeIn();
            document.getElementById('cbo_sexo').innerHTML = data.combo;
        }
    });
}




//Combo tabla sg_usuario_empresa_mosulo vs sg_modulo del sistema
function cbo_moduloUsuario(ruta,id){
ruta = ruta + "/"+id; 
	$.ajax({
		url : ruta,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
            //$('.cbo_modelo').html(response).fadeIn();
            document.getElementById('cbo_moduloUsuario').innerHTML = data.combo;
        }
    });
}


//Combo tabla categoria de producto
function cbo_categoriaProducto(ruta,id){

ruta = ruta + "/"+id; 
	$.ajax({
		url : ruta,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
            //$('.cbo_modelo').html(response).fadeIn();
            document.getElementById('cbo_categoriaProducto').innerHTML = data.combo;
        }
    });
}


//Combo tabla unidad medida
function cbo_unidadMedida(ruta,id){

ruta = ruta + "/"+id; 
	$.ajax({
		url : ruta,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
            //$('.cbo_modelo').html(response).fadeIn();
            document.getElementById('cbo_unidadMedida').innerHTML = data.combo;
        }
    });
}



//Combo tabla empresa
function cbo_empresa(ruta,id){

ruta = ruta + "/"+id; 
	$.ajax({
		url : ruta,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
            //$('.cbo_modelo').html(response).fadeIn();
            document.getElementById('cbo_empresa').innerHTML = data.combo;
        }
    });
}


//Combo tabla region
function cbo_region(ruta,id){

ruta = ruta + "/"+id; 
	$.ajax({
		url : ruta,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
            //$('.cbo_modelo').html(response).fadeIn();
            document.getElementById('cbo_region').innerHTML = data.combo;
        }
    });
}


//Combo tabla sucursal
function cbo_sucursalUsuario(ruta,id,id_pertenece, id_usuario){

ruta = ruta + "/"+id+"/"+id_pertenece+"/"+id_usuario; 

$(document).ready(function() {
	$.ajax({
		url : ruta,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
            //$('.cbo_modelo').html(response).fadeIn();
            document.getElementById('cbo_sucursal').innerHTML = data.combo;
        }
    });
});
}


//Combo tabla Tipo operacion inventario
function cbo_tipoOperacion(ruta,id){
ruta = ruta + "/"+id; 
	$.ajax({
		url : ruta,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
            //$('.cbo_tipoOperacion').html(response).fadeIn();
            document.getElementById('cbo_tipoOperacion').innerHTML = data.combo;
        }
    });
}


//Combo tabla Proveedor inventario
function cbo_proveedorIn(ruta,id){
ruta = ruta + "/"+id; 
alert("Ruta: " + ruta);

	$.ajax({
		url : ruta,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
            //$('.cbo_modelo').html(response).fadeIn();
            document.getElementById('cbo_proveedorIn').innerHTML = data.combo;
        }
    });
}

//Combo tabla Proveedor inventario
function cbo_conceptoIn(ruta,id, tipo){
ruta = ruta + "/"+id+"/"+tipo"; 
	$.ajax({
		url : ruta,
		type: "GET",                     
		dataType: "JSON",
		success: function(data){
            //$('.cbo_modelo').html(response).fadeIn();
            document.getElementById('cbo_conceptoIn').innerHTML = data.combo;
        }
    });
}
