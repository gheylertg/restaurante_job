-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 25-01-2021 a las 12:48:28
-- Versión del servidor: 8.0.22-0ubuntu0.20.04.2
-- Versión de PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `restaurante`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ad_alimento`
--

CREATE TABLE `ad_alimento` (
  `id` int NOT NULL,
  `id_categoria_alimento` int NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `descripcion` text,
  `id_unidad_medida` int DEFAULT NULL,
  `administrador` int NOT NULL DEFAULT '0',
  `id_empresa` int NOT NULL DEFAULT '0',
  `id_sucursal` int NOT NULL DEFAULT '0',
  `activo` int NOT NULL DEFAULT '1',
  `id_create` int NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_update` int DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ad_categoria`
--

CREATE TABLE `ad_categoria` (
  `id` int NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `administrador` int NOT NULL DEFAULT '0',
  `id_empresa` int NOT NULL DEFAULT '0',
  `id_sucursal` int NOT NULL DEFAULT '0',
  `activo` int NOT NULL DEFAULT '1',
  `id_create` int NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_update` int DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ad_categoria_alimento`
--

CREATE TABLE `ad_categoria_alimento` (
  `id` int NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `administrador` int NOT NULL DEFAULT '0',
  `id_empresa` int NOT NULL,
  `id_sucursal` int NOT NULL,
  `no_modificar` int NOT NULL DEFAULT '0',
  `activo` int NOT NULL DEFAULT '1',
  `id_create` int NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_update` int DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ad_contenedor`
--

CREATE TABLE `ad_contenedor` (
  `id` int NOT NULL,
  `id_sabor` int NOT NULL,
  `cantidad` int NOT NULL,
  `id_empresa` int NOT NULL,
  `id_sucursal` int NOT NULL,
  `activo` int NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ad_envoltura`
--

CREATE TABLE `ad_envoltura` (
  `id` int NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `id_ingrediente` int DEFAULT NULL,
  `cantidad_ingrediente` decimal(10,2) NOT NULL DEFAULT '0.00',
  `id_unidad_medida` int NOT NULL DEFAULT '0',
  `costo` decimal(10,2) NOT NULL DEFAULT '0.00',
  `administrador` int NOT NULL DEFAULT '0',
  `id_empresa` int NOT NULL DEFAULT '0',
  `id_sucursal` int NOT NULL DEFAULT '0',
  `activo` int NOT NULL DEFAULT '1',
  `id_create` int NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_update` int DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ad_envoltura`
--

INSERT INTO `ad_envoltura` (`id`, `nombre`, `id_ingrediente`, `cantidad_ingrediente`, `id_unidad_medida`, `costo`, `administrador`, `id_empresa`, `id_sucursal`, `activo`, `id_create`, `date_create`, `id_update`, `date_update`) VALUES
(11, 'Env tajada', 30, '10.00', 4, '10.00', 3, 2, 6, 1, 57, '2021-01-21 22:33:05', 57, '2021-01-22 17:28:29'),
(12, 'Env. Arroz', 29, '30.00', 4, '1.00', 3, 2, 6, 1, 57, '2021-01-21 22:33:33', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ad_ingrediente`
--

CREATE TABLE `ad_ingrediente` (
  `id` int NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `id_unidad_medida` int NOT NULL,
  `administrador` int NOT NULL DEFAULT '0',
  `id_empresa` int NOT NULL DEFAULT '0',
  `id_sucursal` int NOT NULL DEFAULT '0',
  `activo` int NOT NULL DEFAULT '1',
  `id_create` int NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_update` int DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ad_medida`
--

CREATE TABLE `ad_medida` (
  `id` int NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `administrador` int NOT NULL DEFAULT '0',
  `id_empresa` int NOT NULL DEFAULT '0',
  `id_sucursal` int NOT NULL DEFAULT '0',
  `activo` int NOT NULL DEFAULT '1',
  `id_create` int NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_update` int DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ad_mesa`
--

CREATE TABLE `ad_mesa` (
  `id` int NOT NULL,
  `id_sala` int NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `nro_silla` int NOT NULL DEFAULT '0',
  `administrador` int NOT NULL DEFAULT '0',
  `id_empresa` int NOT NULL DEFAULT '0',
  `id_sucursal` int NOT NULL DEFAULT '0',
  `activo` int NOT NULL DEFAULT '1',
  `id_create` int NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_update` int DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ad_mesa`
--

INSERT INTO `ad_mesa` (`id`, `id_sala`, `nombre`, `nro_silla`, `administrador`, `id_empresa`, `id_sucursal`, `activo`, `id_create`, `date_create`, `id_update`, `date_update`) VALUES
(1, 1, 'MESA 01', 6, 0, 2, 2, 1, 3, '2020-12-02 15:46:57', NULL, NULL),
(2, 2, 'MESA 01', 8, 0, 2, 2, 1, 3, '2020-12-02 15:48:49', NULL, NULL),
(3, 1, 'MESA 02', 8, 0, 2, 2, 1, 1, '2020-12-03 22:58:41', NULL, NULL),
(4, 1, 'MESA 03', 6, 0, 2, 2, 1, 3, '2020-12-31 04:05:59', NULL, NULL),
(5, 1, 'MESA 04', 8, 0, 2, 2, 1, 3, '2020-12-31 04:06:13', NULL, NULL),
(7, 4, 'MESA 01', 6, 3, 2, 6, 1, 46, '2021-01-10 13:03:08', NULL, NULL),
(8, 4, 'MESA 02', 8, 3, 2, 6, 1, 46, '2021-01-10 13:05:05', NULL, NULL),
(9, 4, 'MESA 03', 3, 3, 2, 6, 1, 46, '2021-01-10 13:05:41', NULL, NULL),
(10, 4, 'MESA 04', 10, 3, 2, 6, 1, 46, '2021-01-10 13:08:01', 46, '2021-01-10 13:10:12'),
(11, 5, 'Mesa 01', 6, 3, 2, 6, 1, 57, '2021-01-21 23:16:23', NULL, NULL),
(12, 5, 'Mesa 02', 4, 3, 2, 6, 1, 57, '2021-01-21 23:16:35', NULL, NULL),
(13, 5, 'Mesa 03', 3, 3, 2, 6, 1, 57, '2021-01-21 23:16:45', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ad_mesonero`
--

CREATE TABLE `ad_mesonero` (
  `id` int NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `apellido` varchar(100) NOT NULL,
  `administrador` int NOT NULL DEFAULT '0',
  `id_empresa` int NOT NULL,
  `id_sucursal` int NOT NULL,
  `activo` int NOT NULL DEFAULT '1',
  `id_create` int NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_update` int DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ad_mesonero`
--

INSERT INTO `ad_mesonero` (`id`, `nombre`, `apellido`, `administrador`, `id_empresa`, `id_sucursal`, `activo`, `id_create`, `date_create`, `id_update`, `date_update`) VALUES
(4, 'Pedro', 'Perez', 3, 2, 6, 1, 57, '2021-01-21 23:15:18', NULL, NULL),
(5, 'Juana', 'Castillo', 3, 2, 6, 1, 57, '2021-01-21 23:15:35', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ad_sabor_ingrediente`
--

CREATE TABLE `ad_sabor_ingrediente` (
  `id` int NOT NULL,
  `id_sabor` int NOT NULL,
  `id_ingrediente` int NOT NULL,
  `cantidad` decimal(10,2) NOT NULL DEFAULT '0.00',
  `id_unidad_medida` int NOT NULL,
  `activo` int NOT NULL DEFAULT '1',
  `id_create` int NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_update` int DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ad_sala`
--

CREATE TABLE `ad_sala` (
  `id` int NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `administrador` int NOT NULL DEFAULT '0',
  `Id_empresa` int NOT NULL DEFAULT '0',
  `id_sucursal` int NOT NULL DEFAULT '0',
  `activo` int NOT NULL DEFAULT '1',
  `id_create` int NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_update` int DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ad_sala`
--

INSERT INTO `ad_sala` (`id`, `nombre`, `administrador`, `Id_empresa`, `id_sucursal`, `activo`, `id_create`, `date_create`, `id_update`, `date_update`) VALUES
(5, 'Sala Principal', 3, 2, 6, 1, 57, '2021-01-21 23:16:06', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cf_configuracion`
--

CREATE TABLE `cf_configuracion` (
  `id` int NOT NULL,
  `administrador` int NOT NULL DEFAULT '0',
  `id_empresa` int NOT NULL,
  `id_sucursal` int NOT NULL,
  `dolar` decimal(10,2) NOT NULL DEFAULT '0.00',
  `euro` decimal(10,2) NOT NULL DEFAULT '0.00',
  `impuesto` decimal(10,2) NOT NULL DEFAULT '0.00',
  `date_dolar` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_update_dolar` int DEFAULT NULL,
  `date_impuesto` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_update_impuesto` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cf_configuracion`
--

INSERT INTO `cf_configuracion` (`id`, `administrador`, `id_empresa`, `id_sucursal`, `dolar`, `euro`, `impuesto`, `date_dolar`, `id_update_dolar`, `date_impuesto`, `id_update_impuesto`) VALUES
(1, 3, 2, 4, '1.10', '0.15', '0.00', '2021-01-10 21:29:14', NULL, '2021-01-10 21:29:14', NULL),
(2, 3, 2, 6, '1.60', '0.00', '10.00', '2021-01-10 21:50:25', 46, '2021-01-10 21:50:33', 46);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cf_correlativo`
--

CREATE TABLE `cf_correlativo` (
  `id` int NOT NULL,
  `id_empresa` int NOT NULL DEFAULT '0',
  `id_sucursal` int NOT NULL DEFAULT '0',
  `orden_compra` int NOT NULL DEFAULT '0',
  `codigo_venta` int NOT NULL DEFAULT '0',
  `nro_factura` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cf_correlativo`
--

INSERT INTO `cf_correlativo` (`id`, `id_empresa`, `id_sucursal`, `orden_compra`, `codigo_venta`, `nro_factura`) VALUES
(1, 2, 4, 0, 2, 0),
(4, 2, 6, 0, 11, 8);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cf_empresa`
--

CREATE TABLE `cf_empresa` (
  `id` int NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `sw_administrador` int NOT NULL DEFAULT '0',
  `no_modificar` int NOT NULL DEFAULT '0',
  `rut` varchar(100) DEFAULT NULL,
  `correo` varchar(200) DEFAULT NULL,
  `contacto` varchar(200) DEFAULT NULL,
  `telefono_local` varchar(100) DEFAULT NULL,
  `telefono_celular` varchar(100) DEFAULT NULL,
  `direccion` text,
  `imagen` varchar(100) DEFAULT NULL,
  `logo` varchar(300) DEFAULT NULL,
  `administrador` int NOT NULL DEFAULT '0',
  `id_empresa` int NOT NULL DEFAULT '0',
  `id_sucursal` int NOT NULL DEFAULT '0',
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  `id_create` int NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_update` int DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cf_empresa`
--

INSERT INTO `cf_empresa` (`id`, `nombre`, `sw_administrador`, `no_modificar`, `rut`, `correo`, `contacto`, `telefono_local`, `telefono_celular`, `direccion`, `imagen`, `logo`, `administrador`, `id_empresa`, `id_sucursal`, `activo`, `id_create`, `date_create`, `id_update`, `date_update`) VALUES
(1, 'EMPRESA PRINCIPAL', 1, 1, '0', '', '', '', '', '', NULL, NULL, 1, 0, 0, 1, 1, '2020-11-14 12:23:27', 3, '2020-11-24 23:50:32'),
(2, 'Sushi Chile', 0, 0, '125252', '', '', '', '', 'Av Chile, calle perimetral, esquina Libertador, una cadena mas larga para alargar una prueba', '1-mod-imagen-resta04.jpg', '1-mod-logo-foto_logo_principal.jpeg', 1, 0, 0, 1, 1, '2020-12-29 04:01:18', 1, '2021-01-17 16:03:16'),
(28, 'Sushi Mar', 0, 0, '12352512-k', 'sushimat@mdd.com', '', '1252525', '1888855', '', '1-2021-01-19-11-imagen-resta05.jpg', '1-mod-logo-ingredienteSushi.jpeg', 1, 1, 1, 1, 1, '2021-01-19 21:05:11', 1, '2021-01-19 21:06:23');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cf_region`
--

CREATE TABLE `cf_region` (
  `id` int NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `administrador` int NOT NULL DEFAULT '0',
  `id_empresa` int NOT NULL DEFAULT '0',
  `id_sucursal` int NOT NULL DEFAULT '0',
  `activo` int NOT NULL DEFAULT '1',
  `id_create` int NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_update` int DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cf_region`
--

INSERT INTO `cf_region` (`id`, `nombre`, `administrador`, `id_empresa`, `id_sucursal`, `activo`, `id_create`, `date_create`, `id_update`, `date_update`) VALUES
(1, 'Región central', 1, 0, 0, 1, 3, '2020-12-01 00:26:26', NULL, NULL),
(2, 'REGIÓN SUR', 1, 0, 0, 1, 3, '2021-01-04 13:19:07', NULL, NULL),
(4, 'REGIÓN NORTES', 2, 2, 0, 1, 3, '2021-01-05 15:09:03', 3, '2021-01-05 15:10:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cf_sucursal`
--

CREATE TABLE `cf_sucursal` (
  `id` int NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `rut` varchar(100) NOT NULL,
  `correo` varchar(200) DEFAULT NULL,
  `contacto` varchar(200) DEFAULT NULL,
  `direccion` text,
  `telefono_local` varchar(200) DEFAULT NULL,
  `telefono_celular` varchar(200) DEFAULT NULL,
  `id_region` int NOT NULL,
  `id_empresa` int NOT NULL,
  `administrador` int NOT NULL DEFAULT '0',
  `no_modificar` int NOT NULL DEFAULT '0',
  `id_sucursal` int NOT NULL DEFAULT '0',
  `sw_sucursal` int NOT NULL DEFAULT '0',
  `activo` int NOT NULL DEFAULT '1',
  `id_create` int NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_update` int DEFAULT NULL,
  `date_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cf_sucursal`
--

INSERT INTO `cf_sucursal` (`id`, `nombre`, `rut`, `correo`, `contacto`, `direccion`, `telefono_local`, `telefono_celular`, `id_region`, `id_empresa`, `administrador`, `no_modificar`, `id_sucursal`, `sw_sucursal`, `activo`, `id_create`, `date_create`, `id_update`, `date_update`) VALUES
(1, 'SUCURSAL PRINCIPAL', '12345678', '', '', '', 'rerrw', '', 1, 1, 1, 1, 0, 0, 1, 1, '2020-11-30 11:05:20', 3, '2020-12-01 00:45:25'),
(2, 'SUCURSAL PRINCIPAL EMPRESA', '1252525', '', '', '', '', '', 1, 2, 2, 1, 0, 0, 1, 3, '2020-12-01 01:05:53', NULL, '2020-12-01 01:05:53'),
(4, 'AMERICA', '123456', '', '', '', '', '', 2, 2, 2, 0, 2, 1, 1, 3, '2021-01-05 14:12:00', NULL, '2021-01-05 14:12:00'),
(6, 'Sucursal Chile01', '1252525', 'sucursal@sss.com', '', 'Región norte, av, Las maravillas', '056+125-12.63', '', 1, 2, 2, 0, 2, 1, 1, 32, '2021-01-07 20:35:12', NULL, '2021-01-07 20:35:12'),
(25, 'SUCURSAL PRINCIPAL Sushi Mar', '0', NULL, NULL, NULL, NULL, NULL, 1, 28, 1, 1, 0, 0, 1, 1, '2021-01-19 21:05:11', NULL, '2021-01-19 21:05:11');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cj_caja`
--

CREATE TABLE `cj_caja` (
  `id` int NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_usuario_empresa` int NOT NULL,
  `id_usuario` int NOT NULL,
  `id_empresa` int NOT NULL,
  `id_sucursal` int NOT NULL,
  `estado` int NOT NULL DEFAULT '1',
  `observacion` text,
  `fecha_cierre` timestamp NULL DEFAULT NULL,
  `id_encargado_cierre` int DEFAULT NULL,
  `observacion_cierre` text,
  `monto_cierre` decimal(10,2) DEFAULT '0.00',
  `activo` int NOT NULL DEFAULT '1',
  `id_create` int NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_update` int DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cj_caja`
--

INSERT INTO `cj_caja` (`id`, `fecha`, `id_usuario_empresa`, `id_usuario`, `id_empresa`, `id_sucursal`, `estado`, `observacion`, `fecha_cierre`, `id_encargado_cierre`, `observacion_cierre`, `monto_cierre`, `activo`, `id_create`, `date_create`, `id_update`, `date_update`) VALUES
(25, '2021-01-21 23:14:16', 100, 58, 2, 6, 1, '', NULL, NULL, NULL, '0.00', 1, 58, '2021-01-21 23:14:16', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cj_cobrar`
--

CREATE TABLE `cj_cobrar` (
  `id` int NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_venta` int NOT NULL,
  `id_caja` int NOT NULL,
  `id_tipo_despacho` int NOT NULL,
  `monto_cobrar` decimal(10,2) NOT NULL DEFAULT '0.00',
  `monto_pedido` decimal(10,2) NOT NULL DEFAULT '0.00',
  `porc_impuesto` decimal(10,2) NOT NULL DEFAULT '0.00',
  `impuesto` decimal(10,2) NOT NULL DEFAULT '0.00',
  `porc_descuento` decimal(10,2) NOT NULL DEFAULT '0.00',
  `descuento` decimal(10,2) NOT NULL DEFAULT '0.00',
  `total_cobrar` decimal(10,2) NOT NULL DEFAULT '0.00',
  `id_empresa` int NOT NULL,
  `id_sucursal` int NOT NULL,
  `cod_venta` varchar(100) NOT NULL,
  `nombre_cliente` varchar(200) DEFAULT NULL,
  `telefono` varchar(100) DEFAULT NULL,
  `direccion` text,
  `descripcion` text,
  `reverso` int NOT NULL DEFAULT '0',
  `fecha_reverso` timestamp NULL DEFAULT NULL,
  `motivo_reverso` text,
  `activo` int NOT NULL DEFAULT '1',
  `id_create` int NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_update` int DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cj_cobrar_dinero`
--

CREATE TABLE `cj_cobrar_dinero` (
  `id` int NOT NULL,
  `id_cobrar` int NOT NULL,
  `id_caja` int NOT NULL,
  `id_tipo_dinero` int NOT NULL,
  `id_tipo_despacho` int NOT NULL,
  `monto_peso` decimal(10,2) NOT NULL,
  `monto_dolar` decimal(10,2) NOT NULL DEFAULT '0.00',
  `activo` int NOT NULL DEFAULT '1',
  `id_create` int NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_update` int DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cj_movimiento_caja`
--

CREATE TABLE `cj_movimiento_caja` (
  `id` int NOT NULL,
  `id_caja` int NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_tipo_operacion` int NOT NULL,
  `id_tipo_movimiento` int NOT NULL,
  `monto` decimal(10,2) NOT NULL DEFAULT '0.00',
  `observacion` text,
  `reverso` int NOT NULL DEFAULT '0',
  `fecha_reverso` timestamp NULL DEFAULT NULL,
  `motivo_reverso` text,
  `activo` int NOT NULL DEFAULT '1',
  `id_create` int NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_update` int DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cj_movimiento_caja`
--

INSERT INTO `cj_movimiento_caja` (`id`, `id_caja`, `fecha`, `id_tipo_operacion`, `id_tipo_movimiento`, `monto`, `observacion`, `reverso`, `fecha_reverso`, `motivo_reverso`, `activo`, `id_create`, `date_create`, `id_update`, `date_update`) VALUES
(37, 25, '2021-01-21 23:14:16', 1, 1, '30.00', NULL, 0, NULL, NULL, 1, 0, '2021-01-21 23:14:16', NULL, NULL),
(38, 0, '2021-01-25 12:37:28', 1, 8, '100.00', '', 0, NULL, NULL, 1, 58, '2021-01-25 12:37:28', NULL, NULL),
(39, 0, '2021-01-25 15:53:27', 2, 2, '10.00', '', 1, '2021-01-25 15:53:44', 'Reverso xxxx', 0, 58, '2021-01-25 15:53:27', 58, '2021-01-25 15:53:44');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `co_contenedor`
--

CREATE TABLE `co_contenedor` (
  `id` int NOT NULL,
  `id_alimento` int NOT NULL,
  `cantidad` int NOT NULL DEFAULT '0',
  `administrador` int NOT NULL DEFAULT '0',
  `id_empresa` int NOT NULL DEFAULT '0',
  `id_sucursal` int NOT NULL DEFAULT '0',
  `activo` int NOT NULL DEFAULT '1',
  `id_create` int NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_update` int DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `co_contenedor`
--

INSERT INTO `co_contenedor` (`id`, `id_alimento`, `cantidad`, `administrador`, `id_empresa`, `id_sucursal`, `activo`, `id_create`, `date_create`, `id_update`, `date_update`) VALUES
(26, 56, -30, 0, 2, 6, 1, 57, '2021-01-21 23:10:26', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cp_detalle_factura`
--

CREATE TABLE `cp_detalle_factura` (
  `id` int NOT NULL,
  `id_factura` int NOT NULL,
  `id_producto` int NOT NULL,
  `cantidad` decimal(10,2) NOT NULL DEFAULT '0.00',
  `monto` decimal(10,2) NOT NULL DEFAULT '0.00',
  `id_create` int NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_update` int DEFAULT NULL,
  `date_update` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cp_detalle_orden`
--

CREATE TABLE `cp_detalle_orden` (
  `id` int NOT NULL,
  `id_orden` int NOT NULL,
  `id_producto` int NOT NULL,
  `cantidad` decimal(10,0) NOT NULL DEFAULT '0',
  `activo` int NOT NULL DEFAULT '1',
  `id_create` int NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_update` int NOT NULL,
  `date_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cp_factura`
--

CREATE TABLE `cp_factura` (
  `id` int NOT NULL,
  `fecha` timestamp NOT NULL,
  `factura` varchar(100) DEFAULT NULL,
  `monto` decimal(10,2) NOT NULL DEFAULT '0.00',
  `id_proveedor` int NOT NULL,
  `administrador` int NOT NULL DEFAULT '0',
  `id_empresa` int NOT NULL DEFAULT '0',
  `id_sucursal` int NOT NULL DEFAULT '0',
  `activo` int NOT NULL DEFAULT '1',
  `id_create` int NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_update` int DEFAULT NULL,
  `date_update` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cp_orden`
--

CREATE TABLE `cp_orden` (
  `id` int NOT NULL,
  `fecha` timestamp NOT NULL,
  `nro_orden` varchar(100) NOT NULL,
  `id_proveedor` int NOT NULL,
  `id_responsable` int NOT NULL,
  `administrador` int NOT NULL DEFAULT '0',
  `id_empresa` int NOT NULL DEFAULT '0',
  `id_sucursal` int NOT NULL DEFAULT '0',
  `activo` int NOT NULL,
  `id_create` int NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_update` int DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dm_mesa_despacho`
--

CREATE TABLE `dm_mesa_despacho` (
  `id` int NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `monto` decimal(10,2) NOT NULL DEFAULT '0.00',
  `id_sala` int DEFAULT '0',
  `id_mesa` int DEFAULT '0',
  `id_mesonero` int NOT NULL DEFAULT '0',
  `id_tipo_despacho` int NOT NULL,
  `id_empresa` int NOT NULL,
  `id_sucursal` int NOT NULL,
  `estado_despacho` int NOT NULL DEFAULT '1',
  `cobrado` int NOT NULL DEFAULT '0',
  `despachado` int NOT NULL DEFAULT '0',
  `activo` int NOT NULL DEFAULT '1',
  `id_create` int NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_update` int DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dm_tipo_despacho`
--

CREATE TABLE `dm_tipo_despacho` (
  `id` int NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `administrador` int NOT NULL DEFAULT '0',
  `id_empresa` int NOT NULL DEFAULT '0',
  `id_sucursal` int NOT NULL DEFAULT '0',
  `activo` int NOT NULL DEFAULT '1',
  `id_create` int NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_update` int DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `dm_tipo_despacho`
--

INSERT INTO `dm_tipo_despacho` (`id`, `nombre`, `administrador`, `id_empresa`, `id_sucursal`, `activo`, `id_create`, `date_create`, `id_update`, `date_update`) VALUES
(1, 'DESPACHO EN MESA', 1, 0, 0, 1, 1, '2020-12-02 16:05:06', NULL, NULL),
(2, 'RETIRO EN TIENDA', 1, 0, 0, 1, 1, '2020-12-02 16:05:06', NULL, NULL),
(3, 'DELIVERY', 1, 0, 0, 1, 1, '2020-12-02 16:05:26', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dp_despacho`
--

CREATE TABLE `dp_despacho` (
  `id` int NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `codigo_venta` varchar(100) NOT NULL,
  `monto` decimal(10,2) NOT NULL DEFAULT '0.00',
  `impuesto` decimal(10,2) NOT NULL DEFAULT '0.00',
  `id_sala` int NOT NULL DEFAULT '0',
  `id_mesa` int NOT NULL DEFAULT '0',
  `id_mesonero` int NOT NULL DEFAULT '0',
  `id_tipo_despacho` int NOT NULL,
  `id_empresa` int NOT NULL DEFAULT '0',
  `id_sucursal` int NOT NULL DEFAULT '0',
  `id_estado_despacho` int NOT NULL DEFAULT '0',
  `cobrado` int NOT NULL DEFAULT '0',
  `despachado` int NOT NULL DEFAULT '0',
  `nombre_cliente` varchar(200) DEFAULT NULL,
  `telefono_cliente` varchar(200) DEFAULT NULL,
  `direccion_cliente` text,
  `descripcion_cliente` text,
  `cobrar_nuevo` int NOT NULL DEFAULT '0',
  `reverso` int DEFAULT '0',
  `motivo_reverso` text,
  `id_responsable_reverso` int DEFAULT NULL,
  `activo` int NOT NULL DEFAULT '1',
  `id_create` int NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_update` int DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `dp_despacho`
--

INSERT INTO `dp_despacho` (`id`, `fecha`, `codigo_venta`, `monto`, `impuesto`, `id_sala`, `id_mesa`, `id_mesonero`, `id_tipo_despacho`, `id_empresa`, `id_sucursal`, `id_estado_despacho`, `cobrado`, `despachado`, `nombre_cliente`, `telefono_cliente`, `direccion_cliente`, `descripcion_cliente`, `cobrar_nuevo`, `reverso`, `motivo_reverso`, `id_responsable_reverso`, `activo`, `id_create`, `date_create`, `id_update`, `date_update`) VALUES
(19, '2021-01-21 23:17:20', '0000010', '112.40', '10.00', 5, 11, 5, 1, 2, 6, 4, 0, 1, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, 1, 58, '2021-01-21 23:17:20', NULL, NULL),
(20, '2021-01-23 20:23:58', '0000011', '30.00', '10.00', 0, 0, 0, 2, 2, 6, 1, 0, 0, 'Pedro Perez', '12525222', '', '', 0, 0, NULL, NULL, 1, 58, '2021-01-23 20:23:58', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dp_despacho_detalle`
--

CREATE TABLE `dp_despacho_detalle` (
  `id` int NOT NULL,
  `id_despacho` int NOT NULL,
  `id_categoria_menu` int NOT NULL,
  `id_alimento` int NOT NULL,
  `id_presentacion` int NOT NULL,
  `id_envoltura` int NOT NULL DEFAULT '0',
  `precio_sushi` decimal(10,2) NOT NULL DEFAULT '0.00',
  `precio_unitario` decimal(10,2) NOT NULL DEFAULT '0.00',
  `cantidad` int NOT NULL,
  `cantidad_envoltura` int NOT NULL DEFAULT '0',
  `precio_envoltura` decimal(10,2) NOT NULL DEFAULT '0.00',
  `descripcion_adicional` varchar(500) DEFAULT NULL,
  `sin_ingrediente` text,
  `precio_adicional` decimal(10,2) NOT NULL DEFAULT '0.00',
  `precio` decimal(10,2) NOT NULL DEFAULT '0.00',
  `nro_pieza` int NOT NULL,
  `id_unidad_medida` int NOT NULL,
  `activo` int NOT NULL DEFAULT '1',
  `id_create` int NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_update` int DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `dp_despacho_detalle`
--

INSERT INTO `dp_despacho_detalle` (`id`, `id_despacho`, `id_categoria_menu`, `id_alimento`, `id_presentacion`, `id_envoltura`, `precio_sushi`, `precio_unitario`, `cantidad`, `cantidad_envoltura`, `precio_envoltura`, `descripcion_adicional`, `sin_ingrediente`, `precio_adicional`, `precio`, `nro_pieza`, `id_unidad_medida`, `activo`, `id_create`, `date_create`, `id_update`, `date_update`) VALUES
(280, 19, 1, 56, 73, 11, '38.00', '48.00', 1, 1, '10.00', '', NULL, '0.00', '48.00', 12, 2, 1, 58, '2021-01-21 23:19:47', NULL, NULL),
(281, 19, 4, 57, 74, 0, '0.00', '1.20', 2, 2, '0.00', NULL, NULL, '0.00', '2.40', 1, 5, 1, 58, '2021-01-21 23:21:10', NULL, NULL),
(282, 20, 1, 56, 72, 11, '20.00', '30.00', 1, 1, '10.00', '', NULL, '0.00', '30.00', 6, 2, 1, 58, '2021-01-23 20:24:18', NULL, NULL),
(283, 19, 1, 56, 72, 11, '20.00', '32.00', 1, 1, '10.00', 'Tajada(2.00)', NULL, '2.00', '32.00', 6, 2, 1, 58, '2021-01-23 21:08:29', NULL, NULL),
(284, 19, 1, 56, 72, 11, '20.00', '30.00', 1, 1, '10.00', '', 'Salsa Agria', '0.00', '30.00', 6, 2, 1, 58, '2021-01-23 21:09:11', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dp_despacho_detalle_ingrediente`
--

CREATE TABLE `dp_despacho_detalle_ingrediente` (
  `id` int NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_despacho_detalle` int NOT NULL,
  `id_ingrediente` int NOT NULL,
  `id_presentacion` int NOT NULL,
  `cantidad_individual` decimal(10,2) NOT NULL DEFAULT '0.00',
  `nro_alimento` int NOT NULL,
  `cantidad_total` decimal(10,2) NOT NULL DEFAULT '0.00',
  `activo` int NOT NULL DEFAULT '1',
  `id_create` int NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_update` int DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `dp_despacho_detalle_ingrediente`
--

INSERT INTO `dp_despacho_detalle_ingrediente` (`id`, `fecha`, `id_despacho_detalle`, `id_ingrediente`, `id_presentacion`, `cantidad_individual`, `nro_alimento`, `cantidad_total`, `activo`, `id_create`, `date_create`, `id_update`, `date_update`) VALUES
(430, '2021-01-21 23:19:48', 280, 29, 73, '0.00', 1, '0.00', 1, 58, '2021-01-21 23:19:48', NULL, NULL),
(431, '2021-01-21 23:19:48', 280, 32, 73, '0.00', 1, '0.00', 1, 58, '2021-01-21 23:19:48', NULL, NULL),
(432, '2021-01-21 23:19:48', 280, 30, 73, '10.00', 1, '10.00', 1, 58, '2021-01-21 23:19:48', NULL, NULL),
(433, '2021-01-23 20:24:18', 282, 29, 72, '0.00', 1, '0.00', 1, 58, '2021-01-23 20:24:18', NULL, NULL),
(434, '2021-01-23 20:24:18', 282, 32, 72, '0.00', 1, '0.00', 1, 58, '2021-01-23 20:24:18', NULL, NULL),
(435, '2021-01-23 20:24:18', 282, 30, 72, '10.00', 1, '10.00', 1, 58, '2021-01-23 20:24:18', NULL, NULL),
(436, '2021-01-23 21:08:30', 283, 29, 72, '0.00', 1, '0.00', 1, 58, '2021-01-23 21:08:30', NULL, NULL),
(437, '2021-01-23 21:08:30', 283, 32, 72, '0.00', 1, '0.00', 1, 58, '2021-01-23 21:08:30', NULL, NULL),
(438, '2021-01-23 21:08:30', 283, 30, 72, '10.00', 1, '10.00', 1, 58, '2021-01-23 21:08:30', NULL, NULL),
(439, '2021-01-23 21:08:30', 283, 30, 72, '30.00', 1, '30.00', 1, 58, '2021-01-23 21:08:30', NULL, NULL),
(440, '2021-01-23 21:09:12', 284, 29, 72, '0.00', 1, '0.00', 1, 58, '2021-01-23 21:09:12', NULL, NULL),
(441, '2021-01-23 21:09:12', 284, 30, 72, '10.00', 1, '10.00', 1, 58, '2021-01-23 21:09:12', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dp_despacho_detalle_ingrediente_adicional`
--

CREATE TABLE `dp_despacho_detalle_ingrediente_adicional` (
  `id` int NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_despacho_detalle` int NOT NULL,
  `id_ingrediente` int NOT NULL,
  `precio` decimal(10,2) NOT NULL DEFAULT '0.00',
  `cantidad` decimal(10,2) NOT NULL DEFAULT '0.00',
  `nro_vendido` int NOT NULL DEFAULT '0',
  `cantidad_total` decimal(10,0) NOT NULL DEFAULT '0',
  `precio_total` decimal(10,0) NOT NULL DEFAULT '0',
  `activo` int NOT NULL DEFAULT '1',
  `id_create` int NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_update` int DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `dp_despacho_detalle_ingrediente_adicional`
--

INSERT INTO `dp_despacho_detalle_ingrediente_adicional` (`id`, `fecha`, `id_despacho_detalle`, `id_ingrediente`, `precio`, `cantidad`, `nro_vendido`, `cantidad_total`, `precio_total`, `activo`, `id_create`, `date_create`, `id_update`, `date_update`) VALUES
(52, '2021-01-23 21:08:30', 283, 30, '2.00', '30.00', 1, '30', '2', 1, 58, '2021-01-23 21:08:30', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dp_estado_despacho`
--

CREATE TABLE `dp_estado_despacho` (
  `id` int NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `activo` int NOT NULL DEFAULT '1',
  `id_create` int NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_update` int DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `dp_estado_despacho`
--

INSERT INTO `dp_estado_despacho` (`id`, `nombre`, `activo`, `id_create`, `date_create`, `id_update`, `date_update`) VALUES
(1, 'En proceso', 1, 1, '2020-12-18 01:59:05', NULL, NULL),
(2, 'Por despachar', 1, 1, '2020-12-18 01:59:05', NULL, NULL),
(3, 'Despachado', 1, 1, '2020-12-18 01:59:26', NULL, NULL),
(4, 'Por cobrar', 1, 3, '2020-12-24 01:45:43', NULL, NULL),
(5, 'Pagado', 1, 3, '2020-12-24 23:18:20', NULL, NULL),
(6, 'Reversado', 1, 1, '2021-01-02 21:34:15', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dp_tipo_despacho`
--

CREATE TABLE `dp_tipo_despacho` (
  `id` int NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `activo` int NOT NULL DEFAULT '1',
  `id_create` int NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_update` int DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `dp_tipo_despacho`
--

INSERT INTO `dp_tipo_despacho` (`id`, `nombre`, `activo`, `id_create`, `date_create`, `id_update`, `date_update`) VALUES
(1, 'Despacho en mesa', 1, 1, '2020-12-18 03:48:22', NULL, NULL),
(2, 'Retiro en tienda', 1, 1, '2020-12-18 03:48:56', NULL, NULL),
(3, 'Delivery', 1, 1, '2020-12-18 03:49:10', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `in_concepto_inventario`
--

CREATE TABLE `in_concepto_inventario` (
  `id` int NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `id_tipo_operacion` int NOT NULL,
  `administrador` int NOT NULL DEFAULT '0',
  `id_empresa` int NOT NULL DEFAULT '0',
  `id_sucursal` int NOT NULL DEFAULT '0',
  `no_modificar` int NOT NULL DEFAULT '0',
  `activo` int NOT NULL DEFAULT '1',
  `id_create` int NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_update` int DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `in_concepto_inventario`
--

INSERT INTO `in_concepto_inventario` (`id`, `nombre`, `id_tipo_operacion`, `administrador`, `id_empresa`, `id_sucursal`, `no_modificar`, `activo`, `id_create`, `date_create`, `id_update`, `date_update`) VALUES
(1, 'Inventario inicial', 1, 1, 0, 0, 1, 1, 1, '2020-11-30 16:42:47', NULL, NULL),
(2, 'Compra', 1, 1, 0, 0, 1, 1, 1, '2020-11-30 16:43:30', NULL, NULL),
(3, 'Venta', 2, 1, 0, 0, 1, 1, 1, '2020-11-30 16:44:14', NULL, NULL),
(4, 'Merma', 2, 1, 0, 0, 1, 1, 1, '2020-11-30 16:44:53', NULL, NULL),
(6, 'Ajuste entrada', 1, 1, 0, 0, 1, 1, 3, '2020-12-01 03:01:51', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `in_inventario`
--

CREATE TABLE `in_inventario` (
  `id` int NOT NULL,
  `fecha_inicial` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_producto` int NOT NULL,
  `entrada` decimal(10,2) NOT NULL DEFAULT '0.00',
  `salida` decimal(10,2) NOT NULL DEFAULT '0.00',
  `stock` decimal(10,2) NOT NULL DEFAULT '0.00',
  `id_usuario_empresa` int NOT NULL,
  `id_empresa` int NOT NULL,
  `id_sucursal` int NOT NULL,
  `activo` int NOT NULL DEFAULT '1',
  `id_create` int NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_update` int DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `in_transaccion`
--

CREATE TABLE `in_transaccion` (
  `id` int NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_tipo_operacion` int NOT NULL,
  `id_concepto` int NOT NULL,
  `id_producto` int NOT NULL,
  `cantidad` decimal(10,2) NOT NULL DEFAULT '0.00',
  `observacion` text,
  `administrador` int NOT NULL DEFAULT '0',
  `id_empresa` int NOT NULL,
  `id_sucursal` int NOT NULL,
  `activo` int NOT NULL DEFAULT '1',
  `id_create` int NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_update` int DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mn_alimento`
--

CREATE TABLE `mn_alimento` (
  `id` int NOT NULL,
  `id_categoria_menu` int NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `nombre_menu` varchar(20) DEFAULT NULL,
  `id_ingrediente` int NOT NULL DEFAULT '0',
  `descripcion` text,
  `imagen` varchar(500) DEFAULT '',
  `administrador` int NOT NULL DEFAULT '0',
  `id_empresa` int NOT NULL DEFAULT '0',
  `id_sucursal` int NOT NULL DEFAULT '0',
  `orden` int NOT NULL DEFAULT '0',
  `activo` int NOT NULL DEFAULT '1',
  `id_create` int NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_update` int DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `mn_alimento`
--

INSERT INTO `mn_alimento` (`id`, `id_categoria_menu`, `nombre`, `nombre_menu`, `id_ingrediente`, `descripcion`, `imagen`, `administrador`, `id_empresa`, `id_sucursal`, `orden`, `activo`, `id_create`, `date_create`, `id_update`, `date_update`) VALUES
(54, 2, 'Arroz', 'Arroz', 29, 'Arroz', '', 0, 2, 6, 0, 1, 57, '2021-01-21 22:32:01', NULL, NULL),
(55, 2, 'Tajada', 'Tajada', 30, 'Tajada', '', 0, 2, 6, 0, 1, 57, '2021-01-21 22:32:13', NULL, NULL),
(56, 1, 'Sushi 01', 'Sushi 01', 0, 'Sushi xxxx', '6-2021-01-21-26-producto-482021-01-17-mod-imagen-icon.png', 0, 2, 6, 1, 1, 57, '2021-01-21 23:10:26', NULL, NULL),
(57, 4, 'Refresco de cola', 'Refresco de cola', 0, 'Refresco de cola', '6-2021-01-21-16-producto-51-2021-01-17-08-33-43-mod-imagen-profile.jpg', 0, 2, 6, 2, 1, 57, '2021-01-21 23:11:16', 57, '2021-01-21 23:12:22'),
(58, 14, 'Sushi 02', 'Sushi 02', 0, 'Sushi 02', '58-2021-01-21-19-12-42-mod-imagen-mesonero.jpeg', 0, 2, 6, 0, 1, 57, '2021-01-21 23:12:09', 57, '2021-01-21 23:12:42');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mn_alimento_ingrediente`
--

CREATE TABLE `mn_alimento_ingrediente` (
  `id` int NOT NULL,
  `id_alimento` int NOT NULL,
  `id_ingrediente` int NOT NULL,
  `id_unidad_medida` int NOT NULL,
  `activo` int NOT NULL DEFAULT '1',
  `id_create` int NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_update` int DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `mn_alimento_ingrediente`
--

INSERT INTO `mn_alimento_ingrediente` (`id`, `id_alimento`, `id_ingrediente`, `id_unidad_medida`, `activo`, `id_create`, `date_create`, `id_update`, `date_update`) VALUES
(103, 52, 30, 4, 1, 57, '2021-01-21 20:58:27', NULL, NULL),
(104, 53, 31, 7, 1, 57, '2021-01-21 21:05:31', NULL, NULL),
(105, 54, 29, 4, 1, 57, '2021-01-21 22:32:01', NULL, NULL),
(106, 55, 30, 4, 1, 57, '2021-01-21 22:32:14', NULL, NULL),
(107, 56, 29, 4, 1, 57, '2021-01-21 23:10:27', NULL, NULL),
(108, 56, 32, 7, 1, 57, '2021-01-21 23:10:27', NULL, NULL),
(109, 58, 30, 4, 1, 57, '2021-01-21 23:12:09', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mn_alimento_presentacion`
--

CREATE TABLE `mn_alimento_presentacion` (
  `id` int NOT NULL,
  `id_alimento` int NOT NULL DEFAULT '0',
  `id_tipo_presentacion` int NOT NULL,
  `precio` decimal(10,2) NOT NULL DEFAULT '0.00',
  `nro_pieza` int NOT NULL DEFAULT '0',
  `id_unidad_medida` int DEFAULT NULL,
  `orden` int NOT NULL DEFAULT '0',
  `activo` int NOT NULL DEFAULT '1',
  `id_create` int NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_update` int DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `mn_alimento_presentacion`
--

INSERT INTO `mn_alimento_presentacion` (`id`, `id_alimento`, `id_tipo_presentacion`, `precio`, `nro_pieza`, `id_unidad_medida`, `orden`, `activo`, `id_create`, `date_create`, `id_update`, `date_update`) VALUES
(68, 52, 1, '5.00', 1, 1, 1, 1, 57, '2021-01-21 20:58:28', NULL, NULL),
(69, 53, 1, '0.11', 1, 1, 1, 1, 57, '2021-01-21 21:05:31', NULL, NULL),
(70, 54, 1, '1.00', 1, 1, 1, 1, 57, '2021-01-21 22:32:01', NULL, NULL),
(71, 55, 1, '2.00', 1, 1, 1, 1, 57, '2021-01-21 22:32:14', NULL, NULL),
(72, 56, 1, '20.00', 6, 2, 1, 1, 57, '2021-01-21 23:18:30', NULL, NULL),
(73, 56, 4, '38.00', 12, 2, 2, 1, 57, '2021-01-21 23:18:51', NULL, NULL),
(74, 57, 1, '1.20', 1, 5, 1, 1, 57, '2021-01-21 23:20:20', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mn_alimento_presentacion_ingrediente`
--

CREATE TABLE `mn_alimento_presentacion_ingrediente` (
  `id` int NOT NULL,
  `id_alimento_presentacion` int NOT NULL,
  `id_ingrediente` int NOT NULL,
  `cantidad` decimal(10,2) NOT NULL DEFAULT '0.00',
  `activo` int NOT NULL DEFAULT '1',
  `id_create` int NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_update` int DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `mn_alimento_presentacion_ingrediente`
--

INSERT INTO `mn_alimento_presentacion_ingrediente` (`id`, `id_alimento_presentacion`, `id_ingrediente`, `cantidad`, `activo`, `id_create`, `date_create`, `id_update`, `date_update`) VALUES
(93, 68, 30, '20.00', 1, 57, '2021-01-21 20:58:28', NULL, NULL),
(94, 69, 31, '0.11', 1, 57, '2021-01-21 21:05:31', NULL, NULL),
(95, 70, 29, '20.00', 1, 57, '2021-01-21 22:32:01', NULL, NULL),
(96, 71, 30, '30.00', 1, 57, '2021-01-21 22:32:14', NULL, NULL),
(97, 72, 29, '0.00', 1, 57, '2021-01-21 23:18:30', NULL, NULL),
(98, 72, 32, '0.00', 1, 57, '2021-01-21 23:18:30', NULL, NULL),
(99, 73, 29, '0.00', 1, 57, '2021-01-21 23:18:51', NULL, NULL),
(100, 73, 32, '0.00', 1, 57, '2021-01-21 23:18:51', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mn_categoria_menu`
--

CREATE TABLE `mn_categoria_menu` (
  `id` int NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `nombre_menu` varchar(20) DEFAULT NULL,
  `mostrar_menu` int NOT NULL DEFAULT '1',
  `administrador` int NOT NULL DEFAULT '0',
  `id_empresa` int NOT NULL DEFAULT '0',
  `id_sucursal` int NOT NULL DEFAULT '0',
  `no_modificar` int NOT NULL DEFAULT '0',
  `orden` int NOT NULL DEFAULT '0',
  `activo` int NOT NULL DEFAULT '1',
  `id_create` int NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_update` int DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `mn_categoria_menu`
--

INSERT INTO `mn_categoria_menu` (`id`, `nombre`, `nombre_menu`, `mostrar_menu`, `administrador`, `id_empresa`, `id_sucursal`, `no_modificar`, `orden`, `activo`, `id_create`, `date_create`, `id_update`, `date_update`) VALUES
(1, 'Sabor Sushi', 'SABOR SUSHI', 1, 1, 0, 0, 1, 1, 1, 3, '2020-12-14 01:00:43', NULL, NULL),
(2, 'Ingredientes adicionales', 'INGRED. ADICIONAL', 0, 1, 0, 0, 1, 5, 1, 3, '2020-12-14 01:02:17', NULL, NULL),
(4, 'Bebidas', 'BEBIDAS', 1, 1, 0, 0, 1, 2, 1, 1, '2020-12-14 01:06:55', NULL, NULL),
(14, 'Ensalada', 'Ensalada', 1, 3, 2, 6, 0, 3, 1, 57, '2021-01-21 23:02:19', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mn_ingrediente`
--

CREATE TABLE `mn_ingrediente` (
  `id` int NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `id_unidad_medida` int NOT NULL,
  `administrador` int NOT NULL DEFAULT '0',
  `id_empresa` int NOT NULL DEFAULT '0',
  `id_sucursal` int NOT NULL DEFAULT '0',
  `activo` int NOT NULL DEFAULT '1',
  `id_create` int NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_update` int DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `mn_ingrediente`
--

INSERT INTO `mn_ingrediente` (`id`, `nombre`, `id_unidad_medida`, `administrador`, `id_empresa`, `id_sucursal`, `activo`, `id_create`, `date_create`, `id_update`, `date_update`) VALUES
(29, 'Arroz', 4, 2, 2, 6, 1, 57, '2021-01-21 20:55:27', 57, '2021-01-22 17:56:51'),
(30, 'Tajada', 4, 2, 2, 6, 1, 57, '2021-01-21 20:56:03', NULL, NULL),
(31, 'Crema de leche', 7, 2, 2, 6, 1, 57, '2021-01-21 20:56:22', 57, '2021-01-21 20:57:38'),
(32, 'Salsa Agria', 7, 3, 2, 6, 1, 57, '2021-01-21 23:09:24', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mn_tipo_presentacion`
--

CREATE TABLE `mn_tipo_presentacion` (
  `id` int NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `administrador` int NOT NULL DEFAULT '0',
  `id_empresa` int NOT NULL DEFAULT '0',
  `id_sucursal` int NOT NULL DEFAULT '0',
  `no_modificar` int NOT NULL DEFAULT '0',
  `activo` int NOT NULL DEFAULT '1',
  `id_create` int NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_update` int DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `mn_tipo_presentacion`
--

INSERT INTO `mn_tipo_presentacion` (`id`, `nombre`, `administrador`, `id_empresa`, `id_sucursal`, `no_modificar`, `activo`, `id_create`, `date_create`, `id_update`, `date_update`) VALUES
(1, 'Normal', 1, 0, 0, 1, 1, 1, '2020-12-15 13:37:45', NULL, NULL),
(2, 'Pequeño', 1, 0, 0, 0, 1, 1, '2020-12-15 13:37:45', NULL, NULL),
(3, 'Mediano', 1, 0, 0, 0, 1, 1, '2020-12-15 13:37:58', NULL, NULL),
(4, 'Grande', 1, 0, 0, 0, 1, 1, '2020-12-15 16:17:43', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mt_categoria_producto`
--

CREATE TABLE `mt_categoria_producto` (
  `id` int NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `administrador` int NOT NULL DEFAULT '0',
  `id_empresa` int NOT NULL DEFAULT '0',
  `id_sucursal` int NOT NULL DEFAULT '0',
  `activo` int NOT NULL DEFAULT '1',
  `id_create` int NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_update` int DEFAULT NULL,
  `date_update` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `mt_categoria_producto`
--

INSERT INTO `mt_categoria_producto` (`id`, `nombre`, `administrador`, `id_empresa`, `id_sucursal`, `activo`, `id_create`, `date_create`, `id_update`, `date_update`) VALUES
(1, 'Bebida', 0, 2, 2, 1, 3, '2020-11-30 18:48:26', 1, '2021-01-21 18:34:44'),
(2, 'Proteina', 0, 2, 2, 1, 3, '2020-11-30 18:49:00', 1, '2021-01-21 18:37:10'),
(3, 'Legumbre', 3, 0, 0, 1, 44, '2021-01-10 03:26:23', NULL, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mt_condicion_pago`
--

CREATE TABLE `mt_condicion_pago` (
  `id` int NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `activo` int NOT NULL DEFAULT '1',
  `id_create` int NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_update` int DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mt_producto`
--

CREATE TABLE `mt_producto` (
  `id` int NOT NULL,
  `cod_producto` varchar(100) DEFAULT NULL,
  `nombre` varchar(200) NOT NULL,
  `id_categoria_producto` int NOT NULL,
  `id_unidad_medida` int NOT NULL,
  `administrador` int NOT NULL DEFAULT '0',
  `id_empresa` int NOT NULL DEFAULT '0',
  `id_sucursal` int NOT NULL DEFAULT '0',
  `activo` int NOT NULL DEFAULT '1',
  `id_create` int NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_update` int DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mt_proveedor`
--

CREATE TABLE `mt_proveedor` (
  `id` int NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `codigo` varchar(100) DEFAULT NULL,
  `telefono_local` varchar(100) DEFAULT NULL,
  `telefono_celular` varchar(100) DEFAULT NULL,
  `direccion` text,
  `contacto` varchar(200) NOT NULL,
  `correo` varchar(200) DEFAULT NULL,
  `administrador` int NOT NULL DEFAULT '0',
  `id_empresa` int NOT NULL DEFAULT '0',
  `id_sucursal` int NOT NULL DEFAULT '0',
  `activo` int NOT NULL DEFAULT '1',
  `id_create` int NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_update` int DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `mt_proveedor`
--

INSERT INTO `mt_proveedor` (`id`, `nombre`, `codigo`, `telefono_local`, `telefono_celular`, `direccion`, `contacto`, `correo`, `administrador`, `id_empresa`, `id_sucursal`, `activo`, `id_create`, `date_create`, `id_update`, `date_update`) VALUES
(3, 'Proveedor chile01', '12345678-0', '', '', '', '', '', 2, 2, 0, 1, 56, '2021-01-21 19:49:57', 57, '2021-01-22 17:56:24');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mt_tipo_dinero`
--

CREATE TABLE `mt_tipo_dinero` (
  `id` int NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `administrador` int NOT NULL DEFAULT '0',
  `id_empresa` int NOT NULL DEFAULT '0',
  `id_sucursal` int NOT NULL DEFAULT '0',
  `activo` int NOT NULL DEFAULT '1',
  `id_create` int NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_update` int DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `mt_tipo_dinero`
--

INSERT INTO `mt_tipo_dinero` (`id`, `nombre`, `administrador`, `id_empresa`, `id_sucursal`, `activo`, `id_create`, `date_create`, `id_update`, `date_update`) VALUES
(1, 'Peso', 1, 0, 0, 1, 3, '2020-12-02 01:36:04', 1, '2021-01-21 18:44:44'),
(2, 'Dolar', 1, 0, 0, 1, 3, '2020-12-02 01:36:24', 1, '2021-01-21 18:44:29'),
(3, 'Tarjeta débito', 1, 0, 0, 1, 3, '2020-12-02 01:36:43', 1, '2021-01-21 18:45:37'),
(4, 'Tarjeta crédito', 1, 0, 0, 1, 1, '2020-12-03 17:44:00', 1, '2021-01-21 18:45:12'),
(5, 'Vuelto', 1, 0, 0, 1, 1, '2020-12-03 17:44:00', 1, '2021-01-21 18:46:20'),
(6, 'Transferencia', 1, 0, 0, 1, 1, '2020-12-03 23:38:21', 1, '2021-01-21 18:45:57');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mt_tipo_movimiento_caja`
--

CREATE TABLE `mt_tipo_movimiento_caja` (
  `id` int NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `id_tipo_operacion` int NOT NULL,
  `no_modificar` int NOT NULL DEFAULT '0',
  `activo` int NOT NULL DEFAULT '1',
  `id_create` int NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_update` int DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `mt_tipo_movimiento_caja`
--

INSERT INTO `mt_tipo_movimiento_caja` (`id`, `nombre`, `id_tipo_operacion`, `no_modificar`, `activo`, `id_create`, `date_create`, `id_update`, `date_update`) VALUES
(1, 'Apertura caja', 1, 1, 1, 3, '2020-12-02 01:31:38', NULL, NULL),
(2, 'Pago servicio', 2, 0, 1, 3, '2020-12-02 01:31:54', 3, '2020-12-05 13:51:13'),
(8, 'Devolución pago', 1, 0, 1, 3, '2020-12-05 13:50:53', NULL, NULL),
(9, 'Devolución cobro', 2, 0, 1, 3, '2020-12-05 13:52:05', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mt_unidad_medida`
--

CREATE TABLE `mt_unidad_medida` (
  `id` int NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `sigla` varchar(100) DEFAULT NULL,
  `presentacion` int NOT NULL DEFAULT '0',
  `no_modificar` int NOT NULL DEFAULT '0',
  `administrador` int NOT NULL DEFAULT '0',
  `id_empresa` int NOT NULL DEFAULT '0',
  `id_sucursal` int NOT NULL DEFAULT '0',
  `activo` int NOT NULL DEFAULT '1',
  `id_create` int NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_update` int DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `mt_unidad_medida`
--

INSERT INTO `mt_unidad_medida` (`id`, `nombre`, `sigla`, `presentacion`, `no_modificar`, `administrador`, `id_empresa`, `id_sucursal`, `activo`, `id_create`, `date_create`, `id_update`, `date_update`) VALUES
(1, 'Porción', 'PRC', 1, 1, 1, 0, 0, 1, 1, '2020-11-30 18:59:08', 1, '2020-11-30 18:59:40'),
(2, 'Pieza', 'PZA', 1, 1, 1, 0, 0, 1, 1, '2020-11-30 18:59:21', 1, '2020-11-30 18:59:33'),
(3, 'Milimetro', 'ML', 0, 0, 1, 0, 0, 1, 1, '2020-12-19 13:10:45', NULL, NULL),
(4, 'Gramo', 'GRAMO', 0, 0, 1, 0, 0, 1, 1, '2020-12-19 13:12:00', 1, '2021-01-21 18:41:55'),
(5, 'Unidad', NULL, 1, 1, 1, 0, 0, 1, 1, '2021-01-21 18:39:22', NULL, NULL),
(6, 'Vaso', NULL, 0, 0, 2, 2, 0, 1, 56, '2021-01-21 19:43:41', NULL, NULL),
(7, 'Mililitro', NULL, 0, 0, 2, 2, 0, 1, 57, '2021-01-21 20:57:10', NULL, NULL),
(8, 'Taza', NULL, 0, 0, 1, 0, 0, 1, 1, '2021-01-22 18:47:25', NULL, NULL),
(10, 'Cucharilla', NULL, 0, 0, 2, 2, 2, 1, 56, '2021-01-22 19:06:51', NULL, NULL),
(11, 'litro', NULL, 0, 0, 3, 2, 6, 1, 57, '2021-01-22 19:13:29', 57, '2021-01-22 19:14:34');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sg_modulo`
--

CREATE TABLE `sg_modulo` (
  `id` int NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `identificador` int DEFAULT NULL,
  `orden` int NOT NULL DEFAULT '0',
  `icono_fas_fa` varchar(100) NOT NULL,
  `url_modulo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `activo` tinyint NOT NULL DEFAULT '1',
  `id_create` int NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_update` int DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sg_modulo`
--

INSERT INTO `sg_modulo` (`id`, `nombre`, `identificador`, `orden`, `icono_fas_fa`, `url_modulo`, `activo`, `id_create`, `date_create`, `id_update`, `date_update`) VALUES
(1, 'Configuración Sistema', 1, 3, 'fas fa-user-cog', '', 1, 1, '2020-11-14 12:52:46', NULL, NULL),
(2, 'Administrador Sistema', 4, 1, 'fas fa-chakboard-teacher', '', 0, 1, '2020-11-14 12:55:17', 3, '2020-11-23 15:19:56'),
(3, 'Seguridad Sistema', 3, 3, 'fas fa-user-lock', '', 1, 1, '2020-11-14 12:55:17', NULL, NULL),
(4, 'Estructura Sistema', 4, 2, 'fas fa-building', '', 1, 1, '2020-11-14 12:57:21', NULL, NULL),
(5, 'Mantenimiento', 5, 5, ' fab fa-medium', NULL, 1, 1, '2020-11-16 15:31:20', 3, '2020-11-23 15:19:19'),
(6, 'Gestion Empresa', 6, 6, 'fas fa-industry', NULL, 1, 1, '2020-11-17 13:22:16', NULL, NULL),
(7, 'Inventario', 7, 7, 'fas fa-barcode', NULL, 1, 3, '2020-11-18 01:56:48', 1, '2021-01-12 14:32:43'),
(8, 'Compra', 8, 8, ' fas fa-copyright', NULL, 1, 3, '2020-11-21 18:52:56', NULL, NULL),
(9, 'Administración Sucursal', 9, 4, 'fab fa-buysellads', NULL, 1, 3, '2020-11-23 15:15:32', 3, '2020-11-23 15:20:57'),
(10, 'Caja', 10, 10, 'fas fa-money-check-alt', NULL, 1, 3, '2020-11-25 14:09:58', NULL, NULL),
(11, 'Administrador Menú', 11, 11, 'fas fa-fish', NULL, 1, 3, '2020-12-09 15:22:50', 3, '2020-12-09 15:24:25'),
(12, 'Despacho', 12, 12, 'fas fa-concierge-bell', NULL, 1, 3, '2020-12-16 15:39:50', 1, '2021-01-12 14:30:02'),
(13, 'Contenedor', 13, 13, 'fas fa-archive', NULL, 1, 3, '2020-12-17 00:54:20', 1, '2021-01-12 14:30:24'),
(14, 'Historico Facturación', 14, 1, 'fas fa-newspaper', NULL, 1, 1, '2021-01-12 14:32:16', 1, '2021-01-12 14:34:03');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sg_permiso`
--

CREATE TABLE `sg_permiso` (
  `id` int NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `orden` int NOT NULL DEFAULT '0',
  `icono` varchar(100) DEFAULT NULL,
  `accion` int NOT NULL,
  `id_create` int NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_update` int DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL,
  `activo` tinyint NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sg_permiso`
--

INSERT INTO `sg_permiso` (`id`, `nombre`, `orden`, `icono`, `accion`, `id_create`, `date_create`, `id_update`, `date_update`, `activo`) VALUES
(1, 'LECTURA', 1, NULL, 1, 1, '2020-11-14 13:05:32', NULL, NULL, 1),
(2, 'ESCRITURA', 2, NULL, 2, 1, '2020-11-14 13:06:24', NULL, NULL, 1),
(3, 'MODIFICAR', 3, NULL, 3, 1, '2020-11-14 13:06:24', NULL, NULL, 1),
(4, 'ELIMINAR', 4, NULL, 4, 1, '2020-11-14 13:07:13', NULL, NULL, 1),
(5, 'REACTIVAR', 5, NULL, 5, 1, '2020-11-14 13:07:56', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sg_rol`
--

CREATE TABLE `sg_rol` (
  `id` int NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `administrador` int DEFAULT '0',
  `id_empresa` int DEFAULT '0',
  `id_sucursal` int NOT NULL DEFAULT '0',
  `no_cambiar` int DEFAULT '0',
  `activo` tinyint NOT NULL DEFAULT '1',
  `id_create` int NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_update` int DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sg_rol`
--

INSERT INTO `sg_rol` (`id`, `nombre`, `administrador`, `id_empresa`, `id_sucursal`, `no_cambiar`, `activo`, `id_create`, `date_create`, `id_update`, `date_update`) VALUES
(1, 'ADMINISTRADOR DEL SISTEMA.', 1, 1, 1, 1, 1, 1, '2020-11-14 13:42:08', NULL, NULL),
(2, 'ADMINISTRADOR EMPRESA.', 1, 1, 1, 1, 1, 1, '2020-11-14 13:42:08', NULL, NULL),
(3, 'GERENTE DE SUCURSAL.', 1, 1, 1, 1, 1, 1, '2020-11-21 20:52:11', NULL, NULL),
(6, 'EMPLEADO SUCURSAL', 1, 1, 1, 0, 1, 1, '2021-01-07 23:09:24', NULL, NULL),
(7, 'Cocinero', 3, 2, 6, 0, 1, 57, '2021-01-22 21:39:14', NULL, NULL),
(8, 'Sub Gerente', 2, 2, 2, 0, 1, 56, '2021-01-22 21:45:09', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sg_rol_modulo`
--

CREATE TABLE `sg_rol_modulo` (
  `id` int NOT NULL,
  `id_rol` int NOT NULL,
  `id_modulo` int NOT NULL,
  `id_empresa` int NOT NULL DEFAULT '0',
  `id_sucursal` int NOT NULL DEFAULT '0',
  `activo` smallint NOT NULL DEFAULT '1',
  `id_create` int NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_update` int DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sg_rol_modulo`
--

INSERT INTO `sg_rol_modulo` (`id`, `id_rol`, `id_modulo`, `id_empresa`, `id_sucursal`, `activo`, `id_create`, `date_create`, `id_update`, `date_update`) VALUES
(61, 1, 4, 1, 1, 1, 3, '2020-11-29 19:52:21', NULL, NULL),
(62, 1, 3, 1, 1, 1, 3, '2020-11-29 19:52:41', NULL, NULL),
(63, 1, 1, 1, 1, 1, 3, '2020-11-29 19:53:04', NULL, NULL),
(81, 1, 2, 1, 1, 1, 1, '2021-01-04 17:53:12', NULL, NULL),
(82, 1, 5, 1, 1, 1, 1, '2021-01-04 20:53:27', NULL, NULL),
(84, 5, 9, 0, 0, 1, 3, '2021-01-05 18:02:33', NULL, NULL),
(85, 2, 3, 0, 0, 1, 1, '2021-01-05 21:30:49', NULL, NULL),
(86, 2, 5, 0, 0, 1, 1, '2021-01-06 00:08:07', NULL, NULL),
(88, 2, 1, 0, 0, 1, 1, '2021-01-06 00:16:59', NULL, NULL),
(89, 2, 7, 0, 0, 1, 1, '2021-01-06 00:18:28', NULL, NULL),
(92, 3, 11, 0, 0, 1, 1, '2021-01-06 05:05:37', NULL, NULL),
(93, 3, 9, 0, 0, 1, 1, '2021-01-06 12:23:26', NULL, NULL),
(94, 3, 10, 0, 0, 1, 1, '2021-01-06 12:23:57', NULL, NULL),
(95, 3, 8, 0, 0, 1, 1, '2021-01-06 12:24:16', NULL, NULL),
(96, 3, 13, 0, 0, 1, 1, '2021-01-06 12:24:59', NULL, NULL),
(97, 3, 12, 0, 0, 1, 1, '2021-01-06 12:25:55', NULL, NULL),
(98, 3, 7, 0, 0, 1, 1, '2021-01-06 12:26:33', NULL, NULL),
(99, 3, 5, 0, 0, 1, 1, '2021-01-06 12:27:10', NULL, NULL),
(101, 3, 3, 0, 0, 1, 1, '2021-01-07 14:47:41', NULL, NULL),
(102, 6, 10, 0, 0, 1, 1, '2021-01-07 23:09:57', NULL, NULL),
(103, 6, 12, 0, 0, 1, 1, '2021-01-07 23:10:18', NULL, NULL),
(105, 2, 14, 0, 0, 1, 1, '2021-01-12 14:34:32', NULL, NULL),
(106, 3, 14, 0, 0, 1, 1, '2021-01-14 14:10:58', NULL, NULL),
(107, 7, 5, 0, 0, 1, 57, '2021-01-22 21:39:40', NULL, NULL),
(108, 8, 5, 0, 0, 1, 56, '2021-01-22 21:45:56', NULL, NULL),
(109, 8, 10, 0, 0, 1, 56, '2021-01-22 21:46:10', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sg_rol_modulo_permiso`
--

CREATE TABLE `sg_rol_modulo_permiso` (
  `id` int NOT NULL,
  `id_rol_modulo` int NOT NULL,
  `id_permiso` int NOT NULL,
  `id_modulo` int NOT NULL,
  `activo` tinyint NOT NULL DEFAULT '1',
  `id_create` int NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_update` int DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sg_rol_modulo_permiso`
--

INSERT INTO `sg_rol_modulo_permiso` (`id`, `id_rol_modulo`, `id_permiso`, `id_modulo`, `activo`, `id_create`, `date_create`, `id_update`, `date_update`) VALUES
(135, 61, 1, 4, 1, 3, '2020-11-29 19:52:21', NULL, NULL),
(136, 61, 2, 4, 1, 3, '2020-11-29 19:52:22', NULL, NULL),
(137, 61, 3, 4, 1, 3, '2020-11-29 19:52:22', NULL, NULL),
(138, 61, 4, 4, 1, 3, '2020-11-29 19:52:22', NULL, NULL),
(139, 61, 5, 4, 1, 3, '2020-11-29 19:52:22', NULL, NULL),
(140, 62, 1, 3, 1, 3, '2020-11-29 19:52:42', NULL, NULL),
(141, 62, 2, 3, 1, 3, '2020-11-29 19:52:42', NULL, NULL),
(142, 62, 3, 3, 1, 3, '2020-11-29 19:52:42', NULL, NULL),
(143, 62, 4, 3, 1, 3, '2020-11-29 19:52:42', NULL, NULL),
(144, 62, 5, 3, 1, 3, '2020-11-29 19:52:42', NULL, NULL),
(145, 63, 1, 1, 1, 3, '2020-11-29 19:53:05', NULL, NULL),
(146, 63, 2, 1, 1, 3, '2020-11-29 19:53:05', NULL, NULL),
(147, 63, 3, 1, 1, 3, '2020-11-29 19:53:05', NULL, NULL),
(148, 63, 4, 1, 1, 3, '2020-11-29 19:53:06', NULL, NULL),
(149, 63, 5, 1, 1, 3, '2020-11-29 19:53:06', NULL, NULL),
(165, 71, 1, 2, 1, 3, '2020-12-16 17:18:20', NULL, NULL),
(186, 81, 1, 2, 1, 1, '2021-01-04 17:53:12', NULL, NULL),
(187, 81, 2, 2, 1, 1, '2021-01-04 17:53:12', NULL, NULL),
(188, 81, 3, 2, 1, 1, '2021-01-04 17:53:12', NULL, NULL),
(189, 81, 4, 2, 1, 1, '2021-01-04 17:53:12', NULL, NULL),
(190, 81, 5, 2, 1, 1, '2021-01-04 17:53:12', NULL, NULL),
(191, 82, 1, 5, 1, 1, '2021-01-04 20:53:28', NULL, NULL),
(192, 82, 2, 5, 1, 1, '2021-01-04 20:53:29', NULL, NULL),
(193, 82, 3, 5, 1, 1, '2021-01-04 20:53:29', NULL, NULL),
(194, 82, 4, 5, 1, 1, '2021-01-04 20:53:29', NULL, NULL),
(195, 82, 5, 5, 1, 1, '2021-01-04 20:53:29', NULL, NULL),
(201, 84, 1, 9, 1, 3, '2021-01-05 18:02:34', NULL, NULL),
(202, 84, 2, 9, 1, 3, '2021-01-05 18:02:34', NULL, NULL),
(203, 84, 3, 9, 1, 3, '2021-01-05 18:02:34', NULL, NULL),
(204, 84, 4, 9, 1, 3, '2021-01-05 18:02:35', NULL, NULL),
(205, 84, 5, 9, 1, 3, '2021-01-05 18:02:35', NULL, NULL),
(206, 85, 1, 3, 1, 1, '2021-01-05 21:30:50', NULL, NULL),
(207, 85, 2, 3, 1, 1, '2021-01-05 21:30:50', NULL, NULL),
(208, 85, 3, 3, 1, 1, '2021-01-05 21:30:50', NULL, NULL),
(209, 85, 4, 3, 1, 1, '2021-01-05 21:30:50', NULL, NULL),
(210, 85, 5, 3, 1, 1, '2021-01-05 21:30:51', NULL, NULL),
(211, 86, 1, 5, 1, 1, '2021-01-06 00:08:08', NULL, NULL),
(212, 86, 2, 5, 1, 1, '2021-01-06 00:08:08', NULL, NULL),
(213, 86, 3, 5, 1, 1, '2021-01-06 00:08:08', NULL, NULL),
(214, 86, 4, 5, 1, 1, '2021-01-06 00:08:08', NULL, NULL),
(215, 86, 5, 5, 1, 1, '2021-01-06 00:08:08', NULL, NULL),
(221, 88, 1, 1, 1, 1, '2021-01-06 00:16:59', NULL, NULL),
(222, 88, 2, 1, 1, 1, '2021-01-06 00:17:00', NULL, NULL),
(223, 88, 3, 1, 1, 1, '2021-01-06 00:17:00', NULL, NULL),
(224, 88, 4, 1, 1, 1, '2021-01-06 00:17:00', NULL, NULL),
(225, 88, 5, 1, 1, 1, '2021-01-06 00:17:00', NULL, NULL),
(226, 89, 1, 7, 1, 1, '2021-01-06 00:18:28', NULL, NULL),
(227, 89, 2, 7, 1, 1, '2021-01-06 00:18:29', NULL, NULL),
(228, 89, 3, 7, 1, 1, '2021-01-06 00:18:29', NULL, NULL),
(229, 89, 4, 7, 1, 1, '2021-01-06 00:18:29', NULL, NULL),
(230, 89, 5, 7, 1, 1, '2021-01-06 00:18:29', NULL, NULL),
(241, 92, 1, 11, 1, 1, '2021-01-06 05:05:38', NULL, NULL),
(242, 92, 2, 11, 1, 1, '2021-01-06 05:05:39', NULL, NULL),
(243, 92, 3, 11, 1, 1, '2021-01-06 05:05:39', NULL, NULL),
(244, 92, 4, 11, 1, 1, '2021-01-06 05:05:39', NULL, NULL),
(245, 92, 5, 11, 1, 1, '2021-01-06 05:05:39', NULL, NULL),
(246, 93, 1, 9, 1, 1, '2021-01-06 12:23:27', NULL, NULL),
(247, 93, 2, 9, 1, 1, '2021-01-06 12:23:27', NULL, NULL),
(248, 93, 3, 9, 1, 1, '2021-01-06 12:23:27', NULL, NULL),
(249, 93, 4, 9, 1, 1, '2021-01-06 12:23:27', NULL, NULL),
(250, 93, 5, 9, 1, 1, '2021-01-06 12:23:27', NULL, NULL),
(251, 94, 1, 10, 1, 1, '2021-01-06 12:23:58', NULL, NULL),
(252, 94, 2, 10, 1, 1, '2021-01-06 12:23:58', NULL, NULL),
(253, 94, 3, 10, 1, 1, '2021-01-06 12:23:58', NULL, NULL),
(254, 94, 4, 10, 1, 1, '2021-01-06 12:23:58', NULL, NULL),
(255, 94, 5, 10, 1, 1, '2021-01-06 12:23:58', NULL, NULL),
(256, 95, 1, 8, 1, 1, '2021-01-06 12:24:16', NULL, NULL),
(257, 95, 2, 8, 1, 1, '2021-01-06 12:24:16', NULL, NULL),
(258, 95, 3, 8, 1, 1, '2021-01-06 12:24:16', NULL, NULL),
(259, 95, 4, 8, 1, 1, '2021-01-06 12:24:16', NULL, NULL),
(260, 95, 5, 8, 1, 1, '2021-01-06 12:24:17', NULL, NULL),
(261, 96, 1, 13, 1, 1, '2021-01-06 12:24:59', NULL, NULL),
(262, 96, 2, 13, 1, 1, '2021-01-06 12:24:59', NULL, NULL),
(263, 96, 3, 13, 1, 1, '2021-01-06 12:25:00', NULL, NULL),
(264, 96, 4, 13, 1, 1, '2021-01-06 12:25:00', NULL, NULL),
(265, 96, 5, 13, 1, 1, '2021-01-06 12:25:00', NULL, NULL),
(266, 97, 1, 12, 1, 1, '2021-01-06 12:25:55', NULL, NULL),
(267, 97, 2, 12, 1, 1, '2021-01-06 12:25:55', NULL, NULL),
(268, 97, 3, 12, 1, 1, '2021-01-06 12:25:55', NULL, NULL),
(269, 97, 4, 12, 1, 1, '2021-01-06 12:25:56', NULL, NULL),
(270, 97, 5, 12, 1, 1, '2021-01-06 12:25:56', NULL, NULL),
(271, 98, 1, 7, 1, 1, '2021-01-06 12:26:33', NULL, NULL),
(272, 98, 2, 7, 1, 1, '2021-01-06 12:26:33', NULL, NULL),
(273, 98, 3, 7, 1, 1, '2021-01-06 12:26:34', NULL, NULL),
(274, 98, 4, 7, 1, 1, '2021-01-06 12:26:34', NULL, NULL),
(275, 98, 5, 7, 1, 1, '2021-01-06 12:26:34', NULL, NULL),
(276, 99, 1, 5, 1, 1, '2021-01-06 12:27:11', NULL, NULL),
(277, 99, 2, 5, 1, 1, '2021-01-06 12:27:11', NULL, NULL),
(278, 99, 3, 5, 1, 1, '2021-01-06 12:27:11', NULL, NULL),
(279, 99, 4, 5, 1, 1, '2021-01-06 12:27:11', NULL, NULL),
(280, 99, 5, 5, 1, 1, '2021-01-06 12:27:11', NULL, NULL),
(286, 101, 1, 3, 1, 1, '2021-01-07 14:47:42', NULL, NULL),
(287, 101, 2, 3, 1, 1, '2021-01-07 14:47:42', NULL, NULL),
(288, 101, 3, 3, 1, 1, '2021-01-07 14:47:42', NULL, NULL),
(289, 101, 4, 3, 1, 1, '2021-01-07 14:47:42', NULL, NULL),
(290, 101, 5, 3, 1, 1, '2021-01-07 14:47:42', NULL, NULL),
(291, 102, 1, 10, 1, 1, '2021-01-07 23:09:58', NULL, NULL),
(292, 102, 2, 10, 1, 1, '2021-01-07 23:09:58', NULL, NULL),
(293, 102, 3, 10, 1, 1, '2021-01-07 23:09:58', NULL, NULL),
(294, 102, 4, 10, 1, 1, '2021-01-07 23:09:58', NULL, NULL),
(295, 102, 5, 10, 1, 1, '2021-01-07 23:09:58', NULL, NULL),
(296, 103, 1, 12, 1, 1, '2021-01-07 23:10:19', NULL, NULL),
(297, 103, 2, 12, 1, 1, '2021-01-07 23:10:19', NULL, NULL),
(298, 103, 3, 12, 1, 1, '2021-01-07 23:10:19', NULL, NULL),
(299, 103, 4, 12, 1, 1, '2021-01-07 23:10:19', NULL, NULL),
(300, 103, 5, 12, 1, 1, '2021-01-07 23:10:19', NULL, NULL),
(306, 105, 1, 14, 1, 1, '2021-01-12 14:34:32', NULL, NULL),
(307, 105, 2, 14, 1, 1, '2021-01-12 14:34:33', NULL, NULL),
(308, 105, 3, 14, 1, 1, '2021-01-12 14:34:33', NULL, NULL),
(309, 105, 4, 14, 1, 1, '2021-01-12 14:34:33', NULL, NULL),
(310, 105, 5, 14, 1, 1, '2021-01-12 14:34:33', NULL, NULL),
(311, 106, 1, 14, 1, 1, '2021-01-14 14:10:59', NULL, NULL),
(312, 106, 2, 14, 1, 1, '2021-01-14 14:10:59', NULL, NULL),
(313, 106, 3, 14, 1, 1, '2021-01-14 14:10:59', NULL, NULL),
(314, 106, 4, 14, 1, 1, '2021-01-14 14:11:00', NULL, NULL),
(315, 106, 5, 14, 1, 1, '2021-01-14 14:11:00', NULL, NULL),
(316, 107, 1, 5, 1, 57, '2021-01-22 21:39:41', NULL, NULL),
(317, 107, 2, 5, 1, 57, '2021-01-22 21:39:41', NULL, NULL),
(318, 107, 3, 5, 1, 57, '2021-01-22 21:39:41', NULL, NULL),
(319, 107, 4, 5, 1, 57, '2021-01-22 21:39:42', NULL, NULL),
(320, 107, 5, 5, 1, 57, '2021-01-22 21:39:42', NULL, NULL),
(321, 108, 1, 5, 1, 56, '2021-01-22 21:45:58', NULL, NULL),
(322, 108, 2, 5, 1, 56, '2021-01-22 21:45:58', NULL, NULL),
(323, 108, 3, 5, 1, 56, '2021-01-22 21:45:58', NULL, NULL),
(324, 108, 4, 5, 1, 56, '2021-01-22 21:45:58', NULL, NULL),
(325, 108, 5, 5, 1, 56, '2021-01-22 21:45:58', NULL, NULL),
(326, 109, 1, 10, 1, 56, '2021-01-22 21:46:11', NULL, NULL),
(327, 109, 2, 10, 1, 56, '2021-01-22 21:46:11', NULL, NULL),
(328, 109, 3, 10, 1, 56, '2021-01-22 21:46:11', NULL, NULL),
(329, 109, 4, 10, 1, 56, '2021-01-22 21:46:11', NULL, NULL),
(330, 109, 5, 10, 1, 56, '2021-01-22 21:46:12', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sg_rol_modulo_proceso`
--

CREATE TABLE `sg_rol_modulo_proceso` (
  `id` int NOT NULL,
  `id_rol_modulo` int NOT NULL,
  `id_sub_modulo` int NOT NULL,
  `id_modulo` int NOT NULL,
  `activo` int NOT NULL DEFAULT '1',
  `id_create` int NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_update` int DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sg_rol_modulo_proceso`
--

INSERT INTO `sg_rol_modulo_proceso` (`id`, `id_rol_modulo`, `id_sub_modulo`, `id_modulo`, `activo`, `id_create`, `date_create`, `id_update`, `date_update`) VALUES
(6, 79, 37, 13, 1, 3, '2020-12-17 00:55:47', NULL, NULL),
(7, 80, 14, 7, 1, 1, '2021-01-04 17:52:01', NULL, NULL),
(8, 80, 15, 7, 1, 1, '2021-01-04 17:52:01', NULL, NULL),
(9, 80, 29, 7, 1, 1, '2021-01-04 17:52:01', NULL, NULL),
(10, 61, 1, 4, 1, 1, '2021-01-04 20:32:44', NULL, NULL),
(13, 63, 10, 1, 1, 1, '2021-01-04 20:52:17', NULL, NULL),
(14, 63, 11, 1, 1, 1, '2021-01-04 20:52:24', NULL, NULL),
(15, 61, 2, 4, 1, 1, '2021-01-04 20:52:39', NULL, NULL),
(16, 62, 5, 3, 1, 1, '2021-01-04 20:52:50', NULL, NULL),
(17, 62, 4, 3, 1, 1, '2021-01-04 20:52:55', NULL, NULL),
(18, 62, 3, 3, 1, 1, '2021-01-04 20:52:58', NULL, NULL),
(19, 62, 6, 3, 1, 1, '2021-01-04 20:53:00', NULL, NULL),
(21, 82, 8, 5, 1, 1, '2021-01-04 20:53:28', NULL, NULL),
(24, 82, 24, 5, 1, 1, '2021-01-04 20:53:28', NULL, NULL),
(25, 82, 25, 5, 1, 1, '2021-01-04 20:53:28', NULL, NULL),
(28, 84, 20, 9, 1, 3, '2021-01-05 18:02:34', NULL, NULL),
(29, 84, 23, 9, 1, 3, '2021-01-05 18:02:34', NULL, NULL),
(31, 62, 40, 3, 1, 1, '2021-01-05 20:20:27', NULL, NULL),
(32, 85, 3, 3, 1, 1, '2021-01-05 21:30:49', NULL, NULL),
(33, 85, 4, 3, 1, 1, '2021-01-05 21:30:49', NULL, NULL),
(34, 85, 5, 3, 1, 1, '2021-01-05 21:30:49', NULL, NULL),
(35, 85, 6, 3, 1, 1, '2021-01-05 21:30:50', NULL, NULL),
(37, 85, 41, 3, 1, 1, '2021-01-05 21:30:50', NULL, NULL),
(38, 82, 7, 5, 1, 1, '2021-01-05 23:56:38', NULL, NULL),
(39, 86, 7, 5, 1, 1, '2021-01-06 00:08:07', NULL, NULL),
(40, 86, 8, 5, 1, 1, '2021-01-06 00:08:07', NULL, NULL),
(41, 86, 9, 5, 1, 1, '2021-01-06 00:08:07', NULL, NULL),
(42, 86, 13, 5, 1, 1, '2021-01-06 00:08:07', NULL, NULL),
(44, 86, 25, 5, 1, 1, '2021-01-06 00:08:08', NULL, NULL),
(48, 88, 11, 1, 1, 1, '2021-01-06 00:16:59', NULL, NULL),
(49, 88, 12, 1, 1, 1, '2021-01-06 00:16:59', NULL, NULL),
(50, 89, 14, 7, 1, 1, '2021-01-06 00:18:28', NULL, NULL),
(61, 85, 42, 3, 1, 1, '2021-01-06 00:54:04', NULL, NULL),
(62, 92, 32, 11, 1, 1, '2021-01-06 05:05:38', NULL, NULL),
(63, 92, 33, 11, 1, 1, '2021-01-06 05:05:38', NULL, NULL),
(64, 92, 34, 11, 1, 1, '2021-01-06 05:05:38', NULL, NULL),
(65, 92, 35, 11, 1, 1, '2021-01-06 05:05:38', NULL, NULL),
(66, 92, 38, 11, 1, 1, '2021-01-06 05:05:38', NULL, NULL),
(67, 93, 20, 9, 1, 1, '2021-01-06 12:23:27', NULL, NULL),
(68, 93, 23, 9, 1, 1, '2021-01-06 12:23:27', NULL, NULL),
(72, 94, 30, 10, 1, 1, '2021-01-06 12:23:57', NULL, NULL),
(74, 95, 19, 8, 1, 1, '2021-01-06 12:24:16', NULL, NULL),
(75, 96, 37, 13, 1, 1, '2021-01-06 12:24:59', NULL, NULL),
(76, 97, 36, 12, 1, 1, '2021-01-06 12:25:55', NULL, NULL),
(77, 98, 14, 7, 1, 1, '2021-01-06 12:26:33', NULL, NULL),
(78, 98, 15, 7, 1, 1, '2021-01-06 12:26:33', NULL, NULL),
(79, 98, 29, 7, 1, 1, '2021-01-06 12:26:33', NULL, NULL),
(80, 99, 7, 5, 1, 1, '2021-01-06 12:27:10', NULL, NULL),
(81, 99, 8, 5, 1, 1, '2021-01-06 12:27:10', NULL, NULL),
(82, 99, 9, 5, 1, 1, '2021-01-06 12:27:10', NULL, NULL),
(83, 99, 13, 5, 1, 1, '2021-01-06 12:27:10', NULL, NULL),
(85, 99, 25, 5, 1, 1, '2021-01-06 12:27:11', NULL, NULL),
(89, 101, 3, 3, 1, 1, '2021-01-07 14:47:41', NULL, NULL),
(90, 101, 4, 3, 1, 1, '2021-01-07 14:47:41', NULL, NULL),
(91, 101, 5, 3, 1, 1, '2021-01-07 14:47:41', NULL, NULL),
(96, 101, 43, 3, 1, 1, '2021-01-07 14:47:42', NULL, NULL),
(97, 101, 6, 3, 1, 1, '2021-01-07 14:53:06', NULL, NULL),
(98, 62, 41, 3, 1, 32, '2021-01-07 19:27:30', NULL, NULL),
(99, 102, 26, 10, 1, 1, '2021-01-07 23:09:57', NULL, NULL),
(100, 102, 27, 10, 1, 1, '2021-01-07 23:09:57', NULL, NULL),
(101, 102, 28, 10, 1, 1, '2021-01-07 23:09:57', NULL, NULL),
(103, 102, 31, 10, 1, 1, '2021-01-07 23:09:58', NULL, NULL),
(104, 103, 36, 12, 1, 1, '2021-01-07 23:10:19', NULL, NULL),
(106, 93, 45, 9, 1, 1, '2021-01-10 19:43:45', NULL, NULL),
(107, 93, 44, 9, 1, 1, '2021-01-10 19:43:50', NULL, NULL),
(108, 105, 46, 14, 1, 1, '2021-01-12 14:46:19', NULL, NULL),
(109, 105, 47, 14, 1, 1, '2021-01-12 14:46:23', NULL, NULL),
(112, 106, 48, 14, 1, 1, '2021-01-14 14:10:59', NULL, NULL),
(113, 106, 49, 14, 1, 1, '2021-01-14 23:20:32', NULL, NULL),
(114, 107, 7, 5, 1, 57, '2021-01-22 21:39:40', NULL, NULL),
(116, 107, 9, 5, 1, 57, '2021-01-22 21:39:40', NULL, NULL),
(117, 107, 13, 5, 1, 57, '2021-01-22 21:39:40', NULL, NULL),
(120, 108, 7, 5, 1, 56, '2021-01-22 21:45:56', NULL, NULL),
(122, 108, 9, 5, 1, 56, '2021-01-22 21:45:57', NULL, NULL),
(123, 108, 13, 5, 1, 56, '2021-01-22 21:45:57', NULL, NULL),
(129, 109, 30, 10, 1, 56, '2021-01-22 21:46:10', NULL, NULL),
(130, 109, 31, 10, 1, 56, '2021-01-22 21:46:10', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sg_sub_modulo`
--

CREATE TABLE `sg_sub_modulo` (
  `id` int NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `id_modulo` int NOT NULL,
  `url_sub_modulo` varchar(200) NOT NULL,
  `orden` int NOT NULL DEFAULT '0',
  `activo` tinyint NOT NULL DEFAULT '1',
  `id_create` int NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_update` int DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sg_sub_modulo`
--

INSERT INTO `sg_sub_modulo` (`id`, `nombre`, `id_modulo`, `url_sub_modulo`, `orden`, `activo`, `id_create`, `date_create`, `id_update`, `date_update`) VALUES
(1, 'MODULO', 4, 'modulo', 1, 1, 1, '2020-11-14 14:30:26', NULL, NULL),
(2, 'SUBMODULO', 4, 'subModulo', 2, 1, 1, '2020-11-14 14:30:26', NULL, NULL),
(3, 'USUARIO', 3, 'usuario', 1, 1, 1, '2020-11-14 14:32:27', NULL, NULL),
(4, 'ROLES DEL SISTEMA', 3, 'rolesUsuario', 2, 1, 1, '2020-11-14 14:32:27', NULL, NULL),
(5, 'MODULO POR ROL', 3, 'rolModulo', 3, 1, 1, '2020-11-14 14:35:42', 1, '2021-01-05 05:32:30'),
(6, 'Permisologia Usuario', 3, 'usuarioEmpresa', 4, 1, 1, '2020-11-14 14:36:25', 1, '2021-01-07 14:53:58'),
(7, 'CATEGORIA DE PRODUCTO', 5, 'categoriaProducto', 1, 1, 1, '2020-11-16 15:33:18', NULL, NULL),
(8, 'UNIDAD DE MEDIDA', 5, 'unidadMedida', 2, 1, 1, '2020-11-16 16:40:12', NULL, NULL),
(9, 'Producto', 5, 'producto', 3, 1, 1, '2020-11-16 17:28:29', NULL, NULL),
(10, 'Empresa', 1, 'actEmpresa', 1, 1, 1, '2020-11-17 13:23:48', 3, '2020-11-24 23:49:08'),
(11, 'REGION', 1, 'region', 3, 1, 1, '2020-11-17 14:32:26', 1, '2020-11-17 15:29:18'),
(12, 'SUCURSAL', 1, 'sucursal', 2, 1, 1, '2020-11-17 15:28:32', 1, '2020-11-17 15:29:07'),
(13, 'Proveedor', 5, 'proveedor', 5, 1, 3, '2020-11-18 02:04:14', NULL, NULL),
(14, 'CONCEPTO INVENTARIO', 7, 'conceptoInventario', 1, 1, 3, '2020-11-18 03:42:41', 3, '2020-12-22 18:57:19'),
(15, 'Entrada inventario', 7, 'entradaInventario', 2, 0, 3, '2020-11-18 11:57:47', 3, '2020-12-16 20:20:55'),
(19, 'Registrar Factura', 8, 'actFactura', 2, 1, 3, '2020-11-23 10:17:37', NULL, NULL),
(20, 'Sala', 9, 'sala', 1, 1, 3, '2020-11-23 15:16:16', 3, '2020-11-23 15:29:24'),
(23, 'Mesa de sucursal', 9, 'mesaADM', 2, 1, 3, '2020-11-23 16:55:55', NULL, NULL),
(24, 'Tipo_dinero', 5, 'tipoDinero', 10, 1, 3, '2020-11-25 12:35:27', NULL, NULL),
(25, 'Tipo Movimiento Caja', 5, 'tipoMovimientoCaja', 11, 1, 3, '2020-11-25 13:42:08', NULL, NULL),
(26, 'Aperturar Caja', 10, 'aperturaCaja', 1, 1, 3, '2020-11-25 14:10:44', 3, '2020-11-25 16:01:03'),
(27, 'Movimiento de Caja', 10, 'movimientoCaja', 2, 1, 3, '2020-11-26 00:12:02', NULL, NULL),
(28, 'Cobrar Despacho', 10, 'cobroCaja', 3, 1, 3, '2020-11-26 17:00:35', 3, '2020-11-26 17:54:07'),
(29, 'INVENTARIO', 7, 'inventario', 2, 1, 3, '2020-11-30 17:54:19', NULL, NULL),
(30, 'Cierre de caja', 10, 'cierreCaja', 6, 1, 3, '2020-12-05 17:41:34', NULL, NULL),
(31, 'Resumen de caja', 10, 'detalleCaja', 4, 1, 3, '2020-12-09 00:01:07', NULL, NULL),
(32, 'Ingredientes', 11, 'ingrediente', 3, 1, 3, '2020-12-09 15:27:29', 3, '2020-12-13 19:05:50'),
(33, 'Act. Envoltura', 11, 'envoltura', 2, 1, 3, '2020-12-09 17:36:33', NULL, NULL),
(34, 'Act. Alimentos', 11, 'alimento', 5, 1, 3, '2020-12-10 14:56:31', NULL, NULL),
(35, 'Categoría de menú', 11, 'categoriaMenu', 1, 1, 3, '2020-12-13 19:04:56', NULL, NULL),
(36, 'Despacho a cliente', 12, 'despacho', 1, 1, 3, '2020-12-16 15:41:07', NULL, NULL),
(37, 'Act. Contenedor', 13, 'contenedor', 1, 1, 3, '2020-12-17 00:55:04', NULL, NULL),
(38, 'Ingredientes Adicionales', 11, 'ingredienteAdicional', 4, 1, 3, '2020-12-28 13:41:05', 3, '2020-12-28 13:41:29'),
(40, 'ADMINISTRADOR GENERAL', 3, 'administradorGeneral', 8, 1, 1, '2021-01-05 03:10:55', 1, '2021-01-05 03:11:45'),
(41, 'ADMINISTRADOR EMPRESA', 3, 'administradorEmpresa', 9, 1, 1, '2021-01-05 11:21:01', NULL, NULL),
(42, 'Administrador de Sucursal', 3, 'administradorSucursal', 10, 1, 1, '2021-01-06 00:53:34', NULL, NULL),
(43, 'Personal de Sucursal', 3, 'personalSucursal', 11, 1, 1, '2021-01-06 22:01:30', NULL, NULL),
(44, 'Act. Mesonero', 9, 'mesonero', 3, 1, 1, '2021-01-10 17:11:43', 1, '2021-01-10 17:12:12'),
(45, 'Act. Dolar / Impuesto', 9, 'dolarImpuesto', 4, 1, 1, '2021-01-10 19:39:49', NULL, NULL),
(46, 'Consulta Facturación', 14, 'sucursalFacturacion', 1, 1, 1, '2021-01-12 14:35:14', 1, '2021-01-12 14:44:34'),
(47, 'Totales Facturación', 14, 'totalesFacturacion', 2, 1, 1, '2021-01-12 14:45:54', NULL, NULL),
(48, 'Factura Sucursal', 14, 'consultaFactura_S', 1, 1, 1, '2021-01-14 14:01:19', 1, '2021-01-14 14:11:57'),
(49, 'Totales factura sucursal', 14, 'totalesFacturacion_S', 2, 1, 1, '2021-01-14 23:18:51', 1, '2021-01-14 23:19:27');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sg_usuario`
--

CREATE TABLE `sg_usuario` (
  `id` int NOT NULL,
  `login` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
  `password` varchar(100) NOT NULL,
  `run` varchar(20) NOT NULL DEFAULT '0',
  `nombre` varchar(100) NOT NULL,
  `apellido` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `foto` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `sexo` varchar(1) NOT NULL,
  `no_modificar` int NOT NULL DEFAULT '0',
  `administrador` int NOT NULL DEFAULT '0',
  `id_empresa` int NOT NULL DEFAULT '0',
  `id_sucursal` int NOT NULL DEFAULT '0',
  `activo` tinyint NOT NULL DEFAULT '1',
  `id_create` int NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_update` int DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sg_usuario`
--

INSERT INTO `sg_usuario` (`id`, `login`, `password`, `run`, `nombre`, `apellido`, `email`, `foto`, `sexo`, `no_modificar`, `administrador`, `id_empresa`, `id_sucursal`, `activo`, `id_create`, `date_create`, `id_update`, `date_update`) VALUES
(1, 'admin', 'e10adc3949ba59abbe56e057f20f883e', '00000000-0', 'Administrador', 'Principal', 'admin@ggg.com', '1--1--admin-mod-profile.jpg', 'M', 1, 1, 1, 1, 1, 1, '2020-12-29 01:55:32', 1, '2021-01-19 14:12:49'),
(54, '0', 'e10adc3949ba59abbe56e057f20f883e', '10000000-0', 'Administrador', 'Sushi Mar', 'adminsushimar@hhh,com', '1--1--10000000-0-mlane.jpg', 'M', 0, 1, 1, 1, 1, 1, '2021-01-21 19:12:53', NULL, NULL),
(55, '0', 'e10adc3949ba59abbe56e057f20f883e', '11111111-0', 'Administrador 2', 'Sushi Mar', 'adminsushimar2@hhh,com', '1--1--11111111-0-jm_denis.jpg', 'M', 0, 1, 1, 1, 1, 1, '2021-01-21 19:16:08', NULL, NULL),
(56, '0', 'e10adc3949ba59abbe56e057f20f883e', '20000000-0', 'Administrador', 'Sushi Chile', 'adminsushichile@yyy,com', '1--1--20000000-0-chadengle.jpg', 'M', 0, 1, 1, 1, 1, 1, '2021-01-21 19:18:03', NULL, NULL),
(57, '0', 'e10adc3949ba59abbe56e057f20f883e', '30000000-0', 'Administrador', 'Surcursal Chile', 'admsucursalchile@fff,com', '2--2--30000000-0-talha.jpg', 'M', 0, 2, 2, 2, 1, 56, '2021-01-21 19:24:26', NULL, NULL),
(58, '0', 'e10adc3949ba59abbe56e057f20f883e', '40000000-0', 'Empleado Sucursal', 'Sushi', 'empleado@zzz.com', '2--2--40000000-0-profile2.jpg', 'M', 0, 2, 2, 6, 1, 57, '2021-01-21 20:27:50', NULL, NULL),
(59, '0', 'e10adc3949ba59abbe56e057f20f883e', '60000000-0', 'Cocinero', 'Sucursal', 'cocinero@mmm.com', '2--2--60000000-0-arashmil.jpg', 'M', 0, 3, 2, 6, 1, 57, '2021-01-22 21:41:22', NULL, NULL),
(60, '0', 'e10adc3949ba59abbe56e057f20f883e', '80000000-0', 'Sub Gerente', 'Sushi Bar', '', '', 'M', 0, 2, 2, 2, 1, 56, '2021-01-22 21:48:45', NULL, NULL),
(61, '0', 'e10adc3949ba59abbe56e057f20f883e', '90000000-0', 'Usuario', '9000000', '', '', 'M', 0, 3, 2, 6, 1, 60, '2021-01-22 21:53:16', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sg_usuario_administrador`
--

CREATE TABLE `sg_usuario_administrador` (
  `id` int NOT NULL,
  `id_usuario` int NOT NULL,
  `id_rol` int NOT NULL DEFAULT '1',
  `administrador` int NOT NULL DEFAULT '0',
  `id_empresa` int NOT NULL DEFAULT '0',
  `id_sucursal` int NOT NULL DEFAULT '0',
  `activo` int NOT NULL DEFAULT '1',
  `id_empresa_create` int NOT NULL DEFAULT '0',
  `id_sucursal_create` int NOT NULL DEFAULT '0',
  `id_create` int NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_update` int DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sg_usuario_administrador`
--

INSERT INTO `sg_usuario_administrador` (`id`, `id_usuario`, `id_rol`, `administrador`, `id_empresa`, `id_sucursal`, `activo`, `id_empresa_create`, `id_sucursal_create`, `id_create`, `date_create`, `id_update`, `date_update`) VALUES
(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, '2020-12-29 01:53:26', NULL, NULL),
(71, 56, 2, 2, 2, 2, 1, 1, 1, 1, '2021-01-21 19:19:05', NULL, NULL),
(72, 56, 2, 2, 28, 25, 1, 1, 1, 1, '2021-01-21 19:19:27', NULL, NULL),
(73, 57, 3, 3, 2, 6, 1, 2, 2, 56, '2021-01-21 19:25:19', NULL, NULL),
(74, 58, 6, 4, 2, 6, 1, 2, 6, 57, '2021-01-21 20:28:08', NULL, NULL),
(75, 54, 3, 3, 2, 6, 1, 2, 6, 57, '2021-01-22 20:11:32', NULL, NULL),
(76, 59, 7, 4, 2, 6, 1, 2, 6, 57, '2021-01-22 21:41:45', NULL, NULL),
(77, 60, 3, 3, 2, 6, 1, 2, 2, 56, '2021-01-22 21:49:25', NULL, NULL),
(78, 61, 8, 4, 2, 6, 1, 2, 6, 60, '2021-01-22 21:53:32', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sg_usuario_empresa`
--

CREATE TABLE `sg_usuario_empresa` (
  `id` int NOT NULL,
  `id_usuario` int NOT NULL,
  `id_empresa` int NOT NULL,
  `id_sucursal` int DEFAULT NULL,
  `id_rol` int NOT NULL,
  `id_empresa_create` int NOT NULL DEFAULT '0',
  `id_sucursal_create` int NOT NULL DEFAULT '0',
  `activo` tinyint NOT NULL DEFAULT '1',
  `id_create` int NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_update` int DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sg_usuario_empresa`
--

INSERT INTO `sg_usuario_empresa` (`id`, `id_usuario`, `id_empresa`, `id_sucursal`, `id_rol`, `id_empresa_create`, `id_sucursal_create`, `activo`, `id_create`, `date_create`, `id_update`, `date_update`) VALUES
(20, 1, 1, 1, 1, 1, 1, 1, 1, '2020-12-29 04:41:41', NULL, NULL),
(97, 56, 2, 2, 2, 1, 1, 1, 1, '2021-01-21 19:19:05', NULL, NULL),
(98, 56, 28, 25, 2, 1, 1, 1, 1, '2021-01-21 19:19:27', NULL, NULL),
(99, 57, 2, 6, 3, 2, 2, 1, 56, '2021-01-21 19:25:19', NULL, NULL),
(100, 58, 2, 6, 6, 2, 6, 1, 57, '2021-01-21 20:28:08', NULL, NULL),
(101, 54, 2, 6, 3, 2, 6, 1, 57, '2021-01-22 20:11:32', NULL, NULL),
(102, 59, 2, 6, 7, 2, 6, 1, 57, '2021-01-22 21:41:45', NULL, NULL),
(103, 60, 2, 6, 3, 2, 2, 1, 56, '2021-01-22 21:49:25', NULL, NULL),
(104, 61, 2, 6, 8, 2, 6, 1, 60, '2021-01-22 21:53:32', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sg_usuario_empresa_modulo`
--

CREATE TABLE `sg_usuario_empresa_modulo` (
  `id` int NOT NULL,
  `id_usuario_empresa` int NOT NULL,
  `id_modulo` int NOT NULL,
  `activo` tinyint NOT NULL DEFAULT '1',
  `id_create` int NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_update` int DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sg_usuario_empresa_modulo`
--

INSERT INTO `sg_usuario_empresa_modulo` (`id`, `id_usuario_empresa`, `id_modulo`, `activo`, `id_create`, `date_create`, `id_update`, `date_update`) VALUES
(105, 20, 3, 1, 1, '2020-12-29 23:38:20', NULL, NULL),
(259, 20, 1, 1, 1, '2021-01-07 20:46:31', NULL, NULL),
(483, 20, 5, 1, 1, '2021-01-21 18:30:20', NULL, NULL),
(484, 20, 4, 1, 1, '2021-01-21 19:10:13', NULL, NULL),
(485, 97, 1, 1, 1, '2021-01-21 19:19:05', NULL, NULL),
(486, 97, 14, 1, 1, '2021-01-21 19:19:07', NULL, NULL),
(487, 97, 7, 1, 1, '2021-01-21 19:19:08', NULL, NULL),
(488, 97, 5, 1, 1, '2021-01-21 19:19:09', NULL, NULL),
(489, 97, 3, 1, 1, '2021-01-21 19:19:10', NULL, NULL),
(490, 98, 1, 1, 1, '2021-01-21 19:19:28', NULL, NULL),
(491, 98, 14, 1, 1, '2021-01-21 19:19:29', NULL, NULL),
(492, 98, 7, 1, 1, '2021-01-21 19:19:30', NULL, NULL),
(493, 98, 5, 1, 1, '2021-01-21 19:19:31', NULL, NULL),
(494, 98, 3, 1, 1, '2021-01-21 19:19:33', NULL, NULL),
(495, 99, 9, 1, 56, '2021-01-21 19:25:19', NULL, NULL),
(496, 99, 11, 1, 56, '2021-01-21 19:25:21', NULL, NULL),
(497, 99, 10, 1, 56, '2021-01-21 19:25:22', NULL, NULL),
(498, 99, 8, 1, 56, '2021-01-21 19:25:23', NULL, NULL),
(499, 99, 13, 1, 56, '2021-01-21 19:25:24', NULL, NULL),
(500, 99, 12, 1, 56, '2021-01-21 19:25:25', NULL, NULL),
(501, 99, 14, 1, 56, '2021-01-21 19:25:26', NULL, NULL),
(502, 99, 7, 1, 56, '2021-01-21 19:25:27', NULL, NULL),
(503, 99, 5, 1, 56, '2021-01-21 19:25:28', NULL, NULL),
(504, 99, 3, 1, 56, '2021-01-21 19:25:30', NULL, NULL),
(505, 100, 10, 1, 57, '2021-01-21 20:28:08', NULL, NULL),
(507, 100, 12, 1, 57, '2021-01-21 20:28:11', NULL, NULL),
(508, 101, 9, 1, 57, '2021-01-22 20:11:32', NULL, NULL),
(509, 101, 11, 1, 57, '2021-01-22 20:11:34', NULL, NULL),
(510, 101, 10, 1, 57, '2021-01-22 20:11:35', NULL, NULL),
(511, 101, 8, 1, 57, '2021-01-22 20:11:36', NULL, NULL),
(512, 101, 13, 1, 57, '2021-01-22 20:11:37', NULL, NULL),
(513, 101, 12, 1, 57, '2021-01-22 20:11:38', NULL, NULL),
(514, 101, 14, 1, 57, '2021-01-22 20:11:39', NULL, NULL),
(515, 101, 7, 1, 57, '2021-01-22 20:11:41', NULL, NULL),
(516, 101, 5, 1, 57, '2021-01-22 20:11:42', NULL, NULL),
(517, 101, 3, 1, 57, '2021-01-22 20:11:43', NULL, NULL),
(518, 102, 5, 1, 57, '2021-01-22 21:41:45', NULL, NULL),
(519, 103, 9, 1, 56, '2021-01-22 21:49:26', NULL, NULL),
(520, 103, 11, 1, 56, '2021-01-22 21:49:28', NULL, NULL),
(521, 103, 10, 1, 56, '2021-01-22 21:49:30', NULL, NULL),
(522, 103, 8, 1, 56, '2021-01-22 21:49:31', NULL, NULL),
(523, 103, 13, 1, 56, '2021-01-22 21:49:32', NULL, NULL),
(524, 103, 12, 1, 56, '2021-01-22 21:49:33', NULL, NULL),
(525, 103, 14, 1, 56, '2021-01-22 21:49:35', NULL, NULL),
(526, 103, 7, 1, 56, '2021-01-22 21:49:36', NULL, NULL),
(527, 103, 5, 1, 56, '2021-01-22 21:49:37', NULL, NULL),
(528, 103, 3, 1, 56, '2021-01-22 21:49:40', NULL, NULL),
(529, 104, 10, 1, 60, '2021-01-22 21:53:32', NULL, NULL),
(530, 104, 5, 1, 60, '2021-01-22 21:53:34', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sg_usuario_empresa_modulo_permiso`
--

CREATE TABLE `sg_usuario_empresa_modulo_permiso` (
  `id` int NOT NULL,
  `id_uem` int NOT NULL,
  `id_permiso` int NOT NULL,
  `id_modulo` int NOT NULL,
  `activo` tinyint NOT NULL DEFAULT '1',
  `id_create` int NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_update` int DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sg_usuario_empresa_modulo_permiso`
--

INSERT INTO `sg_usuario_empresa_modulo_permiso` (`id`, `id_uem`, `id_permiso`, `id_modulo`, `activo`, `id_create`, `date_create`, `id_update`, `date_update`) VALUES
(459, 105, 1, 3, 1, 3, '2020-12-29 04:45:34', NULL, NULL),
(460, 105, 2, 3, 1, 3, '2020-12-29 04:45:34', NULL, NULL),
(461, 105, 3, 3, 1, 3, '2020-12-29 04:45:34', NULL, NULL),
(462, 105, 4, 3, 1, 3, '2020-12-29 04:45:34', NULL, NULL),
(463, 105, 5, 3, 1, 3, '2020-12-29 04:45:35', NULL, NULL),
(1203, 259, 1, 1, 1, 1, '2021-01-07 20:46:32', NULL, NULL),
(1204, 259, 2, 1, 1, 1, '2021-01-07 20:46:32', NULL, NULL),
(1205, 259, 3, 1, 1, 1, '2021-01-07 20:46:32', NULL, NULL),
(1206, 259, 4, 1, 1, 1, '2021-01-07 20:46:32', NULL, NULL),
(1207, 259, 5, 1, 1, 1, '2021-01-07 20:46:33', NULL, NULL),
(2323, 483, 1, 5, 1, 1, '2021-01-21 18:30:22', NULL, NULL),
(2324, 483, 2, 5, 1, 1, '2021-01-21 18:30:22', NULL, NULL),
(2325, 483, 3, 5, 1, 1, '2021-01-21 18:30:22', NULL, NULL),
(2326, 483, 4, 5, 1, 1, '2021-01-21 18:30:22', NULL, NULL),
(2327, 483, 5, 5, 1, 1, '2021-01-21 18:30:22', NULL, NULL),
(2328, 484, 1, 4, 1, 1, '2021-01-21 19:10:13', NULL, NULL),
(2329, 484, 2, 4, 1, 1, '2021-01-21 19:10:13', NULL, NULL),
(2330, 484, 3, 4, 1, 1, '2021-01-21 19:10:14', NULL, NULL),
(2331, 484, 4, 4, 1, 1, '2021-01-21 19:10:14', NULL, NULL),
(2332, 484, 5, 4, 1, 1, '2021-01-21 19:10:14', NULL, NULL),
(2333, 485, 1, 1, 1, 1, '2021-01-21 19:19:05', NULL, NULL),
(2334, 485, 2, 1, 1, 1, '2021-01-21 19:19:06', NULL, NULL),
(2335, 485, 3, 1, 1, 1, '2021-01-21 19:19:06', NULL, NULL),
(2336, 485, 4, 1, 1, 1, '2021-01-21 19:19:06', NULL, NULL),
(2337, 485, 5, 1, 1, 1, '2021-01-21 19:19:06', NULL, NULL),
(2338, 486, 1, 14, 1, 1, '2021-01-21 19:19:07', NULL, NULL),
(2339, 486, 2, 14, 1, 1, '2021-01-21 19:19:07', NULL, NULL),
(2340, 486, 3, 14, 1, 1, '2021-01-21 19:19:07', NULL, NULL),
(2341, 486, 4, 14, 1, 1, '2021-01-21 19:19:07', NULL, NULL),
(2342, 486, 5, 14, 1, 1, '2021-01-21 19:19:07', NULL, NULL),
(2343, 487, 1, 7, 1, 1, '2021-01-21 19:19:08', NULL, NULL),
(2344, 487, 2, 7, 1, 1, '2021-01-21 19:19:08', NULL, NULL),
(2345, 487, 3, 7, 1, 1, '2021-01-21 19:19:08', NULL, NULL),
(2346, 487, 4, 7, 1, 1, '2021-01-21 19:19:08', NULL, NULL),
(2347, 487, 5, 7, 1, 1, '2021-01-21 19:19:08', NULL, NULL),
(2348, 488, 1, 5, 1, 1, '2021-01-21 19:19:09', NULL, NULL),
(2349, 488, 2, 5, 1, 1, '2021-01-21 19:19:09', NULL, NULL),
(2350, 488, 3, 5, 1, 1, '2021-01-21 19:19:09', NULL, NULL),
(2351, 488, 4, 5, 1, 1, '2021-01-21 19:19:09', NULL, NULL),
(2352, 488, 5, 5, 1, 1, '2021-01-21 19:19:09', NULL, NULL),
(2353, 489, 1, 3, 1, 1, '2021-01-21 19:19:10', NULL, NULL),
(2354, 489, 2, 3, 1, 1, '2021-01-21 19:19:10', NULL, NULL),
(2355, 489, 3, 3, 1, 1, '2021-01-21 19:19:11', NULL, NULL),
(2356, 489, 4, 3, 1, 1, '2021-01-21 19:19:11', NULL, NULL),
(2357, 489, 5, 3, 1, 1, '2021-01-21 19:19:11', NULL, NULL),
(2358, 490, 1, 1, 1, 1, '2021-01-21 19:19:28', NULL, NULL),
(2359, 490, 2, 1, 1, 1, '2021-01-21 19:19:28', NULL, NULL),
(2360, 490, 3, 1, 1, 1, '2021-01-21 19:19:28', NULL, NULL),
(2361, 490, 4, 1, 1, 1, '2021-01-21 19:19:28', NULL, NULL),
(2362, 490, 5, 1, 1, 1, '2021-01-21 19:19:29', NULL, NULL),
(2363, 491, 1, 14, 1, 1, '2021-01-21 19:19:29', NULL, NULL),
(2364, 491, 2, 14, 1, 1, '2021-01-21 19:19:29', NULL, NULL),
(2365, 491, 3, 14, 1, 1, '2021-01-21 19:19:29', NULL, NULL),
(2366, 491, 4, 14, 1, 1, '2021-01-21 19:19:29', NULL, NULL),
(2367, 491, 5, 14, 1, 1, '2021-01-21 19:19:29', NULL, NULL),
(2368, 492, 1, 7, 1, 1, '2021-01-21 19:19:30', NULL, NULL),
(2369, 492, 2, 7, 1, 1, '2021-01-21 19:19:30', NULL, NULL),
(2370, 492, 3, 7, 1, 1, '2021-01-21 19:19:30', NULL, NULL),
(2371, 492, 4, 7, 1, 1, '2021-01-21 19:19:30', NULL, NULL),
(2372, 492, 5, 7, 1, 1, '2021-01-21 19:19:31', NULL, NULL),
(2373, 493, 1, 5, 1, 1, '2021-01-21 19:19:31', NULL, NULL),
(2374, 493, 2, 5, 1, 1, '2021-01-21 19:19:31', NULL, NULL),
(2375, 493, 3, 5, 1, 1, '2021-01-21 19:19:31', NULL, NULL),
(2376, 493, 4, 5, 1, 1, '2021-01-21 19:19:31', NULL, NULL),
(2377, 493, 5, 5, 1, 1, '2021-01-21 19:19:32', NULL, NULL),
(2378, 494, 1, 3, 1, 1, '2021-01-21 19:19:33', NULL, NULL),
(2379, 494, 2, 3, 1, 1, '2021-01-21 19:19:33', NULL, NULL),
(2380, 494, 3, 3, 1, 1, '2021-01-21 19:19:33', NULL, NULL),
(2381, 494, 4, 3, 1, 1, '2021-01-21 19:19:33', NULL, NULL),
(2382, 494, 5, 3, 1, 1, '2021-01-21 19:19:33', NULL, NULL),
(2383, 495, 1, 9, 1, 56, '2021-01-21 19:25:20', NULL, NULL),
(2384, 495, 2, 9, 1, 56, '2021-01-21 19:25:20', NULL, NULL),
(2385, 495, 3, 9, 1, 56, '2021-01-21 19:25:20', NULL, NULL),
(2386, 495, 4, 9, 1, 56, '2021-01-21 19:25:20', NULL, NULL),
(2387, 495, 5, 9, 1, 56, '2021-01-21 19:25:20', NULL, NULL),
(2388, 496, 1, 11, 1, 56, '2021-01-21 19:25:21', NULL, NULL),
(2389, 496, 2, 11, 1, 56, '2021-01-21 19:25:21', NULL, NULL),
(2390, 496, 3, 11, 1, 56, '2021-01-21 19:25:21', NULL, NULL),
(2391, 496, 4, 11, 1, 56, '2021-01-21 19:25:22', NULL, NULL),
(2392, 496, 5, 11, 1, 56, '2021-01-21 19:25:22', NULL, NULL),
(2393, 497, 1, 10, 1, 56, '2021-01-21 19:25:23', NULL, NULL),
(2394, 497, 2, 10, 1, 56, '2021-01-21 19:25:23', NULL, NULL),
(2395, 497, 3, 10, 1, 56, '2021-01-21 19:25:23', NULL, NULL),
(2396, 497, 4, 10, 1, 56, '2021-01-21 19:25:23', NULL, NULL),
(2397, 497, 5, 10, 1, 56, '2021-01-21 19:25:23', NULL, NULL),
(2398, 498, 1, 8, 1, 56, '2021-01-21 19:25:23', NULL, NULL),
(2399, 498, 2, 8, 1, 56, '2021-01-21 19:25:23', NULL, NULL),
(2400, 498, 3, 8, 1, 56, '2021-01-21 19:25:24', NULL, NULL),
(2401, 498, 4, 8, 1, 56, '2021-01-21 19:25:24', NULL, NULL),
(2402, 498, 5, 8, 1, 56, '2021-01-21 19:25:24', NULL, NULL),
(2403, 499, 1, 13, 1, 56, '2021-01-21 19:25:24', NULL, NULL),
(2404, 499, 2, 13, 1, 56, '2021-01-21 19:25:25', NULL, NULL),
(2405, 499, 3, 13, 1, 56, '2021-01-21 19:25:25', NULL, NULL),
(2406, 499, 4, 13, 1, 56, '2021-01-21 19:25:25', NULL, NULL),
(2407, 499, 5, 13, 1, 56, '2021-01-21 19:25:25', NULL, NULL),
(2408, 500, 1, 12, 1, 56, '2021-01-21 19:25:25', NULL, NULL),
(2409, 500, 2, 12, 1, 56, '2021-01-21 19:25:26', NULL, NULL),
(2410, 500, 3, 12, 1, 56, '2021-01-21 19:25:26', NULL, NULL),
(2411, 500, 4, 12, 1, 56, '2021-01-21 19:25:26', NULL, NULL),
(2412, 500, 5, 12, 1, 56, '2021-01-21 19:25:26', NULL, NULL),
(2413, 501, 1, 14, 1, 56, '2021-01-21 19:25:26', NULL, NULL),
(2414, 501, 2, 14, 1, 56, '2021-01-21 19:25:26', NULL, NULL),
(2415, 501, 3, 14, 1, 56, '2021-01-21 19:25:26', NULL, NULL),
(2416, 501, 4, 14, 1, 56, '2021-01-21 19:25:27', NULL, NULL),
(2417, 501, 5, 14, 1, 56, '2021-01-21 19:25:27', NULL, NULL),
(2418, 502, 1, 7, 1, 56, '2021-01-21 19:25:27', NULL, NULL),
(2419, 502, 2, 7, 1, 56, '2021-01-21 19:25:27', NULL, NULL),
(2420, 502, 3, 7, 1, 56, '2021-01-21 19:25:27', NULL, NULL),
(2421, 502, 4, 7, 1, 56, '2021-01-21 19:25:28', NULL, NULL),
(2422, 502, 5, 7, 1, 56, '2021-01-21 19:25:28', NULL, NULL),
(2423, 503, 1, 5, 1, 56, '2021-01-21 19:25:28', NULL, NULL),
(2424, 503, 2, 5, 1, 56, '2021-01-21 19:25:28', NULL, NULL),
(2425, 503, 3, 5, 1, 56, '2021-01-21 19:25:29', NULL, NULL),
(2426, 503, 4, 5, 1, 56, '2021-01-21 19:25:29', NULL, NULL),
(2427, 503, 5, 5, 1, 56, '2021-01-21 19:25:29', NULL, NULL),
(2428, 504, 1, 3, 1, 56, '2021-01-21 19:25:30', NULL, NULL),
(2429, 504, 2, 3, 1, 56, '2021-01-21 19:25:30', NULL, NULL),
(2430, 504, 3, 3, 1, 56, '2021-01-21 19:25:30', NULL, NULL),
(2431, 504, 4, 3, 1, 56, '2021-01-21 19:25:30', NULL, NULL),
(2432, 504, 5, 3, 1, 56, '2021-01-21 19:25:30', NULL, NULL),
(2433, 505, 1, 10, 1, 57, '2021-01-21 20:28:09', NULL, NULL),
(2434, 505, 2, 10, 1, 57, '2021-01-21 20:28:09', NULL, NULL),
(2435, 505, 3, 10, 1, 57, '2021-01-21 20:28:09', NULL, NULL),
(2436, 505, 4, 10, 1, 57, '2021-01-21 20:28:09', NULL, NULL),
(2437, 505, 5, 10, 1, 57, '2021-01-21 20:28:10', NULL, NULL),
(2443, 507, 1, 12, 1, 57, '2021-01-21 20:28:11', NULL, NULL),
(2444, 507, 2, 12, 1, 57, '2021-01-21 20:28:12', NULL, NULL),
(2445, 507, 3, 12, 1, 57, '2021-01-21 20:28:12', NULL, NULL),
(2446, 507, 4, 12, 1, 57, '2021-01-21 20:28:12', NULL, NULL),
(2447, 507, 5, 12, 1, 57, '2021-01-21 20:28:12', NULL, NULL),
(2448, 508, 1, 9, 1, 57, '2021-01-22 20:11:32', NULL, NULL),
(2449, 508, 2, 9, 1, 57, '2021-01-22 20:11:32', NULL, NULL),
(2450, 508, 3, 9, 1, 57, '2021-01-22 20:11:32', NULL, NULL),
(2451, 508, 4, 9, 1, 57, '2021-01-22 20:11:32', NULL, NULL),
(2452, 508, 5, 9, 1, 57, '2021-01-22 20:11:33', NULL, NULL),
(2453, 509, 1, 11, 1, 57, '2021-01-22 20:11:34', NULL, NULL),
(2454, 509, 2, 11, 1, 57, '2021-01-22 20:11:34', NULL, NULL),
(2455, 509, 3, 11, 1, 57, '2021-01-22 20:11:34', NULL, NULL),
(2456, 509, 4, 11, 1, 57, '2021-01-22 20:11:34', NULL, NULL),
(2457, 509, 5, 11, 1, 57, '2021-01-22 20:11:34', NULL, NULL),
(2458, 510, 1, 10, 1, 57, '2021-01-22 20:11:35', NULL, NULL),
(2459, 510, 2, 10, 1, 57, '2021-01-22 20:11:35', NULL, NULL),
(2460, 510, 3, 10, 1, 57, '2021-01-22 20:11:35', NULL, NULL),
(2461, 510, 4, 10, 1, 57, '2021-01-22 20:11:36', NULL, NULL),
(2462, 510, 5, 10, 1, 57, '2021-01-22 20:11:36', NULL, NULL),
(2463, 511, 1, 8, 1, 57, '2021-01-22 20:11:36', NULL, NULL),
(2464, 511, 2, 8, 1, 57, '2021-01-22 20:11:36', NULL, NULL),
(2465, 511, 3, 8, 1, 57, '2021-01-22 20:11:36', NULL, NULL),
(2466, 511, 4, 8, 1, 57, '2021-01-22 20:11:37', NULL, NULL),
(2467, 511, 5, 8, 1, 57, '2021-01-22 20:11:37', NULL, NULL),
(2468, 512, 1, 13, 1, 57, '2021-01-22 20:11:37', NULL, NULL),
(2469, 512, 2, 13, 1, 57, '2021-01-22 20:11:37', NULL, NULL),
(2470, 512, 3, 13, 1, 57, '2021-01-22 20:11:38', NULL, NULL),
(2471, 512, 4, 13, 1, 57, '2021-01-22 20:11:38', NULL, NULL),
(2472, 512, 5, 13, 1, 57, '2021-01-22 20:11:38', NULL, NULL),
(2473, 513, 1, 12, 1, 57, '2021-01-22 20:11:38', NULL, NULL),
(2474, 513, 2, 12, 1, 57, '2021-01-22 20:11:38', NULL, NULL),
(2475, 513, 3, 12, 1, 57, '2021-01-22 20:11:39', NULL, NULL),
(2476, 513, 4, 12, 1, 57, '2021-01-22 20:11:39', NULL, NULL),
(2477, 513, 5, 12, 1, 57, '2021-01-22 20:11:39', NULL, NULL),
(2478, 514, 1, 14, 1, 57, '2021-01-22 20:11:39', NULL, NULL),
(2479, 514, 2, 14, 1, 57, '2021-01-22 20:11:39', NULL, NULL),
(2480, 514, 3, 14, 1, 57, '2021-01-22 20:11:40', NULL, NULL),
(2481, 514, 4, 14, 1, 57, '2021-01-22 20:11:40', NULL, NULL),
(2482, 514, 5, 14, 1, 57, '2021-01-22 20:11:40', NULL, NULL),
(2483, 515, 1, 7, 1, 57, '2021-01-22 20:11:41', NULL, NULL),
(2484, 515, 2, 7, 1, 57, '2021-01-22 20:11:41', NULL, NULL),
(2485, 515, 3, 7, 1, 57, '2021-01-22 20:11:41', NULL, NULL),
(2486, 515, 4, 7, 1, 57, '2021-01-22 20:11:41', NULL, NULL),
(2487, 515, 5, 7, 1, 57, '2021-01-22 20:11:41', NULL, NULL),
(2488, 516, 1, 5, 1, 57, '2021-01-22 20:11:42', NULL, NULL),
(2489, 516, 2, 5, 1, 57, '2021-01-22 20:11:42', NULL, NULL),
(2490, 516, 3, 5, 1, 57, '2021-01-22 20:11:42', NULL, NULL),
(2491, 516, 4, 5, 1, 57, '2021-01-22 20:11:42', NULL, NULL),
(2492, 516, 5, 5, 1, 57, '2021-01-22 20:11:42', NULL, NULL),
(2493, 517, 1, 3, 1, 57, '2021-01-22 20:11:43', NULL, NULL),
(2494, 517, 2, 3, 1, 57, '2021-01-22 20:11:44', NULL, NULL),
(2495, 517, 3, 3, 1, 57, '2021-01-22 20:11:44', NULL, NULL),
(2496, 517, 4, 3, 1, 57, '2021-01-22 20:11:44', NULL, NULL),
(2497, 517, 5, 3, 1, 57, '2021-01-22 20:11:44', NULL, NULL),
(2498, 518, 1, 5, 1, 57, '2021-01-22 21:41:46', NULL, NULL),
(2499, 518, 2, 5, 1, 57, '2021-01-22 21:41:46', NULL, NULL),
(2500, 518, 3, 5, 1, 57, '2021-01-22 21:41:46', NULL, NULL),
(2501, 518, 4, 5, 1, 57, '2021-01-22 21:41:46', NULL, NULL),
(2502, 518, 5, 5, 1, 57, '2021-01-22 21:41:47', NULL, NULL),
(2503, 519, 1, 9, 1, 56, '2021-01-22 21:49:26', NULL, NULL),
(2504, 519, 2, 9, 1, 56, '2021-01-22 21:49:26', NULL, NULL),
(2505, 519, 3, 9, 1, 56, '2021-01-22 21:49:26', NULL, NULL),
(2506, 519, 4, 9, 1, 56, '2021-01-22 21:49:26', NULL, NULL),
(2507, 519, 5, 9, 1, 56, '2021-01-22 21:49:27', NULL, NULL),
(2508, 520, 1, 11, 1, 56, '2021-01-22 21:49:28', NULL, NULL),
(2509, 520, 2, 11, 1, 56, '2021-01-22 21:49:28', NULL, NULL),
(2510, 520, 3, 11, 1, 56, '2021-01-22 21:49:28', NULL, NULL),
(2511, 520, 4, 11, 1, 56, '2021-01-22 21:49:29', NULL, NULL),
(2512, 520, 5, 11, 1, 56, '2021-01-22 21:49:29', NULL, NULL),
(2513, 521, 1, 10, 1, 56, '2021-01-22 21:49:30', NULL, NULL),
(2514, 521, 2, 10, 1, 56, '2021-01-22 21:49:30', NULL, NULL),
(2515, 521, 3, 10, 1, 56, '2021-01-22 21:49:30', NULL, NULL),
(2516, 521, 4, 10, 1, 56, '2021-01-22 21:49:30', NULL, NULL),
(2517, 521, 5, 10, 1, 56, '2021-01-22 21:49:31', NULL, NULL),
(2518, 522, 1, 8, 1, 56, '2021-01-22 21:49:31', NULL, NULL),
(2519, 522, 2, 8, 1, 56, '2021-01-22 21:49:31', NULL, NULL),
(2520, 522, 3, 8, 1, 56, '2021-01-22 21:49:31', NULL, NULL),
(2521, 522, 4, 8, 1, 56, '2021-01-22 21:49:32', NULL, NULL),
(2522, 522, 5, 8, 1, 56, '2021-01-22 21:49:32', NULL, NULL),
(2523, 523, 1, 13, 1, 56, '2021-01-22 21:49:32', NULL, NULL),
(2524, 523, 2, 13, 1, 56, '2021-01-22 21:49:33', NULL, NULL),
(2525, 523, 3, 13, 1, 56, '2021-01-22 21:49:33', NULL, NULL),
(2526, 523, 4, 13, 1, 56, '2021-01-22 21:49:33', NULL, NULL),
(2527, 523, 5, 13, 1, 56, '2021-01-22 21:49:33', NULL, NULL),
(2528, 524, 1, 12, 1, 56, '2021-01-22 21:49:34', NULL, NULL),
(2529, 524, 2, 12, 1, 56, '2021-01-22 21:49:34', NULL, NULL),
(2530, 524, 3, 12, 1, 56, '2021-01-22 21:49:34', NULL, NULL),
(2531, 524, 4, 12, 1, 56, '2021-01-22 21:49:34', NULL, NULL),
(2532, 524, 5, 12, 1, 56, '2021-01-22 21:49:34', NULL, NULL),
(2533, 525, 1, 14, 1, 56, '2021-01-22 21:49:35', NULL, NULL),
(2534, 525, 2, 14, 1, 56, '2021-01-22 21:49:35', NULL, NULL),
(2535, 525, 3, 14, 1, 56, '2021-01-22 21:49:35', NULL, NULL),
(2536, 525, 4, 14, 1, 56, '2021-01-22 21:49:35', NULL, NULL),
(2537, 525, 5, 14, 1, 56, '2021-01-22 21:49:36', NULL, NULL),
(2538, 526, 1, 7, 1, 56, '2021-01-22 21:49:36', NULL, NULL),
(2539, 526, 2, 7, 1, 56, '2021-01-22 21:49:36', NULL, NULL),
(2540, 526, 3, 7, 1, 56, '2021-01-22 21:49:37', NULL, NULL),
(2541, 526, 4, 7, 1, 56, '2021-01-22 21:49:37', NULL, NULL),
(2542, 526, 5, 7, 1, 56, '2021-01-22 21:49:37', NULL, NULL),
(2543, 527, 1, 5, 1, 56, '2021-01-22 21:49:38', NULL, NULL),
(2544, 527, 2, 5, 1, 56, '2021-01-22 21:49:38', NULL, NULL),
(2545, 527, 3, 5, 1, 56, '2021-01-22 21:49:38', NULL, NULL),
(2546, 527, 4, 5, 1, 56, '2021-01-22 21:49:38', NULL, NULL),
(2547, 527, 5, 5, 1, 56, '2021-01-22 21:49:39', NULL, NULL),
(2548, 528, 1, 3, 1, 56, '2021-01-22 21:49:41', NULL, NULL),
(2549, 528, 2, 3, 1, 56, '2021-01-22 21:49:41', NULL, NULL),
(2550, 528, 3, 3, 1, 56, '2021-01-22 21:49:41', NULL, NULL),
(2551, 528, 4, 3, 1, 56, '2021-01-22 21:49:41', NULL, NULL),
(2552, 528, 5, 3, 1, 56, '2021-01-22 21:49:42', NULL, NULL),
(2553, 529, 1, 10, 1, 60, '2021-01-22 21:53:33', NULL, NULL),
(2554, 529, 2, 10, 1, 60, '2021-01-22 21:53:33', NULL, NULL),
(2555, 529, 3, 10, 1, 60, '2021-01-22 21:53:33', NULL, NULL),
(2556, 529, 4, 10, 1, 60, '2021-01-22 21:53:34', NULL, NULL),
(2557, 529, 5, 10, 1, 60, '2021-01-22 21:53:34', NULL, NULL),
(2558, 530, 1, 5, 1, 60, '2021-01-22 21:53:35', NULL, NULL),
(2559, 530, 2, 5, 1, 60, '2021-01-22 21:53:35', NULL, NULL),
(2560, 530, 3, 5, 1, 60, '2021-01-22 21:53:35', NULL, NULL),
(2561, 530, 4, 5, 1, 60, '2021-01-22 21:53:35', NULL, NULL),
(2562, 530, 5, 5, 1, 60, '2021-01-22 21:53:35', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sg_usuario_empresa_modulo_proceso`
--

CREATE TABLE `sg_usuario_empresa_modulo_proceso` (
  `id` int NOT NULL,
  `id_uem` int NOT NULL,
  `id_modulo` int NOT NULL,
  `id_sub_modulo` int NOT NULL,
  `activo` int NOT NULL DEFAULT '1',
  `id_create` int NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_update` int DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sg_usuario_empresa_modulo_proceso`
--

INSERT INTO `sg_usuario_empresa_modulo_proceso` (`id`, `id_uem`, `id_modulo`, `id_sub_modulo`, `activo`, `id_create`, `date_create`, `id_update`, `date_update`) VALUES
(20, 105, 3, 3, 1, 1, '2020-12-29 23:40:27', NULL, NULL),
(21, 105, 3, 4, 1, 1, '2020-12-29 23:41:05', NULL, NULL),
(22, 105, 3, 5, 1, 1, '2020-12-29 23:41:05', NULL, NULL),
(23, 105, 3, 6, 1, 1, '2020-12-29 23:41:23', NULL, NULL),
(355, 105, 3, 40, 1, 1, '2021-01-07 19:00:19', NULL, NULL),
(439, 259, 1, 10, 1, 1, '2021-01-07 20:46:31', NULL, NULL),
(440, 259, 1, 11, 1, 1, '2021-01-07 20:46:32', NULL, NULL),
(441, 259, 1, 12, 1, 1, '2021-01-07 20:46:32', NULL, NULL),
(970, 105, 3, 41, 1, 1, '2021-01-09 14:39:19', NULL, NULL),
(1097, 483, 5, 7, 1, 1, '2021-01-21 18:30:21', NULL, NULL),
(1098, 483, 5, 8, 1, 1, '2021-01-21 18:30:21', NULL, NULL),
(1101, 483, 5, 24, 1, 1, '2021-01-21 18:30:21', NULL, NULL),
(1102, 483, 5, 25, 1, 1, '2021-01-21 18:30:22', NULL, NULL),
(1103, 484, 4, 1, 1, 1, '2021-01-21 19:10:13', NULL, NULL),
(1104, 484, 4, 2, 1, 1, '2021-01-21 19:10:13', NULL, NULL),
(1105, 485, 1, 12, 1, 1, '2021-01-21 19:19:06', NULL, NULL),
(1106, 485, 1, 11, 1, 1, '2021-01-21 19:19:07', NULL, NULL),
(1107, 486, 14, 46, 1, 1, '2021-01-21 19:19:07', NULL, NULL),
(1108, 486, 14, 47, 1, 1, '2021-01-21 19:19:07', NULL, NULL),
(1109, 487, 7, 14, 1, 1, '2021-01-21 19:19:08', NULL, NULL),
(1110, 488, 5, 7, 1, 1, '2021-01-21 19:19:09', NULL, NULL),
(1111, 488, 5, 8, 1, 1, '2021-01-21 19:19:10', NULL, NULL),
(1112, 488, 5, 9, 1, 1, '2021-01-21 19:19:10', NULL, NULL),
(1113, 488, 5, 13, 1, 1, '2021-01-21 19:19:10', NULL, NULL),
(1114, 488, 5, 25, 1, 1, '2021-01-21 19:19:10', NULL, NULL),
(1115, 489, 3, 3, 1, 1, '2021-01-21 19:19:11', NULL, NULL),
(1116, 489, 3, 4, 1, 1, '2021-01-21 19:19:11', NULL, NULL),
(1117, 489, 3, 5, 1, 1, '2021-01-21 19:19:11', NULL, NULL),
(1118, 489, 3, 6, 1, 1, '2021-01-21 19:19:11', NULL, NULL),
(1119, 489, 3, 41, 1, 1, '2021-01-21 19:19:12', NULL, NULL),
(1120, 489, 3, 42, 1, 1, '2021-01-21 19:19:12', NULL, NULL),
(1121, 490, 1, 12, 1, 1, '2021-01-21 19:19:29', NULL, NULL),
(1122, 490, 1, 11, 1, 1, '2021-01-21 19:19:29', NULL, NULL),
(1123, 491, 14, 46, 1, 1, '2021-01-21 19:19:30', NULL, NULL),
(1124, 491, 14, 47, 1, 1, '2021-01-21 19:19:30', NULL, NULL),
(1125, 492, 7, 14, 1, 1, '2021-01-21 19:19:31', NULL, NULL),
(1126, 493, 5, 7, 1, 1, '2021-01-21 19:19:32', NULL, NULL),
(1127, 493, 5, 8, 1, 1, '2021-01-21 19:19:32', NULL, NULL),
(1128, 493, 5, 9, 1, 1, '2021-01-21 19:19:32', NULL, NULL),
(1129, 493, 5, 13, 1, 1, '2021-01-21 19:19:32', NULL, NULL),
(1130, 493, 5, 25, 1, 1, '2021-01-21 19:19:32', NULL, NULL),
(1131, 494, 3, 3, 1, 1, '2021-01-21 19:19:33', NULL, NULL),
(1132, 494, 3, 4, 1, 1, '2021-01-21 19:19:33', NULL, NULL),
(1133, 494, 3, 5, 1, 1, '2021-01-21 19:19:34', NULL, NULL),
(1134, 494, 3, 6, 1, 1, '2021-01-21 19:19:34', NULL, NULL),
(1135, 494, 3, 41, 1, 1, '2021-01-21 19:19:34', NULL, NULL),
(1136, 494, 3, 42, 1, 1, '2021-01-21 19:19:34', NULL, NULL),
(1137, 495, 9, 20, 1, 56, '2021-01-21 19:25:20', NULL, NULL),
(1138, 495, 9, 23, 1, 56, '2021-01-21 19:25:21', NULL, NULL),
(1139, 495, 9, 44, 1, 56, '2021-01-21 19:25:21', NULL, NULL),
(1140, 495, 9, 45, 1, 56, '2021-01-21 19:25:21', NULL, NULL),
(1141, 496, 11, 35, 1, 56, '2021-01-21 19:25:22', NULL, NULL),
(1142, 496, 11, 33, 1, 56, '2021-01-21 19:25:22', NULL, NULL),
(1143, 496, 11, 32, 1, 56, '2021-01-21 19:25:22', NULL, NULL),
(1144, 496, 11, 38, 1, 56, '2021-01-21 19:25:22', NULL, NULL),
(1145, 496, 11, 34, 1, 56, '2021-01-21 19:25:22', NULL, NULL),
(1146, 497, 10, 30, 1, 56, '2021-01-21 19:25:23', NULL, NULL),
(1147, 498, 8, 19, 1, 56, '2021-01-21 19:25:24', NULL, NULL),
(1148, 499, 13, 37, 1, 56, '2021-01-21 19:25:25', NULL, NULL),
(1149, 500, 12, 36, 1, 56, '2021-01-21 19:25:26', NULL, NULL),
(1150, 501, 14, 48, 1, 56, '2021-01-21 19:25:27', NULL, NULL),
(1151, 501, 14, 49, 1, 56, '2021-01-21 19:25:27', NULL, NULL),
(1152, 502, 7, 14, 1, 56, '2021-01-21 19:25:28', NULL, NULL),
(1153, 502, 7, 29, 1, 56, '2021-01-21 19:25:28', NULL, NULL),
(1154, 503, 5, 7, 1, 56, '2021-01-21 19:25:29', NULL, NULL),
(1155, 503, 5, 8, 1, 56, '2021-01-21 19:25:29', NULL, NULL),
(1156, 503, 5, 9, 1, 56, '2021-01-21 19:25:29', NULL, NULL),
(1157, 503, 5, 13, 1, 56, '2021-01-21 19:25:29', NULL, NULL),
(1158, 503, 5, 25, 1, 56, '2021-01-21 19:25:30', NULL, NULL),
(1159, 504, 3, 3, 1, 56, '2021-01-21 19:25:31', NULL, NULL),
(1160, 504, 3, 4, 1, 56, '2021-01-21 19:25:31', NULL, NULL),
(1161, 504, 3, 5, 1, 56, '2021-01-21 19:25:31', NULL, NULL),
(1162, 504, 3, 6, 1, 56, '2021-01-21 19:25:31', NULL, NULL),
(1163, 504, 3, 43, 1, 56, '2021-01-21 19:25:31', NULL, NULL),
(1164, 505, 10, 26, 1, 57, '2021-01-21 20:28:10', NULL, NULL),
(1165, 505, 10, 27, 1, 57, '2021-01-21 20:28:10', NULL, NULL),
(1166, 505, 10, 28, 1, 57, '2021-01-21 20:28:10', NULL, NULL),
(1167, 505, 10, 31, 1, 57, '2021-01-21 20:28:10', NULL, NULL),
(1168, 506, 8, 19, 1, 57, '2021-01-21 20:28:11', NULL, NULL),
(1169, 507, 12, 36, 1, 57, '2021-01-21 20:28:12', NULL, NULL),
(1170, 508, 9, 20, 1, 57, '2021-01-22 20:11:33', NULL, NULL),
(1171, 508, 9, 23, 1, 57, '2021-01-22 20:11:33', NULL, NULL),
(1172, 508, 9, 44, 1, 57, '2021-01-22 20:11:33', NULL, NULL),
(1173, 508, 9, 45, 1, 57, '2021-01-22 20:11:33', NULL, NULL),
(1174, 509, 11, 35, 1, 57, '2021-01-22 20:11:34', NULL, NULL),
(1175, 509, 11, 33, 1, 57, '2021-01-22 20:11:34', NULL, NULL),
(1176, 509, 11, 32, 1, 57, '2021-01-22 20:11:35', NULL, NULL),
(1177, 509, 11, 38, 1, 57, '2021-01-22 20:11:35', NULL, NULL),
(1178, 509, 11, 34, 1, 57, '2021-01-22 20:11:35', NULL, NULL),
(1179, 510, 10, 30, 1, 57, '2021-01-22 20:11:36', NULL, NULL),
(1180, 511, 8, 19, 1, 57, '2021-01-22 20:11:37', NULL, NULL),
(1181, 512, 13, 37, 1, 57, '2021-01-22 20:11:38', NULL, NULL),
(1182, 513, 12, 36, 1, 57, '2021-01-22 20:11:39', NULL, NULL),
(1183, 514, 14, 48, 1, 57, '2021-01-22 20:11:40', NULL, NULL),
(1184, 514, 14, 49, 1, 57, '2021-01-22 20:11:41', NULL, NULL),
(1185, 515, 7, 14, 1, 57, '2021-01-22 20:11:41', NULL, NULL),
(1186, 515, 7, 29, 1, 57, '2021-01-22 20:11:41', NULL, NULL),
(1187, 516, 5, 7, 1, 57, '2021-01-22 20:11:42', NULL, NULL),
(1188, 516, 5, 8, 1, 57, '2021-01-22 20:11:43', NULL, NULL),
(1189, 516, 5, 9, 1, 57, '2021-01-22 20:11:43', NULL, NULL),
(1190, 516, 5, 13, 1, 57, '2021-01-22 20:11:43', NULL, NULL),
(1191, 516, 5, 25, 1, 57, '2021-01-22 20:11:43', NULL, NULL),
(1192, 517, 3, 3, 1, 57, '2021-01-22 20:11:44', NULL, NULL),
(1193, 517, 3, 4, 1, 57, '2021-01-22 20:11:44', NULL, NULL),
(1194, 517, 3, 5, 1, 57, '2021-01-22 20:11:44', NULL, NULL),
(1195, 517, 3, 6, 1, 57, '2021-01-22 20:11:44', NULL, NULL),
(1196, 517, 3, 43, 1, 57, '2021-01-22 20:11:45', NULL, NULL),
(1197, 518, 5, 7, 1, 57, '2021-01-22 21:41:47', NULL, NULL),
(1198, 518, 5, 9, 1, 57, '2021-01-22 21:41:47', NULL, NULL),
(1199, 518, 5, 13, 1, 57, '2021-01-22 21:41:47', NULL, NULL),
(1200, 519, 9, 20, 1, 56, '2021-01-22 21:49:27', NULL, NULL),
(1201, 519, 9, 23, 1, 56, '2021-01-22 21:49:27', NULL, NULL),
(1202, 519, 9, 44, 1, 56, '2021-01-22 21:49:27', NULL, NULL),
(1203, 519, 9, 45, 1, 56, '2021-01-22 21:49:27', NULL, NULL),
(1204, 520, 11, 35, 1, 56, '2021-01-22 21:49:29', NULL, NULL),
(1205, 520, 11, 33, 1, 56, '2021-01-22 21:49:29', NULL, NULL),
(1206, 520, 11, 32, 1, 56, '2021-01-22 21:49:29', NULL, NULL),
(1207, 520, 11, 38, 1, 56, '2021-01-22 21:49:29', NULL, NULL),
(1208, 520, 11, 34, 1, 56, '2021-01-22 21:49:30', NULL, NULL),
(1209, 521, 10, 30, 1, 56, '2021-01-22 21:49:31', NULL, NULL),
(1210, 522, 8, 19, 1, 56, '2021-01-22 21:49:32', NULL, NULL),
(1211, 523, 13, 37, 1, 56, '2021-01-22 21:49:33', NULL, NULL),
(1212, 524, 12, 36, 1, 56, '2021-01-22 21:49:34', NULL, NULL),
(1213, 525, 14, 48, 1, 56, '2021-01-22 21:49:36', NULL, NULL),
(1214, 525, 14, 49, 1, 56, '2021-01-22 21:49:36', NULL, NULL),
(1215, 526, 7, 14, 1, 56, '2021-01-22 21:49:37', NULL, NULL),
(1216, 526, 7, 29, 1, 56, '2021-01-22 21:49:37', NULL, NULL),
(1217, 527, 5, 7, 1, 56, '2021-01-22 21:49:39', NULL, NULL),
(1218, 527, 5, 8, 1, 56, '2021-01-22 21:49:39', NULL, NULL),
(1219, 527, 5, 9, 1, 56, '2021-01-22 21:49:39', NULL, NULL),
(1220, 527, 5, 13, 1, 56, '2021-01-22 21:49:40', NULL, NULL),
(1221, 527, 5, 25, 1, 56, '2021-01-22 21:49:40', NULL, NULL),
(1222, 528, 3, 3, 1, 56, '2021-01-22 21:49:42', NULL, NULL),
(1223, 528, 3, 4, 1, 56, '2021-01-22 21:49:42', NULL, NULL),
(1224, 528, 3, 5, 1, 56, '2021-01-22 21:49:42', NULL, NULL),
(1225, 528, 3, 6, 1, 56, '2021-01-22 21:49:42', NULL, NULL),
(1226, 528, 3, 43, 1, 56, '2021-01-22 21:49:43', NULL, NULL),
(1227, 529, 10, 31, 1, 60, '2021-01-22 21:53:34', NULL, NULL),
(1228, 529, 10, 30, 1, 60, '2021-01-22 21:53:34', NULL, NULL),
(1229, 530, 5, 7, 1, 60, '2021-01-22 21:53:35', NULL, NULL),
(1230, 530, 5, 9, 1, 60, '2021-01-22 21:53:36', NULL, NULL),
(1231, 530, 5, 13, 1, 60, '2021-01-22 21:53:36', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vt_venta`
--

CREATE TABLE `vt_venta` (
  `id` int NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `documento` varchar(100) NOT NULL,
  `id_cobro` int NOT NULL,
  `monto_cobrar` decimal(10,2) NOT NULL DEFAULT '0.00',
  `descuento` decimal(10,2) NOT NULL DEFAULT '0.00',
  `monto_venta` decimal(10,2) NOT NULL DEFAULT '0.00',
  `porcentaje_impuesto` decimal(10,2) NOT NULL DEFAULT '0.00',
  `impuesto` decimal(10,2) NOT NULL DEFAULT '0.00',
  `total_venta` decimal(10,2) NOT NULL DEFAULT '0.00',
  `id_empresa` int NOT NULL,
  `id_sucursal` int NOT NULL,
  `reverso` int NOT NULL DEFAULT '0',
  `motivo_reverso` int DEFAULT NULL,
  `fecha_reverso` timestamp NULL DEFAULT NULL,
  `id_create` int NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_update` int DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vt_venta_detalle`
--

CREATE TABLE `vt_venta_detalle` (
  `id` int NOT NULL,
  `id_venta` int NOT NULL,
  `descripcion` text NOT NULL,
  `precio_unitario` decimal(10,2) NOT NULL DEFAULT '0.00',
  `cantidad` int NOT NULL DEFAULT '0',
  `precio` decimal(10,2) NOT NULL DEFAULT '0.00',
  `id_alimento` int NOT NULL,
  `id_presentacion` int NOT NULL,
  `id_envoltura` int NOT NULL,
  `activo` int NOT NULL DEFAULT '1',
  `id_create` int NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `ad_alimento`
--
ALTER TABLE `ad_alimento`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ad_categoria`
--
ALTER TABLE `ad_categoria`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ad_categoria_alimento`
--
ALTER TABLE `ad_categoria_alimento`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ad_contenedor`
--
ALTER TABLE `ad_contenedor`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ad_envoltura`
--
ALTER TABLE `ad_envoltura`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ad_ingrediente`
--
ALTER TABLE `ad_ingrediente`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ad_medida`
--
ALTER TABLE `ad_medida`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ad_mesa`
--
ALTER TABLE `ad_mesa`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ad_mesonero`
--
ALTER TABLE `ad_mesonero`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ad_sabor_ingrediente`
--
ALTER TABLE `ad_sabor_ingrediente`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ad_sala`
--
ALTER TABLE `ad_sala`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cf_configuracion`
--
ALTER TABLE `cf_configuracion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cf_correlativo`
--
ALTER TABLE `cf_correlativo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cf_empresa`
--
ALTER TABLE `cf_empresa`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `un_nombre_empresa` (`nombre`);

--
-- Indices de la tabla `cf_region`
--
ALTER TABLE `cf_region`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cf_sucursal`
--
ALTER TABLE `cf_sucursal`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cj_caja`
--
ALTER TABLE `cj_caja`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cj_cobrar`
--
ALTER TABLE `cj_cobrar`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cj_cobrar_dinero`
--
ALTER TABLE `cj_cobrar_dinero`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cj_movimiento_caja`
--
ALTER TABLE `cj_movimiento_caja`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `co_contenedor`
--
ALTER TABLE `co_contenedor`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cp_detalle_factura`
--
ALTER TABLE `cp_detalle_factura`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cp_detalle_orden`
--
ALTER TABLE `cp_detalle_orden`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cp_factura`
--
ALTER TABLE `cp_factura`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cp_orden`
--
ALTER TABLE `cp_orden`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `dm_mesa_despacho`
--
ALTER TABLE `dm_mesa_despacho`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `dm_tipo_despacho`
--
ALTER TABLE `dm_tipo_despacho`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `dp_despacho`
--
ALTER TABLE `dp_despacho`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `dp_despacho_detalle`
--
ALTER TABLE `dp_despacho_detalle`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `dp_despacho_detalle_ingrediente`
--
ALTER TABLE `dp_despacho_detalle_ingrediente`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `dp_despacho_detalle_ingrediente_adicional`
--
ALTER TABLE `dp_despacho_detalle_ingrediente_adicional`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `dp_estado_despacho`
--
ALTER TABLE `dp_estado_despacho`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `dp_tipo_despacho`
--
ALTER TABLE `dp_tipo_despacho`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `in_concepto_inventario`
--
ALTER TABLE `in_concepto_inventario`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `in_inventario`
--
ALTER TABLE `in_inventario`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `in_transaccion`
--
ALTER TABLE `in_transaccion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mn_alimento`
--
ALTER TABLE `mn_alimento`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mn_alimento_ingrediente`
--
ALTER TABLE `mn_alimento_ingrediente`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mn_alimento_presentacion`
--
ALTER TABLE `mn_alimento_presentacion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mn_alimento_presentacion_ingrediente`
--
ALTER TABLE `mn_alimento_presentacion_ingrediente`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mn_categoria_menu`
--
ALTER TABLE `mn_categoria_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mn_ingrediente`
--
ALTER TABLE `mn_ingrediente`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mn_tipo_presentacion`
--
ALTER TABLE `mn_tipo_presentacion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mt_categoria_producto`
--
ALTER TABLE `mt_categoria_producto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mt_condicion_pago`
--
ALTER TABLE `mt_condicion_pago`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mt_producto`
--
ALTER TABLE `mt_producto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mt_proveedor`
--
ALTER TABLE `mt_proveedor`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mt_tipo_dinero`
--
ALTER TABLE `mt_tipo_dinero`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mt_tipo_movimiento_caja`
--
ALTER TABLE `mt_tipo_movimiento_caja`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mt_unidad_medida`
--
ALTER TABLE `mt_unidad_medida`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sg_modulo`
--
ALTER TABLE `sg_modulo`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uk_nombre_modulo` (`nombre`);

--
-- Indices de la tabla `sg_permiso`
--
ALTER TABLE `sg_permiso`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uk_nombre_permiso` (`nombre`);

--
-- Indices de la tabla `sg_rol`
--
ALTER TABLE `sg_rol`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uk_nombre_rol` (`nombre`);

--
-- Indices de la tabla `sg_rol_modulo`
--
ALTER TABLE `sg_rol_modulo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sg_rol_modulo_permiso`
--
ALTER TABLE `sg_rol_modulo_permiso`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sg_rol_modulo_proceso`
--
ALTER TABLE `sg_rol_modulo_proceso`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sg_sub_modulo`
--
ALTER TABLE `sg_sub_modulo`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uk_nombre_sub_modulo` (`nombre`);

--
-- Indices de la tabla `sg_usuario`
--
ALTER TABLE `sg_usuario`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uk_run_usuario` (`run`,`id_sucursal`);

--
-- Indices de la tabla `sg_usuario_administrador`
--
ALTER TABLE `sg_usuario_administrador`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sg_usuario_empresa`
--
ALTER TABLE `sg_usuario_empresa`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sg_usuario_empresa_modulo`
--
ALTER TABLE `sg_usuario_empresa_modulo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sg_usuario_empresa_modulo_permiso`
--
ALTER TABLE `sg_usuario_empresa_modulo_permiso`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sg_usuario_empresa_modulo_proceso`
--
ALTER TABLE `sg_usuario_empresa_modulo_proceso`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `vt_venta`
--
ALTER TABLE `vt_venta`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `vt_venta_detalle`
--
ALTER TABLE `vt_venta_detalle`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `ad_alimento`
--
ALTER TABLE `ad_alimento`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `ad_categoria`
--
ALTER TABLE `ad_categoria`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `ad_categoria_alimento`
--
ALTER TABLE `ad_categoria_alimento`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `ad_contenedor`
--
ALTER TABLE `ad_contenedor`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `ad_envoltura`
--
ALTER TABLE `ad_envoltura`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `ad_ingrediente`
--
ALTER TABLE `ad_ingrediente`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `ad_medida`
--
ALTER TABLE `ad_medida`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `ad_mesa`
--
ALTER TABLE `ad_mesa`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `ad_mesonero`
--
ALTER TABLE `ad_mesonero`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `ad_sabor_ingrediente`
--
ALTER TABLE `ad_sabor_ingrediente`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT de la tabla `ad_sala`
--
ALTER TABLE `ad_sala`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `cf_configuracion`
--
ALTER TABLE `cf_configuracion`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `cf_correlativo`
--
ALTER TABLE `cf_correlativo`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `cf_empresa`
--
ALTER TABLE `cf_empresa`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT de la tabla `cf_region`
--
ALTER TABLE `cf_region`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `cf_sucursal`
--
ALTER TABLE `cf_sucursal`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT de la tabla `cj_caja`
--
ALTER TABLE `cj_caja`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT de la tabla `cj_cobrar`
--
ALTER TABLE `cj_cobrar`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;

--
-- AUTO_INCREMENT de la tabla `cj_cobrar_dinero`
--
ALTER TABLE `cj_cobrar_dinero`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=112;

--
-- AUTO_INCREMENT de la tabla `cj_movimiento_caja`
--
ALTER TABLE `cj_movimiento_caja`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT de la tabla `co_contenedor`
--
ALTER TABLE `co_contenedor`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT de la tabla `cp_detalle_factura`
--
ALTER TABLE `cp_detalle_factura`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=119;

--
-- AUTO_INCREMENT de la tabla `cp_detalle_orden`
--
ALTER TABLE `cp_detalle_orden`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `cp_factura`
--
ALTER TABLE `cp_factura`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `cp_orden`
--
ALTER TABLE `cp_orden`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `dm_mesa_despacho`
--
ALTER TABLE `dm_mesa_despacho`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `dm_tipo_despacho`
--
ALTER TABLE `dm_tipo_despacho`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `dp_despacho`
--
ALTER TABLE `dp_despacho`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT de la tabla `dp_despacho_detalle`
--
ALTER TABLE `dp_despacho_detalle`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=285;

--
-- AUTO_INCREMENT de la tabla `dp_despacho_detalle_ingrediente`
--
ALTER TABLE `dp_despacho_detalle_ingrediente`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=442;

--
-- AUTO_INCREMENT de la tabla `dp_despacho_detalle_ingrediente_adicional`
--
ALTER TABLE `dp_despacho_detalle_ingrediente_adicional`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT de la tabla `dp_estado_despacho`
--
ALTER TABLE `dp_estado_despacho`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `dp_tipo_despacho`
--
ALTER TABLE `dp_tipo_despacho`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `in_concepto_inventario`
--
ALTER TABLE `in_concepto_inventario`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `in_inventario`
--
ALTER TABLE `in_inventario`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `in_transaccion`
--
ALTER TABLE `in_transaccion`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT de la tabla `mn_alimento`
--
ALTER TABLE `mn_alimento`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT de la tabla `mn_alimento_ingrediente`
--
ALTER TABLE `mn_alimento_ingrediente`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=110;

--
-- AUTO_INCREMENT de la tabla `mn_alimento_presentacion`
--
ALTER TABLE `mn_alimento_presentacion`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;

--
-- AUTO_INCREMENT de la tabla `mn_alimento_presentacion_ingrediente`
--
ALTER TABLE `mn_alimento_presentacion_ingrediente`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT de la tabla `mn_categoria_menu`
--
ALTER TABLE `mn_categoria_menu`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `mn_ingrediente`
--
ALTER TABLE `mn_ingrediente`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT de la tabla `mn_tipo_presentacion`
--
ALTER TABLE `mn_tipo_presentacion`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `mt_categoria_producto`
--
ALTER TABLE `mt_categoria_producto`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `mt_condicion_pago`
--
ALTER TABLE `mt_condicion_pago`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mt_producto`
--
ALTER TABLE `mt_producto`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `mt_proveedor`
--
ALTER TABLE `mt_proveedor`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `mt_tipo_dinero`
--
ALTER TABLE `mt_tipo_dinero`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `mt_tipo_movimiento_caja`
--
ALTER TABLE `mt_tipo_movimiento_caja`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `mt_unidad_medida`
--
ALTER TABLE `mt_unidad_medida`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `sg_modulo`
--
ALTER TABLE `sg_modulo`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `sg_permiso`
--
ALTER TABLE `sg_permiso`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `sg_rol`
--
ALTER TABLE `sg_rol`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `sg_rol_modulo`
--
ALTER TABLE `sg_rol_modulo`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=110;

--
-- AUTO_INCREMENT de la tabla `sg_rol_modulo_permiso`
--
ALTER TABLE `sg_rol_modulo_permiso`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=331;

--
-- AUTO_INCREMENT de la tabla `sg_rol_modulo_proceso`
--
ALTER TABLE `sg_rol_modulo_proceso`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=131;

--
-- AUTO_INCREMENT de la tabla `sg_sub_modulo`
--
ALTER TABLE `sg_sub_modulo`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT de la tabla `sg_usuario`
--
ALTER TABLE `sg_usuario`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT de la tabla `sg_usuario_administrador`
--
ALTER TABLE `sg_usuario_administrador`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;

--
-- AUTO_INCREMENT de la tabla `sg_usuario_empresa`
--
ALTER TABLE `sg_usuario_empresa`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;

--
-- AUTO_INCREMENT de la tabla `sg_usuario_empresa_modulo`
--
ALTER TABLE `sg_usuario_empresa_modulo`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=531;

--
-- AUTO_INCREMENT de la tabla `sg_usuario_empresa_modulo_permiso`
--
ALTER TABLE `sg_usuario_empresa_modulo_permiso`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2563;

--
-- AUTO_INCREMENT de la tabla `sg_usuario_empresa_modulo_proceso`
--
ALTER TABLE `sg_usuario_empresa_modulo_proceso`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1232;

--
-- AUTO_INCREMENT de la tabla `vt_venta`
--
ALTER TABLE `vt_venta`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT de la tabla `vt_venta_detalle`
--
ALTER TABLE `vt_venta_detalle`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
